<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class lib_account_codes {

	// FOR TREASURY ACCOUNTS
	
	public function GetTreasuryAccounts()
	{
		return array(113,1131,1132,11121,2,21,211,2111,2112,212,213,22);
	}

	// FOR ADVANCES TO OFFICERS AND EMPLOYEES
	public function GetAccountsAOE()
	{
		return "'1132'";
	}
	
	// FOR ACCOUNTS PAYABLE
	public function GetAccountsAP()
	{
		return "'2','21','211','2111','2112','212','213','22'";
	}
	
	// FOR ACCOUNTS RECEIVABLE
	public function GetAccountsAR()
	{
		return "'113','1131','1132'";
	}

	// FOR ACCOUNTS RECEIVABLE
	public function GetAccountsATP()
	{
		return "'1132'";
	}	
	
	// FOR BALANCE SHEET
		// CASH & CASH EQUIVALENTS
		public function GetAccountsBSCCE()
		{
			return "'1111','111','1112','11121','11122','112'";
		}
	
		// ACCOUNTS & OTHER RECEIVABLES
		public function GetAccountsBSAOR()
		{
			return "'113','1131','1132'";
		}
	
		// OTHER ASSETS
		public function GetAccountsBSOA()
		{
			return "'114','115'";
		}
	
		// PROPERTY AND EQUIPMENT
		public function GetAccountsBSPE()
		{
			return "'12','121','1211','12111','121111','1212','12121','121211','12122','121221','1213','12131','121311','122','123'";
		}
	
		// ACCOUNTS PAYABLE AND ACCRUED EXPENSES
		public function GetAccountsBSAPAE()
		{
			return "'2','21','211','2111','2112','212','213','22'";
		}
	
		// ASEAN BIODIVERSITY FUND
		public function GetAccountsBSABF()
		{
			return "'31'";
		}
	
		// UNRESTRICTED FUND BALANCE
		public function GetAccountsBSUFB()
		{
			return "'32'";
		}
	
		// UNRESTRICTED FUND BALANCE
		public function GetAccountsBSRFB()
		{
			return "'33'";
		}
	
		// TRANSLATION ADJUSTMENT
		public function GetAccountsBSRTA()
		{
			return "'34'";
		}						
	// END OF BALANCE SHEET
}

/* End of file lib_account_codes.php */