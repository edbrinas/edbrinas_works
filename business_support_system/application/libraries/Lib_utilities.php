<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lib_utilities {

	public function GenerateTID()
	{
		return substr(number_format(time() * rand(),0,'',''),0,14);
	}

	public function ToSentenceCase($sData)
	{
		return ucwords(strtolower($sData));
	}

	public function SanitizeStr($sData)
	{
		return preg_replace('/[^A-Za-z0-9\-]/', ' ', $sData);
	}

	public function encrypt($sData,$eKey)
	{
		/*
		$dirty = array("+", "/", "=", "-");
		$clean = array("_PLUS_", "_SLASH_", "_EQUALS_" , "_MINUS_");
		$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		$encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $eKey, utf8_encode($sData), MCRYPT_MODE_ECB, $eKey);
		$encrypted_string = base64_encode($encrypted_string);
		return str_replace($dirty, $clean, $encrypted_string);
		*/
		$dirty = array("+", "/", "=", "-");
		$clean = array("_PLUS_", "_SLASH_", "_EQUALS_" , "_MINUS_");

		$encrypt_method = "AES-256-CBC";
		$key = hash('sha256', $eKey);
		$iv = substr(hash('sha256', $eKey), 0, 16);
		
		$output = openssl_encrypt($sData, $encrypt_method, $key, 0, $iv);
        return str_replace($dirty, $clean, base64_encode($output));
	}

	public function decrypt($sData,$eKey)
	{
		/*
		$dirty = array("+", "/", "=", "-");
		$clean = array("_PLUS_", "_SLASH_", "_EQUALS_", "_MINUS_");
		$string = base64_decode(str_replace($clean, $dirty, $sData));
		return (int)mcrypt_decrypt(MCRYPT_BLOWFISH,$eKey,$string, MCRYPT_MODE_ECB, $eKey);
		*/
		$dirty = array("+", "/", "=", "-");
		$clean = array("_PLUS_", "_SLASH_", "_EQUALS_", "_MINUS_");
		$encrypt_method = "AES-256-CBC";
		$key = hash('sha256', $eKey);
		$iv = substr(hash('sha256', $eKey), 0, 16);
		return openssl_decrypt(base64_decode(str_replace($clean, $dirty, $sData)), $encrypt_method, $key, 0, $iv);		
	}

	public function PopUpConfiguration()
	{
		return array('menubar'=>'no','width'=>1080,'height'=>700,'scrollbars' =>'yes','status'=>'no','resizable'=>'no','screenx'=> 0,'screeny'=>0);
	}

	private function ExecuteCURL($url)
	{
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 0);
		$rawdata = curl_exec($ch);
		curl_close($ch);
		return $rawdata;
	}

	public function GenerateCaptcha()
	{
		$captcha_config = array(
			'word_length' => 4,
            'img_path'   => './_plugins/_captcha/',
            'img_url'    => site_url().'_plugins/_captcha/',
            'font_path'  => BASEPATH.'system/fonts/',
            'fonts'      => array('PAPYRUS.TTF'),
            'img_width'  => 145,
            'img_height' => 50,
            'expiration' => 7200
        );
		return $captcha_config;
	}

	public function GetIP()
	{
		#return $this->ExecuteCURL("http://ipecho.net/plain");
		return "127.0.0.1";
	}

	public function GetAccountTypes()
	{
		return array(	1 => 'ADMINISTRATOR',
						2 => 'ADD/EDIT/VIEW',
						3 => 'DELETE ACCOUNT',
						4 => 'APPROVE ACCOUNT',
						5 => 'CANCEL ACCOUNT');
	}

	public function GetErrorMsg($err_id)
	{
		$a_records = array ( 	'0x0E' => 'FATAL ERROR: ONE OR MORE ARGUMENTS ARE NOT CORRECT. PLEASE CONTACT YOUR SYSTEM ADMINISTRATOR.',
								'0x1E' => 'FATAL ERROR: THE DATABASE HANDLE IS INVALID. PLEASE CONTACT YOUR SYSTEM ADMINISTRATOR.',
								'0x2E' => 'Record already exists.',
								'0x3E' => 'Invalid file type! This system only accepts CSV-file format files.',
								'0x4E' => 'Please select a CSV file to upload.',
								'0x5E' => 'Transaction not balanced. Please double check the transaction details.',
								'0x6E' => 'Please insert at least one (1) transaction.',
								'0x7E' => 'Email address already exists.',
								'0x8E' => 'Session timed out, please refresh this page.',
								'0x9E' => 'Please select a date greater than today\'s date.',
								'0x0S' => 'Record added successfully.',
								'0x1S' => 'Record updated successfully.',
								'0x2S' => 'Record deleted successfully.',
								'0x3S' => 'Data uploaded successfully.',
								'0x4S' => 'Selected date(s) has been reserved.',
								'0x5S' => 'Incident and Service request has been created.',
								'0x1Sys' => 'You have been logged off due to inactivity.',
								'0x2Sys' => 'Reservations has been made for the selected date(s).',
								'0x3Sys' => 'Logon Failure: Invalid username or password.',
								'0x4Sys' => 'Logon Failure: Invalid verification code.',
								'0x5Sys' => 'You have successfully logged out.',
								'0x6Sys' => 'Access Denied - You do not have sufficient permissions to access this page. You have been logged off.',
								'0x7Sys' => 'You have been logged off due to inactivity.',
								'0x8Sys' => 'Logon Failure: Account Currently Disabled.');
		return $a_records[$err_id];
	}

	public function GetBankAcctType()
	{
		$a_records = array (	'CURRENT ACCOUNT',
								'SAVINGS ACCOUNT',
								'CHECKING ACCOUNT',
								'CERTIFICATE OF DEPOSIT (CD)',
								'MONEY MARKET ACCOUNT',
								'INDIVIDUAL RETIREMENT ACCOUNTS (IRAS)',
								'TRUST ACCOUNT',
								'TIME DEPOSIT ACCOUNT',
								'BROKERAGE ACCOUNT',
								'CREDIT CARD');
		asort($a_records);
		return $a_records;
	}

	public function GetAssetType()
	{
		$a_records = array (	'COMMUNICATION EQUIPMENT',
								'AUDIO VISUAL EQUIPMENT',
								'COMPUTER EQUIPMENT',
								'COMPUTER SOFTWARE',
								'OFFICE EQUIPMENT',
								'OTHER EQUIPMENT',
								'ELECTRICAL EQUIPMENT',
								'FIELD EQUIPMENT',
								'SAFETY SUPPLIES',
								'TOOLS',
								'TRANSPORTATION EQUIPMENT',
								'FURNITURE AND FIXTURES',
								'VEHICLE',
								'GROUNDS AND BUILDING');
		asort($a_records);
		return $a_records;
	}

	public function GetCountries()
	{
		$a_records = array ('AD'=>'ANDORRA',
							'AE'=>'UNITED ARAB EMIRATES',
							'AF'=>'AFGHANISTAN',
							'AG'=>'ANTIGUA AND BARBUDA',
							'AI'=>'ANGUILLA',
							'AL'=>'ALBANIA',
							'AM'=>'ARMENIA',
							'AN'=>'NETHERLANDS ANTILLES',
							'AO'=>'ANGOLA',
							'AQ'=>'ANTARCTICA',
							'AR'=>'ARGENTINA',
							'AS'=>'AMERICAN SAMOA',
							'AT'=>'AUSTRIA',
							'AU'=>'AUSTRALIA',
							'AW'=>'ARUBA',
							'AZ'=>'AZERBAIJAN',
							'BA'=>'BOSNIA AND HERZEGOVINA',
							'BB'=>'BARBADOS',
							'BD'=>'BANGLADESH',
							'BE'=>'BELGIUM',
							'BF'=>'BURKINA FASO',
							'BG'=>'BULGARIA',
							'BH'=>'BAHRAIN',
							'BI'=>'BURUNDI',
							'BJ'=>'BENIN',
							'BM'=>'BERMUDA',
							'BN'=>'BRUNEI',
							'BO'=>'BOLIVIA',
							'BR'=>'BRAZIL',
							'BS'=>'BAHAMAS',
							'BT'=>'BHUTAN',
							'BV'=>'BOUVET ISLAND',
							'BW'=>'BOTSWANA',
							'BY'=>'BELARUS',
							'BZ'=>'BELIZE',
							'CA'=>'CANADA',
							'CC'=>'COCOS [KEELING] ISLANDS',
							'CD'=>'CONGO [DRC]',
							'CF'=>'CENTRAL AFRICAN REPUBLIC',
							'CG'=>'CONGO [REPUBLIC]',
							'CH'=>'SWITZERLAND',
							'CI'=>'C�TE DIVOIRE',
							'CK'=>'COOK ISLANDS',
							'CL'=>'CHILE',
							'CM'=>'CAMEROON',
							'CN'=>'CHINA',
							'CO'=>'COLOMBIA',
							'CR'=>'COSTA RICA',
							'CU'=>'CUBA',
							'CV'=>'CAPE VERDE',
							'CX'=>'CHRISTMAS ISLAND',
							'CY'=>'CYPRUS',
							'CZ'=>'CZECH REPUBLIC',
							'DE'=>'GERMANY',
							'DJ'=>'DJIBOUTI',
							'DK'=>'DENMARK',
							'DM'=>'DOMINICA',
							'DO'=>'DOMINICAN REPUBLIC',
							'DZ'=>'ALGERIA',
							'EC'=>'ECUADOR',
							'EE'=>'ESTONIA',
							'EG'=>'EGYPT',
							'EH'=>'WESTERN SAHARA',
							'ER'=>'ERITREA',
							'ES'=>'SPAIN',
							'ET'=>'ETHIOPIA',
							'FI'=>'FINLAND',
							'FJ'=>'FIJI',
							'FK'=>'FALKLAND ISLANDS [ISLAS MALVINAS]',
							'FM'=>'MICRONESIA',
							'FO'=>'FAROE ISLANDS',
							'FR'=>'FRANCE',
							'GA'=>'GABON',
							'GB'=>'UNITED KINGDOM',
							'GD'=>'GRENADA',
							'GE'=>'GEORGIA',
							'GF'=>'FRENCH GUIANA',
							'GG'=>'GUERNSEY',
							'GH'=>'GHANA',
							'GI'=>'GIBRALTAR',
							'GL'=>'GREENLAND',
							'GM'=>'GAMBIA',
							'GN'=>'GUINEA',
							'GP'=>'GUADELOUPE',
							'GQ'=>'EQUATORIAL GUINEA',
							'GR'=>'GREECE',
							'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
							'GT'=>'GUATEMALA',
							'GU'=>'GUAM',
							'GW'=>'GUINEA-BISSAU',
							'GY'=>'GUYANA',
							'GZ'=>'GAZA STRIP',
							'HK'=>'HONG KONG',
							'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
							'HN'=>'HONDURAS',
							'HR'=>'CROATIA',
							'HT'=>'HAITI',
							'HU'=>'HUNGARY',
							'ID'=>'INDONESIA',
							'IE'=>'IRELAND',
							'IL'=>'ISRAEL',
							'IM'=>'ISLE OF MAN',
							'IN'=>'INDIA',
							'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
							'IQ'=>'IRAQ',
							'IR'=>'IRAN',
							'IS'=>'ICELAND',
							'IT'=>'ITALY',
							'JE'=>'JERSEY',
							'JM'=>'JAMAICA',
							'JO'=>'JORDAN',
							'JP'=>'JAPAN',
							'KE'=>'KENYA',
							'KG'=>'KYRGYZSTAN',
							'KH'=>'CAMBODIA',
							'KI'=>'KIRIBATI',
							'KM'=>'COMOROS',
							'KN'=>'SAINT KITTS AND NEVIS',
							'KP'=>'NORTH KOREA',
							'KR'=>'SOUTH KOREA',
							'KW'=>'KUWAIT',
							'KY'=>'CAYMAN ISLANDS',
							'KZ'=>'KAZAKHSTAN',
							'LA'=>'LAOS',
							'LB'=>'LEBANON',
							'LC'=>'SAINT LUCIA',
							'LI'=>'LIECHTENSTEIN',
							'LK'=>'SRI LANKA',
							'LR'=>'LIBERIA',
							'LS'=>'LESOTHO',
							'LT'=>'LITHUANIA',
							'LU'=>'LUXEMBOURG',
							'LV'=>'LATVIA',
							'LY'=>'LIBYA',
							'MA'=>'MOROCCO',
							'MC'=>'MONACO',
							'MD'=>'MOLDOVA',
							'ME'=>'MONTENEGRO',
							'MG'=>'MADAGASCAR',
							'MH'=>'MARSHALL ISLANDS',
							'MK'=>'MACEDONIA [FYROM]',
							'ML'=>'MALI',
							'MM'=>'MYANMAR [BURMA]',
							'MN'=>'MONGOLIA',
							'MO'=>'MACAU',
							'MP'=>'NORTHERN MARIANA ISLANDS',
							'MQ'=>'MARTINIQUE',
							'MR'=>'MAURITANIA',
							'MS'=>'MONTSERRAT',
							'MT'=>'MALTA',
							'MU'=>'MAURITIUS',
							'MV'=>'MALDIVES',
							'MW'=>'MALAWI',
							'MX'=>'MEXICO',
							'MY'=>'MALAYSIA',
							'MZ'=>'MOZAMBIQUE',
							'NA'=>'NAMIBIA',
							'NC'=>'NEW CALEDONIA',
							'NE'=>'NIGER',
							'NF'=>'NORFOLK ISLAND',
							'NG'=>'NIGERIA',
							'NI'=>'NICARAGUA',
							'NL'=>'NETHERLANDS',
							'NO'=>'NORWAY',
							'NP'=>'NEPAL',
							'NR'=>'NAURU',
							'NU'=>'NIUE',
							'NZ'=>'NEW ZEALAND',
							'OM'=>'OMAN',
							'PA'=>'PANAMA',
							'PE'=>'PERU',
							'PF'=>'FRENCH POLYNESIA',
							'PG'=>'PAPUA NEW GUINEA',
							'PH'=>'PHILIPPINES',
							'PK'=>'PAKISTAN',
							'PL'=>'POLAND',
							'PM'=>'SAINT PIERRE AND MIQUELON',
							'PN'=>'PITCAIRN ISLANDS',
							'PR'=>'PUERTO RICO',
							'PS'=>'PALESTINIAN TERRITORIES',
							'PT'=>'PORTUGAL',
							'PW'=>'PALAU',
							'PY'=>'PARAGUAY',
							'QA'=>'QATAR',
							'RE'=>'R�UNION',
							'RO'=>'ROMANIA',
							'RS'=>'SERBIA',
							'RU'=>'RUSSIA',
							'RW'=>'RWANDA',
							'SA'=>'SAUDI ARABIA',
							'SB'=>'SOLOMON ISLANDS',
							'SC'=>'SEYCHELLES',
							'SD'=>'SUDAN',
							'SE'=>'SWEDEN',
							'SG'=>'SINGAPORE',
							'SH'=>'SAINT HELENA',
							'SI'=>'SLOVENIA',
							'SJ'=>'SVALBARD AND JAN MAYEN',
							'SK'=>'SLOVAKIA',
							'SL'=>'SIERRA LEONE',
							'SM'=>'SAN MARINO',
							'SN'=>'SENEGAL',
							'SO'=>'SOMALIA',
							'SR'=>'SURINAME',
							'ST'=>'S�O TOM� AND PR�NCIPE',
							'SV'=>'EL SALVADOR',
							'SY'=>'SYRIA',
							'SZ'=>'SWAZILAND',
							'TC'=>'TURKS AND CAICOS ISLANDS',
							'TD'=>'CHAD',
							'TF'=>'FRENCH SOUTHERN TERRITORIES',
							'TG'=>'TOGO',
							'TH'=>'THAILAND',
							'TJ'=>'TAJIKISTAN',
							'TK'=>'TOKELAU',
							'TL'=>'TIMOR-LESTE',
							'TM'=>'TURKMENISTAN',
							'TN'=>'TUNISIA',
							'TO'=>'TONGA',
							'TR'=>'TURKEY',
							'TT'=>'TRINIDAD AND TOBAGO',
							'TV'=>'TUVALU',
							'TW'=>'TAIWAN',
							'TZ'=>'TANZANIA',
							'UA'=>'UKRAINE',
							'UG'=>'UGANDA',
							'UM'=>'U.S. MINOR OUTLYING ISLANDS',
							'US'=>'UNITED STATES',
							'UY'=>'URUGUAY',
							'UZ'=>'UZBEKISTAN',
							'VA'=>'VATICAN CITY',
							'VC'=>'SAINT VINCENT AND THE GRENADINES',
							'VE'=>'VENEZUELA',
							'VG'=>'BRITISH VIRGIN ISLANDS',
							'VI'=>'U.S. VIRGIN ISLANDS',
							'VN'=>'VIETNAM',
							'VU'=>'VANUATU',
							'WF'=>'WALLIS AND FUTUNA',
							'WS'=>'SAMOA',
							'XK'=>'KOSOVO',
							'YE'=>'YEMEN',
							'YT'=>'MAYOTTE',
							'ZA'=>'SOUTH AFRICA',
							'ZM'=>'ZAMBIA',
							'ZW'=>'ZIMBABWE');
		asort($a_records);
		return $a_records;
	}



	public function GetCurrencies()
	{
		$a_records = array (	'AED'=>'UNITED ARAB EMIRATES DIRHAM',
								'AFN'=>'AFGHANISTAN AFGHANI',
								'ALL'=>'ALBANIA LEK',
								'AMD'=>'ARMENIA DRAM',
								'ANG'=>'NETHERLANDS ANTILLES GUILDER',
								'AOA'=>'ANGOLA KWANZA',
								'ARS'=>'ARGENTINA PESO',
								'AUD'=>'AUSTRALIA DOLLAR',
								'AWG'=>'ARUBA GUILDER',
								'AZN'=>'AZERBAIJAN NEW MANAT',
								'BAM'=>'BOSNIA AND HERZEGOVINA CONVERTIBLE MARKA',
								'BBD'=>'BARBADOS DOLLAR',
								'BDT'=>'BANGLADESH TAKA',
								'BGN'=>'BULGARIA LEV',
								'BHD'=>'BAHRAIN DINAR',
								'BIF'=>'BURUNDI FRANC',
								'BMD'=>'BERMUDA DOLLAR',
								'BND'=>'BRUNEI DARUSSALAM DOLLAR',
								'BOB'=>'BOLIVIA BOLIVIANO',
								'BRL'=>'BRAZIL REAL',
								'BSD'=>'BAHAMAS DOLLAR',
								'BTN'=>'BHUTAN NGULTRUM',
								'BWP'=>'BOTSWANA PULA',
								'BYR'=>'BELARUS RUBLE',
								'BZD'=>'BELIZE DOLLAR',
								'CAD'=>'CANADA DOLLAR',
								'CDF'=>'CONGO/KINSHASA FRANC',
								'CHF'=>'SWITZERLAND FRANC',
								'CLP'=>'CHILE PESO',
								'CNY'=>'CHINA YUAN RENMINBI',
								'COP'=>'COLOMBIA PESO',
								'CRC'=>'COSTA RICA COLON',
								'CUC'=>'CUBA CONVERTIBLE PESO',
								'CUP'=>'CUBA PESO',
								'CVE'=>'CAPE VERDE ESCUDO',
								'CZK'=>'CZECH REPUBLIC KORUNA',
								'DJF'=>'DJIBOUTI FRANC',
								'DKK'=>'DENMARK KRONE',
								'DOP'=>'DOMINICAN REPUBLIC PESO',
								'DZD'=>'ALGERIA DINAR',
								'EGP'=>'EGYPT POUND',
								'ERN'=>'ERITREA NAKFA',
								'ETB'=>'ETHIOPIA BIRR',
								'EUR'=>'EURO MEMBER COUNTRIES',
								'FJD'=>'FIJI DOLLAR',
								'FKP'=>'FALKLAND ISLANDS (MALVINAS) POUND',
								'GBP'=>'UNITED KINGDOM POUND',
								'GEL'=>'GEORGIA LARI',
								'GGP'=>'GUERNSEY POUND',
								'GHS'=>'GHANA CEDI',
								'GIP'=>'GIBRALTAR POUND',
								'GMD'=>'GAMBIA DALASI',
								'GNF'=>'GUINEA FRANC',
								'GTQ'=>'GUATEMALA QUETZAL',
								'GYD'=>'GUYANA DOLLAR',
								'HKD'=>'HONG KONG DOLLAR',
								'HNL'=>'HONDURAS LEMPIRA',
								'HRK'=>'CROATIA KUNA',
								'HTG'=>'HAITI GOURDE',
								'HUF'=>'HUNGARY FORINT',
								'IDR'=>'INDONESIA RUPIAH',
								'ILS'=>'ISRAEL SHEKEL',
								'IMP'=>'ISLE OF MAN POUND',
								'INR'=>'INDIA RUPEE',
								'IQD'=>'IRAQ DINAR',
								'IRR'=>'IRAN RIAL',
								'ISK'=>'ICELAND KRONA',
								'JEP'=>'JERSEY POUND',
								'JMD'=>'JAMAICA DOLLAR',
								'JOD'=>'JORDAN DINAR',
								'JPY'=>'JAPAN YEN',
								'KES'=>'KENYA SHILLING',
								'KGS'=>'KYRGYZSTAN SOM',
								'KHR'=>'CAMBODIA RIEL',
								'KMF'=>'COMOROS FRANC',
								'KPW'=>'KOREA (NORTH) WON',
								'KRW'=>'KOREA (SOUTH) WON',
								'KWD'=>'KUWAIT DINAR',
								'KYD'=>'CAYMAN ISLANDS DOLLAR',
								'KZT'=>'KAZAKHSTAN TENGE',
								'LAK'=>'LAOS KIP',
								'LBP'=>'LEBANON POUND',
								'LKR'=>'SRI LANKA RUPEE',
								'LRD'=>'LIBERIA DOLLAR',
								'LSL'=>'LESOTHO LOTI',
								'LTL'=>'LITHUANIA LITAS',
								'LYD'=>'LIBYA DINAR',
								'MAD'=>'MOROCCO DIRHAM',
								'MDL'=>'MOLDOVA LEU',
								'MGA'=>'MADAGASCAR ARIARY',
								'MKD'=>'MACEDONIA DENAR',
								'MMK'=>'MYANMAR (BURMA) KYAT',
								'MNT'=>'MONGOLIA TUGHRIK',
								'MOP'=>'MACAU PATACA',
								'MRO'=>'MAURITANIA OUGUIYA',
								'MUR'=>'MAURITIUS RUPEE',
								'MVR'=>'MALDIVES (MALDIVE ISLANDS) RUFIYAA',
								'MWK'=>'MALAWI KWACHA',
								'MXN'=>'MEXICO PESO',
								'MYR'=>'MALAYSIA RINGGIT',
								'MZN'=>'MOZAMBIQUE METICAL',
								'NAD'=>'NAMIBIA DOLLAR',
								'NGN'=>'NIGERIA NAIRA',
								'NIO'=>'NICARAGUA CORDOBA',
								'NOK'=>'NORWAY KRONE',
								'NPR'=>'NEPAL RUPEE',
								'NZD'=>'NEW ZEALAND DOLLAR',
								'OMR'=>'OMAN RIAL',
								'PAB'=>'PANAMA BALBOA',
								'PEN'=>'PERU NUEVO SOL',
								'PGK'=>'PAPUA NEW GUINEA KINA',
								'PHP'=>'PHILIPPINES PESO',
								'PKR'=>'PAKISTAN RUPEE',
								'PLN'=>'POLAND ZLOTY',
								'PYG'=>'PARAGUAY GUARANI',
								'QAR'=>'QATAR RIYAL',
								'RON'=>'ROMANIA NEW LEU',
								'RSD'=>'SERBIA DINAR',
								'RUB'=>'RUSSIA RUBLE',
								'RWF'=>'RWANDA FRANC',
								'SAR'=>'SAUDI ARABIA RIYAL',
								'SBD'=>'SOLOMON ISLANDS DOLLAR',
								'SCR'=>'SEYCHELLES RUPEE',
								'SDG'=>'SUDAN POUND',
								'SEK'=>'SWEDEN KRONA',
								'SGD'=>'SINGAPORE DOLLAR',
								'SHP'=>'SAINT HELENA POUND',
								'SLL'=>'SIERRA LEONE LEONE',
								'SOS'=>'SOMALIA SHILLING',
								'SPL'=>'SEBORGA LUIGINO',
								'SRD'=>'SURINAME DOLLAR',
								'STD'=>'S�O TOM� AND PR�NCIPE DOBRA',
								'SVC'=>'EL SALVADOR COLON',
								'SYP'=>'SYRIA POUND',
								'SZL'=>'SWAZILAND LILANGENI',
								'THB'=>'THAILAND BAHT',
								'TJS'=>'TAJIKISTAN SOMONI',
								'TMT'=>'TURKMENISTAN MANAT',
								'TND'=>'TUNISIA DINAR',
								'TOP'=>'TONGA PAANGA',
								'TRY'=>'TURKEY LIRA',
								'TTD'=>'TRINIDAD AND TOBAGO DOLLAR',
								'TVD'=>'TUVALU DOLLAR',
								'TWD'=>'TAIWAN NEW DOLLAR',
								'TZS'=>'TANZANIA SHILLING',
								'UAH'=>'UKRAINE HRYVNIA',
								'UGX'=>'UGANDA SHILLING',
								'USD'=>'UNITED STATES DOLLAR',
								'UYU'=>'URUGUAY PESO',
								'UZS'=>'UZBEKISTAN SOM',
								'VEF'=>'VENEZUELA BOLIVAR',
								'VND'=>'VIET NAM DONG',
								'VUV'=>'VANUATU VATU',
								'WST'=>'SAMOA TALA',
								'XAF'=>'COMMUNAUTE FINANCIERE AFRICAINE (BEAC) CFA FRANC BEAC',
								'XCD'=>'EAST CARIBBEAN DOLLAR',
								'XDR'=>'INTERNATIONAL MONETARY FUND (IMF) SPECIAL DRAWING RIGHTS',
								'XOF'=>'COMMUNAUT� FINANCI�RE AFRICAINE (BCEAO) FRANC',
								'XPF'=>'COMPTOIRS FRAN�AIS DU PACIFIQUE (CFP) FRANC',
								'YER'=>'YEMEN RIAL',
								'ZAR'=>'SOUTH AFRICA RAND');
		asort($a_records);
		return $a_records;
	}

	public function GetMaritalStatus()
	{
		$a_records = array('MARRIED','WIDOWED','SEPARATED','DIVORCED','SINGLE');
		asort($a_records);
		return $a_records;
	}

	public function GenerateRandomChars()
	{
		$chars = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
		$password = substr( str_shuffle( $chars ), 0, 5 );
		return $password;
	}

	public function GetEmpType()
	{
		$a_records = array('CONTRACTUAL EMPLOYEE','REGULAR EMPLOYEE');
		asort($a_records);
		return $a_records;
	}

	public function GetVehicleMake()
	{
		$a_records = array('TOYOTA','MITSUBISHI','HONDA','ISUZU','SUZUKI','SUBARU','NISSAN','HYUNDAI','FORD');
		asort($a_records);
		return $a_records;
	}

	private function ConvertToName($num, $tri)
	{
		 $ones = array(
		 "",
		 " One",
		 " Two",
		 " Three",
		 " Four",
		 " Five",
		 " Six",
		 " Seven",
		 " Eight",
		 " Nine",
		 " Ten",
		 " Eleven",
		 " Twelve",
		 " Thirteen",
		 " Fourteen",
		 " Fifteen",
		 " Sixteen",
		 " Seventeen",
		 " Eighteen",
		 " Nineteen"
		);

		$tens = array(
		 "",
		 "",
		 " Twenty",
		 " Thirty",
		 " Forty",
		 " Fifty",
		 " Sixty",
		 " Seventy",
		 " Eighty",
		 " Ninety"
		);

		$triplets = array(
		 "",
		 " Thousand",
		 " Million",
		 " Billion",
		 " Trillion",
		 " Quadrillion",
		 " Quintillion",
		 " Sextillion",
		 " Septillion",
		 " Octillion",
		 " Nonillion"
		);

		// chunk the number, ...rxyy
		$r = (int) ($num / 1000);
		$x = ($num / 100) % 10;
		$y = $num % 100;


		// init the output string
		$str = "";

		// do hundreds
		if ($x > 0)
		$str = $ones[$x] . " Hundred";

		// do ones and tens
		if ($y < 20)
		$str .= $ones[$y];
		else
		$str .= $tens[(int) ($y / 10)] . $ones[$y % 10];

		if ($str != "")
		$str .= $triplets[$tri];

		if ($r > 0)
		{
			return $this->ConvertToName($r, $tri+1).$str;
		}
		else
		{
			return $str;
		}
	}

	public function ConvertToWord($number)
	{
		$number = str_replace(",","",$number);
		$number = number_format($number,2,'.','');
		$decimal = substr($number,strlen($number)-2,strlen($number)-1);
		$number = (int)$number;

		if ($number < 0)
		{
			return "negative".$this->ConvertToName(-$number, 0);
		}

		if ($number == 0)
		{
			return "ZERO";
		}

		return $this->ConvertToName($number, 0)." & {$decimal}/100";
	}

	public function ComputeForex($amount="",$forex="")
	{
		return $amount*$forex;
	}

	public function GetDepartments()
	{
		$a_records = array('EXECUTIVE MANAGEMENT','OPERATIONS','MARKETING','PRODUCT MANAGEMENT','ENGINEERING','BUSINESS MANAGEMENT','INFORMATION TECHNOLOGY','BUILDING MANAGEMENT','HUMAN RESOURCE');
		asort($a_records);
		return $a_records;
	}

	public function GetLeaveTypes()
	{
		$a_records = array('ANNUAL LEAVE','SICK LEAVE','BEREAVEMENT LEAVE','HAJJ','MARRIAGE LEAVE','MATERNITY LEAVE','PATERNITY LEAVE','CHILD BIRTH','EID');
		asort($a_records);
		return $a_records;
	}

	public function GetAssetCat()
	{
		$a_records = array('FIREWALL','GLOBAL POSITIONING SYSTEM','AIR CONDITIONING UNIT','SOFTWARE APPLICATION','VIRTUAL MACHINE','TABLET','LAPTOP','DESKTOP','SERVER','HARD DISK','STORAGE','ROUTER','SWITCH','RACK','UPS','KEYBOARD','TRACK-PAD','MOUSE','MONITOR','LOAD BALANCER','PRINTER','SCANNER','HEADSET','TELEPHONE','IP-TELEPHONE');
		asort($a_records);
		return $a_records;
	}

	public function GetAssetManu()
	{
		$a_records = array('DIGI','PALO ALTO','MEINBERG','EMERSON','RED HAT ENTERPRISE','MICROSOFT','ASUS','ACER','HP','DELL','CISCO','VMWARE','EMC','LOGITECH','SEAGATE','WESTERN DIGITAL','IBM','SUN/ORACLE','APPLE','SAMSUNG','LINKSYS','FUJITSU','LENOVO','EPSON','BROTHER','ADOBE');
		asort($a_records);
		return $a_records;
	}

	public function GetAssetClass()
	{
		$a_records = array('HARDWARE','SOFTWARE','VIRTUAL MACHINE');
		asort($a_records);
		return $a_records;
	}

	public function GetAssetState()
	{
		$a_records = array('SPARE UNIT','IN-USE','DECOMMISSIONED','BROKEN','PULLED-OUT - FOR REPLACEMENT','PULLED-OUT - IN MAINTENANCE','STOLEN');
		asort($a_records);
		return $a_records;
	}

	public function GetIrStat()
	{
		$a_records = array('TROUBLESHOOTING ON-GOING','AWAITING CUSTOMER RESPONSE','RESOLVED - AWAITING CUSTOMER RESPONSE','CLOSED');
		#asort($a_records);
		return $a_records;
	}

	public function GetSites()
	{
		$a_records = array('MAIN SITE','BACK-UP SITE');
		asort($a_records);
		return $a_records;
	}

	public function GetIrPriority()
	{
		$a_records = array('URGENT - EMERGENCY','LOW','MEDIUM','HIGH');
		asort($a_records);
		return $a_records;
	}

	public function GetIrType()
	{
		$a_records = array('ACCESS/SECURITY','HARDWARE','SOFTWARE/BUSINESS APPLICATION','TELEPHONY');
		asort($a_records);
		return $a_records;
	}

	public function GetIrSubType()
	{
		$a_records = array(	'ACCESS/SECURITY'=>array('ACCESS TO A NETWORK FOLDER','APPLICATION ACCESS','INTRUSION/VIRUS','NETWORK/INTERNET ACCCESS','REMOTE ACCESS','OTHER'),
							'HARDWARE'=>array('BROKEN COMPONENT','CONFIGURATION','MAINTENANCE','PERFORMANCE ISSUES','OTHER'),
							'SOFTWARE/BUSINESS APPLICATION'=>array('CONFIGURATION','DEFECT/ERROR MESSAGE','DELAYED RESPONSE','OTHER'),
							'TELEPHONY'=>array('AUDIO QUALITY','BROKEN TELEPHONE','BROKEN TELEPHONE ACCESSORY','CONFIGURATION','OTHER'));
		asort($a_records);
		return $a_records;
	}

	public function GetSrType()
	{
		$a_records = array('ACCESS/SECURITY','HARDWARE','SOFTWARE/BUSINESS APPLICATION','TELEPHONY');
		asort($a_records);
		return $a_records;
	}

	public function GetSrSubType()
	{
		$a_records = array(	'ACCESS/SECURITY'=>array('REQUEST ACCESS TO A NETWORK FOLDER','REQUEST ACCESS APPLICATION ACCESS','REQUEST REMOVAL INTRUSION/VIRUS','REQUEST ACCESS TO NETWORK/INTERNET','REQUEST FOR REMOTE ACCESS','OTHER'),
							'HARDWARE'=>array('REQUEST FOR PROCUREMENT','REQUEST FOR REPAIR BROKEN COMPONENT','REQUEST FOR RE-CONFIGURATION','REQUEST FOR MAINTENANCE','REQUEST FOR CHECKING PERFORMANCE ISSUES','OTHER'),
							'SOFTWARE/BUSINESS APPLICATION'=>array('REQUEST FOR RE-CONFIGURATION','REQUEST FOR REPAIR DEFECT/ERROR MESSAGE','REQUEST FOR REPAIR DELAYED RESPONSE','OTHER'),
							'TELEPHONY'=>array('REQUEST FOR AUDIO QUALITY CHECKING','REQUEST TO FIX BROKEN TELEPHONE','REQUEST TO FIX BROKEN TELEPHONE ACCESSORY','REQUEST FOR RE-CONFIGURATION','OTHER'));
		asort($a_records);
		return $a_records;
	}

	public function CheckFileType($options=array())
	{
		$f_ext = substr(strrchr($options["file_to_upload"]["name"],'.'),1);
		$a_ftype = array("txt","docx","doc","xls","xlsx","ppt","pptx","jpg","jpeg","gif","pdf","png");
		if (in_array(strtolower($f_ext), $a_ftype)) {
			return true;
		}
		return false;
	}

	public function CheckFileSize($options=array())
	{
		if ($options["file_to_upload"]["size"] > 20000000) {
			return false;
		}
		return true;
	}

	public function DeleteFile($options=array())
	{
		if (unlink('./_temp_files/'.basename($options["file_name"]))) {
			return true;
		}
		return false;
	}

	public function UploadFile($options=array())
	{
		if (move_uploaded_file($options["file_to_upload"]["tmp_name"], './_temp_files/'.basename($options["file_to_upload"]["name"]))) {
			return true;
		}
		return true;
	}
	public function FormatUsername($string=NULL)
	{
		return substr($string,0,strpos($string,"@"));
	}

	public function GetDates($sDate, $eDate)
	{
		$dates = array();
		$sDate = strtotime($sDate);
		$eDate = strtotime($eDate);
		while( $sDate <= $eDate ) {
			$dates[] = date('Y-m-d', $sDate);
			$sDate = strtotime('+1 day', $sDate);
		}
		return $dates;
	}

	public function GetDateTimeLapsed($start, $end){

		$startDate = new DateTime($start);
		$endDate = new DateTime($end);
		$periodInterval = new DateInterval( "PT1H" );

		$period = new DatePeriod( $startDate, $periodInterval, $endDate );
		$count = 0;

		foreach($period as $date){
			$startofday = clone $date;
			$startofday->setTime(8,30);
			$endofday = clone $date;
			$endofday->setTime(17,30);
			if($date > $startofday && $date <= $endofday && !in_array($date->format('l'), array('Sunday','Saturday'))){
				$count++;
			}
		}

		//Get seconds of Start time
		$start_d = date("Y-m-d H:00:00", strtotime($start));
		$start_d_seconds = strtotime($start_d);
		$start_t_seconds = strtotime($start);
		$start_seconds = $start_t_seconds - $start_d_seconds;

		//Get seconds of End time
		$end_d = date("Y-m-d H:00:00", strtotime($end));
		$end_d_seconds = strtotime($end_d);
		$end_t_seconds = strtotime($end);
		$end_seconds = $end_t_seconds - $end_d_seconds;

		$diff = $end_seconds-$start_seconds;

		if($diff!=0):
			$count--;
		endif;

		if ($count<0) {
			return "00:".date('i:s',$diff);
		}
		return $count.":".date('i:s',$diff);
	}

	public function GetDays($sDate, $eDate)
	{
		$sDays = floor((strtotime($eDate) - strtotime($sDate)) / (60 * 60 * 24));
		return abs($sDays);
	}


}

/* End of file lib_utilities.php */