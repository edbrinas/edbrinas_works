<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_asset extends CI_Model {

	function __construct() 
	{
        parent::__construct();
        $this->db = $this->load->database('main_db', TRUE);
    }

	private function _cleanRecord($options=array())
	{
		foreach($options as $key=>$value)
		{
			$clean_array[$key] = !is_array($value) ? addslashes(trim(strtoupper($value))) : "";
		}
		return $clean_array;
	}
		
	public function AddRecord($options=array())
	{			
		$options = $this->_cleanRecord($options);
		
		$sql = "INSERT INTO data_asset (model_cat,model_name,model_manu,serial_num,asset_tag,asset_state,assigned_to,asset_class,asset_loc,asset_dept,assigned_date,installed_date,wxp_date,asset_specs,last_update_by) 
				VALUES ('{$options['model_cat']}',
						'{$options['model_name']}',
						'{$options['model_manu']}',
						'{$options['serial_num']}',
						'{$options['asset_tag']}',
						'NEW',
						'{$options['assigned_to']}',
						'{$options['asset_class']}',
						'{$options['asset_loc']}',
						'{$options['asset_dept']}',
						'{$options['assigned_date']}',
						'{$options['installed_date']}',
						'{$options['wxp_date']}',
						'{$options['asset_specs']}',
						'{$options['last_update_by']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}	

	public function UpdateRecord($options=array())
	{	
		$options = $this->_cleanRecord($options);
		
		$sql = "UPDATE data_asset 
				SET ";
		$sql .= !empty($options['model_cat']) ? " model_cat = '{$options['model_cat']}', " : " ";
		$sql .= !empty($options['model_name']) ? " model_name = '{$options['model_name']}', " : " ";
		$sql .= !empty($options['model_manu']) ? " model_manu = '{$options['model_manu']}', " : " ";
		$sql .= !empty($options['serial_num']) ? " serial_num = '{$options['serial_num']}', " : " ";
		$sql .= !empty($options['asset_tag']) ? " asset_tag = '{$options['asset_tag']}', " : " ";
		$sql .= !empty($options['asset_state']) ? " asset_state = '{$options['asset_state']}', " : " ";
		$sql .= !empty($options['assigned_to']) ? " assigned_to = '{$options['assigned_to']}', " : " ";
		$sql .= !empty($options['asset_class']) ? " asset_class = '{$options['asset_class']}', " : " ";
		$sql .= !empty($options['asset_loc']) ? " asset_loc = '{$options['asset_loc']}', " : " ";
		$sql .= !empty($options['asset_dept']) ? " asset_dept = '{$options['asset_dept']}', " : " ";
		$sql .= !empty($options['assigned_date']) ? " assigned_date = '{$options['assigned_date']}', " : " ";
		$sql .= !empty($options['installed_date']) ? " installed_date = '{$options['installed_date']}', " : " ";
		$sql .= !empty($options['decommissioned_date']) ? " decommissioned_date = '{$options['decommissioned_date']}', " : " ";
		$sql .= !empty($options['asset_specs']) ? " asset_specs = '{$options['asset_specs']}', " : " ";
		$sql .= !empty($options['comments']) ? " comments = '{$options['comments']}', " : " ";
		$sql .= !empty($options['wxp_date']) ? " wxp_date = '{$options['wxp_date']}', " : " ";
		$sql .= !empty($options['last_update_by']) ? " last_update_by = '{$options['last_update_by']}', " : " ";
		$sql = rtrim(trim($sql), ",");	
		$sql .= " WHERE id = {$options['id']}";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	public function GetAllRecords($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_asset ";
		$sql .= !empty($search) ? strtoupper($search) : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id DESC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";		
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function DeleteRecord($options=array())
	{
		$sql = "DELETE FROM data_asset
				WHERE id = {$options['id']}";
		$rs = $this->db->query($sql);
		return  $this->db->affected_rows();
	}
			
	public function GetRecById($id=NULL)
	{
		$sql = "SELECT * FROM data_asset ";
		$sql .= !empty($id) ? "WHERE id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}
	
	public function GetFoundRows()
	{
		$sql = "SELECT FOUND_ROWS() AS f_rows";
		$rs = $this->db->query($sql);
		$row = $rs->row_array();	
		return $row['f_rows'];
	}
	
	public function GetTotalRows()
	{
		$sql = "SELECT * FROM data_asset ";
		$rs = $this->db->query($sql);
		return $rs->num_rows();	
	}
	
	public function CheckIfExist($options=array())
	{
		$sql = "SELECT * FROM data_asset 
				WHERE serial_num = '{$options['serial_num']}'
				AND asset_tag = '{$options['asset_tag']}'";
		$rs = $this->db->query($sql);
		return $rs->num_rows();	
	}	
	
}



/* End of file model_asset.php */
/* Location: ./application/model/model_asset.php */