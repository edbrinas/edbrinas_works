<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_im extends CI_Model {

	function __construct()
	{
        parent::__construct();
        $this->db = $this->load->database('main_db', TRUE);
    }

	private function _cleanRecord($options=array())
	{
		foreach($options as $key=>$value)
		{
			$clean_array[$key] = !is_array($value) ? addslashes(trim($value)) : "";
		}
		return $clean_array;
	}

	public function AddRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO data_incident ( tick_num,
											s_num,
											opened_date,
											closed_date,
											rec_stat,
											rec_type,
											client_name,
											created_by,
											prio_level,
											rec_site,
											detail_desc,
											problem_desc,
											rec_attach)
				VALUES ('{$options['tick_num']}',
						'{$options['s_num']}',
						'{$options['opened_date']}',
						'{$options['closed_date']}',
						'{$options['rec_stat']}',
						'{$options['rec_type']}',
						'{$options['client_name']}',
						'{$options['created_by']}',
						'{$options['prio_level']}',
						'{$options['rec_site']}',
						'{$options['detail_desc']}',
						'{$options['problem_desc']}',
						'{$options['rec_attach']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	public function AddRecordDetails($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO data_incident_details ( ir_id,
													updated_by,
													update_date,
													action_desc)
				VALUES ('{$options['ir_id']}',
						'{$options['updated_by']}',
						'{$options['update_date']}',
						'{$options['action_desc']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	public function UpdateRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE data_incident
				SET ";
		$sql .= !empty($options['closed_date']) ? " closed_date = '{$options['closed_date']}', " : " ";
		$sql .= !empty($options['rec_stat']) ? " rec_stat = '{$options['rec_stat']}', " : " ";
		$sql .= !empty($options['rec_type']) ? " rec_type = '{$options['rec_type']}', " : " ";
		$sql .= !empty($options['detail_desc']) ? " detail_desc = '{$options['detail_desc']}', " : " ";
		$sql .= !empty($options['problem_desc']) ? " problem_desc = '{$options['problem_desc']}', " : " ";
		$sql .= !empty($options['rec_attach']) ? " rec_attach = '{$options['rec_attach']}', " : " ";
		$sql = rtrim(trim($sql), ",");
		$sql .= " WHERE id = {$options['id']}";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	public function RemoveFile($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE data_incident
				SET rec_attach = NULL
				WHERE id = {$options['id']}";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}	
	
	public function GetAllRecords($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_incident ";
		$sql .= !empty($search) ? strtoupper($search) : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id DESC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function GetAllRecordDetails($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_incident_details ";
		$sql .= !empty($search) ? strtoupper($search) : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id DESC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function DeleteRecord($options=array())
	{
		$sql = "DELETE FROM data_incident
				WHERE id = {$options['id']}";
		$rs = $this->db->query($sql);
		return  $this->db->affected_rows();
	}

	public function GetRecById($id=NULL)
	{
		$sql = "SELECT * FROM data_incident ";
		$sql .= !empty($id) ? "WHERE id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}

	public function GetRecDetailsById($id=NULL)
	{
		$sql = "SELECT * FROM data_incident_details ";
		$sql .= !empty($id) ? "WHERE ir_id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}

	public function GetFoundRows()
	{
		$sql = "SELECT FOUND_ROWS() AS f_rows";
		$rs = $this->db->query($sql);
		$row = $rs->row_array();
		return $row['f_rows'];
	}

	public function GetTotalRows()
	{
		$sql = "SELECT * FROM data_incident ";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}

	public function CheckIfExist()
	{
		$sql = "SELECT * FROM data_incident
				WHERE DATE_FORMAT(opened_date,'%Y-%m') = DATE_FORMAT(CURRENT_DATE,'%Y-%m')";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}

}



/* End of file model_im.php */
/* Location: ./application/model/model_im.php */