<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_captcha extends CI_Model {
	
	function GenerateCaptcha()
	{
		$captcha_config = array(
			'word_length' => 4,
            'img_path'   => './_plugins/_captcha/',
            'img_url'    => site_url().'_plugins/_captcha/',
            'font_path'  => BASEPATH.'system/fonts/',
            'fonts'      => array('PAPYRUS.TTF'),
            'img_width'  => 145,
            'img_height' => 50,
            'expiration' => 7200
        );
		return $captcha_config;
	}
}



/* End of file model_captcha.php */
/* Location: ./application/model/model_captcha.php */