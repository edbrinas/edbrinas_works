<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_lr extends CI_Model {

	function __construct()
	{
        parent::__construct();
        $this->db = $this->load->database('main_db', TRUE);
    }
	private function _cleanRecord($options=array())
	{
		foreach($options as $key=>$value)
		{
			$clean_array[$key] = addslashes(trim(strtoupper($value)));
		}
		return $clean_array;
	}

	public function AddRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO data_calendar (	c_id,
											leave_date,
											filed_date,
											leave_reason,
											staff_name)
				VALUES ('{$options['c_id']}',
						'{$options['leave_date']}',
						CURRENT_TIMESTAMP,
						'{$options['leave_reason']}',
						'{$options['staff_name']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	public function AddRecordDetails($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO data_calendar_details (	filed_date,
											leave_reason,
											s_date,
											e_date,
											staff_name,
											leave_status)
				VALUES (CURRENT_TIMESTAMP,
						'{$options['leave_reason']}',
						'{$options['s_date']}',
						'{$options['e_date']}',
						'{$options['staff_name']}',
						'PENDING_APPROVAL')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	public function UpdateRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE data_calendar
				SET ";
		$sql .= !empty($options['leave_date']) ? " leave_date = '{$options['leave_date']}', " : " ";
		$sql .= !empty($options['filed_date']) ? " filed_date = '{$options['filed_date']}', " : " ";
		$sql .= !empty($options['staff_name']) ? " staff_name = '{$options['staff_name']}', " : " ";
		$sql .= !empty($options['leave_status']) ? " leave_status = '{$options['leave_status']}', " : " ";
		$sql .= !empty($options['leave_reason']) ? " leave_reason = '{$options['leave_reason']}', " : " ";
		$sql .= !empty($options['approved_by']) ? " approved_by = '{$options['approved_by']}', " : " ";
		$sql .= !empty($options['approved_date']) ? " approved_date = '{$options['approved_date']}', " : " ";
		$sql = rtrim(trim($sql), ",");
		$sql .= !empty($options['c_id']) ? " WHERE c_id = {$options['c_id']} " : " WHERE id = {$options['id']} ";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	public function UpdateDetailsRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE data_calendar_details
				SET ";
		$sql .= !empty($options['leave_status']) ? " leave_status = '{$options['leave_status']}', " : " ";
		$sql .= !empty($options['approved_by']) ? " approved_by = '{$options['approved_by']}', " : " ";
		$sql .= !empty($options['approved_date']) ? " approved_date = '{$options['approved_date']}', " : " ";
		$sql = rtrim(trim($sql), ",");
		$sql .= " WHERE id = {$options['c_id']} ";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	public function GetAllRecords($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_calendar ";
		$sql .= !empty($search) ? $search : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id DESC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function GetAllFiledLeaves($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_calendar_details ";
		$sql .= !empty($search) ? $search : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id ASC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function GetInclDates($options=array())
	{
		$sql = "SELECT DATE_FORMAT(MIN(leave_date),'%d-%b-%Y') AS minDate, DATE_FORMAT(MAX(leave_date),'%d-%b-%Y') AS maxDate
				FROM data_calendar
				WHERE DATE_FORMAT(filed_date,'%Y-%m-%d') = '{$options['filed_date']}'
				AND leave_status = '{$options['leave_status']}'
				AND staff_name = '{$options['staff_name']}' ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}

	public function DeleteRecord($options=array())
	{
		$sql = "DELETE FROM data_calendar
				WHERE c_id = {$options['c_id']}";
		$rs = $this->db->query($sql);
		return  $this->db->affected_rows();
	}
	public function DeleteRecordDetails($options=array())
	{
		$sql = "DELETE FROM data_calendar_details
				WHERE id = {$options['c_id']}";
		$rs = $this->db->query($sql);
		return  $this->db->affected_rows();
	}
	
	public function GetRecById($id=NULL)
	{
		$sql = "SELECT * FROM data_calendar ";
		$sql .= !empty($id) ? "WHERE id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}

	public function GetRecDetailsById($id=NULL)
	{
		$sql = "SELECT * FROM data_calendar_details ";
		$sql .= !empty($id) ? " WHERE id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}

	public function GetFoundRows()
	{
		$sql = "SELECT FOUND_ROWS() AS f_rows";
		$rs = $this->db->query($sql);
		$row = $rs->row_array();
		return $row['f_rows'];
	}

	public function GetTotalRows()
	{
		$sql = "SELECT * FROM data_calendar ";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}

	public function GetTotalRowsDetails()
	{
		$sql = "SELECT * FROM data_calendar_details ";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}	
	
	public function CheckIfExist($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "SELECT * FROM data_calendar
				WHERE leave_date BETWEEN '{$options['s_date']}' AND '{$options['e_date']}'
				AND staff_name = '{$options['staff_name']}'";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}

	public function CheckIfDetailsExist($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "SELECT * FROM data_calendar_details
				WHERE s_date = '{$options['s_date']}'
				AND e_date = '{$options['e_date']}'
				AND staff_name = '{$options['staff_name']}'";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}

	public function GetAllSuppliers()
	{
		$sql = "SELECT supplier_name FROM
				(
					SELECT company_name AS supplier_name FROM data_calendar
					UNION
					SELECT CONCAT(emp_f_name,' ',emp_m_name,' ',emp_l_name) AS supplier_name FROM tbl_employee
				) AS payee
				ORDER BY payee.supplier_name ASC ";
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function CheckIfSupplierExist($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "SELECT supplier_name FROM
				(
					SELECT company_name AS supplier_name FROM data_calendar
					UNION
					SELECT CONCAT(emp_f_name,' ',emp_m_name,' ',emp_l_name) AS supplier_name FROM tbl_employee
				) AS payee
				WHERE payee.supplier_name = '{$options['supplier_name']}'
				ORDER BY payee.supplier_name ASC ";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}
}

/* End of file model_supplier.php */
/* Location: ./application/model/model_supplier.php */