<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_sr extends CI_Model {

	function __construct()
	{
        parent::__construct();
        $this->db = $this->load->database('main_db', TRUE);
    }

	private function _cleanRecord($options=array())
	{
		foreach($options as $key=>$value)
		{
			$clean_array[$key] = !is_array($value) ? addslashes(trim($value)) : "";
		}
		return $clean_array;
	}

	public function AddRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO data_service_req ( ir_id,
											s_num,
											tick_num,
											opened_date,
											rec_stat,
											rec_type,
											created_by,
											prio_level,
											detail_desc,
											problem_desc,
											rec_attach)
				VALUES ('{$options['ir_id']}',
						'{$options['s_num']}',
						'{$options['tick_num']}',
						'{$options['opened_date']}',
						'{$options['rec_stat']}',
						'{$options['rec_type']}',
						'{$options['created_by']}',
						'{$options['prio_level']}',
						'{$options['detail_desc']}',
						'{$options['problem_desc']}',						
						'{$options['rec_attach']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	public function AddRecordDetails($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO data_service_req_details ( sr_id,
													updated_by,
													update_date,
													action_desc)
				VALUES ('{$options['sr_id']}',
						'{$options['updated_by']}',
						'{$options['update_date']}',
						'{$options['action_desc']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}	
	
	public function UpdateRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE data_service_req
				SET ";
		$sql .= !empty($options['assigned_to']) ? " assigned_to = LOWER('{$options['assigned_to']}'), " : " ";
		$sql .= !empty($options['closed_date']) ? " closed_date = '{$options['closed_date']}', " : " ";
		$sql .= !empty($options['rec_stat']) ? " rec_stat = '{$options['rec_stat']}', " : " ";
		$sql .= !empty($options['rec_type']) ? " rec_type = '{$options['rec_type']}', " : " ";
		$sql .= !empty($options['prio_level']) ? " prio_level = '{$options['prio_level']}', " : " ";
		$sql .= !empty($options['detail_desc']) ? " detail_desc = '{$options['detail_desc']}', " : " ";
		$sql .= !empty($options['problem_desc']) ? " problem_desc = '{$options['problem_desc']}', " : " ";
		$sql .= !empty($options['approved_by']) ? " approved_by = '{$options['approved_by']}', " : " ";
		$sql .= !empty($options['approved_date']) ? " approved_date = '{$options['approved_date']}', " : " ";
		$sql .= !empty($options['rec_attach']) ? " rec_attach = '{$options['rec_attach']}', " : " ";	
		$sql = rtrim(trim($sql), ",");
		$sql .= !empty($options['id']) ? " WHERE id = '{$options['id']}' " : " WHERE ir_id = '{$options['ir_id']}' " ;
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	public function GetAllRecords($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_service_req ";
		$sql .= !empty($search) ? strtoupper($search) : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id DESC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function GetAllRecordDetails($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_service_req_details ";
		$sql .= !empty($search) ? strtoupper($search) : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id DESC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function DeleteRecord($options=array())
	{
		$sql = "DELETE FROM data_service_req
				WHERE id = {$options['id']}";
		$rs = $this->db->query($sql);
		return  $this->db->affected_rows();
	}

	public function GetRecById($id=NULL)
	{
		$sql = "SELECT * FROM data_service_req ";
		$sql .= !empty($id) ? "WHERE id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}

	public function GetRecDetailsById($id=NULL)
	{
		$sql = "SELECT * FROM data_service_req_details ";
		$sql .= !empty($id) ? "WHERE ir_id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}

	public function GetFoundRows()
	{
		$sql = "SELECT FOUND_ROWS() AS f_rows";
		$rs = $this->db->query($sql);
		$row = $rs->row_array();
		return $row['f_rows'];
	}

	public function GetTotalRows()
	{
		$sql = "SELECT * FROM data_service_req ";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}

	public function CheckIfExist()
	{
		$sql = "SELECT * FROM data_service_req
				WHERE DATE_FORMAT(opened_date,'%Y-%m') = DATE_FORMAT(CURRENT_DATE,'%Y-%m')";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}

	public function RemoveFile($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE data_service_req
				SET rec_attach = NULL
				WHERE id = {$options['id']}";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}
}



/* End of file model_sr.php */
/* Location: ./application/model/model_sr.php */