<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_mon extends CI_Model {

	function __construct() 
	{
        parent::__construct();
        $this->db = $this->load->database('main_db', TRUE);
    }

	private function _cleanRecord($options=array())
	{
		foreach($options as $key=>$value)
		{
			$clean_array[$key] = !is_array($value) ? addslashes(trim(strip_tags($value))) : "";
		}
		return $clean_array;
	}
		
	public function AddRecord($options=array())
	{			
		$options = $this->_cleanRecord($options);
		
		$sql = "INSERT INTO data_mon (	mon_date,
										mon_site,
										created_by,
										mon_rem,
										rec_attach) 
				VALUES ('{$options['mon_date']}',
						'{$options['mon_site']}',
						'{$options['created_by']}',
						'{$options['mon_rem']}',
						'{$options['rec_attach']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}	

	public function UpdateRecord($options=array())
	{	
		$options = $this->_cleanRecord($options);
		
		$sql = "UPDATE data_mon 
				SET ";
		$sql .= !empty($options['mon_site']) ? " mon_site = '{$options['mon_site']}', " : " ";
		$sql .= !empty($options['mon_rem']) ? " mon_rem = '{$options['mon_rem']}', " : " ";
		$sql .= !empty($options['rec_attach']) ? " rec_attach = '{$options['rec_attach']}', " : " ";
		$sql = rtrim(trim($sql), ",");	
		$sql .= " WHERE id = {$options['id']}";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	public function GetAllRecords($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_mon ";
		$sql .= !empty($search) ? strtoupper($search) : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id DESC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";		
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function DeleteRecord($options=array())
	{
		$sql = "DELETE FROM data_mon
				WHERE id = {$options['id']}";
		$rs = $this->db->query($sql);
		return  $this->db->affected_rows();
	}
			
	public function GetRecById($id=NULL)
	{
		$sql = "SELECT * FROM data_mon ";
		$sql .= !empty($id) ? "WHERE id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}
	
	public function GetFoundRows()
	{
		$sql = "SELECT FOUND_ROWS() AS f_rows";
		$rs = $this->db->query($sql);
		$row = $rs->row_array();	
		return $row['f_rows'];
	}
	
	public function GetTotalRows()
	{
		$sql = "SELECT * FROM data_mon ";
		$rs = $this->db->query($sql);
		return $rs->num_rows();	
	}
	
	public function CheckIfExist($options=array())
	{
		$sql = "SELECT * FROM data_mon 
				WHERE mon_date = '{$options['mon_date']}'";
		$rs = $this->db->query($sql);
		return $rs->num_rows();	
	}	

	public function RemoveFile($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE data_mon
				SET rec_attach = NULL
				WHERE id = {$options['id']}";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}	
}

/* End of file model_mon.php */
/* Location: ./application/model/model_mon.php */