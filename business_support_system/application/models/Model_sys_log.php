<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_sys_log extends CI_Model {

	function __construct() 
	{
        parent::__construct();
        $this->db = $this->load->database('cr_db', TRUE);
    }
	
	public function AddTLog($options=array())
	{				
		$sql = "INSERT INTO data_trx_logs (username,activity,ip_address) 
				VALUES (UPPER('{$options['username']}'),
						UPPER('{$options['activity']}'),
						'{$options['ip_address']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	public function GetAllRecords($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_trx_logs ";
		$sql .= !empty($search) ? strtoupper($search) : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id DESC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";		
		$rs = $this->db->query($sql);
		#echo $this->db->last_query();
		return $rs->result_array();
	}
	
	public function GetFoundRows()
	{
		$sql = "SELECT FOUND_ROWS() AS f_rows";
		$rs = $this->db->query($sql);
		$row = $rs->row_array();	
		return $row['f_rows'];
	}

	public function GetTotalRows()
	{
		$sql = "SELECT * FROM data_trx_logs";
		$rs = $this->db->query($sql);
		return $rs->num_rows();	
	}	

	public function GetRecById($id=NULL)
	{
		$sql = "SELECT * FROM data_trx_logs ";
		$sql .= !empty($id) ? "WHERE id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}		
}



/* End of file model_sys_log.php */
/* Location: ./application/model/model_sys_log.php */