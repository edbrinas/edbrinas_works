<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_users extends CI_Model {

	function __construct()
	{
        parent::__construct();
        $this->db = $this->load->database('cr_db', TRUE);
    }

	private function _cleanRecord($options=array())
	{
		foreach($options as $key=>$value)
		{
			$clean_array[$key] = !is_array($value) ? addslashes(trim(strip_tags($value))) : "";
		}
		return $clean_array;
	}


	public function AddRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO data_users (username,password,f_name,m_name,l_name,e_add,e_dept,status,enable_notification,emp_id)
				VALUES (LOWER('{$options['username']}'),
						md5('{$options['password']}'),
						UPPER('{$options['emp_f_name']}'),
						UPPER('{$options['emp_m_name']}'),
						UPPER('{$options['emp_l_name']}'),
						UPPER('{$options['email_address']}'),
						UPPER('{$options['e_dept']}'),
						'A',
						'E',
						UPPER('{$options['emp_id']}'))";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	public function UpdateRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE data_users
				SET ";
		$sql .= !empty($options['emp_f_name']) ? " f_name = UPPER('{$options['emp_f_name']}'), " : " ";
		$sql .= !empty($options['emp_m_name']) ? " m_name = UPPER('{$options['emp_m_name']}'), " : " ";
		$sql .= !empty($options['emp_l_name']) ? " l_name = UPPER('{$options['emp_l_name']}'), " : " ";
		$sql .= !empty($options['r_status']) ? " status = UPPER('{$options['r_status']}'), " : " ";
		$sql .= !empty($options['email_address']) ? " e_add = UPPER('{$options['email_address']}'), " : " ";
		$sql .= !empty($options['e_dept']) ? " e_dept = UPPER('{$options['e_dept']}'), " : " ";
		$sql .= !empty($options['username']) ? " username = LOWER('{$options['username']}'), " : " ";
		$sql .= !empty($options['password']) ? " password = md5('{$options['password']}'), " : " ";
		$sql .= !empty($options['emp_id']) ? " emp_id = UPPER('{$options['emp_id']}'), " : " ";
		$sql .= !empty($options['enable_notification']) ? " enable_notification = UPPER('{$options['enable_notification']}'), " : " ";
		$sql = rtrim(trim($sql), ",");
		$sql .= !empty($options['emp_id']) ? "  WHERE emp_id = {$options['emp_id']}  " : " WHERE id = {$options['id']} ";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	public function UpdatePRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE data_users SET ";
		$sql .= !empty($options['password']) ? " password = md5('{$options['password']}'), " : " ";
		$sql .= !empty($options['enable_notification']) ? " enable_notification = UPPER('{$options['enable_notification']}'), " : " ";
		$sql .= !empty($options['emp_f_name']) ? " f_name = UPPER('{$options['emp_f_name']}'), " : " ";
		$sql .= !empty($options['emp_m_name']) ? " m_name = UPPER('{$options['emp_m_name']}'), " : " ";
		$sql .= !empty($options['emp_l_name']) ? " l_name = UPPER('{$options['emp_l_name']}'), " : " ";
		$sql .= !empty($options['emp_id']) ? " emp_id = UPPER('{$options['emp_id']}'), " : " ";
		$sql = rtrim(trim($sql), ",");
		$sql .= " WHERE id = {$options['id']} ";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	public function GetAllRecord($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_users ";
		$sql .= !empty($search) ? strtoupper($search) : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id DESC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function GetRecordInfo($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "SELECT * FROM data_users
				WHERE username = '{$options['username']}'
				AND	password = md5('{$options['password']}')
				LIMIT 1";
		$rs = $this->db->query($sql);
		return $rs->row(0);
	}

	public function GetRecordById($id=NULL)
	{
		$sql = "SELECT * FROM data_users ";
		$sql .= !empty($id) ? "WHERE id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}

	public function GetRecordByEmpId($id=NULL)
	{
		$sql = "SELECT * FROM data_users ";
		$sql .= !empty($id) ? "WHERE emp_id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}

	public function GetRecordByUname($uname=NULL)
	{
		$sql = "SELECT * FROM data_users ";
		$sql .= !empty($uname) ? "WHERE UPPER(username) = UPPER('{$uname}') " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}

	public function GetFoundRows()
	{
		$sql = "SELECT FOUND_ROWS() AS f_rows";
		$rs = $this->db->query($sql);
		$row = $rs->row_array();
		return $row['f_rows'];
	}

	public function GetTotalRows()
	{
		$sql = "SELECT * FROM data_users ";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}

	public function CheckIfExist($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "SELECT * FROM data_users WHERE username = UPPER('{$options['username']}')";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}

	public function CheckIfEmailExist($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "SELECT * FROM data_users WHERE e_add = UPPER('{$options['e_add']}')";
		$rs = $this->db->query($sql);
		return $rs->num_rows();
	}

	public function GetAccessType($u_id)
	{
		$sql = "SELECT access_id FROM data_user_type WHERE user_id = {$u_id}";
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function DelRecordType($options=array())
	{
		$sql = "DELETE FROM data_user_type
				WHERE user_id = {$options['id']}";
		$rs = $this->db->query($sql);
		return  $this->db->affected_rows();
	}

	public function AddRecordType($options=array())
	{
		$sql = "INSERT INTO data_user_type (user_id,access_id)
				VALUES ('{$options['u_id']}',
						'{$options['a_id']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	public function DeleteRecord($options=array())
	{
		$sql = "DELETE FROM data_user_type
				WHERE user_id = {$options['id']}";
		$this->db->query($sql);

		$sql = "DELETE FROM data_user_access
				WHERE user_id = {$options['id']}";
		$rs = $this->db->query($sql);

		$sql = "DELETE FROM data_users
				WHERE emp_id = {$options['emp_id']}";
		$rs = $this->db->query($sql);
		return  $this->db->affected_rows();
	}

}



/* End of file model_users.php */
/* Location: ./application/model/model_users.php */