<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_check_login extends CI_Model {


     /*
      *  CheckSession method checks if the user is currently logged in.
      *
      * Option: Value
      * -----------------
      * None
      *
      * return bool
      *
      */
     function CheckSession()
     {
	 	$is_logged = $this->session->userdata('sessionlogged');
		return ($is_logged) ? TRUE : FALSE;
     }
}

/* End of file model_check_login.php */
/* Location: ./application/model/model_check_login.php */