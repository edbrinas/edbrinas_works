<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_inventory extends CI_Model {

	function __construct() 
	{
        parent::__construct();
        $this->db = $this->load->database('main_db', TRUE);
    }

	private function _cleanRecord($options=array())
	{
		foreach($options as $key=>$value)
		{
			$clean_array[$key] = !is_array($value) ? addslashes(trim(strip_tags($value))) : "";
		}
		return $clean_array;
	}
		
	public function AddRecord($options=array())
	{			
		$options = $this->_cleanRecord($options);
		
		$sql = "INSERT INTO data_inventory (e_ip_add,
											e_name,
											e_desc,
											e_pur_date,
											e_exp_date,
											e_sup_name,
											e_loc,
											e_type,
											e_manu,
											e_model,
											e_serial_num,
											e_proc,
											e_mem,
											e_hdd,
											e_os) 
				VALUES ('{$options['e_ip_add']}',
						'{$options['e_name']}',
						'{$options['e_desc']}',
						'{$options['e_pur_date']}',
						'{$options['e_exp_date']}',
						'{$options['e_sup_name']}',
						'{$options['e_loc']}',
						'{$options['e_type']}',
						'{$options['e_manu']}',
						'{$options['e_model']}',
						'{$options['e_serial_num']}',
						'{$options['e_proc']}',
						'{$options['e_mem']}',
						'{$options['e_hdd']}',
						'{$options['e_os']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}	

	public function UpdateRecord($options=array())
	{	
		$options = $this->_cleanRecord($options);
		
		$sql = "UPDATE data_inventory 
				SET ";
		$sql .= !empty($options['e_ip_add']) ? " e_ip_add = '{$options['e_ip_add']}', " : " ";
		$sql .= !empty($options['e_name']) ? " e_name = '{$options['e_name']}', " : " ";
		$sql .= !empty($options['e_desc']) ? " e_desc = '{$options['e_desc']}', " : " ";
		$sql .= !empty($options['e_pur_date']) ? " e_pur_date = '{$options['e_pur_date']}', " : " ";
		$sql .= !empty($options['e_exp_date']) ? " e_exp_date = '{$options['e_exp_date']}', " : " ";
		$sql .= !empty($options['e_sup_name']) ? " e_sup_name = '{$options['e_sup_name']}', " : " ";
		$sql .= !empty($options['e_loc']) ? " e_loc = '{$options['e_loc']}', " : " ";
		$sql .= !empty($options['e_type']) ? " e_type = '{$options['e_type']}', " : " ";
		$sql .= !empty($options['e_manu']) ? " e_manu = '{$options['e_manu']}', " : " ";
		$sql .= !empty($options['e_model']) ? " e_model = '{$options['e_model']}', " : " ";
		$sql .= !empty($options['e_serial_num']) ? " e_serial_num = '{$options['e_serial_num']}', " : " ";
		$sql .= !empty($options['e_proc']) ? " e_proc = '{$options['e_proc']}', " : " ";
		$sql .= !empty($options['e_mem']) ? " e_mem = '{$options['e_mem']}', " : " ";
		$sql .= !empty($options['e_hdd']) ? " e_hdd = '{$options['e_hdd']}', " : " ";
		$sql .= !empty($options['e_os']) ? " e_os = '{$options['e_os']}', " : " ";
		$sql = rtrim(trim($sql), ",");	
		$sql .= " WHERE id = {$options['id']}";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	public function GetAllRecords($limit=0,$offset=0,$search=NULL,$orderby=NULL)
	{
		$sql = "SELECT * FROM data_inventory ";
		$sql .= !empty($search) ? strtoupper($search) : " ";
		$sql .= !empty($orderby) ? strtoupper($orderby)." " : " ORDER BY id DESC ";
        $sql .= $limit > 0 ? "limit {$limit} " : " ";
        $sql .= $offset > 0 ? "offset {$offset} " : " ";		
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}

	public function DeleteRecord($options=array())
	{
		$sql = "DELETE FROM data_inventory
				WHERE id = {$options['id']}";
		$rs = $this->db->query($sql);
		return  $this->db->affected_rows();
	}
			
	public function GetRecById($id=NULL)
	{
		$sql = "SELECT * FROM data_inventory ";
		$sql .= !empty($id) ? "WHERE id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}
	
	public function GetFoundRows()
	{
		$sql = "SELECT FOUND_ROWS() AS f_rows";
		$rs = $this->db->query($sql);
		$row = $rs->row_array();	
		return $row['f_rows'];
	}
	
	public function GetTotalRows()
	{
		$sql = "SELECT * FROM data_inventory ";
		$rs = $this->db->query($sql);
		return $rs->num_rows();	
	}
	
	public function CheckIfExist($options=array())
	{
		$sql = "SELECT * FROM data_inventory 
				WHERE e_serial_num = '{$options['e_serial_num']}'";
		$rs = $this->db->query($sql);
		return $rs->num_rows();	
	}	
	
}



/* End of file model_inventory.php */
/* Location: ./application/model/model_inventory.php */