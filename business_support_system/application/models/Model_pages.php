<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pages extends CI_Model {

	function __construct() 
	{
        parent::__construct();
        $this->db = $this->load->database('cr_db', TRUE);
    }
	
	public function GetAllPages($p_id)
	{
		$sql = "SELECT * FROM data_pages WHERE p_id=$p_id ORDER BY o_num ASC";	
		$rs = $this->db->query($sql);
		return $rs->result_array();
	}		

	public function GetSinglePage($parent_id,$p_id)
	{
		$sql = "SELECT * FROM data_pages 
				WHERE p_id=$parent_id AND 
				p_id<>$p_id AND id<>$p_id ORDER BY o_num ASC";	
		$rs = $this->db->query($sql);

		return $rs->result_array();
	}
		
	public function AddPage($options=array())
	{
		$a_onum = $this->GetLastOrderNumber($options);
		$o_num = $a_onum->ordernum+1;
		
		$sql = "INSERT INTO data_pages (p_id,p_name,p_link,o_num) 
				VALUES ('{$options['p_id']}',
						UPPER('{$options['p_name']}'),
						'{$options['p_link']}',
						'{$o_num}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}	

	public function UpdatePage($options=array())
	{		
		$sql = "UPDATE data_pages 
				SET p_id = '{$options['p_id']}',  ";
		$sql .= !empty($options['p_name']) ? " p_name = UPPER('{$options['p_name']}'), " : " ";
		$sql .= !empty($options['p_link']) ? " p_link = '{$options['p_link']}', " : " ";
		$sql .= !empty($options['o_num']) ? " o_num = '{$options['o_num']}', " : " ";
		$sql = rtrim(trim($sql), ",");	
		$sql .= " WHERE id = {$options['id']}";			
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}
	
	public function CheckAccess($options=array())
	{
		$sql = "SELECT * FROM data_user_access 
				WHERE page_id = '{$options['page_id']}' 
				AND user_id = '{$options['user_id']}'";
		$rs = $this->db->query($sql);
		return $rs->num_rows();	
	}

	public function GetPageById($id=NULL)
	{
		$sql = "SELECT * FROM data_pages ";
		$sql .= !empty($id) ? "WHERE id = {$id} " : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query($sql);
		return $rs->row();
	}	
	
	public function GetLastOrderNumber($options=array())
	{
		$sql = "SELECT MAX(o_num) AS ordernum FROM data_pages WHERE p_id={$options['p_id']}";
		$rs = $this->db->query($sql);
		return $rs->row();		
	}

	public function DelUserAccess($options=array())
	{
		$sql = "DELETE FROM data_user_access
				WHERE user_id = {$options['id']}";
		$rs = $this->db->query($sql);
		return  $this->db->affected_rows();
	}	

	public function AddPageAccess($options=array())
	{	
		$sql = "INSERT INTO data_user_access (user_id,page_id) 
				VALUES ('{$options['u_id']}',
						'{$options['p_id']}')";
		$this->db->query($sql);
		return $this->db->insert_id();
	}	

	public function GetAllowedPages($u_id)
	{
		$sql = "SELECT 	b.id AS id,
						b.p_id AS p_id,
						b.p_name AS p_name,
						b.p_link As p_link
				FROM data_user_access a, data_pages b
				WHERE a.page_id = b.p_id
				AND a.user_id=$u_id 
				ORDER BY b.p_id ASC";	
		$rs = $this->db->query($sql);

		return $rs->result_array();
	}	

	public function CheckUserType($u_id,$a_id)
	{
		$sql = "SELECT * FROM data_user_type 
				WHERE access_id = {$a_id}
				AND user_id = {$u_id}";
		$rs = $this->db->query($sql);
		return $rs->num_rows();	
	}
	
	public function GetPidByController($c_name)
	{
		$sql = "SELECT * FROM data_pages 
				WHERE p_link = '{$c_name}'";
		$rs = $this->db->query($sql);
		return $rs->row();
	}		
}



/* End of file model_pages.php */
/* Location: ./application/model/model_pages.php */