<?php include('includes/_header.php'); ?>
<div id="main">	
    <h1>IMPORTANT NOTICE</h1>
    <p><img src="<?php echo base_url(); ?>_plugins/_css/main_images/company_logo.jpg" width="250" height="100" alt="Saudi Electricity Company" class="float-left" border="0" />
    		<p>This site is to be used by <b><?php echo COMPANY_NAME; ?></b> employees only. Unauthorized access or use is strictly prohibited and constitutes a crime punishable by law.</p>

			<p>It is a criminal offense to:<br />
                1. Obtain access to data without authority<br />
                2. Corrupt, alter, steal, or destroy data<br />
                3. Interfere in computer system or server;<br />
                4. Introduce computer virus
    		</p>
    </p>

</div>
<?php include('includes/_footer.php'); ?>
