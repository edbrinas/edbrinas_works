<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>
        <h1><?php echo $module_name; ?> [LIST]</h1>
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="<?php echo $table_id; ?>" width="100%">
        <thead>
            <tr>
            	<th>REFERENCE NUMBER</th>
                <th>CREATED BY</th>
				<th>DATE OPENED</th>
                <th>SUBJECT</th>
                <th>STATUS</th>
                <th>PRIORITY</th>
                <th>TIME LAPSED</th>
                <th>OPTIONS</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
 </div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>