<script type="text/javascript">

$(document).ready(function() 
{
	//$("#items_place_holder").empty();
	$('#processing').hide();
    var msg_req = "<font color='#FF0000'>Required field.</font>";
	var msg_email = "<font color='#FF0000'>Please enter a valid email address.</font>";
	var msg_digits = "<font color='#FF0000'>Numbers are only allowed.</font>";
	var sys_response;

	$("#sys_frm").validate({
		rules: {
			detail_desc: { required: true },
			rec_site: { required: true },
			prio_level: { required: true },
			sr_type: { required: true },
			sub_sr_type: { required: true },
			problem_desc: { required: true },
			action_desc: { required: true },
			create_sr: { required: true },
			client_name: { required: true },
			rec_stat: { required: true }
		},
		messages: {
			detail_desc: { required: msg_req },
			rec_site: { required: msg_req },
			prio_level: { required: msg_req },
			sr_type: { required: msg_req },
			sub_sr_type: { required: msg_req },
			problem_desc: { required: msg_req },
			action_desc: { required: msg_req },
			create_sr: { required: msg_req },
			client_name: { required: msg_req },
			rec_stat: { required: msg_req }
		}
	});

    $("#sys-frm-message").dialog({
        autoOpen: false,
        modal: true,
        title: "System Message",
        buttons: {
            Ok: function () {
                $(this).dialog('close');
				$(window).attr("location","<?php echo base_url().$controller_main; ?>");
            }
        }
    });	
	
	$("#but_add").click(function(){
		$(this).attr("disabled", true);
		for (instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}		
		if($("#sys_frm").valid()) 
		{
			$.ajax({type: 'POST',
				   url: "<?php echo base_url().$controller_main.'CreateRecord'; ?>",
				   data: new FormData($('#sys_frm')[0]),
				   async: false,
				   contentType: false,
				   processData: false,
				   cache: false,
				   success:function(response)
				   {
						$(".sys-frm-inner-message").empty();
						$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-lightbulb' style='float:left; margin:0 7px 50px 0;' />" + response + "</span></p>");
						$("#sys-frm-message").dialog("open");
				   },
				   error:function (xhr, ajaxOptions, thrownError)
						{
							$(".sys-frm-inner-message").empty();
							$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />" + thrownError + "</span></p>");
							$("#sys-frm-message").dialog("open");
						}
				 })  
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});

	$("#but_edit").click(function (e) {
		e.preventDefault();
		if($("#sys_frm").valid()) 
		{
			$(".sys-frm-edit-msg-inner-msg").empty();
			$(".sys-frm-edit-msg-inner-msg").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />Are you sure you want to edit this record?<br /><br />Click Yes to continue, otherwise click No.</span></p>");
			$("#sys-frm-edit-msg").dialog("open");
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});	

	$('#sys-frm-edit-msg').dialog({ 
		title: "System Message",
		autoOpen: false,
		modal: true,
		width: 400,
		resizable: false,
		closeOnEscape: true,
		draggable: false,
		buttons: {
			'Yes': function(){
				$(this).dialog('close');
				for (instance in CKEDITOR.instances) {
					CKEDITOR.instances[instance].updateElement();
				}				
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'ModifyRecord'; ?>",
					   data: new FormData($('#sys_frm')[0]),
					   async: false,
					   contentType: false,
					   processData: false,
					   cache: false,
					   success:function(response)
					   {
							$(".sys-frm-inner-message").empty();
							$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-lightbulb' style='float:left; margin:0 7px 50px 0;' />" + response + "</span></p>");
							$("#sys-frm-message").dialog("open");
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								$(".sys-frm-inner-message").empty();
								$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />" + thrownError + "</span></p>");
								$("#sys-frm-message").dialog("open");
							}
					 })				
			},
			'No': function(){
				$(this).dialog('close');
			}
		}
	});
	
	$("#but_del_file").click(function (e) {
		e.preventDefault();
		$(".sys-frm-del-msg-inner-msg").empty();
		$(".sys-frm-del-msg-inner-msg").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />Are you sure you want to delete this file?<br /><br />Click Yes to continue, otherwise click No.</span></p>");
		$("#sys-frm-del-msg").dialog("open");
	});	

	$('#sys-frm-del-msg').dialog({ 
		title: "System Message",
		autoOpen: false,
		modal: true,
		width: 400,
		resizable: false,
		closeOnEscape: true,
		draggable: false,
		buttons: {
			'Yes': function(){
				$(this).dialog('close');
				$("#items_place_holder").empty();			
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'RemoveFile'; ?>",
					   data: $("#sys_frm").serialize(),
					   success:function(response)
					   {
							$("#items_place_holder").append(response);
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								$(".sys-frm-inner-message").empty();
								$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />" + thrownError + "</span></p>");
								$("#sys-frm-message").dialog("open");
							}
					 })				
			},
			'No': function(){
				$(this).dialog('close');
			}
		}
	});	

	$("#but_can").click(function(){
		window.location = '<?php echo base_url().$controller_main; ?>';
	});		
	
	$('#mList').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllRecords'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 7, "desc" ]]	
	});

	$('#faList').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllForApproval'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 7, "desc" ]]	
	});	

	$('#mySrList').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllMyList'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 7, "desc" ]]	
	});
	
	$('#sr_type').change(function(){
		$("#items_place_holder").empty();
		$('#processing').show();
		$.ajax({type: 'POST',
			   url: "<?php echo base_url().$controller_main.'GetReqFields'; ?>",
			   data: $("#sys_frm").serialize(),
			   success:function(response)
			   {
					$('#processing').hide();
					$("#items_place_holder").append(response);
			   },
			   error:function (xhr, ajaxOptions, thrownError)
					{
						console.log(thrownError);
						alert(thrownError);
					}
			 }) 
	});	
	
	$("#client_name").tokenInput("<?php echo base_url().$controller_main.'GetUsers'; ?>", {
		theme: "facebook"
		<?php
		if($a_pre_pop) {
			echo ", prePopulate: ".$a_pre_pop;
		}
		?>
	});

	$("#assigned_to").tokenInput("<?php echo base_url().$controller_main.'GetUsers'; ?>", {
		theme: "facebook"
		<?php
		if($a_pre_pop) {
			echo ", prePopulate: ".$a_pre_pop;
		}
		?>
	});	
});
</script>