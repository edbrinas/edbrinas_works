<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>
		<h1><?php echo $module_name; ?> [ADD RECORD]</h1>
        <form id="sys_frm" enctype="multipart/form-data" />
            <p>
            <label>SUBJECT *</label>
            <input type="text" id="detail_desc" name="detail_desc" size="75"/>
            <label>REQUEST TYPE *</label>
            <select name="sr_type" id="sr_type" />
			<option value="" />=== PLEASE SELECT ===</option>
			<?php
			foreach ($a_sr_type as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>
			<p>
			<span id="processing" style="display: none;">
				<img alt="Please Wait" src="<?php echo base_url() ;?>_plugins/_css/main_images/processing.gif"/>
			</span>
			<div id="items_place_holder"></div>
			</p>
			<p>
            <label>REQUEST DETAILS  *</label>
			<textarea name="problem_desc" id="problem_desc"></textarea>
            <label>OTHER REMARKS</label>
			<textarea name="action_desc" id="action_desc"></textarea>
			<label>ATTACHMENT EVIDENCE</label>
			<input type="file" name="file_to_upload" id="file_to_upload">
            <script>
                CKEDITOR.replace( 'problem_desc' );
				CKEDITOR.replace( 'action_desc' );
            </script>
			<br /><br />
			<b><i>* = Required Field</i></b>			
            <br /><br />
            <input type="button" name="but_add" id="but_add" value="SAVE"/>
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>