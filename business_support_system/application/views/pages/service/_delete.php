<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [DELETE RECORD]</h1>
        <form id="sys_frm">
            <p>
              <table cellpadding="1" cellspacing="1" border="1" class="display" width="100%">
                 <thead>
                    <tr>
                        <th width="20%">FIELDS</th>
                        <th>DETAILS</th>
                    </tr>
                </thead>
                <tbody>           
                    <tr>
                        <td>BANK NAME</td>
                        <td><?php echo $a_records->bank_name; ?></td>
                    </tr> 
                    <tr>
                        <td>BANK SUBSIDIARY LEDGER CODE</td>
                        <td><?php echo $a_records->acct_sl_code; ?></td>
                    </tr>                          
                    <tr>
                        <td>ACCOUNT NAME</td>
                        <td><?php echo $a_records->acct_name; ?></td>
                    </tr>  
                    <tr>
                        <td>ACCOUNT NUMBER</td>
                        <td><?php echo $a_records->acct_number; ?></td>
                    </tr>
                    <tr>
                        <td>ACCOUNT TYPE</td>
                        <td valign="top"><?php echo $a_records->acct_type; ?></td>
                    </tr>                 
                    <tr>
                        <td>CURRENCY</td>
                        <td><?php echo $a_records->acct_currency; ?></td>
                    </tr>  
                    <tr>
                        <td>ADDRESS</td>
                        <td valign="top"><?php echo $a_records->address; ?></td>
                    </tr>   
                    <tr>
                        <td>POSTAL CODE</td>
                        <td valign="top"><?php echo $a_records->postal_code; ?></td>
                    </tr>                
                    <tr>
                        <td>COUNTRY</td>
                        <td valign="top"><?php echo $a_records->country; ?></td>
                    </tr>
                    <tr>
                        <td>CONTACT PERSON</td>
                        <td valign="top"><?php echo $a_records->contact_person; ?></td>
                    </tr>
                    <tr>
                        <td>CONTACT PERSON</td>
                        <td valign="top"><?php echo $a_records->contact_number; ?></td>
                    </tr>
                    <tr>
                        <td>EMAIL ADDRESS</td>
                        <td valign="top"><?php echo $a_records->email_address; ?></td>
                    </tr>                
                    </tbody>
                </table>
            <br /><br />
            <input type='hidden' name='id' id='id' value="<?php echo $a_records->id; ?>" readonly/>                                                              
            <input type="button" name="but_delete" id="but_delete" value="DELETE RECORD"/> 
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>