<script type="text/javascript">

$(document).ready(function() 
{
    var msg_req = "<font color='#FF0000'>Required field.</font>";
    var msg_minlen = jQuery.format("<font color='#FF0000'>At least {0} characters are necessary.</font>");
	var msg_chkreq = jQuery.format("<font color='#FF0000'>Check no more than 1 box.</font>");
    var msg_maxlen = jQuery.format("<font color='#FF0000'>Maximum {0} characters are only allowed.</font>");
    var msg_digits = "<font color='#FF0000'>Please enter a valid number.</font>";
	var sys_response;

	$("#sys_frm").validate({
		rules: {
			p_name: { required: true },
			p_link: { required: true },
			o_num: { required: true, number: true }		   
		},
		messages: {
			p_name: { required: msg_req },
			p_link: { required: msg_req },			
			o_num: { required: msg_req, number: msg_digits }
		}
	});
	
	$("#but_add").click(function(){
		$(this).attr("disabled", true);
		if($("#sys_frm").valid()) 
		{
			$.ajax({type: 'POST',
				   url: "<?php echo base_url().$controller_main.'CreateRecord'; ?>",
				   data: $("#sys_frm").serialize(),
				   success:function(response)
				   {
				   		alert(response);
						window.location = '<?php echo base_url().$controller_main; ?>';
				   },
				   error:function (xhr, ajaxOptions, thrownError)
						{
							console.log(thrownError);
							alert(thrownError);
						}
				 })   
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});

	$("#but_edit").click(function(){
		if(confirm('Are you sure you want to edit this record?\n\nClick OK to continue. Otherwise click Cancel.\n')){
			$(this).attr("disabled", true);
			if($("#sys_frm").valid()) 
			{
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'ModifyRecord'; ?>",
					   data: $("#sys_frm").serialize(),
					   success:function(response)
					   {
							alert(response);
							window.location = '<?php echo base_url().$controller_main; ?>';
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								console.log(thrownError);
								alert(thrownError);
							}
					 }) 
			}
			else
			{
				$(this).attr("disabled", false);
			}
		}
		else
		{
			window.location = '<?php echo base_url().$controller_main; ?>';
		}
	});	

	$("#but_can").click(function(){
		window.location = '<?php echo base_url().$controller_main; ?>';
	});		
});

</script>