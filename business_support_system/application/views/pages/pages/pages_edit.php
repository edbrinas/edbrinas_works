<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1>MODIFY PAGE</h1>
        
        <form id="sys_frm">
            <p>
            <label>PAGE NAME</label>
            <input type="text" id="p_name" name="p_name" value="<?php echo $a_pages->p_name; ?>" size="50"/> 
            <label>CONTROLLER LINK</label>
            <input type="text" id="p_link" name="p_link" value="<?php echo $a_pages->p_link; ?>" size="50"/>                        
            <label>PARENT PAGE</label>
            <select name="p_id" id="p_id">
            	<option value="0">-- SET AS PARENT PAGE --</option>
                <?php echo $p_links; ?>
            </select>
            <label>LINK ORDER</label>
            <input type="text" id="o_num" name="o_num" value="<?php echo $a_pages->o_num; ?>" size="2"/>   
             
            <br /><br />
            <input type="hidden" id="id" name="id" value="<?php echo $a_pages->id; ?>" readonly/>
            <input type="button" value="SAVE" id="but_edit" name="but_edit" /> 
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>