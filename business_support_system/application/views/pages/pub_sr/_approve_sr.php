<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>
		<h1><?php echo $module_name; ?> [EDIT RECORD]</h1>
        <form id="sys_frm" enctype="multipart/form-data" />
            <p>
			<label>REFERENCE NUMBER</label>
			<?php echo $a_records->s_num; ?>
            <label>SUBJECT</label>
			<?php echo $a_records->detail_desc; ?>
            <label>CREATED BY</label>
			<?php echo $a_records->created_by; ?>
            <label>ASSIGNED TO</label>
			<input type="text" id="assigned_to" name="assigned_to" size="75" value="<?php echo $a_records->assigned_to; ?>"/>
            <label>PRIORITY</label>
            <select name="prio_level" id="prio_level" />
			<option value="" />=== PLEASE SELECT ===</option>
			<?php
			foreach ($a_prio as $data)
			{
				?>
                <option value="<?php echo $data; ?>" <?php if ($a_records->prio_level==$data) { echo "SELECTED"; } ?>/><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>
			</p>
			<hr>
            <p><label>PROBLEMS ENCOUNTERED/OBSERVED/REPORTED OR REQUEST DESCRIPTION</label></p>
			<hr>
				<p><?php echo $a_records->problem_desc; ?></p>
			<hr>
            <p><label>ACTION TAKEN</label></p>
			<hr>
				<p><?php echo $iUpdates; ?></p>
			<hr>
			<p><label>REMARKS</label>
			<textarea name="action_desc" id="action_desc"></textarea>
			</p>
            <script>
				CKEDITOR.replace( 'action_desc' );
            </script>
            <p><label>VERDICT</label>
            <select name="rec_stat" id="rec_stat" />
			<option value="" />=== PLEASE SELECT ===</option>
				<option value="OPEN" />KEEP OPEN</option>
                <option value="APPROVED" />APPROVED</option>
				<option value="REJECTED" />REJECTED</option>
            </select>
            <br /><br />
			<input type="hidden" id="id" name="id" value="<?php echo $a_records->id; ?>" />
            <input type="button" name="but_edit" id="but_edit" value="UPDATE" />
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>