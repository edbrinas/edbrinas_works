<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>
		<h1><?php echo $module_name; ?> [EDIT RECORD]</h1>
        <form id="sys_frm" enctype="multipart/form-data" />
            <p>
			<label>REFERENCE NUMBER</label>
			<?php echo $a_records->s_num; ?>
            <label>SUBJECT</label>
			<?php echo $a_records->detail_desc; ?>
            <label>ASSIGNED TO</label>
			<?php echo ($a_records->assigned_to) ? $a_records->assigned_to : 'N/A'; ?>
            <label>CREATED BY</label>
			<?php echo $a_records->created_by; ?>
            <label>APPROVED BY</label>
			<?php echo ($a_records->approved_by) ? $a_records->approved_by : 'PENDING MANAGER APPROVAL'; ?>
            <label>APPROVED DATE</label>
			<?php echo ($a_records->approved_date!='0000-00-00 00:00:00') ? $a_records->approved_date : 'N/A'; ?>
            <label>PRIORITY</label>
			<?php echo ($a_records->prio_level) ? $a_records->prio_level : 'N/A'; ?>
			<label>REQUEST TYPE</label>
			<?php echo $a_records->rec_type; ?>
			</p>
			<hr>
            <p><label>REQUEST DETAILS</label></p>
			<hr>
			<p><?php echo $a_records->problem_desc; ?></p>
			<hr>
            <p><label>OTHER REMARKS</label></p>
			<hr>
			<p><?php echo $iUpdates; ?></p>
			<hr>
			<p>
			<textarea name="action_desc" id="action_desc"></textarea>
			<label>ATTACHMENT EVIDENCE</label>
			<div id="items_place_holder">
			<?php
			if ($a_records->rec_attach) {
				?>
				<p>
				<input type="button" name="but_del_file" id="but_del_file" value="DELETE ATTACHMENT" />
				<a href='<?php echo base_url().'_temp_files/'.$a_records->rec_attach; ?>'><?php echo ucwords(strtolower($a_records->rec_attach)); ?></a>
				</p>
				<?php
			}
			else {
				?>
				<p>
				<input type='file' name='file_to_upload' id='file_to_upload' />
				</p>
				<?php
			}
			?>
			</div>
            <script>
				CKEDITOR.replace( 'action_desc' );
            </script>
            <p>
            <br /><br />
			<input type="hidden" id="ir_id" name="ir_id" value="<?php echo $a_records->ir_id; ?>" />
			<input type="hidden" id="id" name="id" value="<?php echo $a_records->id; ?>" />
            <input type="button" name="but_edit" id="but_edit" value="SAVE" />
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>