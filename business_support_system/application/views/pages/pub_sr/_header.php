<script type="text/javascript">

$(document).ready(function() 
{
	//$("#items_place_holder").empty();
	$('#processing').hide();
    var msg_req = "<font color='#FF0000'>Required field.</font>";
	var msg_email = "<font color='#FF0000'>Please enter a valid email address.</font>";
	var msg_digits = "<font color='#FF0000'>Numbers are only allowed.</font>";
	var sys_response;

	$("#sys_frm").validate({
		rules: {
			detail_desc: { required: true },
			rec_site: { required: true },
			prio_level: { required: true },
			sr_type: { required: true },
			sub_sr_type: { required: true },
			problem_desc: { required: true },
			action_desc: { required: true },
			create_sr: { required: true },
			client_name: { required: true },
			rec_stat: { required: true }
		},
		messages: {
			detail_desc: { required: msg_req },
			rec_site: { required: msg_req },
			prio_level: { required: msg_req },
			sr_type: { required: msg_req },
			sub_sr_type: { required: msg_req },
			problem_desc: { required: msg_req },
			action_desc: { required: msg_req },
			create_sr: { required: msg_req },
			client_name: { required: msg_req },
			rec_stat: { required: msg_req }
		}
	});
	
	$("#but_add").click(function(){
		$(this).attr("disabled", true);
		for (instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}		
		if($("#sys_frm").valid()) 
		{
			$.ajax({type: 'POST',
				   url: "<?php echo base_url().$controller_main.'CreateRecord'; ?>",
				   data: new FormData($('#sys_frm')[0]),
				   async: false,
				   contentType: false,
				   processData: false,
				   cache: false,
				   success:function(response)
				   {
						console.log(response);
						alert(response);
						window.location = '<?php echo base_url().$controller_main; ?>';
				   },
				   error:function (xhr, ajaxOptions, thrownError)
						{
							console.log(thrownError);
							alert(thrownError);
						}
				 })  
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});

	$("#but_edit").click(function(){
		if(confirm('Are you sure you want to edit this record?\n\nClick OK to continue. Otherwise click Cancel.\n')){
			$(this).attr("disabled", true);
			if($("#sys_frm").valid()) 
			{
				for (instance in CKEDITOR.instances) {
					CKEDITOR.instances[instance].updateElement();
				}				
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'ModifyRecord'; ?>",
					   data: new FormData($('#sys_frm')[0]),
					   async: false,
					   contentType: false,
					   processData: false,
					   cache: false,
					   success:function(response)
					   {
							alert(response);
							window.location = '<?php echo base_url().$controller_main; ?>';
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								console.log(thrownError);
								alert(thrownError);
							}
					 }) 
			}
			else
			{
				$(this).attr("disabled", false);
			}
		}
		else
		{
			window.location = '<?php echo base_url().$controller_main; ?>';
		}
	});	

	$("#but_del_file").click(function(){
		if(confirm('Are you sure you want to delete this file?\n\nClick OK to continue. Otherwise click Cancel.\n')){
			$(this).attr("disabled", true);
				$("#items_place_holder").empty();
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'RemoveFile'; ?>",
					   data: $("#sys_frm").serialize(),
					   success:function(response)
					   {
							alert("File deleted successfully!");
							$("#items_place_holder").append(response);
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								alert(thrownError);
							}
					 })
			$(this).attr("disabled", false);
		}
	});

	$("#but_can").click(function(){
		window.location = '<?php echo base_url().$controller_main; ?>';
	});		
	
	$('#mList').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllRecords'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 7, "desc" ]]	
	});

	$('#faList').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllForApproval'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 7, "desc" ]]	
	});	

	$('#mySrList').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllMyList'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 7, "desc" ]]	
	});
	
	$('#sr_type').change(function(){
		$("#items_place_holder").empty();
		$('#processing').show();
		$.ajax({type: 'POST',
			   url: "<?php echo base_url().$controller_main.'GetReqFields'; ?>",
			   data: $("#sys_frm").serialize(),
			   success:function(response)
			   {
					$('#processing').hide();
					$("#items_place_holder").append(response);
			   },
			   error:function (xhr, ajaxOptions, thrownError)
					{
						console.log(thrownError);
						alert(thrownError);
					}
			 }) 
	});	
	
	$("#client_name").tokenInput("<?php echo base_url().$controller_main.'GetUsers'; ?>", {
		theme: "facebook"
		<?php
		if($a_pre_pop) {
			echo ", prePopulate: ".$a_pre_pop;
		}
		?>
	});

	$("#assigned_to").tokenInput("<?php echo base_url().$controller_main.'GetUsers'; ?>", {
		theme: "facebook"
		<?php
		if($a_pre_pop) {
			echo ", prePopulate: ".$a_pre_pop;
		}
		?>
	});	
});
</script>