<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [ADD RECORD]</h1>
        <form id="sys_frm" enctype="multipart/form-data">
            <p>
            <label>DATE</label>
            <input type="text" id="mon_date" name="mon_date" class="sys_date" readonly />
			<label>SITE</label>
            <select name="mon_site" id="mon_site" />
			<option value="" />=== PLEASE SELECT ===</option>
			<?php
			foreach ($a_sites as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select> 
            <label>REMARKS</label>
			<textarea name="mon_rem" id="mon_rem"></textarea>	
			<label>ATTACHMENT</label>
			<input type="file" name="file_to_upload" id="file_to_upload">	
            <script>
                CKEDITOR.replace( 'mon_rem' );
            </script>
			<br /><br />
			<b><i>* = Required Field</i></b>			
            <br /><br />
            <input type="button" name="but_add" id="but_add" value="SAVE"/> 
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>