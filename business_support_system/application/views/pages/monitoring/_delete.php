<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [DELETE RECORD]</h1>
        <form id="sys_frm">
            <p>
            <table cellpadding="1" cellspacing="1" border="1" class="display" width="100%">
                <thead>
                    <tr>
                        <th width="20%">NAME</th>
                        <th width="80%">DESCRIPTION</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>TAG NUMBER</td> 
                        <td><?php echo ucwords(strtolower($a_records->tag_no)); ?></td>
                    </tr>
                    <tr>
                        <td>TYPE</td> 
                        <td><?php echo ucwords(strtolower($a_records->type)); ?></td>  
                    </tr>
                    <tr>
                        <td>SERIAL NUMBER</td> 
                        <td><?php echo ucwords(strtolower($a_records->serial_no)); ?></td>  
                    </tr> 
                    <tr>
                        <td>MAKE/MODEL</td> 
                        <td><?php echo ucwords(strtolower($a_records->make_model)); ?></td>  
                    </tr> 
                    <tr>
                        <td>DESCRIPTION</td> 
                        <td><?php echo ucwords(strtolower($a_records->description)); ?></td>  
                    </tr>   
                    <tr>
                        <td>PURCHASE DATE</td> 
                        <td><?php echo ucwords(strtolower($a_records->purchase_date)); ?></td>  
                    </tr>    
                    <tr>
                        <td>COMPONENT ID</td> 
                        <td><?php echo ucwords(strtolower($a_records->component_id)); ?></td>  
                    </tr> 
                    <tr>
                        <td>DONOR ID</td> 
                        <td><?php echo ucwords(strtolower($a_records->donor_id)); ?></td>  
                    </tr> 
                    <tr>
                        <td>PROJECT ID</td> 
                        <td><?php echo ucwords(strtolower($a_records->project_id)); ?></td>  
                    </tr>   
                    <tr>
                        <td>EMPLOYEE</td> 
                        <td><?php echo ucwords(strtolower($a_records->employee_id)); ?></td>  
                    </tr>            
                    <tr>
                        <td>UNIT</td> 
                        <td><?php echo ucwords(strtolower($a_records->unit)); ?></td>  
                    </tr> 
                    <tr>
                        <td>LOCATION</td> 
                        <td><?php echo ucwords(strtolower($a_records->location)); ?></td>  
                    </tr>                                                                                      
                </tbody>
            </table>
            <br /><br />
            <input type='hidden' name='id' id='id' value="<?php echo $a_records->id; ?>" readonly/>                                                              
            <input type="button" name="but_delete" id="but_delete" value="DELETE RECORD"/> 
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>