<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [EDIT RECORD]</h1>
        <form id="sys_frm" enctype="multipart/form-data">
            <p>
            <label>DATE *</label>
            <input type="text" id="mon_date" name="mon_date" class="sys_date" value="<?php echo $a_records->mon_date; ?>" readonly />
			<label>SITE *</label>
            <select name="mon_site" id="mon_site" />
			<option value="" />=== PLEASE SELECT ===</option>
			<?php
			foreach ($a_sites as $data)
			{
				?>
                <option value="<?php echo $data; ?>" <?php if ($data==$a_records->mon_site) { echo "SELECTED"; } ?>/><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select> 
            <label>REMARKS</label>
			<textarea name="mon_rem" id="mon_rem"><?php echo $a_records->mon_rem; ?></textarea>	
            <script>
                CKEDITOR.replace( 'mon_rem' );
            </script>
			<label>ATTACHMENT EVIDENCE</label>
			<div id="items_place_holder">
			<?php
			if ($a_records->rec_attach) {
				?>
				<p>
				<input type="button" name="but_del_file" id="but_del_file" value="DELETE ATTACHMENT" />
				<a href='<?php echo base_url().'_temp_files/'.$a_records->rec_attach; ?>'><?php echo ucwords(strtolower($a_records->rec_attach)); ?></a>
				</p>
				<?php
			}
			else {
				?>
				<p>
				<input type='file' name='file_to_upload' id='file_to_upload' />
				</p>
				<?php
			}
			?>
			</div>	
			<br /><br />
			<b><i>* = Required Field</i></b>
            <br /><br />
			<input type="hidden" id="id" name="id" value="<?php echo $a_records->id; ?>" />
            <input type="button" name="but_edit" id="but_edit" value="SAVE" /> 
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>