<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [EDIT RECORD]</h1>
        <form id="sys_frm">
            <p>
            <label>EQUIPMENT NAME</label>
            <input name='e_name' id='e_name' type='text' size="75" value='<?php echo $a_records->e_name; ?>' />            
            <label>EQUIPMENT DESCRIPTION</label>
            <textarea name="e_desc" id="e_desc" cols="50" rows="10"><?php echo $a_records->e_desc; ?></textarea>			
            <label>PURCHASED DATE</label>
            <input type="text" id="e_pur_date" name="e_pur_date" class="sys_date" value='<?php echo $a_records->e_pur_date; ?>' readonly />
            <label>WARRANTY EXPIRATION DATE</label>
            <input type="text" id="e_exp_date" name="e_exp_date" class="sys_date" value='<?php echo $a_records->e_exp_date; ?>' readonly />
            <label>EQUIPMENT SERIAL NUMBER</label>
            <input name='e_serial_num' id='e_serial_num' type='text' size="75" value='<?php echo $a_records->e_serial_num; ?>'/>
            <label>SUPPLIER NAME</label>
            <input name='e_sup_name' id='e_sup_name' type='text' size="75" value='<?php echo $a_records->e_sup_name; ?>'/>
			<label>EQUIPMENT LOCATION</label>
            <select name="e_loc" id="e_loc" />
			<?php
			foreach ($a_sites as $data)
			{
				?>
                <option value="<?php echo $data; ?>" <?php if ($a_records->e_loc==$data) { echo "SELECTED"; } ?>/><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select> 	
			<label>EQUIPMENT TYPE</label>
            <select name="e_type" id="e_type" />
			<?php
			foreach ($a_e_types as $data)
			{
				?>
                <option value="<?php echo $data; ?>" <?php if ($a_records->e_type==$data) { echo "SELECTED"; } ?>/><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>
			<label>EQUIPMENT MANUFACTURER</label>
            <select name="e_manu" id="e_manu" value='<?php echo $a_records->e_manu; ?>' />
			<?php
			foreach ($a_m_types as $data)
			{
				?>
                <option value="<?php echo $data; ?>" <?php if ($a_records->e_type==$data) { echo "SELECTED"; } ?> /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select> 
            <label>EQUIPMENT MODEL</label>
            <input name='e_model' id='e_model' type='text' size="75" value='<?php echo $a_records->e_model; ?>'/>
            <label>IP ADDRESS</label>
            <input name='e_ip_add' id='e_ip_add' type='text' size="75" value='<?php echo $a_records->e_ip_add; ?>'/>			
			<label>PROCESSOR</label>
            <select name="e_proc" id="e_proc" />
				<option value="N/A" <?php if ($a_records->e_proc=='N/A') { echo "SELECTED"; } ?> />N/A</option>
				<?php
				for ($x=1;$x<=64; $x++) {
					?>
					<option value="<?php echo $x; ?>" <?php if ($a_records->e_proc==$x) { echo "SELECTED"; } ?> /><?php echo sprintf("%02d",$x); ?></option>
					<?php
				} 				
				unset($x);
				?>
            </select> 			
			<label>MEMORY (GB)</label>
            <select name="e_mem" id="e_mem" />
				<option value="N/A" <?php if ($a_records->e_mem=='N/A') { echo "SELECTED"; } ?> />N/A</option>
				<?php
				foreach ($a_mem as $data)
				{
					?>
					<option value="<?php echo $data; ?>" <?php if ($a_records->e_mem==$data) { echo "SELECTED"; } ?> /><?php echo sprintf("%02d",$data); ?></option>
					<?php
				}
				unset($data);
				?>
            </select>  			
            <label>HDD</label>
            <input name='e_hdd' id='e_hdd' type='text' value='<?php echo $a_records->e_hdd; ?>' /> GB			
			<label>OPERATING SYSTEM</label>
            <select name="e_os" id="e_os" />
				<option value="N/A" <?php if ($a_records->e_mem=='N/A') { echo "SELECTED"; } ?> />N/A</option>
				<?php
				foreach ($a_os_types as $data)
				{
					?>
					<option value="<?php echo $data; ?>" <?php if ($a_records->e_os==$data) { echo "SELECTED"; } ?> /><?php echo $data; ?></option>
					<?php
				}
				unset($data);
				?>
            </select>                                                                                   
            <br /><br />
            <input type='hidden' name='id' id='id' value="<?php echo $a_records->id; ?>" readonly/>                                                              
            <input type="button" name="but_edit" id="but_edit" value="UPDATE RECORD"/> 
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>); ?>