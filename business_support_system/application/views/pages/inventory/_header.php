<script type="text/javascript">

$(document).ready(function() 
{
    var msg_req = "<font color='#FF0000'>Required field.</font>";
	var msg_digits = "<font color='#FF0000'>Must be numeric.</font>";
	var sys_response;

    $("#sys-frm-message").dialog({
        autoOpen: false,
        modal: true,
        title: "System Message",
        buttons: {
            Ok: function () {
                $(this).dialog('close');
				$(window).attr("location","<?php echo base_url().$controller_main; ?>");
            }
        }
    });		
	
	$("#sys_frm").validate({
		rules: {
			e_ip_add : { required: true },
			e_name : { required: true },
			e_desc : { required: true },
			e_pur_date : { required: true },
			e_exp_date : { required: true },
			e_sup_name : { required: true },
			e_loc : { required: true },
			e_type : { required: true },
			e_manu : { required: true },
			e_model : { required: true },
			e_serial_num : { required: true },
			e_proc : { required: true },
			e_mem : { required: true },
			e_hdd : { required: true, number: true },
			e_os : { required: true }   
		},
		messages: {
			e_ip_add : { required: msg_req },
			e_name : { required: msg_req },
			e_desc : { required: msg_req },
			e_pur_date : { required: msg_req },
			e_exp_date : { required: msg_req },
			e_sup_name : { required: msg_req },
			e_loc : { required: msg_req },
			e_type : { required: msg_req },
			e_manu : { required: msg_req },
			e_model : { required: msg_req },
			e_serial_num : { required: msg_req },
			e_proc : { required: msg_req },
			e_mem : { required: msg_req },
			e_hdd : { required: msg_req, number: msg_digits },
			e_os : { required: msg_req }
		}
	});
	
	$("#but_add").click(function(){
		$(this).attr("disabled", true);
		for (instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}		
		if($("#sys_frm").valid()) 
		{
			$.ajax({type: 'POST',
				   url: "<?php echo base_url().$controller_main.'CreateRecord'; ?>",
				   data: new FormData($('#sys_frm')[0]),
				   async: false,
				   contentType: false,
				   processData: false,
				   cache: false,
				   success:function(response)
				   {
						$(".sys-frm-inner-message").empty();
						$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-lightbulb' style='float:left; margin:0 7px 50px 0;' />" + response + "</span></p>");
						$("#sys-frm-message").dialog("open");
				   },
				   error:function (xhr, ajaxOptions, thrownError)
						{
							$(".sys-frm-inner-message").empty();
							$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />" + thrownError + "</span></p>");
							$("#sys-frm-message").dialog("open");
						}
				 })  
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});

	$("#but_edit").click(function (e) {
		e.preventDefault();
		if($("#sys_frm").valid()) 
		{
			$(".sys-frm-edit-msg-inner-msg").empty();
			$(".sys-frm-edit-msg-inner-msg").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />Are you sure you want to edit this record?<br /><br />Click Yes to continue, otherwise click No.</span></p>");
			$("#sys-frm-edit-msg").dialog("open");
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});	

	$('#sys-frm-edit-msg').dialog({ 
		title: "System Message",
		autoOpen: false,
		modal: true,
		width: 400,
		resizable: false,
		closeOnEscape: true,
		draggable: false,
		buttons: {
			'Yes': function(){
				$(this).dialog('close');
				for (instance in CKEDITOR.instances) {
					CKEDITOR.instances[instance].updateElement();
				}				
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'ModifyRecord'; ?>",
					   data: new FormData($('#sys_frm')[0]),
					   async: false,
					   contentType: false,
					   processData: false,
					   cache: false,
					   success:function(response)
					   {
							$(".sys-frm-inner-message").empty();
							$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-lightbulb' style='float:left; margin:0 7px 50px 0;' />" + response + "</span></p>");
							$("#sys-frm-message").dialog("open");
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								$(".sys-frm-inner-message").empty();
								$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />" + thrownError + "</span></p>");
								$("#sys-frm-message").dialog("open");
							}
					 })				
			},
			'No': function(){
				$(this).dialog('close');
			}
		}
	});
	
	$("#but_del_file").click(function (e) {
		e.preventDefault();
		$(".sys-frm-del-msg-inner-msg").empty();
		$(".sys-frm-del-msg-inner-msg").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />Are you sure you want to delete this file?<br /><br />Click Yes to continue, otherwise click No.</span></p>");
		$("#sys-frm-del-msg").dialog("open");
	});	

	$('#sys-frm-del-msg').dialog({ 
		title: "System Message",
		autoOpen: false,
		modal: true,
		width: 400,
		resizable: false,
		closeOnEscape: true,
		draggable: false,
		buttons: {
			'Yes': function(){
				$(this).dialog('close');
				$("#items_place_holder").empty();			
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'RemoveFile'; ?>",
					   data: $("#sys_frm").serialize(),
					   success:function(response)
					   {
							$("#items_place_holder").append(response);
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								$(".sys-frm-inner-message").empty();
								$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />" + thrownError + "</span></p>");
								$("#sys-frm-message").dialog("open");
							}
					 })				
			},
			'No': function(){
				$(this).dialog('close');
			}
		}
	});	

	$('#rec_list').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllRecords'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 4, "desc" ]]	
	});

	$('input').filter('.sys_date').datepicker({
		dateFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		minDate: '-10Y'
	});
});
</script>