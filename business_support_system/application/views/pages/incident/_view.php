<!-- DO NOT EDIT BELOW THIS COMMENT -->
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title><?php echo PAGE_TITLE; ?> [ REPORT VIEWER ]</title>
<link href='<?php echo base_url(); ?>_plugins/_css/screen.css' rel='stylesheet' type='text/css' media='screen'>
<link href='<?php echo base_url(); ?>_plugins/_css/print.css' rel='stylesheet' type='text/css' media='print'>
<link href='<?php echo base_url(); ?>_plugins/_css/report_table.css' rel='stylesheet' type='text/css'>
</head>
<body>
	<div align='center'><b><?php echo COMPANY_NAME; ?></b></div>
    <div align='center'><?php echo COMPANY_ADDRESS; ?></div>
    <div align='center'><?php echo 'Website: '.COMPANY_WEBSITE; ?></div>
    <div align='center'><?php echo 'Email Address: '.COMPANY_EMAIL_ADDRESS; ?></div>
    <div align='center'><?php echo 'Phone Number: '.COMPANY_PHONE_NUMBER; ?></div>
	<div align='center'><?php echo 'Fax Number: '.COMPANY_FAX_NUMBER; ?></div>
    <br />
    <div align='center'><?php echo $report_name; ?></div>
    <br />
    <!-- DO NOT EDIT ABOVE THIS COMMENT -->

    <table width='100%' class='report' border='1'>
        <thead>
        	<tr>
                <th width="20%">NAME</th>
                <th width="80%">DESCRIPTION</th>
            </tr>
        </thead>
        <tbody>
         	<tr>
            	<td>REFERENCE NUMBER</td>
                <td><?php echo $a_records->s_num; ?></td>
            </tr> 
         	<tr>
            	<td>SUBJECT</td>
                <td><?php echo $a_records->detail_desc; ?></td>
            </tr> 	
         	<tr>
            	<td>REPORTED BY</td>
                <td><?php echo $a_records->client_name; ?></td>
            </tr>
         	<tr>
            	<td>OPENED BY</td>
                <td><?php echo $a_records->created_by; ?></td>
            </tr>			
         	<tr>
            	<td>OPENED DATE</td>
                <td><?php echo $a_records->opened_date; ?></td>
            </tr>	
         	<tr>
            	<td>CLOSED DATE</td>
                <td><?php echo $a_records->closed_date; ?></td>
            </tr>
         	<tr>
            	<td>TIME LAPSED</td>
                <td><?php echo $ieDays; ?></td>
            </tr> 			
         	<tr>
            	<td>INCIDENT STATUS</td>
                <td><?php echo $a_records->rec_stat; ?></td>
            </tr>
         	<tr>
            	<td>INCIDENT TYPE</td>
                <td><?php echo $a_records->rec_type; ?></td>
            </tr>
         	<tr>
            	<td>PRIORITY LEVEL</td>
                <td><?php echo $a_records->prio_level; ?></td>
            </tr>
         	<tr>
            	<td>SITE OCCURED</td>
                <td><?php echo ucwords(strtolower($a_records->rec_site)); ?></td>
            </tr>			 			
         	<tr>
            	<td valign='top'>PROBLEMS ENCOUNTERED/OBSERVED/REPORTED</td>
                <td><?php echo $a_records->problem_desc; ?></td>
            </tr>
         	<tr>
            	<td valign='top'>LATEST UPDATES</td>
                <td valign='top'><?php echo $iUpdates; ?></td>
            </tr>		
         	<tr>
            	<td>EVIDENCE ATTACHMENT</td>
                <td><?php echo ($a_records->rec_attach) ? "<a href='".base_url()."_temp_files/".$a_records->rec_attach."'>".$a_records->rec_attach."</a>" : "N/A"; ?></td>
            </tr>			
        </tbody>
    </table>
 
    <!-- DO NOT EDIT BELOW THIS COMMENT -->
    <br><br>
    <div align='center' class='hideonprint'>
        <a href='JavaScript:window.print();'><img src='<?php echo base_url(); ?>_plugins/_css/main_images/printer.png' border='0' title='Print this page'></a> &nbsp; 
    </div>
    <div align='center' class='hideonprint'>
        <em>
        To remove header and footer of page
        <br>
        Go to 'File' -> 'Page Setup...' then erase the text in the 'Headers and Footers' field.
        </em>
    </div>
</body>
</html>
<!-- DO NOT EDIT ABOVE THIS COMMENT -->