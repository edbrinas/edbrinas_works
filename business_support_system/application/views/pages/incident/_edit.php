<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [EDIT RECORD]</h1>
        <form id="sys_frm" enctype="multipart/form-data" />
            <p>
			<label>REFERENCE NUMBER *</label>
			<?php echo $a_records->s_num; ?>
            <label>SUBJECT *</label>
			<?php echo $a_records->detail_desc; ?>
            <label>REPORTED BY *</label>
			<?php echo $a_records->client_name; ?>	
            <label>INCIDENT SITE *</label>
			<?php echo $a_records->rec_site; ?> 
            <label>PRIORITY *</label>
			<?php echo $a_records->prio_level; ?>
            <label>INCIDENT TYPE *</label>
			<?php echo $a_records->rec_type; ?>	
            <label>INCIDENT STATUS *</label>
            <select name="rec_stat" id="rec_stat" />
			<option value="" />=== PLEASE SELECT ===</option>
			<?php
			foreach ($a_ir_stat as $data)
			{
				?>
                <option value="<?php echo $data; ?>" <?php if ($data==$a_records->rec_stat) { echo "SELECTED"; } ?>/><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>
			</p>
			
			<hr>
            <label>PROBLEMS ENCOUNTERED/OBSERVED/REPORTED *</label>
			<hr>
			<p><?php echo $a_records->problem_desc; ?></p>
            
			<p>
			<hr>
			<label>ACTION TAKEN *</label>
			<hr>
			</p>
			<p><?php echo $iUpdates; ?></p>
			<hr>
			<p>
			<textarea name="action_desc" id="action_desc"></textarea>
			<label>ATTACHMENT EVIDENCE</label>
			<div id="items_place_holder">
			<?php
			if ($a_records->rec_attach) {
				?>
				<p>
				<input type="button" name="but_del_file" id="but_del_file" value="DELETE ATTACHMENT" />
				<a href='<?php echo base_url().'_temp_files/'.$a_records->rec_attach; ?>'><?php echo ucwords(strtolower($a_records->rec_attach)); ?></a>
				</p>
				<?php
			}
			else {
				?>
				<p>
				<input type='file' name='file_to_upload' id='file_to_upload' />
				</p>
				<?php
			}
			?>
			</div>
            <script>
				CKEDITOR.replace( 'action_desc' );
            </script>			
			<br /><br />
			<b><i>* = Required Field</i></b>                  
            <br /><br />
			<input type="hidden" id="id" name="id" value="<?php echo $a_records->id; ?>" />
            <input type="button" name="but_edit" id="but_edit" value="SAVE" /> 
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>