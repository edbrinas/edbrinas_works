<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [ADD RECORD]</h1>
        <form id="sys_frm" enctype="multipart/form-data" />
            <p>
            <label>SUBJECT *</label>
            <input type="text" id="detail_desc" name="detail_desc" size="75"/>
            <label>REPORTED BY *</label>
			<input type="text" id="client_name" name="client_name" size="75"/>
			<label>INCIDENT SITE *</label>
            <select name="rec_site" id="rec_site" />
			<option value="" />=== PLEASE SELECT ===</option>
			<?php
			foreach ($a_sites as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select> 
            <label>PRIORITY *</label>
            <select name="prio_level" id="prio_level" />
			<option value="" />=== PLEASE SELECT ===</option>
			<?php
			foreach ($a_prio as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>	
            <label>INCIDENT TYPE *</label>
            <select name="ir_type" id="ir_type" />
			<option value="" />=== PLEASE SELECT ===</option>
			<?php
			foreach ($a_ir_type as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>	
			</p>
			
			<p>
			<span id="processing" style="display: none;">
				<img alt="Please Wait" src="<?php echo base_url() ;?>_plugins/_css/main_images/processing.gif"/>
			</span>
			<div id="items_place_holder"></div>
			</p>
			
			<p>
			<label>CREATE SERVICE REQUEST? *</label>
            <select name="create_sr" id="create_sr" />
				<option value="" />=== PLEASE SELECT ===</option>
                <option value="YES" />YES</option>
				<option value="NO" />NO</option>
            </select>
			</p>
			
			<p>
            <label>PROBLEMS ENCOUNTERED/OBSERVED/REPORTED *</label>
			<textarea name="problem_desc" id="problem_desc"></textarea>		
            <label>ACTION TAKEN *</label>
			<textarea name="action_desc" id="action_desc"></textarea>	
			<label>ATTACHMENT EVIDENCE</label>
			<input type="file" name="file_to_upload" id="file_to_upload">
            <script>
                CKEDITOR.replace( 'problem_desc' );
				CKEDITOR.replace( 'action_desc' );
            </script>        
			<br /><br />
			<b><i>* = Required Field</i></b>			
            <br /><br />
            <input type="button" name="but_add" id="but_add" value="SAVE"/> 
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>