<script type="text/javascript">

$(document).ready(function() 
{
	$('#t_table').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllRecords'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 4, "desc" ]]	
	});
});

</script>