<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [APPROVE RECORD]</h1>
        <form id="sys_frm">
            <p>
			<label>STAFF NAME</label><?php echo $a_records->staff_name; ?>
			<label>START DATE</label><?php echo date("d-M-Y", strtotime($a_records->s_date)); ?>
			<label>END DATE</label><?php echo date("d-M-Y", strtotime($a_records->e_date)); ?>			
			<label>DATE FILED</label><?php echo $a_records->filed_date; ?>
			<label>APPROVAL ACTION</label>
			<select name='leave_status' id='leave_status'>
				<option value='APPROVED'>LEAVE APPROVED</option>
				<option value='REJECTED'>REJECT LEAVE</option>
				<option value='PENDING_APPROVAL' <?php if ($a_records->leave_status=='PENDING_APPROVAL') { echo "SELECTED"; } ?>>DO NOT MAKE ANY CHANGES OR RE-SUBMIT LEAVE</option>
			</select>       
            <br /><br />
            <input type='hidden' name='c_id' id='c_id' value="<?php echo $a_records->id; ?>" readonly/>
            <input type="button" name="but_approve" id="but_approve" value="SAVE"/> 
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>