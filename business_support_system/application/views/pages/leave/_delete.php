<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [DELETE RECORD]</h1>
        <form id="sys_frm">
            <p>
            <label>NAME</label>
            <input type="text" id="name" name="name" size="75" value="<?php echo $a_records->name; ?>" readonly/>
            <label>ADDRESS</label>
            <textarea name="address" id="address" readonly/><?php echo $a_records->address; ?></textarea>
            <label>POSTAL CODE</label>
            <input type="text" id="postal_code" name="postal_code" size="10" value="<?php echo $a_records->postal_code; ?>" readonly/>   
            <label>COUNTRY</label>
            <select name="country" id="country" disabled />
			<?php
			foreach ($a_countries as $key=>$value)
			{
				?>
                <option value="<?php echo $value; ?>" <?php if ($a_records->country==$value) { echo "SELECTED"; } ?> /><?php echo $value; ?></option>
                <?php
			}
			?>
            </select> 
            <label>CONTACT PERSON</label>
            <input type="text" id="contact_person" name="contact_person" size="75" value="<?php echo $a_records->contact_person; ?>" readonly/>      
            <label>CONTACT NUMBER</label>
            <input type="text" id="contact_number" name="contact_number" size="75" value="<?php echo $a_records->contact_number; ?>" readonly/>  
            <label>EMAIL ADDRESS</label>
            <input type="text" id="email_address" name="email_address" size="75" value="<?php echo $a_records->email_address; ?>" readonly/>
            <br /><br />
            <input type='hidden' name='id' id='id' value="<?php echo $a_records->id; ?>" readonly/>                                                              
            <input type="button" name="but_delete" id="but_delete" value="DELETE RECORD"/> 
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>