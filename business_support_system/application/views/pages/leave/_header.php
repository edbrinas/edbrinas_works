<script type="text/javascript">

$(document).ready(function() 
{
    var msg_req = "<font color='#FF0000'>Required field.</font>";
	var msg_email = "<font color='#FF0000'>Please enter a valid email address.</font>";
	var msg_digits = "<font color='#FF0000'>Numbers are only allowed.</font>";
	var sys_response;

	$("#sys_frm").validate({
		rules: {
			s_date: { required: true },
			e_date: { required: true },
			leave_reason: { required: true },
			leave_status: { required: true }
		},
		messages: {
			s_date: { required: msg_req },
			e_date: { required: msg_req },
			leave_reason: { required: msg_req },
			leave_status: { required: msg_req }
		}
	});

	$("#but_add").click(function (e) {
		e.preventDefault();
		if($("#sys_frm").valid()) 
		{
			$(".sys-frm-add-date-inner-msg").empty();
			$(".sys-frm-add-date-inner-msg").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />Are you sure you want to reserve this/these date(s)?<br /><br />Click Yes to continue, otherwise click No.</span></p>");
			$("#sys-frm-add-date-msg").dialog("open");
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});	

	$('#sys-frm-add-date-msg').dialog({ 
		title: "System Message",
		autoOpen: false,
		modal: true,
		width: 400,
		resizable: false,
		closeOnEscape: true,
		draggable: false,
		buttons: {
			'Yes': function(){
				$(this).dialog('close');
				for (instance in CKEDITOR.instances) {
					CKEDITOR.instances[instance].updateElement();
				}				
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'CreateRecord'; ?>",
					   data: new FormData($('#sys_frm')[0]),
					   async: false,
					   contentType: false,
					   processData: false,
					   cache: false,
					   success:function(response)
					   {
							$(".sys-frm-cr-inner-message").empty();
							$(".sys-frm-cr-inner-message").append("<p><span class='ui-icon ui-icon-lightbulb' style='float:left; margin:0 7px 50px 0;' />" + response + "</span></p>");
							$("#sys-frm-cr-message").dialog("open");
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								$(".sys-frm-cr-inner-message").empty();
								$(".sys-frm-cr-inner-message").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />" + thrownError + "</span></p>");
								$("#sys-frm-cr-message").dialog("open");
							}
					 })				
			},
			'No': function(){
				$(this).dialog('close');
			}
		}
	});
	
    $("#sys-frm-cr-message").dialog({
        autoOpen: false,
        modal: true,
        title: "System Message",
        buttons: {
            Ok: function () {
                $(this).dialog('close');
				$(window).attr("location","<?php echo base_url().$controller_main.'AddRecord'; ?>");
            }
        }
    });	
	
	$("#but_cancel").click(function (e) {
		e.preventDefault();
		if($("#sys_frm").valid()) 
		{
			$(".sys-frm-cancel-date-inner-msg").empty();
			$(".sys-frm-cancel-date-inner-msg").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />Are you sure you want to modify this date(s)?<br /><br />Click Yes to continue, otherwise click No.</span></p>");
			$("#sys-frm-cancel-date-msg").dialog("open");
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});	

	$('#sys-frm-cancel-date-msg').dialog({ 
		title: "System Message",
		autoOpen: false,
		modal: true,
		width: 400,
		resizable: false,
		closeOnEscape: true,
		draggable: false,
		buttons: {
			'Yes': function(){
				$(this).dialog('close');
				for (instance in CKEDITOR.instances) {
					CKEDITOR.instances[instance].updateElement();
				}				
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'ModifyRecord'; ?>",
					   data: new FormData($('#sys_frm')[0]),
					   async: false,
					   contentType: false,
					   processData: false,
					   cache: false,
					   success:function(response)
					   {
							$(".sys-frm-cncel-inner-message").empty();
							$(".sys-frm-cncel-inner-message").append("<p><span class='ui-icon ui-icon-lightbulb' style='float:left; margin:0 7px 50px 0;' />" + response + "</span></p>");
							$("#sys-frm-cncel-message").dialog("open");
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								$(".sys-frm-cncel-inner-message").empty();
								$(".sys-frm-cncel-inner-message").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />" + thrownError + "</span></p>");
								$("#sys-frm-cncel-message").dialog("open");
							}
					 })				
			},
			'No': function(){
				$(this).dialog('close');
			}
		}
	});
	
    $("#sys-frm-cncel-message").dialog({
        autoOpen: false,
        modal: true,
        title: "System Message",
        buttons: {
            Ok: function () {
                $(this).dialog('close');
				$(window).attr("location","<?php echo base_url().$controller_main.'MyLeaveRecord'; ?>");
            }
        }
    });	

	$("#but_approve").click(function (e) {
		e.preventDefault();
		if($("#sys_frm").valid()) 
		{
			$(".sys-frm-approve-date-inner-msg").empty();
			$(".sys-frm-approve-date-inner-msg").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />Are you sure you want to edit this/these date(s)?<br /><br />Click Yes to continue, otherwise click No.</span></p>");
			$("#sys-frm-approve-date-msg").dialog("open");
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});	

	$('#sys-frm-approve-date-msg').dialog({ 
		title: "System Message",
		autoOpen: false,
		modal: true,
		width: 400,
		resizable: false,
		closeOnEscape: true,
		draggable: false,
		buttons: {
			'Yes': function(){
				$(this).dialog('close');
				for (instance in CKEDITOR.instances) {
					CKEDITOR.instances[instance].updateElement();
				}				
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'ModifyApprovedRecord'; ?>",
					   data: new FormData($('#sys_frm')[0]),
					   async: false,
					   contentType: false,
					   processData: false,
					   cache: false,
					   success:function(response)
					   {
							$(".sys-frm-appr-inner-message").empty();
							$(".sys-frm-appr-inner-message").append("<p><span class='ui-icon ui-icon-lightbulb' style='float:left; margin:0 7px 50px 0;' />" + response + "</span></p>");
							$("#sys-frm-appr-message").dialog("open");
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								$(".sys-frm-appr-inner-message").empty();
								$(".sys-frm-appr-inner-message").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />" + thrownError + "</span></p>");
								$("#sys-frm-appr-message").dialog("open");
							}
					 })				
			},
			'No': function(){
				$(this).dialog('close');
			}
		}
	});

	$("#but_can").click(function(){
		window.location = '<?php echo base_url().$controller_main; ?>';
	});	
	
    $("#sys-frm-appr-message").dialog({
        autoOpen: false,
        modal: true,
        title: "System Message",
        buttons: {
            Ok: function () {
                $(this).dialog('close');
				$(window).attr("location","<?php echo base_url().$controller_main.'ApproveRecord'; ?>");
            }
        }
    });	

	$('#my_leaves').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllMyLeaves'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 6, "desc" ]]	
	});

	$('#for_approval').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllForApproval'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 5, "desc" ]]	
	});

    $("#s_date").datepicker({
        dateFormat: "yy-mm-dd",
		numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 0);
            $("#e_date").datepicker("option", "minDate", dt);
        }
    });
    $("#e_date").datepicker({
		dateFormat: "yy-mm-dd",
        numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 0);
            $("#s_date").datepicker("option", "maxDate", dt);
        }
    });
		
});
</script>

<!-- START OF LEAVE FORM ERROR MESSAGES DISPLAY -->
<div id="sys-frm-add-date-msg" style="display: none">
  <div class="sys-frm-add-date-inner-msg"></div>
</div>

<div id="sys-frm-cr-message" style="display: none">
	<div class="sys-frm-cr-inner-message"></div>
</div>	

<div id="sys-frm-cancel-date-msg" style="display: none">
  <div class="sys-frm-cancel-date-inner-msg"></div>
</div>

<div id="sys-frm-cncel-message" style="display: none">
	<div class="sys-frm-cncel-inner-message"></div>
</div>	

<div id="sys-frm-approve-date-msg" style="display: none">
  <div class="sys-frm-approve-date-inner-msg"></div>
</div>

<div id="sys-frm-appr-message" style="display: none">
	<div class="sys-frm-appr-inner-message"></div>
</div>
<!-- END OF LEAVE FORM ERROR MESSAGES DISPLAY -->