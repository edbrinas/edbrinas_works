<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [EDIT RECORD]</h1>
        <form id="sys_frm">
            <p>
            <label>START DATE</label>
            <input type="text" id="s_date" name="s_date" value="<?php echo $a_records->s_date; ?>" readonly />
            <label>END DATE</label>
            <input type="text" id="e_date" name="e_date" value="<?php echo $a_records->e_date; ?>" readonly />
            <label>LEAVE REASON</label>
			<select name="leave_reason" id="leave_reason" />
			<option value="" />=== SELECT ONE ===</option>
			<?php
			foreach ($a_l_type as $value)
			{
				?>
                <option value="<?php echo $value; ?>" <?php if ($value==$a_records->leave_reason) { echo "SELECTED"; } ?>/><?php echo $value; ?></option>
                <?php
			}
			?>
			</select>
            <label>LEAVE STATUS</label>
			<select name="leave_status" id="leave_status" />
			<option value="" />=== SELECT ONE ===</option>
				<option value="CANCELLED" />CANCEL</option>
				<option value="PENDING_APPROVAL" />UPDATE CHANGES</option>
			</select>
            </p>       
            <br /><br />
            <input type='hidden' name='c_id' id='c_id' value="<?php echo $a_records->id; ?>" readonly/>                                                              
            <input type="button" name="but_cancel" id="but_cancel" value="SAVE" /> 
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>