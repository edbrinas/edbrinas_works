<script type="text/javascript">

$(document).ready(function() 
{
    var msg_req = "<font color='#FF0000'>Required field.</font>";
	var msg_email = "<font color='#FF0000'>Please enter a valid email address.</font>";
	var msg_digits = "<font color='#FF0000'>Numbers are only allowed.</font>";
	var sys_response;

	$("#sys_frm").validate({
		rules: {
			s_date: { required: true },
			e_date: { required: true },
			leave_reason: { required: true },
			leave_status: { required: true }
		},
		messages: {
			s_date: { required: msg_req },
			e_date: { required: msg_req },
			leave_reason: { required: msg_req },
			leave_status: { required: msg_req }
		}
	});

	$("#but_add").click(function(){
		if(confirm('Are you sure you want to edit this/these date(s)?\n\nClick OK to continue. Otherwise click Cancel.\n')){
			$(this).attr("disabled", true);
			if($("#sys_frm").valid()) 
			{
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'CreateRecord'; ?>",
					   data: $("#sys_frm").serialize(),
					   success:function(response)
					   {
							alert(response);
							window.location = '<?php echo base_url().$controller_main.'AddRecord'; ?>';
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								console.log(thrownError);
								alert(thrownError);
							}
					 }) 
			}
			else
			{
				$(this).attr("disabled", false);
			}
		}
		else
		{
			window.location = '<?php echo base_url().$controller_main.'AddRecord'; ?>';
		}
	});	
	
	
	$("#but_cancel").click(function(){
		if(confirm('Are you sure you want to edit this record?\n\nClick OK to continue. Otherwise click Cancel.\n')){
			$(this).attr("disabled", true);
			if($("#sys_frm").valid()) 
			{
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'ModifyRecord'; ?>",
					   data: $("#sys_frm").serialize(),
					   success:function(response)
					   {
							alert(response);
							window.location = '<?php echo base_url().$controller_main.'CancelRecord'; ?>';
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								console.log(thrownError);
								alert(thrownError);
							}
					 }) 
			}
			else
			{
				$(this).attr("disabled", false);
			}
		}
		else
		{
			window.location = '<?php echo base_url().$controller_main; ?>';
		}
	});	

	$("#but_approve").click(function(){
		if(confirm('Are you sure you want to edit this record?\n\nClick OK to continue. Otherwise click Cancel.\n')){
			$(this).attr("disabled", true);
			if($("#sys_frm").valid()) 
			{
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'ModifyApprovedRecord'; ?>",
					   data: $("#sys_frm").serialize(),
					   success:function(response)
					   {
							alert(response);
							window.location = '<?php echo base_url().$controller_main.'ApproveRecord'; ?>';
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								console.log(thrownError);
								alert(thrownError);
							}
					 }) 
			}
			else
			{
				$(this).attr("disabled", false);
			}
		}
		else
		{
			window.location = '<?php echo base_url().$controller_main; ?>';
		}
	});

	$('#my_leaves').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllMyLeaves'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 4, "desc" ]]	
	});

	$('#for_approval').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllForApproval'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 0, "desc" ]]	
	});

    $("#s_date").datepicker({
        dateFormat: "yy-mm-dd",
		numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#e_date").datepicker("option", "minDate", dt);
        }
    });
    $("#e_date").datepicker({
		dateFormat: "yy-mm-dd",
        numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#s_date").datepicker("option", "maxDate", dt);
        }
    });
		
});
</script>