<script type="text/javascript">
	$(document).ready(function() 
	{
		$( "#graphs_div" ).accordion({
			collapsible: true,
			autoHeight: false,
			navigation: true 
		});	
	});	
</script>
<script type="text/javascript">

	var pOptions = {
title: '<?php echo $cTitle; ?>',
is3D: true,
		pieStartAngle: 100,
	fontSize: 12,
	fontName: 'Verdana',

		chartArea: {
			left:50,top:50,width:'200%',height:'80%' 
		}	
	};
	
	var bOptions = {
		width:600,height:500,
		bars: 'horizontal',
		axes: {
			x: {
				0: { side: 'top', label: 'Percentage'}
			}
		},
		bar: { groupWidth: "80%" },
		chartArea: {
			left:350,top:10,width:'100%',height:'100%' 
		}
	};

	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

	function drawChart() {
		
		/* Incident Types */
		<?php $cTitle = "INCIDENT TYPES"; ?>
		var iTdata = new google.visualization.DataTable();
		iTdata.addColumn('string', 'Topping');
		iTdata.addColumn('number', 'Slices');
		iTdata.addRows([
		  ['ACCESS/SECURITY - REQEST ACCESS TO NETWORK/INTERNET', 3],
		  ['ACCESS/SECURITY - ACCESS TO A NETWORK FOLDER', 1],
		  ['HARDWARE - BROKEN COMPONENT', 1],
		  ['SOFTWARE/BUSINESS APPLICATION - DEFECT/ERROR MESSAGE', 1],
		  ['TELEPHONY - CONFIGURATION', 2]
		]);

		var iTpchart = new google.visualization.PieChart(document.getElementById('iTPieChart'));
		iTpchart.draw(iTdata, pOptions);

		
		/* Service Request Types */
		var sRtdata = new google.visualization.DataTable();
		sRtdata.addColumn('string', 'Topping');
		sRtdata.addColumn('number', 'Slices');
		sRtdata.addRows([
		  ['ACCESS/SECURITY - REQEST ACCESS TO NETWORK/INTERNET', 3],
		  ['HARDWARE - REQUEST FOR REPAIR BROKEN COMPONENT', 1],
		  ['SOFTWARE/BUSINESS APPLICATION - REQUEST FOR REPAIR DEFECT/ERROR MESSAGE', 1],
		  ['TELEPHONY - REQUEST TO FIX BROKEN TELEPHONE', 1],
		  ['HARDWARE - REQUEST FOR MAINTENANCE', 2]
		]);

		var sRtchart = new google.visualization.PieChart(document.getElementById('sRtPieChart'));
		sRtchart.draw(sRtdata, pOptions);	
		
		/* Incident Status */
		var iSdata = new google.visualization.DataTable();
		iSdata.addColumn('string', 'Topping');
		iSdata.addColumn('number', 'Count');
		iSdata.addRows([			
		  ['TROUBLESHOOTING ON-GOING', 500],
		  ['AWAITING CUSTOMER RESPONSE', 200],
		  ['RESOLVED - AWAITING CUSTOMER RESPONSE', 575],
		  ['CLOSED', 1000],
		  ['OPEN', 2500]			  
		]);

		var iSchart = new google.visualization.PieChart(document.getElementById('iSPieChart'));
		iSchart.draw(iSdata, pOptions);		

		/* Service Request Status */
		var sRsdata = new google.visualization.DataTable();
		sRsdata.addColumn('string', 'Topping');
		sRsdata.addColumn('number', 'Slices');
		sRsdata.addRows([
		  ['TROUBLESHOOTING ON-GOING', 500],
		  ['AWAITING CUSTOMER RESPONSE', 200],
		  ['RESOLVED - AWAITING CUSTOMER RESPONSE', 575],
		  ['CLOSED', 1000],
		  ['OPEN', 2500]
		]);

		var sRschart = new google.visualization.PieChart(document.getElementById('sRsPieChart'));
		sRschart.draw(sRsdata, pOptions);					
	}
</script>