<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<div id="main">	
	<?php
	if (empty($show_graphs))
	{
		?>
        <h1>IMPORTANT Notice</h1>
        <p><img src="<?php echo base_url(); ?>_plugins/_css/main_images/company_logo.jpg" width="250" height="100" alt="Saudi Electricity Company" class="float-left" border="0" />
                <p>This site is to be used by <?php echo COMPANY_NAME; ?> employees only. Unauthorized access or use is strictly prohibited and constitutes a crime punishable by law.</p>
    
                <p>It is a criminal offense to:<br />
                    1. Obtain access to data without authority<br />
                    2. Corrupt, alter, steal, or destroy data<br />
                    3. Interfere in computer system or server;<br />
                    4. Introduce computer virus
                </p>
        </p>
		<?php
	}
	else
	{
		?>
		<h1>Dashboard</h1>
		<p>
		<?php include_once(APPPATH.'/views/pages/_default/charts/_sys_charts.php'); ?>


		<div id="iSPieChart" style="width: 600px; height: 450px;"></div>
		<div id="sRsPieChart" style="width: 600px; height: 450px;"></div>
		<div id="iTPieChart" style="width: 600px; height: 450px;"></div>
		<div id="sRtPieChart" style="width: 600px; height: 450px;"></div>

		
</p>
	<?php
	}
	?>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>