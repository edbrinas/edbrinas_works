<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [ADD RECORD]</h1>
        <form id="sys_frm">
            <p>
            <label>ASSET CLASS *</label>
            <select name="asset_class" id="asset_class" />
			<option value="" />=== SELECT ONE ===</option>
			<?php
			foreach ($a_class as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>
			<label>ASSET LOCATION *</label>
            <select name="asset_loc" id="asset_loc" />
			<option value="" />=== SELECT ONE ===</option>			
			<?php
			foreach ($a_sites as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select> 	
            <label>ASSET CATEGORY *</label>
            <select name="model_cat" id="model_cat" />
			<option value="" />=== SELECT ONE ===</option>
			<?php
			foreach ($a_cat as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>            
            <label>ASSET NAME *</label>
            <input name='model_name' id='model_name' type='text' size="75"/>
            <label>ASSET MANUFACTURER *</label>
            <select name="model_manu" id="model_manu" />
			<option value="" />=== SELECT ONE ===</option>
			<?php
			foreach ($a_manu as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>
            <label>SERIAL NUMBER *</label>
            <input name='serial_num' id='serial_num' type='text' size="75" value="N/A" />
            <label>ASSET TAG *</label>
            <input name='asset_tag' id='asset_tag' type='text' size="75"/>
            <label>ASSIGNED TO</label>
			<input type="text" id="assigned_to" name="assigned_to" size="75" />			
			<label>ASSET DEPARTMENT *</label>
            <select name="asset_dept" id="asset_dept" />
			<option value="" />=== SELECT ONE ===</option>
			<?php
			foreach ($a_dept as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>			
            <label>WARRANTY EXPIRATION DATE</label>
            <input type="text" id="wxp_date" name="wxp_date" class="sys_date" readonly />
            <label>INSTALLED DATE</label>
            <input type="text" id="installed_date" name="installed_date" class="sys_date" readonly />
            <label>ASSIGNED DATE</label>
            <input type="text" id="assigned_date" name="assigned_date" class="sys_date" readonly />
            <label>ASSET SPECIFICATIONS *</label>
			<textarea name="asset_specs" id="asset_specs"></textarea>		
            <script>
                CKEDITOR.replace( 'asset_specs' );
            </script>	
			<br /><br />
			<b><i>* = Required Field</i></b>
            <br /><br />
            <input type="button" name="but_add" id="but_add" value="SAVE"/> 
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>