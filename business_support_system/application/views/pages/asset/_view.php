<!-- DO NOT EDIT BELOW THIS COMMENT -->
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title><?php echo PAGE_TITLE; ?> [ REPORT VIEWER ]</title>
<link href='<?php echo base_url(); ?>_plugins/_css/screen.css' rel='stylesheet' type='text/css' media='screen'>
<link href='<?php echo base_url(); ?>_plugins/_css/print.css' rel='stylesheet' type='text/css' media='print'>
<link href='<?php echo base_url(); ?>_plugins/_css/report_table.css' rel='stylesheet' type='text/css'>
</head>
<body>
	<div align='center'><b><?php echo COMPANY_NAME; ?></b></div>
    <div align='center'><?php echo COMPANY_ADDRESS; ?></div>
    <div align='center'><?php echo 'Website: '.COMPANY_WEBSITE; ?></div>
    <div align='center'><?php echo 'Email Address: '.COMPANY_EMAIL_ADDRESS; ?></div>
    <div align='center'><?php echo 'Phone Number: '.COMPANY_PHONE_NUMBER; ?></div>
	<div align='center'><?php echo 'Fax Number: '.COMPANY_FAX_NUMBER; ?></div>
    <br />
    <div align='center'><?php echo $report_name; ?></div>
    <br />
    <!-- DO NOT EDIT ABOVE THIS COMMENT -->
    
    <table width='100%' class='report' border='1'>
        <thead>
        	<tr>
                <th width="20%">NAME</th>
                <th width="80%">DESCRIPTION</th>
            </tr>
        </thead>
        <tbody>
			<tr>
				<td>ASSET CLASS</td>
				<td><?php echo $a_records->asset_class; ?></td>
			</tr>
			<tr>
				<td>ASSET LOCATION</td>
				<td><?php echo $a_records->asset_loc; ?></td>
			</tr>
			<tr>
				<td>ASSET CATEGORY</td>
				<td><?php echo $a_records->model_cat; ?></td>
			</tr>   
			<tr>			
				<td>ASSET NAME</td>
				<td><?php echo $a_records->model_name; ?></td>
			</tr>
			<tr>
				<td>ASSET MANUFACTURER</td>
				<td><?php echo $a_records->model_manu; ?></td>
			</tr>
			<tr>
				<td>SERIAL NUMBER</td>
				<td><?php echo $a_records->serial_num; ?></td>
			</tr>
			<tr>
				<td>ASSET TAG</td>
				<td><?php echo $a_records->asset_tag; ?></td>
			</tr>
			<tr>
				<td>ASSET STATE</td>
				<td><?php echo $a_records->asset_state; ?></td>
			</tr>			
			<tr>
				<td>ASSIGNED TO</td>
				<td><?php echo $a_records->assigned_to; ?></td>
			</tr>
			<tr>
				<td>ASSET DEPARTMENT</td>
				<td><?php echo $a_records->asset_dept; ?></td>
			</tr>
			<tr>
				<td>INSTALLED DATE</td>
				<td><?php echo $a_records->installed_date; ?></td>
			</tr>				
			<tr>
				<td>ASSIGNED DATE</td>
				<td><?php echo $a_records->assigned_date; ?></td>
			</tr>
			<tr>
				<td>WARRANTY EXPIRATION DATE</td>
				<td><?php echo $a_records->wxp_date; ?></td>
			</tr>			
			<tr  >
				<td valign='top'>ASSET SPECIFICATIONS</td>
				<td valign='top'><?php echo $a_records->asset_specs; ?></td>
			</tr>
			<tr>
				<td valign='top'>COMMENTS</td>
				<td valign='top'><?php echo $a_records->comments; ?></td>
			</tr>
			<tr>
				<td>DECOMMISSIONED DATE</td>
				<td><?php echo $a_records->decommissioned_date; ?></td>
			</tr>
			<tr>
				<td valign='top'>LAST UPDATE BY</td>
				<td><?php echo $a_records->last_update_by.' @ '.$a_records->last_update_date; ?></td>
			</tr>
        </tbody>
    </table>
    
    <!-- DO NOT EDIT BELOW THIS COMMENT -->
    <br><br>
    <div align='center' class='hideonprint'>
        <a href='JavaScript:window.print();'><img src='<?php echo base_url(); ?>_plugins/_css/main_images/printer.png' border='0' title='Print this page'></a> &nbsp; 
    </div>
    <div align='center' class='hideonprint'>
        <em>
        To remove header and footer of page
        <br>
        Go to 'File' -> 'Page Setup...' then erase the text in the 'Headers and Footers' field.
        </em>
    </div>
</body>
</html>
<!-- DO NOT EDIT ABOVE THIS COMMENT -->