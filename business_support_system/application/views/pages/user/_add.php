<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>
		<h1><?php echo $module_name; ?> [ADD RECORD]</h1>
        <form id="sys_frm">
            <p>
            <label>Email Address</label>
            <input name='email_address' id='email_address' type='text' size="75"/>
            <label>Password</label>
            <input name='password' id='password' type='password' size="75"/>
            <label>Confirm Password</label>
            <input name='retype_password' id='retype_password' type='password' size="75"/>
            <label>First Name</label>
            <input name='emp_f_name' id='emp_f_name' type='text' size="75"/>
            <label>Middle Initial</label>
            <input name='emp_m_name' id='emp_m_name' type='text' size="1"/>
            <label>Last Name</label>
            <input name='emp_l_name' id='emp_l_name' type='text' size="75"/>
            <label>Employee ID</label>
            <input name='emp_id' id='emp_id' type='text' size="75"/>			
			<label>DEPARTMENT</label>
            <select name="e_dept" id="e_dept" />
			<option value="" />=== PLEASE SELECT ===</option>
			<?php
			foreach ($a_dept as $data)
			{
				?>
                <option value="<?php echo $data; ?>" /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>
            <label>Account Type</label>
			<?php echo $a_type; ?>
            <hr />
            <label>Page Access: </label>
            <?php echo $p_links; ?>
            <br /><br />
            <input type="button" value="SAVE" id="but_add" name="but_add" />
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>