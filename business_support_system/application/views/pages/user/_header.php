<script type="text/javascript">

$(document).ready(function()
{
    var msg_req = "<font color='#FF0000'>Required field.</font>";
    var msg_minlen = jQuery.format("<font color='#FF0000'>At least {0} characters are necessary.</font>");
	var msg_chkreq = jQuery.format("<font color='#FF0000'>Check no more than 1 box.</font>");
    var msg_maxlen = jQuery.format("<font color='#FF0000'>Maximum {0} characters are only allowed.</font>");
    var msg_digits = "<font color='#FF0000'>Numbers are only allowed.</font>";
	var msg_rtypepass = "<font color='#FF0000'>Please enter the same password value again.</font>";
	var msg_email = "<font color='#FF0000'>Please enter a valid email address.</font>";
	var sys_response;

	$("#sys_frm").validate({
		rules: {
			username: {
				required: true
			},
			password: {
				minlength : 5,
				required: true
			},
			retype_password:
			{
				equalTo: "#password"
			},
			emp_f_name: {
				required: true
			},
			emp_l_name: {
				required: true
			},
			status: {
				required: true
			},
			e_add: {
				required: true,
				email: true
			},
			e_dept: {
				required: true
			},
			'type[]': {
				required: true
			},
			'p_id[]': {
				required: true
			}

		},
		messages: {
			username: {
				required: msg_req
			},
			password: {
				minlength : msg_minlen,
				required: msg_req
			},
			retype_password:
			{
				equalTo: msg_rtypepass
			},
			emp_f_name: {
				required: msg_req
			},
			emp_l_name: {
				required: msg_req
			},
			status: {
				required: msg_req
			},
			e_add: {
				required: msg_req,
				email: msg_email
			},
			e_dept: {
				required: msg_req
			},
			'type[]': {
				required: msg_req
			},'p_id[]': {
				required: msg_req
			}
		}
	});

	$("#but_add").click(function(){
		$(this).attr("disabled", true);
		if($("#sys_frm").valid())
		{
			$.ajax({type: 'POST',
				   url: "<?php echo base_url().$controller_main.'CreateRecord'; ?>",
				   data: $("#sys_frm").serialize(),
				   success:function(response)
				   {
				   		alert(response);
						window.location = '<?php echo base_url().$controller_main; ?>';
				   },
				   error:function (xhr, ajaxOptions, thrownError)
						{
							console.log(thrownError);
							alert(thrownError);
						}
				 })
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});

	$("#but_edit").click(function(){
		if(confirm('Are you sure you want to edit this record?\n\nClick OK to continue. Otherwise click Cancel.\n')){
			$(this).attr("disabled", true);
			if($("#sys_frm").valid())
			{
				$.ajax({type: 'POST',
					   url: "<?php echo base_url().$controller_main.'ModifyRecord'; ?>",
					   data: $("#sys_frm").serialize(),
					   success:function(response)
					   {
							alert(response);
							window.location = '<?php echo base_url().$controller_main; ?>';
					   },
					   error:function (xhr, ajaxOptions, thrownError)
							{
								console.log(thrownError);
								alert(thrownError);
							}
					 })
			}
			else
			{
				$(this).attr("disabled", false);
			}
		}
		else
		{
			window.location = '<?php echo base_url().$controller_main; ?>';
		}
	});

	$("#but_can").click(function(){
		window.location = '<?php echo base_url().$controller_main; ?>';
	});	
	
	$('#rec_list').dataTable({
			"sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url().$controller_main.'GetAllRecord'; ?>",
            "iDisplayLength": 50,
			"aaSorting": [[ 6, "desc" ]]
	});
});

</script>