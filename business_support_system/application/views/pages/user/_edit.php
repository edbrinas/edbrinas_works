<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header.php'); ?>
<div id='main'>	
		<h1><?php echo $module_name; ?> [EDIT RECORD]</h1>
        <form id="sys_frm">
            <p>
            <label>USERNAME</label>
			<?php echo $a_user->username ; ?>
			<label>EMAIL ADDRESS</label>
			<input name='email_address' id='email_address' type='text' size="75" value="<?php echo $a_user->e_add; ?>"/>
            <label>First Name</label>
            <input name='emp_f_name' id='emp_f_name' type='text' size="75" value="<?php echo $a_user->f_name; ?>"/>
            <label>Middle Initial</label>
            <input name='emp_m_name' id='emp_m_name' type='text' size="1" value="<?php echo $a_user->m_name; ?>"/>  
            <label>Last Name</label>
            <input name='emp_l_name' id='emp_l_name' type='text' size="75" value="<?php echo $a_user->l_name; ?>"/>
            <label>Employee ID</label>
            <input name='emp_id' id='emp_id' type='text' size="75" value="<?php echo $a_user->emp_id; ?>"/>
			<label>DEPARTMENT</label>
            <select name="e_dept" id="e_dept" />
			<option value="" />=== PLEASE SELECT ===</option>
			<?php
			foreach ($a_dept as $data)
			{
				?>
                <option value="<?php echo $data; ?>" <?php if ($a_user->e_dept==$data) { echo "SELECTED"; } ?> /><?php echo $data; ?></option>
                <?php
			}
			unset($data);
			?>
            </select>			
            </p>
            <hr />
            <p>
            <label>NEW PASSWORD</label>
            <input name='password' id='password' type='password'/>
            <label>CONFIRM PASSWORD</label>
            <input name='retype_password' id='retype_password' type='password'/>     
			<label>EMAIL ALERTS</label>
			<select name='enable_notification' id='enable_notification'>
					<option value='E' <?php if ($a_user->enable_notification=='E') { echo "SELECTED"; } ?>>ENABLED</option>
                    <option value='D' <?php if ($a_user->enable_notification=='D') { echo "SELECTED"; } ?>>DISABLED</option>
            </select>             
            <label>ACCOUNT STATUS</label>
			<select name='r_status' id='r_status'>
					<option value='A' <?php if ($a_user->status=='A') { echo "SELECTED"; } ?>>ACTIVE</option>
                    <option value='D' <?php if ($a_user->status=='D') { echo "SELECTED"; } ?>>DISABLED</option>
            </select>    
            </p>  
            <hr />
            <p>
            <label>ACCOUNT TYPE</label>
			<?php echo $a_type; ?>  
            </p>
            <hr />
            <p>
            <label>ALLOWED PAGES</label>
            <?php echo $p_links; ?>                                                             
            <br /><br />
            <input name='id' id='id' type='hidden' size="75" value='<?php echo $a_user->id ; ?>' readonly/>
            <input type="button" value="SAVE" id="but_edit" name="but_edit" /> 
			<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>		
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>