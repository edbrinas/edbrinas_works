<?php include_once(APPPATH.'views/includes/_header.php'); ?>
<?php include_once('_header_public_profile.php'); ?>
<div id='main'>
		<h1><?php echo $module_name; ?> [EDIT RECORD]</h1>
        <form id="sys_frm">
            <p>
                <label>USERNAME</label>
                <?php echo $record->username; ?>
                <label>EMAIL ADDRESS</label>
                <?php echo $record->e_add; ?>
            </p>
            <hr />
				<label>CHANGE SYSTEM ACCESS PASSWORD</label>
			<hr />
            <p>
				<label>First Name</label>
				<input name='emp_f_name' id='emp_f_name' type='text' size="75" value="<?php echo $record->f_name; ?>"/>
				<label>Middle Initial</label>
				<input name='emp_m_name' id='emp_m_name' type='text' size="1" value="<?php echo $record->m_name; ?>"/>
				<label>Last Name</label>
				<input name='emp_l_name' id='emp_l_name' type='text' size="75" value="<?php echo $record->l_name; ?>"/>
				<label>Employee ID</label>
				<input name='emp_id' id='emp_id' type='text' size="75" value="<?php echo $record->emp_id; ?>"/>
                <label>NEW PASSWORD</label>
                <input name='password' id='password' type='password' size="75"/>
                <label>CONFIRM NEW PASSWORD</label>
                <input name='retype_password' id='retype_password' type='password' size="75"/>
                <label>EMAIL ALERTS</label>
                <select name='enable_notification' id='enable_notification'>
                        <option value='E' <?php if ($record->enable_notification=='E') { echo "SELECTED"; } ?>>ENABLED</option>
                        <option value='D' <?php if ($record->enable_notification=='D') { echo "SELECTED"; } ?>>DISABLED</option>
                </select>
                <br /><br />
                <input name='id' id='id' type='hidden' size="75" value='<?php echo $record->id ; ?>' readonly/>
                <input type="button" value="SAVE" id="but_edit" name="but_edit" />
				<input type="button" value="CANCEL" id="but_can" name="but_can" />
            </p>
        </form>
</div>
<?php include_once(APPPATH.'views/includes/_footer.php'); ?>