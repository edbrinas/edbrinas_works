<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<meta name="Description" content="." />
<meta name="Keywords" content="" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="Distribution" content="Global" />
<meta name="Author" content="" />
<meta name="Robots" content="index,follow" />

<!-- MAIN WEBSITE CSS INCLUDES -->
<link rel="stylesheet" href="<?php echo base_url() ;?>_plugins/_css/main.css" />			
<link rel="stylesheet" href="<?php echo base_url() ;?>_plugins/_css/data_table.css" />
<link rel="stylesheet" href="<?php echo base_url() ;?>_plugins/_css/calendar.css" />
<link rel="stylesheet" href="<?php echo base_url() ;?>_plugins/_css/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url() ;?>_plugins/_css/jquery-ui-1.8.18.custom.css"/>
<link rel="stylesheet" href="<?php echo base_url() ;?>_plugins/_css/token-input.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url() ;?>_plugins/_css/token-input-facebook.css" type="text/css" />

<!-- JQUERY INCLUDES -->
<script src="<?php echo base_url() ;?>_plugins/_jquery/jquery_1.7.2.js"></script>
<script src="<?php echo base_url() ;?>_plugins/_jquery/jquery.qtip.js"></script>
<script src="<?php echo base_url() ;?>_plugins/_jquery/jquery-ui-1.8.21.custom.min.js"></script>
<script src="<?php echo base_url() ;?>_plugins/_jquery/jquery.validate.js"></script>
<script src="<?php echo base_url() ;?>_plugins/_jquery/jquery.validation.functions.js"></script>
<script type="text/javascript" src="<?php echo base_url() ;?>_plugins/_jquery/jquery.tokeninput.js"></script>
<script type="text/javascript" src="<?php echo base_url() ;?>_plugins/_jquery/loader.js"></script>

<!-- DATA TABLE INCLUDES -->
<script src="<?php echo base_url() ;?>_plugins/_jquery/data_table.js"></script>

<!-- cKEDITOR INCLUDES -->
<script src="<?php echo base_url() ;?>_plugins/_ckeditor/ckeditor.js"></script>

<script type="text/javascript">
$(document).ready(function() 
{
	//window.history.forward(-1);
	$( "#sys-message" ).dialog({
		title: "System Message",
		modal: true,
		buttons: {
			Ok: function() {
				<?php
				if (!empty($controller_main)) 
				{
					?>
					if(window.opener != null) {
						window.close();
					}
					else {
						window.location = '<?php echo base_url().$controller_main; ?>';
					}
					<?php
				}
				else
				{
					?>
					if(window.opener != null) {
						window.close();
					}
					else {
						window.location = '<?php echo base_url(); ?>';
					}
					<?php				
				}
				?>
				$( this ).dialog( "close" );
			}
		}
	});	
	
	$("#status").fadeOut();
	$("#preloader").delay(1000).fadeOut("slow");	
});
</script>
	
<!-- error messages starts here --> 
<?php include_once('_error_messages.php'); ?>
<?php include_once('_form_messages.php'); ?>
<!-- error messages ends here --> 

<title><?php echo PAGE_TITLE; ?></title>	
</head>
<div id="preloader"><div id="status">&nbsp;</div></div>
<body>

<!-- wrap starts here -->
<div id="wrap">

		<!-- header -->
		<div id="header">			
			<span id="slogan"><?php echo COMPANY_NAME; ?></span>
			<!-- tabs -->
			<?php if (isset($top_menu)) { include_once('menu_header.php'); } ?>
		</div>
		
		<div id="header-logo">			
			<div id="logo"><?php echo SYSTEM_NAME; ?><span class="red"> SYSTEM</span></div>     
		</div>
        
        <!-- Side Menu -->

        <?php
			include_once('menu_side.php');
		?>
		
        <!-- Main Body -->
        