<div id="sidebar" >		
	<?php
	if (!$this->model_check_login->CheckSession())					
	{
		include_once('_authenticate.php');
		?>
		<h1>Sign-in</h1>
		<div class='left-box'>
			<form id="frm_chk_login">
				<p>
				<label>Username</label>
				<input name='username' id='uname' type='text' /> 
				<label>Password</label>
				<input name='password' id='passwd' type='password' />
				<br /><br />
				<?php if (isset($captcha['image'])) { echo $captcha['image']; }?>
				<input name='login_img_captcha' id='login_img_captcha' type='text'/>
				<label>*Please enter the text you see above.</label>            
				<br />
				<input type="button" value="Login" id="but_login" name="but_login" /> 
				</p>		
			</form>
		</div>  
		<?php
	}
	else
	{
		?>
        <h1>Welcome!</h1>
        <div class='left-box'><center><b><?php echo ucwords(strtolower($this->session->userdata('fullname'))); ?></b>
        <br>
        <script type='text/javascript'>
			var currenttime = '<?php echo date('F d, Y H:i:s', time()) ?>'
			var montharray=new Array('January','February','March','April','May','June','July','August','September','October','November','December')
			var serverdate=new Date(currenttime)
			function padlength(what)
			{
				var output=(what.toString().length==1)? '0'+what : what
				return output
			}
			function displaytime()
			{
				serverdate.setSeconds(serverdate.getSeconds()+1)
				var datestring=montharray[serverdate.getMonth()]+' '+padlength(serverdate.getDate())+', '+serverdate.getFullYear()
				var timestring=padlength(serverdate.getHours())+':'+padlength(serverdate.getMinutes())+':'+padlength(serverdate.getSeconds())
				document.getElementById('servertime').innerHTML=datestring+' '+timestring
			}
			window.onload=function()
			{
				setInterval('displaytime()', 1000)
			}
		</script>
			<span id='servertime' class='standard'></span>	
			<br>
			[<a href='<?php echo base_url()."controller_user_public/"; ?>'>EDIT PROFILE</a>]
			[<a href='<?php echo base_url() . "controller_main/logout"; ?>'>LOG OUT</a>]</center>
        </div>
        <h1>Home</h1> 
		<div class='left-box'>
            <ul class='sidemenu'>
                <li><a href='<?php echo base_url(); ?>'>HOME</a></li>
            </ul>
		</div>
        <?php	
	 	echo $this->session->userdata('u_links');
	}
	?> 
</div>