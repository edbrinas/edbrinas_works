<?php
if(!empty($display_message))
{
    ?>
	<div id="sys-message" style="display: none">
	  <p>
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
		<?php echo $display_message; ?>
	  </p>
	</div>
    <?php
	die;
}
?>