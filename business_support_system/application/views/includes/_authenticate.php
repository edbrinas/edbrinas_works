<script type="text/javascript">

$(document).ready(function() 
{
    var msg_req = "<font color='#FF0000'>Required field.</font>";

    $("#sys-frm-message").dialog({
        autoOpen: false,
        modal: true,
        title: "System Message",
        buttons: {
            Ok: function () {
                $(this).dialog('close');
				location.reload();
            }
        }
    });
	
	$("#frm_chk_login").validate({
		rules: {
			username: {
				required: true
			},
			password: {
				required: true
			},
			login_img_captcha: {
				required: true
			}		   
		},
		messages: {
			username: {
				required: msg_req
			},
			password: {
				required: msg_req
			},
			login_img_captcha: {
				required: msg_req
			}
		}
	});

	$("#but_login").click(function(){
		$(this).attr("disabled", true);
		if($("#frm_chk_login").valid()) 
		{
			$.ajax({type: "POST",
					url: "<?php echo base_url().'controller_main/CheckLogIn'; ?>",
					data: $("#frm_chk_login").serialize(),
					success:function(response)
					{
						$(".sys-frm-inner-message").empty();
						$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-lightbulb' style='float:left; margin:0 7px 50px 0;' />" + response + "</span></p>");
						$("#sys-frm-message").dialog("open");
					},
					error:function (xhr, ajaxOptions, thrownError)
						{
							$(".sys-frm-inner-message").empty();
							$(".sys-frm-inner-message").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 50px 0;' />" + thrownError + "</span></p>");
							$("#sys-frm-message").dialog("open");
						}						
					});	
		}
		else
		{
			$(this).attr("disabled", false);
		}
	});		
});
</script>


