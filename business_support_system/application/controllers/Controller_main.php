<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_main extends CI_Controller {
    
	public function __construct() 
	{
        parent::__construct();
		$this->load->model('model_sys_log');
		$this->load->model('model_users');
		$this->load->model('model_pages');
		$this->load->model('model_check_login');
		$this->load->library('lib_utilities');
		
		header('Access-Control-Allow-Origin: *');
    }
	
	public function index()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha()); 
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{		
			$data['u_links'] = substr($this->_pLinks(0,1),0,-11);
			$data['show_graphs'] = true;
			$this->load->view('pages/_default/_default_page',$data);	
		}
	}
	
	public function CheckLogIn()
	{	
		if(isset($_POST) && $_POST) {	
			if (strtoupper($_POST['login_img_captcha'])==strtoupper($this->session->userdata('word_captcha'))) {
				$this->session->unset_userdata('word_captcha');
				$a_user_info = $this->model_users->GetRecordInfo($_POST);
				if (!empty($a_user_info)) {
					if ($a_user_info->status=='D') {
						echo $this->lib_utilities->GetErrorMsg("0x8Sys");
					}	
					else {
						$u_links = substr($this->_pLinks(0,$a_user_info->id),0,-11);
						$this->session->set_userdata('u_links',$u_links);
						$this->session->set_userdata('usr_id',$a_user_info->id);
						$this->session->set_userdata('username',$a_user_info->username);
						$this->session->set_userdata('fullname',$a_user_info->f_name." ".$a_user_info->l_name);
						$this->session->set_userdata('sessionlogged',TRUE);	
						$this->session->set_userdata('encryption_key',   $this->lib_utilities->GenerateRandomChars());
						$uname = $this->session->userdata('username');
						$uip = $this->lib_utilities->GetIP();
						$this->model_sys_log->AddTLog(array('username'=>$uname,'activity'=>'LOGGED TO SYSTEM','ip_address'=>$uip));
						echo "Welcome ".$this->lib_utilities->ToSentenceCase($a_user_info->f_name)." ".$this->lib_utilities->ToSentenceCase($a_user_info->l_name)."!";						
					}
				}
				else {
					echo $this->lib_utilities->GetErrorMsg("0x3Sys");
				}	
			}
			else {
				$this->session->unset_userdata('word_captcha');
				echo $this->lib_utilities->GetErrorMsg("0x4Sys");
			}
		}
		else {
			$this->session->unset_userdata('word_captcha');
			echo $this->lib_utilities->GetErrorMsg("0x0E");
		}	
	}
	
	public function logout()
	{	
		$uname = $this->session->userdata('username');
		$uip = $this->lib_utilities->GetIP();
		$this->model_sys_log->AddTLog(array('username'=>$uname,'activity'=>'LOGGED OUT TO SYSTEM','ip_address'=>$this->lib_utilities->GetIP()));
		$this->session->sess_destroy(); 
		$data['display_message'] = $this->lib_utilities->GetErrorMsg("0x5Sys");
		$this->load->view('pages/_default/_default_page',$data);
	}

	public function AccessDenied()
	{	
		$uname = $this->session->userdata('username');
		$uip = $this->lib_utilities->GetIP();
		$this->model_sys_log->AddTLog(array('username'=>$uname,'activity'=>'ACCESS DENIED','ip_address'=>$this->lib_utilities->GetIP()));
		$this->session->sess_destroy(); 
		$data['display_message'] = $this->lib_utilities->GetErrorMsg("0x6Sys");
		$this->load->view('pages/_default/_default_page',$data);
	}

	public function SessionTimedOut()
	{	
		$uname = $this->session->userdata('username');
		$uip = $this->lib_utilities->GetIP();
		$this->model_sys_log->AddTLog(array('username'=>$uname,'activity'=>'SESSION TIMED OUT','ip_address'=>$this->lib_utilities->GetIP()));
		$this->session->sess_destroy(); 
		$data['display_message'] = $this->lib_utilities->GetErrorMsg("0x7Sys");
		$this->load->view('pages/_default/_default_page',$data);
	}	

	public function MissingPage()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha()); 
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->load->view('pages/_default/_default');	
		}		
	}

	private function _pLinks($parent_id,$u_id)
	{
		$op_link = "";
		$page_url = base_url();
		$rows = $this->model_pages->GetAllPages($parent_id);
		foreach ($rows as $data)
		{
			if (!empty($u_id)) :
				$w_access = $this->model_pages->CheckAccess(array('user_id'=>$u_id,'page_id'=>$data['id']));
				if ($w_access) :
					if($data['p_id']==0) :
						$op_link .= "<h1>{$data['p_name']}</h1>";
						$op_link .= "<div class='left-box'>";
						$op_link .= "<ul class='sidemenu'>";
					else:
						$op_link .= "<li><a href='{$page_url}{$data['p_link']}'>{$data['p_name']}</a></li>";
					endif;
					$child_rows = $this->model_pages->GetAllPages($data['id']);	
					if ($child_rows):
						if ($data['id']!=$parent_id) : $op_link .= $this->_pLinks($data['id'],$u_id); endif;
					endif;		
				endif;
			endif;
		}
		$op_link .= "</ul></div>";
		return $op_link;
	}
}

/* End of file controller_main.php */
/* Location: ./application/controllers/controller_main.php */
