<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_sys_log extends CI_Controller {

	private $controller_main = 'Controller_sys_log/';
	private $module_name = 'SYSTEM LOGS';
	private $report_name = 'SYSTEM LOG DETAILS';
		
	public function __construct()
	{
        parent::__construct();
		$this->load->model('model_sys_log');
		$this->load->library('lib_utilities');
		$this->load->library('datatables');
		$this->load->model('model_check_login');
		$this->load->model('model_sys_log');
		$this->load->model('model_pages');
		header('Access-Control-Allow-Origin: *');

		if ($this->model_check_login->CheckSession())
		{
			$a_pid = $this->model_pages->GetPidByController($this->controller_main); 
			$with_access = $this->model_pages->CheckAccess(array('user_id'=>$this->session->userdata('usr_id'),'page_id'=>$a_pid->id));
			if (empty($with_access)) {
				redirect(base_url()."controller_main/AccessDenied", 'refresh'); 
			}		
		}
		else
		{
			redirect(base_url()."controller_main/SessionTimedOut", 'refresh'); 
		}		
    }
	
	public function GetAllRecords()
	{
		$pop_up_config = $this->lib_utilities->PopUpConfiguration();		
		$sLimit = intval($_GET['iDisplayLength']);
		$sOff   = intval($_GET['iDisplayStart']) > 0 ? intval($_GET['iDisplayStart']) : "";
		$searchable_items = array('transaction_date','username','activity','ip_address');
		$sortable_items = array('transaction_date','username','activity','ip_address','id');

		$sWhere = NULL;

		if(isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
			$sWhere = " WHERE (";
			foreach($searchable_items as $field) {
				$sWhere .= " {$field} LIKE '%{$_GET['sSearch']}%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}

		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$sortable_items[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'ASC' : 'DESC') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = NULL;
			}
		}

		$arr_dta = $this->model_sys_log->GetAllRecords($sLimit,$sOff,$sWhere,$sOrder);
		$iTotalRec = $this->model_sys_log->GetFoundRows();
		$iTotalDispRec = $this->model_sys_log->GetTotalRows();

		$response = array(	'sEcho' => $_GET['sEcho'],
							'iTotalRecords' => $iTotalRec,
							'iTotalDisplayRecords' => $iTotalDispRec,
							'aaData' => array());
		foreach ($arr_dta as $data)
		{			
			unset($o_link);
			$o_link .= 	"&nbsp;".anchor_popup(base_url().$this->controller_main.'ViewRecord/'.$data['id'], '<img src="'.base_url().'_plugins/_css/main_images/b_browse.png" title="VIEW RECORD" width="16" height="16" />', $pop_up_config);
			$response['aaData'][] = array($data['transaction_date'],$data['username'],$data['activity'],$data['ip_address'],$o_link);
		}
		echo json_encode($response);
	}	


	public function index()
	{
		$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[SYS LOGS] VIEWED MASTERLIST','ip_address'=>$this->lib_utilities->GetIP()));
		$data['module_name'] = $this->module_name;
		$data['controller_main'] = $this->controller_main;		
		$this->load->view('pages/sys_log/_list',$data);
	}

	public function ViewRecord($id)
	{
		$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[SYS LOGS] VIEWED RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
		$data['a_records'] = $this->model_sys_log->GetRecById($id);
		$data['report_name'] = $this->report_name;;
		$data['controller_main'] = $this->controller_main;
		$this->load->view('pages/sys_log/_view',$data);
	}	
}

/* End of file controller_sys_log.php */
/* Location: ./application/controllers/controller_sys_log.php */