<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_asset extends CI_Controller {

	private $controller_main = 'Controller_asset/';
	private $module_name = 'ASSET MANAGEMENT';
	private $report_name = 'ASSET MANAGEMENT DETAILS';

	public function __construct()
	{
        parent::__construct();
		$this->load->model('model_sys_log');
		$this->load->library('lib_utilities');
		$this->load->library('datatables');
		$this->load->model('model_check_login');
		$this->load->model('model_sys_log');
		$this->load->model('model_pages');
		$this->load->model('model_asset');
		$this->load->model('model_users');
		header('Access-Control-Allow-Origin: *');

		if ($this->model_check_login->CheckSession())
		{
			$a_pid = $this->model_pages->GetPidByController($this->controller_main);
			$with_access = $this->model_pages->CheckAccess(array('user_id'=>$this->session->userdata('usr_id'),'page_id'=>$a_pid->id));
			if (empty($with_access)) {
				redirect(base_url()."controller_main/AccessDenied", 'refresh');
			}
		}
		else
		{
			redirect(base_url()."controller_main/SessionTimedOut", 'refresh');
		}

		$this->eKey = $this->session->userdata('encryption_key');
	}

	public function GetAllRecords()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$pop_up_config = $this->lib_utilities->PopUpConfiguration();
			$sLimit = intval($_GET['iDisplayLength']);
			$sOff   = intval($_GET['iDisplayStart']) > 0 ? intval($_GET['iDisplayStart']) : "";
			$searchable_items = array('model_cat','model_name','model_manu','serial_num','asset_tag','asset_state','assigned_to','asset_class','asset_loc','asset_dept','assigned_date','installed_date','decommissioned_date','system_specs');
			$sortable_items = array('model_name','asset_tag','model_manu','model_cat','serial_num','asset_loc','asset_state','assigned_to','id');

			$sWhere = NULL;

			if(isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
				$sWhere = "AND (";
				foreach($searchable_items as $field) {
					$sWhere .= " {$field} LIKE '%{$_GET['sSearch']}%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				$sWhere .= ')';
			}

			$sOrder = "";
			if ( isset( $_GET['iSortCol_0'] ) )
			{
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
					if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
					{
						$sOrder .= "".$sortable_items[ intval( $_GET['iSortCol_'.$i] ) ]." ".
							($_GET['sSortDir_'.$i]==='asc' ? 'ASC' : 'DESC') .", ";
					}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = NULL;
				}
			}

			$arr_dta = $this->model_asset->GetAllRecords($sLimit,$sOff,$sWhere,$sOrder);
			$iTotalRec = $this->model_asset->GetFoundRows();
			$iTotalDispRec = $this->model_asset->GetTotalRows();

			$response = array(	'sEcho' => $_GET['sEcho'],
								'iTotalRecords' => $iTotalRec,
								'iTotalDisplayRecords' => $iTotalDispRec,
								'aaData' => array());

			foreach ($arr_dta as $data)
			{
				unset($o_link);
				$o_link .= '&nbsp;<a href="'.base_url().$this->controller_main.'EditRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey).'">
						<img src="'.base_url().'_plugins/_css/main_images/b_edit.png" title="EDIT RECORD" width="16" height="16" />
						</a>';

				$o_link .= 	"&nbsp;".anchor_popup(base_url().$this->controller_main.'ViewRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey), '<img src="'.base_url().'_plugins/_css/main_images/b_browse.png" title="VIEW RECORD" width="16" height="16" />', $pop_up_config);
				$response['aaData'][] = array($data['model_name'],$data['asset_tag'],$data['model_manu'],$data['model_cat'],$data['serial_num'],$data['asset_loc'],$data['asset_state'],$data['assigned_to'],$o_link);
			}

			echo json_encode($response);
		}
	}


	public function index()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[ASSET] VIEWED MASTERLIST','ip_address'=>$this->lib_utilities->GetIP()));
			$top_menu = '<ul>';
				$top_menu .= '<li><a href="'.base_url().$this->controller_main.'AddRecord"><span>ADD NEW RECORD</span></a></li>';
			$top_menu .= '</ul>';
			$data['top_menu'] = $top_menu;
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/asset/_list',$data);
		}
	}

	public function AddRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[ASSET] VIEWED ADD RECORD','ip_address'=>$this->lib_utilities->GetIP()));
			$data['a_sites'] = $this->lib_utilities->GetSites();
			$data['a_cat'] = $this->lib_utilities->GetAssetCat();
			$data['a_manu'] = $this->lib_utilities->GetAssetManu();
			$data['a_state'] = $this->lib_utilities->GetAssetState();
			$data['a_class'] = $this->lib_utilities->GetAssetClass();
			$data['a_dept'] = $this->lib_utilities->GetDepartments();
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/asset/_add',$data);
		}
	}

	public function CreateRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				$if_exist = $this->model_asset->CheckIfExist($_POST);
				if ($if_exist==0 || strtoupper($_POST['serial_num']) == 'N/A')
				{
					$_POST = array_merge($_POST,array('last_update_by'=>$this->session->userdata('username')));
					$t_id = $this->model_asset->AddRecord($_POST);
					$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[ASSET] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
					echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x0S") : $this->lib_utilities->GetErrorMsg("0x1E");
				}
				else
				{
					echo $this->lib_utilities->GetErrorMsg("0x2E");
				}
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function EditRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);		
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[ASSET] VIEWED EDIT RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$data['a_sites'] = $this->lib_utilities->GetSites();
			$data['a_cat'] = $this->lib_utilities->GetAssetCat();
			$data['a_manu'] = $this->lib_utilities->GetAssetManu();
			$data['a_state'] = $this->lib_utilities->GetAssetState();
			$data['a_class'] = $this->lib_utilities->GetAssetClass();
			$data['a_dept'] = $this->lib_utilities->GetDepartments();
			$a_records = $this->model_asset->GetRecById($id);

			if ($a_records->assigned_to) {
				$a_asTo = explode(",",$a_records->assigned_to);
				foreach ($a_asTo as $asTo) {
					$aUser = $this->model_users->GetRecordByUname($asTo);
					$aPrePop[] = array ('id'=>strtoupper($aUser->username),'name'=>strtoupper($aUser->f_name).' '.strtoupper($aUser->m_name).' '.strtoupper($aUser->l_name));
				}
				
				$data['a_pre_pop'] = json_encode($aPrePop);;				
			}

			$data['a_records'] = $a_records;
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/asset/_edit',$data);
		}
	}

	public function ModifyRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				$t_id = $this->model_asset->UpdateRecord($_POST);
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[ASSET] MODIFIED RECORD ID: '.$_POST['id'],'ip_address'=>$this->lib_utilities->GetIP()));
				echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x1S") : $this->lib_utilities->GetErrorMsg("0x1E");
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function ViewRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);		
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[ASSET] VIEWED RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$data['a_records'] = $this->model_asset->GetRecById($id);
			$data['report_name'] = $this->report_name;;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/asset/_view',$data);
		}
	}

	public function GetUsers()
	{
		$this->session->unset_userdata('word_captcha');
		if ($this->model_check_login->CheckSession())
		{
			$arr_dta = $this->model_users->GetAllRecord($sLimit,$sOff,$sWhere,$sOrder);
			foreach ($arr_dta as $data)
			{
				$arr_data[] = array( 	'id' => 	strtoupper($data['username']),
										'name' =>	strtoupper($data['f_name']).' '.
													strtoupper($data['m_name']).' '.
													strtoupper($data['l_name']) );
			}
			echo json_encode($arr_data);
		}
	}	
}

/* End of file controller_asset.php */
/* Location: ./application/controllers/controller_asset.php */