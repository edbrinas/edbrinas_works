<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_im extends CI_Controller {

	private $controller_main = 'Controller_im/';
	private $module_name = 'INCIDENT MANAGEMENT';
	private $report_name = 'INCIDENT MANAGEMENT DETAILS';

	public function __construct()
	{
        parent::__construct();
		$this->load->model('model_sys_log');
		$this->load->library('lib_utilities');
		$this->load->library('datatables');
		$this->load->model('model_check_login');
		$this->load->model('model_sys_log');
		$this->load->model('model_pages');
		$this->load->model('model_im');
		$this->load->model('model_sr');
		$this->load->model('model_users');

		header('Access-Control-Allow-Origin: *');

		if ($this->model_check_login->CheckSession())
		{
			$a_pid = $this->model_pages->GetPidByController($this->controller_main);
			$with_access = $this->model_pages->CheckAccess(array('user_id'=>$this->session->userdata('usr_id'),'page_id'=>$a_pid->id));
			if (empty($with_access)) {
				redirect(base_url()."controller_main/AccessDenied", 'refresh');
			}
		}
		else
		{
			redirect(base_url()."controller_main/SessionTimedOut", 'refresh');
		}

		$this->eKey = $this->session->userdata('encryption_key');
		
		$this->top_menu = '	<ul>
								<li><a href="'.base_url().$this->controller_main.'AddRecord"><span>ADD NEW RECORD</span></a></li>
							</ul>';
    }

	public function GetAllRecords()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$pop_up_config = $this->lib_utilities->PopUpConfiguration();
			$sLimit = intval($_GET['iDisplayLength']);
			$sOff   = intval($_GET['iDisplayStart']) > 0 ? intval($_GET['iDisplayStart']) : "";
			$searchable_items = array('s_num','opened_date','closed_date','rec_stat','rec_type','client_name','created_by','prio_level','rec_site','detail_desc','problem_desc','rec_attach');
			$sortable_items = array('s_num','created_by','opened_date','detail_desc','rec_stat','prio_level','closed_date','id');

			$sWhere = NULL;

			if(isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
				$sWhere = "AND (";
				foreach($searchable_items as $field) {
					$sWhere .= " {$field} LIKE '%{$_GET['sSearch']}%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				$sWhere .= ')';
			}

			$sOrder = "";
			if ( isset( $_GET['iSortCol_0'] ) )
			{
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
					if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
					{
						$sOrder .= "".$sortable_items[ intval( $_GET['iSortCol_'.$i] ) ]." ".
							($_GET['sSortDir_'.$i]==='asc' ? 'ASC' : 'DESC') .", ";
					}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = NULL;
				}
			}

			$arr_dta = $this->model_im->GetAllRecords($sLimit,$sOff,$sWhere,$sOrder);
			$iTotalRec = $this->model_im->GetFoundRows();
			$iTotalDispRec = $this->model_im->GetTotalRows();

			$response = array(	'sEcho' => $_GET['sEcho'],
								'iTotalRecords' => $iTotalRec,
								'iTotalDisplayRecords' => $iTotalDispRec,
								'aaData' => array());

			foreach ($arr_dta as $data) {
				unset($o_link);
				unset($arr_ddta);

				$eDate = ($data['closed_date']=='0000-00-00 00:00:00') ? date("Y-m-d h:i:s") : $data['closed_date'];
				$iTime = $this->lib_utilities->GetDateTimeLapsed($data['opened_date'], $eDate);
				$iDays = $this->lib_utilities->GetDays($data['opened_date'], $eDate);


				$o_link .= ($data['rec_stat']!='CLOSED') ? '&nbsp;<a href="'.base_url().$this->controller_main.'EditRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey).'">
						<img src="'.base_url().'_plugins/_css/main_images/b_edit.png" title="EDIT RECORD" width="16" height="16" />
						</a>' : null;

				$o_link .= 	"&nbsp;".anchor_popup(base_url().$this->controller_main.'ViewRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey), '<img src="'.base_url().'_plugins/_css/main_images/b_browse.png" title="VIEW RECORD" width="16" height="16" />', $pop_up_config);
				$response['aaData'][] = array(  $data['s_num'],
												$data['created_by'],
												$data['opened_date'],
												$data['detail_desc'],
												$data['rec_stat'],
												$data['prio_level'],
												$iDays.' Day(s) <br /> and '.$iTime.' Hour(s)',$o_link);
			}
			echo json_encode($response);
		}
	}

	public function index()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] VIEWED MASTERLIST','ip_address'=>$this->lib_utilities->GetIP()));
			$data['top_menu'] = $this->top_menu;
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/incident/_list',$data);
		}
	}

	public function AddRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] VIEWED ADD RECORD','ip_address'=>$this->lib_utilities->GetIP()));
			$data['top_menu'] = $this->top_menu;
			$data['a_ir_type'] = $this->lib_utilities->GetIrType();
			$data['a_prio'] = $this->lib_utilities->GetIrPriority();
			$data['a_sites'] = $this->lib_utilities->GetSites();
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/incident/_add',$data);
		}
	}

	public function CreateRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
			echo $this->lib_utilities->GetErrorMsg("0x1Sys");
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				$arr_dta = $this->model_im->GetAllRecords($sLimit=1,$sOff=NULL,' WHERE DATE_FORMAT(opened_date,"%Y-%m") = DATE_FORMAT(CURRENT_DATE,"%Y-%m") ',$sOrder=NULL);
				$tick_num = $arr_dta[0]['tick_num']+1;
				unset($arr_dta);

				if(isset($_FILES) && $_FILES) {
					$a_type = $this->lib_utilities->CheckFileType($_FILES);
					if (!empty($a_type)) {
						if($this->lib_utilities->CheckFileSize($_FILES)) {
							if($this->lib_utilities->UploadFile($_FILES)) {
								$f_name = $_FILES["file_to_upload"]["name"];
							}
							else {
								$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] ERROR UPLOADING FILE.','ip_address'=>$this->lib_utilities->GetIP()));
								$f_name = null;
							}
						}
						else {
							$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] INVALID FILE SIZE.','ip_address'=>$this->lib_utilities->GetIP()));
							$f_name = null;
						}
					}
					else {
						$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] INVALID FILE TYPE.','ip_address'=>$this->lib_utilities->GetIP()));
						$f_name = null;
					}
				}
				else {
					$f_name = null;
				}

				$_POST = array_merge($_POST, array(	's_num'=>"IR-".date("Y-m").'-'.sprintf("%05d",$tick_num),
													'tick_num'=>$tick_num,
													'opened_date'=>date("Y-m-d h:i:s"),
													'rec_stat'=>'OPEN',
													'created_by'=>$this->session->userdata('username'),
													'rec_type'=>$_POST['ir_type'].' - '.$_POST['sub_ir_type'],
													'rec_attach'=>$f_name ));
				$t_id = $this->model_im->AddRecord($_POST);
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
				$_POST = array_merge($_POST,array('ir_id'=>$t_id,'updated_by'=>$this->session->userdata('username'),'update_date'=>date("Y-m-d h:i:s")));
				$t_id = $this->model_im->AddRecordDetails($_POST);
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT-DETAILS] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
				
				if ($_POST['create_sr']=='YES') {
					unset($tick_num);
					unset($_POST['rec_type']);
					unset($_POST['update_date']);
					unset($_POST['prio_level']);
					$arr_dta = $this->model_sr->GetAllRecords($sLimit=1,$sOff=NULL,' WHERE DATE_FORMAT(opened_date,"%Y-%m") = DATE_FORMAT(CURRENT_DATE,"%Y-%m") ',$sOrder=NULL);
					$tick_num = $arr_dta[0]['tick_num']+1;
					$_POST = array_merge($_POST, array('ir_id'=>$t_id,'tick_num'=>$tick_num,'s_num'=>"SR-".date("Y-m").'-'.sprintf("%05d",$tick_num)));
					unset($t_id);
					$t_id = $this->model_sr->AddRecord($_POST);
					$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT-SERVICE REQUEST] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
					$_POST = array_merge($_POST,array('sr_id'=>$t_id,'update_date'=>date("Y-m-d h:i:s")));
					$t_id = $this->model_sr->AddRecordDetails($_POST);	
					$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT-SERVICE REQUEST - DETAILS] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));					
					echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x5S") : $this->lib_utilities->GetErrorMsg("0x1E");
				}
				else {
					echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x0S") : $this->lib_utilities->GetErrorMsg("0x1E");
				}
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function EditRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);		
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] VIEWED EDIT RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$a_records = $this->model_im->GetRecById($id);
			$arr_ddta = $this->model_im->GetAllRecordDetails($sLimit,$sOff,' WHERE ir_id = '.$a_records->id.' ',$sOrder);

			$iUpdates = "<ul>";
				foreach ($arr_ddta as $ddata) {
					$iUpdates .= '<li> <b>[ '.$ddata['updated_by'].' @ '.$ddata['update_date'].' ]</b> '.$ddata['action_desc'].'</li>';
				}
			$iUpdates .= "</ul>";
			$data['iUpdates'] = $iUpdates;
			$data['a_records'] = $a_records;
			$data['top_menu'] = $this->top_menu;
			$data['a_ir_stat'] = $this->lib_utilities->GetIrStat();
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/incident/_edit',$data);
		}
	}

	public function ModifyRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
			echo $this->lib_utilities->GetErrorMsg("0x1Sys");
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				if(isset($_FILES) && $_FILES) {
					$a_type = $this->lib_utilities->CheckFileType($_FILES);
					if (!empty($a_type)) {
						if($this->lib_utilities->CheckFileSize($_FILES)) {
							if($this->lib_utilities->UploadFile($_FILES)) {
								$f_name = $_FILES["file_to_upload"]["name"];
							}
							else {
								$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] ERROR UPLOADING FILE.','ip_address'=>$this->lib_utilities->GetIP()));
								$f_name = null;
							}
						}
						else {
							$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] INVALID FILE SIZE.','ip_address'=>$this->lib_utilities->GetIP()));
							$f_name = null;
						}
					}
					else {
						$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] INVALID FILE TYPE.','ip_address'=>$this->lib_utilities->GetIP()));
						$f_name = null;
					}
				}
				else {
					$f_name = null;
				}

				if ($_POST['rec_stat']=='CLOSED') {
					$_POST = array_merge($_POST, array('closed_date'=>date("Y-m-d h:i:s")));
				}

				$_POST = array_merge($_POST, array('updated_by'=>$this->session->userdata('username'),'rec_attach'=>$f_name,'update_date'=>date("Y-m-d h:i:s"),'ir_id'=>$_POST['id']));
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] MODIFIED RECORD ID: '.$_POST['id'],'ip_address'=>$this->lib_utilities->GetIP()));
				$t_id = $this->model_im->UpdateRecord($_POST);
				$t_id = $this->model_im->AddRecordDetails($_POST);
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT-DETAILS] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
				echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x1S") : $this->lib_utilities->GetErrorMsg("0x1E");
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function ViewRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
			$data['display_message'] = $this->lib_utilities->GetErrorMsg("0x1Sys");
			$this->load->view('pages/incident/_view',$data);
		}
		else {
			$id = $this->lib_utilities->decrypt($id,$this->eKey);		
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] VIEWED RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$a_records = $this->model_im->GetRecById($id);
			$eDate = ($a_records->closed_date=='0000-00-00 00:00:00') ? date("Y-m-d h:i:s") : $a_records->closed_date;
			$iTime = $this->lib_utilities->GetDateTimeLapsed( $a_records->opened_date, $eDate);
			$iDays = $this->lib_utilities->GetDays( $a_records->opened_date, $eDate);
			$arr_ddta = $this->model_im->GetAllRecordDetails($sLimit,$sOff,' WHERE ir_id = '.$a_records->id.' ',$sOrder);
			$iUpdates = "<ul>";
				foreach ($arr_ddta as $ddata) {
					$iUpdates .= '<li> <b>[ '.$ddata['updated_by'].' @ '.$ddata['update_date'].' ]</b> '.$ddata['action_desc'].'</li>';
				}
			$iUpdates .= "</ul>";
			$data['iUpdates'] = $iUpdates;
			$data['ieDays'] = $iDays.' Day(s) '.$iTime.' Hour(s)';
			$data['a_records'] = $a_records;
			$data['report_name'] = $this->report_name;;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/incident/_view',$data);
		}
	}

	public function RemoveFile()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
			$data['display_message'] = $this->lib_utilities->GetErrorMsg("0x1Sys");
			$this->load->view('pages/incident/_view',$data);
		}
		else {
			if(isset($_POST) && $_POST)
			{
				$a_records = $this->model_im->GetRecById($_POST['id']);
				$this->lib_utilities->DeleteFile(array('file_name'=>$a_records->rec_attach));
				$t_id = $this->model_im->RemoveFile($_POST);
				$items = "<p><input type='file' name='file_to_upload' id='file_to_upload' /></p>";
				echo !empty($items) ? $items : null;
			}
		}
	}

	public function GetReqFields()
	{
		$this->session->unset_userdata('word_captcha');
		if ($this->model_check_login->CheckSession())
		{
			$a_ir_sub_type = $this->lib_utilities->GetIrSubType();
			$items = "<p>";
			$items .= "<label>SUB-CATEGORY</label>";
			$items .= "<select name='sub_ir_type' id='sub_ir_type'>";
				$items .= "<option value=''>=== PLEASE SELECT ===</option>";
				foreach($a_ir_sub_type[$_POST["ir_type"]] as $data)
				{
					$items .= "<option value='{$data}'>{$data}</option>";
				}
			$items .= "</select>";
			$items .= "</p>";
			echo $items;
		}
	}

	public function GetUsers()
	{
		$this->session->unset_userdata('word_captcha');
		if ($this->model_check_login->CheckSession())
		{
			$arr_dta = $this->model_users->GetAllRecord($sLimit,$sOff,$sWhere,$sOrder);
			foreach ($arr_dta as $data)
			{
				$arr_data[] = array( 	'id' => 	strtoupper($data['username']),
										'name' =>	strtoupper($data['f_name']).' '.
													strtoupper($data['m_name']).' '.
													strtoupper($data['l_name']) );
			}
			echo json_encode($arr_data);
		}
	}
}

/* End of file controller_im.php */
/* Location: ./application/controllers/controller_im.php */