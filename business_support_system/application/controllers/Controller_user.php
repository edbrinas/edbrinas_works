<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_user extends CI_Controller {

	private $controller_main = 'Controller_user/';
	private $module_name = 'SYSTEM ACCESS';
	private $report_name = 'SYSTEM ACCESS DETAILS';
		
	public function __construct()
	{
        parent::__construct();
		$this->load->model('model_sys_log');
		$this->load->library('lib_utilities');
		$this->load->library('datatables');
		$this->load->model('model_check_login');
		$this->load->model('model_users');
		$this->load->model('model_sys_log');
		$this->load->model('model_pages');
    }
	

	public function GetAllRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{		
			$is_admin = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),1);
			$access_level_4 = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),4);
			$pop_up_config = $this->lib_utilities->PopUpConfiguration();	
			$sLimit = intval($_GET['iDisplayLength']);
			$sOff   = intval($_GET['iDisplayStart']) > 0 ? intval($_GET['iDisplayStart']) : "";
			$searchable_items = array('username','f_name','m_name','status','type');
			$sortable_items = array('username','f_name','m_name','l_name','type','status','id');

			$sWhere = NULL;

			if(isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
				$sWhere = "AND (";
				foreach($searchable_items as $field) {
					$sWhere .= " {$field} LIKE '%{$_GET['sSearch']}%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				$sWhere .= ')';
			}

			$sOrder = "";
			if ( isset( $_GET['iSortCol_0'] ) )
			{
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
					if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
					{
						$sOrder .= "".$sortable_items[ intval( $_GET['iSortCol_'.$i] ) ]." ".
							($_GET['sSortDir_'.$i]==='asc' ? 'ASC' : 'DESC') .", ";
					}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = NULL;
				}
			}

			$arr_dta = $this->model_users->GetAllRecord($sLimit,$sOff,$sWhere,$sOrder);
			$iTotalRec = $this->model_users->GetFoundRows();
			$iTotalDispRec = $this->model_users->GetTotalRows();

			$response = array(	'sEcho' => $_GET['sEcho'],
								'iTotalRecords' => $iTotalRec,
								'iTotalDisplayRecords' => $iTotalDispRec,
								'aaData' => array());
								
			foreach ($arr_dta as $data)
			{
				$status=($data['status']=='A') ? "ACTIVE" : "DEACTIVATED";	
				$a_paccess = $this->model_users->GetAccessType($data['id']);
				$a_types = $this->lib_utilities->GetAccountTypes();
				
				$type = "<ul>";
				foreach ($a_paccess AS $types)
				{
					$type .=  "<li>".$a_types[$types['access_id']]."</li>";
				}
				$type .= "<ul>";
							
				$e_link = (!empty($is_admin) || !empty($access_level_4)) ? '<a href="'.base_url().$this->controller_main.'EditRecord/'.$data['id'].'"><img src="'.base_url().'_plugins/_css/main_images/b_edit.png" title="EDIT RECORD" width="16" height="16" /></a>' : "&nbsp;";
				$e_link .= "&nbsp;".anchor_popup(base_url().$this->controller_main.'ViewRecord/'.$data['id'], '<img src="'.base_url().'_plugins/_css/main_images/b_browse.png" title="VIEW RECORD" width="16" height="16" />', $pop_up_config);
				$response['aaData'][] = array($data['username'],$data['f_name'],$data['m_name'],$data['l_name'],$type,$status,$e_link);
			}

			echo json_encode($response);
		}
	}	


	public function index()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$is_admin = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),1);
			$access_level_2 = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),4);
		
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[USER] VIEWED MASTERLIST','ip_address'=>$this->lib_utilities->GetIP()));
			if (!empty($is_admin) || !empty($access_level_2)) 
			{
				$top_menu = '<ul>';
					$top_menu .= '<li><a href="'.base_url().$this->controller_main.'AddRecord"><span>ADD NEW RECORD</span></a></li>';
				$top_menu .= '</ul>';
				$data['top_menu'] = $top_menu;
			}
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;		
			$this->load->view('pages/user/_list',$data);
		}
	}

	public function AddRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[USER] VIEWED ADD RECORD','ip_address'=>$this->lib_utilities->GetIP()));
			$a_types = $this->lib_utilities->GetAccountTypes();
			foreach ($a_types AS $key=>$value)
			{
				$data['a_type'] .=  '<input type="checkbox" name="type[]" id="type[]" value="'.$key.'" />&ensp;'.$value.'<br />';
			}
			$data['a_dept'] = $this->lib_utilities->GetDepartments();
			$data['module_name'] = $this->module_name;
			$data['p_links'] = $this->_pLinks("",0,"",1,"","");
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/user/_add',$data);
		}
	}

	public function CreateRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			if(isset($_POST) && $_POST) 
			{
				$if_exist = $this->model_users->CheckIfEmailExist($_POST);
				if ($if_exist==0)
				{
					$username = strstr($_POST['email_address'], '@', true);
					$_POST = array_merge($_POST,array('username'=>$username));
					$t_id = $this->model_users->AddRecord($_POST);
					if (!empty($_POST['p_id']))
					{
						$this->model_pages->DelUserAccess(array('id'=>$t_id));
						foreach ($_POST['p_id'] as $key=>$p_id)
						{
							$this->model_pages->AddPageAccess(array('u_id'=>$t_id,'p_id'=>$p_id));
						}
					}
					if (!empty($_POST['type']))
					{
						$this->model_users->DelRecordType(array('id'=>$t_id));
						foreach ($_POST['type'] as $key=>$a_id)
						{
							$this->model_users->AddRecordType(array('u_id'=>$t_id,'a_id'=>$a_id));
						}
					}		
					$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[USER] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
					echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x0S") : $this->lib_utilities->GetErrorMsg("0x1E");
				}
				else
				{
					echo $this->lib_utilities->GetErrorMsg("0x2E");
				}
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}	

	public function EditRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[USER] VIEWED EDIT RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$this->session->set_userdata('a_type',$atype);
			$a_types = $this->lib_utilities->GetAccountTypes();
			$a_type = $this->model_users->GetAccessType($id);
			foreach($this->model_users->GetAccessType($id) as $type) { $atype[] = $type['access_id']; }
					
			foreach ($a_types AS $key=>$value)
			{
				$checked = (in_array($key,$atype)) ? "CHECKED" : "";
				$data['a_type'] .=  "<input type='checkbox' name='type[]' id='type[]' value='{$key}' {$checked}/>&ensp;{$value}<br />";
			}
			$data['a_dept'] = $this->lib_utilities->GetDepartments();
			$data['module_name'] = $this->module_name;
			$data['p_links'] = $this->_pLinks("",0,"",1,$id,"");
			$data['a_user'] = $this->model_users->GetRecordById($id);
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/user/_edit',$data);
		}
	}

	public function ModifyRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			if(isset($_POST) && $_POST) 
			{
				$username = strstr($_POST['email_address'], '@', true);
				$_POST = array_merge($_POST,array('username'=>$username));
				$t_id = $this->model_users->UpdateRecord($_POST);
				if (!empty($_POST['p_id']))
				{
					$this->model_pages->DelUserAccess($_POST);
					foreach ($_POST['p_id'] as $key=>$p_id)
					{
						$this->model_pages->AddPageAccess(array('u_id'=>$_POST['id'],'p_id'=>$p_id));
					}
				}
				if (!empty($_POST['type']))
				{
					$this->model_users->DelRecordType($_POST);
					foreach ($_POST['type'] as $key=>$a_id)
					{
						$this->model_users->AddRecordType(array('u_id'=>$_POST['id'],'a_id'=>$a_id));
					}
				}
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[USER] MODIFIED RECORD ID: '.$_POST['id'],'ip_address'=>$this->lib_utilities->GetIP()));
				echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x1S") : $this->lib_utilities->GetErrorMsg("0x1E");
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function ViewRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
			$data['display_message'] = $this->lib_utilities->GetErrorMsg("0x1Sys");
			$this->load->view('pages/incident/_view',$data);
		}
		else {
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[USER] VIEWED RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$this->session->set_userdata('a_type',$atype);
			$a_types = $this->lib_utilities->GetAccountTypes();
			$a_type = $this->model_users->GetAccessType($id);
			foreach($this->model_users->GetAccessType($id) as $type) { 
				$atype[] = $type['access_id']; 
			}
					
			foreach ($a_types AS $key=>$value)
			{
				$checked = (in_array($key,$atype)) ? "CHECKED" : "";
				$data['a_type'] .=  "<input type='checkbox' name='type[]' id='type[]' value='{$key}' {$checked} disabled/>&ensp;{$value}<br />";
			}
			$data['a_dept'] = $this->lib_utilities->GetDepartments();
			$data['module_name'] = $this->module_name;
			$data['p_links'] = $this->_pLinks("",0,"",1,$id,"DISABLED");
			$data['a_records'] = $this->model_users->GetRecordById($id);
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/user/_view',$data);
		}
	}	
	
	private function _pLinks($aux, $parent_id, $wdparent_id, $use_tree, $u_id, $disabled)
	{
		$page_url = base_url();
		$rows = $this->model_pages->GetAllPages($parent_id);
		$i=1;
		foreach ($rows as $data)
		{
			if($parent_id==0) { $aux = ""; }
			if (!empty($u_id)) :
				$w_access = $this->model_pages->CheckAccess(array('user_id'=>$u_id,'page_id'=>$data['id']));
				$checked = (!empty($w_access)) ? "CHECKED" : "";
			endif;
			$child_rows = $this->model_pages->GetAllPages($data['id']);						
			if($i==count($rows)):
				$icon = (count($child_rows)) ? "ftv2mlastnode.gif":"ftv2lastnode.gif";			
			else:
				$icon = (count($child_rows)) ? "ftv2mnode.gif":"ftv2node.gif";
			endif;
			$selected = ($wdparent_id==$data['id']) ? "selected" : "";
			$op_link .= ($use_tree) ? 
		"{$aux}<img src='{$page_url}_plugins/_css/images/{$icon}' align='middle'>&ensp;<input type='checkbox' name='p_id[]' value='{$data['id']}' id='p_id[]' {$checked} {$disabled}/>&ensp;{$data['p_name']}<br \>" : 
			"<option value='{$data['id']}' {$selected}>{$aux}&ensp; {$data['p_name']}</option>";						
			if ($child_rows):
				$_aux = $aux;
				if ($data['id']!=$parent_id) :	
					if ($use_tree) :
						if (count($child_rows)) :
						$aux .= ($i!=count($rows)) ? 
						"<img src='{$page_url}_plugins/_css/images/ftv2vertline.gif' align='middle'>":
						"<img src='{$page_url}_plugins/_css/images/ftv2blank.gif' align='middle'>";
						endif;
					else:
						$aux .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					endif;
					$op_link .= $this->_pLinks($aux,$data['id'],$wdparent_id, $use_tree, $u_id, $disabled);
				endif;
				$aux = $_aux;
			endif;
			$i++;
		}
	
		return $op_link;
	}	
}

/* End of file controller_user.php */
/* Location: ./application/controllers/controller_user.php */