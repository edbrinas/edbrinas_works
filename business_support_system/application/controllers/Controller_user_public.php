<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_user_public extends CI_Controller {

	private $controller_main = 'Controller_user_public/';
	private $module_name = 'CHANGE PASSWORD ';
	
	public function __construct()
	{
        parent::__construct();
		$this->load->model('model_sys_log');
		$this->load->library('lib_utilities');
		$this->load->library('datatables');
		$this->load->model('model_check_login');
		$this->load->model('model_users');
		header('Access-Control-Allow-Origin: *');
    }
	
	public function index()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[USER - PUBLIC PROF] VIEWED MODULE','ip_address'=>$this->lib_utilities->GetIP()));
			$u_id = $this->session->userdata('usr_id');
			$data['record'] = $this->model_users->GetRecordById($u_id);
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;		
			$this->load->view('pages/user/_edit_public_profile',$data);
		}
	}

	public function ModifyRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
			echo $this->lib_utilities->GetErrorMsg("0x1Sys");
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				$t_id = $this->model_users->UpdatePRecord($_POST);
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[USER - PUBLIC PROF] UPDATED PASSWORD FOR RECORD ID: '.$_POST['id'],'ip_address'=>$this->lib_utilities->GetIP()));
				echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x1S") : $this->lib_utilities->GetErrorMsg("0x1E");
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}	
}

/* End of file controller_user_public.php */
/* Location: ./application/controllers/controller_user_public.php */