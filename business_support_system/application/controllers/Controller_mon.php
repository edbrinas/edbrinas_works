<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_mon extends CI_Controller {

	private $controller_main = 'Controller_mon/';
	private $module_name = 'SERVER MONITORING';
	private $report_name = 'MONITORING DETAILS';

	public function __construct()
	{
        parent::__construct();
		$this->load->model('model_sys_log');
		$this->load->library('lib_utilities');
		$this->load->library('datatables');
		$this->load->model('model_check_login');
		$this->load->model('model_sys_log');
		$this->load->model('model_pages');
		$this->load->model('model_mon');
		header('Access-Control-Allow-Origin: *');

		if ($this->model_check_login->CheckSession())
		{
			$a_pid = $this->model_pages->GetPidByController($this->controller_main);
			$with_access = $this->model_pages->CheckAccess(array('user_id'=>$this->session->userdata('usr_id'),'page_id'=>$a_pid->id));
			if (empty($with_access)) {
				redirect(base_url()."controller_main/AccessDenied", 'refresh');
			}
		}
		else
		{
			redirect(base_url()."controller_main/SessionTimedOut", 'refresh');
		}

		$this->eKey = $this->session->userdata('encryption_key');
	}

	public function GetAllRecords()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$pop_up_config = $this->lib_utilities->PopUpConfiguration();
			$sLimit = intval($_GET['iDisplayLength']);
			$sOff   = intval($_GET['iDisplayStart']) > 0 ? intval($_GET['iDisplayStart']) : "";
			$searchable_items = array('mon_date','mon_site','created_by','mon_rem','mon_udate');
			$sortable_items = array('mon_date','mon_site','created_by','mon_rem','rec_attach','mon_udate','id');

			$sWhere = NULL;

			if(isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
				$sWhere = "AND (";
				foreach($searchable_items as $field) {
					$sWhere .= " {$field} LIKE '%{$_GET['sSearch']}%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				$sWhere .= ')';
			}

			$sOrder = "";
			if ( isset( $_GET['iSortCol_0'] ) )
			{
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
					if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
					{
						$sOrder .= "".$sortable_items[ intval( $_GET['iSortCol_'.$i] ) ]." ".
							($_GET['sSortDir_'.$i]==='asc' ? 'ASC' : 'DESC') .", ";
					}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = NULL;
				}
			}

			$arr_dta = $this->model_mon->GetAllRecords($sLimit,$sOff,$sWhere,$sOrder);
			$iTotalRec = $this->model_mon->GetFoundRows();
			$iTotalDispRec = $this->model_mon->GetTotalRows();

			$response = array(	'sEcho' => $_GET['sEcho'],
								'iTotalRecords' => $iTotalRec,
								'iTotalDisplayRecords' => $iTotalDispRec,
								'aaData' => array());

			foreach ($arr_dta as $data)
			{
				unset($o_link);
				
				$rec_attach = ($data['rec_attach']) ? "<a href='".base_url()."_temp_files/".$a_records->rec_attach."'>".$data['rec_attach']."</a>" : "N/A";
				
				$o_link .= ($data['rec_stat']!='CLOSED') ? '&nbsp;<a href="'.base_url().$this->controller_main.'EditRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey).'">
						<img src="'.base_url().'_plugins/_css/main_images/b_edit.png" title="EDIT RECORD" width="16" height="16" />
						</a>' : null;

				$o_link .= 	"&nbsp;".anchor_popup(base_url().$this->controller_main.'ViewRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey), '<img src="'.base_url().'_plugins/_css/main_images/b_browse.png" title="VIEW RECORD" width="16" height="16" />', $pop_up_config);
				$response['aaData'][] = array($data['mon_date'],$data['mon_site'],$data['created_by'],$data['mon_rem'],$rec_attach,$data['mon_udate'],$o_link);
			}

			echo json_encode($response);
		}
	}


	public function index()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] VIEWED MASTERLIST','ip_address'=>$this->lib_utilities->GetIP()));
			$top_menu = '<ul>';
				$top_menu .= '<li><a href="'.base_url().$this->controller_main.'AddRecord"><span>ADD NEW RECORD</span></a></li>';
			$top_menu .= '</ul>';
			$data['top_menu'] = $top_menu;
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/monitoring/_list',$data);
		}
	}

	public function AddRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] VIEWED ADD RECORD','ip_address'=>$this->lib_utilities->GetIP()));
			$data['a_sites'] = $this->lib_utilities->GetSites();
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/monitoring/_add',$data);
		}
	}

	public function CreateRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
			echo $this->lib_utilities->GetErrorMsg("0x1Sys");
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				
				if(isset($_FILES) && $_FILES) {
					$a_type = $this->lib_utilities->CheckFileType($_FILES);
					if (!empty($a_type)) {
						if($this->lib_utilities->CheckFileSize($_FILES)) {
							if($this->lib_utilities->UploadFile($_FILES)) {
								$f_name = $_FILES["file_to_upload"]["name"];
							}
							else {
								$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] ERROR UPLOADING FILE.','ip_address'=>$this->lib_utilities->GetIP()));
								$f_name = null;
							}
						}
						else {
							$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] INVALID FILE SIZE.','ip_address'=>$this->lib_utilities->GetIP()));
							$f_name = null;
						}
					}
					else {
						$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] INVALID FILE TYPE.','ip_address'=>$this->lib_utilities->GetIP()));
						$f_name = null;
					}
				}
				else {
					$f_name = null;
				}

				$_POST = array_merge($_POST, array('created_by'=>$this->session->userdata('username'),'rec_attach'=>$f_name ));
				$t_id = $this->model_mon->AddRecord($_POST);
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
				echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x0S") : $this->lib_utilities->GetErrorMsg("0x1E");
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function EditRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);		
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] VIEWED EDIT RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$data['a_records'] = $this->model_mon->GetRecById($id);
			$data['a_sites'] = $this->lib_utilities->GetSites();
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/monitoring/_edit',$data);
		}
	}

	public function ModifyRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
			echo $this->lib_utilities->GetErrorMsg("0x1Sys");
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				if(isset($_FILES) && $_FILES) {
					$a_type = $this->lib_utilities->CheckFileType($_FILES);
					if (!empty($a_type)) {
						if($this->lib_utilities->CheckFileSize($_FILES)) {
							if($this->lib_utilities->UploadFile($_FILES)) {
								$f_name = $_FILES["file_to_upload"]["name"];
							}
							else {
								$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] ERROR UPLOADING FILE.','ip_address'=>$this->lib_utilities->GetIP()));
								$f_name = null;
							}
						}
						else {
							$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] INVALID FILE SIZE.','ip_address'=>$this->lib_utilities->GetIP()));
							$f_name = null;
						}
					}
					else {
						$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] INVALID FILE TYPE.','ip_address'=>$this->lib_utilities->GetIP()));
						$f_name = null;
					}
				}
				else {
					$f_name = null;
				}

				$_POST = array_merge($_POST, array('updated_by'=>$this->session->userdata('username'),'rec_attach'=>$f_name));
				$t_id = $this->model_mon->UpdateRecord($_POST);
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] MODIFIED RECORD ID: '.$_POST['id'],'ip_address'=>$this->lib_utilities->GetIP()));
				echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x1S") : $this->lib_utilities->GetErrorMsg("0x1E");
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function ViewRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);		
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[MONITORING] VIEWED RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$data['a_records'] = $this->model_mon->GetRecById($id);
			$data['report_name'] = $this->report_name;;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/monitoring/_view',$data);
		}
	}

	public function RemoveFile()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
			$data['display_message'] = $this->lib_utilities->GetErrorMsg("0x1Sys");
			$this->load->view('pages/incident/_view',$data);
		}
		else {
			if(isset($_POST) && $_POST)
			{
				$a_records = $this->model_mon->GetRecById($_POST['id']);
				$this->lib_utilities->DeleteFile(array('file_name'=>$a_records->rec_attach));
				$t_id = $this->model_mon->RemoveFile($_POST);
				$items = "<p><input type='file' name='file_to_upload' id='file_to_upload' /></p>";
				echo !empty($items) ? $items : null;
			}
		}
	}	
}

/* End of file controller_mon.php */
/* Location: ./application/controllers/controller_mon.php */