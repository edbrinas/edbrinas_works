<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_lr extends CI_Controller {

	private $controller_main = 'Controller_lr/';
	private $module_name = 'STAFF LEAVE REPORT';
	private $report_name = 'LEAVE REPORT DETAILS';

	public function __construct()
	{
        parent::__construct();
		$this->load->model('model_sys_log');
		$this->load->library('lib_utilities');
		$this->load->library('datatables');
		$this->load->model('model_check_login');
		$this->load->model('model_sys_log');
		$this->load->model('model_pages');
		$this->load->model('model_lr');

		$this->cal_template ='
			{table_open}<table border="0" cellpadding="0" cellspacing="0" class="calendar" width="100%">{/table_open}

			{heading_row_start}<tr>{/heading_row_start}

			{heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
			{heading_title_cell}<th colspan="{colspan}" class="details"><span id="mo_yr_num">{heading}</span></th>{/heading_title_cell}
			{heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

			{heading_row_end}</tr>{/heading_row_end}

			{week_row_start}<tr>{/week_row_start}
			{week_day_cell}<td align="center">{week_day}</td>{/week_day_cell}
			{week_row_end}</tr>{/week_row_end}

			{cal_row_start}<tr class="days">{/cal_row_start}
			{cal_cell_start}<td class="details">{/cal_cell_start}

			{cal_cell_content}
				<div class="day_num"> {day} </div>
				<div class="content"> {content} </div>
			{/cal_cell_content}

			{cal_cell_content_today}
				<div class="day_num highlight"> {day} </div>
				<div class="content"> {content} </div>
			{/cal_cell_content_today}

			{cal_cell_no_content}
				<div class="day_num"> {day} </div>
			{/cal_cell_no_content}

			{cal_cell_no_content_today}
				<div class="day_num highlight"> {day} </div>
			{/cal_cell_no_content_today}

			{cal_cell_blank}&nbsp;{/cal_cell_blank}

			{cal_cell_end}</td>{/cal_cell_end}
			{cal_row_end}</tr>{/cal_row_end}

			{table_close}</table>{/table_close}
			';




		if ($this->model_check_login->CheckSession())
		{
			$a_pid = $this->model_pages->GetPidByController($this->controller_main);
			$with_access = $this->model_pages->CheckAccess(array('user_id'=>$this->session->userdata('usr_id'),'page_id'=>$a_pid->id));
			if (empty($with_access)) {
				redirect(base_url()."controller_main/AccessDenied", 'refresh');
			}
		}
		else
		{
			redirect(base_url()."controller_main/SessionTimedOut", 'refresh');
		}
		$this->eKey = $this->session->userdata('encryption_key');
		$is_admin = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),1);
		$access_level_4 = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),4);
		$this->top_menu = '<ul>';
		$this->top_menu .= (!empty($is_admin) || !empty($access_level_4)) ? '<li><a href="'.base_url().$this->controller_main.'ApproveRecord"><span>APPROVE LEAVE RECORD</span></a></li>' : "";
		$this->top_menu .= '<li><a href="'.base_url().$this->controller_main.'AddRecord"><span>APPLY FOR A LEAVE</span></a></li>';
		$this->top_menu .= '<li><a href="'.base_url().$this->controller_main.'MyLeaveRecord"><span>MY LEAVE REQUESTS</span></a></li>';
		$this->top_menu .= '</ul>';

    }


	public function GetAllForApproval()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$pop_up_config = $this->lib_utilities->PopUpConfiguration();
			$sLimit = intval($_GET['iDisplayLength']);
			$sOff   = intval($_GET['iDisplayStart']) > 0 ? intval($_GET['iDisplayStart']) : "";
			$searchable_items = array('filed_date','staff_name','s_date','e_date','leave_reason','leave_status','approved_by','approved_date');
			$sortable_items = array('filed_date','staff_name','s_date','e_date','leave_reason','id');

			$sWhere = NULL;

			if(isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
				$sWhere = "AND (";
				foreach($searchable_items as $field) {
					$sWhere .= " {$field} LIKE '%{$_GET['sSearch']}%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				$sWhere .= ')';
				$sWhere .= ' AND leave_status = "PENDING_APPROVAL" ';
			}
			else {
				$sWhere .= ' WHERE leave_status = "PENDING_APPROVAL" ';
			}

			$sOrder = "";
			if ( isset( $_GET['iSortCol_0'] ) )
			{
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
					if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
					{
						$sOrder .= "".$sortable_items[ intval( $_GET['iSortCol_'.$i] ) ]." ".
							($_GET['sSortDir_'.$i]==='asc' ? 'ASC' : 'DESC') .", ";
					}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = NULL;
				}
			}

			$arr_dta = $this->model_lr->GetAllFiledLeaves($sLimit,$sOff,$sWhere,$sOrder);
			$iTotalRec = $this->model_lr->GetFoundRows();
			$iTotalDispRec = $this->model_lr->GetTotalRows();

			$response = array(	'sEcho' => $_GET['sEcho'],
								'iTotalRecords' => $iTotalRec,
								'iTotalDisplayRecords' => $iTotalDispRec,
								'aaData' => array());
			$is_admin = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),1);
			$access_level_2 = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),2);
			$access_level_3 = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),3);
			$access_level_4 = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),4);


			foreach ($arr_dta as $data)
			{
				unset($o_link);
				$o_link .= (!empty($is_admin) || ($access_level_4)) ?
						'&nbsp;<a href="'.base_url().$this->controller_main.'EditApproveRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey).'">
						<img src="'.base_url().'_plugins/_css/main_images/s_success.png" title="APPROVE RECORD" width="16" height="16" />
						</a>' : NULL;
				$o_link .= 	"&nbsp;".anchor_popup(base_url().$this->controller_main.'ViewApproveRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey), '<img src="'.base_url().'_plugins/_css/main_images/b_browse.png" title="VIEW RECORD" width="16" height="16" />', $pop_up_config);

				$response['aaData'][] = array(	date("d-M-Y", strtotime($data['filed_date'])),
												$data['staff_name'],
												date("d-M-Y", strtotime($data['s_date'])),
												date("d-M-Y", strtotime($data['e_date'])),
												$data['leave_reason'],$o_link);
			}

			echo json_encode($response);
		}
	}

	public function GetAllMyLeaves()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$pop_up_config = $this->lib_utilities->PopUpConfiguration();
			$sLimit = intval($_GET['iDisplayLength']);
			$sOff   = intval($_GET['iDisplayStart']) > 0 ? intval($_GET['iDisplayStart']) : "";
			$searchable_items = array('filed_date','s_date','e_date','leave_status','approved_by','approved_date');
			$sortable_items = array('filed_date','s_date','e_date','leave_status','approved_by','approved_date','id');

			$sWhere = NULL;

			if(isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
				$sWhere = " WHERE (";
				foreach($searchable_items as $field) {
					$sWhere .= " {$field} LIKE '%{$_GET['sSearch']}%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				$sWhere .= ')';
				$sWhere .= " AND staff_name = '".strtoupper($this->session->userdata('fullname'))."' ";
			}
			else {
				$sWhere = " WHERE staff_name = '".strtoupper($this->session->userdata('fullname'))."'  ";
			}

			$sOrder = "";
			if ( isset( $_GET['iSortCol_0'] ) )
			{
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
					if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
					{
						$sOrder .= "".$sortable_items[ intval( $_GET['iSortCol_'.$i] ) ]." ".
							($_GET['sSortDir_'.$i]==='asc' ? 'ASC' : 'DESC') .", ";
					}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = NULL;
				}
			}

			$arr_dta = $this->model_lr->GetAllFiledLeaves($sLimit,$sOff,$sWhere,$sOrder);
			$iTotalRec = $this->model_lr->GetFoundRows();
			$iTotalDispRec = $this->model_lr->GetTotalRowsDetails();

			$response = array(	'sEcho' => $_GET['sEcho'],
								'iTotalRecords' => $iTotalRec,
								'iTotalDisplayRecords' => $iTotalDispRec,
								'aaData' => array());
			$access_level_2 = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),2);
			$access_level_3 = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),3);
			foreach ($arr_dta as $data)
			{
				unset($o_link);
				$o_link .= ($data['leave_status']!='APPROVED') ? '&nbsp;<a href="'.base_url().$this->controller_main.'EditRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey).'">
						<img src="'.base_url().'_plugins/_css/main_images/b_edit.png" title="EDIT RECORD" width="16" height="16" />
						</a>': NULL;
				$o_link .= 	"&nbsp;".anchor_popup(base_url().$this->controller_main.'ViewRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey), '<img src="'.base_url().'_plugins/_css/main_images/b_browse.png" title="VIEW RECORD" width="16" height="16" />', $pop_up_config);

				$response['aaData'][] = array(	date("d-M-Y", strtotime($data['filed_date'])),
												date("d-M-Y", strtotime($data['s_date'])),
												date("d-M-Y", strtotime($data['e_date'])),
												$data['leave_status'],
												$data['approved_by'],
												$data['approved_date'],$o_link);
			}

			echo json_encode($response);
		}
	}


	public function index()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			unset($cal_pref);
			unset($cal_data);
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] VIEWED MASTERFILE','ip_address'=>$this->lib_utilities->GetIP()));
			$cal_pref['template'] = $this->cal_template;
			$cal_pref['start_day']='sunday';
			$cal_pref['day_type']='long';
			$cal_pref['show_next_prev']=true;

			$this->load->library('calendar',$cal_pref);
			$pop_up_config = $this->lib_utilities->PopUpConfiguration();
			
			$s3 = $this->uri->segment(3);
			$s4 = $this->uri->segment(4);
			
			$yr = empty($s3) ? date('Y') : $s3;
			$mo = empty($s4) ? date('m') : $s4;

			$arr_dta = $this->model_lr->GetAllRecords($limit=0,$offset=0," WHERE DATE_FORMAT(leave_date,'%Y-%m') = '".$yr."-".$mo."' AND leave_status = 'APPROVED' "," ORDER BY id ASC ");
			$cal_data = array();

			foreach ($arr_dta as $aaData)
			{
				unset($aDay);
				unset($s_names);
				unset($aaaData);
				unset($aarr_dta);
				$aDay = date("j", strtotime($aaData['leave_date']));
				$aarr_dta = $this->model_lr->GetAllRecords($limit=0,$offset=0," WHERE leave_date = '".$aaData['leave_date']."' AND leave_status = 'APPROVED' "," ORDER BY id ASC ");
				foreach ($aarr_dta as $aaaData) {
					$s_names .= $aaaData['staff_name'].',';
				}
				if ($aarr_dta) {
					$cal_data[$aDay] = (array_key_exists($aDay,$cal_data)) ? anchor_popup(base_url().$this->controller_main.'ViewCalRecord/'.$aaData['leave_date'], '<img src="'.base_url().'_plugins/_css/main_images/b_browse.png" title="'.rtrim($s_names, ",").'" width="16" height="16" />', $pop_up_config)  : NULL;
				}
			}

			$data['top_menu'] = $this->top_menu;
			$data['calendar_view'] = $this->calendar->generate($yr, $mo, $cal_data);
			$data['a_l_type'] = $this->lib_utilities->GetLeaveTypes();
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/leave/_list',$data);
		}
	}

	public function AddRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			unset($cal_pref);
			unset($cal_data);
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] VIEWED ADD RECORD','ip_address'=>$this->lib_utilities->GetIP()));
			$cal_pref['template'] = $this->cal_template;
			$cal_pref['start_day']='sunday';
			$cal_pref['day_type']='long';
			$cal_pref['show_next_prev']=true;

			$this->load->library('calendar',$cal_pref);
			$pop_up_config = $this->lib_utilities->PopUpConfiguration();

			$s3 = $this->uri->segment(3);
			$s4 = $this->uri->segment(4);
			
			$yr = empty($s3) ? date('Y') : $s3;
			$mo = empty($s4) ? date('m') : $s4;

			$arr_dta = $this->model_lr->GetAllRecords($limit=0,$offset=0," WHERE DATE_FORMAT(leave_date,'%Y-%m') = '".$yr."-".$mo."' AND leave_status != 'APPROVED' "," ORDER BY id ASC ");
			$cal_data = array();

			foreach ($arr_dta as $aaData)
			{
				unset($aDay);
				unset($s_names);
				unset($aaaData);
				unset($aarr_dta);
				$aDay = date("j", strtotime($aaData['leave_date']));
				$aarr_dta = $this->model_lr->GetAllRecords($limit=0,$offset=0," WHERE leave_date = '".$aaData['leave_date']."' AND leave_status != 'APPROVED' "," ORDER BY id ASC ");
				foreach ($aarr_dta as $aaaData) {
					$s_names .= $aaaData['staff_name'].',';
				}
				if ($aarr_dta) {
					$cal_data[$aDay] = (array_key_exists($aDay,$cal_data)) ?  anchor_popup(base_url().$this->controller_main.'ViewCalRecord/'.$aaData['leave_date'], '<img src="'.base_url().'_plugins/_css/main_images/b_browse.png" title="'.rtrim($s_names, ",").'" width="16" height="16" />', $pop_up_config) : NULL;
				}
			}

			$data['top_menu'] = $this->top_menu;
			$data['calendar_view'] = $this->calendar->generate($yr, $mo, $cal_data);
			$data['a_l_type'] = $this->lib_utilities->GetLeaveTypes();
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/leave/_add',$data);
		}
	}

	public function CreateRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				$_POST = array_merge($_POST, array('staff_name'=>$this->session->userdata('fullname')));
				$if_exist_details = $this->model_lr->CheckIfDetailsExist($_POST);
				$if_exist = $this->model_lr->CheckIfExist($_POST);
				if (!($if_exist_details) && !($if_exist)) {
					$t_id = $this->model_lr->AddRecordDetails($_POST);
					$_POST = array_merge($_POST, array('c_id'=>$t_id));
					$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE DETAILS] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
					$aDates = $this->lib_utilities->GetDates($_POST['s_date'], $_POST['e_date']);
					foreach ($aDates as $lDate) {
						$wday = "";
						$wday = date('l', strtotime($lDate));
						if (strtoupper($wday)!="FRIDAY" && strtoupper($wday)!="SATURDAY") {
							unset ($if_exist);
							$_POST = array_merge($_POST, array('leave_date'=>$lDate));
							$t_id = $this->model_lr->AddRecord($_POST);
							$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
						}
					}
					echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x4S") : $this->lib_utilities->GetErrorMsg("0x10E");
				}
				else {
					echo $this->lib_utilities->GetErrorMsg("0x2E");
				}
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function MyLeaveRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] VIEWED MY LEAVE MASTERLIST','ip_address'=>$this->lib_utilities->GetIP()));
			$data['top_menu'] = $this->top_menu;
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/leave/_my_leave',$data);
		}
	}

	public function ApproveRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] VIEWED FOR APPROVAL MASTERLIST','ip_address'=>$this->lib_utilities->GetIP()));
			$data['top_menu'] = $this->top_menu;
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/leave/_for_approval',$data);
		}
	}

	public function EditApproveRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] VIEWED EDIT APPROVE ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$data['top_menu'] = $this->top_menu;
			$data['a_records'] = $this->model_lr->GetRecDetailsById($id);
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/leave/_approve_leave',$data);
		}
	}

	public function EditRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] VIEWED EDIT RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$data['top_menu'] = $this->top_menu;
			$data['a_records'] = $this->model_lr->GetRecDetailsById($id);
			$data['a_l_type'] = $this->lib_utilities->GetLeaveTypes();
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/leave/_edit',$data);
		}
	}

	public function ModifyApprovedRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			if(isset($_POST) && $_POST)
			{			
				if ($_POST['leave_status']=='APPROVED') {
					$_POST = array_merge($_POST, array('approved_date'=>date("Y-m-d h:i:s"),'approved_by'=>$this->session->userdata('fullname')));
				}
								
				$t_id = $this->model_lr->UpdateRecord($_POST);
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] APPROVED RECORD ID: '.$_POST['id'],'ip_address'=>$this->lib_utilities->GetIP()));				
				$t_id = $this->model_lr->UpdateDetailsRecord($_POST);
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] APPROVED RECORD ID: '.$_POST['id'],'ip_address'=>$this->lib_utilities->GetIP()));
				echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x1S") : $this->lib_utilities->GetErrorMsg("0x1E");
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function ModifyRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				if ($_POST['leave_status']=='CANCELLED') {
					unset($_POST['s_date']);
					unset($_POST['e_date']);
					unset($_POST['leave_reason']);
					$t_id = $this->model_lr->UpdateRecord($_POST);
					$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] UPDATED RECORD DETAILS ID: '.$_POST['c_id'],'ip_address'=>$this->lib_utilities->GetIP()));
					$t_id = $this->model_lr->UpdateDetailsRecord($_POST);
					$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE DETAILS] UPDATED RECORD DETAILS ID: '.$_POST['c_id'],'ip_address'=>$this->lib_utilities->GetIP()));	
					echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x1S") : $this->lib_utilities->GetErrorMsg("0x1E");
				} else {
					$this->model_lr->DeleteRecordDetails($_POST);
					$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE DETAILS] DELETED RECORD DETAILS ID: '.$_POST['c_id'],'ip_address'=>$this->lib_utilities->GetIP()));
					$this->model_lr->DeleteRecord($_POST);
					$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] DELETED RECORD ID: '.$_POST['c_id'],'ip_address'=>$this->lib_utilities->GetIP()));
					unset($_POST['c_id']);
					$_POST = array_merge($_POST, array('staff_name'=>$this->session->userdata('fullname')));
					$if_exist_details = $this->model_lr->CheckIfDetailsExist($_POST);
					$if_exist = $this->model_lr->CheckIfExist($_POST);
					if (!($if_exist_details) && !($if_exist)) {
						$t_id = $this->model_lr->AddRecordDetails($_POST);
						$_POST = array_merge($_POST, array('c_id'=>$t_id));
						$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE DETAILS] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
						$aDates = $this->lib_utilities->GetDates($_POST['s_date'], $_POST['e_date']);
						foreach ($aDates as $lDate) {
							$wday = "";
							$wday = date('l', strtotime($lDate));
							if (strtoupper($wday)!="FRIDAY" && strtoupper($wday)!="SATURDAY") {
								unset ($if_exist);
								$_POST = array_merge($_POST, array('leave_date'=>$lDate));
								$t_id = $this->model_lr->AddRecord($_POST);
								$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
							}
						}
						echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x1S") : $this->lib_utilities->GetErrorMsg("0x1E");
					}
					else {
						echo $this->lib_utilities->GetErrorMsg("0x2E");
					}				
				}
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function DeleteRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] DELETED RECORD ID: '.$_POST['id'],'ip_address'=>$this->lib_utilities->GetIP()));
			$data['top_menu'] = $this->top_menu;
			$data['a_records'] = $this->model_lr->GetRecById($id);
			$data['a_countries'] = $this->lib_utilities->GetCountries();
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/leave/_delete',$data);
		}
	}

	public function RemoveRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				$t_id = $this->model_lr->DeleteRecord($_POST);
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] DELETED RECORD ID: '.$_POST['id'],'ip_address'=>$this->lib_utilities->GetIP()));
				echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x2S") : $this->lib_utilities->GetErrorMsg("0x1E");
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function ViewRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] VIEWED RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$data['a_records'] = $this->model_lr->GetRecDetailsById($id);
			$data['report_name'] = $this->report_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/leave/_view',$data);
		}
	}

	public function ViewApproveRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			unset($iReasons);
			$id = $this->lib_utilities->decrypt($id,$this->eKey);
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] VIEWED EDIT APPROVE DATE ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
			$a_records = $this->model_lr->GetRecByfDate(array('filed_date'=>$id,'leave_status'=>'PENDING_APPROVAL'));

			$arr_adta = $this->model_lr->GetInclDates(array('filed_date'=>$a_records->filed_date));
			$arr_aadta = $this->model_lr->GetAllRecords($limit=0,$offset=0," WHERE DATE_FORMAT(filed_date,'%Y-%m-%d') = '".$a_records->filed_date."'"," ORDER BY leave_date ASC ");

			foreach ($arr_aadta as $aaData)
			{
				$iReasons .= date("d M, Y", strtotime($aaData['leave_date'])).' | '.$aaData['leave_reason'].'<br />';
			}

			$data['i_dates'] = "<b>FROM: </b>".$arr_adta->minDate." <b>TO: </b>".$arr_adta->maxDate;
			$data['i_reasons'] = $iReasons;
			$data['top_menu'] = $this->top_menu;
			$data['a_records'] = $a_records;
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/leave/_view_approve_leave',$data);
		}
	}
	public function ViewCalRecord($lDate)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[LEAVE] VIEWED CALENDAR RECORD MASTERLIST DATE '.$lDate,'ip_address'=>$this->lib_utilities->GetIP()));
			#$aarr_dta = $this->model_lr->GetAllRecords($limit=0,$offset=0," WHERE leave_date = '".$lDate."' AND leave_status = 'APPROVED' "," ORDER BY staff_name ASC ");
			$aarr_dta = $this->model_lr->GetAllRecords($limit=0,$offset=0," WHERE leave_date = '".$lDate."' "," ORDER BY staff_name ASC ");
			$a_records = "";
			foreach ($aarr_dta as $aaaData) {

				$a_records .= "<tr>";
					$a_records .= "<td>".date("d-M-Y", strtotime($aaaData['leave_date']))."</td>";
					$a_records .= "<td>".$aaaData['staff_name']."</td>";
					$a_records .= "<td>".$aaaData['leave_reason']."</td>";
					$a_records .= "<td>".$aaaData['leave_status']."</td>";
					$a_records .= "<td>".$aaaData['approved_by']."</td>";
					$a_records .= "<td>".$aaaData['approved_date']."</td>";
				$a_records .= "</tr>";
			}

			$data['a_records'] = $a_records;
			$data['report_name'] = $this->report_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/leave/_view_cal_rec',$data);
		}
	}
}

/* End of file controller_lr.php */
/* Location: ./application/controllers/controller_lr.php */