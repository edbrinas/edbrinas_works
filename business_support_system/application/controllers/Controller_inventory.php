<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_inventory extends CI_Controller {

	private $controller_main = 'Controller_inventory/';
	private $module_name = 'INVENTORY';
	private $report_name = 'INVENTORY DETAILS';

	public function __construct()
	{
        parent::__construct();
		$this->load->model('model_sys_log');
		$this->load->library('lib_utilities');
		$this->load->library('datatables');
		$this->load->model('model_check_login');
		$this->load->model('model_sys_log');
		$this->load->model('model_pages');
		$this->load->model('model_inventory');
		header('Access-Control-Allow-Origin: *');

		if ($this->model_check_login->CheckSession())
		{
			$a_pid = $this->model_pages->GetPidByController($this->controller_main);
			$with_access = $this->model_pages->CheckAccess(array('user_id'=>$this->session->userdata('usr_id'),'page_id'=>$a_pid->id));
			if (empty($with_access)) {
				redirect(base_url()."controller_main/AccessDenied", 'refresh');
			}
		}
		else
		{
			redirect(base_url()."controller_main/SessionTimedOut", 'refresh');
		}

		$this->eKey = $this->session->userdata('encryption_key');
	}

	public function GetAllRecords()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$pop_up_config = $this->lib_utilities->PopUpConfiguration();
			$sLimit = intval($_GET['iDisplayLength']);
			$sOff   = intval($_GET['iDisplayStart']) > 0 ? intval($_GET['iDisplayStart']) : "";
			$searchable_items = array('e_ip_add','e_name','e_desc','e_pur_date','e_sup_name','e_loc','e_type','e_manu','e_serial_num','e_proc','e_mem','e_hdd','e_os');
			$sortable_items = array('e_name','e_desc','e_ip_add','e_loc','id');

			$sWhere = NULL;

			if(isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
				$sWhere = "AND (";
				foreach($searchable_items as $field) {
					$sWhere .= " {$field} LIKE '%{$_GET['sSearch']}%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				$sWhere .= ')';
			}

			$sOrder = "";
			if ( isset( $_GET['iSortCol_0'] ) )
			{
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
				{
					if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
					{
						$sOrder .= "".$sortable_items[ intval( $_GET['iSortCol_'.$i] ) ]." ".
							($_GET['sSortDir_'.$i]==='asc' ? 'ASC' : 'DESC') .", ";
					}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = NULL;
				}
			}

			$arr_dta = $this->model_inventory->GetAllRecords($sLimit,$sOff,$sWhere,$sOrder);
			$iTotalRec = $this->model_inventory->GetFoundRows();
			$iTotalDispRec = $this->model_inventory->GetTotalRows();

			$response = array(	'sEcho' => $_GET['sEcho'],
								'iTotalRecords' => $iTotalRec,
								'iTotalDisplayRecords' => $iTotalDispRec,
								'aaData' => array());

			foreach ($arr_dta as $data)
			{
				unset($o_link);
				$o_link .= ($data['rec_stat']!='CLOSED') ? '&nbsp;<a href="'.base_url().$this->controller_main.'EditRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey).'">
						<img src="'.base_url().'_plugins/_css/main_images/b_edit.png" title="EDIT RECORD" width="16" height="16" />
						</a>' : null;

				$o_link .= 	"&nbsp;".anchor_popup(base_url().$this->controller_main.'ViewRecord/'.$this->lib_utilities->encrypt($data['id'],$this->eKey), '<img src="'.base_url().'_plugins/_css/main_images/b_browse.png" title="VIEW RECORD" width="16" height="16" />', $pop_up_config);
				$response['aaData'][] = array($data['e_name'],$data['e_desc'],$data['e_ip_add'],$data['e_loc'],$o_link);
			}

			echo json_encode($response);
		}
	}


	public function index()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[INCIDENT] VIEWED MASTERLIST','ip_address'=>$this->lib_utilities->GetIP()));
			$top_menu = '<ul>';
				$top_menu .= '<li><a href="'.base_url().$this->controller_main.'AddRecord"><span>ADD NEW RECORD</span></a></li>';
			$top_menu .= '</ul>';
			$data['top_menu'] = $top_menu;
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/inventory/_list',$data);
		}
	}

	public function AddRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$data['a_mem'] = array(2,4,8,16,32,64,128,256,512,1024,2048,4096,8192);
			$data['a_sites'] = $this->lib_utilities->GetSites();
			$data['a_os_types'] = $this->lib_utilities->GetOsTypes();
			$data['a_e_types'] = $this->lib_utilities->GetEquipmentTypes();
			$data['a_m_types'] = $this->lib_utilities->GetManufacturerTypes();
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/inventory/_add',$data);
		}
	}

	public function CreateRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				$if_exist = $this->model_inventory->CheckIfExist($_POST);
				if ($if_exist==0)
				{
					$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'ADDED INVENTORY','ip_address'=>$this->lib_utilities->GetIP()));
					$t_id = $this->model_inventory->AddRecord($_POST);
					echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x0S") : $this->lib_utilities->GetErrorMsg("0x1E");
				}
				else
				{
					echo $this->lib_utilities->GetErrorMsg("0x2E");
				}
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function EditRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);		
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$data['a_mem'] = array(2,4,8,16,32,64,128,256,512,1024,2048,4096,8192);
			$data['a_sites'] = $this->lib_utilities->GetSites();
			$data['a_os_types'] = $this->lib_utilities->GetOsTypes();
			$data['a_e_types'] = $this->lib_utilities->GetEquipmentTypes();
			$data['a_m_types'] = $this->lib_utilities->GetManufacturerTypes();
			$data['a_records'] = $this->model_inventory->GetRecById($id);
			$data['module_name'] = $this->module_name;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/inventory/_edit',$data);
		}
	}

	public function ModifyRecord()
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			if(isset($_POST) && $_POST)
			{
				$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'MODIFIED INVENTORY','ip_address'=>$this->lib_utilities->GetIP()));
				$t_id = $this->model_inventory->UpdateRecord($_POST);
				echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x1S") : $this->lib_utilities->GetErrorMsg("0x1E");
			}
			else
			{
				echo $this->lib_utilities->GetErrorMsg("0x0E");
			}
		}
	}

	public function ViewRecord($id)
	{
		$this->session->unset_userdata('word_captcha');
		if (!$this->model_check_login->CheckSession())
		{
	 		$data['captcha'] = create_captcha($this->lib_utilities->GenerateCaptcha());
			$this->session->set_userdata('word_captcha',$data['captcha']['word']);
			$this->load->view('main',$data);
		}
		else
		{
			$id = $this->lib_utilities->decrypt($id,$this->eKey);		
			($id==0) ? redirect(base_url()."controller_main/AccessDenied", 'refresh') : null;
			$data['a_records'] = $this->model_inventory->GetRecById($id);
			$data['report_name'] = $this->report_name;;
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/inventory/_view',$data);
		}
	}
}

/* End of file controller_inventory.php */
/* Location: ./application/controllers/controller_inventory.php */