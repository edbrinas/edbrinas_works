<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_pages extends CI_Controller {

	private $controller_main = 'Controller_pages/';
	
	
	public function __construct()
	{
        parent::__construct();
		$this->load->model('model_sys_log');
		$this->load->library('lib_utilities');
		$this->load->library('datatables');
		$this->load->model('model_check_login');
		$this->load->model('model_pages');
		$this->load->helper(array('form', 'url'));
		header('Access-Control-Allow-Origin: *');
		
		if ($this->model_check_login->CheckSession())
		{
			$a_pid = $this->model_pages->GetPidByController($this->controller_main); 
			$with_access = $this->model_pages->CheckAccess(array('user_id'=>$this->session->userdata('usr_id'),'page_id'=>$a_pid->id));
			if (empty($with_access)) {
				redirect(base_url()."controller_main/AccessDenied", 'refresh'); 
			}		
		}
		else
		{
			redirect(base_url()."controller_main/SessionTimedOut", 'refresh'); 
		}
    }
	
	public function index()
	{
		$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[PAGES] VIEWED MASTERLIST','ip_address'=>$this->lib_utilities->GetIP()));
		$is_admin = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),1);
		$access_level_2 = $this->model_pages->CheckUserType($this->session->userdata('usr_id'),2);
					
		if (!empty($is_admin) || !empty($access_level_2)) 
		{
			$top_menu = '<ul>';
				$top_menu .= '<li><a href="'.base_url().$this->controller_main.'AddRecord"><span>ADD NEW RECORD</span></a></li>';
			$top_menu .= '</ul>';
			$data['top_menu'] = $top_menu;
		}
		$data['controller_main'] = $this->controller_main;		
		$data['p_links'] = $this->_pLink("",0,"",1);
		$this->load->view('pages/pages/pages_list',$data);
	}

	public function AddRecord()
	{
		$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[PAGES] VIEWED ADD RECORD','ip_address'=>$this->lib_utilities->GetIP()));
		$data['p_links'] = $this->_pLink("",0,"","");
		$data['controller_main'] = $this->controller_main;
		$this->load->view('pages/pages/pages_add',$data);
	}

	public function EditRecord($id)
	{
		$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[PAGES] VIEWED EDIT RECORD ID: '.$id,'ip_address'=>$this->lib_utilities->GetIP()));
		$a_pages = $this->model_pages->GetPageById($id);
		$data['a_pages'] = $a_pages;
		$data['p_links'] = $this->_pLink("",0,$a_pages->p_id,"");
		$data['controller_main'] = $this->controller_main;
		$this->load->view('pages/pages/pages_edit',$data);
	}	

	public function CreateRecord()
	{
		if(isset($_POST) && $_POST)
		{
			$t_id = $this->model_pages->AddPage($_POST);
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[PAGES] ADDED RECORD ID: '.$t_id,'ip_address'=>$this->lib_utilities->GetIP()));
			echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x0S") : $this->lib_utilities->GetErrorMsg("0x1E");
		}
		else
		{
			echo $this->lib_utilities->GetErrorMsg("0x4E");
		}
	}	

	public function ModifyRecord()
	{
		if(isset($_POST) && $_POST)
		{
			$t_id = $this->model_pages->UpdatePage($_POST);
			$this->model_sys_log->AddTLog(array('username'=>$this->session->userdata('username'),'activity'=>'[PAGES] MODIFIED RECORD ID: '.$_POST['id'],'ip_address'=>$this->lib_utilities->GetIP()));
			echo (is_numeric($t_id)) ? $this->lib_utilities->GetErrorMsg("0x1S") : $this->lib_utilities->GetErrorMsg("0x1E");
		}
		else
		{
			echo $this->lib_utilities->GetErrorMsg("0x0E");
		}		
	}

	private function _pLink($aux, $parent_id, $wdparent_id, $use_tree)
	{
		$page_url = base_url();
		$rows = $this->model_pages->GetAllPages($parent_id);
		$i=1;
		$op_link = "";
		foreach ($rows as $data)
		{
			if($parent_id==0) { $aux = ""; }
			$child_rows = $this->model_pages->GetAllPages($data['id']);						
			if($i==count($rows)):
				$icon = (count($child_rows)) ? "ftv2mlastnode.gif":"ftv2lastnode.gif";			
			else:
				$icon = (count($child_rows)) ? "ftv2mnode.gif":"ftv2node.gif";
			endif;
			$selected = ($wdparent_id==$data['id']) ? "selected" : "";
			$s_link = ($this->model_pages->CheckUserType($this->session->userdata('usr_id'),1)!== NULL || $this->model_pages->CheckUserType($this->session->userdata('usr_id'),2)!== NULL)	? "{$aux}<img src='{$page_url}_plugins/_css/images/{$icon}' align='middle'>&ensp;[{$data['o_num']}]&ensp;<a href='EditRecord/{$data['id']}'>{$data['p_name']}</a><br \>" :
				 "{$aux}<img src='{$page_url}_plugins/_css/images/{$icon}' align='middle'>&ensp;{$data['p_name']}<br \>";
			$op_link .= ($use_tree) ? $s_link : "<option value='{$data['id']}' {$selected}>{$aux}&ensp; {$data['p_name']}</option>";						
			if ($child_rows):
				$_aux = $aux;
				if ($data['id']!=$parent_id) :	
					if ($use_tree) :
						if (count($child_rows)) :
						$aux .= ($i!=count($rows)) ? 
						"<img src='{$page_url}_plugins/_css/images/ftv2vertline.gif' align='middle'>":
						"<img src='{$page_url}_plugins/_css/images/ftv2blank.gif' align='middle'>";
						endif;
					else:
						$aux .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					endif;
					$op_link .= $this->_pLink($aux,$data['id'],$wdparent_id, $use_tree);
				endif;
				$aux = $_aux;
			endif;
			$i++;
		}
	
		return $op_link;
	}	
}

/* End of file controller_pages.php */
/* Location: ./application/controllers/controller_pages.php */