-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2016 at 06:14 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;

--
-- Database: `db_bss_sys`
--
CREATE DATABASE IF NOT EXISTS `db_bss_sys` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_bss_sys`;

-- --------------------------------------------------------

--
-- Table structure for table `data_asset`
--

DROP TABLE IF EXISTS `data_asset`;
CREATE TABLE IF NOT EXISTS `data_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_cat` varchar(256) DEFAULT NULL,
  `model_name` varchar(256) DEFAULT NULL,
  `model_manu` varchar(256) DEFAULT NULL,
  `serial_num` varchar(256) DEFAULT NULL,
  `asset_tag` varchar(256) DEFAULT NULL,
  `asset_state` varchar(256) DEFAULT NULL,
  `assigned_to` varchar(256) DEFAULT NULL,
  `asset_class` varchar(256) DEFAULT NULL,
  `asset_loc` varchar(256) DEFAULT NULL,
  `asset_dept` varchar(256) DEFAULT NULL,
  `assigned_date` date NOT NULL DEFAULT '0000-00-00',
  `installed_date` date NOT NULL DEFAULT '0000-00-00',
  `wxp_date` date NOT NULL DEFAULT '0000-00-00',
  `decommissioned_date` date NOT NULL DEFAULT '0000-00-00',
  `asset_specs` text,
  `comments` text,
  `last_update_by` varchar(256) DEFAULT NULL,
  `last_update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_asset`
--

INSERT INTO `data_asset` (`id`, `model_cat`, `model_name`, `model_manu`, `serial_num`, `asset_tag`, `asset_state`, `assigned_to`, `asset_class`, `asset_loc`, `asset_dept`, `assigned_date`, `installed_date`, `wxp_date`, `decommissioned_date`, `asset_specs`, `comments`, `last_update_by`, `last_update_date`) VALUES
(1, 'HARD DISK', 'ASDASD', 'ADOBE', 'SADASD', 'ASDASD', 'IN-USE', '', 'HARDWARE', 'BACK-UP SITE', 'BUILDING MANAGEMENT', '0000-00-00', '0000-00-00', '2016-08-02', '0000-00-00', '', NULL, 'ADMINISTRATOR', '2016-08-29 10:35:37'),
(2, 'DESKTOP', 'SDFSDF', 'ACER', 'N/A', 'DSFSDFSDSF', 'NEW', '', 'HARDWARE', 'MAIN SITE', 'INFORMATION TECHNOLOGY', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '<P>SDFSDFSDFSDF</P>', NULL, 'ADMINISTRATOR', '2016-08-29 10:34:40');

-- --------------------------------------------------------

--
-- Table structure for table `data_calendar`
--

DROP TABLE IF EXISTS `data_calendar`;
CREATE TABLE IF NOT EXISTS `data_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(11) DEFAULT NULL,
  `leave_date` date NOT NULL DEFAULT '0000-00-00',
  `filed_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `staff_name` varchar(256) DEFAULT NULL,
  `leave_status` varchar(25) NOT NULL DEFAULT 'PENDING_APPROVAL',
  `leave_reason` varchar(256) DEFAULT NULL,
  `approved_by` varchar(256) DEFAULT NULL,
  `approved_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_calendar`
--

INSERT INTO `data_calendar` (`id`, `c_id`, `leave_date`, `filed_date`, `staff_name`, `leave_status`, `leave_reason`, `approved_by`, `approved_date`) VALUES
(1, 1, '2016-08-29', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(2, 1, '2016-08-30', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(3, 1, '2016-08-31', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(4, 1, '2016-09-01', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(5, 1, '2016-09-04', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(6, 1, '2016-09-05', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(7, 1, '2016-09-06', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(8, 1, '2016-09-07', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(9, 1, '2016-09-08', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(10, 1, '2016-09-11', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(11, 1, '2016-09-12', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(12, 1, '2016-09-13', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(13, 1, '2016-09-14', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(14, 1, '2016-09-15', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(15, 1, '2016-09-18', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(16, 1, '2016-09-19', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(17, 1, '2016-09-20', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(18, 1, '2016-09-21', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(19, 1, '2016-09-22', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(20, 1, '2016-09-25', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(21, 1, '2016-09-26', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(22, 1, '2016-09-27', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(23, 1, '2016-09-28', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(24, 1, '2016-09-29', '2016-08-29 19:32:24', 'OMAR ONG', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(49, 3, '2016-08-29', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(50, 3, '2016-08-30', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(51, 3, '2016-08-31', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(52, 3, '2016-09-01', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(53, 3, '2016-09-04', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(54, 3, '2016-09-05', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(55, 3, '2016-09-06', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(56, 3, '2016-09-07', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(57, 3, '2016-09-08', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(58, 3, '2016-09-11', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(59, 3, '2016-09-12', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(60, 3, '2016-09-13', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(61, 3, '2016-09-14', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(62, 3, '2016-09-15', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(63, 3, '2016-09-18', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(64, 3, '2016-09-19', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(65, 3, '2016-09-20', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(66, 3, '2016-09-21', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(67, 3, '2016-09-22', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(68, 3, '2016-09-25', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(69, 3, '2016-09-26', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(70, 3, '2016-09-27', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(71, 3, '2016-09-28', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(72, 3, '2016-09-29', '2016-08-29 19:33:45', 'RONALDO RONQUILLO', 'APPROVED', 'ANNUAL LEAVE', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(158, 12, '2016-08-30', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(159, 12, '2016-08-31', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(160, 12, '2016-09-01', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(161, 12, '2016-09-04', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(162, 12, '2016-09-05', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(163, 12, '2016-09-06', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(164, 12, '2016-09-07', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(165, 12, '2016-09-08', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(166, 12, '2016-09-11', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(167, 12, '2016-09-12', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(168, 12, '2016-09-13', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(169, 12, '2016-09-14', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(170, 12, '2016-09-15', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(171, 12, '2016-09-18', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(172, 12, '2016-09-19', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(173, 12, '2016-09-20', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(174, 12, '2016-09-21', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00'),
(175, 12, '2016-09-22', '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', 'PENDING_APPROVAL', 'ANNUAL LEAVE', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `data_calendar_details`
--

DROP TABLE IF EXISTS `data_calendar_details`;
CREATE TABLE IF NOT EXISTS `data_calendar_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filed_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `staff_name` varchar(256) DEFAULT NULL,
  `s_date` date NOT NULL DEFAULT '0000-00-00',
  `e_date` date NOT NULL DEFAULT '0000-00-00',
  `leave_reason` varchar(256) DEFAULT NULL,
  `leave_status` varchar(256) DEFAULT NULL,
  `approved_by` varchar(156) DEFAULT NULL,
  `approved_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_calendar_details`
--

INSERT INTO `data_calendar_details` (`id`, `filed_date`, `staff_name`, `s_date`, `e_date`, `leave_reason`, `leave_status`, `approved_by`, `approved_date`) VALUES
(1, '2016-08-29 19:32:24', 'OMAR ONG', '2016-08-29', '2016-09-30', 'ANNUAL LEAVE', 'PENDING_APPROVAL', NULL, '2016-08-29 19:32:24'),
(3, '2016-08-29 19:33:45', 'RONALDO RONQUILLO', '2016-08-29', '2016-09-30', 'ANNUAL LEAVE', 'APPROVED', 'HESHAM IBRAHIM AL-BATLI', '2016-08-29 07:06:56'),
(12, '2016-08-30 08:49:32', 'EDWARD PAUL BRINAS', '2016-08-30', '2016-09-22', 'ANNUAL LEAVE', 'PENDING_APPROVAL', NULL, '2016-08-30 08:49:32');

-- --------------------------------------------------------

--
-- Table structure for table `data_incident`
--

DROP TABLE IF EXISTS `data_incident`;
CREATE TABLE IF NOT EXISTS `data_incident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tick_num` int(11) DEFAULT NULL,
  `s_num` varchar(25) DEFAULT NULL,
  `opened_date` datetime DEFAULT '0000-00-00 00:00:00',
  `closed_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rec_stat` varchar(256) DEFAULT NULL,
  `rec_type` varchar(256) DEFAULT NULL,
  `client_name` varchar(256) DEFAULT NULL,
  `created_by` varchar(256) DEFAULT NULL,
  `prio_level` varchar(256) DEFAULT NULL,
  `rec_site` varchar(256) DEFAULT NULL,
  `detail_desc` varchar(256) DEFAULT NULL,
  `problem_desc` text,
  `rec_attach` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_incident_details`
--

DROP TABLE IF EXISTS `data_incident_details`;
CREATE TABLE IF NOT EXISTS `data_incident_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ir_id` int(11) DEFAULT NULL,
  `updated_by` varchar(256) DEFAULT NULL,
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `action_desc` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_mon`
--

DROP TABLE IF EXISTS `data_mon`;
CREATE TABLE IF NOT EXISTS `data_mon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mon_date` date DEFAULT '0000-00-00',
  `mon_udate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mon_site` varchar(256) DEFAULT NULL,
  `created_by` varchar(256) DEFAULT NULL,
  `mon_rem` text,
  `rec_attach` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_service_req`
--

DROP TABLE IF EXISTS `data_service_req`;
CREATE TABLE IF NOT EXISTS `data_service_req` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ir_id` int(11) DEFAULT NULL,
  `s_num` varchar(20) DEFAULT NULL,
  `tick_num` int(11) DEFAULT NULL,
  `opened_date` datetime DEFAULT '0000-00-00 00:00:00',
  `closed_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rec_stat` varchar(256) DEFAULT NULL,
  `rec_type` varchar(256) DEFAULT NULL,
  `prio_level` varchar(256) DEFAULT NULL,
  `created_by` varchar(256) DEFAULT NULL,
  `assigned_to` varchar(256) DEFAULT NULL,
  `detail_desc` varchar(256) DEFAULT NULL,
  `problem_desc` text,
  `approved_by` varchar(256) DEFAULT NULL,
  `approved_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rec_attach` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_service_req`
--

INSERT INTO `data_service_req` (`id`, `ir_id`, `s_num`, `tick_num`, `opened_date`, `closed_date`, `rec_stat`, `rec_type`, `prio_level`, `created_by`, `assigned_to`, `detail_desc`, `problem_desc`, `approved_by`, `approved_date`, `rec_attach`) VALUES
(1, 0, 'SR-2016-08-00001', 1, '2016-08-30 12:20:31', '0000-00-00 00:00:00', 'APPROVED', 'HARDWARE - REQUEST FOR RE-CONFIGURATION', 'HIGH', 'edward.brinas', 'edward.brinas,ronaldo.ronquillo', 'Create cronjobs for linux maintenance', '<p>Create cronjobs for linux maintenance</p>', 'administrator', '2016-08-30 01:12:48', '');

-- --------------------------------------------------------

--
-- Table structure for table `data_service_req_details`
--

DROP TABLE IF EXISTS `data_service_req_details`;
CREATE TABLE IF NOT EXISTS `data_service_req_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sr_id` int(11) DEFAULT NULL,
  `updated_by` varchar(256) DEFAULT NULL,
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `action_desc` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_service_req_details`
--

INSERT INTO `data_service_req_details` (`id`, `sr_id`, `updated_by`, `update_date`, `action_desc`) VALUES
(1, 1, 'edward.brinas', '2016-08-30 12:20:31', '<p>Need this asap</p>'),
(2, 1, 'administrator', '2016-08-30 01:12:48', '<p>Service request approved.</p>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
