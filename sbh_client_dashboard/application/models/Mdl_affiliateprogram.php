<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_affiliateprogram extends CI_Model {

  function __construct()  {
      parent::__construct();
      $this->db = $this->load->database('main',TRUE);
  }

	/**
	 * Function to clean user input.
	 *
	 */
	private function _cleanRecord($options=array()) {
		foreach($options as $key=>$value) {
			$clean_array[$key] = addslashes(trim(strip_tags($value)));
		}
		return $clean_array;
	}

	/**
	 * Function Name
	 * ----------------
	 * AddRecord - Creates a record to database.
	 *
	 * Options: Array
	 * -----------------
	 * usr_id = Number
	 * address = String
	 * city = String
	 * state = String
	 * postalcode = String
	 *
	 * return numeric
	 *
	 */
	public function AddRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO affiliateprogram (usr_id,address,city,state,postalcode,ref_by)
				VALUES ({$this->db->escape($options['usr_id'])},
						{$this->db->escape($options['address'])},
						{$this->db->escape($options['city'])},
						{$this->db->escape($options['state'])},
						{$this->db->escape($options['postalcode'])},
            {$this->db->escape($options['ref_by'])})";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * UpdateRecord- Updates a record in the database.
	 *
	 * Options: Array
	 * -----------------
   * usr_id = Number
	 * address = String
	 * city = String
	 * state = String
	 * postalcode = String
	 *
	 * return numeric
	 *
	 */

	public function UpdateRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE affiliateprogram SET ";
		$sql .= !empty($options['usr_id']) ? " usr_id = {$this->db->escape($options['usr_id'])}, " : " ";
		$sql .= !empty($options['city']) ? " city = {$this->db->escape($options['city'])}, " : " ";
		$sql .= !empty($options['state']) ? " state = {$this->db->escape($options['state'])}, " : " ";
		$sql .= !empty($options['postalcode']) ? " postalcode = {$this->db->escape($options['postalcode'])}, " : " ";
		$sql = rtrim(trim($sql), ",");
		$sql .= "  WHERE id = {$options['id']}  ";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetAllRecords- Retrieves all records in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 * Order By = String
	 * Limit = Number
	 * Offset = Number
	 *
	 * return Array
	 *
	 */
	public function GetAllRecords($options=array()) {
		$sql = "SELECT a.*,b.* FROM users a, affiliateprogram b WHERE a.id = b.usr_id ";
		$sql .= !empty($options['search_by']) ? strtoupper($options['search_by']) : " ";
		$sql .= !empty($options['order_by']) ? strtoupper($options['order_by'])." " : " ORDER BY a.id DESC ";
    $sql .= !empty($options['limit']) ? "LIMIT ".$options['limit'] : " ";
    $sql .= !empty($options['offset']) ? "OFFSET ".$options['offset'] : " ";
		$rs = $this->db->query(trim($sql));
		return $rs->result_array();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetSingleRecord - Retrieve a single record in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 *
	 * return Arrays
	 *
	 */
	public function GetSingleRecord($options=array()) {
		$sql = "SELECT * FROM affiliateprogram ";
		$sql .= !empty($options['search_by']) ? strtoupper($options['search_by']) : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query(trim($sql));
		return $rs->row();
	}

}

/* End of file Mdl_affiliateprogram.php */
/* Location: ./application/model/Mdl_affiliateprogram.php */
