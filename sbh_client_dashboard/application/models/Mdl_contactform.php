<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_contactform extends CI_Model {

	function __construct()  {
        parent::__construct();
		$this->db = $this->load->database('main',TRUE);
    }

	/**
	 * Function to clean user input.
	 *$this->db->escape
	 */
	private function _cleanRecord($options=array()) {
		foreach($options as $key=>$value) {
			$clean_array[$key] = addslashes(trim(strip_tags($value)));
		}
		return $clean_array;
	}
	
	/**
	 * Function Name
	 * ----------------
	 * AddRecord - Creates a record to database.
	 *
	 * Options: Array
	 * -----------------
	 * firstname = String
	 * lastname = String
	 * phone = String
	 * email = String
	 * messages = String
	 * contacted = String
	 * notified = String
	 * noted = String
	 *
	 * return numeric
	 *
	 */
	public function AddRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO contactform (firstname,lastname,phone,email,message,contacted,notified,notes)
				VALUES ('{$options['firstname']}',
						'{$options['lastname']}',
						'{$options['phone']}',
						'{$options['email']}',
						'{$options['message']}',
						'{$options['contacted']}',
						'{$options['notified']}',
						'{$options['notes']}')";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * UpdateRecord- Updates a record in the database.
	 *
	 * Options: Array
	 * -----------------
	 * id = Numeric
	 * firstname = String
	 * lastname = String
	 * phone = String
	 * email = String
	 * messages = String
	 * contacted = String
	 * notified = String
	 * noted = String
	 *
	 * return numeric
	 *
	 */
	public function UpdateRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE contactform
				SET ";
		$sql .= !empty($options['firstname']) ? " firstname = '{$options['firstname']}', " : " ";
		$sql .= !empty($options['lastname']) ? " lastname = '{$options['lastname']}', " : " ";
		$sql .= !empty($options['phone']) ? " phone = '{$options['phone']}', " : " ";
		$sql .= !empty($options['email']) ? " email = '{$options['email']}', " : " ";
		$sql .= !empty($options['message']) ? " message = '{$options['message']}', " : " ";
		$sql .= !empty($options['contacted']) ? " contacted = '{$options['contacted']}', " : " ";
		$sql .= !empty($options['notified']) ? " notified = '{$options['notified']}', " : " ";
		$sql .= !empty($options['notes']) ? " notes = '{$options['notes']}', " : " ";
		$sql = rtrim(trim($sql), ",");
		$sql .= " WHERE id = {$options['id']} ";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetAllRecords- Retrieves all records in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 * Order By = String
	 * Limit = Number
	 * Offset = Number
	 *
	 * return Array
	 *
	 */
	public function GetAllRecords($options=array()) {
		$sql = "SELECT * FROM contactform ";
		$sql .= !empty($options['search_by']) ? strtoupper($options['search_by']) : " ";
		$sql .= !empty($options['order_by']) ? strtoupper($options['order_by'])." " : " ORDER BY id DESC ";
        $sql .= !empty($options['limit']) ? "LIMIT ".$options['limit'] : " ";
        $sql .= !empty($options['offset']) ? "OFFSET ".$options['offset'] : " ";
		$rs = $this->db->query(trim($sql));
		return $rs->result_array();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetSingleRecord - Retrieve a single record in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 *
	 * return Arrays
	 *
	 */
	public function GetSingleRecord($options=array()) {
		$sql = "SELECT * FROM contactform ";
		$sql .= !empty($options['search_by']) ? strtoupper($options['search_by']) : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query(trim($sql));
		return $rs->row();
	}
}

/* End of file Mdl_contactform.php */
/* Location: ./application/model/Mdl_contactform.php */
