<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_users extends CI_Model {

	function __construct()  {
        parent::__construct();
		$this->db = $this->load->database('main',TRUE);
    }

	/**
	 * Function to clean user input.
	 *
	 */
	private function _cleanRecord($options=array()) {
		foreach($options as $key=>$value) {
			$clean_array[$key] = addslashes(trim(strip_tags($value)));
		}
		return $clean_array;
	}

	/**
	 * Function Name
	 * ----------------
	 * AddRecord - Creates a record to database.
	 *
	 * Options: Array
	 * -----------------
	 * firstname = String
	 * lastname = String
	 * position = String
	 * phone = String
	 * companyname = String
	 * username = String
	 * email = String
	 * password = String
	 * adminlevel = String
	 * active = String
	 * ref_id = String
	 *
	 * return numeric
	 *
	 */
	public function AddRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO users (firstname,lastname,position,phone,companyname,username,email,password,adminlevel,active,ref_id)
				VALUES ({$this->db->escape($options['firstname'])},
						{$this->db->escape($options['lastname'])},
						{$this->db->escape($options['position'])},
						{$this->db->escape($options['phone'])},
						{$this->db->escape($options['companyname'])},
						{$this->db->escape($options['username'])},
						{$this->db->escape($options['email'])},
						{$this->db->escape($options['password'])},
						{$this->db->escape($options['adminlevel'])},
						{$this->db->escape($options['active'])},
						{$this->db->escape($options['ref_id'])})";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * UpdateRecord- Updates a record in the database.
	 *
	 * Options: Array
	 * -----------------
	 * id = numeric
	 * firstname = String
	 * lastname = String
	 * position = String
	 * phone = String
	 * companyname = String
	 * username = String
	 * email = String
	 * password = String
	 * adminlevel = String
	 * active = String
	 *
	 * return numeric
	 *
	 */

	public function UpdateRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE users SET ";
		$sql .= !empty($options['firstname']) ? " firstname = {$this->db->escape($options['firstname'])}, " : " ";
		$sql .= !empty($options['lastname']) ? " lastname = {$this->db->escape($options['lastname'])}, " : " ";
		$sql .= !empty($options['position']) ? " position = {$this->db->escape($options['position'])}, " : " ";
		$sql .= !empty($options['phone']) ? " phone = {$this->db->escape($options['phone'])}, " : " ";
		$sql .= !empty($options['companyname']) ? " companyname = {$this->db->escape($options['companyname'])}, " : " ";
		$sql .= !empty($options['username']) ? " username = {$this->db->escape($options['username'])}, " : " ";
		$sql .= !empty($options['email']) ? " email = {$this->db->escape($options['email'])}, " : " ";
		$sql .= !empty($options['password']) ? " password = {$this->db->escape($options['password'])}, " : " ";
		$sql .= !empty($options['adminlevel']) ? " adminlevel = {$this->db->escape($options['adminlevel'])}, " : " ";
		$sql .= !empty($options['active']) ? " active = {$this->db->escape($options['active'])}, " : " ";
		$sql = rtrim(trim($sql), ",");
		$sql .= "  WHERE id = {$options['id']}  ";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetAllRecords- Retrieves all records in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 * Order By = String
	 * Limit = Number
	 * Offset = Number
	 *
	 * return Array
	 *
	 */
	public function GetAllRecords($options=array()) {
		$sql = "SELECT * FROM users ";
		$sql .= !empty($options['search_by']) ? strtoupper($options['search_by']) : " ";
		$sql .= !empty($options['order_by']) ? strtoupper($options['order_by'])." " : " ORDER BY id DESC ";
        $sql .= !empty($options['limit']) ? "LIMIT ".$options['limit'] : " ";
        $sql .= !empty($options['offset']) ? "OFFSET ".$options['offset'] : " ";
		$rs = $this->db->query(trim($sql));
		return $rs->result_array();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetSingleRecord - Retrieve a single record in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 *
	 * return Arrays
	 *
	 */
	public function GetSingleRecord($options=array()) {
		$sql = "SELECT * FROM users ";
		$sql .= !empty($options['search_by']) ? strtoupper($options['search_by']) : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query(trim($sql));
		return $rs->row();
	}

	/**
	 * Function Name
	 * ----------------
	 * DeleteRecord - Retrieve a single record in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * id = Numeric
	 *
	 * return String
	 *
	 */
	public function DeleteRecord($options=array()) {

		$options = $this->_cleanRecord($options);

		$sql = "DELETE FROM users ";
		$sql .= "WHERE id = {$this->db->escape($options['id'])} ";
		$rs = $this->db->query(trim($sql));
		return $rs->affected_rows();
	}
}

/* End of file Mdl_users.php */
/* Location: ./application/model/Mdl_users.php */
