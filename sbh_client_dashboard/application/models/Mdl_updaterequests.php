<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_updaterequests extends CI_Model {

	function __construct()  {
        parent::__construct();
		$this->db = $this->load->database('offsite',TRUE);
    }

	/**
	 * Function to clean user input.
	 *$this->db->escape
	 */
	private function _cleanRecord($options=array()) {
		foreach($options as $key=>$value) {
			$clean_array[$key] = addslashes(trim(strip_tags($value)));
		}
		return $clean_array;
	}

	/**
	 * Function Name
	 * ----------------
	 * AddRecord - Creates a record to database.
	 *
	 * Options: Array
	 * -----------------
	 * firstname = String
	 * lastname = String
	 * phone = String
	 * email = String
	 * messages = String
	 * contacted = String
	 * notified = String
	 * noted = String
	 *
	 * return numeric
	 *
	 */
	public function AddRecord($options=array())
	{
		$options = $this->_cleanRecord($options);
		
		$numberpages = !empty($options['numberpages']) ? $options['numberpages'] : 0;
		$notes = !empty($options['notes']) ? $options['notes'] : "N/A";
		
		$sql = "INSERT INTO updaterequests (companyname,updatetype,numberpages,pagechange,changedetails,notes,completed,notified)
				VALUES ({$this->db->escape($options['companyname'])},
						{$this->db->escape($options['updatetype'])},
						{$this->db->escape($numberpages)},
						{$this->db->escape($options['pagechange'])},
						{$this->db->escape($options['changedetails'])},
						{$this->db->escape($notes)},
						{$this->db->escape($options['completed'])},
						{$this->db->escape($options['notified'])})";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * UpdateRecord- Updates a record in the database.
	 *
	 * Options: Array
	 * -----------------
	 * id = Numeric
	 * firstname = String
	 * lastname = String
	 * phone = String
	 * email = String
	 * messages = String
	 * contacted = String
	 * notified = String
	 * noted = String
	 *
	 * return numeric
	 *
	 */

	public function UpdateRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE updaterequests
				SET ";
		$sql .= !empty($options['companyname']) ? " companyname = {$this->db->escape($options['companyname'])}, " : " ";
		$sql .= !empty($options['updatetype']) ? " updatetype = {$this->db->escape($options['updatetype'])}, " : " ";
		$sql .= !empty($options['numberpages']) ? " numberpages = {$this->db->escape($options['numberpages'])}, " : " ";
		$sql .= !empty($options['pagechange']) ? " pagechange = {$this->db->escape($options['pagechange'])}, " : " ";
		$sql .= !empty($options['changedetails']) ? " changedetails = {$this->db->escape($options['changedetails'])}, " : " ";
		$sql .= !empty($options['notes']) ? " notes = {$this->db->escape($options['notes'])}, " : " ";
		$sql .= !empty($options['completed']) ? " completed = {$this->db->escape($options['completed'])}, " : " ";
		$sql .= !empty($options['notified']) ? " notified = {$this->db->escape($options['notified'])}, " : " ";
		$sql = rtrim(trim($sql), ",");
		$sql .= "  WHERE id = {$options['id']}  ";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetAllRecords- Retrieves all records in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 * Order By = String
	 * Limit = Number
	 * Offset = Number
	 *
	 * return Array
	 *
	 */
	public function GetAllRecords($options=array()) {
		$sql = "SELECT * FROM updaterequests ";
		$sql .= !empty($options['search_by']) ? $options['search_by'] : " ";
		$sql .= !empty($options['order_by']) ? $options['order_by']." " : " ORDER BY id DESC ";
        $sql .= !empty($options['limit']) ? "LIMIT ".$options['limit'] : " ";
        $sql .= !empty($options['offset']) ? "OFFSET ".$options['offset'] : " ";
		$rs = $this->db->query(trim($sql));
		return $rs->result_array();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetSingleRecord - Retrieve a single record in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 *
	 * return Arrays
	 *
	 */
	public function GetSingleRecord($options=array()) {
		$sql = "SELECT * FROM updaterequests ";
		$sql .= !empty($options['search_by']) ? strtoupper($options['search_by']) : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query(trim($sql));
		return $rs->row();
	}
}

/* End of file Mdl_updaterequests.php */
/* Location: ./application/model/Mdl_updaterequests.php */