<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_payments extends CI_Model {

	function __construct()  {
    parent::__construct();
    $this->db = $this->load->database('payments',TRUE);
  }

	/**
	 * Function to clean user input.
	 *$this->db->escape
	 */
	private function _cleanRecord($options=array()) {
		foreach($options as $key=>$value) {
			$clean_array[$key] = addslashes(trim(strip_tags($value)));
		}
		return $clean_array;
	}


	/**
	 * Function Name
	 * ----------------
	 * GetAllRecords- Retrieves all records in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 * Order By = String
	 * Limit = Number
	 * Offset = Number
	 *
	 * return Array
	 *
	 */
	public function GetAllRecords($options=array()) {
		$sql = "SELECT * FROM registrations a, trx_payment b WHERE a.registrantid = b.registrantid ";
		$sql .= !empty($options['search_by']) ? $options['search_by'] : " ";
		$sql .= !empty($options['order_by']) ? strtoupper($options['order_by'])." " : " ORDER BY id DESC ";
    $sql .= !empty($options['limit']) ? "LIMIT ".$options['limit'] : " ";
    $sql .= !empty($options['offset']) ? "OFFSET ".$options['offset'] : " ";
		$rs = $this->db->query(trim($sql));
		return $rs->result_array();
	}

}

/* End of file Mdl_payments.php */
/* Location: ./application/model/Mdl_payments.php */
