<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_signups extends CI_Model {

	function __construct()  {
        parent::__construct();
		$this->db = $this->load->database('offsite',TRUE);
    }

	/**
	 * Function to clean user input.
	 *$this->db->escape
	 */
	private function _cleanRecord($options=array()) {
		foreach($options as $key=>$value) {
			$clean_array[$key] = addslashes(trim(strip_tags($value)));
		}
		return $clean_array;
	}

	/**
	 * Function Name
	 * ----------------
	 * AddRecord - Creates a record to database.
	 *
	 * Options: Array
	 * -----------------
	 * companyname - String
	 * firstname - String
	 * lastname - String
	 * email - String
	 * phone - String
	 * address1 - String
	 * address2 - String
	 * city - String
	 * state - String
	 * zip - Int
	 * template - String
	 * domain - String
	 * domainregistrar - String
	 * url - String
	 * hosting - String
	 * hostingprovider - String
	 * bio - String
	 * finished - String
	 * activecustomer - String
	 * monthlyrevenue - Int
	 * plan - String
	 * idx - String
	 * industry - String
	 * paymenttype - String
	 * annualrevenue - Int
	 * seen - String
	 * referredby - Int
	 *
	 * return numeric
	 *
	 */
	public function AddRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO signups (companyname,
									firstname,
									lastname,
									email,
									phone,
									address1,
									address2,
									city,
									state,
									zip,
									template,
									domain,
									domainregistrar,
									url,
									hosting,
									hostingprovider,
									bio,
									finished,
									activecustomer,
									monthlyrevenue,
									plan,
									idx,
									industry,
									paymenttype,
									annualrevenue,
									seen,
									referredby)
				VALUES ({$this->db->escape($options['companyname'])},
						{$this->db->escape($options['firstname'])},
						{$this->db->escape($options['lastname'])},
						{$this->db->escape($options['email'])},
						{$this->db->escape($options['phone'])},
						{$this->db->escape($options['address1'])},
						{$this->db->escape($options['address2'])},
						{$this->db->escape($options['city'])},
						{$this->db->escape($options['state'])},
						{$this->db->escape($options['zip'])},
						{$this->db->escape($options['template'])},
						{$this->db->escape($options['domain'])},
						{$this->db->escape($options['domainregistrar'])},
						{$this->db->escape($options['url'])},
						{$this->db->escape($options['hosting'])},
						{$this->db->escape($options['hostingprovider'])},
						{$this->db->escape($options['bio'])},
						{$this->db->escape($options['finished'])},
						{$this->db->escape($options['activecustomer'])},
						{$this->db->escape($options['monthlyrevenue'])},
						{$this->db->escape($options['plan'])},
						{$this->db->escape($options['idx'])},
						{$this->db->escape($options['industry'])},
						{$this->db->escape($options['paymenttype'])},
						{$this->db->escape($options['annualrevenue'])},
						{$this->db->escape($options['seen'])},
						{$this->db->escape($options['referredby'])})";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * UpdateRecord- Updates a record in the database.
	 *
	 * Options: Array
	 * -----------------
	 * companyname - String
	 * firstname - String
	 * lastname - String
	 * email - String
	 * phone - String
	 * address1 - String
	 * address2 - String
	 * city - String
	 * state - String
	 * zip - Int
	 * template - String
	 * domain - String
	 * domainregistrar - String
	 * url - String
	 * hosting - String
	 * hostingprovider - String
	 * bio - String
	 * finished - String
	 * activecustomer - String
	 * monthlyrevenue - Int
	 * plan - String
	 * idx - String
	 * industry - String
	 * paymenttype - String
	 * annualrevenue - Int
	 * seen - String
	 * referredby - Int
	 *
	 * return numeric
	 *
	 */
	public function UpdateRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE signups
				SET ";
		$sql .= !empty($options['companyname']) ? " companyname = {$this->db->escape($options['companyname'])}, " : " ";
		$sql .= !empty($options['firstname']) ? " firstname = {$this->db->escape($options['firstname'])}, " : " ";
		$sql .= !empty($options['lastname']) ? " lastname = {$this->db->escape($options['lastname'])}, " : " ";
		$sql .= !empty($options['email']) ? " email = {$this->db->escape($options['email'])}, " : " ";
		$sql .= !empty($options['phone']) ? " phone = {$this->db->escape($options['phone'])}, " : " ";
		$sql .= !empty($options['address1']) ? " address1 = {$this->db->escape($options['address1'])}, " : " ";
		$sql .= !empty($options['address2']) ? " address2 = {$this->db->escape($options['address2'])}, " : " ";
		$sql .= !empty($options['city']) ? " city = {$this->db->escape($options['city'])}, " : " ";
		$sql .= !empty($options['state']) ? " state = {$this->db->escape($options['state'])}, " : " ";
		$sql .= !empty($options['zip']) ? " zip = {$this->db->escape($options['zip'])}, " : " ";
		$sql .= !empty($options['template']) ? " template = {$this->db->escape($options['template'])}, " : " ";
		$sql .= !empty($options['domain']) ? " domain = {$this->db->escape($options['domain'])}, " : " ";
		$sql .= !empty($options['domainregistrar']) ? " domainregistrar = {$this->db->escape($options['domainregistrar'])}, " : " ";
		$sql .= !empty($options['url']) ? " url = {$this->db->escape($options['url'])}, " : " ";
		$sql .= !empty($options['hosting']) ? " hosting = {$this->db->escape($options['hosting'])}, " : " ";
		$sql .= !empty($options['hostingprovider']) ? " hostingprovider = {$this->db->escape($options['hostingprovider'])}, " : " ";
		$sql .= !empty($options['bio']) ? " bio = {$this->db->escape($options['bio'])}, " : " ";
		$sql .= !empty($options['finished']) ? " finished = {$this->db->escape($options['finished'])}, " : " ";
		$sql .= !empty($options['activecustomer']) ? " activecustomer = {$this->db->escape($options['activecustomer'])}, " : " ";
		$sql .= !empty($options['monthlyrevenue']) ? " monthlyrevenue = {$this->db->escape($options['monthlyrevenue'])}, " : " ";
		$sql .= !empty($options['plan']) ? " plan = {$this->db->escape($options['plan'])}, " : " ";
		$sql .= !empty($options['idx']) ? " idx = {$this->db->escape($options['idx'])}, " : " ";
		$sql .= !empty($options['industry']) ? " industry = {$this->db->escape($options['industry'])}, " : " ";
		$sql .= !empty($options['paymenttype']) ? " paymenttype = {$this->db->escape($options['paymenttype'])}, " : " ";
		$sql .= !empty($options['annualrevenue']) ? " annualrevenue = {$this->db->escape($options['annualrevenue'])}, " : " ";
		$sql .= !empty($options['seen']) ? " seen = {$this->db->escape($options['seen'])}, " : " ";
		$sql .= !empty($options['referredby']) ? " referredby = {$this->db->escape($options['referredby'])}, " : " ";
		$sql = rtrim(trim($sql), ",");
		$sql .= "  WHERE id = {$options['id']}  ";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetAllRecords- Retrieves all records in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 * Order By = String
	 * Limit = Number
	 * Offset = Number
	 *
	 * return Array
	 *
	 */
	public function GetAllRecords($options=array()) {
		$sql = "SELECT * FROM signups ";
		$sql .= !empty($options['search_by']) ? $options['search_by'] : " ";
		$sql .= !empty($options['order_by']) ? $options['order_by']." " : " ORDER BY id DESC ";
        $sql .= !empty($options['limit']) ? "LIMIT ".$options['limit'] : " ";
        $sql .= !empty($options['offset']) ? "OFFSET ".$options['offset'] : " ";
		$rs = $this->db->query(trim($sql));
		return $rs->result_array();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetSingleRecord - Retrieve a single record in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 *
	 * return Arrays
	 *
	 */
	public function GetSingleRecord($options=array()) {
		$sql = "SELECT * FROM signups ";
		$sql .= !empty($options['search_by']) ? strtoupper($options['search_by']) : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query(trim($sql));
		return $rs->row();
	}
}

/* End of file Mdl_signups.php */
/* Location: ./application/model/Mdl_signups.php */