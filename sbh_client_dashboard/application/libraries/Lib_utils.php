<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lib_utils {

	private $sms_token = "e442173029d03d54b957b1f39933460d";
	private $sms_sid = "ACe43acaee974fca9a8399f1dfd6e82a83";
	private $sms_from = "+17044132425";

	/*
	 * _cleanRecord method encrypts a string.
	 *
	 * Option: Array
	 * -----------------
	 * key - String
	 *
	 * return Array
	 *
	 */
	public function _cleanRecord($options=array()) {
		foreach($options as $key=>$value) {
			$clean_array[$key] = trim(strip_tags($value));
		}
		return $clean_array;
	}

	/*
	 * _encryptStr method encrypts a string.
	 *
	 * Option: Array
	 * -----------------
	 * str - String
	 * hash - String
	 *
	 * return String
	 *
	 */
	public function _encryptStr($options=array()) {

		$dirty = array("+","/","=","-");
		$clean = array("_PLUS_","_SLASH_","_EQUAL_","_DASH_");
		$key = hash('sha256',$options['hash']);
		$iv = substr(hash('sha256',$options['hash']),0,16);

		return str_replace($dirty,$clean,base64_encode(openssl_encrypt($options['str'],"AES-256-CBC",$key,0,$iv)));
	}

	/*
	 * _decryptStr method decrypts a string.
	 *
	 * Option: Array
	 * -----------------
	 * str - String
	 * hash - String
	 *
	 * return String
	 *
	 */
	public function _decryptStr($options=array()) {

		$dirty = array("+","/","=","-");
		$clean = array("_PLUS_","_SLASH_","_EQUAL_","_DASH_");
		$key = hash('sha256',$options['hash']);
		$iv = substr(hash('sha256',$options['hash']),0,16);

		return openssl_decrypt(base64_decode(str_replace($clean,$dirty,$options['str'])),"AES-256-CBC",$key,0,$iv);
	}

	/*
	 * _genRandStr method generates a random string.
	 *
	 * Option: None
	 * -----------------
	 *
	 * return String
	 *
	 */
	public function _genRandStr() {
		return substr(str_shuffle("abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789"),0,5);
	}

	/*
	 * _genUsStates method generates an array of US States.
	 *
	 * Option: None
	 * -----------------
	 *
	 * return array
	 *
	 */
	public function _genUsStates() {

		return array('AL'=>'Alabama',
									'AK'=>'Alaska',
									'AZ'=>'Arizona',
									'AR'=>'Arkansas',
									'CA'=>'California',
									'CO'=>'Colorado',
									'CT'=>'Connecticut',
									'DE'=>'Delaware',
									'DC'=>'District Of Columbia',
									'FL'=>'Florida',
									'GA'=>'Georgia',
									'HI'=>'Hawaii',
									'ID'=>'Idaho',
									'IL'=>'Illinois',
									'IN'=>'Indiana',
									'IA'=>'Iowa',
									'KS'=>'Kansas',
									'KY'=>'Kentucky',
									'LA'=>'Louisiana',
									'ME'=>'Maine',
									'MD'=>'Maryland',
									'MA'=>'Massachusetts',
									'MI'=>'Michigan',
									'MN'=>'Minnesota',
									'MS'=>'Mississippi',
									'MO'=>'Missouri',
									'MT'=>'Montana',
									'NE'=>'Nebraska',
									'NV'=>'Nevada',
									'NH'=>'New Hampshire',
									'NJ'=>'New Jersey',
									'NM'=>'New Mexico',
									'NY'=>'New York',
									'NC'=>'North Carolina',
									'ND'=>'North Dakota',
									'OH'=>'Ohio',
									'OK'=>'Oklahoma',
									'OR'=>'Oregon',
									'PA'=>'Pennsylvania',
									'RI'=>'Rhode Island',
									'SC'=>'South Carolina',
									'SD'=>'South Dakota',
									'TN'=>'Tennessee',
									'TX'=>'Texas',
									'UT'=>'Utah',
									'VT'=>'Vermont',
									'VA'=>'Virginia',
									'WA'=>'Washington',
									'WV'=>'West Virginia',
									'WI'=>'Wisconsin',
									'WY'=>'Wyoming'
								);
	}

	/*
	 * _captchaSettings method will be used for captcha image verification
	 *
	 * Option: None
	 * -----------------
	 *
	 * return array
	 *
	 */
	public function _captchaSettings() {

		return array(
			'word_length' => 4,
            'img_path'   => './captcha_images/',
            'img_url'    => site_url().'captcha_images/',
            'font_path'  => BASEPATH.'system/fonts/',
            'fonts'      => array('textb.ttf'),
            'img_width'  => 200,
            'img_height' => 50,
            'expiration' => 7200
        );
	}

	/*
	 * _sendSMS method will be used for sending SMS notifications
	 *
	 * Option: Array
	 * -----------------
	 *
	 * sid = string
	 * token = string
	 * to = string
	 * from = string
	 * body = string
	 *
	 * return array
	 *
	 */
	function _sendSMS($options=array()) {

	    $uri = 'https://api.twilio.com/2010-04-01/Accounts/' . $this->sms_sid . '/Messages';

	    $fields =
	        '&To=' .  urlencode( $options['to'] ) .
	        '&From=' . urlencode( $this->sms_from ) .
	        '&Body=' . urlencode( $options['body'] );

	    $res = curl_init();
	    curl_setopt( $res, CURLOPT_URL, $uri );
	    curl_setopt( $res, CURLOPT_POST, 3 );
	    curl_setopt( $res, CURLOPT_POSTFIELDS, $fields );
	    curl_setopt( $res, CURLOPT_USERPWD, $this->sms_sid.':'.$this->sms_token );
	    curl_setopt( $res, CURLOPT_RETURNTRANSFER, true );
			$output = curl_exec($res);
			curl_close($res);

			$doc = simplexml_load_string($output);
			$resultnode = $doc->xpath('//RestException');

			return array('err_code'=>$resultnode[0]->Code,'err_msg'=>$resultnode[0]->Message);
	}
}

/* End of file Lib_utils.php */
