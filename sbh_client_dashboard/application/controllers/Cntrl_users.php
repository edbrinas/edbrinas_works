<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cntrl_users extends CI_Controller {

	private $controller_main = 'cntrl_users/';

	public function __construct() {
        parent::__construct();

		// Load all libraries and models on page load
		$this->load->library('Lib_utils');
		$this->load->model('Mdl_users');
		$this->load->model('Mdl_contactform');
		$this->load->model('Mdl_updaterequests');

		$this->eKey = $this->session->userdata('encryption_key');
    }

	/*
	 * CheckSession method checks if the user is currently logged in.
	 *
	 * Option: Value
	 * -----------------
	 * None
	 *
	 * return bool
	 *
	 */
	private function _CheckSession() {
	   $is_logged = $this->session->userdata('userSession');
	   return ($is_logged) ? TRUE : FALSE;
	}

	/**
	 * Index Page for this controller.
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return Page
	 *
	 */
	public function index() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// If post parameters are sent
			if (isset($_POST['submit'])) {

				// Clean user input
				$clean_post = $this->lib_utils->_cleanRecord($_POST);

				// Check if email exists
				$a_users = $this->Mdl_users->GetAllRecords(array('search_by'=>' WHERE email = '.$this->db->escape($clean_post['email']).' '));
				if (count($a_users)!=0) {
					$data['msg'] = "<div class='alert alert-danger'><span class='glyphicon glyphicon-info-sign'></span> &nbsp; Sorry email already taken !</div>";
				} else {
					$ref_id = $this->lib_utils->_genRandStr(); // Generate random referral id
					$hashed_password = password_hash($clean_post['password'], PASSWORD_DEFAULT); // Hashed the password
					unset($clean_post['password']); // Remove existing password in the array
					$clean_post = array_merge($clean_post,array('password'=>$hashed_password,'ref_id'=>$ref_id)); //Add the hashed password and referral id to the array
					// Add records
					$t_id = $this->Mdl_users->AddRecord($clean_post);
					// Send successful message
					$data['msg'] = is_numeric($t_id) ? "<div class='alert alert-success'><span class='glyphicon glyphicon-info-sign'></span> &nbsp; Successfully registered !</div>" : "<div class='alert alert-danger'><span class='glyphicon glyphicon-info-sign'></span> &nbsp; Error while registering !</div>";
				}
			}

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			$data['a_users'] = $this->Mdl_users->GetAllRecords(array('search_by'=>' WHERE companyname = "'.$this->session->userdata('companyname').'" '));

			foreach ($data['a_users'] as $row)	{
				// Encrypt record id
				unset($r_id);
				$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));

				$a_currentuserstable .= "<tr>";
				$a_currentuserstable .= "<td>". $row["firstname"] . " " . $row["lastname"] . "</td>";
				$a_currentuserstable .= "<td><a href='".base_url()."Cntrl_users/EditRecord/{$r_id}'>Edit User</a></td>";
				$a_currentuserstable .= "</tr>";
			}

			$data['a_currentuserstable'] = $a_currentuserstable;
			$this->load->view('pages/users/_list',$data);
		}
	}

	/**
	 * EditRecord function to modify record on the database.
	 *
	 * Option: String
	 * -----------------
	 * id - String
	 *
	 * return String
	 *
	 */
	public function EditRecord($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// Checks for $id encryption
			if (!empty($id)) {
				$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
				($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;
			}

			// If post parameters are sent
			if (isset($_POST['updatedetails'])) {
				// Clean user input
				$clean_post = $this->lib_utils->_cleanRecord($_POST);

				// Remove encrypted ID and replace it with valid ID.
				unset($clean_post['id']);
				$clean_post = array_merge($clean_post,array('id'=>$id));
				// Update records
				$t_id = $this->Mdl_users->UpdateRecord($clean_post);
				// Send successful message
				$data['msg'] = is_numeric($t_id) ? "<p>Changes Successfully Saved.</p>" : "<p>Error Updating Record. Please contact site Administrator</p>";
			}

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For users table
			$data['a_users'] = $this->Mdl_users->GetAllRecords(array('search_by'=>' WHERE id = "'.$id.'" '));

			foreach ($data['a_users'] as $row) {
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Name</td><td>" . $row["firstname"]. " " . $row["lastname"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Position</td><td>" . $row["position"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Username</td><td>" . $row["username"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Phone</td><td>" . $row["phone"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Email</td><td>" . $row["email"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company Name</td><td>" . $row["companyname"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Admin Level</td><td>" . $row["adminlevel"]. "</p></td>";
				$a_profiledetails .= "</tr>";
			}


			$data['id'] = $this->lib_utils->_encryptStr(array('str'=>$id,'hash'=>$this->eKey));
			$data['a_profiledetails'] = $a_profiledetails;
			$this->load->view('pages/users/_edit',$data);
		}
	}

	/**
	 * EditRecord function to modify record on the database.
	 *
	 * Option: String
	 * -----------------
	 * id - String
	 *
	 * return String
	 *
	 */
	public function DeleteRecord($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// Checks for $id encryption
			if (!empty($id)) {
				$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
				($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;
			}

			// If post parameters are sent
			if (isset($_POST['deleteuser'])) {
				// Remove encrypted ID and replace it with valid ID.
				unset($_POST['id']);
				$_POST = array_merge($_POST,array('id'=>$id));
				// Update records
				$t_id = $this->Mdl_users->UpdateRecord($_POST);
				// Send successful message
				$data['msg'] = is_numeric($t_id) ? "<p>Changes Successfully Saved.</p>" : "<p>Error Updating Record. Please contact site Administrator</p>";
			}

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For users table
			$data['a_users'] = $this->Mdl_users->GetAllRecords(array('search_by'=>' WHERE id = "'.$id.'" '));

			foreach ($data['a_users'] as $row) {
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Name</td><td>" . $row["firstname"]. " " . $row["lastname"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Position</td><td>" . $row["position"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Username</td><td>" . $row["username"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Phone</td><td>" . $row["phone"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Email</td><td>" . $row["email"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company Name</td><td>" . $row["companyname"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Admin Level</td><td>" . $row["adminlevel"]. "</p></td>";
				$a_profiledetails .= "</tr>";
			}

			$data['id'] = $this->lib_utils->_encryptStr(array('str'=>$id,'hash'=>$this->eKey));
			$data['a_profiledetails'] = $a_profiledetails;
			$this->load->view('pages/users/_edit',$data);
		}
	}

	/**
	 * EditProfile function to modify record on the database.
	 *
	 * Option: String
	 * -----------------
	 * id - String
	 *
	 * return String
	 *
	 */
	public function EditProfile() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {


			// If post parameters are sent
			if (isset($_POST['submit'])) {
				// Add primary key id
				$_POST = array_merge($_POST,array('id'=>$this->session->userdata('userSession')));
				// Clean user input
				$clean_post = $this->lib_utils->_cleanRecord($_POST);
				// Update records
				$t_id = $this->Mdl_users->UpdateRecord($clean_post);
				// Send successful message
				$data['msg'] = is_numeric($t_id) ? "<p>Changes Successfully Saved.</p>" : "<p>Error Updating Record. Please contact site Administrator</p>";
			}

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For users table
			$data['a_users'] = $this->Mdl_users->GetAllRecords(array('search_by'=>' WHERE id = "'.$this->session->userdata('userSession').'" '));

			foreach ($data['a_users'] as $row) {
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Name</td><td>" . $row["firstname"]. " " . $row["lastname"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Position</td><td>" . $row["position"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Username</td><td>" . $row["username"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Phone</td><td>" . $row["phone"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Email</td><td>" . $row["email"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company Name</td><td>" . $row["companyname"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Admin Level</td><td>" . $row["adminlevel"]. "</p></td>";
				$a_profiledetails .= "</tr>";
			}

			$data['id'] = $this->lib_utils->_encryptStr(array('str'=>$id,'hash'=>$this->eKey));
			$data['a_profiledetails'] = $a_profiledetails;
			$this->load->view('pages/users/_edit_profile',$data);
		}
	}

	/**
	 * ViewProfile function to display user profile.
	 *
	 * Option: String
	 * -----------------
	 * id - String
	 *
	 * return String
	 *
	 */
	public function ViewProfile() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For users table
			$data['a_users'] = $this->Mdl_users->GetAllRecords(array('search_by'=>' WHERE id = "'.$this->session->userdata('userSession').'" '));

			foreach ($data['a_users'] as $row) {
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Name</td><td>" . $row["firstname"]. " " . $row["lastname"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Position</td><td>" . $row["position"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Username</td><td>" . $row["username"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Phone</td><td>" . $row["phone"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Email</td><td>" . $row["email"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company Name</td><td>" . $row["companyname"]. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Admin Level</td><td>" . $row["adminlevel"]. "</p></td>";
				$a_profiledetails .= "</tr>";
			}

			$data['a_profiledetails'] = $a_profiledetails;
			$this->load->view('pages/users/_view_profile',$data);
		}
	}
}
