<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cntrl_data extends CI_Controller {

	private $controller_main = 'Cntrl_data/';

	public function __construct() {
        parent::__construct();

		// Load all libraries and models on page load
		$this->load->library('Lib_utils');
		$this->load->model('Mdl_contactform');
		$this->load->model('Mdl_updaterequests');
		$this->load->model('Mdl_users');

		$this->eKey = $this->session->userdata('encryption_key');
    }

	/*
	 *  CheckSession method checks if the user is currently logged in.
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return bool
	 *
	 */
	private function _CheckSession() {
	   $is_logged = $this->session->userdata('userSession');
	   return ($is_logged) ? TRUE : FALSE;
	}

	/**
	 * View records master table
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return String
	 */
	public function ViewRecords() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For Users Data
			
			$a_data = $this->Mdl_users->GetAllRecords(array('search_by'=>' WHERE id > 1 AND active="Yes" '));
			if ($a_data) {
				$users_data = "<h1 style='color:black;text-align:center;'>" .count($a_data) . "</h1>";
			} else {
				$users_data = "<h1>0</h1>";
			} unset($a_data);
			$data['users_data'] = $users_data;
			unset($users_data);			
				
			// For Total Change Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE companyname = "'.$this->session->userdata('companyname').'" '));
			if ($a_data) {
				$cr_data = "<h1 style='color:black;text-align:center;'>" .count($a_data) . "</h1>";
			} else {
				$cr_data = "<h1>0</h1>";
			} unset($a_data);
			$data['cr_data'] = $cr_data;
			unset($cr_data);

			// For Total Change Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE companyname = "'.$this->session->userdata('companyname').'" AND completed != "Yes" '));
			if ($a_data) {
				$outstanding_cr_data = "<h1 style='color:black;text-align:center;'>" .count($a_data) . "</h1>";
			} else {
				$outstanding_cr_data = "<h1>0</h1>";
			} unset($a_data);
			$data['outstanding_cr_data'] = $outstanding_cr_data;
			unset($outstanding_cr_data);			
			
			// For Total Change Requests
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>NULL));
			if ($a_data) {
				$all_contact_submissions = "<h1 style='color:black;text-align:center;'>" .count($a_data) . "</h1>";
			} else {
				$all_contact_submissions = "<h1>0</h1>";
			} unset($a_data);
			$data['all_contact_submissions'] = $all_contact_submissions;
			unset($all_contact_submissions);
			
			// For Not Yet Contacted
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE contacted != "Yes" '));
			if ($a_data) {
				$not_yet_contacted = "<h1 style='color:black;text-align:center;'>" .count($a_data) . "</h1>";
			} else {
				$not_yet_contacted = "<h1>0</h1>";
			} unset($a_data);
			$data['not_yet_contacted'] = $not_yet_contacted;
			unset($not_yet_contacted);
			
			// For Not Yet Contacted
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE contacted = "Yes" '));
			if ($a_data) {
				$contacted = "<h1 style='color:black;text-align:center;'>" .count($a_data) . "</h1>";
			} else {
				$contacted = "<h1>0</h1>";
			} unset($a_data);
			$data['contacted'] = $contacted;
			unset($contacted);			

			$data['show_not_contacted'] = TRUE;
			$this->load->view('pages/data/_list',$data);
		}

	}

	/**
	 * UpdateRecord - Updates the record in the database.
	 *
	 * Option: String
	 * -----------------
	 * id = String
	 *
	 * return None
	 */
	public function UpdateRecord($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url().$this->controller_parent);
		} else {

			// Decrypt ID, if issues encountered, destroy session.
			$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
			($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;

			// Update records
			$_POST = array_merge($_POST,array('id'=>$id,'contacted'=>'Yes'));
			$this->Mdl_contactform->UpdateRecord($_POST);
			redirect(base_url().$this->controller_main.'ViewRecords');
		}
	}

	/**
	 * MarkRead - Updates the record in the database.
	 *
	 * Option: String
	 * -----------------
	 * id = String
	 *
	 * return None
	 */
	public function MarkRead($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url().$this->controller_parent);
		} else {
			$r_id = $id;
			// Decrypt ID, if issues encountered, destroy session.
			$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
			($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;

			// Update records
			$_POST = array_merge($_POST,array('id'=>$id,'notified'=>'Yes'));
			$this->Mdl_contactform->UpdateRecord($_POST);
			redirect(base_url().'Cntrl_contact/index/'.$r_id);
		}
	}


}
