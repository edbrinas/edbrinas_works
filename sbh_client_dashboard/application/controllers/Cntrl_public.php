<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cntrl_public extends CI_Controller {

	public function __construct() {
    parent::__construct();

    // Load all libraries and models on page load
    $this->load->library('Lib_utils');
		$this->load->model('Mdl_affiliateprogram');
		$this->load->model('Mdl_users');
  }

	/**
	 * Index Page for this controller.
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return Page
	 *
	 */
	public function index() {

		if (isset($_POST['btn-register'])) {
			if ($_POST['captcha']==$this->session->userdata('sys_captcha')) {

				if ($_POST['password']==$_POST['confirm_password']) {

					// Clean user input
					$clean_post = $this->lib_utils->_cleanRecord($_POST);
					// Retrieve user record
					$a_user_info = $this->Mdl_users->GetSingleRecord(array('search_by'=>' WHERE email = '.$this->db->escape($clean_post['email']).' '));

					if (count($a_user_info)==0) {

						// Insert first to Users Table POrtion

						#$ref_id = $this->lib_utils->_encryptStr(array('str'=>$a_user_info->id,'hash'=>$this->session->userdata('encryption_key'))); // Encrypt email to be used for referral id
						$ref_id = $this->lib_utils->_genRandStr(); // Generate random reference id
						$hashed_password = password_hash($clean_post['password'], PASSWORD_DEFAULT); // Hashed the Password
						unset($clean_post['password']); // Remove existing password in the array
						unset($clean_post['confirmpassword']); // Remove confirm password in the array
						$clean_post = array_merge($clean_post,array('username'=>$clean_post['email'],'password'=>$hashed_password,'ref_id'=>$ref_id,'adminlevel'=>'Full','active'=>'Yes')); // Add the hashed password & unique referral id to the array
						$t_id = $this->Mdl_users->AddRecord($clean_post); // Add records to users table
						unset($a_user_info);

						// Insert to Affiliate Table Portion

						// Search if referral id is present
						if ($_POST['ref_by']!=0) {
								$a_user_info = $this->Mdl_users->GetSingleRecord(array('search_by'=>' WHERE ref_id = '.$this->db->escape($clean_post['ref_by']).' '));
								if (count($a_user_info)>0) {
									$clean_post = array_merge($clean_post,array('ref_by'=>$a_user_info->id)); // Add the user id to the array
								}
						}

						$clean_post = array_merge($clean_post,array('usr_id'=>$t_id)); // Add the user id to the array
						unset($t_id); // Remove old t_id
						$t_id = $this->Mdl_affiliateprogram->AddRecord($clean_post); // Add records to affliate table
						// Send successful message
						$data['msg'] = is_numeric($t_id) ? "<div class='alert alert-success'><span class='glyphicon glyphicon-info-sign'></span> &nbsp; Successfully registered !</div>" : "<div class='alert alert-danger'><span class='glyphicon glyphicon-info-sign'></span> &nbsp; Error while registering !</div>";

					} else {
						$data['msg'] = "<div class='alert alert-danger'><span class='glyphicon glyphicon-info-sign'></span> &nbsp; Sorry email already taken !</div>";
					}
				} else {
					$data['msg'] = "<div class='alert alert-danger'><span class='glyphicon glyphicon-info-sign'></span> &nbsp; Password does not match the confirm password !</div>";
				}
			} else {
				$data['msg'] = "<div class='alert alert-danger'><span class='glyphicon glyphicon-info-sign'></span> &nbsp; Invalid Human Verification Code !</div>";
			}
		}

		// Generate captcha verification
		$this->session->unset_userdata('sys_captcha');
		$c_word['captcha'] = create_captcha($this->lib_utils->_captchaSettings());
		$this->session->set_userdata('sys_captcha',$c_word['captcha']['word']);
		$data['img_captcha'] = $c_word['captcha']['image'];

		$data['a_states'] = $this->lib_utils->_genUsStates();

    $this->load->view('affiliate_registration',$data);
	}

}
