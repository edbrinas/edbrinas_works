<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cntrl_main extends CI_Controller {

	private $controller_main = 'cntrl_main/';

	public function __construct() {
        parent::__construct();

		// Load all libraries and models on page load
		$this->load->library('Lib_utils');
		$this->load->model('Mdl_users');
		$this->load->model('Mdl_contactform');
		$this->load->model('Mdl_updaterequests');
		$this->load->model('Mdl_signups');
		$this->load->model('Mdl_affiliateprogram');
		$this->load->model('Mdl_payments');

		$this->eKey = $this->session->userdata('encryption_key');
    }

	/*
	 * CheckSession method checks if the user is currently logged in.
	 *
	 * Option: Value
	 * -----------------
	 * None
	 *
	 * return bool
	 *
	 */
	private function _CheckSession() {
	   $is_logged = $this->session->userdata('userSession');
	   return ($is_logged) ? TRUE : FALSE;
	}

	/**
	 * RefreshCaptcha refreshes captcha code
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return String
	 *
	 */
	public function RefreshCaptcha() {

		$this->session->unset_userdata('sys_captcha');
		$c_word['captcha'] = create_captcha($this->lib_utils->_captchaSettings());
		$this->session->set_userdata('sys_captcha',$c_word['captcha']['word']);
		echo $c_word['captcha']['image'];

	}

	/**
	 * Index Page for this controller.
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return Page
	 *
	 */
	public function index() {

		// Check user session
		if (!$this->_CheckSession()) {

			// If post parameters are sent
			if (isset($_POST['btn-login'])) {

				// Verify if Captcha is Correct
				if ($_POST['captcha']==$this->session->userdata('sys_captcha')) {
					// Clean user input
					$clean_post = $this->lib_utils->_cleanRecord($_POST);
					// Retrieve user record
					$a_user_info = $this->Mdl_users->GetSingleRecord(array('search_by'=>" WHERE email = ".$this->db->escape($clean_post['email'])." "));
					// Verify if password entered is correct
					if (password_verify($clean_post['password'], $a_user_info->password) && count($a_user_info)==1 && $a_user_info->active='Yes') {

						// Set all session variables
						$this->session->set_userdata('userSession',$a_user_info->id);
						$this->session->set_userdata('firstname',$a_user_info->firstname);
						$this->session->set_userdata('lastname',$a_user_info->lastname);
						$this->session->set_userdata('companyname',$a_user_info->companyname);
						$this->session->set_userdata('adminlevel',$a_user_info->adminlevel);
						$this->session->set_userdata('encryption_key',$this->lib_utils->_genRandStr());

						$this->session->unset_userdata('sys_captcha');
						redirect(base_url());

					} elseif (count($a_user_info)==0) {
						$data['msg'] = "<div class='alert alert-danger'><span><i class='fa fa-ban'></i></span> &nbsp; No User Exists !<br> Contact Your Administrator to Create An Account.</div>";
					} else {
						$data['msg'] = "<div class='alert alert-danger'><span class='glyphicon glyphicon-info-sign'></span> &nbsp; Invalid Username or Password !</div>";
					}

					// Generate captcha verification
					$this->session->unset_userdata('sys_captcha');
					$c_word['captcha'] = create_captcha($this->lib_utils->_captchaSettings());
					$this->session->set_userdata('sys_captcha',$c_word['captcha']['word']);
					$data['img_captcha'] = $c_word['captcha']['image'];

					$this->load->view('login',$data);

				} else {
					// Generate captcha verification
					$this->session->unset_userdata('sys_captcha');
					$c_word['captcha'] = create_captcha($this->lib_utils->_captchaSettings());
					$this->session->set_userdata('sys_captcha',$c_word['captcha']['word']);
					$data['img_captcha'] = $c_word['captcha']['image'];

					$data['msg'] = "<div class='alert alert-danger'><span class='glyphicon glyphicon-info-sign'></span> &nbsp; Invalid Security Code !</div>";

					$data['cntrl_main'] = $this->controller_main;
					$this->load->view('login',$data);
				}
			} else {

				// Generate captcha verification
				$this->session->unset_userdata('sys_captcha');
				$c_word['captcha'] = create_captcha($this->lib_utils->_captchaSettings());
				$this->session->set_userdata('sys_captcha',$c_word['captcha']['word']);
				$data['img_captcha'] = $c_word['captcha']['image'];

				$data['cntrl_main'] = $this->controller_main;
				$this->load->view('login',$data);
			}


		} else {

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For Recent Contact Submissions Table
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE contacted != "Yes" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));

					$td_data .= "<tr>";
					$td_data .= "<td>" . $row["firstname"]. " " . $row["lastname"]. "</td>";
					$td_data .= "<td><a href='tel:{$row["phone"]}'>" . $row["phone"]. "</a></td>";
					$td_data .= "<td><a href='mailto:{$row["email"]}'>" . $row["email"] . "</a></td>";
					$td_data .= "<td align='center'>";
					$td_data .= "<a href='".base_url()."Cntrl_contact/EditRecord/{$r_id}' rel='tooltip' title='View Details' class='btn btn-info btn-simple btn-xs'><i class='fa fa-user'></i></a>";
					$td_data .= "<a href='".base_url()."Cntrl_contact/EditRecord/{$r_id}' rel='tooltip' title='Edit Submission' class='btn btn-success btn-simple btn-xs'><i class='fa fa-edit'></i></a>";
					$td_data .= "<a href='".base_url()."Cntrl_contact/UpdateRecord/{$r_id}' rel='tooltip' title='Mark As Contacted' class='btn btn-danger btn-simple btn-xs'><i class='fa fa-times'></i></a></td>";
					$td_data .= "</tr>";

				} unset($row); unset($a_data);
			} unset($a_data);
			$data['a_tblcontactsubmissions'] = $td_data;
			unset($td_data);

			// For Recent Update Requests Table
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE companyname="'.$this->session->userdata('companyname').'" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					$td_data .=  "<tr>";
					$td_data .=  "<td>" . $row["updatetype"]. "</td>";
					$td_data .=  "<td>" . $row["pagechange"]. "</td>";
					$td_data .=  "<td>" . $row["changedetails"] . "</td>";
					$td_data .=  "<td>" . $row["completed"] . "</td>";
					$td_data .=  "</tr>";
				} unset($row); unset($a_data);
			} unset($a_data);
			$data['a_recentupdaterequests'] = $td_data;
			unset($td_data);

			// For Affiliates Table
			$a_data = $this->Mdl_affiliateprogram->GetAllRecords(array('search_by'=>' AND b.ref_by ="'.$this->session->userdata('userSession').'" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					$td_data .=  "<tr>";
					$td_data .=  "<td>" . $row["firstname"].' '.$row["lastname"]. "</td>";
					$td_data .=  "<td>" . $row["companyname"]. "</td>";
					$td_data .=  "<td>" . $row["position"] . "</td>";
					$td_data .=  "<td>" . $row["address"] . "</td>";
					$td_data .=  "<td>" . $row["city"] . "</td>";
					$td_data .=  "<td>" . $row["state"] . "</td>";
					$td_data .=  "<td>" . $row["postalcode"] . "</td>";
					$td_data .=  "</tr>";
				} unset($row); unset($a_data);
			} unset($a_data);
			$data['a_tblaffiliates'] = $td_data;
			unset($td_data);

			// For Payments Table
			$a_data = $this->Mdl_payments->GetAllRecords(array('search_by'=>NULL,'order_by'=>' ORDER BY inv_num, t_date ASC '));
			if ($a_data) {
				foreach ($a_data as $row) {

					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['registrantid'],'hash'=>$this->eKey));
					$td_data .=  "<tr>";
						$td_data .=  "<td>" . "INV-".date("Y-m")."-".sprintf('%04d', $row["inv_num"])  . "</td>";
						$td_data .=  "<td>" . $row["firstname"].' '.$row["lastname"]. "</td>";
						$td_data .=  "<td>" . $row["address1"].' '.$row["address2"].' '.$row["city"] .' '.$row["state"].' '.$row["zip"]. "</td>";
						$td_data .=  "<td>" . $row["t_amt"] . "</td>";
						$td_data .= "<td align='center'>";
						$td_data .= "<a href='".base_url()."Cntrl_payments/ViewRecord/{$r_id}' rel='tooltip' title='View Details' class='btn btn-info btn-simple btn-xs'><i class='fa fa-user'></i></a>";
						$td_data .= "</tr>";
					$td_data .=  "</tr>";
				} unset($row); unset($a_data);
			} unset($a_data);
			$data['a_tblpayments'] = $td_data;
			unset($td_data);


			$this->load->view('portal',$data);
		}
	}

	/**
	 * ViewCompanyProfile function to display company profile.
	 *
	 * Option: String
	 * -----------------
	 * id - String
	 *
	 * return String
	 *
	 */
	public function ViewCompanyProfile() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For users table
			$data['a_users'] = $this->Mdl_signups->GetAllRecords(array('search_by'=>' WHERE companyname = "'.$this->session->userdata('companyname').'" '));

			foreach ($data['a_users'] as $row) {
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company Name:</td><td>" . $row['companyname']. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company URL:</td><td><a href='{$row['url']}' target='_blank'>" . $row['url']. "</a></p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company Phone:</td><td><a href='tel:{$row['phone']}'>" . $row['phone']. "</a></p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company Address:</td><td>" . $row['address1']. "<br>" . $row['address2']. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company City:</td><td>" . $row['city']. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company State:</td><td>" . $row['state']. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Company Zip Code:</td><td>" . $row['zip']. "</p></td>";
				$a_profiledetails .= "</tr>";
				$a_profiledetails .= "<tr>";
				$a_profiledetails .= "<td><p>Current Plan:</td><td>" . $row['plan']. "</p></td>";
				$a_profiledetails .= "</tr>";
			}

			$data['a_profiledetails'] = $a_profiledetails;
			$data['c_name'] = $this->session->userdata('companyname');
			$this->load->view('pages/users/_view_company_profile',$data);
		}
	}


	/**
	 * LogOut - destroys current session.
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return None
	 *
	 */
	public function LogOut() {
		session_destroy();
		redirect(base_url());
	}

	/**
	 * PageNotFound - 404 Page.
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return None
	 *
	 */
	public function PageNotFound() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========


			$this->load->view('page_not_found',$data);
		}
	}

}
