<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cntrl_payments extends CI_Controller {

	private $controller_main = 'Cntrl_payments/';

	public function __construct() {
      parent::__construct();

      // Load all libraries and models on page load
      $this->load->library('Lib_utils');
      $this->load->model('Mdl_contactform');
      $this->load->model('Mdl_updaterequests');
      $this->load->model('Mdl_payments');

      $this->eKey = $this->session->userdata('encryption_key');
    }

	/*
	 *  CheckSession method checks if the user is currently logged in.
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return bool
	 *
	 */
	private function _CheckSession() {
	   $is_logged = $this->session->userdata('userSession');
	   return ($is_logged) ? TRUE : FALSE;
	}

	/**
	 * View records master table
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return String
	 */
	public function ViewRecord($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

      // Checks for $id encryption
			if (!empty($id)) {
				$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
				($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;
			}

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For Contact Details Table
			$a_data = $this->Mdl_payments->GetAllRecords(array('search_by'=>' AND a.registrantid = '.$id));
			if ($a_data) {
				foreach ($a_data as $row) {

					$td_data .= "<tr>";
					     $td_data .= "<td>Name</td>";
               $td_data .= "<td>" . $row["firstname"]. " " . $row["lastname"]. "</td>";
          $td_data .= "</tr>";
          $td_data .= "<tr>";
              $td_data .= "<td>Address</td>";
              $td_data .= "<td>" . $row["address"]. " " . $row["address1"]. "</td>";
          $td_data .= "</tr>";
          $td_data .= "<tr>";
              $td_data .= "<td>City</td>";
              $td_data .= "<td>" . $row["city"]. "</td>";
          $td_data .= "</tr>";
          $td_data .= "<tr>";
              $td_data .= "<td>State</td>";
              $td_data .= "<td>" . $row["state"]. "</td>";
          $td_data .= "</tr>";
          $td_data .= "<tr>";
              $td_data .= "<td>Zip</td>";
              $td_data .= "<td>" . $row["zip"]. "</td>";
          $td_data .= "</tr>";
          $td_data .= "<tr>";
              $td_data .= "<td>Email</td>";
              $td_data .= "<td><a href='mailto:{$row["email"]}'>" . $row["email"] . "</a></td>";
          $td_data .= "</tr>";
          $td_data .= "<tr>";
              $td_data .= "<td>Phone</td>";
              $td_data .= "<td><a href='tel:{$row["phone"]}'>" . $row["phone"]. "</a></td>";
          $td_data .= "</tr>";

          $td_data1 .= "<tr>";
              $td_data1 .= "<td>Invoice Number</td>";
              $td_data1 .= "<td>" . "INV-".date("Y-m")."-".sprintf('%04d', $row["inv_num"])  . "</td>";
          $td_data1 .= "</tr>";

          $td_data1 .= "<tr>";
              $td_data1 .= "<td>Invoice Date</td>";
              $td_data1 .= "<td>" . $row["t_date"]. "</td>";
          $td_data1 .= "</tr>";

          $td_data1 .= "<tr>";
              $td_data1 .= "<td>Transaction Amount</td>";
              $td_data1 .= "<td>" . number_format($row["t_amt"],2). "</td>";
          $td_data1 .= "</tr>";

          $td_data1 .= "<tr>";
              $td_data1 .= "<td>Class</td>";
              $td_data1 .= "<td>" . $row["class"]. "</td>";
          $td_data1 .= "</tr>";

          $td_data1 .= "<tr>";
              $td_data1 .= "<td>Number of Tickets</td>";
              $td_data1 .= "<td>" . $row["numtickets"]. "</td>";
          $td_data1 .= "</tr>";

          $td_data2 .= "<tr>";
              $td_data2 .= "<td>Gateway Response</td>";
              $td_data2 .= "<td>" . $row["gw_response"]. "</td>";
          $td_data2 .= "</tr>";
          $td_data2 .= "<tr>";
              $td_data2 .= "<td>Authentication Code</td>";
              $td_data2 .= "<td>" . $row["auth_code"]. "</td>";
          $td_data2 .= "</tr>";
          $td_data2 .= "<tr>";
              $td_data2 .= "<td>Transaction ID</td>";
              $td_data2 .= "<td>" . $row["t_id"]. "</td>";
          $td_data2 .= "</tr>";

				} unset($row); unset($a_data);
			} unset($a_data);
			$data['a_tblpayments'] = $td_data;
      $data['a_tblpaymentdetails'] = $td_data1;
      $data['a_tblcarddetails'] = $td_data2;
			unset($td_data);

			$this->load->view('pages/payments/_list',$data);
		}

	}

}
