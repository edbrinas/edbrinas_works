<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cntrl_requests extends CI_Controller {

	private $controller_main = 'Cntrl_requests/';

	public function __construct() {
        parent::__construct();

		// Load all libraries and models on page load
		$this->load->library('Lib_utils');
		$this->load->model('Mdl_contactform');
		$this->load->model('Mdl_updaterequests');
		$this->load->model('Mdl_signups');

		$this->eKey = $this->session->userdata('encryption_key');
    }

	/*
	 *  CheckSession method checks if the user is currently logged in.
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return bool
	 *
	 */
	private function _CheckSession() {
	   $is_logged = $this->session->userdata('userSession');
	   return ($is_logged) ? TRUE : FALSE;
	}

	/**
	 * Index of this controller
	 *
	 * Option: String
	 * -----------------
	 * ID = String @ Record ID
	 *
	 * return String
	 */
	public function index($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// Checks for $id encryption
			if (!empty($id)) {
				$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
				($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;
			}

			// If post parameters are sent
			if (isset($_POST['updatedetails'])) {
				// Remove encrypted ID and replace it with valid ID.
				unset($_POST['id']);
				$_POST = array_merge($_POST,array('id'=>$id));
				// Update records
				$t_id = $this->Mdl_contactform->UpdateRecord($_POST);
				// Send successful message
				$data['msg'] = is_numeric($t_id) ? "<p>Contact Updated Successfully</p>" : "<p>Error Updating Record. Please contact site Administrator</p>";
			}

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For Contact Details Table
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE id = '.$id));
			if ($a_data) {
				foreach ($a_data as $row) {

					$td_data .= "<tr>";
					$td_data .= "<td><p>Name</td><td>" . $row["firstname"]. " " . $row["lastname"]. "</p></td>";
					$td_data .= "</tr>";
					$td_data .= "<tr>";
					$td_data .= "<td><p>Phone</td><td><a href='mailto:{$row['phone']}'>" . $row["phone"]. "</a></p></td>";
					$td_data .= "</tr>";
					$td_data .= "<tr>";
					$td_data .= "<td><p>Email</td><td><a href='mailto:{$row['email']}'>" . $row["email"]. "</a></p></td>";
					$td_data .= "</tr>";
					$td_data .= "<tr>";
					$td_data .= "<td><p>Message</td><td>" . $row["message"]. "</p></td>";
					$td_data .= "</tr>";
					$td_data .= "<tr>";
					$td_data .= "<td><p>Contacted</td><td>" . $row["contacted"]. "</p></td>";
					$td_data .= "</tr>";
					$td_data .= "<tr>";
					$td_data .= "<td><p>Notes</td><td>" . $row["notes"]. "</p></td>";
					$td_data .= "</tr>";

				} unset($row); unset($a_data);
			} unset($a_data);
			$data['a_tblcontactdetails'] = $td_data;
			unset($td_data);

			$data['id'] = $this->lib_utils->_encryptStr(array('str'=>$id,'hash'=>$this->eKey));
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/contact/_edit',$data);
		}

	}

	/**
	 * AddRecord - Inserts record to database
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return String
	 */
	public function AddRecord() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// If post parameters are sent
			if (isset($_POST['submit'])) {
				// Merge all data
				$_POST = array_merge($_POST,array('companyname'=>$this->session->userdata('companyname')));
				// Clean user input
				$clean_post = $this->lib_utils->_cleanRecord($_POST);
				// Update records
				$t_id = $this->Mdl_updaterequests->AddRecord($clean_post);
				// Send successful message
				$data['msg'] = is_numeric($t_id) ? "<p>Record Inserted Successfully</p>" : "<p>Error Inserting Record. Please contact site Administrator</p>";
			}
			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For PLANS display
			$a_data = $this->Mdl_signups->GetAllRecords();
			foreach ($a_data as $row) {
				if ($row['plan'] == 'Basic') {
					$subscription_plan = "<p>You have 30 minutes of Development Time Per Month</p>";
				}
				elseif ($row['plan']  == 'Advanced') {
					$subscription_plan = "<p>You have 1 hour of Development Time Per Month</p>";
				}
				else {
					$subscription_plan = "<p>You currently do not have any development time. Add some <a href='".base_url()."Cntrl_requests/AddDevTime/'>Here</a>.";
				}
			}
			$data['subscription_plan'] = $subscription_plan;
			$data['c_name'] = $this->session->userdata('companyname');
			$this->load->view('pages/requests/_add',$data);
		}

	}

	/**
	 * AddDevTime - Inserts dev time record to database
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return String
	 */
	public function AddDevTime() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// If post parameters are sent
			if (isset($_POST['submit'])) {
				// Merge all data
				$_POST = array_merge($_POST,array('companyname'=>$this->session->userdata('companyname')));
				// Clean user input
				$clean_post = $this->lib_utils->_cleanRecord($_POST);
				// Update records
				$t_id = $this->Mdl_updaterequests->AddRecord($clean_post);
				// Send successful message
				$data['msg'] = is_numeric($t_id) ? "<p>Record Inserted Successfully</p>" : "<p>Error Inserting Record. Please contact site Administrator</p>";
			}
			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			$data['c_name'] = $this->session->userdata('companyname');
			$this->load->view('pages/requests/_add_devtime',$data);
		}
	}

	/**
	 * AddPage - Inserts Pages record to database
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return String
	 */
	public function AddPage() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// If post parameters are sent
			if (isset($_POST['submit'])) {
				// Merge all data
				$_POST = array_merge($_POST,array('companyname'=>$this->session->userdata('companyname')));
				// Clean user input
				$clean_post = $this->lib_utils->_cleanRecord($_POST);
				// Update records
				$t_id = $this->Mdl_updaterequests->AddRecord($clean_post);
				// Send successful message
				$data['msg'] = is_numeric($t_id) ? "<p>Record Inserted Successfully</p>" : "<p>Error Inserting Record. Please contact site Administrator</p>";
			}
			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			$data['c_name'] = $this->session->userdata('companyname');
			$this->load->view('pages/requests/_add_devtime',$data);
		}
	}

	/**
	 * View records master table
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return String
	 */
	public function ViewRecords() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For All Contact From Submissions Table
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE companyname = "'.$this->session->userdata('companyname').'" '));
			$th_data .= "<tr>";
			$th_data .= "<th>Update Type</th>";
			$th_data .= "<th>Page Change is On</th>";
			$th_data .= "<th>Update Details</th>";
			$th_data .= "<th>Completed</th>";
			$th_data .= "</tr>";
			if ($a_data) {
				foreach ($a_data as $row) {

					$td_data .= "<tr>";
					$td_data .= "<td>" . $row["updatetype"]. "</td>";
					$td_data .= "<td>" . $row["pagechange"]. "</td>";
					$td_data .= "<td>" . $row["changedetails"]. "</td>";
					$td_data .= "<td>" . $row['completed']. "</td>";
					$td_data .= "</tr>";

				} unset($row); unset($a_data);
			} unset($a_data);
			$data['tbl_header'] = $th_data;
			$data['tbl_details'] = $td_data;
			$data['tbl_title'] = "All Contact Form Submissions";
			unset($td_data);

			$data['tbl_button'] = '<a href="'.base_url().'Cntrl_requests/ViewNotContacted/" class="btn btn-danger btn-fill">View Active Requests</a>';
			$data['table_title'] = "All Update Requests";
			$this->load->view('pages/requests/_list',$data);
		}

	}

	/**
	 * ViewNotContacted - master table for not contacted users
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return String
	 */
	public function ViewNotContacted() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For Outstanding Tickets Table
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE companyname = "'.$this->session->userdata('companyname').'" AND completed = "No" '));
			$th_data .= "<tr>";
			$th_data .= "<th>Update Type</th>";
			$th_data .= "<th>Page Change is On</th>";
			$th_data .= "<th>Update Details</th>";
			$th_data .= "</tr>";
			if ($a_data) {
				foreach ($a_data as $row) {
					$td_data .= "<tr>";
					$td_data .=  "<td>" . $row["updatetype"]. "</td>";
					$td_data .=  "<td>" . $row["pagechange"]. "</td>";
					$td_data .=  "<td>" . $row["changedetails"]. "</td>";
					$td_data .=  "</tr>";

				} unset($row); unset($a_data);
			} unset($a_data);
			$data['tbl_header'] = $th_data;
			$data['tbl_details'] = $td_data;
			$data['tbl_title'] = "Outstanding Tickets";
			unset($td_data);

			$data['tbl_button'] = '<a href="'.base_url().'Cntrl_requests/ViewRecords/" class="btn btn-danger btn-fill">View All Requests</a>';
			$data['table_title'] = "Outstanding Tickets";
			$this->load->view('pages/requests/_list',$data);
		}

	}

	/**
	 * UpdateRecord - Updates the record in the database.
	 *
	 * Option: String
	 * -----------------
	 * id = String
	 *
	 * return None
	 */
	public function UpdateRecord($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url().$this->controller_parent);
		} else {

			// Decrypt ID, if issues encountered, destroy session.
			$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
			($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;

			// Update records
			$_POST = array_merge($_POST,array('id'=>$id,'contacted'=>'Yes'));
			$this->Mdl_contactform->UpdateRecord($_POST);
			redirect(base_url().$this->controller_main.'ViewRecords');
		}
	}

	/**
	 * MarkRead - Updates the record in the database.
	 *
	 * Option: String
	 * -----------------
	 * id = String
	 *
	 * return None
	 */
	public function MarkRead($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url().$this->controller_parent);
		} else {

			// Decrypt ID, if issues encountered, destroy session.
			$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
			($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;

			// Update records
			$_POST = array_merge($_POST,array('id'=>$id,'notified'=>'Yes'));
			$this->Mdl_updaterequests->UpdateRecord($_POST);
			redirect(base_url().'/Cntrl_requests/ViewRecords/');
		}
	}
}
