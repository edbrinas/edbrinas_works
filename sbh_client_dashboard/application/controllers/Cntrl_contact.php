<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cntrl_contact extends CI_Controller {

	private $controller_main = 'Cntrl_contact/';

	public function __construct() {
		parent::__construct();

		// Load all libraries and models on page load
		$this->load->library('Lib_utils');
		$this->load->model('Mdl_contactform');
		$this->load->model('Mdl_updaterequests');

		$this->eKey = $this->session->userdata('encryption_key');
	}

	/*
	 *  CheckSession method checks if the user is currently logged in.
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return bool
	 *
	 */
	private function _CheckSession() {
	   $is_logged = $this->session->userdata('userSession');
	   return ($is_logged) ? TRUE : FALSE;
	}

	/**
	 * EditRecord displays the page for the user to modify a record.
	 *
	 * Option: String
	 * -----------------
	 * ID = String @ Record ID
	 *
	 * return String
	 */
	public function EditRecord($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// Checks for $id encryption
			if (!empty($id)) {
				$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
				($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;
			}

			// If post parameters are sent
			if (isset($_POST['updatedetails'])) {
				// Clean user input
				$clean_post = $this->lib_utils->_cleanRecord($_POST);
				// Remove encrypted ID and replace it with valid ID.
				unset($clean_post['id']);
				$clean_post = array_merge($clean_post,array('id'=>$id));
				// Update records
				$t_id = $this->Mdl_contactform->UpdateRecord($clean_post);
				// Send successful message
				$data['msg'] = is_numeric($t_id) ? "<p>Contact Updated Successfully</p>" : "<p>Error Updating Record. Please contact site Administrator</p>";
			}

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For Contact Details Table
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE id = '.$id));
			if ($a_data) {
				foreach ($a_data as $row) {

					$td_data .= "<tr>";
					$td_data .= "<td><p>Name</td><td>" . $row["firstname"]. " " . $row["lastname"]. "</p></td>";
					$td_data .= "</tr>";
					$td_data .= "<tr>";
					$td_data .= "<td><p>Phone</td><td><a href='mailto:{$row['phone']}'>" . $row["phone"]. "</a></p></td>";
					$td_data .= "</tr>";
					$td_data .= "<tr>";
					$td_data .= "<td><p>Email</td><td><a href='mailto:{$row['email']}'>" . $row["email"]. "</a></p></td>";
					$td_data .= "</tr>";
					$td_data .= "<tr>";
					$td_data .= "<td><p>Message</td><td>" . $row["message"]. "</p></td>";
					$td_data .= "</tr>";
					$td_data .= "<tr>";
					$td_data .= "<td><p>Contacted</td><td>" . $row["contacted"]. "</p></td>";
					$td_data .= "</tr>";
					$td_data .= "<tr>";
					$td_data .= "<td><p>Notes</td><td>" . $row["notes"]. "</p></td>";
					$td_data .= "</tr>";

				} unset($row); unset($a_data);
			} unset($a_data);
			$data['a_tblcontactdetails'] = $td_data;
			unset($td_data);

			$data['id'] = $this->lib_utils->_encryptStr(array('str'=>$id,'hash'=>$this->eKey));
			$data['controller_main'] = $this->controller_main;
			$this->load->view('pages/contact/_edit',$data);
		}

	}

	/**
	 * View records master table
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return String
	 */
	public function ViewRecords() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For Contact Details Table
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>NULL));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));

					$td_data .= "<tr>";
					$td_data .= "<td>" . $row["firstname"]. " " . $row["lastname"]. "</td>";
					$td_data .= "<td><a href='tel:{$row["phone"]}'>" . $row["phone"]. "</a></td>";
					$td_data .= "<td><a href='mailto:{$row["email"]}'>" . $row["email"] . "</a></td>";
					$td_data .= "<td>" . $row["contacted"] . "</td>";
					$td_data .= "<td><a href='".base_url()."Cntrl_contact/EditRecord/{$r_id}' rel='tooltip' title='View Details' class='btn btn-info btn-simple btn-xs'><i class='fa fa-user'></i></a>";
					$td_data .= "<a href='".base_url()."Cntrl_contact/EditRecord/{$r_id}' rel='tooltip' title='Edit Submission' class='btn btn-success btn-simple btn-xs'><i class='fa fa-edit'></i></a>";
					$td_data .= "<a href='".base_url()."Cntrl_contact/UpdateRecord/{$r_id}' rel='tooltip' title='Mark As Contacted' class='btn btn-danger btn-simple btn-xs'><i class='fa fa-times'></i></a></td>";
					$td_data .= "</tr>";

				} unset($row); unset($a_data);
			} unset($a_data);
			$data['a_tblallcontactformsubmission'] = $td_data;
			$data['tbl_title'] = "All Contact Form Submissions";
			unset($td_data);

			$data['show_not_contacted'] = TRUE;
			$this->load->view('pages/contact/_list',$data);
		}

	}

	/**
	 * ViewNotContacted - master table for not contacted users
	 *
	 * Option: None
	 * -----------------
	 * None
	 *
	 * return String
	 */
	public function ViewNotContacted() {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url());
		} else {

			// ========== HEADER NOTIFICATIONS STARTS HERE ==========

			// For notification count
			$a_contactform = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified = "No" AND contacted = "No" '));
			$a_updaterequests = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>'WHERE completed = "Yes" AND notified = "No"'));
			$data['notif_count'] = count($a_contactform) + count($a_updaterequests);

			// For New Contact Submissions
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE notified != "Yes" AND contacted = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					$li_data .= "<li><a href='".base_url()."Cntrl_contact/MarkRead/{$r_id}'>" . $row["firstname"]. " " . $row["lastname"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_newcontactsubmission'] = $li_data;
			unset($li_data);

			// For Completed Update Requests
			$a_data = $this->Mdl_updaterequests->GetAllRecords(array('search_by'=>' WHERE completed = "Yes" AND notified = "No" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));
					 $li_data .= "<li><a href='".base_url()."Cntrl_requests/MarkRead/{$r_id}'>" . $row["updatetype"]. "</a></li>";
				} unset($row); unset($a_data);
			} else {
				$li_data = "<li style='margin-bottom:10px;'><p style='text-align:center;font-size:12px;'>0 New Notifications</p></li>";
			} unset($a_data);
			$data['a_completedupdaterequest'] = $li_data;
			unset($li_data);

			// ========== HEADER NOTIFICATIONS ENDS HERE ==========

			// For Contact Details Table
			$a_data = $this->Mdl_contactform->GetAllRecords(array('search_by'=>' WHERE contacted != "Yes" '));
			if ($a_data) {
				foreach ($a_data as $row) {
					// Encrypt record id
					unset($r_id);
					$r_id = $this->lib_utils->_encryptStr(array('str'=>$row['id'],'hash'=>$this->eKey));

					$td_data .= "<tr>";
					$td_data .= "<td>" . $row["firstname"]. " " . $row["lastname"]. "</td>";
					$td_data .= "<td><a href='tel:{$row["phone"]}'>" . $row["phone"]. "</a></td>";
					$td_data .= "<td><a href='mailto:{$row["email"]}'>" . $row["email"] . "</a></td>";
					$td_data .= "<td>" . $row["contacted"] . "</td>";
					$td_data .= "<td><a href='".base_url()."Cntrl_contact/index/{$r_id}' rel='tooltip' title='View Details' class='btn btn-info btn-simple btn-xs'><i class='fa fa-user'></i></a>";
					$td_data .= "<a href='".base_url()."Cntrl_contact/index/{$r_id}' rel='tooltip' title='Edit Submission' class='btn btn-success btn-simple btn-xs'><i class='fa fa-edit'></i></a>";
					$td_data .= "<a href='".base_url()."Cntrl_contact/UpdateRecord/{$r_id}' rel='tooltip' title='Mark As Contacted' class='btn btn-danger btn-simple btn-xs'><i class='fa fa-times'></i></a></td>";
					$td_data .= "</tr>";

				} unset($row); unset($a_data);
			} unset($a_data);
			$data['a_tblallcontactformsubmission'] = $td_data;
			$data['tbl_title'] = "Contact Form Submissions";
			unset($td_data);

			$this->load->view('pages/contact/_list',$data);
		}

	}

	/**
	 * UpdateRecord - Updates a record in the database.
	 *
	 * Option: String
	 * -----------------
	 * id = String
	 *
	 * return None
	 */
	public function UpdateRecord($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url().$this->controller_parent);
		} else {

			// Decrypt ID, if issues encountered, destroy session.
			$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
			($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;

			// Update records
			$_POST = array_merge($_POST,array('id'=>$id,'contacted'=>'Yes'));
			$this->Mdl_contactform->UpdateRecord($_POST);
			redirect(base_url().$this->controller_main.'ViewRecords');
		}
	}

	/**
	 * MarkRead - Updates a record in the database.
	 *
	 * Option: String
	 * -----------------
	 * id = String
	 *
	 * return None
	 */
	public function MarkRead($id) {

		// Check user session
		if (!$this->_CheckSession()) {
			redirect(base_url().$this->controller_parent);
		} else {
			$r_id = $id;
			// Decrypt ID, if issues encountered, destroy session.
			$id = $this->lib_utils->_decryptStr(array('str'=>$id,'hash'=>$this->eKey));
			($id==0) ? redirect(base_url().'cntrl_main/LogOut','refresh') : NULL;

			// Update records
			$_POST = array_merge($_POST,array('id'=>$id,'notified'=>'Yes'));
			$this->Mdl_contactform->UpdateRecord($_POST);
			redirect(base_url().'Cntrl_contact/index/'.$r_id);
		}
	}

	public function SendSMS() {
			$a = $this->lib_utils->_sendSMS(array('to'=>'+966534412751','body'=>'This is a test broadcast'));
			echo $a['err_msg'];
	}

}
