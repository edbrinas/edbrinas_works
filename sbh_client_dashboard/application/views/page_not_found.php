<?php include('_includes/_header.php'); ?>

<div class="wrapper">

	<!-- Side Bar starts here -->
	<?php include('_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->

    <div class="main-panel">

		<!-- Upper Bar starts here -->
		<?php include('_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->

    <div class="content"> <!-- Content starts here -->

      <div class="container-fluid"> <!-- Containter Fluid Starts Here -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Ooops 404 Error!</h4>
                    </div>
                    <div class="content table-responsive">
                    <p class="category">We can't seem to find the page you're looking for.</p>
                    </div>
                </div>
            </div>
        </div>

      </div> <!-- Containter Fluid Ends Here -->

  		<!-- Footer Bar starts here -->
  		<?php include('_includes/_footerbar.php'); ?>
  		<!-- Footer Bar ends here -->

		</div> <!-- Content ends here -->
	</div>
</div>

<?php include('_includes/_footer.php'); ?>
