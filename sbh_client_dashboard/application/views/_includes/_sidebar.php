<div class="sidebar" data-color="blue" data-image="./assets/img/full-screen-image-3.jpg">
	<div class="logo">
		<a href="<?php echo base_url(); ?>" class="logo-text">
			<?php echo $this->session->userdata('companyname'); ?>
		</a>
	</div>
	<div class="logo logo-mini">
		<a href="<?php echo $this->session->userdata('url'); ?>" class="logo-text">
			SBH
		</a>
	</div>

	<div class="sidebar-wrapper">
		<div class="user">
		   <!-- <div class="photo">
				<img src="<?php echo base_url(); ?>_assets/img/default-avatar.png" />
			</div>-->
			<div class="info">
				<a data-toggle="collapse" href="#collapseExample" class="collapsed">
					Welcome <?php echo $this->session->userdata('firstname') . " " . $this->session->userdata('lastname'); ?>
					<b class="caret"></b>
				</a>
				<div class="collapse" id="collapseExample">
					<ul class="nav">
						<li>
							<a href="<?php echo base_url().'Cntrl_users/EditProfile/'; ?>">Edit Profile</a>
						</li>

						<li>
							<a href="<?php echo base_url().'Cntrl_users/ViewProfile/'; ?>">View Profile</a>
						</li>

					</ul>
				</div>
			</div>
		</div>

		<ul class="nav">
			<li class="active">
				<a href="<?php echo base_url(); ?>">
					<i class="pe-7s-home"></i>
					<p>Home</p>
				</a>
			</li>
			<li>
				<a data-toggle="collapse" href="#customerRelations">
					<i class="pe-7s-phone"></i>
					<p>Site Manager
						<b class="caret"></b>
					</p>
				</a>
				<div class="collapse" id="customerRelations">
					<ul class="nav">
						<li>
							<a href="<?php echo base_url().'Cntrl_requests/AddRecord/'; ?>">Request Website Changes</a>
						</li>
						<li>
							<a href="<?php echo base_url().'Cntrl_requests/ViewNotContacted/'; ?>">Check Change Status</a>
						</li>
						<li>
							<a href="https://sitesbyheroes.freshdesk.com" target="_blank">Technical Support</a>
						</li>
					</ul>
				</div>
			</li>
			<li>
				<a href="<?php echo base_url().'Cntrl_contact/ViewRecords/'; ?>">
					<i class="pe-7s-mail"></i>
					<p>Contact Submissions</p>
				</a>
			</li>
		</ul>
	</div>
</div>