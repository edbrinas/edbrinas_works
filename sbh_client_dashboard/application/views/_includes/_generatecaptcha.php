<script>
     $(function(){
         var base_url = '<?php echo base_url(); ?>';
         $('.reload-captcha').click(function(event){
           $('.captcha-img').empty();
           event.preventDefault();
           $.ajax({
               url:base_url+'Cntrl_main/RefreshCaptcha/',
               success:function(data){
                 $('.captcha-img').append(data);
               }
           });
          });
      });
</script>
