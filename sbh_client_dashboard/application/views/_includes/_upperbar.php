        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-danger btn-fill btn-round btn-icon">
                        <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                        <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                    </button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>"><?php echo $this->session->userdata('companyname'); ?></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
					    <?php if ($this->session->userdata('adminlevel')=='Full') { ?>
                            <li>
                                <a href="<?php echo base_url().'Cntrl_data/ViewRecords/'; ?>">
                                    <i class="fa fa-line-chart"></i>
                                    <p>Data</p>
                                </a>
                            </li>
					    <?php } ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="notification">
								<?php echo number_format($notif_count); ?>
                                    </span>
                                <p class="hidden-md hidden-lg">
                                    Notifications
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <ul class="dropdown-menu">
                                <li><p style="font-size:10px;">New Contact Submissions</p></li>
                                <li class="divider"></li>
							    <?php echo $a_newcontactsubmission; ?>
                                <li class="divider"></li>
                                <li><p style="font-size:10px;">Completed Update Requests</p></li>
                                <li class="divider"></li>
							    <?php echo $a_completedupdaterequest; ?>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-with-icons">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i>
                                <p class="hidden-md hidden-lg">
                                    Actions
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <ul class="dropdown-menu dropdown-with-icons">
                                <li>
                                    <a href="https://sitesbyheroes.freshdesk.com" target="_blank">
                                        <i class="pe-7s-help1"></i> Help Center
                                    </a>
                                </li>
							    <?php if ($this->session->userdata('adminlevel')=='Full'){ ?>
                                    <li>
                                        <a href="<?php echo base_url().'Cntrl_users/'; ?>">
                                            <i class="pe-7s-user"></i> Manage Users
                                        </a>
                                    </li>
							    <?php } ?>

                                <li>
                                    <a href="<?php echo base_url().'Cntrl_main/ViewCompanyProfile/'; ?>">
                                        <i class="pe-7s-info"></i> Company Profile
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?php echo base_url().'Cntrl_main/LogOut'; ?>" class="text-danger">
                                        <i class="pe-7s-close-circle"></i>
                                        Log out
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>