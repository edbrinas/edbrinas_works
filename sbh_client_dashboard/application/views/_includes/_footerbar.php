<footer class="footer">
	<div class="container-fluid">
		<p class="copyright pull-left">
			Version 1.0
		</p>
		<p class="copyright pull-right">
			Copyright&copy; <?php echo date('Y'); ?> <a href="https://sitesbyheroes.com">Sites By Heroes</a> | All Rights Reserved
		</p>
	</div>
</footer>