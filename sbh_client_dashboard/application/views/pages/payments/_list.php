<?php include(APPPATH.'views/_includes/_header.php'); ?>

<div class="wrapper">

	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->

    <div class="main-panel">

		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->

        <div class="content"> <!-- Content starts here -->

            <div class="container-fluid"> <!-- Containter Fluid Starts Here -->

						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="card">
									<div class="header">
										<h4 class="card-title">Transaction Details</h4>
									</div>
									<div class="content">
										<div class="table-responsive">
											<table class="table">
												<thead>
												<tr>
													<th>Title</th>
													<th>Details</th>
												</tr>
												</thead>
												<tbody>
												<?php echo $a_tblpayments; ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="card">
									<div class="header">
										<h4 class="card-title">Order Details</h4>
									</div>
									<div class="content">
										<div class="table-responsive">
											<table class="table">
												<thead>
												<tr>
													<th>Title</th>
													<th>Details</th>
												</tr>
												</thead>
												<tbody>
												<?php echo $a_tblpaymentdetails; ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="header">
										<h4 class="card-title">Card Receipt Details</h4>
									</div>
									<div class="content">
										<div class="table-responsive">
											<table class="table">
												<thead>
												<tr>
													<th>Title</th>
													<th>Details</th>
												</tr>
												</thead>
												<tbody>
												<?php echo $a_tblcarddetails; ?>
												</tbody>
											</table>
											<p><button onclick="window.history.back();" class="btn btn-fill btn-danger">Go Back</button></p>
										</div>
									</div>
								</div>
							</div>

						</div>

            </div> <!-- Containter Fluid Ends Here -->

						<!-- Footer Bar starts here -->
						<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
						<!-- Footer Bar ends here -->

		</div> <!-- Content ends here -->

	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>
