<?php include(APPPATH.'views/_includes/_header.php'); ?>

<div class="wrapper">
	
	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->
	
    <div class="main-panel">
		
		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->
		
		<!-- Content starts here -->

        <div class="content"> <!-- Containter Fluid Starts Here -->
		
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-4 col-md-4">
						<div class="card">
							<div class="header text-center">
								<h4>Users</h4>
							</div>
							<div class="content text-center" style="margin:0;padding:0;">
								<?php echo $users_data; ?>
							</div>
							<div class="footer text-center">
								<a class="btn btn-danger btn-fill" href="<?php echo base_url().'Cntrl_users/'; ?>">Manage Users</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="card">
							<div class="header text-center">
								<h4>Total Change Requests</h4>
							</div>
							<div class="content text-center" style="margin:0;padding:0;">
								<?php echo $cr_data; ?>
							</div>
							<div class="footer text-center">
								<a class="btn btn-danger btn-fill" href="<?php echo base_url().'Cntrl_requests/ViewRecords/'; ?>">View All Requests</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="card">
							<div class="header text-center">
								<h4>Outstanding Change Requests</h4>
							</div>
							<div class="content text-center" style="margin:0 !important;padding:0 !important;">
								<?php echo $outstanding_cr_data; ?>
							</div>
							<div class="footer text-center">
								<a class="btn btn-danger btn-fill" href="<?php echo base_url().'Cntrl_requests/ViewRecords/'; ?>">View Outstanding Requests</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-4">
						<div class="card">
							<div class="header text-center">
								<h4>All Contact Submissions</h4>
							</div>
							<div class="content text-center" style="margin:0 !important;padding:0 !important;">
								<?php echo $all_contact_submissions; ?>
							</div>
							<div class="footer text-center">
								<a class="btn btn-danger btn-fill" href="<?php echo base_url().'Cntrl_contact/ViewRecords/'; ?>">View All Submissions</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="card">
							<div class="header text-center">
								<h4>Contact Submissions (not yet contacted)</h4>
							</div>
							<div class="content text-center" style="margin:0;padding:0;">
								<?php echo $not_yet_contacted; ?>
							</div>
							<div class="footer text-center">
								<a class="btn btn-danger btn-fill" href="<?php echo base_url().'Cntrl_contact/ViewNotContacted/'; ?>">View Not Yet Contacted</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="card">
							<div class="header text-center">
								<h4>Contact Submissions (contacted)</h4>
							</div>
							<div class="content text-center" style="margin:0;padding:0;">
								<?php echo $contacted; ?>
							</div>
							<div class="footer text-center">
								<a class="btn btn-danger btn-fill" href="<?php echo base_url().'Cntrl_contact/ViewRecords/'; ?>">View All Submissions</a>
							</div>
						</div>
					</div>
				</div>
			</div>			
				
        </div> <!-- Containter Fluid Ends Here -->
			

		<!-- Content ends here -->
		
		<!-- Footer Bar starts here -->
		<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
		<!-- Footer Bar ends here -->
		
		</div> <!-- Containter Fluid Ends Here -->
		
	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>