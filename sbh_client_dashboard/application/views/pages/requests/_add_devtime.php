<?php include(APPPATH.'views/_includes/_header.php'); ?>

<div class="wrapper">
	
	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->
	
    <div class="main-panel">
		
		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->
		
		<!-- Content starts here -->

        <div class="content"> <!-- Containter Fluid Starts Here -->
			<div class="container-fluid">
				<div class="row" style="margin-bottom:40px;">
					<div class="col-md-12">
						<h2 class="text-center">Add More Development Time</h2>
					</div>
				</div>
				<div class="row" style="margin-bottom:40px;">
					<div class="col-lg-4 col-md-4">
						<div class="card">
							<div class="header text-center">
								<h5>Add 30 Minutes</h5>
							</div>

							<div class="content text-center" style="margin:0;padding:0;">
								<h1>$50</h1>
							</div>
							<div class="footer text-center">
								<a href="https://sitesbyheroes.chargebee.com/hosted_pages/plans/add-30-minutes" class="btn btn-danger btn-fill">Buy Now</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="card">
							<div class="header text-center">
								<h5>Add 1 Hour</h5>
							</div>

							<div class="content text-center" style="margin:0;padding:0;">
								<h1>$80</h1>
							</div>
							<div class="footer text-center">
								<a href="https://sitesbyheroes.chargebee.com/hosted_pages/plans/add-1-hour" class="btn btn-danger btn-fill">Buy Now</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="card">
							<div class="header text-center">
								<h5>Add 2 Hours</h5>
							</div>

							<div class="content text-center" style="margin:0;padding:0;">
								<h1>$140</h1>
							</div>
							<div class="footer text-center">
								<a href="https://sitesbyheroes.chargebee.com/hosted_pages/plans/add-2-hours" class="btn btn-danger btn-fill">Buy Now</a>
							</div>
						</div>
					</div>
				</div>
			</div>		
				
        </div> <!-- Containter Fluid Ends Here -->
			

		<!-- Content ends here -->
		
		<!-- Footer Bar starts here -->
		<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
		<!-- Footer Bar ends here -->
		
		</div> <!-- Containter Fluid Ends Here -->
		
	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>