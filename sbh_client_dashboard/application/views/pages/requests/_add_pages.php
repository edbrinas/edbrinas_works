<?php include(APPPATH.'views/_includes/_header.php'); ?>

<div class="wrapper">
	
	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->
	
    <div class="main-panel">
		
		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->
		
		<!-- Content starts here -->

        <div class="content"> <!-- Containter Fluid Starts Here -->
		
			<div class="container-fluid">
				<div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="card">
                            <form action="<?php echo base_url().'Cntrl/AddPages/'; ?>" method="post" style="padding-top:20px;padding-bottom:20px;">
                                <div class="header">
                                    <h4 class="title">Add Pages for <?php echo $c_name; ?></h4>
                                </div>
                                <div class="content" style="margin-top:20px;">
                                    <div class="form-group label-floating">
                                        <select class="form-control" name="numberpages" required>
                                            <option selected disabled>Number of Pages To Add</option>
                                            <option value="1">1 Page</option>
                                            <option value="2">2 Pages</option>
                                            <option value="3">3 Pages</option>
                                            <option value="4">4 Pages</option>
                                            <option value="5">5 Pages</option>
                                        </select>
                                    </div>
                                    <input type="hidden" value="<?php echo $c_name; ?>" name="companyname">
                                    <input type="hidden" value="No" name="completed">
                                    <input type="hidden" value="Add Page(s)" name="updatetype">
                                    <p>**IMPORTANT** If your pages require custom images, send the images to <a href="mailto:support@sitesbyheroes.com">Support@SitesByHeroes.com</a></p>
                                    <div class="form-footer" style="margin-top:20px;margin-bottom:20px;">
                                        <button type="submit" name="submit" class="btn btn-primary btn-fill">Add Pages</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>
			</div>			
				
        </div> <!-- Containter Fluid Ends Here -->
			

		<!-- Content ends here -->
		
		<!-- Footer Bar starts here -->
		<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
		<!-- Footer Bar ends here -->
		
		</div> <!-- Containter Fluid Ends Here -->
		
	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>