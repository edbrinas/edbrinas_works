<?php include(APPPATH.'views/_includes/_header.php'); ?>

<div class="wrapper">

	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->

    <div class="main-panel">

		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->

		<!-- Content starts here -->

        <div class="content"> <!-- Containter Fluid Starts Here -->

			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 col-md-6">
						<div class="card">
							<form action="<?php echo base_url().'Cntrl_requests/AddRecord/'; ?>" method="post">
								<div class="header">
                                    <h4 class="title">Website Update Request for <?php echo $c_name; ?></h4>
								</div>
								<div class="content">
									<p>Submit Form Once For Each Update</p>
                                    <p style="margin-top:10px;margin-bottom:40px;">To Add A New Page click <a href="<?php echo base_url().'Cntrl_requests/AddPage/'; ?>">HERE</a>.</p>
									<div class="form-group label-floating">
										<label class="control-label">
											Type of Update
										</label><br>
										<select name="updatetype" class="form-control" required>
											<option selected disabled>Choose One</option>
											<option value="Text Change">Text Change</option>
											<option value="Image Change">Image Change</option>
											<option value="Add Section">Add Section</option>
											<option value="Remove Page">Remove Page</option>
											<option value="Change Colors 1 Section">Change Colors in 1 Section</option>
											<option value="Change Colors 1 Page">Change Colors on 1 page</option>
										</select>
									</div>
									<div class="form-group label-floating">
										<label class="control-label">
											What page is this change on?
										</label>
										<input class="form-control" name="pagechange" type="text" required>
									</div>
									<div class="form-group label-floating">
										<label class="control-label">
											Describe the change in detail
										</label>
										<textarea class="form-control" name="changedetails" maxlength="1000" required></textarea>
									</div
                                    <input type="hidden" value="No" name="completed">
                                    <input type="hidden" value="No" name="notified">
									<p>**IMPORTANT** If your change requires an image change, send the image to <a href="mailto:support@sitesbyheroes.com">Support@SitesByHeroes.com</a></p>
									<div class="form-footer">
                                        <button type="submit" name="submit" class="btn btn-danger btn-fill">Request Update</button>
									</div>
									<br />
									<?php echo $msg; ?>
								</div>
							</form>
						</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="card">
							<div class="header">
                                <h4 class="card-title">Time Allotments for <?php echo $c_name; ?></h4>
							</div>
							<div class="content">
								<?php echo $subscription_plan; ?>
								<p>All time is per instance. (i.e. 1 text change on 1 page would be 10 minutes. 1 text change on 2 pages would be 20 minutes)</p>
								<ul class="timeList">
									<li>Change Text: 10 minutes</li>
									<li>Change Image: 10 minutes/image</li>
									<li>Add Section: 30 minutes/section</li>
                                    <li>Change Section: 15 minutes/section</li>
									<li>Add Page: $100 (Click <a href="<?php echo base_url().'Cntrl_requests/AddPage/'; ?>">HERE</a> to add pages. After you've filled out the form and paid, send your images to <a href="mailto:support@sitesbyheroes.com">Support@SitesByHeroes.com</a>
									before changes will be made)</li>
									<li>Remove Page: 10 mins/page</li>
									<li>Change Colors (in 1 section): 10 minutes</li>
									<li>Change Colors (on 1 entire page): 20 minutes</li>
								</ul>

								<div class="form-footer" style="padding-top:20px;">
									<a type="submit" href="<?php echo base_url().'Cntrl_requests/AddDevTime/'; ?>" class="btn btn-danger btn-fill">Add More Time</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

        </div> <!-- Containter Fluid Ends Here -->


		<!-- Content ends here -->

		<!-- Footer Bar starts here -->
		<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
		<!-- Footer Bar ends here -->

		</div> <!-- Containter Fluid Ends Here -->

	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>
