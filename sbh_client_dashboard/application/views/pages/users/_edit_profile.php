<?php include(APPPATH.'views/_includes/_header.php'); ?>

<div class="wrapper">
	
	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->
	
    <div class="main-panel">
		
		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->
		
		<!-- Content starts here -->

        <div class="content"> <!-- Containter Fluid Starts Here -->
        	<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 col-md-6">
						<div class="card">
							<div class="header">
								<h4 class="card-title">Edit Profile</h4>
							</div>
							<div class="content">
								<form method="post" action="<?php echo base_url().'Cntrl_users/EditProfile/'; ?>">
									<div class="form-group">
										<input type="text" name="firstname" placeholder="First Name" class="form-control">
									</div>
									<div class="form-group">
										<input type="text" name="lastname" placeholder="Last Name" class="form-control">
									</div>
									<div class="form-group">
										<input type="text" name="username" placeholder="Username" class="form-control">
									</div>
									<div class="form-group">
										<input type="tel" name="phone" placeholder="Phone Number" class="form-control">
									</div>
									<div class="form-group">
										<input type="email" name="email" placeholder="Email" class="form-control">
									</div>
									<input type="submit" name="submit" value="Edit Profile" class="btn btn-fill btn-danger">
									<br /><br />
									<?php echo $msg; ?>
								</form>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="card">
							<div class="header">
								<h4 class="card-title">Profile Details</h4>
							</div>
							<div class="content">
								<div class="table-responsive">
									<table class="table">
										<thead>
										<tr>
											<th>Title</th>
											<th>Details</th>
										</tr>
										</thead>
										<tbody><?php echo $a_profiledetails; ?></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
				
        </div> <!-- Containter Fluid Ends Here -->
			

		<!-- Content ends here -->
		
		<!-- Footer Bar starts here -->
		<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
		<!-- Footer Bar ends here -->
		
		</div> <!-- Containter Fluid Ends Here -->
		
	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>