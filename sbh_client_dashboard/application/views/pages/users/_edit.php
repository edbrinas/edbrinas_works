<?php include(APPPATH.'views/_includes/_header.php'); ?>

<div class="wrapper">

	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->

    <div class="main-panel">

		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->

		<!-- Content starts here -->

        <div class="content"> <!-- Containter Fluid Starts Here -->

				<div class="container-fluid"> <!-- Containter Fluid Starts Here -->
				<div class="row">

					<div class="col-lg-6 col-md-6">
						<div class="card">
							<div class="header">
								<h4 class="card-title">Profile Details</h4>
							</div>
							<div class="content">
								<div class="table-responsive"><?php echo $msg ?>
									<table class="table">
										<thead>
										<tr>
											<th>Title</th>
											<th>Details</th>
										</tr>
										</thead>
										<tbody>
										<?php echo $a_profiledetails; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="card-title">Edit User Details</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="<?php echo base_url().'Cntrl_users/EditRecord/'.$id; ?>">
                                    <div class="form-group">
                                        <input type="text" name="firstname" placeholder="First Name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="lastname" placeholder="Last Name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="username" placeholder="Username" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="tel" name="phone" placeholder="Phone Number" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" placeholder="Email" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <select name="adminlevel" class="form-control">
                                            <option selected disabled>Update Admin Level</option>
                                            <option value="Low">Low</option>
                                            <option value="Medium">Medium</option>
                                            <option value="Full">Full</option>
                                        </select>
                                    </div>
                                    <input type="hidden" value="<?php echo $id; ?>" name="userid">
                                    <input type="submit" name="updatedetails" value="Edit Profile" id="updatedetails" class="btn btn-fill btn-danger">
                                </form>

                            </div>
                        </div>
                        <div class="card">
                            <div class="header">
                                <h4 class="card-title">Delete User</h4>
                            </div>
                            <div class="content">
                                <form action="<?php echo base_url().'Cntrl_users/DeleteRecord/'.$id; ?>" method="post">
                                    <div class="form-group">
                                        <select class="form-control" name="active">
                                            <option selected disabled>Are You Sure You Want To Delete User?</option>
                                            <option value="No">Yes</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                                    <input class="btn btn-fill btn-danger" type="submit" name="deleteuser" value="Remove User">
                                </form>

                            </div>
                        </div>
                    </div>
				</div>

            </div> <!-- Containter Fluid Ends Here -->


		<!-- Content ends here -->

		<!-- Footer Bar starts here -->
		<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
		<!-- Footer Bar ends here -->

		</div> <!-- Containter Fluid Ends Here -->

	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>
