<?php include(APPPATH.'views/_includes/_header.php'); ?>

<div class="wrapper">
	
	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->
	
    <div class="main-panel">
		
		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->
		
		<!-- Content starts here -->

        <div class="content"> <!-- Containter Fluid Starts Here -->
        	<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 col-md-6">
						<div class="card">
                            <div class="header">
                                <h4 class="title">Company Profile for <?php echo $c_name; ?></h4>
                            </div>
							<div class="content table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Details</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php echo $a_profiledetails; ?>
                                    </tbody>
                                </table>
							</div>
						</div>
					</div>
				</div>
			</div>		
			
				
        </div> <!-- Containter Fluid Ends Here -->
			

		<!-- Content ends here -->
		
		<!-- Footer Bar starts here -->
		<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
		<!-- Footer Bar ends here -->
		
		</div> <!-- Containter Fluid Ends Here -->
		
	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>