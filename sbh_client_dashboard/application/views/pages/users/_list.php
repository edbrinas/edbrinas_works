<?php include(APPPATH.'views/_includes/_header.php'); ?>

<div class="wrapper">
	
	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->
	
    <div class="main-panel">
		
		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->
		
		<!-- Content starts here -->

        <div class="content"> <!-- Containter Fluid Starts Here -->
		
            <div class="container-fluid"> <!-- Containter Fluid Starts Here -->
				
				<div class="row">
					<div class="col-lg-6 col-md-6">
						<div class="card">
							<div class="header">
                                <h4>Create New User</h4>
							</div>

							<div class="content">
                                <?php if (count($a_users) < 6 ) { ?>
                                    <form class="form" method="post"
                                          action="<?php echo base_url().'cntrl_users/'; ?>">
                                        <div class="card-content">
                                            <div class="form-group">
                                                <select class="selectpicker" data-style="btn btn-primary btn-fill"
                                                        title="Access Level" name="adminlevel" data-size="7" required>
                                                    <option selected disabled>Access Level</option>
                                                    <option value="Low">Low</option>
                                                    <option value="Medium">Medium</option>
                                                    <option value="Full">Full</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="First Name"
                                                       name="firstname" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Last Name"
                                                       name="lastname" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Position"
                                                       name="position" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Username"
                                                       name="username" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="tel" class="form-control" placeholder="Phone Number"
                                                       name="phone" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Email Address"
                                                       name="email" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" placeholder="Password" class="form-control"
                                                       name="password" required/>
                                            </div>
                                            <input type="hidden" name="companyname"
                                                   value="<?php echo $_SESSION['companyname']; ?>">
                                            <input type="hidden" name="active" value="Yes">
                                        </div>
                                        <div class="footer text-center">
                                            <button type="submit" name="submit" value="btn-signup" class="btn btn-danger btn-fill"
                                                    style="width:100%;">Create User
                                            </button>
                                        </div>
                                    </form>
								<?php
                                } else {
                                    echo "<h4>You have used your maximum number of users (5)</h4>";
                                }
								echo $msg;
                                ?>
							</div>
						</div>

					</div>
                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="card-title">Current Users</h4>
                            </div>
                            <div class="content">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php echo $a_currentuserstable; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>				
				
            </div> <!-- Containter Fluid Ends Here -->
			

		<!-- Content ends here -->
		
		<!-- Footer Bar starts here -->
		<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
		<!-- Footer Bar ends here -->
		
		</div> <!-- Containter Fluid Ends Here -->
		
	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>