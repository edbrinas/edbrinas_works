<?php include(APPPATH.'views/_includes/_header.php'); ?>
<?php include('_header.php'); ?>

<div class="wrapper">

	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->

    <div class="main-panel">

		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->

		<!-- Content starts here -->

        <div class="content"> <!-- Containter Fluid Starts Here -->

            <div class="container-fluid"> <!-- Containter Fluid Starts Here -->

				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="card">
							<div class="header">
								<h4 class="card-title"><?php echo $tbl_title; ?></h4>
							</div>
							<div class="content">
								<?php if ($show_not_contacted) { ?>
											<p><a href="<?php echo base_url().'Cntrl_contact/ViewNotContacted/'; ?>" class="btn btn-fill btn-danger">View Not Yet Contacted</a></p>
								<?php } ?>
								<div class="table-responsive">
									<table id="table" class="display compact" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>Name</th>
												<th>Phone</th>
												<th>Email</th>
												<th>Contacted</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<?php echo $a_tblallcontactformsubmission; ?>
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>
				</div>

            </div> <!-- Containter Fluid Ends Here -->


		<!-- Content ends here -->

		<!-- Footer Bar starts here -->
		<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
		<!-- Footer Bar ends here -->

		</div> <!-- Containter Fluid Ends Here -->

	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>
