<?php include(APPPATH.'views/_includes/_header.php'); ?>

<div class="wrapper">

	<!-- Side Bar starts here -->
	<?php include(APPPATH.'views/_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->

    <div class="main-panel">

		<!-- Upper Bar starts here -->
		<?php include(APPPATH.'views/_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->

        <div class="content"> <!-- Content starts here -->

            <div class="container-fluid"> <!-- Containter Fluid Starts Here -->

						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="card">
									<div class="header">
										<h4 class="card-title">Contact Details</h4>
									</div>
									<div class="content">
										<div class="table-responsive">
											<table class="table">
												<thead>
												<tr>
													<th>Title</th>
													<th>Details</th>
												</tr>
												</thead>
												<tbody>
												<?php echo $a_tblcontactdetails; ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="card">
									<div class="header">
										<h4 class="card-title">Edit Details</h4>
									</div>
									<div class="content">
										<form method="post" action="<?php echo base_url().$controller_main.'EditRecord/'.$id; ?>">
											<div class="form-group">
												<input type="text" name="firstname" class="form-control" placeholder="Update First Name" required />
											</div>
											<div class="form-group">
												<input type="text" name="lastname" class="form-control" placeholder="Update Last Name" required />
											</div>
											<div class="form-group">
												<input type="tel" name="phone" class="form-control" placeholder="Update Phone Number" required />
											</div>
											<div class="form-group">
												<input type="email" name="email" class="form-control" placeholder="Update Email Address" required />
											</div>
											<div class="form-group">
												<select name="contacted" class="form-control">
													<option selected disabled>Mark As Contacted?</option>
													<option value="Yes">Yes</option>
													<option value="No">No</option>
												</select>
											</div>
											<div class="form-group">
												<textarea name="notes" class="form-control" placeholder="Add Notes"></textarea>
											</div>
											<input type="hidden" value="<?php echo $id ?>" name="id">
											<input type="submit" value="Update Contact" class="btn btn-fill btn-danger" name="updatedetails" id="updatedetails">
										</form>
										<?php echo $msg ?>
									</div>
								</div>
							</div>
						</div>

            </div> <!-- Containter Fluid Ends Here -->

						<!-- Footer Bar starts here -->
						<?php include(APPPATH.'views/_includes/_footerbar.php'); ?>
						<!-- Footer Bar ends here -->

		</div> <!-- Content ends here -->

	</div>
</div>

<?php include(APPPATH.'views/_includes/_footer.php'); ?>
