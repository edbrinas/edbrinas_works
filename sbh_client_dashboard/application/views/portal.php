<?php include('_includes/_header.php'); ?>
<?php include('_includes/_initialize_tables.php'); ?>

<div class="wrapper">

	<!-- Side Bar starts here -->
	<?php include('_includes/_sidebar.php'); ?>
	<!-- Side Bar starts here -->

    <div class="main-panel">

		<!-- Upper Bar starts here -->
		<?php include('_includes/_upperbar.php'); ?>
		<!-- Upper Bar starts here -->

		<!-- Content starts here -->

        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Recent Contact Submissions</h4>
                                <p class="category">Most Recent From Contact Form</p>
                            </div>
                            <div class="content table-responsive">
															<table id="tblcontactsubmissions" class="display compact" cellspacing="0" width="100%">
																<thead>
																	<tr>
																		<th>Name</th>
																		<th>Phone</th>
																		<th>Email</th>
																		<th>Actions</th>
																	</tr>
																</thead>
																<tbody>
																	<?php echo $a_tblcontactsubmissions; ?>
																</tbody>
															</table>
															<a href="<?php echo base_url().'Cntrl_contact/ViewRecords/'; ?>" class="btn btn-danger btn-fill">View All</a>
															<!--
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php echo $a_tblcontactsubmissions; ?>
                                    </tbody>
                                </table>
                                <a href="<?php echo base_url().'Cntrl_contact/ViewRecords/'; ?>" class="btn btn-danger btn-fill">View All</a>
															-->

														</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Recent Update Requests</h4>
                                <p class="category">Most Recent Update Requests</p>
                            </div>
                            <div class="content table-responsive">
															<table id="recentupdaterequests" class="display compact" cellspacing="0" width="100%">
																<thead>
																	<tr>
																		<th>Update Type</th>
																		<th>Page Change</th>
																		<th>Change Details</th>
																		<th>Completed</th>
																	</tr>
																</thead>
																<tbody>
																	<?php echo $a_recentupdaterequests; ?>
																</tbody>
															</table>
															<a href="<?php echo base_url().'Cntrl_requests/ViewRecords/'; ?>" class="btn btn-danger btn-fill">View All</a>
																<!--
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Update Type</th>
                                        <th>Page Change</th>
                                        <th>Change Details</th>
                                        <th>Completed</th>
                                    </tr>
                                    </thead>
                                    <tbody>
						            						<?php echo $a_recentupdaterequests; ?>
                                    </tbody>
                                </table>
															-->
														</div>
                        </div>
                    </div>
                </div>
								<?php if (ENABLE_AFFILIATE_LINK) { ?>
								<div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Affliates</h4>
                                <p class="category">Most Recent From Affiliates Sign Up</p>
                            </div>
                            <div class="content table-responsive">
															<table id="tblaffiliates" class="display compact" cellspacing="0" width="100%">
																<thead>
																	<tr>
																		<th>Name</th>
																		<th>Company Name</th>
																		<th>Position</th>
																		<th>Address</th>
																		<th>City</th>
																		<th>State</th>
																		<th>Postal Code</th>
																	</tr>
																</thead>
																<tbody>
																	<?php echo $a_tblaffiliates; ?>
																</tbody>
															</table>
															<!--
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php echo $a_tblcontactsubmissions; ?>
                                    </tbody>
                                </table>
                                <a href="<?php echo base_url().'Cntrl_contact/ViewRecords/'; ?>" class="btn btn-danger btn-fill">View All</a>
															-->

														</div>
                        </div>
                    </div>
                </div>
								<?php } ?>
								<?php if (SHOW_PAYMENTS_TABLE) { ?>
								<div class="row">
										<div class="col-md-12">
												<div class="card">
														<div class="header">
																<h4 class="title">Payments</h4>
																<p class="category">Most Recent From Payments</p>
														</div>
														<div class="content table-responsive">
															<table id="tblaffiliates" class="display compact" cellspacing="0" width="100%">
																<thead>
																	<tr>
																		<th>Invoice Number</th>
																		<th>Name</th>
																		<th>Address</th>
																		<th>Amount</th>
																		<th>Options</th>
																	</tr>
																</thead>
																<tbody>
																	<?php echo $a_tblpayments; ?>
																</tbody>
															</table>
															<!--
																<table class="table">
																		<thead>
																		<tr>
																				<th>Name</th>
																				<th>Phone</th>
																				<th>Email</th>
																				<th class="text-right">Actions</th>
																		</tr>
																		</thead>
																		<tbody>
																		<?php echo $a_tblcontactsubmissions; ?>
																		</tbody>
																</table>
																<a href="<?php echo base_url().'Cntrl_contact/ViewRecords/'; ?>" class="btn btn-danger btn-fill">View All</a>
															-->

														</div>
												</div>
										</div>
								</div>
								<?php } ?>
            </div>

		<!-- Content ends here -->

		<!-- Footer Bar starts here -->
		<?php include('_includes/_footerbar.php'); ?>
		<!-- Footer Bar ends here -->

		</div>
	</div>
</div>

<?php include('_includes/_footer.php'); ?>
