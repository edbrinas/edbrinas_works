<?php include('_includes/_header.php'); ?>
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>">SBH Client Login</a>
        </div>
        <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo base_url(); ?>">Dashboard</a>
                </li>
                <li>
                    <a href="<?php echo base_url().'Cntrl_public/'; ?>">Affiliate Program Registration</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="wrapper wrapper-full-page">
    <div class="full-page login-page" data-color="blue" data-image="<?php echo base_url(); ?>_assets/img/full-screen-image-1.jpg">

        <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form method="POST" action="<?php echo base_url().'Cntrl_public/'; ?>">
                            <!--   if you want to have the card without animation please remove the ".card-hidden" class   -->
                            <div class="card card-hidden">
                                <div class="header text-center">Affiliate Program Registration</div>

                                <div class="content">
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                                    </div>
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" name="firstname" placeholder="First Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" name="lastname" placeholder="Last Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" class="form-control" name="address" placeholder="Address" required>
                                    </div>
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" class="form-control" name="city" placeholder="City" required>
                                    </div>
                                    <div class="form-group">
                                        <label>State</label>
                                        <select class="form-control" name="state" required>
                            							<option selected disabled>State</option>
                                          <?php
                                          foreach ($a_states as $sname=>$lname) {
                                            ?>
                                              <option value="<?php echo $sname; ?>"><?php echo $lname; ?></option>
                                            <?php
                                          }
                                          ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Postal Code</label>
                                        <input type="number" class="form-control" name="postalcode" placeholder="Postal Code" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input type="text" class="form-control" name="companyname" placeholder="Company Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Position</label>
                                        <input type="text" class="form-control" name="position" placeholder="Company Position" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="number" class="form-control" name="phone" placeholder="Phone Number" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Security Code</label>
                                        <input type="text" class="form-control" name="captcha" placeholder="Please enter security code as it appears below." maxlength="4" required>
                                    </div>
                                    <div class="form-group" align="center">
                                      <span id="captcha-img" class="captcha-img">
                                          <?php echo $img_captcha; ?>
                                      </span>
                                      <a href="#" class="reload-captcha" >
                                        <img height="30" width="35" class="reload-img" src="<?php echo base_url().'_assets/img/refresh-icon.png'; ?>" alt="reload" />
                                      </a>
                                    </div>
                                </div>
                                <?php echo ($msg) ? $msg : "" ; ?>
                                <div class="footer text-center">
                                    <input type="hidden" name="ref_by" id="ref_by" value="<?php echo ($_GET['ref_by']) ? $_GET['ref_by'] : 0 ; ?>" />
                                    <button type="submit" name="btn-register" id="btn-aff-reg" class="btn btn-fill btn-danger btn-wd">Register</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
        <footer class="footer footer-transparent">
            <div class="container">
                <p class="copyright pull-right" style="color:white;">
                    Copyright&copy; <?php echo date('Y'); ?>  <a href="https://sitesbyheroes.com">Sites By Heroes</a> | All Rights Reserved
                </p>
            </div>
        </footer>
    </div>
</div>
<?php include('_includes/_footer.php'); ?>
<?php include('_includes/_generatecaptcha.php'); ?>
