<?php
session_start();
$_SESSION["logged"] = "";
session_destroy();
#include("connections/cn.php");
include("includes/header.main.php");

$msg = $_GET["msg"];
if($msg==1)
{
	$show_msg = 1;
	$msg = "Invalid username or password";
}
else if($msg==2)
{
	$show_msg = 1;
	$msg = "You have been logged out";
	$uid = $_GET["id"];
	$activity = "Log out to the system";
	include_once("./includes/function.php");
	insertEventLog($uid,$activity);
}
else if($msg==3)
{
	$show_msg = 1;
	$msg = "You have been logged out due to inactivity.";
}
else if($msg==4)
{
	$show_msg = 1;
	$msg = "You are not allowed to access this page, please log-in!";
}
?>
<!-- Start of main table -->
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="2" class="main">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<form name="form1" method="post" action="includes/login.authenticate.php">
      <table width="26%" border="0" align="center" cellpadding="2" cellspacing="2" class="main">
			<tr>
			  <th colspan="2" bgcolor="#FF4415" scope="col"><div align="left"><img src="images/bullet_hp_dot.gif" width="10" height="10" />&nbsp;Log-in</div></th>
		    </tr>
			<?php
			if ($msg != "")
			{
				?>
				<tr>
					<th colspan="2" scope="col"><?php echo $msg; ?></th>
				</tr>
			<?php
			}
				?>
		  <tr>
		    <th width="32%" scope="col">Username</th>
			  <th width="68%" scope="col"><label>
			    <input name="username" type="text" id="username">
		    </label></th>
			</tr>
		  <tr>
		    <th scope="col">Password</th>
			  <th scope="col"><label>
			    <input name="password" type="password" id="password">
		    </label></th>
			</tr>
			<tr>
			  <th colspan="2" scope="col"><label>
			    <input name="Submit" type="submit" id="Submit" value="Log-in">
			  </label></th>
		  </tr>
	    </table>
      </form>    
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<!-- End of main table -->
<?php
	include("includes/footer.main.php");
?>