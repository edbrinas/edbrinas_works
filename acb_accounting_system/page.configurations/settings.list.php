<?php 
include("./../includes/check.admin.rights.php");
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$page_size = getConfigurationValueByName('max_record_per_page');

$record_id = $_GET['id'];
$method = $_GET['method'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 

if($order=="")
{
  	$order = "cfg_id";
}
if ($get_msg==1)
{
	$msg = "Record updated successfully!";
	$display_msg = 1;
}

if($cur_page=="")
{
   	$cur_page= 1;
}
if($cur_page==1)
{
	$count_page = 0;
}
else
{
  	$count_page = (($cur_page - 1) * $page_size);
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
  <tr>
    <td class="redlabelheader">Warning! Altering any of the settings below may damage the system!</td>
  </tr>
  <tr>
    <td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
			$sql1 = "	SELECT 	SQL_BUFFER_RESULT
								SQL_CACHE
								cfg_id,
								cfg_name,
								cfg_value,
								cfg_description 
						FROM 	tbl_config
						ORDER BY $order DESC ";
			$rs2 = mysql_query($sql1) or die("Error in sql1 in module: settings.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs2);
			$total_pages = ceil($total_records/$page_size);
			
			$sql2 = "	SELECT 	SQL_BUFFER_RESULT
								SQL_CACHE
								cfg_id,
								cfg_name,
								cfg_value,
								cfg_description 
						FROM 	tbl_config 
						ORDER BY $order DESC 
						LIMIT	$count_page, $page_size";
			$rs = mysql_query($sql2) or die("Error in sql2 in module: settings.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs);	 
			?>
			<thead>
				<tr>
					<th scope="col" class="rounded-header"><a href="<?php echo $page; ?>?order=cfg_id" class="whitelabel">ID</a></th>
					<th scope="col" class="rounded-q1"><a href="<?php echo $page; ?>?order=cfg_name" class="whitelabel">Configuration Name</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=cfg_value" class="whitelabel">Configuration Value</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=cfg_description" class="whitelabel">Configuration Description</a></th>
					<th scope="col" class="rounded-q4"><div class="whitelabel">Options</div></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2" class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td colspan="3" class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							
							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>		
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="5"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="5"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{
				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs);
					$id = $rows["cfg_id"];
					$a = $rows["cfg_name"];
					$b = $rows["cfg_value"];
					$c = $rows["cfg_description"];
					$a = stripslashes($a);
					$b = stripslashes($b);
					$c = stripslashes($c);
				?>      
				 <tr>
					<td><?php echo $id; ?>&nbsp;</td>
					<td><?php echo $a; ?>&nbsp;</td>
					<td><?php echo $b;?>&nbsp;</td>
					<td><?php echo $c;?>&nbsp;</td>
					<td>
						  <?php
						  if ($_SESSION["level"]==1)
						  {
						  ?>
								<a href="settings.edit.php?id=<?php echo encrypt($id); ?>">[Edit]</a>
						  <?php
						  }
						  ?>
					</td>
				</tr>
				<?php 
				} 
				?>
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	
	</td>
  </tr>
</table>
<?php include("./../includes/footer.main.php"); ?>