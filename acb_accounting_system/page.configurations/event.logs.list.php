<?php 
include("./../includes/check.admin.rights.php");
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$page_size = getConfigurationValueByName('max_record_per_page');

$record_id = $_GET['id'];
$action = $_GET['action'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$search_for = $_GET['search_for'];
$search_by = $_GET['search_by'];

if($order=="")
{
  	$order = "event_id";
}
if ($get_msg==1)
{
	$msg = "Record updated successfully!";
	$display_msg = 1;
}

if($cur_page=="")
{
   	$cur_page= 1;
}
if($cur_page==1)
{
	$count_page = 0;
}
else
{
  	$count_page = (($cur_page - 1) * $page_size);
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
<tr>
	<td>
		<form method="get">
			<table id="rounded-search-table">
				<thead>
					<tr>
						<th class="rounded-header"><div class="whitelabel">Search</div></th>
						<th class="rounded-q4">&nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="rounded-foot-left">&nbsp;</td>
						<td class="rounded-foot-right">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td><input name="search_for" type="text" id="search_for" class="formbutton" value="<?php echo $search_for; ?>"></td>
						<td><input type="submit" name="action" value="Search" class="formbutton"></td>
					</tr>
				</tbody>
			</table>
		</form>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
			if ($action == "Search" && $search_for != "")
			{
				$sql1 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE				
									event_date,
									event_owner,
									event_description
							FROM 	tbl_event_logs
							WHERE	event_date LIKE '%".$search_for."%'
							OR		event_owner LIKE '%".$search_for."%'
							OR		event_description LIKE '%".$search_for."%'						
							ORDER BY $order DESC ";

				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE				
									event_date,
									event_owner,
									event_description
							FROM 	tbl_event_logs
							WHERE	event_date LIKE '%".$search_for."%'
							OR		event_owner LIKE '%".$search_for."%'
							OR		event_description LIKE '%".$search_for."%'
							ORDER BY $order DESC 
							LIMIT	$count_page, $page_size";
			}
			else
			{
				$sql1 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE				
									event_date,
									event_owner,
									event_description
							FROM 	tbl_event_logs
							ORDER BY $order DESC ";

				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE				
									event_date,
									event_owner,
									event_description
							FROM 	tbl_event_logs
							ORDER BY $order DESC 
							LIMIT	$count_page, $page_size";
			}	
			$rs1 = mysql_query($sql1) or die("Error in sql1 in module: bank.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs1);
			$total_pages = ceil($total_records/$page_size); 
			$rs2 = mysql_query($sql2) or die("Error in sql2 in module: bank.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs2);	
			?>
			<thead>
				<tr>
					<th width="10%" class="rounded-header" scope="col">Date</th>
					<th width="10%" class="rounded-q2" scope="col">User Name</th>
					<th width="80%" class="rounded-q4" scope="col">Transaction</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2" class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							

							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>		
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="3"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{

				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs2);
					
					$event_date = $rows["event_date"];
					$event_owner = $rows["event_owner"];
					$event_description = $rows["event_description"];
					$event_owner = getFullName($event_owner,3);
				?>      
				 <tr>
					<td><?php echo $event_date; ?>&nbsp;</td>
					<td><?php echo $event_owner; ?>&nbsp;</td>
					<td><?php echo $event_description; ?>&nbsp;</td>
				</tr>
				<?php 
				} 
				?>
			<?php 
			} 
			?>
			</tbody>
	  </table>
	<!-- END OF TABLE-->	
	</td>
</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>