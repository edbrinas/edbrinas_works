<?php 
include("./../includes/check.admin.rights.php");
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];

if ($_POST['Submit'] == 'Submit')
{
	$cfg_name = $_POST['cfg_name'];
	$cfg_value = $_POST['cfg_value'];
	$cfg_description = $_POST['cfg_description'];
	$cfg_name = addslashes($cfg_name);
	$cfg_value = addslashes($cfg_value);
	$cfg_description = addslashes($cfg_description);

	if ($cfg_name != "" && $cfg_value != "" && $cfg_description != "")
	{
		$sql = "SELECT 	cfg_id
				FROM	tbl_config
				WHERE 	cfg_name LIKE '%".$cfg_name."%' 
				AND 	cfg_value LIKE '%".$cfg_value."%'";
		$rs = mysql_query($sql) or die("Query Error1 " .mysql_error());			
		$rec_count = mysql_num_rows($rs);
		
		if ($rec_count == 0)
		{
			$sql = "INSERT INTO tbl_config 
								(
								cfg_name,
								cfg_value,
								cfg_description
								)
					VALUES 		(
								'$cfg_name',
								'$cfg_value',
								'$cfg_description'
								)";
			mysql_query($sql) or die("Query Error1 " .mysql_error());
			insertEventLog($user_id,$sql);
			
			$cfg_name = "";
			$cfg_value = "";
			$cfg_description = "";
			
			$display_msg = 1;
			$msg = "Record inserted successfully!";
		}
		else
		{
			$display_msg = 1;
			$msg = "Record already exists!";
		}
	}
	else
	{
		$display_msg = 1;
		$msg = "Please do not leave the fields blank!";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="system_users" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>System Configuration</strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Submit" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><strong>Configuration Name* </strong></td>
							<td width="80%"><label><input name="cfg_name" type="text" class="formbutton" id="cfg_name" value="<?php echo $cfg_name; ?>" size="100"></label></td>
						</tr>
						<tr>
						  <td><strong>Configuration Value* </strong></td>
						  <td><label><input name="cfg_value" type="text" id="cfg_value" value="<?php echo $cfg_value; ?>" size="100" class="formbutton"></label></td>
						</tr>
						<tr valign="top">
						  <td><strong>Configuration Description* </strong></td>
						  <td><label><textarea name="cfg_description" cols="25" rows="10" class="formbutton"><?php echo $cfg_description; ?></textarea></label></td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>