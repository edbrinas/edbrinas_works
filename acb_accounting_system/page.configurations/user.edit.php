<?php 
#include("./../includes/check.admin.rights.php");
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$access_level = $_SESSION["level"];
$id = $_GET['id'];
$file_id = $_GET['file_id'];
$action = $_GET['action'];
$id = decrypt($id);
$file_id = decrypt($file_id);

if ($file_id != "" && $id != "" && $action == "delete")
{

	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					file_id,
					file_name,
					file_extention
			FROM 	tbl_file_attachment
			WHERE 	file_id = '$file_id'";
	$rs = mysql_query($sql) or die("Query Error " .mysql_error());
	$rows = mysql_fetch_array($rs);
	$file_id = $rows["file_id"];	
	$file_name = $rows["file_name"];			
	$file_extention = $rows["file_extention"];
	
	$htmlDirectory = getWebRootDirectory ();
	$checkFileDestinationDirectory = checkFileDestinationDirectory ($file_extention);
	$doc_name = $htmlDirectory.$checkFileDestinationDirectory.$file_name;
	
	if (unlink($doc_name))
	{
		$log = "Deleted file: ".$doc_name;
		insertEventLog($shift_on_duty,$log);
		
		$sql = "DELETE FROM tbl_file_attachment
				WHERE	file_id = '$file_id'";
		mysql_query($sql) or die("Query Error1 " .mysql_error());
		insertEventLog($usr_id,$sql);
		
		$sql = "UPDATE 	tbl_users 
				SET		usr_digital_signature = ''
				WHERE 	usr_id = $id";	
		mysql_query($sql) or die("Query Error2 " .mysql_error());			
		insertEventLog($usr_id,$sql);	
		
		$msg = "File deleted successfully!";
		$display_msg = 1;
	}
	else
	{
		$display_msg = 1;
		$msg = "Error deleting file!";	
	}
}

if ($id == $user_id || $user_id == 1)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					usr_id,
					usr_username,
					usr_first_name,
					usr_middle_initial,
					usr_last_name,
					usr_account_type,
					usr_digital_signature,
					usr_account_active
			FROM	tbl_users
			WHERE	usr_id = '$id'";
	$rs = mysql_query($sql) or die("Query Error1 " .mysql_error());
	$rows = mysql_fetch_array($rs);
	
	$usr_id = $rows['usr_id']; 
	$username = $rows['usr_username'];
	$firstname = $rows['usr_first_name'];
	$middleinitial = $rows['usr_middle_initial'];
	$lastname = $rows['usr_last_name'];
	$account_type = $rows['usr_account_type'];
	$account_active = $rows['usr_account_active'];
	$usr_digital_signature = $rows['usr_digital_signature'];
}
else
{
	$display_msg=1;
	$msg = "WARNING YOU ARE TRYING TO MANIPULATE THE SYSTEM!";
	insertEventLog($user_id,$msg);
	$_SESSION["logged"] = "";
	session_destroy();	
}
if ($_POST['Submit'] == 'Update')
{
	$change_password = 0;
	
	$id = $_POST['record_id'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$retype_password = $_POST['retype_password'];
	$usr_authentication_password = $_POST['usr_authentication_password'];
	$firstname = $_POST['firstname'];
	$middleinitial = $_POST['middleinitial'];
	$lastname = $_POST['lastname'];
	$account_type = $_POST['account_type'];
	$account_active = $_POST['account_active'];
	$user_file = $_FILES['userfile']['tmp_name'];
	
	$username  = addslashes($username);
	$firstname = addslashes($firstname);
	$middleinitial = addslashes($middleinitial);
	$lastname = addslashes($lastname);
	$usr_authentication_password = md5($usr_authentication_password);

	$password = trim($password);
	$retype_password = trim($retype_password);

	if ($password != "" && $retype_password != "")
	{
		$password = md5($password);
		$retype_password = md5($retype_password);
		$change_password = 1;
	}
	
	if(isset($user_file))
	{
		#for($i=0; $i < count($user_file); $i++)
		#{
			#$temp_file = $_FILES['userfile']['tmp_name'][$i];
			#$file_name = $_FILES['userfile']['name'][$i];
			$edit_digital_signature = 1;
			$temp_file = $_FILES['userfile']['tmp_name'];
			$file_name = $_FILES['userfile']['name'];
						
			if ($file_name != "")
			{
				$file_extention = getFileExtension($file_name);
				$msg .= checkForFileSize($file_size,$file_name);
				$msg .= checkFileExtention($file_extention);
				if ($msg == "")
				{
					$msg .= copyFile($temp_file,$file_name,$file_extention); 
					if ($msg == "")
					{
						$digital_signature = insertToFileTable($date_time_inserted,$username,$file_name,$file_extention,$user_id);
					}
				}
			}
		#}
	}	
	if ($id != "" && $username != "" && $firstname != "" && $middleinitial != "" &&	$lastname != "")
	{
		if ($password == $retype_password)
		{
			if ($change_password == 1)
			{
				$sql = "UPDATE 	tbl_users 
						SET		usr_username  = '$username',
								usr_password = '$password',
								usr_first_name = '$firstname',
								usr_middle_initial = '$middleinitial',
								usr_last_name = '$lastname',
								usr_account_type = '$account_type',
								usr_account_active = '$account_active' ";
				if ($edit_digital_signature == 1)
				{
					$sql .=",usr_digital_signature = '$digital_signature' ";
				}
				if ($usr_authentication_password!="")
				{
					$sql .=",usr_authentication_password = '$usr_authentication_password' ";
				}
				$sql .="WHERE 	usr_id = $id";
			}
			else
			{
				$sql = "UPDATE	tbl_users 
						SET		usr_username  = '$username',
								usr_first_name = '$firstname',
								usr_middle_initial = '$middleinitial',
								usr_last_name = '$lastname',
								usr_account_type = '$account_type',
								usr_account_active = '$account_active' ";
				if ($edit_digital_signature == 1)
				{
					$sql .=",usr_digital_signature = '$digital_signature' ";
				}
				if ($usr_authentication_password!="")
				{
					$sql .=",usr_authentication_password = '$usr_authentication_password' ";
				}				
				$sql .="WHERE 	usr_id = $id";
			}
			mysql_query($sql) or die("Query Error ".$sql.mysql_error());
			
			$display_msg=1;
			insertEventLog($usr_id,$sql);
			
			if ($access_level == 1)
			{
				header("location: /workspaceGOP/page.configurations/user.list.php?msg=1");
			}
			else
			{
				$display_msg=1;
				$msg = "Record updated successfully!";	
			}
		}
		else
		{
			$display_msg=1;
			$msg = "Password did not match!";
		}		
	}
	else
	{
		$display_msg=1;
		$msg = "Please do not leave the fields blank!";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="system_users" method="post" enctype="multipart/form-data">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>System Users</strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton" /></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="17%"><strong>Username* </strong></td>
							<td width="83%"><label><input name="username" type="text" class="formbutton" id="username" value="<?php echo $username; ?>" readonly></label></td>
						</tr>
						<tr>
						  <td><strong>Password* </strong></td>
						  <td><label><input name="password" type="password" id="password" class="formbutton"></label></td>
						</tr>
						<tr>
						  <td><strong>Re-type Password* </strong></td>
						  <td><input name="retype_password" type="password" id="retype_password" class="formbutton" /></td>
						</tr>
						<?php
						if ($access_level== 1 || $access_level == 2 || $access_level == 3)
						{
						?>
						<tr>
						  <td><b>Approval Authentication Password *</b></td>
						  <td><input name="usr_authentication_password" type="password" id="usr_authentication_password" class="formbutton" /></td>
						</tr>
						<?php
						}
						?>
						<tr>
							<td><strong>First Name*  </strong></td>
							<td><label><input name="firstname" type="text" id="firstname" class="formbutton" value="<?php echo $firstname; ?>"/></label></td>
						</tr>
						<tr>
							<td><strong>Middle Initial* </strong></td>
							<td><label><input name="middleinitial" type="text" id="middleinitial" class="formbutton" value="<?php echo $middleinitial; ?>" size="1"/></label></td>
						</tr>
						<tr>
							<td><strong>Last Name*  </strong></td>
							<td><input name="lastname" type="text" id="lastname" class="formbutton" value="<?php echo $lastname; ?>"/></td>
						</tr> 
						<tr>
							<td><strong>Digital Signature*  </strong></td>
							<td>
							<?php
								$encrypt_record_id = $usr_id;
								$encrypt_record_id = encrypt($encrypt_record_id);
								
								$sql = "SELECT	SQL_BUFFER_RESULT
												SQL_CACHE
												file_id,
												file_name
										FROM 	tbl_file_attachment
										WHERE 	file_id = '$usr_digital_signature'";
								$rs = mysql_query($sql) or die("Query Error " .mysql_error());
								$totalRows = mysql_num_rows($rs);	 
								
								if ($totalRows != 0)
								{
									for($i=0;$i<$totalRows;$i++)
									{
										$rows = mysql_fetch_array($rs);
										$file_id = $rows["file_id"];			
										$file_name = $rows["file_name"];
										
										$file_id = encrypt($file_id);		
										?>
											<a href='/workspaceGOP/includes/document.download.php?id=<?php echo $file_id; ?>'><?php echo $file_name; ?></a> 
											| 
											<a href="<?php echo $page; ?>user.edit.php?id=<?php echo $encrypt_record_id; ?>&file_id=<?php echo $file_id; ?>&action=delete">Delete File</a>
									  <?php
									}						
								}
								else
								{
									?>
									<input name='userfile' type='file' />
									<?
								}
								?>							</td>
						</tr> 					  
						<?php
						if ($access_level==1)
						{
						?>
						<tr>
							<td><strong>Account Level*</strong></td>
							<td>
								<?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_value,
													cfg_description
											FROM 	tbl_config
											WHERE 	cfg_name = 'account_type'";
									$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
									$str .= "<select name='account_type' class='formbutton'>";
									while($data=mysql_fetch_array($rs))
									{
										$column_id = $data['cfg_value'];
										$column_name = $data['cfg_description'];
										$str .= "<option value='$column_id'";
										if ($account_type == $column_id)
										{
											$str .= " SELECTED";
										}
										$str .= ">".$column_name."";
									}
									$str .= "</select>&nbsp;";
									echo $str;
								?>							</td>
						</tr>  
						<tr>
							<td><strong>User Status*</strong></td>
							<td>
								<select name="account_active" class="formbutton">
								<option value="1" <?php if ($account_active == 1) { echo "selected"; } ?>>Active</option>
								<option value="0" <?php if ($account_active == 0) { echo "selected"; } ?>>Inactive</option>
								</select>							</td>
						</tr>   
						<?php 
						}
						else
						{
							?>
							<input type='hidden' name="account_type" value="<?php echo $account_type; ?>" >
							<input type='hidden' name="account_active" value="<?php echo $account_active; ?>" >
							<?php
						}
						?>      
						<input type='hidden' name="record_id" value="<?php echo $usr_id; ?>" > 
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>
