<?php 
include("./../includes/check.admin.rights.php");
include("./../includes/header.main.php");


$display_msg = 0;
$user_id = $_SESSION["id"];
$date_time_inserted = date("Y-m-d H:i:s");

if ($_POST['Submit'] == 'Submit')
{
	$msg = "";
	
	$username = $_POST['username'];
	$password = $_POST['password'];
	$retype_password = $_POST['retype_password'];
	$firstname = $_POST['firstname'];
	$middleinitial = $_POST['middleinitial'];
	$lastname = $_POST['lastname'];
	$account_type = $_POST['account_type'];
	$account_active = $_POST['account_active'];
	$user_file = $_FILES['userfile']['tmp_name'];

	$password = md5($password);
	$retype_password = md5($retype_password);
	
	$username = trim($username);
	$firstname = trim($firstname);
	$middleinitial = trim($middleinitial);
	$lastname = trim($lastname);
	$account_type = trim($account_type);
	$account_active = trim($account_active);
		
	$firstname = sentenceCase($firstname);
	$middleinitial = sentenceCase($middleinitial);
	$lastname = sentenceCase($lastname);
	
	if (is_null($username))	{ $display_msg = 1; $msg .= "Please enter username<br>"; }
	if ($password != $retype_password) { $display_msg = 1; $msg .= "Password mismatch!<br>"; }
	if (is_null($firstname)) { $display_msg = 1; $msg .= "Please enter firstname<br>"; }	
	if (is_null($middleinitial)) { $display_msg = 1; $msg .= "Please enter firstname<br>"; }	
	if (is_null($lastname)) { $display_msg = 1; $msg .= "Please enter lastname<br>"; }	
	if (is_null($account_type)) { $display_msg = 1; $msg .= "Please select account typr<br>"; }	
	if (is_null($account_active == "")) { $display_msg = 1; $msg .= "Please select account active<br>"; }	

	if(isset($user_file))
	{
		#for($i=0; $i < count($user_file); $i++)
		#{
			#$temp_file = $_FILES['userfile']['tmp_name'][$i];
			#$file_name = $_FILES['userfile']['name'][$i];
		
			$temp_file = $_FILES['userfile']['tmp_name'];
			$file_name = $_FILES['userfile']['name'];
						
			if ($file_name != "")
			{
				$file_extention = getFileExtension($file_name);
				$msg .= checkForFileSize($file_size,$file_name);
				$msg .= checkFileExtention($file_extention);
				if ($msg == "")
				{
					$msg .= copyFile($temp_file,$file_name,$file_extention); 
					if ($msg == "")
					{
						$digital_signature = insertToFileTable($date_time_inserted,$username,$file_name,$file_extention,$user_id);
					}
				}
			}
		#}
	}	
	$sql = "SELECT 	usr_id
			FROM 	tbl_users
			WHERE 	usr_username ='$username'";
	$rs = mysql_query($sql) or die("Query Error1 " .mysql_error());			
	$total_records = mysql_num_rows($rs);
	
	if ($total_records >= 1) { $display_msg = 1; $msg .= "Username already exists<br>"; }	
	
	if ($msg == "")
	{
		$sql = "INSERT	INTO	tbl_users
								(
								usr_username,
								usr_password,
								usr_first_name,
								usr_middle_initial,
								usr_last_name,
								usr_account_type,
								usr_digital_signature,
								usr_account_active
								)
				VALUES 			(
								'$username',
								'$password',
								'$firstname',
								'$middleinitial',
								'$lastname',
								'$account_type',
								'$digital_signature',
								'$account_active'
								)";
		mysql_query($sql) or die("Query Error " .mysql_error());
		
		insertEventLog($user_id,$sql);
		$display_msg = 1;
		$msg = "Record inserted successfully!";
		
		$username = "";
		$password = "";
		$retype_password = "";
		$firstname = "";
		$middleinitial = "";
		$lastname = "";
		$account_type = "";
		$account_active = "";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="system_users" method="post" enctype="multipart/form-data">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>System Users</strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Submit" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="17%"><strong>Username* </strong></td>
							<td width="83%"><label><input name="username" type="text" class="formbutton" id="username" value="<?php echo $username; ?>"></label></td>
						</tr>
						<tr>
						  <td><strong>Password* </strong></td>
						  <td><label><input name="password" type="password" id="password" class="formbutton"></label></td>
						</tr>
						<tr>
						  <td><strong>Re-type Password* </strong></td>
						  <td><input name="retype_password" type="password" id="retype_password" class="formbutton" /></td>
						</tr>
						<tr>
							<td><strong>First Name*  </strong></td>
							<td><label><input name="firstname" type="text" id="firstname" class="formbutton" value="<?php echo $firstname; ?>"/></label></td>
						</tr>
						<tr>
							<td><strong>Middle Initial* </strong></td>
							<td><label><input name="middleinitial" type="text" id="middleinitial" class="formbutton" value="<?php echo $middleinitial; ?>" size="1"/></label></td>
						</tr>
						<tr>
							<td><strong>Last Name*  </strong></td>
							<td><input name="lastname" type="text" id="lastname" class="formbutton" value="<?php echo $lastname; ?>"/></td>
						</tr> 
						<tr>
							<td><strong>Digital Signature*</strong></td>
							<td><input name='userfile' type='file' /></td>
						</tr> 						  
						
						<tr>
							<td><strong>Account Level*</strong></td>
							<td>
								<?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_value,
													cfg_description
											FROM 	tbl_config
											WHERE 	cfg_name = 'account_type'";
									$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
									$str .= "<select name='account_type' class='formbutton'>";
									while($data=mysql_fetch_array($rs))
									{
										$column_id = $data['cfg_value'];
										$column_name = $data['cfg_description'];
										$str .= "<option value='$column_id'";
										if ($account_type == $column_id)
										{
											$str .= " SELECTED";
										}
										$str .= ">".$column_name."";
									}
									$str .= "</select>&nbsp;";
									echo $str;
								?>			
							</td>
						</tr> 
						          
						<tr>
							<td><strong>User Status*</strong></td>
							<td>
								<select name="account_active" class="formbutton">
								<option value="1" <?php if ($account_active == 1) { echo "selected"; } ?>>Active</option>
								<option value="0" <?php if ($account_active == 0) { echo "selected"; } ?>>Inactive</option>
								</select>			
							</td>
						</tr>          
						
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>