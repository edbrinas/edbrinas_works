<?php 
include("./../includes/check.admin.rights.php");
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$year = date("Y");
$startYear = date("Y") - 1;
$endYear = date("Y");
$date_time_inserted = date("Y-m-d H:i:s");

if ($_POST['Submit'] == 'Close Accounting Year')
{
	$cl_end_year = $_POST['cl_end_year'];
	$reference_year = $cl_end_year + 1;
	
	if ($cl_end_year!="")
	{
		$sql = "SELECT 	SQL_BUFFER_RESULT
						SQL_CACHE
						a.sl_id AS sl_id,
						b.cfg_id AS cfg_id
				FROM 	tbl_subsidiary_ledger a,
						tbl_config b
				WHERE 	b.cfg_name = 'currency_type'
				ORDER BY a.sl_id ASC";
		$rs = mysql_query($sql) or die("SQL 1".$sql." ".mysql_error());
		while($rows=mysql_fetch_array($rs))
		{
			$sql3 = "SELECT SQL_BUFFER_RESULT
							SQL_CACHE
							(SUM(gl_debit)-SUM(gl_credit)) AS SMDRCR
					FROM 	tbl_general_ledger 
					WHERE 	gl_account_code = '".$rows['sl_id']."'
					AND		gl_currency_code = '".$rows['cfg_id']."'
					AND		gl_date LIKE '".$cl_end_year."%'";	
			$rs3 = mysql_query($sql3) or die("SQL 2".$sql3." ".mysql_error());
			while($rows3=mysql_fetch_array($rs3))
			{				
				$sql4 = "SELECT SQL_BUFFER_RESULT
								SQL_CACHE
								COUNT(*) AS record_count
						 FROM	tbl_subsidiary_ledger_details
						 WHERE	sl_details_reference_number = '".$rows['sl_id']."'
						 AND	sl_details_year = '".$reference_year."'
						 AND	sl_details_currency_code = '".$rows['cfg_id']."'";	
				$rs4 = mysql_query($sql4) or die("SQL 3".$sql4." ".mysql_error());		
				$rows4=mysql_fetch_array($rs4);
				if ($rows4['record_count']!=1)
				{
					$sql5 = "INSERT INTO tbl_subsidiary_ledger_details (sl_details_reference_number,	
																		sl_details_year,	
																		sl_details_currency_code,	
																		sl_details_starting_balance,	
																		sl_details_ending_balance)
							 VALUES ('".$rows['sl_id']."',
									'".$reference_year."',
									'".$rows['cfg_id']."',
									'".$rows3["SMDRCR"]."',
									'0')";
					$rs5 = mysql_query($sql5) or die("SQL 4".$sql5." ".mysql_error());
					insertEventLog($user_id,$sql5);
				}
				else
				{
					if ($rows3['SMDRCR']!=0 || $rows3['SMDRCR']!="")
					{
						$sql6 = "UPDATE tbl_subsidiary_ledger_details 
								 SET 	sl_details_starting_balance = '".$rows3['SMDRCR']."'
								 WHERE	sl_details_reference_number = '".$rows['sl_id']."'
								 AND	sl_details_year = '".$reference_year."'
								 AND	sl_details_currency_code = '".$rows['cfg_id']."'";
						$rs6 = mysql_query($sql6) or die("SQL 5".$sql6." ".mysql_error());
						insertEventLog($user_id,$sql6);
					}
				}				
			}
		}

		$sql = "SELECT 	COUNT(*) AS record_count
				FROM	tbl_config
				WHERE	cfg_value = '".$reference_year."'";
		$rs = mysql_query($sql) or die("SQL".$sql." ".mysql_error());
		$rows=mysql_fetch_array($rs);
		if ($rows["record_count"]==0)
		{
			$sql = "INSERT INTO tbl_config (cfg_name,cfg_value) 
					VALUES 		('budget_identification','".$reference_year."')";
			mysql_query($sql) or die("SQL 6".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
			
			$sql = "INSERT INTO tbl_config (cfg_name,cfg_value) 
					VALUES 		('closed_accounting_year','".$cl_end_year."')";
			mysql_query($sql) or die("SQL 6".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
		}
		
		$display_msg=1;
		$msg .= "Successfully closed accounting year ".$cl_end_year.".";	
	}
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
		<form method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th width="20%" class="rounded-header" scope="col"><div class="main" align="left"><strong>Close Accounting Year </strong></div></th>
							<th width="80%" class="rounded-q4" scope="col"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left">&nbsp;</td>
							<td class="rounded-foot-right" align="right"><input type="submit" name="Submit" value="Close Accounting Year" class="formbutton"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td colspan="2"><div align="center" class="redlabel"><b>IMPORTANT REMINDER!<br>
							Back-up the database first before closing the accounting year.<br>
							 If you proceed, this action will not be reverted!</b></div></td>
						</tr>
						<tr valign="top">
						  <td><b>Accounting Year*</b></td>
						  <td>
							<select name='cl_end_year' class="formbutton">
                            <?php
							$sql = "SELECT 	cfg_value
									FROM	tbl_config
									WHERE	cfg_name = 'budget_identification'
									ORDER BY cfg_id DESC
									LIMIT 1";
							$rs = mysql_query($sql) or die("SQL ".$sql." ".mysql_error());
							$rows = mysql_fetch_array($rs);
							?>
							<option value='<?php echo $rows["cfg_value"]; ?>'><?php echo $rows["cfg_value"]; ?></option>
							</select>						
                          </td>
					  </tr>																																
					</tbody>
				</table>
		  </form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>