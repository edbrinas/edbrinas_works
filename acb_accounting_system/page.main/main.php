<?php 
include("./../includes/check.login.php");
include("./../includes/header.main.php");
#include("./../includes/function.php"); 
include("./../includes/timer.includes.php"); 
include("./../includes/http.function.php"); 
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
          <table id="rounded-add-entries" align="center">
            <thead>
              <tr>
                <th scope="col" width="37%" class="rounded-header"><div class="main" align="left"></div></th>
                <th scope="col" width="63%" class="rounded-q4"><div class="main" align="right"><strong>Accounting and General Ledger System Version 1.1</strong></div></th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <td class="rounded-foot-left" align="left">&nbsp;</td>
                <td class="rounded-foot-right" align="right">&nbsp;</td>
              </tr>
            </tfoot>
            <tbody>				
			  <?php
					$year_now = date("Y");
			  		$sqla = "SELECT	COUNT(DISTINCT dv.dv_id) AS dv_certify_count
							FROM	tbl_disbursement_voucher AS dv,
									tbl_disbursement_voucher_details AS dv_details
							WHERE 	dv.dv_id = dv_details.dv_details_reference_number
							AND 	dv.dv_obligated_by = 0
							AND 	dv.dv_obligated_by_date = '0000-00-00 00:00:00'							
							AND 	dv.dv_certified_by = 0
							AND 	dv.dv_certified_by_date = '0000-00-00 00:00:00'
							AND 	dv.dv_approved_by = 0
							AND 	dv.dv_approved_by_date = '0000-00-00 00:00:00'
							AND		dv.dv_cancelled_by = 0
							AND		dv.dv_cancelled_by_date = '0000-00-00 00:00:00'
							AND 	dv_details.dv_details_debit IS NOT NULL
							AND 	dv_details.dv_details_credit IS NOT NULL";
					$rsa = mysql_query($sqla) or die("Error in sql1 in module: main.php ".$sqla." ".mysql_error());
					$rowsa = mysql_fetch_array($rsa);
					$dv_certify_counta = $rowsa['dv_certify_count'];
					
			  		$sqlb = "SELECT	COUNT(DISTINCT jv.jv_id) AS jv_certify_count
							FROM	tbl_journal_voucher AS jv,
									tbl_journal_voucher_details AS jv_details
							WHERE 	jv.jv_id = jv_details.jv_details_reference_number
							AND 	jv.jv_obligated_by = 0
							AND 	jv.jv_obligated_by_date = '0000-00-00 00:00:00'								
							AND 	jv.jv_certified_by = 0
							AND 	jv.jv_certified_by_date = '0000-00-00 00:00:00'
							AND 	jv.jv_approved_by = 0
							AND 	jv.jv_approved_by_date = '0000-00-00 00:00:00'
							AND		jv.jv_cancelled_by = 0
							AND		jv.jv_cancelled_by_date = '0000-00-00 00:00:00'							
							AND 	jv_details.jv_details_debit IS NOT NULL
							AND 	jv_details.jv_details_credit IS NOT NULL";
					$rsb = mysql_query($sqlb) or die("Error in sql1 in module: main.php ".$sqlb." ".mysql_error());
					$rowsb = mysql_fetch_array($rsb);
					$jv_certify_countb = $rowsb['jv_certify_count'];
					
			  		$sqlc = "SELECT	COUNT(DISTINCT cv.cv_id) AS cv_certify_count
							FROM	tbl_cash_voucher AS cv,
									tbl_cash_voucher_details AS cv_details
							WHERE 	cv.cv_id = cv_details.cv_details_reference_number
							AND 	cv.cv_obligated_by = 0
							AND 	cv.cv_obligated_by_date = '0000-00-00 00:00:00'								
							AND 	cv.cv_certified_by = 0
							AND 	cv.cv_certified_by_date = '0000-00-00 00:00:00'
							AND 	cv.cv_approved_by = 0
							AND 	cv.cv_approved_by_date = '0000-00-00 00:00:00'
							AND		cv.cv_cancelled_by = 0
							AND		cv.cv_cancelled_by_date = '0000-00-00 00:00:00'							
							AND 	cv_details.cv_details_debit IS NOT NULL
							AND 	cv_details.cv_details_credit IS NOT NULL";
					$rsc = mysql_query($sqlc) or die("Error in sql1 in module: main.php ".$sqlc." ".mysql_error());
					$rowsc = mysql_fetch_array($rsc);
					$cv_certify_countc = $rowsc['cv_certify_count'];	
			  ?> 
              <tr>
                <td align="right"><span style="font-weight: bold">Obligate </span></td>
                <td align="left">
                <?php
				if ($_SESSION['level']==5 || $_SESSION['level']==1)
				{
						echo "<a href='/workspaceGOP/page.transactions/disbursement.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=dv_id&sort_by=obligate'>Disbursement Voucher</a> ( ".$dv_certify_counta." )";
						echo "&nbsp;&nbsp;";
						echo "<a href='/workspaceGOP/page.transactions/journal.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=jv_id&sort_by=obligate'>Journal Voucher</a> ( ".$jv_certify_countb." )";
						echo "&nbsp;&nbsp;";
						echo "<a href='/workspaceGOP/page.transactions/cash.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=cv_id&sort_by=obligate'>Cash Voucher</a> ( ".$cv_certify_countc." )";	
						echo "&nbsp;&nbsp;";				
				}
				else
				{
					echo "Disbursement Voucher ( ".$dv_certify_counta." )";
					echo "&nbsp;&nbsp;";
					echo "Journal Voucher ( ".$jv_certify_countb." )";
					echo "&nbsp;&nbsp;";	
					echo "Cash Voucher ( ".$cv_certify_countc." )";
					echo "&nbsp;&nbsp;";														
				}				
				?>
                </td>
              </tr>
			  <?php
			  		$sql = "SELECT	COUNT(DISTINCT dv.dv_id) AS dv_certify_count
							FROM	tbl_disbursement_voucher AS dv,
									tbl_disbursement_voucher_details AS dv_details
							WHERE 	dv.dv_id = dv_details.dv_details_reference_number
							AND 	dv.dv_obligated_by != 0
							AND 	dv.dv_obligated_by_date != '0000-00-00 00:00:00'
							AND 	dv.dv_certified_by = 0
							AND 	dv.dv_certified_by_date = '0000-00-00 00:00:00'
							AND 	dv.dv_approved_by = 0
							AND 	dv.dv_approved_by_date = '0000-00-00 00:00:00'
							AND		dv.dv_cancelled_by = 0
							AND		dv.dv_cancelled_by_date = '0000-00-00 00:00:00'
							AND 	dv_details.dv_details_debit IS NOT NULL
							AND 	dv_details.dv_details_credit IS NOT NULL";
					$rs = mysql_query($sql) or die("Error in sql1 in module: main.php ".$sql." ".mysql_error());
					$rows = mysql_fetch_array($rs);
					$dv_certify_count = $rows['dv_certify_count'];
					
			  		$sql2 = "SELECT	COUNT(DISTINCT jv.jv_id) AS jv_certify_count
							FROM	tbl_journal_voucher AS jv,
									tbl_journal_voucher_details AS jv_details
							WHERE 	jv.jv_id = jv_details.jv_details_reference_number
							AND 	jv.jv_obligated_by != 0
							AND 	jv.jv_obligated_by_date != '0000-00-00 00:00:00'							
							AND 	jv.jv_certified_by = 0
							AND 	jv.jv_certified_by_date = '0000-00-00 00:00:00'
							AND 	jv.jv_approved_by = 0
							AND 	jv.jv_approved_by_date = '0000-00-00 00:00:00'
							AND		jv.jv_cancelled_by = 0
							AND		jv.jv_cancelled_by_date = '0000-00-00 00:00:00'							
							AND 	jv_details.jv_details_debit IS NOT NULL
							AND 	jv_details.jv_details_credit IS NOT NULL";
					$rs2 = mysql_query($sql2) or die("Error in sql1 in module: main.php ".$sql2." ".mysql_error());
					$rows2 = mysql_fetch_array($rs2);
					$jv_certify_count = $rows2['jv_certify_count'];
					
			  		$sql3 = "SELECT	COUNT(DISTINCT cv.cv_id) AS cv_certify_count
							FROM	tbl_cash_voucher AS cv,
									tbl_cash_voucher_details AS cv_details
							WHERE 	cv.cv_id = cv_details.cv_details_reference_number
							AND 	cv.cv_obligated_by != 0
							AND 	cv.cv_obligated_by_date != '0000-00-00 00:00:00'							
							AND 	cv.cv_certified_by = 0
							AND 	cv.cv_certified_by_date = '0000-00-00 00:00:00'
							AND 	cv.cv_approved_by = 0
							AND 	cv.cv_approved_by_date = '0000-00-00 00:00:00'
							AND		cv.cv_cancelled_by = 0
							AND		cv.cv_cancelled_by_date = '0000-00-00 00:00:00'							
							AND 	cv_details.cv_details_debit IS NOT NULL
							AND 	cv_details.cv_details_credit IS NOT NULL";
					$rs3 = mysql_query($sql3) or die("Error in sql1 in module: main.php ".$sql3." ".mysql_error());
					$rows3 = mysql_fetch_array($rs3);
					$cv_certify_count = $rows3['cv_certify_count'];	
			  ?>              
              <tr>
                <td align="right"><span style="font-weight: bold">Certify</span></td>
                <td align="left">
				<?php
					if ($_SESSION['level']==4 || $_SESSION['level']==1)
					{
						echo "<a href='/workspaceGOP/page.transactions/disbursement.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=dv_id&sort_by=certify'>Disbursement Voucher</a> ( ".$dv_certify_count." )";
						echo "&nbsp;&nbsp;";
						echo "<a href='/workspaceGOP/page.transactions/journal.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=jv_id&sort_by=certify'>Journal Voucher</a> ( ".$jv_certify_count." )";
						echo "&nbsp;&nbsp;";
						echo "<a href='/workspaceGOP/page.transactions/cash.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=cv_id&sort_by=certify'>Cash Voucher</a> ( ".$cv_certify_count." )";	
						echo "&nbsp;&nbsp;";											
					}
					else
					{
						echo "Disbursement Voucher ( ".$dv_certify_count." )";
						echo "&nbsp;&nbsp;";
						echo "Journal Voucher ( ".$jv_certify_count." )";
						echo "&nbsp;&nbsp;";	
						echo "Cash Voucher ( ".$cv_certify_count." )";
						echo "&nbsp;&nbsp;";														
					}
				?>				
                </td>
              </tr>
 			  <?php
			  		$sql4 = "SELECT	COUNT(DISTINCT dv.dv_id) AS dv_recommended_count
							FROM	tbl_disbursement_voucher AS dv,
									tbl_disbursement_voucher_details AS dv_details
							WHERE 	dv.dv_id = dv_details.dv_details_reference_number
							AND 	dv.dv_obligated_by != 0
							AND 	dv.dv_obligated_by_date != '0000-00-00 00:00:00'							
							AND		dv.dv_certified_by != 0
							AND		dv.dv_certified_by_date != '0000-00-00 00:00:00'
							AND 	dv.dv_recommended_by = 0
							AND 	dv.dv_recommended_by_date = '0000-00-00 00:00:00'
							AND 	dv.dv_approved_by = 0
							AND 	dv.dv_approved_by_date = '0000-00-00 00:00:00'
							AND		dv.dv_cancelled_by = 0
							AND		dv.dv_cancelled_by_date = '0000-00-00 00:00:00'							
							AND 	dv_details.dv_details_debit IS NOT NULL
							AND 	dv_details.dv_details_credit IS NOT NULL";
					$rs4 = mysql_query($sql4) or die("Error in sql1 in module: main.php ".$sql4." ".mysql_error());
					$rows4 = mysql_fetch_array($rs4);
					$dv_recommended_count = $rows4['dv_recommended_count'];
					
			  		$sql5 = "SELECT	COUNT(DISTINCT jv.jv_id) AS jv_recommended_count
							FROM	tbl_journal_voucher AS jv,
									tbl_journal_voucher_details AS jv_details
							WHERE 	jv.jv_id = jv_details.jv_details_reference_number
							AND 	jv.jv_obligated_by != 0
							AND 	jv.jv_obligated_by_date != '0000-00-00 00:00:00'							
							AND		jv.jv_certified_by != 0
							AND		jv.jv_certified_by_date != '0000-00-00 00:00:00'							
							AND 	jv.jv_recommended_by = 0
							AND 	jv.jv_recommended_by_date = '0000-00-00 00:00:00'
							AND 	jv.jv_approved_by = 0
							AND 	jv.jv_approved_by_date = '0000-00-00 00:00:00'
							AND		jv.jv_cancelled_by = 0
							AND		jv.jv_cancelled_by_date = '0000-00-00 00:00:00'							
							AND 	jv_details.jv_details_debit IS NOT NULL
							AND 	jv_details.jv_details_credit IS NOT NULL";
					$rs5 = mysql_query($sql5) or die("Error in sql1 in module: main.php ".$sql5." ".mysql_error());
					$rows5 = mysql_fetch_array($rs5);
					$jv_recommended_count = $rows5['jv_recommended_count'];		
					
			  ?>             
			  <tr>
                <td align="right"><span style="font-weight: bold">Recommend</span></td>
                <td align="left">
				<?php
					if ($_SESSION['level']==3 || $_SESSION['level']==1)
					{
						echo "<a href='/workspaceGOP/page.transactions/disbursement.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=dv_id&sort_by=recommended'>Disbursement Voucher</a> ( ".$dv_recommended_count." )";
						echo "&nbsp;&nbsp;";
						echo "<a href='/workspaceGOP/page.transactions/journal.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=jv_id&sort_by=recommended'>Journal Voucher</a> ( ".$jv_recommended_count." )";
						echo "&nbsp;&nbsp;";
					}
					else
					{				
						echo "Disbursement Voucher ( ".$dv_recommended_count." )";
						echo "&nbsp;&nbsp;";
						echo "Journal Voucher ( ".$jv_recommended_count." )";
						echo "&nbsp;&nbsp;";
				
					}
				?>				
                </td>
              </tr>
  			  <?php
			  		$sql6 = "SELECT	COUNT(DISTINCT dv.dv_id) AS dv_approved_count
							FROM	tbl_disbursement_voucher AS dv,
									tbl_disbursement_voucher_details AS dv_details
							WHERE 	dv.dv_id = dv_details.dv_details_reference_number
							AND 	dv.dv_obligated_by != 0
							AND 	dv.dv_obligated_by_date != '0000-00-00 00:00:00'							
							AND 	dv.dv_recommended_by != 0
							AND 	dv.dv_recommended_by_date != '0000-00-00 00:00:00'
							AND 	dv.dv_certified_by != 0
							AND 	dv.dv_certified_by_date != '0000-00-00 00:00:00'
							AND 	dv.dv_approved_by = 0
							AND 	dv.dv_approved_by_date = '0000-00-00 00:00:00'
							AND		dv.dv_cancelled_by = 0
							AND		dv.dv_cancelled_by_date = '0000-00-00 00:00:00'							
							AND 	dv_details.dv_details_debit IS NOT NULL
							AND 	dv_details.dv_details_credit IS NOT NULL";
					$rs6 = mysql_query($sql6) or die("Error in sql1 in module: main.php ".$sql6." ".mysql_error());
					$rows6 = mysql_fetch_array($rs6);
					$dv_approved_count = $rows6['dv_approved_count'];
					
			  		$sql7 = "SELECT	COUNT(DISTINCT jv.jv_id) AS jv_approved_count
							FROM	tbl_journal_voucher AS jv,
									tbl_journal_voucher_details AS jv_details
							WHERE 	jv.jv_id = jv_details.jv_details_reference_number
							AND 	jv.jv_obligated_by != 0
							AND 	jv.jv_obligated_by_date != '0000-00-00 00:00:00'							
							AND 	jv.jv_recommended_by != 0
							AND 	jv.jv_recommended_by_date != '0000-00-00 00:00:00'
							AND 	jv.jv_certified_by != 0
							AND 	jv.jv_certified_by_date != '0000-00-00 00:00:00'
							AND 	jv.jv_approved_by = 0
							AND 	jv.jv_approved_by_date = '0000-00-00 00:00:00'
							AND		jv.jv_cancelled_by = 0
							AND		jv.jv_cancelled_by_date = '0000-00-00 00:00:00'							
							AND 	jv_details.jv_details_debit IS NOT NULL
							AND 	jv_details.jv_details_credit IS NOT NULL";
					$rs7 = mysql_query($sql7) or die("Error in sql1 in module: main.php ".$sql7." ".mysql_error());
					$rows7 = mysql_fetch_array($rs7);
					$jv_approved_count = $rows7['jv_approved_count'];		
					
			  		$sql8 = "SELECT	COUNT(DISTINCT cv.cv_id) AS cv_approved_count
							FROM	tbl_cash_voucher AS cv,
									tbl_cash_voucher_details AS cv_details
							WHERE 	cv.cv_id = cv_details.cv_details_reference_number
							AND 	cv.cv_obligated_by != 0
							AND 	cv.cv_obligated_by_date != '0000-00-00 00:00:00'							
							AND 	cv.cv_certified_by != 0
							AND 	cv.cv_certified_by_date != '0000-00-00 00:00:00'
							AND 	cv.cv_approved_by = 0
							AND 	cv.cv_approved_by_date = '0000-00-00 00:00:00'
							AND		cv.cv_cancelled_by = 0
							AND		cv.cv_cancelled_by_date = '0000-00-00 00:00:00'							
							AND 	cv_details.cv_details_debit IS NOT NULL
							AND 	cv_details.cv_details_credit IS NOT NULL";
					$rs8 = mysql_query($sql8) or die("Error in sql1 in module: main.php ".$sql8." ".mysql_error());
					$rows8 = mysql_fetch_array($rs8);
					$cv_approved_count = $rows8['cv_approved_count'];									
			  ?>              
			  <tr>
                <td align="right"><span style="font-weight: bold">Approve</span></td>
                <td align="left">
				<?php
					if ($_SESSION['level']==2 || $_SESSION['level']==1)
					{
						echo "<a href='/workspaceGOP/page.transactions/disbursement.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=dv_id&sort_by=approval'>Disbursement Voucher</a> ( ".$dv_approved_count." )";
						echo "&nbsp;&nbsp;";
						echo "<a href='/workspaceGOP/page.transactions/journal.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=jv_id&sort_by=approval'>Journal Voucher</a> ( ".$jv_approved_count." )";
						echo "&nbsp;&nbsp;";	
						echo "Cash Voucher ( ".$cv_approved_count." )";
						echo "&nbsp;&nbsp;";										
					}
					elseif ($_SESSION['level']==3)
					{
						echo "Disbursement Voucher ( ".$dv_approved_count." )";
						echo "&nbsp;&nbsp;";
						echo "Journal Voucher ( ".$jv_approved_count." )";
						echo "&nbsp;&nbsp;";
						echo "<a href='/workspaceGOP/page.transactions/cash.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=cv_id&sort_by=approval'>Cash Voucher</a> ( ".$cv_approved_count." )";	
						echo "&nbsp;&nbsp;";						
					}
					else
					{				
						echo "Disbursement Voucher ( ".$dv_approved_count." )";
						echo "&nbsp;&nbsp;";
						echo "Journal Voucher ( ".$jv_approved_count." )";
						echo "&nbsp;&nbsp;";	
						echo "Cash Voucher ( ".$cv_approved_count." )";
						echo "&nbsp;&nbsp;";	
					}													
				?>				
                </td>
              </tr>	
			</tbody>
          </table>
	  </td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>