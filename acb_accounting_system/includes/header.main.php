<?php
/*
if($_SESSION["logged"]==1)
{ 
	include("./../includes/check.login.php");
	#include("./../includes/check.admin.rights.php");
	include("./../includes/function.php");
	require("./../includes/FCKeditor/fckeditor.php") ;
	include("./../includes/timer.includes.php");
}
$sql = "SELECT	SQL_BUFFER_RESULT
				SQL_CACHE
				cfg_value 
		FROM	tbl_config
		WHERE 	cfg_name = 'main_title_header'";
$rs = mysql_query($sql) or die("Title Query Error " .mysql_error());
$rows = mysql_fetch_array($rs);
$main_title_header = $rows["cfg_value"];
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title><?php echo $main_title_header; ?></title>

<link rel="stylesheet" type="text/css" href="/workspaceGOP/css/main.css" />
<link rel="stylesheet" type="text/css" href="/workspaceGOP/css/menu.base.css" />
<link rel="stylesheet" type="text/css" href="/workspaceGOP/css/menu.top.bar.css" />
<script type="text/javascript" src="/workspaceGOP/jscript/menu.js"></script>
<script type="text/javascript" src="/workspaceGOP/jscript/main.js"></script>

</head>
<body>
<!-- Start of header -->
<table id="rounded-corner">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div align="left" class="whitelabelheader"><?php echo $main_title_header; ?></div></th>
			<th scope="col" class="rounded-q4">		
				<div align="right" class="whitelabelheader">
				<?php
					if($_SESSION["logged"]==1)
					{ 
					
					?>
					
						<a href="/workspaceGOP/page.configurations/user.edit.php?id=<?php echo encrypt($_SESSION["id"]); ?>" class="whitelabel">Edit Profile</a>
						&nbsp;|&nbsp;
						<a href="/workspaceGOP/index.php?msg=2&id=<?php echo $_SESSION["id"]; ?>" class="whitelabel">Sign Out</a>
				<?php 
					} 
					?>
				</div>
			</th>
		</tr>
	</thead>
</table>
<br>
<!-- End of header -->
<!-- Start of welcome -->
<?php
if($_SESSION["logged"]==1)
{ 
?>
	<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td class="blacklabelheader">Welcome <?php echo $_SESSION["userfullname"]; ?>,</td>
	</tr>
	<tr>
		<td class="main"><b>Server Date/Time:&nbsp;
		<script type="text/javascript">
					
			var currenttime = '<? print date("F d, Y H:i:s", time())?>' //PHP method of getting server date
			
			var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
			var serverdate=new Date(currenttime)
			
			function padlength(what)
			{
				var output=(what.toString().length==1)? "0"+what : what
				return output
			}
			
			function displaytime()
			{
				serverdate.setSeconds(serverdate.getSeconds()+1)
				var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear()
				var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())
				document.getElementById("servertime").innerHTML=datestring+" "+timestring
			}
			
			window.onload=function()
			{
				setInterval("displaytime()", 1000)
			}					
			
			
			</script>
			<span id="servertime"></span> 
		</b>
		</td>
	</tr>
	</table>
<?php 
} 
?>	
<!-- End of welcome -->

