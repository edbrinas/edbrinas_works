<?php include('./../includes/check.login.php'); ?>
<!-- Start of table for Menu -->
<table width='90%' border='0' cellspacing='2' cellpadding='2' align='center'>
  <tr>
    <td>

		<div id='ddtopmenubar' class='menu'>
			<ul>
				<li><a href='/workspaceGOP/page.main/main.php'>Home</a></li>
				<?php
				if ($_SESSION['level'] == 1)
				{
					?>
					<li><a href='/workspaceGOP/page.main/main.php' rel='ddsubmenu1'>System Administration</a></li>
					<?php
				}
				?>
				<li><a href='/workspaceGOP/page.main/main.php' rel='ddsubmenu2'>Reports Component</a></li>
				<li><a href='/workspaceGOP/page.main/main.php' rel='ddsubmenu3'>Transactions</a></li>
				<li><a href='/workspaceGOP/page.main/main.php' rel='ddsubmenu4'>Generate Reports</a></li>
			</ul>
		</div>
		
		<script type='text/javascript'>ddlevelsmenu.setup('ddtopmenubar', 'topbar')</script>
			<!-- Start of Menu 1 -->
			<ul id='ddsubmenu1' class='ddsubmenustyle'>
				<li><a href='/workspaceGOP/page.main/main.php'>System Users</a>
					<ul>
						<li><a href='/workspaceGOP/page.configurations/user.add.php'>Add New User</a></li>
						<li><a href='/workspaceGOP/page.configurations/user.list.php'>User List</a></li>
					</ul>
				</li>
				<li><a href='/workspaceGOP/page.main/main.php'>System Configuration</a>
					<ul>
						<li><a href='/workspaceGOP/page.configurations/settings.add.php'>Add New Configuration</a></li>
						<li><a href='/workspaceGOP/page.configurations/settings.list.php'>Configuration List</a></li>
					</ul>
				</li>
				<li><a href='/workspaceGOP/page.main/main.php'>Close Accounting Year</a>
					<ul>
						<li><a href='/workspaceGOP/page.configurations/close.accounting.year.list.php'>Accounting Year</a></li>
					</ul>
				</li>	
				<li><a href='/workspaceGOP/page.main/main.php'>Audit Trail</a>
					<ul>
						<li><a href='/workspaceGOP/page.configurations/event.logs.list.php'>View Audit Trail</a></li>
					</ul>
				</li>				
																				
			</ul>
			<!-- End of Menu 1 -->
			<!-- Start of Menu 2 -->
			<ul id='ddsubmenu2' class='ddsubmenustyle'>
				<?php
                if ($_SESSION['level']==1 || $_SESSION['level']==5)
                {
                ?>	            
				<li><a href='/workspaceGOP/page.main/main.php'>Report</a>
					<ul>
						<li><a href='/workspaceGOP/page.report.components/reports.index.php'>Manage Report</a></li>
					</ul>
				</li>  
				<?php
                }
                ?>	                            			
				<li><a href='/workspaceGOP/page.main/main.php'>Fund Analysis Budget Configuration</a>
					<ul>
						<li><a href='/workspaceGOP/page.main/main.php'>Per AWP, Per Budget Line</a>
							<ul>
								<?php
								if ($_SESSION['level']==1 || $_SESSION['level']==5)
								{
								?>							
									<li><a href='/workspaceGOP/page.report.components/fa.by.awp.by.budget.line.add.php'>Add Budget Item</a></li>
								<?php
								}
								?>									
								<li><a href='/workspaceGOP/page.report.components/fa.by.awp.by.budget.line.list.php'>Budget Item List</a></li>						
							</ul>
						</li>
						<li><a href='/workspaceGOP/page.main/main.php'>Per Component ID, Per Activity ID</a>
							<ul>
								<?php
								if ($_SESSION['level']==1 || $_SESSION['level']==5)
								{
								?>							
									<li><a href='/workspaceGOP/page.report.components/fa.by.component.id.by.activity.id.add.php'>Add Budget Item</a></li>
								<?php
								}
								?>									
								<li><a href='/workspaceGOP/page.report.components/fa.by.component.id.by.activity.id.list.php'>Budget Item List</a></li>						
							</ul>
						</li>	
						<li><a href='/workspaceGOP/page.main/main.php'>Per Component ID, Per Budget Line</a>
							<ul>
								<?php
								if ($_SESSION['level']==1 || $_SESSION['level']==5)
								{
								?>							
									<li><a href='/workspaceGOP/page.report.components/fa.by.component.id.by.budget.id.add.php'>Add Budget Item</a></li>
								<?php
								}
								?>									
								<li><a href='/workspaceGOP/page.report.components/fa.by.component.id.by.budget.id.list.php'>Budget Item List</a></li>						
							</ul>
						</li>	
						<li><a href='/workspaceGOP/page.main/main.php'>Per Financing Agreement, Per Budget Line Item</a>
							<ul>
								<?php
								if ($_SESSION['level']==1 || $_SESSION['level']==5)
								{
								?>							
									<li><a href='/workspaceGOP/page.report.components/fa.by.financing.agreement.add.php'>Add Budget Item</a></li>
								<?php
								}
								?>									
								<li><a href='/workspaceGOP/page.report.components/fa.by.financing.agreement.list.php'>Budget Item List</a></li>						
							</ul>
						</li>
					</ul>
				</li>					
				<li><a href='/workspaceGOP/page.main/main.php'>Subsidiary Ledger Configuration</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>
							<li><a href='/workspaceGOP/page.report.components/subsidiary.ledger.add.php'>Add New Subsidiary Ledger</a></li>
						<?php
						}
						?>
						<li><a href='/workspaceGOP/page.report.components/subsidiary.ledger.list.php'>Subsidiary Ledger List</a></li>
					</ul>
				</li>			
				<li><a href='/workspaceGOP/page.main/main.php'>Bank Configuration</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>					
							<li><a href='/workspaceGOP/page.report.components/bank.add.php'>Add New Bank</a></li>
						<?php
						}
						?>						
						<li><a href='/workspaceGOP/page.report.components/bank.list.php'>Bank List</a></li>
					</ul>
				</li>	
				<li><a href='/workspaceGOP/page.main/main.php'>Exchange Rate Configuration</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>					
							<li><a href='/workspaceGOP/page.report.components/forex.add.php'>Add New Exchange Rate</a></li>
						<?php
						}
						?>						
						<li><a href='/workspaceGOP/page.report.components/forex.list.php'>Exchange Rate List</a></li>
					</ul>
				</li>
				<li><a href='/workspaceGOP/page.main/main.php'>Payee</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>					
							<li><a href='/workspaceGOP/page.report.components/payee.add.php'>Add New Payee</a></li>
						<?php
						}
						?>						
						<li><a href='/workspaceGOP/page.report.components/payee.list.php'>Payee List</a></li>
					</ul>
				</li>				
				<li><a href='/workspaceGOP/page.main/main.php'>Donor</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>					
							<li><a href='/workspaceGOP/page.report.components/donor.add.php'>Add New Donor</a></li>
						<?php
						}
						?>						
						<li><a href='/workspaceGOP/page.report.components/donor.list.php'>Donor List</a></li>
					</ul>
				</li>
				<li><a href='/workspaceGOP/page.main/main.php'>Component</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>						
							<li><a href='/workspaceGOP/page.report.components/component.add.php'>Add New Component</a></li>
						<?php
						}
						?>							
						<li><a href='/workspaceGOP/page.report.components/component.list.php'>Component List</a></li>
					</ul>
				</li>
				<li><a href='/workspaceGOP/page.main/main.php'>Other Cost/Services</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>
							<li><a href='/workspaceGOP/page.report.components/other.cost.add.php'>Add New Other Cost/Services</a></li>
						<?php
						}
						?>							
						<li><a href='/workspaceGOP/page.report.components/other.cost.list.php'>Other Cost/Services List</a></li>
					</ul>
				</li>	
				<li><a href='/workspaceGOP/page.main/main.php'>Budget</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>					
							<li><a href='/workspaceGOP/page.report.components/budget.add.php'>Add New Buget</a></li>
						<?php
						}
						?>							
						<li><a href='/workspaceGOP/page.report.components/budget.list.php'>Budget List</a></li>
					</ul>
				</li>
				<li><a href='/workspaceGOP/page.main/main.php'>Activity</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>	
							<li><a href='/workspaceGOP/page.report.components/activity.add.php'>Add New Activity</a></li>
						<?php
						}
						?>							
						<li><a href='/workspaceGOP/page.report.components/activity.list.php'>Activity List</a></li>
					</ul>
				</li>	
				<li><a href='/workspaceGOP/page.main/main.php'>Staff</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>						
							<li><a href='/workspaceGOP/page.report.components/staff.add.php'>Add New Staff</a></li>
						<?php
						}
						?>								
						<li><a href='/workspaceGOP/page.report.components/staff.list.php'>Staff List</a></li>
					</ul>
				</li>	
				<li><a href='/workspaceGOP/page.main/main.php'>Benefits</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>						
							<li><a href='/workspaceGOP/page.report.components/benefits.add.php'>Add New Benefits</a></li>
						<?php
						}
						?>						
						<li><a href='/workspaceGOP/page.report.components/benefits.list.php'>Benefits List</a></li>
					</ul>
				</li>	
				<li><a href='/workspaceGOP/page.main/main.php'>Vehicle</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>					
							<li><a href='/workspaceGOP/page.report.components/vehicle.add.php'>Add New Vehicles</a></li>
						<?php
						}
						?>								
						<li><a href='/workspaceGOP/page.report.components/vehicle.list.php'>Vehicles List</a></li>
					</ul>
				</li>	
				<li><a href='/workspaceGOP/page.main/main.php'>Equipment</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>						
							<li><a href='/workspaceGOP/page.report.components/equipment.add.php'>Add New Equipment</a></li>
						<?php
						}
						?>								
						<li><a href='/workspaceGOP/page.report.components/equipment.list.php'>Equipment List</a></li>
					</ul>
				</li>
				<li><a href='/workspaceGOP/page.main/main.php'>Item Code</a>
					<ul>
						<?php
						if ($_SESSION['level']==1 || $_SESSION['level']==5)
						{
						?>						
							<li><a href='/workspaceGOP/page.report.components/item.add.php'>Add New Item Code</a></li>
						<?php
						}
						?>								
						<li><a href='/workspaceGOP/page.report.components/item.list.php'>Item Code List</a></li>
					</ul>
				</li>                			
			</ul>
			<!-- End of Menu 2 -->
			<!-- Start of Menu 3 -->
			<ul id='ddsubmenu3' class='ddsubmenustyle'>
				<li><a href='/workspaceGOP/page.main/main.php'>Disbursement Voucher</a>
					<ul>
						<?php
						if ($_SESSION['level'] == 1 || $_SESSION['level']== 5 || $_SESSION['level']== 6)
						{
							?>
							<li><a href='/workspaceGOP/page.transactions/disbursement.voucher.add.php'>Add New DV</a></li>
							<li><a href='/workspaceGOP/page.transactions/disbursement.voucher.list.php'>DV List</a></li>
							<?php
						}
						?>
						<?php
						if ($_SESSION['level']== 2)
						{
							?>
							<li><a href='/workspaceGOP/page.transactions/disbursement.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=dv_id&sort_by=approval'>DV List</a></li>
							<?php
						}
						elseif ($_SESSION['level']== 3)
						{
							?>
							<li><a href='/workspaceGOP/page.transactions/disbursement.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=dv_id&sort_by=recommended'>DV List</a></li>
							<?php
						}
						elseif ($_SESSION['level']== 4)
						{
							?>
							<li><a href='/workspaceGOP/page.transactions/disbursement.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=dv_id&sort_by=certify'>DV List</a></li>
							<?php
						}
						?>							
					</ul>
				</li>
				<li><a href='/workspaceGOP/page.main/main.php'>Journal Voucher</a>
					<ul>
						<?php
						if ($_SESSION['level'] == 1 || $_SESSION['level']== 5 || $_SESSION['level']== 6)
						{
							?>					
							<li><a href='/workspaceGOP/page.transactions/journal.voucher.add.php'>Add New JV</a></li>
							<li><a href='/workspaceGOP/page.transactions/journal.voucher.list.php'>JV List</a></li>
							<?php
						}
						?>
						<?php
						if ($_SESSION['level']== 2)
						{
							?>
							<li><a href='/workspaceGOP/page.transactions/journal.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=jv_id&sort_by=approval'>JV List</a></li>
							<?php
						}
						elseif ($_SESSION['level']== 3)
						{
							?>
							<li><a href='/workspaceGOP/page.transactions/journal.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=jv_id&sort_by=recommended'>JV List</a></li>
							<?php
						}
						elseif ($_SESSION['level']== 4)
						{
							?>
							<li><a href='/workspaceGOP/page.transactions/journal.voucher.list.php?search_for=&search_by=&action=&cur_page=&order=jv_id&sort_by=certify'>JV List</a></li>
							<?php
						}
						?>						
					</ul>
				</li>	
				<li><a href='/workspaceGOP/page.main/main.php'>Cash Voucher</a>
					<ul>

						<?php
						if ($_SESSION['level'] == 1 || $_SESSION['level'] == 5 || $_SESSION['level']== 6)
						{
							?>					
							<li><a href='/workspaceGOP/page.transactions/cash.voucher.add.php'>Add New CV</a></li>
							<?php
						}
						?>	
						
						<li><a href='/workspaceGOP/page.transactions/cash.voucher.list.php'>CV List</a></li>
					</ul>
				</li>
				<li><a href='/workspaceGOP/page.main/main.php'>Manage Cheque</a>
					<ul>
                    	<li><a href='/workspaceGOP/page.transactions/cheque.add.manual.php'>Add Cheque</a></li>
						<li><a href='/workspaceGOP/page.transactions/cheque.list.php'>Cheque List</a></li>
					</ul>
				</li> 
				<li><a href='/workspaceGOP/page.main/main.php'>Manage Telegraphic Transfer</a>
					<ul>
						<?php
						if ($_SESSION['level'] == 1 || $_SESSION['level'] == 5 || $_SESSION['level']== 6)
						{
							?>					
							<li><a href='/workspaceGOP/page.transactions/telegraphic.transfer.add.php'>Add New TT</a></li>
							<?php
						}
						?>	
						<li><a href='/workspaceGOP/page.transactions/telegraphic.transfer.list.php'>TT List</a></li>
					</ul>
				</li>
				<li><a href='/workspaceGOP/page.main/main.php'>Manage Bank Reconciliation Details</a>
					<ul>
						<?php
						if ($_SESSION['level'] == 1 || $_SESSION['level'] == 5 || $_SESSION['level']== 6)
						{
							?>					
							<li><a href='/workspaceGOP/page.transactions/bank.reconciliation.add.php'>Add New Bank Reconciliation</a></li>
							<?php
						}
						?>	
						<li><a href='/workspaceGOP/page.transactions/bank.reconciliation.list.php'>Bank Reconciliation List</a></li>
					</ul>
				</li>                
			</ul>
			<!-- End of Menu 3 -->
			<!-- Start of Menu 4 -->
			<ul id='ddsubmenu4' class='ddsubmenustyle'>
            	<li><a href='/workspaceGOP/page.report/voucher.list.php'>Voucher List</a></li>
				<li><a href='/workspaceGOP/page.report/general.ledger.list.php'>General Ledger</a></li>
				<li><a href='/workspaceGOP/page.report/balance.sheet.list.php'>Balance Sheet</a></li>
				<li><a href='/workspaceGOP/page.report/account.inquiry.list.php'>Account Inquiry</a></li>
				<li><a href='/workspaceGOP/page.report/bank.reconciliation.list.php'>Bank Reconciliation</a></li>
				<li><a href='/workspaceGOP/page.report/trial.balance.list.php'>Trial Balance</a></li>
				<li><a href='/workspaceGOP/page.report/statement.of.receipts.and.disbursements.list.php'>Statement of Receipts and Disbursements</a></li>
				<li><a href='/workspaceGOP/page.report/telegraphic.transfer.list.php'>Telegraphic Transfer Logs</a></li>
				<li><a href='/workspaceGOP/page.report/aging.advances.list.php'>Aging Advances</a></li>
				<li><a href='/workspaceGOP/page.report/cash.flows.list.php'>Cash Flows</a></li>
				<li><a href='/workspaceGOP/page.main/main.php'>Cheque Summary</a>
					<ul>
						<li><a href='/workspaceGOP/page.report/cheque.issued.list.php'>Cheque Issued</a></li>
						<li><a href='/workspaceGOP/page.report/cheque.cancelled.list.php'>Cheque Cancelled</a></li>						
					</ul>
				</li>					
				<li><a href='/workspaceGOP/page.main/main.php'>Fund Analysis</a>
					<ul>
						<li><a href='/workspaceGOP/page.report/fa.by.awp.by.budget.line.list.php'>Per AWP, Per Budget Line</a></li>
						<li><a href='/workspaceGOP/page.report/fa.by.component.id.by.activity.list.php'>Per Component ID, Per Activity ID</a></li>
						<li><a href='/workspaceGOP/page.report/fa.by.component.id.budget.line.item.list.php'>Per Component ID, Per Budget Line</a></li>
						<li><a href='/workspaceGOP/page.report/fa.by.financial.agreement.by.budget.line.list.php'>Per Financing Agreement, Per Budget Line Item</a></li>
					</ul>
				</li>	
			</ul>
			<!-- End of Menu 4 -->
	</td>
  </tr>
</table>
<br>
<!-- End of table for Menu -->