<?php
#DO NOT EDIT THE CONFIGURATION BELOW!
#Edward Brinas

function covertExchangeRate($from_currency,$to_currency,$rate_date,$original_amount)
{
	/*
	PESO to DOLLAR
	PESO to EUROS
	DOLLAR to PESO
	DOLLAR to EUROS
	EUROS to PESO
	EUROS to DOLLAR
	PESO 16
	EURO 17
	USD 18
	Peso to Euro   Formula A 34
	Peso to Dollar Formula B 35
	Dollar to Euro Formula C 36
	
	if original amount = php
	   convert to euro (oa/A)
	   convert to dollar (oa*B)
	
	if original amount = usd
	   convert to euro (oa/B)
	   convert to dollar (oa*B)
	
	if original amount = EURO
	   convert to dollar (oa*c)
	   convert to peso (oa*a)
	*/
	$r_date = explode('-',$rate_date);
	$year = $r_date[0];
	$month = $r_date[1];
	$day = $r_date[2];
	
	$rate_date = $month."-".$year;
	
	if ($from_currency==16 && $to_currency==17) { $forex_currency_code = 34; }
	if ($from_currency==16 && $to_currency==18) { $forex_currency_code = 35; }	

	if ($from_currency==18 && $to_currency==17) { $forex_currency_code = 36; }
	if ($from_currency==18 && $to_currency==16) { $forex_currency_code = 35; }
	
	if ($from_currency==17 && $to_currency==18) { $forex_currency_code = 36; }
	if ($from_currency==17 && $to_currency==16) { $forex_currency_code = 34; }
	
	if ($from_currency==57 && $to_currency==16) { $forex_currency_code = 58; }
	if ($from_currency==57 && $to_currency==18) { $forex_currency_code = 59; }
	
	if ($from_currency==16 && $to_currency==57) { $forex_currency_code = 58; }
	if ($from_currency==18 && $to_currency==57) { $forex_currency_code = 59; }
	
	
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					forex_currency_code,	
					forex_currency_value
			FROM	tbl_forex
			WHERE 	forex_transaction_date = '$rate_date'
			AND		forex_currency_code = '$forex_currency_code'";
	$rs = mysql_query($sql) or die("Query Error" .mysql_error());
	$total_rows = mysql_num_rows($rs);
	
	if ($total_rows!=0)
	{
		$rows=mysql_fetch_array($rs);
		$forex_currency_code = $rows["forex_currency_code"];
		$forex_currency_value =	$rows["forex_currency_value"];
	}
	else
	{
		$forex_currency_code = $to_currency;
		$forex_currency_value = 1;
	}
	/*
	PESO 16
	EURO 17
	USD 18
	*/
	/*
	PESO to EURO
	USD to EURO
	PESO to USD
	PESO to GBP
	USD to GBP
	$rate_output = $original_amount/$forex_currency_value;
	*/
	if ($from_currency==16 && $to_currency==17 || $from_currency==18 && $to_currency==17
	//modified by ansel
		|| $from_currency==16 && $to_currency==18 || $from_currency==16 && $to_currency==57 || $from_currency==18 && $to_currency==57
	//end of modified line
	)
	{ 
		$rate_output = $original_amount/$forex_currency_value; 
	}

	/*
	PESO 16
	EURO 17
	USD 18
	GBP 57
		
	USD to PESO
	EURO to USD
	EURO to PESO
	GBP to PESO
	GBP to USD

	$rate_output = $original_amount*$forex_currency_value;
	*/
	
	if (//$from_currency==16 && $to_currency==18 || 
		$from_currency==18 && $to_currency==16 || 
		$from_currency==17 && $to_currency==18 || 
		$from_currency==17 && $to_currency==16
												
												|| 
		$from_currency==57 && $to_currency==16	|| 
		$from_currency==57 && $to_currency==18
		) 
	{ 
		if($from_currency==57 && $to_currency==18)
		{
		
		}
		$rate_output = $original_amount*$forex_currency_value; 
	}	
	
	/*
	if ($from_currency==16 && $to_currency==17) { $rate_output = $original_amount/$forex_currency_value; }
	if ($from_currency==16 && $to_currency==18) { $rate_output = $original_amount*$forex_currency_value; }	

	if ($from_currency==18 && $to_currency==17) { $rate_output = $original_amount/$forex_currency_value; }
	if ($from_currency==18 && $to_currency==16) { $rate_output = $original_amount*$forex_currency_value; }
	
	if ($from_currency==17 && $to_currency==18) { $rate_output = $original_amount*$forex_currency_value; }
	if ($from_currency==17 && $to_currency==16) { $rate_output = $original_amount*$forex_currency_value; }	
	*/
	
	return $rate_output;
}
function enableDigitalSignature()
{
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value 
			FROM	tbl_config
			WHERE 	cfg_name = 'enable_digital_signature'";
	$rs = mysql_query($sql) or die("Error in sql in ".$sql." ".mysql_error());
	$rows=mysql_fetch_array($rs);
	
	return $rows['cfg_value'];
}
function getFAByAWPByBudget($account_code,$budget_year)
{
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					awp_budget_amount 
			FROM	tbl_fund_analysis_by_awp_by_budget_line
			WHERE 	awp_account_code = '$account_code'
			AND		awp_budget_year = '$budget_year'";
	$rs = mysql_query($sql) or die("Error in sql in ".$sql." ".mysql_error());
	$rows=mysql_fetch_array($rs);
	
	return $rows['awp_budget_amount'];
}
function checkIfActivityIndex($activity_id)
{
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					COUNT(activity_id) AS activity_count
			FROM	tbl_activity
			WHERE 	activity_id = '$activity_id'
			AND		activity_index = '1'";
	$rs = mysql_query($sql) or die("Error in sql in ".$sql." ".mysql_error());
	$rows=mysql_fetch_array($rs);
	
	return $rows['activity_count'];
}
function getFAByFinancingAgreement($account_code,$budget_year)
{
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					fa_budget_amount 
			FROM	tbl_fund_analysis_by_financing_agreement
			WHERE 	fa_account_code = '$account_code'
			AND		fa_budget_year = '$budget_year'";
	$rs = mysql_query($sql) or die("Error in sql in ".$sql." ".mysql_error());
	$rows=mysql_fetch_array($rs);
	
	return $rows['fa_budget_amount'];
}
function getFAByComponentByBudget($account_code,$component_id,$budget_year)
{
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					fa_budget 
			FROM	tbl_fund_analysis_by_component_by_budget
			WHERE 	fa_account_code = '$account_code'
			AND		fa_component_id = '$component_id'
			AND		fa_budget_year = '$budget_year'";
	$rs = mysql_query($sql) or die("Error in sql in ".$sql." ".mysql_error());
	$rows=mysql_fetch_array($rs);
	
	return $rows['fa_budget'];
}
function getGLSumCredit($sl_id,$start_date,$end_date,$report_currency_code)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					gl_date,
					gl_currency_code,
					gl_credit
			FROM	tbl_general_ledger
			WHERE  	gl_account_code = '$sl_id'
			AND		gl_date
			BETWEEN	'$start_date'
			AND		'$end_date' ";
	if ($consolidate_reports!=1)
	{
		$sql .= "AND	gl_currency_code = '$rep_currency_code' ";
	}

	$rs = mysql_query($sql) or die("Error in sql in ".$sql." ".mysql_error());	
	while($rows=mysql_fetch_array($rs))
	{
		$gl_credit = $rows["gl_credit"];
		$gl_currency_code = $rows["gl_currency_code"];
		$gl_date = $rows["gl_date"];
		
		if ($gl_currency_code != $rep_currency_code && $consolidate_reports==1)
		{
			$gl_credit = covertExchangeRate($gl_currency_code,$rep_currency_code,$gl_date,$gl_credit);
		}
		$arr_gl_credit[] = $gl_credit;
	}	
	if (count($arr_gl_credit)!=0)
	{
		$arr_sum_credit = array_sum($arr_gl_credit);
	}
	
	return $arr_sum_credit;	
}
function getGLSumDebitSumCredit($sl_id,$start_date,$end_date,$report_currency_code,$donor_id,$budget_id,$consolidate_reports)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					gl_date,
					gl_currency_code,
					gl_credit,
					gl_debit
			FROM	tbl_general_ledger
			WHERE  	gl_account_code = '$sl_id'
			AND		gl_date
			BETWEEN	'$start_date'
			AND		'$end_date' ";
	if ($consolidate_reports!=1)
	{
		$sql .= "AND	gl_currency_code = '$report_currency_code' ";
	}
	if ($budget_id!="")
	{
		$sql .= "AND	gl_budget_id = '$budget_id' ";
	}
	if ($donor_id!="")
	{
		$sql .= "AND	gl_donor_id = '$donor_id' ";
	}

	$rs = mysql_query($sql) or die("Error in sql in function getGLSumDebitSumCredit ".$sql." ".mysql_error());	
	while($rows=mysql_fetch_array($rs))
	{
		$gl_debit = $rows["gl_debit"];
		$gl_credit = $rows["gl_credit"];
		$gl_currency_code = $rows["gl_currency_code"];
		$gl_date = $rows["gl_date"];
		
		if ($gl_currency_code != $report_currency_code && $consolidate_reports==1)
		{
			#covertExchangeRate($from_currency,$to_currency,$rate_date,$rate_value)
			$gl_credit = covertExchangeRate($gl_currency_code,$report_currency_code,$gl_date,$gl_credit);
			$gl_debit = covertExchangeRate($gl_currency_code,$report_currency_code,$gl_date,$gl_debit);
		}
		$arr_gl_credit[] = $gl_credit;
		$arr_gl_debit[] = $gl_debit;
	}	
	if (count($arr_gl_credit)!=0 || count($arr_sum_debit)!=0)
	{
		$arr_sum_credit = array_sum($arr_gl_credit);
		$arr_sum_debit = array_sum($arr_gl_debit);
	}	

	$sum_dr_cr = $arr_sum_debit-$arr_sum_credit;

	return $sum_dr_cr;
}
function getVoucherId($currency)
{
	/*	
	PESO 16
	EURO 17
	USD 18
	*/
	switch($currency)
	{
		case "16":
			return "P";
		break;
		
		case "17":
			return "E";
		break;
		
		case "18":
			return "U";
		break;
		
//death
		case "57":
			return "L";
		break;				
	}
}
function getClosedAccountingYear()
{
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_name,
					cfg_value
			FROM 	tbl_config
			WHERE	cfg_name = 'closed_accounting_year'
			ORDER BY cfg_id DESC LIMIT 1";
	$rs = mysql_query($sql) or die("Error in function: getClosedAccountingYear: ".$sql.mysql_error());
	$rows = mysql_fetch_array($rs);
	return $rows['cfg_value'];
}
function getCashVoucherSequenceNumber($budget_id,$report_month,$report_year)
{
	#1 EU
	#2 AM
	$report_month = date("d", mktime(0, 0, 0, 0, $report_month, 0));
	$closed_accounting_year = getClosedAccountingYear();
	$currency_name = "PC";
	$report_name = "CV";
	
	if ($report_month=="") { $report_month = date("m"); }
	#if ($budget_id==1) { $donor_name = "E"; }
	#elseif ($budget_id==2) { $donor_name = "A"; }
	
	switch ($budget_id)
	{
		case "1":
			$donor_name = "E";
		break;
		
		case "2":
			$donor_name = "A";
		break;		
		
		default:
			exit();
		break;
	}
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					cv_year,
					cv_sequence_number,
					cv_budget_id
			FROM 	tbl_cash_voucher
			WHERE	cv_budget_id = '".trim($budget_id)."'
			AND 	cv_year = '".$report_year."'
			ORDER BY cv_number DESC LIMIT 1";
	
	$rs = mysql_query($sql) or die("Error in function: getCashVoucherSequenceNumber: ".$sql.mysql_error());
	$rows = mysql_fetch_array($rs);
	
	$record_year = $rows['cv_year'];
	$record_sequence_number = $rows['cv_sequence_number'];		
	$record_budget_id = $rows['cv_budget_id'];
	
	if ($closed_accounting_year != $record_year)
	{
		if ($record_sequence_number=="" || $record_sequence_number==0)
		{
			$record_sequence_number = 1;
		}
		else
		{
			$record_sequence_number = $record_sequence_number + 1;
		}
		$voucher_year = substr($report_year, -2);
	}
	else
	{
		$record_sequence_number = $record_sequence_number + 1;
		$voucher_year = date("y");		
	}
	
	$record_sequence_number = str_pad($record_sequence_number,"4","0",STR_PAD_LEFT);
	
	$cv_number = $donor_name.$currency_name.$voucher_year.$report_name.$report_month.$record_sequence_number;
			
	return $cv_number;	
	
}
function getVoucherSequenceNumber($mode,$currency,$report_month,$report_year)
{
	$closed_accounting_year = getClosedAccountingYear();
	$report_month = date("d", mktime(0, 0, 0, 0, $report_month, 0));
	$currency_name = getVoucherId($currency);
	if ($report_month=="") { $report_month = date("m"); }

	switch($mode)
	{
		case "DV":	
			$table_order_by = "dv_number";
			$table_row_1 = "dv_year";
			$table_row_2 = "dv_sequence_number";
			$table_row_3 = "dv_currency_code";						
			$table_name = "tbl_disbursement_voucher";
			$report_name = "DV";	
		break;
		
		case "JV":
		$table_order_by = "jv_number";
		$table_row_1 = "jv_year";
		$table_row_2 = "jv_sequence_number";
		$table_row_3 = "jv_currency_code";							
		$table_name = "tbl_journal_voucher";
		$report_name = "JV";			
		break;
	}
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					".$table_row_1." AS record_year,
					".$table_row_2." AS record_sequence_number
			FROM 	".$table_name."
			WHERE	".$table_row_3." = '".$currency."'
			AND 	".$table_row_1." = '".$report_year."'
			ORDER BY ".$table_order_by." DESC LIMIT 1";
	$rs = mysql_query($sql) or die("Error in function: getVoucherSequenceNumber: ".$sql.mysql_error());
	$rows = mysql_fetch_array($rs);
	
	$record_year = $rows['record_year'];
	$record_sequence_number = $rows['record_sequence_number'];		

	if ($closed_accounting_year != $record_year)
	{
		if ($record_sequence_number=="" || $record_sequence_number==0)
		{
			$record_sequence_number = 1;
		}
		else
		{
			$record_sequence_number = $record_sequence_number + 1;
		}
		$voucher_year = substr($report_year, -2);
	}
	else
	{
		$record_sequence_number = $record_sequence_number + 1;
		$voucher_year = date("y");		
	}
	
	$record_sequence_number = str_pad($record_sequence_number,"4","0",STR_PAD_LEFT);
	$voucher_number = $currency_name.$voucher_year.$report_name.$report_month.$record_sequence_number;
		
	return $voucher_number;
	
}

function getTelegraphicTransferSequenceNumber()
{
	$current_year = date("Y");
	
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					tt_year,
					tt_sequence_number
			FROM 	tbl_telegraphic_transfer
			ORDER BY tt_sequence_number DESC";	
	$rs = mysql_query($sql) or die("Error in sql in function getTelegraphicTransferSequenceNumber ".$sql." ".mysql_error());			
	$rows = mysql_fetch_array($rs);
	
	$record_year = $rows['tt_year'];
	$record_sequence_number = $rows['tt_sequence_number'];	
		
	if ($current_year == $record_year)
	{
		$record_sequence_number = $record_sequence_number + 1;
	}
	else
	{
		$record_sequence_number = 1;
	}
	
	$record_sequence_number = str_pad($record_sequence_number,"4","0",STR_PAD_LEFT);
	
	$tt_number = "TT ".$current_year."-".$record_sequence_number;
		
	return $tt_number;
}
function getSlIdByAccountTitle($sl_account_title)
{
	/*
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					sl_id
			FROM 	tbl_subsidiary_ledger
			WHERE 	sl_account_title
			LIKE 	'%".trim($sl_account_title)."%'";
			echo $sql."<br>";
	*/		
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					sl_id
			FROM 	tbl_subsidiary_ledger
			WHERE 	sl_account_title = '".trim($sl_account_title)."'";

	$rs = mysql_query($sql) or die("Error in sql in function getBeginningBalanceByAccountTitle ".$sql." ".mysql_error());
	$rows=mysql_fetch_array($rs);
	return $rows["sl_id"];
}
function getSlIdByAccountCode($sl_account_code)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					sl_id
			FROM 	tbl_subsidiary_ledger
			WHERE 	sl_account_code = '".trim($sl_account_code)."'";
	$rs = mysql_query($sql) or die("Error in sql in function getBeginningBalanceByAccountTitle ".$sql." ".mysql_error());
	$rows=mysql_fetch_array($rs);
	return $rows["sl_id"];
}
function getEndingBalanceById($sl_id,$report_year,$end_date,$report_currency_code,$donor_id,$budget_id,$staff_id,$other_cost_id,$benefits_id,$vehicle_id,$equipment_id,$consolidate_reports)
{
	$current_year = date("Y");
	$start_date = $report_year."-01-01";

	if ($current_year!=$report_year)
	{
		$sql = "SELECT	SQL_BUFFER_RESULT
						SQL_CACHE
						sl_details_ending_balance AS report_balance
				FROM 	tbl_subsidiary_ledger_details
				WHERE  	sl_details_reference_number = '$sl_id'
				AND		sl_details_year = '$report_year'
				AND		sl_details_currency_code = '$report_currency_code'";
		$rs = mysql_query($sql) or die("Error in sql in function: getEndingBalanceById ".$sql." ".mysql_error());
		$rows = mysql_fetch_array($rs);
		$report_balance = $rows["report_balance"];				
	}
	else
	{
		$sql = "SELECT	SQL_BUFFER_RESULT
						SQL_CACHE
						gl_debit,
						gl_credit,
						gl_currency_code,
						gl_date						
				FROM 	tbl_general_ledger
				WHERE  	gl_date
				BETWEEN	'$start_date'
				AND		'$end_date' 
				AND		gl_account_code = '$sl_id' ";	
		if ($consolidate_reports!=1)
		{
			$sql .= "AND	gl_currency_code = '$report_currency_code' ";
		}
		if ($budget_id!="")
		{
			$sql .= "AND	gl_budget_id = '$budget_id' ";
		}
		if ($donor_id!="")
		{
			$sql .= "AND	gl_donor_id = '$donor_id' ";
		}		
		if ($staff_id!="")
		{
			$sql .= "AND	gl_staff_id = '$staff_id' ";
		}
		if ($other_cost_id!="")
		{
			$sql .= "AND	gl_other_cost_id  = '$other_cost_id' ";
		}	
		if ($benefits_id!="")
		{
			$sql .= "AND	gl_benefits_id  = '$benefits_id' ";
		}	
		if ($vehicle_id!="")
		{
			$sql .= "AND	gl_vehicle_id  = '$vehicle_id' ";
		}
		if ($equipment_id!="")
		{
			$sql .= "AND	gl_equipment_id  = '$equipment_id' ";
		}	

		$rs = mysql_query($sql) or die("Error in sql in function getEndingBalanceById ".$sql." ".mysql_error());	
		while($rows=mysql_fetch_array($rs))
		{
			$gl_debit = $rows["gl_debit"];
			$gl_credit = $rows["gl_credit"];
			$gl_currency_code = $rows["gl_currency_code"];
			$gl_date = $rows["gl_date"];

			if ($gl_currency_code != $report_currency_code && $consolidate_reports==1)
			{
				#covertExchangeRate($from_currency,$to_currency,$rate_date,$rate_value)
				$gl_credit = covertExchangeRate($gl_currency_code,$report_currency_code,$gl_date,$gl_credit);
				$gl_debit = covertExchangeRate($gl_currency_code,$report_currency_code,$gl_date,$gl_debit);
			}
			$arr_gl_credit[] = $gl_credit;
			$arr_gl_debit[] = $gl_debit;
		}	
		
		if (count($arr_gl_credit)!=0 || count($arr_sum_debit)!=0)
		{
			$arr_sum_credit = array_sum($arr_gl_credit);
			$arr_sum_debit = array_sum($arr_gl_debit);
		}	

		$report_balance = $arr_sum_credit-$arr_sum_debit;	
	}

	return $report_balance;
}

function getBudgetIdByName($budget_name)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					budget_id
			FROM	tbl_budget
			WHERE 	budget_name LIKE '%".trim($budget_name)."%'";
	$rs = mysql_query($sql) or die("Error in sql in function: getBudgetIdByName ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	return $rows["budget_id"];

}
function getAccountNumber($bank_id)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					bank_account_number
			FROM	tbl_bank
			WHERE 	bank_id = '$bank_id'";
	$rs = mysql_query($sql) or die("Error in sql in function: getAccountNumber ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	return $rows["bank_account_number"];
}
function getBankName($bank_id)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					bank_name
			FROM	tbl_bank
			WHERE 	bank_id = '$bank_id'";
	$rs = mysql_query($sql) or die("Error in sql in function: getBankName ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	return $rows["bank_name"];
}
function getBankIdBySLCode($bank_account_sl_code)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					bank_id
			FROM	tbl_bank
			WHERE 	bank_account_sl_code = '$bank_account_sl_code'";
	$rs = mysql_query($sql) or die("Error in sql in function: getBankIdBySLCode ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	return $rows["bank_id"];
}
function getBankAddress($bank_id)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					bank_address 
			FROM	tbl_bank
			WHERE 	bank_id = '$bank_id'";
	$rs = mysql_query($sql) or die("Error in sql in function: getBudgetIdByName ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	return $rows["bank_address"];
}
function getBeginningBalanceById($sl_id,$year,$currency_code)
{
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					sl_details_starting_balance
			FROM 	tbl_subsidiary_ledger_details
			WHERE  	sl_details_reference_number = '$sl_id'
			AND		sl_details_year = '$year'
			AND		sl_details_currency_code = '$currency_code'";
	$rs = mysql_query($sql) or die("Error in sql in function: getEndingBalanceById ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	$sl_details_starting_balance = $rows["sl_details_starting_balance"];
	
	return $sl_details_starting_balance;
}

function getCompanyName()
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
			FROM 	tbl_config
			WHERE	cfg_name = 'company_name'";
	$rs = mysql_query($sql) or die("Error in function: getCompanyName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cfg_value"];
}
function getCompanyAddress()
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
			FROM 	tbl_config
			WHERE	cfg_name = 'company_address'";
	$rs = mysql_query($sql) or die("Error in function: getCompanyAddress ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cfg_value"];
}
function getCompanyPhoneNumber()
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
			FROM 	tbl_config
			WHERE	cfg_name = 'company_phone_number'";
	$rs = mysql_query($sql) or die("Error in function: getCompanyPhoneNumber ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cfg_value"];
}
function getCompanyFaxNumber()
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
			FROM 	tbl_config
			WHERE	cfg_name = 'company_fax_number'";
	$rs = mysql_query($sql) or die("Error in function: getCompanyFaxNumber ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cfg_value"];
}
function getCompanyEmailAddress()
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
			FROM 	tbl_config
			WHERE	cfg_name = 'company_email_address'";
	$rs = mysql_query($sql) or die("Error in function: getCompanyEmailAddress ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cfg_value"];
}
function getCompanyWebsite()
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
			FROM 	tbl_config
			WHERE	cfg_name = 'company_website'";
	$rs = mysql_query($sql) or die("Error in function: getCompanyWebsite ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cfg_value"];
}
function getFullName($user_id,$format)
{
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					usr_first_name,	
					usr_middle_initial,	
					usr_last_name
			FROM 	tbl_users
			WHERE 	usr_id = '$user_id'";
	$rs = mysql_query($sql) or die("Error in function: getFullName ".$sql." ".mysql_error());	
	$count = mysql_num_rows($rs);
	if($count==1)
	{
		$rows = mysql_fetch_array($rs);
		$last_name = $rows["usr_last_name"];
		$first_name = $rows["usr_first_name"];
		$middle_initial = $rows["usr_middle_initial"];
		if($format==1)
		{
			$full_name = "$first_name $last_name";
		}
		else if($format==2)
		{
			$full_name = "$last_name, $first_name";
		}
		else if($format==3)
		{
			$first_name = substr($first_name, 0);
			$full_name = $first_name." ".$middle_initial.". ".$last_name;
		}
	}
	return $full_name;
}
function insertEventLog($uid,$activity)
{
	$time_stamp = date("Y-m-d h:i:s");
	$activity = addslashes($activity);
	$sql = "INSERT INTO tbl_event_logs (event_date,event_owner,event_description)
			VALUES ('$time_stamp','$uid','$activity')";
	mysql_query($sql) or die("Error inserting records in tbl_event_logs " . mysql_error());	
	
}
function stringtoarray($string) 
{
	$len = strlen($string);
	
	for($x=0; $x<$len; $x++) 
	{
		$a_string[] = substr($string, $x, 1);
	}
	
	return $a_string;
}

function sentenceCase($string)
{
	return ucwords(strtolower($string));
}
function upperCase($string)
{
	return strtoupper($string);
}
function checkForAdministratorAccount()
{
	
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					COUNT(usr_id) AS administrator_count
			FROM	tbl_users
			WHERE	usr_account_type = 1
			AND		usr_account_active = 1";
	$rs = mysql_query($sql) or die("Query Error" .mysql_error());
	$rows = mysql_fetch_array($rs);
	$administrator_count = $rows["administrator_count"];
	
	if ($administrator_count==0)
	{
		$sql = "INSERT INTO tbl_users 
						(
						 usr_username,
						 usr_password,
						 usr_first_name,
						 usr_middle_initial,
						 usr_last_name,
						 usr_account_type,
						 usr_department,
						 usr_account_active
						)
				VALUES	(
						'".DEFAULT_U."',
						'".md5(DEFAULT_P)."',
						'".DEFAULT_FN."',
						'".DEFAULT_MI."',
						'".DEFAULT_LN."',
						'1',
						'',
						'1'
						)";
		mysql_query($sql) or die("Query Error" .mysql_error());
	}
}
function getConfigurationValueByName($cfg_name)
{ 
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
			FROM	tbl_config
			WHERE	cfg_name = '$cfg_name'";
	$rs = mysql_query($sql) or die("Error in function: getConfigurationValueByName" . mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cfg_value"];
}

function getConfigurationValueById($cfg_id)
{ 
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
			FROM	tbl_config
			WHERE	cfg_id = '$cfg_id'";
	$rs = mysql_query($sql) or die("Error in function: getConfigurationValueByName" . mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cfg_value"];
}

function getConfigurationDescriptionByConfigValue($cfg_value)
{ 
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_description
			FROM	tbl_config
			WHERE	cfg_value = '$cfg_value'";
	$rs = mysql_query($sql) or die("Error in function: getConfigurationDescriptionByConfigValue" . mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cfg_description"];
}

function getConfigurationDescriptionById($cfg_id)
{ 
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_description
			FROM	tbl_config
			WHERE	cfg_id = '$cfg_id'";
	$rs = mysql_query($sql) or die("Error in function: getConfigurationDescriptionById" . mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cfg_description"];
}
function convertTri($num, $tri)
{
	 $ones = array(
	 "",
	 " One",
	 " Two",
	 " Three",
	 " Four",
	 " Five",
	 " Six",
	 " Seven",
	 " Eight",
	 " Nine",
	 " Ten",
	 " Eleven",
	 " Twelve",
	 " Thirteen",
	 " Fourteen",
	 " Fifteen",
	 " Sixteen",
	 " Seventeen",
	 " Eighteen",
	 " Nineteen"
	);
	
	$tens = array(
	 "",
	 "",
	 " Twenty",
	 " Thirty",
	 " Forty",
	 " Fifty",
	 " Sixty",
	 " Seventy",
	 " Eighty",
	 " Ninety"
	);
	
	$triplets = array(
	 "",
	 " Thousand",
	 " million",
	 " Billion",
	 " Trillion",
	 " Quadrillion",
	 " Quintillion",
	 " Sextillion",
	 " Septillion",
	 " Octillion",
	 " Nonillion"
	);
	
	
	
	// chunk the number, ...rxyy
	$r = (int) ($num / 1000);
	$x = ($num / 100) % 10;
	$y = $num % 100;
	
	// init the output string
	$str = "";
	
	// do hundreds
	if ($x > 0)
	$str = $ones[$x] . " Hundred";
	
	// do ones and tens
	if ($y < 20)
	$str .= $ones[$y];
	else
	$str .= $tens[(int) ($y / 10)] . $ones[$y % 10];
	
	// add triplet modifier only if there
	// is some output to be modified...
	if ($str != "")
	$str .= $triplets[$tri];
	
	// continue recursing?
	if ($r > 0)
	{
		return convertTri($r, $tri+1).$str;
	}
	else
	{
		return $str;
	}
}
function convertToWords($number) 
{ 
    $number = str_replace(",","",$number);
    $number = number_format($number,NUMBEROFDECIMALPLACES,'.','');
    $decimal = substr($number,strlen($number)-2,strlen($number)-1);
    $number = (int)$number;
	
	if ($number < 0)
	{
		return "negative".convertTri(-$number, 0);
	}
	
	if ($number == 0)
	{
		return "zero";
	}
	
	#if ($decimal != "00")
	#{
    	$output = " & $decimal/100";
	#}
	return convertTri($number, 0)."".$output;
 
} 

function dateTimeFormat($hr,$min,$sec,$month,$day,$year)
{
	if ($hr=="") 
	{
		$hr=0;
	}
	if ($min=="")
	{
		$min=0;
	}
	if ($sec=="")
	{
		$sec=0;
	}	
	
	return date("Y-m-d h:i:s", mktime($hr, $min, $sec, $month, $day, $year));
}
function dateFormat($month,$day,$year)
{
	return date("Y-m-d", mktime(0,0,0,$month, $day, $year));
}
function encrypt($sData)
{
	$sKey = WILDCARD;
	$sResult = '';
	
	for($i = 0; $i < strlen($sData); $i ++)
	{
	 $sChar    = substr($sData, $i, 1);
	 $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1);
	 $sChar    = chr(ord($sChar) + ord($sKeyChar));
	 $sResult .= $sChar;
	}
	
	return encode_base64($sResult);
}

function decrypt($sData)
{
	$sKey = WILDCARD;
	$sResult = '';
	$sData   = decode_base64($sData);
	
	for($i = 0; $i < strlen($sData); $i ++)
	{
	 $sChar    = substr($sData, $i, 1);
	 $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1);
	 $sChar    = chr(ord($sChar) - ord($sKeyChar));
	 $sResult .= $sChar;
	}
	
	return $sResult;
}

function encode_base64($sData)
{
	$sBase64 = base64_encode($sData);
	
	return strtr($sBase64, '+/', '-_');
}

function decode_base64($sData)
{
	$sBase64 = strtr($sData, '-_', '+/');
	
	return base64_decode($sBase64);
}
function getSubsidiaryLedgerAccountTitleById($sl_id)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					sl_account_title
			FROM	tbl_subsidiary_ledger
			WHERE	sl_id = '$sl_id'";
	$rs = mysql_query($sql) or die("Error in function: getSubsidiaryLedgerAccountTitleById ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["sl_account_title"];
}
function getSubsidiaryLedgerAccountCodeById($sl_id)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					sl_account_code
			FROM	tbl_subsidiary_ledger
			WHERE	sl_id = '$sl_id'";
	$rs = mysql_query($sql) or die("Error in function: getSubsidiaryLedgerAccountCodeById ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["sl_account_code"];
}

function checkIfNumber($string)
{
	if (is_numeric($string))
	{
		return true;
	}
	else
	{
		return false;
	}
}
function getBudgetName($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					budget_name
			FROM 	tbl_budget
			WHERE	budget_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getBudgetName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["budget_name"];	
}
function getBudgetID($str)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					budget_id
			FROM 	tbl_budget
			WHERE	budget_name = '$str'";
	$rs = mysql_query($sql) or die("Error in function: getBudgetID ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["budget_id"];	
}
function getDonorName($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					donor_name
			FROM 	tbl_donor
			WHERE	donor_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getDonorName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["donor_name"];	
}
function getDonorID($str)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					donor_id
			FROM 	tbl_donor
			WHERE	donor_name = '$str'";
	$rs = mysql_query($sql) or die("Error in function: getDonorID ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["donor_id"];	
}
function getComponentName($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					component_name
			FROM 	tbl_component
			WHERE	component_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getComponentName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["component_name"];	
}
function getComponentID($str)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					component_id
			FROM 	tbl_component
			WHERE	component_name = '$str'";
	$rs = mysql_query($sql) or die("Error in function: getComponentName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["component_id"];	
}
function getActivityName($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					activity_name
			FROM 	tbl_activity
			WHERE	activity_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getActivityName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["activity_name"];	
}

function getActivityId($str)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					activity_id
			FROM 	tbl_activity
			WHERE	activity_name = '$str'";
	$rs = mysql_query($sql) or die("Error in function: getActivityId ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["activity_id"];	
}
function getOtherCostName($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cs_name
			FROM 	tbl_other_cost_services
			WHERE	cs_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getOtherCostName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cs_name"];	
}
function getOtherCostID($str)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cs_id
			FROM 	tbl_other_cost_services
			WHERE	cs_name = '$str'";
	$rs = mysql_query($sql) or die("Error in function: getOtherCostName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cs_id"];	
}
function getStaffName($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					staff_name
			FROM 	tbl_staff
			WHERE	staff_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getStaffName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["staff_name"];	
}
function getStaffIDByID($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					staff_id
			FROM 	tbl_staff
			WHERE	staff_name = '$int'";
			
			echo $sql."<br>";
	$rs = mysql_query($sql) or die("Error in function: getStaffName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["staff_id"];	
}
function getBenefitsName($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					benefits_name
			FROM 	tbl_benefits
			WHERE	benefits_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getBenefitsName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["benefits_name"];	
}
function getBenefitsID($str)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					benefits_id
			FROM 	tbl_benefits
			WHERE	benefits_name = '$str'";
	$rs = mysql_query($sql) or die("Error in function: getBenefitsName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["benefits_id"];	
}
function getVehicleName($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					vehicle_name
			FROM 	tbl_vehicle
			WHERE	vehicle_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getVehicleName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["vehicle_name"];	
}
function getVehicleID($str)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					vehicle_id
			FROM 	tbl_vehicle
			WHERE	vehicle_name = '$str'";
	$rs = mysql_query($sql) or die("Error in function: getVehicleName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["vehicle_id"];	
}
function getEquipmentName($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					equipment_name
			FROM 	tbl_equipment
			WHERE	equipment_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getEquipmentName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["equipment_name"];	
}
function getEquipmentID($str)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					equipment_id
			FROM 	tbl_equipment
			WHERE	equipment_name = '$str'";
	$rs = mysql_query($sql) or die("Error in function: getEquipmentID ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["equipment_id"];	
}
function getItemName($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					item_name
			FROM 	tbl_item_code
			WHERE	item_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getItemName ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["item_name"];	
}
function getItemId($str)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					item_id
			FROM 	tbl_item_code
			WHERE	item_name = '$str'";
	$rs = mysql_query($sql) or die("Error in function: getItemId ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["item_id"];	
}
function getBudgetDescription($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					budget_description
			FROM 	tbl_budget
			WHERE	budget_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getBudgetDescription ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["budget_description"];	
}
function getDonorDescription($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					donor_description
			FROM 	tbl_donor
			WHERE	donor_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getDonorDescription ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["donor_description"];	
}
function getComponentDescription($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					component_description
			FROM 	tbl_component
			WHERE	component_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getComponentDescription ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["component_description"];	
}
function getActivityDescription($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					activity_description
			FROM 	tbl_activity
			WHERE	activity_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getActivityDescription ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["activity_description"];	
}
function getOtherCostDescription($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cs_description
			FROM 	tbl_other_cost_services
			WHERE	cs_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getOtherCostDescription ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["cs_description"];	
}
function getStaffDescription($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					staff_description
			FROM 	tbl_staff
			WHERE	staff_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getStaffDescription ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["staff_description"];	
}
function getBenefitsDescription($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					benefits_description
			FROM 	tbl_benefits
			WHERE	benefits_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getBenefitsDescription ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["benefits_description"];	
}
function getVehicleDescription($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					vehicle_description
			FROM 	tbl_vehicle
			WHERE	vehicle_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getVehicleDescription ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["vehicle_description"];	
}
function getEquipmentDescription($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					equipment_description
			FROM 	tbl_equipment
			WHERE	equipment_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getEquipmentDescription ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["equipment_description"];	
}
function getItemDescription($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					item_description
			FROM 	tbl_item_code
			WHERE	item_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getItemDescription ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["item_description"];	
}
function getPayeeById($int)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					payee_name
			FROM 	tbl_payee
			WHERE	payee_id = '$int'";
	$rs = mysql_query($sql) or die("Error in function: getPayeeById ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["payee_name"];	
}
function getPayeeIDByName($payee)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					payee_id
			FROM 	tbl_payee
			WHERE	payee_name LIKE '%".$payee."%'";
	$rs = mysql_query($sql) or die("Error in function: getPayeeById ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	return $rows["payee_id"];	
}
function getChequeNumber($cheque_reference_number)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cheque_number
			FROM 	tbl_cheque
			WHERE	cheque_reference_number = '$cheque_reference_number'
			ORDER BY cheque_id ASC";
	$rs = mysql_query($sql) or die("Error in function: getChequeNumber ".$sql." ".mysql_error());	
	while($rows=mysql_fetch_array($rs))
	{
		$cheque_number[] = $rows["cheque_number"];
	}
	
	return $cheque_number;	
}
function getChequeNumberDate($cheque_reference_number)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cheque_date
			FROM 	tbl_cheque
			WHERE	cheque_reference_number = '$cheque_reference_number'
			ORDER BY cheque_id ASC";
	$rs = mysql_query($sql) or die("Error in function: getChequeNumber ".$sql." ".mysql_error());	
	while($rows=mysql_fetch_array($rs))
	{
		$cheque_date[] = $rows["cheque_date"];
	}
	
	return $cheque_date;	
}
function checkSubsidiaryLedgerRequirement($account_code,$mode)
{
	$sl_ledger_requirement = "";
	
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					$mode
			FROM 	tbl_subsidiary_ledger
			WHERE	sl_id = '$account_code'";
	$rs = mysql_query($sql) or die("Error in function: checkSubsidiaryLedgerRequirement ".$sql." ".mysql_error());	
	$rows = mysql_fetch_array($rs);
	$sl_ledger_requirement = $rows[$mode];
	
	return $sl_ledger_requirement;
}
function numberFormat($int)
{
	return number_format($int,NUMBEROFDECIMALPLACES);
}
function getTotalDebitCredit($mode,$reference_number)
{
	if ($mode)
	{
		switch ($mode)
		{
			case "DV":
				$get_data_from_table = "tbl_disbursement_voucher_details";
				$record_id = "dv_details_reference_number";
				$field_a = "dv_details_debit";
				$field_b = "dv_details_credit";
			break;
			case "JV":
				$get_data_from_table = "tbl_journal_voucher_details";
				$record_id = "jv_details_reference_number";
				$field_a = "jv_details_debit";
				$field_b = "jv_details_credit";
			break;	
			case "CV":
				$get_data_from_table = "tbl_cash_voucher_details";
				$record_id = "cv_details_reference_number";
				$field_a = "cv_details_debit";
				$field_b = "cv_details_credit";
			break;						
		}
	
		$sql = "SELECT 	SQL_BUFFER_RESULT
						SQL_CACHE
						SUM(".$field_a.") AS total_debit,
						SUM(".$field_b.") AS total_credit
				FROM 	".$get_data_from_table."
				WHERE	".$record_id." = '".$reference_number."'";
		$rs = mysql_query($sql) or die("Error in function: getTotalDebitCredit ".$sql." ".mysql_error());	
		$rows = mysql_fetch_array($rs);
		
		$total_debit = $rows["total_debit"];	
		$total_credit = $rows["total_credit"];
		
		$arr_debit_credit[] = array("total_debit" => $total_debit, "total_credit" => $total_credit);
		
		return $arr_debit_credit;
	}
}
function getFileExtension($filename)
{
  return substr($filename, strrpos($filename, '.'));
}
function getMaxUploadSize()
{
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
			FROM 	tbl_config
			WHERE 	cfg_name = 'file_size_limit'";
	$rs = mysql_query($sql) or die("Query Error getMaxUploadSize" .mysql_error());
	$rows = mysql_fetch_array($rs);
	
	$file_size_limit = $rows["cfg_value"];
	
	return $file_size_limit;
}
function checkFileExtention($file_ext)
{
	$sql = "SELECT	count(cfg_id) AS recordcount
			FROM	tbl_config
			WHERE	cfg_name = 'allowed_file_type'
			AND		cfg_value = '$file_ext'";
	$rs = mysql_query($sql) or die("Query Error checkFileExtention" .mysql_error());
	$rows = mysql_fetch_array($rs);
	$recordcount = $rows["recordcount"];

	if ($recordcount == 0)
	{
		$error = "Error in File ".$file_name." : Invalid File Format, choose image files only!<br>";
	}

	return $error;
}
function copyFile($tempfilename,$filename,$file_ext)
{
	$htmlDirectory = getWebRootDirectory ();
	$checkFileDestinationDirectory = checkFileDestinationDirectory ($file_ext);
	$document_path = $htmlDirectory.$checkFileDestinationDirectory.$filename;

	if (!copy($tempfilename,$document_path)) 
	{ 
		$error =  "Failed to upload file: ".$filename.", please contact your administrator!<br>"; 
	} 
	return $error;
}

function checkFileDestinationDirectory ($fileExtention)
{

	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_description
			FROM	tbl_config 
			WHERE	cfg_value = '$fileExtention'";
	$rs = mysql_query($sql) or die("Query Error checkFileDestinationDirectory" .mysql_error());
	$rows = mysql_fetch_array($rs);
	$fileDirectory = $rows["cfg_description"];
	
	return $fileDirectory;
}
function getWebRootDirectory ()
{
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
				FROM tbl_config
				WHERE cfg_name = 'root_directory'";
	$rs = mysql_query($sql) or die("Query Error getWebRootDirectory" .mysql_error());
	$rows = mysql_fetch_array($rs);
	
	$root_directory = $rows["cfg_value"];
	
	return $root_directory;
	
}
function checkForFileSize($filesize,$file_name)
{
	$maxupload_size = getMaxUploadSize();
	if ($filesize > $maxupload_size)
	{
		$error = "Uploaded File ".$file_name." exceeds the ".$maxupload_size." limit, choose another file!<br>";
	}
	return $error;
}
function convertBytes($bytes) 
{
	$size = $bytes / 1024;
	if($size < 1024)
	{
		$size = number_format($size);
        $size .= ' KB';
	} 
    else 
    {
        if($size / 1024 < 1024) 
		{
            $size = number_format($size / 1024);
            $size .= ' MB';
		} 
        else if ($size / 1024 / 1024 < 1024)  
		{
            $size = number_format($size / 1024 / 1024);
            $size .= ' GB';
		} 
    }
	return $size;
}
function insertToFileTable($file_date,$file_reference,$file_name,$file_extention,$inserted_by)
{
	$sql = "INSERT INTO tbl_file_attachment
						(file_date,
						 file_reference,
						 file_name,
						 file_extention
						 )
			VALUES		('$file_date',
						 '$file_reference',
						 '$file_name',
						 '$file_extention'
						)";
	$rs = mysql_query($sql) or die("Error in function: insertToFileTable" .mysql_error());
	$transaction_id = mysql_insert_id();
	insertEventLog($inserted_by,$sql);
	return $transaction_id;
}
function authenticateApprover($user_id,$usr_authentication_password)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					COUNT(usr_id) AS count_records
			FROM	tbl_users
			WHERE	usr_id = '$user_id'
			AND		usr_authentication_password = '$usr_authentication_password'";
	$rs = mysql_query($sql) or die("Error in function: authenticateApprover" .mysql_error());
	$rows = mysql_fetch_array($rs);
	
	$count_records = $rows["count_records"];
	
	if ($count_records == 0)
	{
		$flag = 1;
	}
	
	return $flag;
}
function getUserSignature($user_id)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					usr_digital_signature
			FROM	tbl_users
			WHERE	usr_id = '$user_id'";
	$rs = mysql_query($sql) or die("Error in function: getUserSignature" .mysql_error());
	$rows = mysql_fetch_array($rs);
	
	$usr_digital_signature = $rows["usr_digital_signature"];	
	
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					file_name,
					file_extention
			FROM	tbl_file_attachment
			WHERE	file_id = '$usr_digital_signature'";
	$rs = mysql_query($sql) or die("Error in function: getUserSignature" .mysql_error());
	$rows = mysql_fetch_array($rs);
	$total_rows = mysql_num_rows($rs);
	
	if ($total_rows!=0)
	{
		$file_name = $rows["file_name"];
		$file_extention = $rows["file_extention"];	
		
		$htmlDirectory = getWebRootDirectory ();
		$checkFileDestinationDirectory = checkFileDestinationDirectory ($file_extention);
		$doc_name = "/workspaceGOP/".$checkFileDestinationDirectory.$file_name;	
		
		$attached_image = "<img src='".$doc_name."' width='150' height='50'>";
	}
	else
	{
		$attached_image = "&nbsp;";
	}
	
	return $attached_image;
}
function addBankCheque($account_code)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					COUNT(bank_id) AS count_records
			FROM	tbl_bank
			WHERE	bank_account_sl_code = '$account_code'";
	$rs = mysql_query($sql) or die("Error in function: addBankCheque" .mysql_error());
	$rows = mysql_fetch_array($rs);
	
	$count_records = $rows["count_records"];
	
	if ($count_records == 1)
	{
		$flag = 1;
	}
	
	return $flag;
}

function checkIfCashInBank($sl_id)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					COUNT(*) AS count_records
			FROM 	tbl_subsidiary_ledger
			WHERE 	sl_account_title LIKE 'Cash in%'
			AND		sl_id = '$sl_id'";
	$rs = mysql_query($sql) or die("Error in function: checkIfCashInBank" .mysql_error());
	$rows = mysql_fetch_array($rs);
	if ($rows["count_records"] == 1) { return 1;}
}

function checkIfPettyCash($sl_id)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					COUNT(*) AS count_records
			FROM 	tbl_subsidiary_ledger
			WHERE 	sl_account_title LIKE 'Petty Cash%'
			AND		sl_id = '$sl_id'";
	$rs = mysql_query($sql) or die("Error in function: checkIfPettyCash" .mysql_error());
	$rows = mysql_fetch_array($rs);
	if ($rows["count_records"] == 1) { return 1;}
}
function getBudgetIdentification($budget_year)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_description
			FROM 	tbl_config
			WHERE 	cfg_value = '$budget_year'
			AND		cfg_name = 'budget_identification'";
	$rs = mysql_query($sql) or die("Error in function: getBudgetIdentification " .mysql_error());
	$rows = mysql_fetch_array($rs);
	
	return $rows["cfg_description"];
}
function yearlySLBeginningBalance($user_id)
{
	$current_year = date("Y");
	
	$sql = "SELECT COUNT(*) AS count_records
			FROM tbl_subsidiary_ledger_details 
			WHERE sl_details_year = '".$current_year."'";
	$rs = mysql_query($sql) or die("Error in function: yearlySLBeginningBalance" .mysql_error());
	$rows = mysql_fetch_array($rs);
	if ($rows["count_records"] == 0) 
	{
		$sql = "SELECT 	SQL_BUFFER_RESULT
						SQL_CACHE
						a.sl_id AS sl_id,
						b.cfg_id AS cfg_id
				FROM 	tbl_subsidiary_ledger a,
						tbl_config b
				WHERE 	b.cfg_name = 'currency_type'
				ORDER BY a.sl_id ASC";
		$rs = mysql_query($sql) or die("SQL".$sql." ".mysql_error());
		while($rows=mysql_fetch_array($rs))
		{
			$sql2 = "SELECT SQL_BUFFER_RESULT
							SQL_CACHE
							COUNT(*) AS record_count
					 FROM	tbl_subsidiary_ledger_details
					 WHERE	sl_details_reference_number = '".$rows['sl_id']."'
					 AND	sl_details_year = '".$current_year."'
					 AND	sl_details_currency_code = '".$rows['cfg_id']."'";	
			$rs2 = mysql_query($sql2) or die("SQL".$sql2." ".mysql_error());		
			$rows2=mysql_fetch_array($rs2);
			if ($rows2['record_count']!=1)
			{
				$sql3 = "INSERT INTO tbl_subsidiary_ledger_details (sl_details_reference_number,	
																	sl_details_year,	
																	sl_details_currency_code,	
																	sl_details_starting_balance,	
																	sl_details_ending_balance)
						 VALUES ('".$rows['sl_id']."',
								'".$current_year."',
								'".$rows['cfg_id']."',
								'0',
								'0')";
				$rs3 = mysql_query($sql3) or die("SQL".$sql3." ".mysql_error());
				insertEventLog($user_id,$sql3);
			}
		}

		$sql = "SELECT 	COUNT(*) AS record_count
				FROM	tbl_config
				WHERE	cfg_value = '".$current_year."'";
		$rs = mysql_query($sql) or die("SQL".$sql." ".mysql_error());
		$rows=mysql_fetch_array($rs);
		if ($rows["record_count"]==0)
		{
			$sql = "INSERT INTO (cfg_name,cfg_value) 
					VALUES 		('budget_identification','".$reference_year."')";
			mysql_query($sql) or die("SQL".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
		}	
	}
}
function getDebitCreditSumValue($account_code,$currency_code,$start_date,$end_date,$budget_id,$donor_id,$component_id,$activity_id,$other_cost_id,$staff_id,$benefits_id,$vehicle_id,$equipment_id,$item_id,$consolidate_report,$use_obligated_values)
{
	if ($use_obligated_values==1)
	{
		$sql = "SELECT	SQL_BUFFER_RESULT
						SQL_CACHE
						bc_date AS transaction_date,
						bc_currency_code AS transaction_currency_code,
						bc_debit AS transaction_debit,
						bc_credit AS transaction_credit
				FROM	tbl_budget_controller
				WHERE	bc_account_code = '$account_code' 
				AND		bc_date
				BETWEEN	'$start_date'
				AND		'$end_date' ";
		if ($consolidate_report!=1)
		{
			$sql .= "AND	bc_currency_code = '$currency_code' ";
		}
		if ($budget_id!="")
		{
			$sql .= "AND	bc_budget_id = '$budget_id' ";
		}
		if ($donor_id!="")
		{
			$sql .= "AND	bc_donor_id = '$donor_id' ";
		}
		if ($component_id!="")
		{
			$sql .= "AND	bc_component_id = '$component_id '";
		}
		if ($activity_id!="")
		{
			$sql .= "AND	bc_activity_id = '$activity_id' ";
		}
		if ($other_cost_id!="")
		{
			$sql .= "AND	bc_other_cost_id = '$other_cost_id' ";
		}
		if ($staff_id!="")
		{
			$sql .= "AND	bc_staff_id = '$staff_id' ";
		}
		if ($benefits_id!="")
		{
			$sql .= "AND	bc_benefits_id = '$benefits_id' ";
		}
		if ($vehicle_id!="")
		{
			$sql .= "AND	bc_vehicle_id = '$vehicle_id' ";
		}
		if ($vehicle_id!="")
		{
			$sql .= "AND	bc_vehicle_id = '$vehicle_id' ";
		}
		if ($equipment_id!="")
		{
			$sql .= "AND	bc_equipment_id = '$equipment_id' ";
		}
		if ($item_id!="")
		{
			$sql .= "AND	bc_item_id = '$item_id' ";
		}	
	}
	else
	{
		$sql = "SELECT	SQL_BUFFER_RESULT
						SQL_CACHE
						gl_date AS transaction_date,
						gl_currency_code AS transaction_currency_code,
						gl_debit AS transaction_debit,
						gl_credit AS transaction_credit
				FROM	tbl_general_ledger
				WHERE	gl_account_code = '$account_code' 
				AND		gl_date
				BETWEEN	'$start_date'
				AND		'$end_date' ";
		if ($consolidate_report!=1)
		{
			$sql .= "AND	gl_currency_code = '$currency_code' ";
		}
		if ($budget_id!="")
		{
			$sql .= "AND	gl_budget_id = '$budget_id' ";
		}
		if ($donor_id!="")
		{
			$sql .= "AND	gl_donor_id = '$donor_id' ";
		}
		if ($component_id!="")
		{
			$sql .= "AND	gl_component_id = '$component_id '";
		}
		if ($activity_id!="")
		{
			$sql .= "AND	gl_activity_id = '$activity_id' ";
		}
		if ($other_cost_id!="")
		{
			$sql .= "AND	gl_other_cost_id = '$other_cost_id' ";
		}
		if ($staff_id!="")
		{
			$sql .= "AND	gl_staff_id = '$staff_id' ";
		}
		if ($benefits_id!="")
		{
			$sql .= "AND	gl_benefits_id = '$benefits_id' ";
		}
		if ($vehicle_id!="")
		{
			$sql .= "AND	gl_vehicle_id = '$vehicle_id' ";
		}
		if ($vehicle_id!="")
		{
			$sql .= "AND	gl_vehicle_id = '$vehicle_id' ";
		}
		if ($equipment_id!="")
		{
			$sql .= "AND	gl_equipment_id = '$equipment_id' ";
		}
		if ($item_id!="")
		{
			$sql .= "AND	gl_item_id = '$item_id' ";
		}
	}
#echo $sql."<br><br>";
	$rs = mysql_query($sql) or die("Error in sql in function getDebitCreditSumValue ".$sql." ".mysql_error());	
	while($rows=mysql_fetch_array($rs))
	{
		$transaction_debit = $rows["transaction_debit"];
		$transaction_credit = $rows["transaction_credit"];
		$transaction_currency_code = $rows["transaction_currency_code"];
		$transaction_date = $rows["transaction_date"];
		
		
		if ($transaction_currency_code != $currency_code && $consolidate_report==1)
		{
			#covertExchangeRate($from_currency,$to_currency,$rate_date,$rate_value)
			$transaction_credit = covertExchangeRate($transaction_currency_code,$currency_code,$transaction_date,$transaction_credit);
			$transaction_debit = covertExchangeRate($transaction_currency_code,$currency_code,$transaction_date,$transaction_debit);
		}
		$arr_credit[] = $transaction_credit;
		$arr_debit[] = $transaction_debit;
	}	

	if (count($arr_credit)>0 && count($arr_debit)>0)
	{
		$arr_sum_credit = array_sum($arr_credit);
		$arr_sum_debit = array_sum($arr_debit);
	}	

	return $arr_sum_debit-$arr_sum_credit;
}
function getDebitOrCredit($mode,$account_code,$currency_code,$start_date,$end_date,$budget_id,$donor_id,$component_id,$activity_id,$other_cost_id,$staff_id,$benefits_id,$vehicle_id,$equipment_id,$item_id,$consolidate_report,$use_obligated_values)
{
	unset($arr_credit);
	unset($arr_debit);

	if ($use_obligated_values==1)
	{
		$sql = "SELECT	SQL_BUFFER_RESULT
						SQL_CACHE
						bc_date AS transaction_date,
						bc_currency_code AS transaction_currency_code ";
		if ($mode=="DEBIT")
		{
			$sql .= ", ";
			$sql .= "	bc_debit AS transaction_debit";
		}
		if ($mode=="CREDIT")
		{
			$sql .= ", ";
			$sql .= "	bc_credit AS transaction_credit";
		}
		$sql .="FROM	tbl_budget_controller
				WHERE	bc_account_code = '$account_code' 
				AND		bc_date
				BETWEEN	'$start_date'
				AND		'$end_date' ";
		if ($consolidate_report!=1)
		{
			$sql .= "AND	bc_currency_code = '$currency_code' ";
		}
		if ($budget_id!="")
		{
			$sql .= "AND	bc_budget_id = '$budget_id' ";
		}
		if ($donor_id!="")
		{
			$sql .= "AND	bc_donor_id = '$donor_id' ";
		}
		if ($component_id!="")
		{
			$sql .= "AND	bc_component_id = '$component_id '";
		}
		if ($activity_id!="")
		{
			$sql .= "AND	bc_activity_id = '$activity_id' ";
		}
		if ($other_cost_id!="")
		{
			$sql .= "AND	bc_other_cost_id = '$other_cost_id' ";
		}
		if ($staff_id!="")
		{
			$sql .= "AND	bc_staff_id = '$staff_id' ";
		}
		if ($benefits_id!="")
		{
			$sql .= "AND	bc_benefits_id = '$benefits_id' ";
		}
		if ($vehicle_id!="")
		{
			$sql .= "AND	bc_vehicle_id = '$vehicle_id' ";
		}
		if ($vehicle_id!="")
		{
			$sql .= "AND	bc_vehicle_id = '$vehicle_id' ";
		}
		if ($equipment_id!="")
		{
			$sql .= "AND	bc_equipment_id = '$equipment_id' ";
		}
		if ($item_id!="")
		{
			$sql .= "AND	bc_item_id = '$item_id' ";
		}	
	}
	else
	{
		$sql = "SELECT	SQL_BUFFER_RESULT
						SQL_CACHE
						gl_date AS transaction_date,
						gl_currency_code AS transaction_currency_code ";
		if ($mode=="DEBIT")
		{
			$sql .= ", ";
			$sql .= "	gl_debit AS transaction_debit";
		}
		if ($mode=="CREDIT")
		{
			$sql .= ", ";
			$sql .= "	gl_credit AS transaction_credit";
		}
		$sql .="
				FROM	tbl_general_ledger
				WHERE	gl_account_code = '$account_code' 
				AND		gl_date
				BETWEEN	'$start_date'
				AND		'$end_date' ";
		if ($consolidate_report!=1)
		{
			$sql .= "AND	gl_currency_code = '$currency_code' ";
		}
		if ($budget_id!="")
		{
			$sql .= "AND	gl_budget_id = '$budget_id' ";
		}
		if ($donor_id!="")
		{
			$sql .= "AND	gl_donor_id = '$donor_id' ";
		}
		if ($component_id!="")
		{
			$sql .= "AND	gl_component_id = '$component_id '";
		}
		if ($activity_id!="")
		{
			$sql .= "AND	gl_activity_id = '$activity_id' ";
		}
		if ($other_cost_id!="")
		{
			$sql .= "AND	gl_other_cost_id = '$other_cost_id' ";
		}
		if ($staff_id!="")
		{
			$sql .= "AND	gl_staff_id = '$staff_id' ";
		}
		if ($benefits_id!="")
		{
			$sql .= "AND	gl_benefits_id = '$benefits_id' ";
		}
		if ($vehicle_id!="")
		{
			$sql .= "AND	gl_vehicle_id = '$vehicle_id' ";
		}
		if ($vehicle_id!="")
		{
			$sql .= "AND	gl_vehicle_id = '$vehicle_id' ";
		}
		if ($equipment_id!="")
		{
			$sql .= "AND	gl_equipment_id = '$equipment_id' ";
		}
		if ($item_id!="")
		{
			$sql .= "AND	gl_item_id = '$item_id' ";
		}
	}

	$rs = mysql_query(trim($sql)) or die("Error in sql in function getDebitOrCredit ".$sql." ".mysql_error());	
	while($rows=mysql_fetch_array($rs))
	{
		$transaction_debit = $rows["transaction_debit"];
		$transaction_credit = $rows["transaction_credit"];
		$transaction_currency_code = $rows["transaction_currency_code"];
		$transaction_date = $rows["transaction_date"];
		
		if ($transaction_currency_code != $currency_code && $consolidate_report==1)
		{
			$transaction_credit = covertExchangeRate($transaction_currency_code,$currency_code,$transaction_date,$transaction_credit);
			$transaction_debit = covertExchangeRate($transaction_currency_code,$currency_code,$transaction_date,$transaction_debit);
		}
		$arr_credit[] = $transaction_credit;
		$arr_debit[] = $transaction_debit;
	}	

	if ($mode=="DEBIT" && count($arr_debit)>0)
	{
		return array_sum($arr_debit);
	}
	if ($mode=="CREDIT" && count($arr_credit)>0)
	{
		return array_sum($arr_credit);
	}
}
function getProgramStartYear()
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					cfg_value
			FROM 	tbl_config
			WHERE 	cfg_name = 'budget_identification'
			ORDER BY cfg_name ASC
			LIMIT 1";
	$rs = mysql_query($sql) or die("Error in function: getProgramStartYear " .mysql_error());
	$rows = mysql_fetch_array($rs);
	
	return $rows["cfg_value"];
}
function checkIfLastYearIsClose($reference_year)
{
	$reference_year = $reference_year - 1;
	
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					COUNT(cfg_id) AS cfg_id
			FROM 	tbl_config
			WHERE 	cfg_name = 'closed_accounting_year'
			AND		cfg_value = '$reference_year'
			LIMIT 1";
	$rs = mysql_query($sql) or die("Error in function: checkIfLastYearIsClose " .mysql_error());
	$rows = mysql_fetch_array($rs);
	
	if($rows["cfg_id"]!=0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function checkIfNominalAccount($sl_id)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					is_nominal_account
			FROM 	tbl_subsidiary_ledger
			WHERE	sl_id = '$sl_id'";
	$rs = mysql_query($sql) or die("Error in function: checkIfNominalAccount " .mysql_error());
	$rows = mysql_fetch_array($rs);	
	
	if($rows["is_nominal_account"]==1)
	{
		return true;
	}
	else
	{
		return false;
	}	
}
?>