<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$id = $_GET['id'];
$id = decrypt($id);

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$record_id = $_POST["record_id"];
	$payee_name = $_POST["payee_name"];
	$payee_type = $_POST["payee_type"];
	$payee_contact_name = $_POST["payee_contact_name"];
	$payee_contact_title = $_POST["payee_contact_title"];
	$payee_address = $_POST["payee_address"];
	$payee_city = $_POST["payee_city"];
	$payee_phone_number = $_POST["payee_phone_number"];
	$payee_fax_number = $_POST["payee_fax_number"];
	
	if ($payee_name == "") { $display_msg = 1; $msg .= "Please enter Payee Name"; $msg .= "<br>"; }
	if ($payee_type == "") { $display_msg = 1; $msg .= "Please select Payee Type"; $msg .= "<br>"; }
	if ($payee_contact_name == "") { $display_msg = 1; $msg .= "Please enter Contact Name"; $msg .= "<br>"; }
	if ($payee_contact_title == "") { $display_msg = 1; $msg .= "Please select Contact Title"; $msg .= "<br>"; }
	if ($payee_address == "") { $display_msg = 1; $msg .= "Please enter Address"; $msg .= "<br>"; }
	if ($payee_city == "") { $display_msg = 1; $msg .= "Please enter City"; $msg .= "<br>"; }
	if ($payee_phone_number == "") { $display_msg = 1; $msg .= "Please enter Phone Number"; $msg .= "<br>"; }
	if ($payee_fax_number == "") { $display_msg = 1; $msg .= "Please enter Fax Number"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$payee_name = addslashes($payee_name);
		$payee_contact_name = addslashes($payee_contact_name);
		$payee_address = addslashes($payee_address);
		
		$sql = "UPDATE 	tbl_payee
				SET		payee_name = '".trim($payee_name)."',
						payee_type = '".trim($payee_type)."',
						payee_contact_name = '".trim($payee_contact_name)."',
						payee_contact_title = '".trim($payee_contact_title)."',
						payee_address = '".trim($payee_address)."',
						payee_city = '".trim($payee_city)."',
						payee_phone_number = '".trim($payee_phone_number)."',
						payee_fax_number = '".trim($payee_fax_number)."'
				WHERE	payee_id = '$record_id'";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		$display_msg = 1;
		$msg = "Record updated successfully!";
		header("location:/workspaceGOP/page.report.components/payee.list.php?msg=1");
	}
}


$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				payee_id,
				payee_name,
				payee_type,
				payee_contact_name,
				payee_contact_title,
				payee_address,
				payee_city,
				payee_phone_number,
				payee_fax_number
		FROM	tbl_payee
		WHERE	payee_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: settings.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["payee_id"];
$payee_name = $rows["payee_name"];
$payee_type = $rows["payee_type"];
$payee_contact_name = $rows["payee_contact_name"];
$payee_contact_title = $rows["payee_contact_title"];
$payee_address = $rows["payee_address"];
$payee_city = $rows["payee_city"];
$payee_phone_number = $rows["payee_phone_number"];
$payee_fax_number = $rows["payee_fax_number"];

$payee_name = stripslashes($payee_name);
$payee_contact_name = stripslashes($payee_contact_name);
$payee_address = stripslashes($payee_address);
		

if ($record_id!=$id)
{
	insertEventLog($user_id,"User trying to manipulate the page: payee.edit.list.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
else
{
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="journal_voucher" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Payee Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Payee Name* </b></td>
							<td width="80%"><label><input name="payee_name" type="text" class="formbutton" id="payee_name" size="80" value="<?php echo $payee_name; ?>"/></label></td>
						</tr>
						<tr>
							<td><b>Payee Type*</b></td>
							<td>
								<?php
								$sql = "SELECT	SQL_BUFFER_RESULT
												SQL_CACHE
												cfg_id,
												cfg_value
										FROM 	tbl_config
										WHERE	cfg_name = 'payee_type'";
								$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
								$str .= "<select name='payee_type' class='formbutton'>";
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['cfg_id'];
									$column_name = $data['cfg_value'];
									$str .= "<option value='$column_id'";
									if ($payee_type == $column_id)
									{
										$str .= " SELECTED";
									}
									$str .= ">".$column_name."";
								}
								$str .= "</select>&nbsp;";
								echo $str;
								?>								
							</td>
						</tr>
						<tr>
							<td><b>Title*</b></td>
							<td>
								<?php
								$sql1 = "SELECT	SQL_BUFFER_RESULT
												SQL_CACHE
												cfg_id,
												cfg_value
										FROM 	tbl_config
										WHERE	cfg_name = 'payee_contact_title'";
								$rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
								$str1 .= "<select name='payee_contact_title' class='formbutton'>";
								while($data1=mysql_fetch_array($rs1))
								{
									$column_id = $data1['cfg_id'];
									$column_name = $data1['cfg_value'];
									$str1 .= "<option value='$column_id'";
									if ($payee_contact_title == $column_id)
									{
										$str1 .= " SELECTED";
									}
									$str1 .= ">".$column_name."";
								}
								$str1 .= "</select>&nbsp;";
								echo $str1;
								?>							
							</td>
						</tr>
						<tr>
							<td><b>Contact Name*</b></td>
							<td><label><input name="payee_contact_name" type="text" class="formbutton" id="payee_contact_name" value="<?php echo $payee_contact_name; ?>" size="80"/>
							</label></td>
						</tr>	
						<tr>
							<td valign="top"><b>Contact Address*</b></td>
							<td><label>
							  <textarea name="payee_address" cols="75" rows="5" class="formbutton"><?php echo $payee_address; ?></textarea>
							</label></td>
						</tr>
						<tr>
							<td><b>City*</b></td>
							<td><label><input name="payee_city" type="text" class="formbutton" id="payee_city" value="<?php echo $payee_city; ?>" />
							</label></td>
						</tr>
						<tr>
							<td><b>Phone Number*</b></td>
							<td><label><input name="payee_phone_number" type="text" class="formbutton" id="payee_phone_number" value="<?php echo $payee_phone_number; ?>" />
							</label></td>
						</tr>				
						<tr>
							<td><b>Fax Number*</b></td>
							<td><label><input name="payee_fax_number" type="text" class="formbutton" id="payee_fax_number" value="<?php echo $payee_fax_number; ?>" />
							</label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php
}
?>
<?php include("./../includes/footer.main.php"); ?>