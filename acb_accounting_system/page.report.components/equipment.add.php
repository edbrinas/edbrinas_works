<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

if ($_POST['Submit'] == 'Save')
{
	$msg = "";
	
	$equipment_name = $_POST["equipment_name"];
	$equipment_description = $_POST["equipment_description"];
	
	if ($equipment_name == "") { $display_msg = 1; $msg .= "Please enter Equipment Name"; $msg .= "<br>"; }
	if ($equipment_description == "") { $display_msg = 1; $msg .= "Please select Equipment Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$equipment_name = addslashes($equipment_name);
		$equipment_description = addslashes($equipment_description);
		
		$sql = "INSERT INTO tbl_equipment
							(
							equipment_name,
							equipment_description
							)
				VALUES
							(
							'".trim($equipment_name)."',
							'".trim($equipment_description)."'
							)";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		$display_msg = 1;
		$msg = "Record inserted successfully!";
		
		$equipment_name = "";
		$equipment_description = "";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="equipment" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Equipment Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Save" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Equipment Name* </b></td>
							<td width="80%"><label><input name="equipment_name" type="text" class="formbutton" id="equipment_name" size="80" value="<?php echo $equipment_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Equipment Description </b></td>
							<td><label><textarea name="equipment_description" cols="50" rows="10" class="formbutton"><?php echo $equipment_description; ?></textarea></label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>