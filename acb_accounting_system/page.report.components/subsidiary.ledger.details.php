<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$sl_code = $_GET['sl_account_code'];
$id = decrypt($id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				sl_id,
				sl_account_code,
				sl_account_title
				sl_budget_id,
				sl_donor_id,
				sl_component_id,
				sl_activity_id,
				sl_other_cost_id,
				sl_staff_id,
				sl_benefits_id,
				sl_vehicle_id,
				sl_equipment_id,
				sl_item_id,
				sl_added_by,
				sl_date_added
		FROM 	tbl_subsidiary_ledger
		WHERE	sl_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: subsidiary.ledger.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["sl_id"];
$sl_account_code = $rows["sl_account_code"];
$sl_account_title = $rows["sl_account_title"];
$sl_budget_id = $rows["sl_budget_id"];
$sl_donor_id = $rows["sl_donor_id"];
$sl_component_id = $rows["sl_component_id"];
$sl_activity_id = $rows["sl_activity_id"];
$sl_other_cost_id = $rows["sl_other_cost_id"];
$sl_staff_id = $rows["sl_staff_id"];
$sl_benefits_id = $rows["sl_benefits_id"];
$sl_vehicle_id = $rows["sl_vehicle_id"];
$sl_equipment_id = $rows["sl_equipment_id"];
$sl_item_id = $rows["sl_item_id"];
$sl_added_by = $rows["sl_added_by"];
$sl_date_added = $rows["sl_date_added"];

if ($sl_budget_id == 1 ) { $sl_budget_id = "Y"; } else { $sl_budget_id = "N"; }
if ($sl_donor_id == 1 ) { $sl_donor_id = "Y"; } else { $sl_donor_id = "N"; }
if ($sl_component_id == 1 ) { $sl_component_id = "Y"; } else { $sl_component_id = "N"; }
if ($sl_activity_id == 1 ) { $sl_activity_id = "Y"; } else { $sl_activity_id = "N"; }
if ($sl_other_cost_id == 1 ) { $sl_other_cost_id = "Y"; } else { $sl_other_cost_id = "N"; }
if ($sl_staff_id == 1 ) { $sl_staff_id = "Y"; } else { $sl_staff_id = "N"; }
if ($sl_benefits_id == 1 ) { $sl_benefits_id = "Y"; } else { $sl_benefits_id = "N"; }
if ($sl_vehicle_id == 1 ) { $sl_vehicle_id = "Y"; } else { $sl_vehicle_id = "N"; }
if ($sl_equipment_id == 1 ) { $sl_equipment_id = "Y"; } else { $sl_equipment_id = "N"; }
if ($sl_item_id == 1 ) { $sl_item_id = "Y"; } else { $sl_item_id = "N"; }

$sl_starting_balance = numberFormat($sl_starting_balance);
$sl_ending_balance = numberFormat($sl_ending_balance);	

if ($id == $record_id && $sl_code == md5($sl_account_code))
{
?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="subsidiary_ledger" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th colspan="2" scope="col" class="rounded-header"><div class="main" align="left"><strong>Subsidiary Ledger Details</strong></div></th>
							<th colspan="2" scope="col" class="rounded-q4">&nbsp;</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="2" class="rounded-foot-left" align="left">&nbsp;</td>
							<td colspan="2" class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td width="20%"><b>Account Code</b></td>
							<td width="30%"><?php echo $sl_account_code; ?></td>
							<td width="20%"><b>Account Title</b></td>
							<td width="30%"><?php echo $sl_account_title; ?></td>
						</tr> 						
						<tr valign="top">
					  		<td colspan="4">
								<table id="rounded-add-entries" align="center">
									<thead>
										<tr>
											<th colspan="5" scope="col" class="rounded-header"><div class="main" align="left"><strong>Report Details</strong></div></th>
											<th colspan="5" scope="col" class="rounded-q4">&nbsp;</th>	
										</tr>
									</thead>
									<tbody>
										<tr>
											<td align="center">Budget ID</td>
											<td align="center">Donor ID</td>
											<td align="center">Component ID</td>
											<td align="center">Activity ID</td>
											<td align="center">Other Cost/Services ID</td>
											<td align="center">Staff ID</td>
											<td align="center">Benefits ID</td>
											<td align="center">Vehicle ID</td>
											<td align="center">Equipment ID</td>
                                            <td align="center">Item ID</td>
										</tr>
										<tr>
											<td align="center"><?php echo $sl_budget_id; ?></td>
											<td align="center"><?php echo $sl_donor_id; ?></td>
											<td align="center"><?php echo $sl_component_id; ?></td>
											<td align="center"><?php echo $sl_activity_id; ?></td>
											<td align="center"><?php echo $sl_other_cost_id; ?></td>
											<td align="center"><?php echo $sl_staff_id; ?></td>
											<td align="center"><?php echo $sl_benefits_id; ?></td>
											<td align="center"><?php echo $sl_vehicle_id; ?></td>
											<td align="center"><?php echo $sl_equipment_id; ?></td>
                                            <td align="center"><?php echo $sl_item_id; ?></td>
										</tr>										
									</tbody>
								</table>							</td>			  
					  	</tr>
						<tr valign="top">
					  		<td colspan="4">
								<table id="rounded-add-entries" align="center">
									<thead>
										<tr>
											<th scope="col" class="rounded-header" align="center"><b>Year</b></th>
											<th scope="col" class="rounded-q1" align="center"><b>Currency Code</b></th>
											<th scope="col" class="rounded-q1" align="center"><b>Starting Balance</b></th>
											<th scope="col" class="rounded-q4" align="center"><b>Ending Balance</b></th>	
										</tr>
									</thead>
									<tbody>
									<?php
									$sql = "SELECT 	SQL_BUFFER_RESULT
													SQL_CACHE
													sl_details_year, 
													sl_details_currency_code,
													sl_details_starting_balance, 
													sl_details_ending_balance 
											FROM  	tbl_subsidiary_ledger_details
											WHERE 	sl_details_reference_number = '$record_id'
											ORDER BY sl_details_year DESC";
									$rs = mysql_query($sql) or die("Error in sql in module: subsidiary.ledger.details.php ".$sql." ".mysql_error());
									$total_rows = mysql_num_rows($rs);	
									for($row_number=0;$row_number<$total_rows;$row_number++)
									{
										$rows = mysql_fetch_array($rs);		
										$sl_details_year = $rows["sl_details_year"];
										$sl_details_currency_code = $rows["sl_details_currency_code"];
										$sl_starting_balance = $rows["sl_details_starting_balance"];
										$sl_ending_balance = $rows["sl_details_ending_balance"];
										$sl_details_currency_code = getConfigurationValueById($sl_details_currency_code);								
									?>
										<tr>
											<td align="center"><?php echo $sl_details_year; ?></td>
											<td align="center"><?php echo $sl_details_currency_code; ?></td>
											<td align="center"><?php echo numberFormat($sl_starting_balance); ?></td>
											<td align="center"><?php echo numberFormat($sl_ending_balance); ?></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>							</td>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: activity.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
?>