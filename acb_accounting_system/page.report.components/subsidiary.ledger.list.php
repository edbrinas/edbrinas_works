<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$page_size = getConfigurationValueByName('max_record_per_page');

$record_id = $_GET['id'];
$action = $_GET['action'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$search_for = $_GET['search_for'];
$search_by = $_GET['search_by'];
$current_year = date("Y");
$delete_record = $_GET['delete_record'];

if($order=="")
{
  	$order = "sl_id";
}
if ($get_msg==1)
{
	$msg = "Record updated successfully!";
	$display_msg = 1;
}

if($cur_page=="")
{
   	$cur_page= 1;
}
if($cur_page==1)
{
	$count_page = 0;
}
else
{
  	$count_page = (($cur_page - 1) * $page_size);
}


if ($record_id != "" && $action == "confirm_delete")
{
?>
	<script type="text/javascript">
	var flg=confirm('Are you sure you want to delete this record?\n\nClick OK to continue. Otherwise click Cancel.\n');
	if (flg==true)
	{
		

		window.location = '/workspaceGOP/page.report.components/subsidiary.ledger.list.php?id=<?php echo $record_id; ?>&delete_record=1&action=delete'
	}
	else
	{
		window.location = '/workspaceGOP/page.report.components/subsidiary.ledger.list.php'
	}
	</script> 
    <?php
}

if ($delete_record==1 && $record_id != "" && $action == "delete")
{
	
	$sql = "DELETE FROM tbl_subsidiary_ledger
			WHERE 		sl_id = '$record_id'";
	mysql_query($sql) or die("Error in module: sl.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);
	
	$msg = "Record deleted successfully!";
	$display_msg = 1;
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main" style='table-layout:fixed'>
<tr>
	<td>
		<form method="get">
			<table id="rounded-search-table">
				<thead>
					<tr>
						<th class="rounded-header"><div class="whitelabel">Search</div></th>
						<th class="rounded-q4">&nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="rounded-foot-left"><input type="submit" name="action" value="Search" class="formbutton"></td>
						<td class="rounded-foot-right" align="right">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td><input name="search_for" type="text" id="search_for" class="formbutton" value="<?php echo $search_for; ?>"></td>
						<td>
							<select name="search_by" class="formbutton">
							  <option value="ALL" selected>ALL</option>
							  <option value="sl_account_code" <?php if ($search_by == 'sl_account_code') { echo "selected"; } ?> >Account Code</option>
							  <option value="sl_account_title" <?php if ($search_by == 'sl_account_title') { echo "selected"; } ?> >Account Title</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries" width="100%">
		<?php
			$search_for = trim($search_for);
			if ($action == "Search" && $search_for != "" && $search_by != "")
			{
				if ($search_for != "" && ($search_by != "" && $search_by != "ALL"))	
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										sl_id,
										sl_account_code,
										sl_account_title,
										sl_budget_id,
										sl_donor_id,
										sl_component_id,
										sl_activity_id,
										sl_other_cost_id,
										sl_staff_id,
										sl_benefits_id,
										sl_vehicle_id,
										sl_equipment_id,
										sl_item_id,
										sl_added_by,
										sl_date_added
								FROM 	tbl_subsidiary_ledger
								WHERE 	".$search_by." LIKE '%".$search_for."%'
								ORDER BY sl_account_title ASC";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										sl_id,
										sl_account_code,
										sl_account_title,
										sl_budget_id,
										sl_donor_id,
										sl_component_id,
										sl_activity_id,
										sl_other_cost_id,
										sl_staff_id,
										sl_benefits_id,
										sl_vehicle_id,
										sl_equipment_id,
										sl_item_id,
										sl_added_by,
										sl_date_added
								FROM 	tbl_subsidiary_ledger
								WHERE 	".$search_by." LIKE '%".$search_for."%'
								ORDER BY sl_account_title ASC 
								LIMIT	$count_page, $page_size";	
				}
				if ($search_for == "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										sl_id,
										sl_account_code,
										sl_account_title,
										sl_budget_id,
										sl_donor_id,
										sl_component_id,
										sl_activity_id,
										sl_other_cost_id,
										sl_staff_id,
										sl_benefits_id,
										sl_vehicle_id,
										sl_equipment_id,
										sl_item_id,
										sl_added_by,
										sl_date_added
								FROM 	tbl_subsidiary_ledger
								ORDER BY sl_account_title ASC";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										sl_id,
										sl_account_code,
										sl_account_title,
										sl_budget_id,
										sl_donor_id,
										sl_component_id,
										sl_activity_id,
										sl_other_cost_id,
										sl_staff_id,
										sl_benefits_id,
										sl_vehicle_id,
										sl_equipment_id,
										sl_item_id,
										sl_added_by,
										sl_date_added
								FROM 	tbl_subsidiary_ledger
								ORDER BY sl_account_title ASC 
								LIMIT	$count_page, $page_size";		
				}
				if ($search_for != "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										sl_id,
										sl_account_code,
										sl_account_title,
										sl_budget_id,
										sl_donor_id,
										sl_component_id,
										sl_activity_id,
										sl_other_cost_id,
										sl_staff_id,
										sl_benefits_id,
										sl_vehicle_id,
										sl_equipment_id,
										sl_item_id,
										sl_added_by,
										sl_date_added
								FROM 	tbl_subsidiary_ledger
								WHERE	sl_account_code LIKE '%".$search_for."%'
								OR		sl_account_title LIKE '%".$search_for."%'
								ORDER BY sl_account_title ASC";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										sl_id,
										sl_account_code,
										sl_account_title,
										sl_budget_id,
										sl_donor_id,
										sl_component_id,
										sl_activity_id,
										sl_other_cost_id,
										sl_staff_id,
										sl_benefits_id,
										sl_vehicle_id,
										sl_equipment_id,
										sl_item_id,
										sl_added_by,
										sl_date_added
								FROM 	tbl_subsidiary_ledger
								WHERE	sl_account_code LIKE '%".$search_for."%'
								OR		sl_account_title LIKE '%".$search_for."%'
								ORDER BY sl_account_title ASC 
								LIMIT	$count_page, $page_size";
				}
			}
			else
			{
				$sql1 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									sl_id,
									sl_account_code,
									sl_account_title,
									sl_budget_id,
									sl_donor_id,
									sl_component_id,
									sl_activity_id,
									sl_other_cost_id,
									sl_staff_id,
									sl_benefits_id,
									sl_vehicle_id,
									sl_equipment_id,
									sl_item_id,
									sl_added_by,
									sl_date_added
							FROM 	tbl_subsidiary_ledger
							ORDER BY sl_account_title ASC";

				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									sl_id,
									sl_account_code,
									sl_account_title,
									sl_budget_id,
									sl_donor_id,
									sl_component_id,
									sl_activity_id,
									sl_other_cost_id,
									sl_staff_id,
									sl_benefits_id,
									sl_vehicle_id,
									sl_equipment_id,
									sl_item_id,
									sl_added_by,
									sl_date_added
							FROM 	tbl_subsidiary_ledger
							ORDER BY sl_account_title ASC 
							LIMIT	$count_page, $page_size";
			}	
			$rs1 = mysql_query($sql1) or die("Error in sql1 in module: sl.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs1);
			$total_pages = ceil($total_records/$page_size); 
			$rs2 = mysql_query($sql2) or die("Error in sql2 in module: sl.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs2);	
			?>
			<thead>
				<tr>
					<th class="rounded-header" scope="col"><a href="<?php echo $page; ?>?order=sl_account_code" class="whitelabel">Account Title</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_account_title" class="whitelabel">Account Code</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_budget_id" class="whitelabel">Budget ID</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_donor_id" class="whitelabel">Donor ID</a></th>	
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_component_id" class="whitelabel">Component ID</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_activity_id" class="whitelabel">Activity ID</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_other_cost_id" class="whitelabel">Other Cost/ Services ID</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_staff_id" class="whitelabel">Staff ID</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_benefits_id" class="whitelabel">Benefits ID</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_vehicle_id" class="whitelabel">Vehicle ID</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_equipment_id" class="whitelabel">Equipment ID</a></th>
                    <th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_item_id" class="whitelabel">Item ID</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_added_by" class="whitelabel">Added By</a></th>
					<th class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=sl_date_added" class="whitelabel">Date Added</a></th>
				  <th class="rounded-q4" scope="col"><div class="whitelabel">Options</div></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3" class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td colspan="12" class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							
							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>		
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="15"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="15"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{

				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs2);
					$id = $rows["sl_id"];
					$sl_account_code = $rows["sl_account_code"];
					$sl_account_title = $rows["sl_account_title"];
					$sl_budget_id = $rows["sl_budget_id"];
					$sl_donor_id = $rows["sl_donor_id"];
					$sl_component_id = $rows["sl_component_id"];
					$sl_activity_id = $rows["sl_activity_id"];
					$sl_other_cost_id = $rows["sl_other_cost_id"];
					$sl_staff_id = $rows["sl_staff_id"];
					$sl_benefits_id = $rows["sl_benefits_id"];
					$sl_vehicle_id = $rows["sl_vehicle_id"];
					$sl_equipment_id = $rows["sl_equipment_id"];
					$sl_item_id = $rows["sl_item_id"];
					$sl_added_by = $rows["sl_added_by"];
					$sl_date_added = $rows["sl_date_added"];
					
					$sl_starting_balance = numberFormat($sl_starting_balance);
					$sl_ending_balance = numberFormat($sl_ending_balance);
					
					if ($sl_budget_id == 1 ) { $sl_budget_id = "Y"; } else { $sl_budget_id = "N"; }
					if ($sl_donor_id == 1 ) { $sl_donor_id = "Y"; } else { $sl_donor_id = "N"; }
					if ($sl_component_id == 1 ) { $sl_component_id = "Y"; } else { $sl_component_id = "N"; }
					if ($sl_activity_id == 1 ) { $sl_activity_id = "Y"; } else { $sl_activity_id = "N"; }
					if ($sl_other_cost_id == 1 ) { $sl_other_cost_id = "Y"; } else { $sl_other_cost_id = "N"; }
					if ($sl_staff_id == 1 ) { $sl_staff_id = "Y"; } else { $sl_staff_id = "N"; }
					if ($sl_benefits_id == 1 ) { $sl_benefits_id = "Y"; } else { $sl_benefits_id = "N"; }
					if ($sl_vehicle_id == 1 ) { $sl_vehicle_id = "Y"; } else { $sl_vehicle_id = "N"; }
					if ($sl_equipment_id == 1 ) { $sl_equipment_id = "Y"; } else { $sl_equipment_id = "N"; }
					if ($sl_item_id == 1 ) { $sl_item_id = "Y"; } else { $sl_item_id = "N"; }
					
					$sl_added_by = getFullName($sl_added_by,3);
				?>      
				 <tr>
					<td><?php echo $sl_account_code; ?>&nbsp;</td>
					<td><?php echo $sl_account_title; ?>&nbsp;</td>
					<td><?php echo $sl_budget_id; ?>&nbsp;</td>
					<td><?php echo $sl_donor_id; ?>&nbsp;</td>
					<td><?php echo $sl_component_id; ?>&nbsp;</td>
					<td><?php echo $sl_activity_id; ?>&nbsp;</td>
					<td><?php echo $sl_other_cost_id; ?>&nbsp;</td>
					<td><?php echo $sl_staff_id; ?>&nbsp;</td>
					<td><?php echo $sl_benefits_id; ?>&nbsp;</td>
					<td><?php echo $sl_vehicle_id; ?>&nbsp;</td>
					<td><?php echo $sl_equipment_id; ?>&nbsp;</td>
					<td><?php echo $sl_item_id; ?>&nbsp;</td>
                    <td><?php echo $sl_added_by; ?>&nbsp;</td>
					<td><?php echo $sl_date_added; ?>&nbsp;</td>
					<td>
								<a href="javascript:void(0);" NAME="View Record" title="View Record" onClick=window.open("subsidiary.ledger.details.php?id=<?php echo encrypt($id); ?>&sl_account_code=<?php echo md5($sl_account_code); ?>","Ratting","toolbar=no,resizable=no,scrollbars=yes,height=650,width=1024");>[View]</a>		
						  <?php
						  if ($_SESSION["level"]==1 || $_SESSION["level"]==5)
						  {
						  ?>
							<br>	<a href="subsidiary.ledger.edit.php?id=<?php echo encrypt($id); ?>">[Edit Reports Included]</a>
							<br>	<a href="subsidiary.ledger.budget.edit.php?id=<?php echo encrypt($id); ?>">[Edit Assigned Budget]</a>
						  <?php
						  }
						  ?>							
						  <?php
						  if ($_SESSION["level"]==1)
						  {
						  ?>
							<br>	<a href="subsidiary.ledger.list.php?id=<?php echo $id; ?>&action=confirm_delete">[Delete Record]</a>							
						  <?php
						  }
						  ?>
					</td>
				 </tr>
				<?php 
				} 
				?>
				
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	
	</td>
</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>