<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$id = $_GET['id'];
$id = decrypt($id);

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);

$startYear = date("Y") - 1;
$endYear = date("Y") + 2;
$date_time_inserted = date("Y-m-d H:i:s");

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				forex_id,
				forex_currency_code,
				forex_transaction_date,
				forex_currency_value
		FROM	tbl_forex
		WHERE	forex_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: forex.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["forex_id"];
$forex_currency_code = $rows["forex_currency_code"];
$forex_transaction_date = $rows["forex_transaction_date"];
$forex_currency_value = $rows["forex_currency_value"];

if ($forex_currency_code==16) { $forex_currency_value = $forex_currency_value; }
if ($forex_currency_code==17) { $forex_currency_value = $forex_currency_value; }
if ($forex_currency_code==18) { $forex_currency_value = $forex_currency_value; }

$exploded_date = explode("-",$forex_transaction_date);
$year = $exploded_date[1];
$month = $exploded_date[0];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$record_id = $_POST["record_id"];
	$forex_currency_code = $_POST["forex_currency_code"];
	$forex_transaction_date = $_POST["forex_transaction_date"];
	$forex_currency_value = $_POST["forex_currency_value"];
	$forex_year = $_POST['forex_year'];
	$forex_month = $_POST['forex_month'];
	$forex_month = sprintf("%02d",$forex_month);
	$forex_transaction_date = $forex_month."-".$forex_year;
	
	if ($forex_currency_code == "") { $display_msg = 1; $msg .= "Please enter Type of Currency"; $msg .= "<br>"; }
	if ($forex_currency_value == "") { $display_msg = 1; $msg .= "Please enter Currency Rate"; $msg .= "<br>"; }
	if (checkIfNumber($forex_currency_value) == 0) { $display_msg = 1; $msg .= "Currency Rate must be a NUMBER"; $msg .= "<br>"; }
	
	if ($msg == "")
	{
		$sql = "UPDATE 	tbl_forex
				SET		forex_currency_code = '".trim($forex_currency_code)."',
						forex_transaction_date = '".trim($forex_transaction_date)."',
						forex_currency_value = '".trim($forex_currency_value)."',
						forex_inserted_by = $user_id,
						forex_date_time_inserted = '$date_time_inserted'
				WHERE	forex_id = '$record_id'";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		$display_msg = 1;
		header("location:/workspaceGOP/page.report.components/forex.list.php?msg=2");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="forex" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Forex Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Type of rate  * </b></td>
							<td width="80%">
								<?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_id,
													cfg_value,
													cfg_description
											FROM 	tbl_config
											WHERE 	cfg_name = 'exchange_rate_denomination'";
									$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
									$str .= "<select name='forex_currency_code' class='formbutton'>";
									while($data=mysql_fetch_array($rs))
									{
										$column_id = $data['cfg_id'];
										$column_name = $data['cfg_value'];
										$str .= "<option value='$column_id'";
										if ($forex_currency_code == $column_id )
										{
											$str .= " SELECTED";
										}
										$str .= ">".$column_name."";
									}
									$str .= "</select>&nbsp;";
									echo $str;
								?>							
							</td>
						</tr>
						<tr valign="top">
						  <td><b>Trading Date</b></td>
						  <td>
							<select name='forex_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
								?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='forex_month' class="formbutton">
                              <?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
                              <option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
										?>
										 selected="selected"
										 <?
									}
									?>
									><?php echo $months[$mo-1]; ?>
                              <?php
								}
								?>
                              </option>
                            </select></td>
					  </tr>
						<tr valign="top">
							<td><b>Currency Rate* </b></td>
							<td><input name="forex_currency_value" type="text" id="forex_currency_value" value="<?php echo $forex_currency_value; ?>" class="formbutton"></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>