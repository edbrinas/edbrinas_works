<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

if ($_POST['Submit'] == 'Delete Field')
{
	$sql = "DELETE FROM tbl_report_category
			WHERE	(report_id = '".$_POST['report_field_id']."' OR report_field_id = '".$_POST['report_field_id']."')
			AND		report_code = '".$_POST['report_code']."'";
	mysql_query($sql) or die($sql.mysql_error());	
	insertEventLog($user_id,$sql);	

	$_POST['report_field_id'] = "";
	$_POST['report_type'] = "";
	$_POST['report_name'] = "";
	$_POST['report_account_code'] = "";
	$_POST['report_is_negative'] = "";

	$display_msg = 1; 
	$msg .= "Field Deleted Successfully!";	
}
elseif ($_POST['Submit'] == 'Back')
{
	header("location: /workspaceGOP/page.report.components/reports.index.php");
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="manage_report" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Report Details</strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>&nbsp;</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="hidden" name="report_code" value="<?php echo decrypt($_GET['report_code']); ?>" /></td>
							<td class="rounded-foot-right" align="right">
                            <input type="submit" name="Submit" value="Back" class="formbutton">
                            <input type="submit" name="Submit" value="Delete Field" class="formbutton">
                            </td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr valign="top">
							<td><b>Parent Field</b></td>
							<td>
							<select name="report_field_id">
								<option value="0">-- Set As Parent Field --</option>
                                <?php
								$sql = "SELECT 	report_id,
												report_name
										FROM 	tbl_report_category 
										WHERE 	report_field_id = 0
										AND		report_code = '".decrypt($_GET['report_code'])."'";
										
								$rs = mysql_query($sql) or die($sql.mysql_error());
								while($rows = mysql_fetch_array($rs))
								{
									?>
									<option value="<?php echo $rows['report_id']; ?>" <?php if ($_POST['report_field_id']==$rows['report_id']) { echo "SELECTED"; } ?>><?php echo "Main Field: ".$rows['report_name']; ?></option>
									<?php
                                    $sql2 = "SELECT report_id,
													report_sl_id
                                              FROM	tbl_report_category
                                              WHERE	report_field_id = '".$rows['report_id']."'";
                                    $rs2 = mysql_query($sql2) or die($sql2.mysql_error());
                                    while($rows2 = mysql_fetch_array($rs2))
                                    {		
                                        ?>
                                        <option value="<?php echo $rows2['report_id']; ?>">
                                        <?php
                                        echo "<br>&nbsp;&nbsp;&raquo;&nbsp;".getSubsidiaryLedgerAccountTitleById($rows2['report_sl_id']);
                                        ?>
                                        </option>                                            
                                        <?php
                                    }
								}
								?>
							</select>                  
                            </td>
						</tr>	
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>