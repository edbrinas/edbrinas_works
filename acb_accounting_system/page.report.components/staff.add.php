<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

if ($_POST['Submit'] == 'Save')
{
	$msg = "";
	
	$staff_name = $_POST["staff_name"];
	$staff_description = $_POST["staff_description"];
	
	if ($staff_name == "") { $display_msg = 1; $msg .= "Please enter Staff Name"; $msg .= "<br>"; }
	if ($staff_description == "") { $display_msg = 1; $msg .= "Please select Staff Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$staff_name = addslashes($staff_name);
		$staff_description = addslashes($staff_description);
		
		$sql = "INSERT INTO tbl_staff
							(
							staff_name,
							staff_description
							)
				VALUES
							(
							'".trim($staff_name)."',
							'".trim($staff_description)."'
							)";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		$display_msg = 1;
		$msg = "Record inserted successfully!";
		
		$staff_name = "";
		$staff_description = "";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="staff" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Staff Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Save" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Staff Name* </b></td>
							<td width="80%"><label><input name="staff_name" type="text" class="formbutton" id="staff_name" size="80" value="<?php echo $staff_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Staff Description </b></td>
							<td><label><textarea name="staff_description" cols="50" rows="10" class="formbutton"><?php echo $staff_description; ?></textarea></label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>