<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$rec_id = $_GET['id'];
$a_name = $_GET['bank_name'];

$sql = "	SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE				
					bank_id,
					bank_name,
					bank_address,
					bank_account_name,
					bank_account_number,
					bank_account_account_type,
					bank_account_sl_code,
					bank_signed_cheque,
					bank_deposit_in_transit,
					bank_outstanding_cheque,				
					bank_inserted_by,
					bank_date_inserted
			FROM 	tbl_bank
			WHERE	bank_id = '$rec_id'";
$rs = mysql_query($sql) or die("Error in sql in module: bank.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);
$rec_id = $rows["bank_id"];
$bank_name = $rows["bank_name"];
$bank_address = $rows["bank_address"];
$bank_account_name = $rows["bank_account_name"];
$bank_account_number = $rows["bank_account_number"];
$bank_account_account_type = $rows["bank_account_account_type"];
$bank_account_sl_code = $rows["bank_account_sl_code"];
$bank_signed_cheque = $rows["bank_signed_cheque"];
$bank_deposit_in_transit = $rows["bank_deposit_in_transit"];
$bank_outstanding_cheque = $rows["bank_outstanding_cheque"];
$bank_account_type = getConfigurationValueById($bank_account_account_type);
$bank_account_sl_code = getSubsidiaryLedgerAccountTitleById($bank_account_sl_code);
$bank_inserted_by = getFullName($bank_inserted_by,3);

$bank_account_starting_balance = numberFormat($bank_account_starting_balance);
$bank_account_ending_balance = numberFormat($bank_account_ending_balance);

if ($bank_signed_cheque==1) { $bank_signed_cheque = "Yes"; } else { $bank_signed_cheque = "Field not added"; }
if ($bank_deposit_in_transit==1) { $bank_deposit_in_transit = "Yes"; } else { $bank_deposit_in_transit = "Field not added"; }
if ($bank_outstanding_cheque==1) { $bank_outstanding_cheque = "Yes"; } else { $bank_outstanding_cheque = "Field not added"; }

if ($id == $record_id && $a_name == md5($bank_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Bank Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Bank Name</b></td>
			<td width="80%"><?php echo $bank_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Account Name</b></td>
			<td><?php echo $bank_account_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Bank Address</b></td>
			<td><?php echo $bank_address; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Account Number</b></td>
			<td><?php echo $bank_account_number; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Account Type</b></td>
			<td><?php echo $bank_account_type; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>SL Code</b></td>
			<td><?php echo $bank_account_sl_code; ?>&nbsp;</td>
		</tr>	
		<tr>
		  <td colspan="2"><label>
		  <div align="center">The following fields are included in Bank Reconciliation</div>
		  </label></td>
		</tr>	
		<tr>
		  <td><strong>Signed disbursement vouchers for cheque preparation * </strong></td>
		  <td><?php echo $bank_signed_cheque; ?></td>
		</tr>
		<tr>
		  <td><strong>Deposit in transit * </strong></td>
		  <td><?php echo $bank_deposit_in_transit; ?></td>
		</tr>		
		<tr>
		  <td><strong>Outstanding cheque * </strong></td>
		  <td><?php echo $bank_outstanding_cheque; ?></td>
		</tr>									
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: activity.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
?>