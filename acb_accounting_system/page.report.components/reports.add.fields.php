<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

if ($_POST['Submit'] == 'Add Field')
{
	$msg = "";

	$sql = "SELECT 	COUNT(report_id) AS record_count
			FROM	tbl_report_category
			WHERE	report_field_id = '".trim($_POST['report_field_id'])."'
			AND		report_type = '".trim($_POST['report_type'])."'
			AND		report_code = '".trim($_POST['report_code'])."'
			AND		report_sl_id = '".trim($_POST['report_account_code'])."'";
	if ($_POST['report_name']!="")
	{
		$sql .= " AND report_name LIKE '".trim($_POST['report_name'])."'";
	}
	$rs = mysql_query($sql) or die("SQL ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	if ($rows["record_count"]!=0) { $display_msg = 1; $msg .= "Record already exists!"; $msg .= "<br>"; }
	if ($_POST['report_field_id']==0 && $_POST['report_name']=="") { $display_msg = 1; $msg .= "Pleae enter valid field name."; $msg .= "<br>"; }
	if ($_POST['report_field_id']=="INVALID") { $display_msg = 1; $msg .= "Unable to Add Parent Field, please select Main Field or Set As Parent Field Only."; $msg .= "<br>"; }

	
	if ($msg=="")
	{
		$sql = "INSERT INTO tbl_report_category
							(
							report_field_id,
							report_type,
							report_code,
							report_name,
							report_sl_id,
							report_is_negative)
				VALUES
							(
							'".trim($_POST['report_field_id'])."',
							'".trim($_POST['report_type'])."',
							'".trim($_POST['report_code'])."',
							'".trim($_POST['report_name'])."',
							'".trim($_POST['report_account_code'])."',
							'".trim($_POST['report_is_negative'])."'
							)";
		mysql_query($sql) or die($sql.mysql_error());	
		insertEventLog($user_id,$sql);
		
		$display_msg = 1;
		$msg = "Record inserted successfully!";
		
		if ($_POST['report_field_id']==0)
		{
			$_POST['report_field_id'] = "";
			$_POST['report_type'] = "";
			$_POST['report_name'] = "";
			$_POST['report_account_code'] = "";
			$_POST['report_is_negative'] = "";
		}	
	}
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="manage_report" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Report Details</strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>&nbsp;</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="hidden" name="report_code" value="<?php echo decrypt($_GET['report_code']); ?>" /></td>
							<td class="rounded-foot-right" align="right">
                            <input type="submit" name="Submit" value="Add Field" class="formbutton">
                            </td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr valign="top">
							<td><b>Parent Field</b></td>
							<td>
							<select name="report_field_id">
								<option value="0">-- Set As Parent Field --</option>
                                <?php
								$sql = "SELECT 	report_id,
												report_name
										FROM 	tbl_report_category 
										WHERE 	report_field_id = 0
										AND		report_code = '".decrypt($_GET['report_code'])."'";
								$rs = mysql_query($sql) or die($sql.mysql_error());
								while($rows = mysql_fetch_array($rs))
								{
									?>
									<option value="<?php echo $rows['report_id']; ?>" <?php if ($_POST['report_field_id']==$rows['report_id']) { echo "SELECTED"; } ?>><?php echo "Main Field: ".$rows['report_name']; ?></option>
									<?php
                                    $sql2 = "SELECT report_id,
													report_sl_id
                                              FROM	tbl_report_category
                                              WHERE	report_field_id = '".$rows['report_id']."'";
                                    $rs2 = mysql_query($sql2) or die($sql2.mysql_error());
                                    while($rows2 = mysql_fetch_array($rs2))
                                    {		
                                        ?>
                                        <option value="INVALID">
                                        <?php
                                        echo "<br>&nbsp;&nbsp;&raquo;&nbsp;".getSubsidiaryLedgerAccountTitleById($rows2['report_sl_id']);
                                        ?>
                                        </option>                                            
                                        <?php
                                    }
								}
								?>
							</select>                  
                            </td>
						</tr>	
						<tr valign="top">
							<td><b>Account Code</b></td>
							<td>
                            <select name="report_account_code">
                            <option value="0">[Select Account Code]</option>
                            <?php
							$sql = "SELECT 	sl_id,
											sl_account_code,
											sl_account_title
									FROM	tbl_subsidiary_ledger
									ORDER BY sl_account_code ASC";
							$rs = mysql_query($sql) or die($sql.mysql_error());
							while($rows = mysql_fetch_array($rs))
							{
								?>
                                <option value="<?php echo $rows['sl_id']; ?>" <?php if ($_POST['report_account_code']==$rows['sl_id']) { echo "SELECTED"; } ?>>
                                [<?php echo $rows['sl_account_code']; ?>]&nbsp;<?php echo $rows['sl_account_title']; ?>
                                </option>
                                <?php
							}
							?>
                            </select>
                            </td>
						</tr>                      
						<tr>
							<td width="20%"><b>Field Name</b></td>
							<td width="80%"><label><input name="report_name" type="text" class="formbutton" size="80" value="<?php echo $report_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Field Type</b></td>
							<td>
                            <select name="report_type">
                            <?php
							$sql = "SELECT 	cfg_id,
											cfg_value
									FROM	tbl_config
									WHERE	cfg_name = 'report_type'";
							$rs = mysql_query($sql) or die($sql.mysql_error());
							while($rows = mysql_fetch_array($rs))
							{
								?>
                                <option value="<?php echo $rows['cfg_id']; ?>" <?php if ($_POST['report_type']==$rows['cfg_id']) { echo "SELECTED"; } ?>><?php echo $rows['cfg_value']; ?></option>
                                <?php
							}
							?>
                            </select>
                            </td>
						</tr>
						<tr valign="top">
							<td><b>Value must be negative?</b></td>
							<td>
							<input type="checkbox" value="1" name="report_is_negative" <?php if ($_POST['report_is_negative']==1) { echo "CHECKED"; } ?>/> * check if yes
                            </td>
						</tr>                                                																															
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>