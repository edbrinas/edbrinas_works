<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$c_name = $_GET['component_name'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				component_id,
				component_name,
				component_description
		FROM	tbl_component
		WHERE	component_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: component.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["component_id"];
$component_name = $rows["component_name"];
$component_description = $rows["component_description"];

if ($id == $record_id && $c_name == md5($component_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Component Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Component Name</b></td>
			<td width="80%"><?php echo $component_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Component Description</b></td>
			<td><?php echo $component_description; ?>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: component.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
