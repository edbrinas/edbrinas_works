<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$page_size = getConfigurationValueByName('max_record_per_page');

$record_id = $_GET['id'];
$action = $_GET['action'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$search_for = $_GET['search_for'];
$search_by = $_GET['search_by'];
$delete_record = $_GET['delete_record'];

if($order=="")
{
  	$order = "activity_name";
}
if ($get_msg==1)
{
	$msg = "Record updated successfully!";
	$display_msg = 1;
}

if($cur_page=="")
{
   	$cur_page= 1;
}
if($cur_page==1)
{
	$count_page = 0;
}
else
{
  	$count_page = (($cur_page - 1) * $page_size);
}


if ($record_id != "" && $action == "confirm_delete")
{
?>
	<script type="text/javascript">
	var flg=confirm('Are you sure you want to delete this record?\n\nClick OK to continue. Otherwise click Cancel.\n');
	if (flg==true)
	{
		

		window.location = '/workspaceGOP/page.report.components/activity.list.php?id=<?php echo $record_id; ?>&delete_record=1&action=delete'
	}
	else
	{
		window.location = '/workspaceGOP/page.report.components/activity.list.php'
	}
	</script> 
    <?php
}

if ($delete_record==1 && $record_id != "" && $action == "delete")
{
	$sql = "DELETE FROM tbl_activity
			WHERE  activity_id = '$record_id'";
	mysql_query($sql) or die("Error in module: activity.list.php ".$sql." ".mysql_error());
			
	insertEventLog($user_id,$sql);
	$msg = "Record deleted successfully!";
	$display_msg = 1;
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
<tr>
	<td>
		<form method="get">
			<table id="rounded-search-table">
				<thead>
					<tr>
						<th class="rounded-header"><div class="whitelabel">Search</div></th>
						<th class="rounded-q4">&nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="rounded-foot-left"><input type="submit" name="action" value="Search" class="formbutton"></td>
						<td class="rounded-foot-right" align="right">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td><input name="search_for" type="text" id="search_for" class="formbutton" value="<?php echo $search_for; ?>"></td>
						<td>
							<select name="search_by" class="formbutton">
							  <option value="ALL" selected>ALL</option>
							  <option value="activity_name" <?php if ($search_by == 'activity_name') { echo "selected"; } ?> >Activity Name</option>
							  <option value="activity_description" <?php if ($search_by == 'activity_description') { echo "selected"; } ?> >Activity Description</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
			if ($action == "Search" && $search_for != "" && $search_by != "")
			{
				if ($search_for != "" && ($search_by != "" && $search_by != "ALL"))	
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										activity_id,
										activity_name,
										activity_description
								FROM 	tbl_activity
								WHERE 	".$search_by." LIKE '%".$search_for."%'
								ORDER BY $order ASC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										activity_id,
										activity_name,
										activity_description
								FROM 	tbl_activity
								WHERE 	".$search_by." LIKE '%".$search_for."%'
								ORDER BY $order ASC 
								LIMIT	$count_page, $page_size";	
				}
				if ($search_for == "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										activity_id,
										activity_name,
										activity_description
								FROM 	tbl_activity
								ORDER BY $order ASC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										activity_id,
										activity_name,
										activity_description
								FROM 	tbl_activity
								ORDER BY $order ASC 
								LIMIT	$count_page, $page_size";		
				}
				if ($search_for != "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										activity_id,
										activity_name,
										activity_description
								FROM 	tbl_activity
								WHERE	activity_name LIKE '%".$search_for."%'
								OR		activity_description LIKE '%".$search_for."%'
								ORDER BY $order ASC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										activity_id,
										activity_name,
										activity_description
								FROM 	tbl_activity
								WHERE	activity_name LIKE '%".$search_for."%'
								OR		activity_description LIKE '%".$search_for."%'
								ORDER BY $order ASC 
								LIMIT	$count_page, $page_size";
				}
			}
			else
			{
				$sql1 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									activity_id,
									activity_name,
									activity_description
							FROM 	tbl_activity
							ORDER BY $order ASC ";

				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									activity_id,
									activity_name,
									activity_description
							FROM 	tbl_activity
							ORDER BY $order ASC 
							LIMIT	$count_page, $page_size";
			}	
			$rs1 = mysql_query($sql1) or die("Error in sql1 in module: activity.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs1);
			$total_pages = ceil($total_records/$page_size); 
			$rs2 = mysql_query($sql2) or die("Error in sql2 in module: activity.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs2);	
			?>
			<thead>
				<tr>
					<th scope="col" class="rounded-header"><a href="<?php echo $page; ?>?order=activity_name" class="whitelabel">Activity Name</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=activity_description" class="whitelabel">Activity Description</a></th>
					<th scope="col" class="rounded-q4"><div class="whitelabel">Options</div></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td>&nbsp;</td>
					<td class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							
							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>		
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="3"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}			
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="3"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{

				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs2);
					$id = $rows["activity_id"];
					$a = $rows["activity_name"];
					$b = $rows["activity_description"];
					
					$b = stripslashes($b);

				?>      
				 <tr>
					<td><?php echo $a; ?>&nbsp;</td>
					<td><?php echo $b; ?>&nbsp;</td>
					<td>
								<a href="javascript:void(0);" NAME="View Record" title="View Record" onClick=window.open("activity.details.php?id=<?php echo $id; ?>&activity_name=<?php echo md5($a); ?>","Ratting","toolbar=no,resizable=no,scrollbars=yes,height=650,width=1024");>View</a>
						  <?php
						  if ($_SESSION["level"]==1 || $_SESSION["level"]==5)
						  {
						  ?>		
								<br><a href="activity.edit.php?id=<?php echo $id; ?>">Edit Record</a>
						  <?php
						  }
						  ?>
						  <?php
						  if ($_SESSION["level"]==1)
						  {
						  ?>
								<br><a href="activity.list.php?id=<?php echo $id; ?>&action=confirm_delete">Delete</a>							
						  <?php
						  }
						  ?>
					</td>
				<?php 
				} 
				?>
			</tr>
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	
	</td>
</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>