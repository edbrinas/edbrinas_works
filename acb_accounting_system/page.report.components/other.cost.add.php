<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

if ($_POST['Submit'] == 'Save')
{
	$msg = "";
	
	$cs_name = $_POST["cs_name"];
	$cs_description = $_POST["cs_description"];
	
	if ($cs_name == "") { $display_msg = 1; $msg .= "Please enter Cost/Services Name"; $msg .= "<br>"; }
	if ($cs_description == "") { $display_msg = 1; $msg .= "Please select Cost/Services Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$cs_name = addslashes($cs_name);
		$cs_description = addslashes($cs_description);
		
		$sql = "INSERT INTO tbl_other_cost_services
							(
							cs_name,
							cs_description
							)
				VALUES
							(
							'".trim($cs_name)."',
							'".trim($cs_description)."'
							)";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		$display_msg = 1;
		$msg = "Record inserted successfully!";
		
		$cs_name = "";
		$cs_description = "";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="cs" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Cost/Services Details</strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Save" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Cost/Services Name* </b></td>
							<td width="80%"><label><input name="cs_name" type="text" class="formbutton" id="cs_name" size="80" value="<?php echo $cs_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Cost/Services Description</b></td>
							<td><label><textarea name="cs_description" cols="50" rows="10" class="formbutton"><?php echo $cs_description; ?></textarea></label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>