<?php 
session_start();
include("./../includes/header.main.php");

if ($_POST['Submit'] == 'Manage Template')
{
	header("location: /workspaceGOP/page.report.components/reports.add.fields.php?report_code=".encrypt($_POST['report_code'])."");
}
elseif ($_POST['Submit'] == 'Remove Specific Field')
{
	header("location: /workspaceGOP/page.report.components/reports.delete.fields.php?report_code=".encrypt($_POST['report_code'])."");
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="vehicle" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Select Report</strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"></td>
							<td class="rounded-foot-right" align="right">
                            <input type="submit" name="Submit" value="Manage Template" class="formbutton">
                            <input type="submit" name="Submit" value="Remove Specific Field" class="formbutton">
                            </td>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<td width="20%"><b>Report Name* </b></td>
							<td width="80%">
                            <select name="report_code">
                            <?php
							$sql = "SELECT 	cfg_id,
											cfg_value
									FROM	tbl_config
									WHERE	cfg_name = 'report_code'";
							$rs = mysql_query($sql) or die($sql.mysql_error());
							while($rows = mysql_fetch_array($rs))
							{
								?>
                                <option value="<?php echo $rows['cfg_id']; ?>" <?php if ($_POST['report_type']==$rows['cfg_id']) { echo "SELECTED"; } ?>><?php echo $rows['cfg_value']; ?></option>
                                <?php
							}
							?>
                            </select>                            
                            </td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>