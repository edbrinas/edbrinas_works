<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

if ($_POST['Submit'] == 'Save')
{
	$msg = "";
	
	$budget_name = $_POST["budget_name"];
	$budget_description = $_POST["budget_description"];
	
	if ($budget_name == "") { $display_msg = 1; $msg .= "Please enter Budget Name"; $msg .= "<br>"; }
	if ($budget_description == "") { $display_msg = 1; $msg .= "Please select Budget Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$budget_name = addslashes($budget_name);
		$budget_description = addslashes($budget_description);
		
		$sql = "INSERT INTO tbl_budget
							(
							budget_name,
							budget_description
							)
				VALUES
							(
							'".trim($budget_name)."',
							'".trim($budget_description)."'
							)";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		$display_msg = 1;
		$msg = "Record inserted successfully!";
		
		$budget_name = "";
		$budget_description = "";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="budget" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Budget Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Save" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Budget Name* </b></td>
							<td width="80%"><label><input name="budget_name" type="text" class="formbutton" id="budget_name" size="80" value="<?php echo $budget_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Budget Description </b></td>
							<td><label><textarea name="budget_description" cols="50" rows="10" class="formbutton"><?php echo $budget_description; ?></textarea></label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>