<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$e_name = $_GET['equipment_name'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				equipment_id,
				equipment_name,
				equipment_description
		FROM	tbl_equipment
		WHERE	equipment_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: equipment.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["equipment_id"];
$equipment_name = $rows["equipment_name"];
$equipment_description = $rows["equipment_description"];

if ($id == $record_id && $e_name == md5($equipment_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Equipment Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Equipment Name</b></td>
			<td width="80%"><?php echo $equipment_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Equipment Description</b></td>
			<td><?php echo $equipment_description; ?>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: equipment.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
