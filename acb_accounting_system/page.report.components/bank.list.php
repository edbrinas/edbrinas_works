<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$page_size = getConfigurationValueByName('max_record_per_page');

$record_id = $_GET['id'];
$action = $_GET['action'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$search_for = $_GET['search_for'];
$search_by = $_GET['search_by'];
$delete_record = $_GET['delete_record'];

if($order=="")
{
  	$order = "bank_id";
}
if ($get_msg==1)
{
	$msg = "Record updated successfully!";
	$display_msg = 1;
}
if ($get_msg==2)
{
	$msg = "Record added successfully!";
	$display_msg = 1;
}

if($cur_page=="")
{
   	$cur_page= 1;
}
if($cur_page==1)
{
	$count_page = 0;
}
else
{
  	$count_page = (($cur_page - 1) * $page_size);
}
if ($record_id != "" && $action == "confirm_delete")
{
?>
	<script type="text/javascript">
	var flg=confirm('Are you sure you want to delete this record?\n\nClick OK to continue. Otherwise click Cancel.\n');
	if (flg==true)
	{
		

		window.location = '/workspaceGOP/page.report.components/bank.list.php?id=<?php echo $record_id; ?>&delete_record=1&action=delete'
	}
	else
	{
		window.location = '/workspaceGOP/page.report.components/bank.list.php'
	}
	</script> 
    <?php
}

if ($delete_record==1 && $record_id != "" && $action == "delete")
{
	$sql = "DELETE FROM tbl_bank
			WHERE 		bank_id = '$record_id'";
	mysql_query($sql) or die("Error in module: bank.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);
	
	$msg = "Record deleted successfully!";
	$display_msg = 1;
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
<tr>
	<td>
		<form method="get">
			<table id="rounded-search-table">
				<thead>
					<tr>
						<th class="rounded-header"><div class="whitelabel">Search</div></th>
						<th class="rounded-q4">&nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="rounded-foot-left"><input type="submit" name="action" value="Search" class="formbutton"></td>
						<td class="rounded-foot-right" align="right">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td><input name="search_for" type="text" id="search_for" class="formbutton" value="<?php echo $search_for; ?>"></td>
						<td>
							<select name="search_by" class="formbutton">
							  <option value="ALL" selected>ALL</option>
							  <option value="bank_name" <?php if ($search_by == 'bank_name') { echo "selected"; } ?> >Bank Name</option>
							  <option value="bank_account_name" <?php if ($search_by == 'bank_contact_name') { echo "selected"; } ?> >Account Name</option>
							  <option value="bank_account_number" <?php if ($search_by == 'bank_contact_address') { echo "selected"; } ?> >Account Number</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
			if ($action == "Search" && $search_for != "" && $search_by != "")
			{
				if ($search_for != "" && ($search_by != "" && $search_by != "ALL"))	
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										bank_id,
										bank_name,
										bank_address,
										bank_account_name,
										bank_account_number,
										bank_account_account_type,
										bank_account_sl_code,
										bank_inserted_by,
										bank_date_inserted
								FROM 	tbl_bank
								WHERE 	".$search_by." LIKE '%".$search_for."%'
								ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										bank_id,
										bank_name,
										bank_address,
										bank_account_name,
										bank_account_number,
										bank_account_account_type,
										bank_account_sl_code,
										bank_inserted_by,
										bank_date_inserted
								FROM 	tbl_bank
								WHERE 	".$search_by." LIKE '%".$search_for."%'
								ORDER BY $order DESC 
								LIMIT	$count_page, $page_size";	
				}
				if ($search_for == "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										bank_id,
										bank_name,
										bank_address,
										bank_account_name,
										bank_account_number,
										bank_account_account_type,
										bank_account_sl_code,
										bank_inserted_by,
										bank_date_inserted
								FROM 	tbl_bank
								ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										bank_id,
										bank_name,
										bank_account_name,
										bank_account_number,
										bank_account_account_type,
										bank_account_sl_code,
										bank_inserted_by,
										bank_date_inserted
								FROM 	tbl_bank
								ORDER BY $order DESC 
								LIMIT	$count_page, $page_size";		
				}
				if ($search_for != "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										bank_id,
										bank_name,
										bank_address,
										bank_account_name,
										bank_account_number,
										bank_account_account_type,
										bank_account_sl_code,
										bank_inserted_by,
										bank_date_inserted
								FROM 	tbl_bank
								WHERE	bank_name LIKE '%".$search_for."%'
								OR		bank_account_name LIKE '%".$search_for."%'
								OR		bank_account_number LIKE '%".$search_for."%'						
								ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										bank_id,
										bank_name,
										bank_address,
										bank_account_name,
										bank_account_number,
										bank_account_account_type,
										bank_account_sl_code,
										bank_inserted_by,
										bank_date_inserted
								FROM 	tbl_bank
								WHERE	bank_name LIKE '%".$search_for."%'
								OR		bank_account_name LIKE '%".$search_for."%'
								OR		bank_account_number LIKE '%".$search_for."%'
								ORDER BY $order DESC 
								LIMIT	$count_page, $page_size";
				}
			}
			else
			{
				$sql1 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE				
									bank_id,
									bank_name,
									bank_account_name,
									bank_address,
									bank_account_number,
									bank_account_account_type,
									bank_account_sl_code,
									bank_inserted_by,
									bank_date_inserted
							FROM 	tbl_bank
							ORDER BY $order DESC ";

				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE				
									bank_id,
									bank_name,
									bank_account_name,
									bank_address,
									bank_account_number,
									bank_account_account_type,
									bank_account_sl_code,
									bank_inserted_by,
									bank_date_inserted
							FROM 	tbl_bank
							ORDER BY $order DESC 
							LIMIT	$count_page, $page_size";
			}	
			$rs1 = mysql_query($sql1) or die("Error in sql1 in module: bank.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs1);
			$total_pages = ceil($total_records/$page_size); 
			$rs2 = mysql_query($sql2) or die("Error in sql2 in module: bank.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs2);	
			?>
			<thead>
				<tr>
					<th scope="col" class="rounded-header"><a href="<?php echo $page; ?>?order=bank_name" class="whitelabel">Bank Name</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=bank_account_name" class="whitelabel">Account Name</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=bank_address" class="whitelabel">Bank Address</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=bank_account_number" class="whitelabel">Account Number</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=bank_account_account_type" class="whitelabel">Account Type</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=bank_account_sl_code" class="whitelabel">SL Code</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=bank_inserted_by" class="whitelabel">Inserted By</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=bank_date_inserted" class="whitelabel">Date Inserted</a></th>
					<th scope="col" class="rounded-q4"><div class="whitelabel">Options</div></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2" class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td colspan="7" class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							

							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>		
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="9"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="9"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{

				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs2);
					$id = $rows["bank_id"];
					$bank_name = $rows["bank_name"];
					$bank_address = $rows["bank_address"];
					$bank_account_name = $rows["bank_account_name"];
					$bank_account_number = $rows["bank_account_number"];
					$bank_account_account_type = $rows["bank_account_account_type"];
					$bank_account_sl_code = $rows["bank_account_sl_code"];
					$bank_inserted_by = $rows["bank_inserted_by"];
					$bank_date_inserted = $rows["bank_date_inserted"];
					$bank_account_currency = getConfigurationValueById($bank_account_currency);
					$bank_inserted_by = getFullName($bank_inserted_by,3);
					$bank_account_type = getConfigurationValueById($bank_account_account_type);
					$bank_account_sl_code = getSubsidiaryLedgerAccountTitleById($bank_account_sl_code);
				?>      
				 <tr>
					<td><?php echo $bank_name; ?>&nbsp;</td>
					<td><?php echo $bank_account_name; ?>&nbsp;</td>
					<td><?php echo $bank_address; ?>&nbsp;</td>
					<td><?php echo $bank_account_number; ?>&nbsp;</td>
					<td><?php echo $bank_account_type; ?>&nbsp;</td>	
					<td><?php echo $bank_account_sl_code; ?>&nbsp;</td>										
					<td><?php echo $bank_inserted_by; ?>&nbsp;</td>
					<td><?php echo $bank_date_inserted; ?>&nbsp;</td>															
					<td>
								<a href="javascript:void(0);" NAME="View Record" title="View Record" onClick=window.open("bank.details.php?id=<?php echo $id; ?>&bank_name=<?php echo md5($bank_name); ?>","Ratting","toolbar=no,resizable=no,scrollbars=yes,height=650,width=1024");>[View]</a>		
						  <?php
						  if ($_SESSION["level"]==1 || $_SESSION["level"]==5)
						  {
						  ?>
							<br><a href="bank.edit.php?id=<?php echo $id; ?>">[Edit]</a>
							<br><a href="bank.list.php?id=<?php echo $id; ?>&action=confirm_delete">[Delete]</a>							
						  <?php
						  }
						  ?>
					</td>
				<?php 
				} 
				?>
			</tr>
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	
	</td>
</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>