<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$year = date("Y");
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;

if ($_POST['Submit'] == 'Submit')
{
	$msg = "";
	$awp_account_code = $_POST['awp_account_code'];
	$awp_budget_year = $_POST['awp_budget_year'];
	$awp_budget_amount = $_POST['awp_budget_amount'];

	if ($awp_account_code=="") { $display_msg = 1; $msg .= "Please select account code!"; $msg = "<br>"; }
	if ($awp_budget_amount=="") { $display_msg = 1; $msg .= "Please enter budget amount!"; $msg = "<br>"; }
	if (checkIfNumber($awp_budget_amount) == 0)	{ $display_msg = 1; $msg .= "Budget must be a number"; $msg .= "<br>"; }
	
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					awp_account_code
			FROM	tbl_fund_analysis_by_awp_by_budget_line
			WHERE 	awp_account_code like '%".$awp_account_code."%'
			AND		awp_budget_year = '$awp_budget_year'";
	$rs = mysql_query($sql) or die("Query Error1 " .mysql_error());			
	$rec_count = mysql_num_rows($rs);
	
	if ($rec_count == 0)
	{
		if ($msg=="")
		{
			$sql = "INSERT INTO tbl_fund_analysis_by_awp_by_budget_line 
								(
								awp_account_code,
								awp_budget_year,
								awp_budget_amount
								)
					VALUES 		(
								'$awp_account_code',
								'$awp_budget_year',
								'$awp_budget_amount'
								)";
			mysql_query($sql) or die("Query Error1 " .mysql_error());
			insertEventLog($user_id,$sql);

			$awp_account_code = "";
			$awp_budget_amount = "";
			
			$display_msg = 1;
			$msg = "Record inserted successfully!";
		}
	}
	else
	{
		$display_msg = 1;
		$msg = "Record already exists!";
	}

}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="system_users" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><b>Fund Analysis - Per AWP, Per Budget Line</b></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Submit" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><strong>Account Code * </strong></td>
							<td width="80%">
							<?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											sl_id,
											sl_account_code,
											sl_account_title
									FROM 	tbl_subsidiary_ledger
									ORDER BY sl_account_code";
							$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
							?>
							<select name='awp_account_code' class='formbutton'>
							<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['sl_id'];
									$column_name = $data['sl_account_title'];
									$column_code = $data['sl_account_code'];
									?>
									<option value='<?php echo $column_id; ?>'
									<?php
									if ($column_id==$awp_account_code)
									{
									?>
										SELECTED
									<?php
									}
									?>
									><?php echo $column_code." [ ".$column_name." ]"; 
								}
								?></option>
							</select>							</td>
						</tr>
						<tr>
						  <td><b>Budget Year *</b></td>
						  <td>
                          		<select name='awp_budget_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
                          
                          </td>
					  </tr>
						<tr>
						  <td><strong>Alloted Budget* </strong></td>
						  <td><input name="awp_budget_amount" type="text" value="<?php echo $awp_budget_amount; ?>" class='formbutton'></td>
						</tr>
					</tbody>
				</table>
		  </form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>