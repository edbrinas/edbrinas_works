<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$record_id = $_GET['record_id'];
$record_id = decrypt($record_id);
$year = date("Y");
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;

$sql = "SELECT	SQL_BUFFER_RESULT
				SQL_CACHE
				fa_id,
				fa_budget_year,
				fa_account_code,
				fa_component_id,
				fa_activity_id,
				fa_budget_id,
				fa_item_id,
				fa_number_of_units,
				fa_cost_per_item
		FROM	tbl_fund_analysis_by_component_by_activity
		WHERE	fa_id = '$record_id'";
$rs = mysql_query($sql) or die("Error in sql2 in module: fa.by.awp.by.budget.line.edit ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);		
$fa_id  = $rows["fa_id"];
$fa_budget_year = $rows["fa_budget_year"];
$fa_account_code = $rows["fa_account_code"];
$fa_component_id = $rows["fa_component_id"];
$fa_activity_id = $rows["fa_activity_id"];
$fa_budget_id = $rows["fa_budget_id"];
$fa_item_id = $rows["fa_item_id"];
$fa_number_of_units = $rows["fa_number_of_units"];
$fa_cost_per_item = $rows["fa_cost_per_item"];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$fa_id = $_POST['fa_id'];
	$fa_budget_year = $_POST["fa_budget_year"];
	$fa_account_code = $_POST["fa_account_code"];
	$fa_component_id = $_POST['fa_component_id'];
	$fa_activity_id = $_POST['fa_activity_id'];
	$fa_budget_id = $_POST['fa_budget_id'];
	$fa_item_id = $_POST['fa_item_id'];
	$fa_number_of_units = $_POST['fa_number_of_units'];
	$fa_cost_per_item = $_POST['fa_cost_per_item'];
	$fa_amount = $fa_number_of_units*$fa_cost_per_item;

	if ($fa_account_code=="") { $display_msg = 1; $msg .= "Please select account code!"; $msg = "<br>"; }
	if ($fa_component_id=="") { $display_msg = 1; $msg .= "Please select component id!"; $msg = "<br>"; }
	if ($fa_activity_id=="") { $display_msg = 1; $msg .= "Please enter activity id!"; $msg = "<br>"; }
	if ($fa_budget_id=="") { $display_msg = 1; $msg .= "Please select budget id!"; $msg = "<br>"; }
	if ($fa_item_id=="") { $display_msg = 1; $msg .= "Please select item id!"; $msg = "<br>"; }
	if ($fa_number_of_units=="") { $display_msg = 1; $msg .= "Please enter number of units!"; $msg = "<br>"; }
	if ($fa_cost_per_item=="") { $display_msg = 1; $msg .= "Please enter cost per item!"; $msg = "<br>"; }
	
	if (checkIfNumber($fa_number_of_units) == 0)	{ $display_msg = 1; $msg .= "Numbe of units must be a number"; $msg .= "<br>"; }
	if (checkIfNumber($fa_cost_per_item) == 0)	{ $display_msg = 1; $msg .= "Cost per item must be a number"; $msg .= "<br>"; }
	
	/*
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					fa_component_id,
					fa_activity_id,
					fa_budget_id
			FROM	tbl_fund_analysis_by_component_by_activity
			WHERE 	fa_component_id = '".$fa_component_id."'
			AND		fa_activity_id = '".$fa_activity_id."'
			AND		fa_budget_id = '".$fa_budget_id."'";
	$rs = mysql_query($sql) or die("Query Error1 " .mysql_error());			
	$rec_count = mysql_num_rows($rs);
	
	if ($rec_count == 0)
	{
	*/
		if ($msg=="")
		{
			$sql = "UPDATE 	tbl_fund_analysis_by_component_by_activity 
					SET		fa_budget_year = '$fa_budget_year',
							fa_account_code = '$fa_account_code',
							fa_component_id = '$fa_component_id',
							fa_activity_id = '$fa_activity_id',
							fa_budget_id = '$fa_budget_id',
							fa_item_id = '$fa_item_id',
							fa_number_of_units = '$fa_number_of_units',
							fa_cost_per_item = '$fa_cost_per_item',
							fa_amount = '$fa_amount'
					WHERE 	fa_id = '$fa_id'";
			mysql_query($sql) or die("Query Error1 " .mysql_error());
			insertEventLog($user_id,$sql);

			$fa_component_id = "";
			$fa_activity_id = "";
			$fa_budget_id = "";
			$fa_item_id = "";
			$fa_number_of_units = "";
			$fa_cost_per_item = "";
			$fa_amount = "";
			
			header("location: /workspaceGOP/page.report.components/fa.by.component.id.by.activity.id.list.php?msg=1");
		}
	/*
	}
	else
	{
		$display_msg = 1;
		$msg = "Record already exists!";
	}
	
	*/

}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="system_users" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><b>Fund Analysis - Per Component ID, Per Activity ID</b></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input type="hidden" name="fa_id" value="<?php echo $fa_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><strong>Budget ID * </strong></td>
							<td width="80%"><input name="fa_budget_id" type="text" value="<?php echo $fa_budget_id; ?>" class='formbutton' /></td>
						</tr>
						<tr>
						  <td><b>Budget Year *</b></td>
						  <td>
                          		<select name='fa_budget_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
                          
                          </td>
					  </tr>                              
						<tr>
							<td><strong>Account Code * </strong></td>
							<td>
							<?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											sl_id,
											sl_account_code,
											sl_account_title
									FROM 	tbl_subsidiary_ledger
									ORDER BY sl_account_code";
							$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
							?>
							<select name='fa_account_code' class='formbutton'>
							<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['sl_id'];
									$column_name = $data['sl_account_title'];
									$column_code = $data['sl_account_code'];
									?>
									<option value='<?php echo $column_id; ?>'
									<?php
									if ($column_id==$fa_account_code)
									{
									?>
										SELECTED
									<?php
									}
									?>
									><?php echo $column_code." [ ".$column_name." ]"; 
								}
								?></option>
							</select>							</td>
						</tr>                        
						<tr>
						  <td><strong>Component ID* </strong></td>
						  <td>
							<?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											component_id,
											component_name,
											component_description
									FROM 	tbl_component
									ORDER BY component_name";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?>
							<select name='fa_component_id' class='formbutton'>
							<option value='' SELECTED>[Please Select]</option>
							<?php
							while($data=mysql_fetch_array($rs))
							{
								$column_id = $data['component_id'];
								$column_name = $data['component_name'];
								$column_description = $data['component_description'];
								?>
								<option value='<?php echo $column_id; ?>'
								<?php
								if ($fa_component_id == $column_id)
								{
								?>
									SELECTED
								<?php
								}
								?>
								><?php echo $column_name;
							}
							?></option>
							</select>							
						 </td>
						</tr>
						<tr>
						  <td><strong>Activity ID* </strong></td>
						  <td>
							<?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											activity_id,
											activity_name,
											activity_description
									FROM 	tbl_activity
									ORDER BY activity_name";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?>
							<select name='fa_activity_id' class='formbutton'>
							<option value='' SELECTED>[Please Select]</option>
							<?php
							while($data=mysql_fetch_array($rs))
							{
								$column_id = $data['activity_id'];
								$column_name = $data['activity_name'];
								$column_description = $data['activity_description'];
								?>
								<option value='<?php echo $column_id; ?>'
								<?php
								if ($fa_activity_id == $column_id)
								{
								?>
									SELECTED
								<?php
								}
								?>
								><?php echo $column_name;
							}
							?> </option>
							</select>							
                            </td>
						</tr>
						<tr>
						  <td><strong>Item ID* </strong></td>
						  <td>
							<?php
                            $sql = "SELECT	SQL_BUFFER_RESULT
                                            SQL_CACHE
                                            item_id,
                                            item_name,
                                            item_description
                                    FROM 	tbl_item_code
                                    ORDER BY item_name";
                            $rs = mysql_query($sql) or die('Error ' . mysql_error());
                            ?>
                            <select name='fa_item_id' class='formbutton'>
                            <option value='' SELECTED>[Please Select]</option>
                            <?php
                                while($data=mysql_fetch_array($rs))
                                {
                                    $column_id = $data['item_id'];
                                    $column_name = $data['item_name'];
                                    $column_description = $data['item_description'];
                                    ?>
                                    <option value='<?php echo $column_id; ?>'
                                    <?php
                                    if ($fa_item_id == $column_id)
                                    {
                                    ?>
                                        SELECTED
                                    <?php
                                    }
                                    ?>
                                    ><?php echo $column_description;
                                }
                                ?> </option>
                            </select>
                          </td>
						</tr>						
						<tr>
						  <td><strong>Number of Units * </strong></td>
						  <td><input name="fa_number_of_units" type="text" value="<?php echo $fa_number_of_units; ?>" class='formbutton'></td>
						</tr>
						<tr>
						  <td><strong>Cost Per Item * </strong></td>
						  <td><input name="fa_cost_per_item" type="text" value="<?php echo $fa_cost_per_item; ?>" class='formbutton'></td>
						</tr>																								
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>