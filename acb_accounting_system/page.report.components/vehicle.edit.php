<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$id = $_GET['id'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				vehicle_id,
				vehicle_name,
				vehicle_description
		FROM	tbl_vehicle
		WHERE	vehicle_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: vehicle.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["vehicle_id"];
$vehicle_name = $rows["vehicle_name"];
$vehicle_description = $rows["vehicle_description"];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$record_id = $_POST["record_id"];
	$vehicle_name = $_POST["vehicle_name"];
	$vehicle_description = $_POST["vehicle_description"];
	
	if ($vehicle_name == "") { $display_msg = 1; $msg .= "Please enter Vehicles Name"; $msg .= "<br>"; }
	if ($vehicle_description == "") { $display_msg = 1; $msg .= "Please select Vehicles Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$vehicle_name = addslashes($vehicle_name);
		$vehicle_description = addslashes($vehicle_description);
		
		$sql = "UPDATE	tbl_vehicle
				SET		vehicle_name= '".trim($vehicle_name)."',
						vehicle_description = '".trim($vehicle_description)."'
				WHERE 	vehicle_id = '$record_id'";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		header("location: /workspaceGOP/page.report.components/vehicle.list.php?msg=1");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="vehicle" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Vehicles Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Vehicles Name* </b></td>
							<td width="80%"><label><input name="vehicle_name" type="text" class="formbutton" id="vehicle_name" size="80" value="<?php echo $vehicle_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Vehicles Description </b></td>
							<td><label><textarea name="vehicle_description" cols="50" rows="10" class="formbutton"><?php echo $vehicle_description; ?></textarea></label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>