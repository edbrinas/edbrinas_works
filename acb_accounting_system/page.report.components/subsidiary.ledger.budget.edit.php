<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$id = $_GET['id'];
$date_time_inserted = date("Y-m-d H:i:s");
$current_year = date("Y");
$id = decrypt($id);

$sql = "SELECT	SQL_BUFFER_RESULT
				SQL_CACHE	
				sl_id,
				sl_account_code,
				sl_account_title
		FROM 	tbl_subsidiary_ledger
		WHERE	sl_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: subsidiary.ledger.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["sl_id"];
$sl_account_code = $rows["sl_account_code"];
$sl_account_title = $rows["sl_account_title"];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$record_id = $_POST["record_id"];
	$sl_details_id = $_POST["sl_details_id"];
	$sl_details_currency_code = $_POST["sl_details_currency_code"];
	$sl_details_starting_balance = $_POST["sl_details_starting_balance"];
	$sl_details_ending_balance = $_POST["sl_details_ending_balance"];
	$number_of_records = count($sl_details_id);
		
	for($i=0;$i<$number_of_records;$i++)
	{
		if ($sl_details_starting_balance[$i] == "")	{ $display_msg = 1; $msg .= "Please enter starting balance"; $msg .= "<br>"; }
		if ($sl_details_ending_balance[$i] == "")	{ $display_msg = 1; $msg .= "Please enter ending balance"; $msg .= "<br>"; }
		
		if (checkIfNumber($sl_details_starting_balance[$i]) == 0)	{ $display_msg = 1; $msg .= "Starting balance must be a number"; $msg .= "<br>"; }
		if (checkIfNumber($sl_details_ending_balance[$i]) == 0)	{ $display_msg = 1; $msg .= "Ending balance must be a number"; $msg .= "<br>"; }
		
		if ($msg=="")
		{ 
			if ($sl_details_ending_balance[$i]==0)
			{
				$sl_details_ending_balance[$i] = $sl_details_starting_balance[$i];
			}
			$sql = "UPDATE tbl_subsidiary_ledger_details
					SET		sl_details_starting_balance = '$sl_details_starting_balance[$i]',
							sl_details_ending_balance = '$sl_details_ending_balance[$i]'
					WHERE	sl_details_id = '$sl_details_id[$i]'
					AND 	sl_details_currency_code = '$sl_details_currency_code[$i]'";
			mysql_query($sql) or die("Error in sql ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
			header("location: /workspaceGOP/page.report.components/subsidiary.ledger.list.php?msg=1");
		}
	}
	
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main" style='table-layout:fixed' >
	<tr>
		<td>
			<form name="subsidiary_ledger" method="post">
				<table id="rounded-add-entries" align="center" >
					<thead>
						<tr>
							<th colspan="2" scope="col" class="rounded-header"><div class="main" align="left"><strong>Subsidiary Ledger Budget Details</strong></div></th>
							<th colspan="2" scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="2" class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td colspan="2" class="rounded-foot-right" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Account Code*</b></td>
							<td width="30%">
								<?php echo $sl_account_code; ?>
								<input name="sl_account_code" type="hidden" id="sl_account_code" value="<?php echo $sl_account_code; ?>" class="formbutton">
							</td>
							<td width="20%"><b>Account Title* </b></td>
							<td width="30%">
								<?php echo $sl_account_title; ?>
								<input name="sl_account_title" type="hidden" id="sl_account_title" value="<?php echo $sl_account_title; ?>" class="formbutton">
							</td>
						</tr>

						<tr valign="top">
						  <td colspan="4" align="center">
						  <table width="50%" border="0" cellspacing="2" cellpadding="2">
                            <tr>
                              <th scope="col">Year</th>
                              <th scope="col">Currency Code </th>
                              <th scope="col">Starting Balance </th>
                              <th scope="col">Ending Balance </th>
                            </tr>
							<?php
								$sql2 = "	SELECT SQL_BUFFER_RESULT
													SQL_CACHE
													sl_details_id,
													sl_details_year,
													sl_details_currency_code,
													sl_details_starting_balance,
													sl_details_ending_balance
											FROM 	tbl_subsidiary_ledger_details
											WHERE	sl_details_reference_number = '$record_id'
											AND		sl_details_year = '$current_year'";
								$rs2 = mysql_query($sql2) or die("Error in sql in function ".$sql2." ".mysql_error());
								while($rows2=mysql_fetch_array($rs2))
								{	
									$sl_details_id = $rows2["sl_details_id"];
									$sl_details_year = $rows2["sl_details_year"];
									$sl_details_currency_code = $rows2["sl_details_currency_code"];
									$sl_details_starting_balance = $rows2["sl_details_starting_balance"];
									$sl_details_ending_balance = $rows2["sl_details_ending_balance"];
									$sl_details_currency_name = getConfigurationValueById($sl_details_currency_code);
								?>
								<tr>
								  <td scope="col"><input name="sl_details_id[]" type="hidden" value="<?php echo $sl_details_id; ?>" class="formbutton"><?php echo $sl_details_year; ?></td>
								  <td scope="col"><input name="sl_details_currency_code[]" type="hidden" value="<?php echo $sl_details_currency_code; ?>" class="formbutton"><?php echo $sl_details_currency_name; ?></th>
								  <td scope="col"><input name="sl_details_starting_balance[]" type="text" value="<?php echo $sl_details_starting_balance; ?>" class="formbutton"></td>
								  <td scope="col"><input name="sl_details_ending_balance[]" type="text" value="<?php echo $sl_details_ending_balance; ?>" class="formbutton"></td>
								</tr>
							<?php
								}
								?>                      
						  </table>
						  </td>
					  </tr>
						<tr valign="top">
					  		<td colspan="4">&nbsp;</td>			  
					  	</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>