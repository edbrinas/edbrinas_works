<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

if ($_POST['Submit'] == 'Save')
{
	$msg = "";
	
	$vehicle_name = $_POST["vehicle_name"];
	$vehicle_description = $_POST["vehicle_description"];
	
	if ($vehicle_name == "") { $display_msg = 1; $msg .= "Please enter Vehicle Name"; $msg .= "<br>"; }
	if ($vehicle_description == "") { $display_msg = 1; $msg .= "Please select Vehicle Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$vehicle_name = addslashes($vehicle_name);
		$vehicle_description = addslashes($vehicle_description);
		
		$sql = "INSERT INTO tbl_vehicle
							(
							vehicle_name,
							vehicle_description
							)
				VALUES
							(
							'".trim($vehicle_name)."',
							'".trim($vehicle_description)."'
							)";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		$display_msg = 1;
		$msg = "Record inserted successfully!";
		
		$vehicle_name = "";
		$vehicle_description = "";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="vehicle" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Vehicles Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Save" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Vehicles Name* </b></td>
							<td width="80%"><label><input name="vehicle_name" type="text" class="formbutton" id="vehicle_name" size="80" value="<?php echo $vehicle_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Vehicles Description </b></td>
							<td><label><textarea name="vehicle_description" cols="50" rows="10" class="formbutton"><?php echo $vehicle_description; ?></textarea></label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>