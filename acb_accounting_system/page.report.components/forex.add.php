<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$year = date("Y");
$month = date("m");
$day = date("d");
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;
$date_time_inserted = date("Y-m-d H:i:s");

if ($_POST['Submit'] == 'Save')
{
	$msg = "";
	
	$forex_transaction_date = $_POST["forex_transaction_date"];
	$forex_peso_to_euro_value = $_POST["peso_to_euro"];
	$forex_peso_to_dollar_value = $_POST["peso_to_dollar"];
	$forex_dollar_to_euro_value = $_POST["dollar_to_euro"];

	$forex_peso_to_pound_value = $_POST["peso_to_pound"];
	$forex_dollar_to_pound_value = $_POST["dollar_to_pound"];
	
	$forex_year = $_POST['forex_year'];
	$forex_month = $_POST['forex_month'];
	#$forex_day = $_POST['forex_day'];
	$forex_month = sprintf("%02d",$forex_month);
	$forex_transaction_date = $forex_month."-".$forex_year;
	
	
	if ($forex_peso_to_euro_value == "") { $display_msg = 1; $msg .= "Please enter Peso To Euro Rate"; $msg .= "<br>"; }
	if ($forex_peso_to_dollar_value == "") { $display_msg = 1; $msg .= "Please enter Peso To Dollar Rate"; $msg .= "<br>"; }
	if ($forex_dollar_to_euro_value == "") { $display_msg = 1; $msg .= "Please enter Dollar To Euro Rate"; $msg .= "<br>"; }

	if ($forex_peso_to_pound_value == "") { $display_msg = 1; $msg .= "Please enter Peso To Pound Rate"; $msg .= "<br>"; }
	if ($forex_dollar_to_pound_value == "") { $display_msg = 1; $msg .= "Please enter Dollar To Pound Rate"; $msg .= "<br>"; }

	if (checkIfNumber($forex_peso_to_euro_value) == 0) { $display_msg = 1; $msg .= "Peso To Euro Rate must be a NUMBER"; $msg .= "<br>"; }
	if (checkIfNumber($forex_peso_to_dollar_value) == 0) { $display_msg = 1; $msg .= "Peso To Dollar Rate must be a NUMBER"; $msg .= "<br>"; }
	if (checkIfNumber($forex_dollar_to_euro_value) == 0) { $display_msg = 1; $msg .= "Dollar To Euro Rate must be a NUMBER"; $msg .= "<br>"; }

	if (checkIfNumber($forex_peso_to_pound_value) == 0) { $display_msg = 1; $msg .= "Peso To Pound Rate must be a NUMBER"; $msg .= "<br>"; }
	if (checkIfNumber($forex_dollar_to_pound_value) == 0) { $display_msg = 1; $msg .= "Dollar To Pound Rate must be a NUMBER"; $msg .= "<br>"; }
	
	if ($msg == "")
	{
		#for($forex_day=1;$forex_day<32;$forex_day++)
		#{
		#	$forex_transaction_date = dateTimeFormat($hr="",$min="",$sec="",$forex_month,$forex_day,$forex_year);
			$sql = "INSERT INTO tbl_forex
								(
								forex_currency_code,
								forex_transaction_date,
								forex_currency_value,
								forex_inserted_by,
								forex_date_time_inserted
								)
					VALUES
								(
								'34',
								'".trim($forex_transaction_date)."',
								'".trim($forex_peso_to_euro_value)."',
								'$user_id',
								'$date_time_inserted'
								)";
			mysql_query($sql) or die($sql.mysql_error());	
			insertEventLog($user_id,$sql);
			
			$sql = "INSERT INTO tbl_forex
								(
								forex_currency_code,
								forex_transaction_date,
								forex_currency_value,
								forex_inserted_by,
								forex_date_time_inserted
								)
					VALUES
								(
								'35',
								'".trim($forex_transaction_date)."',
								'".trim($forex_peso_to_dollar_value)."',
								'$user_id',
								'$date_time_inserted'
								)";
			mysql_query($sql) or die($sql.mysql_error());
			insertEventLog($user_id,$sql);
			
			$sql = "INSERT INTO tbl_forex
								(
								forex_currency_code,
								forex_transaction_date,
								forex_currency_value,
								forex_inserted_by,
								forex_date_time_inserted
								)
					VALUES
								(
								'36',
								'".trim($forex_transaction_date)."',
								'".trim($forex_dollar_to_euro_value)."',
								'$user_id',
								'$date_time_inserted'
								)";
			mysql_query($sql) or die($sql.mysql_error());	
			insertEventLog($user_id,$sql);	

			$sql = "INSERT INTO tbl_forex
								(
								forex_currency_code,
								forex_transaction_date,
								forex_currency_value,
								forex_inserted_by,
								forex_date_time_inserted
								)
					VALUES
								(
								'58',
								'".trim($forex_transaction_date)."',
								'".trim($forex_peso_to_pound_value)."',
								'$user_id',
								'$date_time_inserted'
								)";
			mysql_query($sql) or die($sql.mysql_error());	
			insertEventLog($user_id,$sql);
			$sql = "INSERT INTO tbl_forex
								(
								forex_currency_code,
								forex_transaction_date,
								forex_currency_value,
								forex_inserted_by,
								forex_date_time_inserted
								)
					VALUES
								(
								'59',
								'".trim($forex_transaction_date)."',
								'".trim($forex_dollar_to_pound_value)."',
								'$user_id',
								'$date_time_inserted'
								)";
			mysql_query($sql) or die($sql.mysql_error());	
			insertEventLog($user_id,$sql);
								
		#}
		$forex_transaction_date = "";
		$forex_peso_to_euro_value = "";
		$forex_peso_to_dollar_value = "";
		$forex_dollar_to_euro_value = "";
		
		$forex_peso_to_pound_value = "";
		$forex_dollar_to_pound_value = "";		

		$display_msg = 1;
		header("location:/workspaceGOP/page.report.components/forex.list.php?msg=1");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="forex" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Forex Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Save" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr valign="top">
						  <td width="20%"><b>Trading Year/Month*</b></td>
						  <td width="80%">
							<select name='forex_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
								?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='forex_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
										?>
										 selected="selected"
										 <?
									}
									?>
									><?php echo $months[$mo-1]; ?>
								<?php
								}
								?>
									</option>
							</select>						  
						</td>
					  </tr>
					<tr valign="top">
						<td><b>Peso to Euro (1 Euro=) *</b></td>
						<td><input name="peso_to_euro" type="text" id="peso_to_euro" value="<?php echo $peso_to_euro; ?>" class="formbutton"></td>
					</tr>
					<tr valign="top">
						<td><b>Peso to Dollar (1 USD=) *</b></td>
						<td><input name="peso_to_dollar" type="text" id="peso_to_dollar" value="<?php echo $peso_to_dollar; ?>" class="formbutton"></td>
					</tr>
					<tr valign="top">
						<td><b>Dollar to Euro (1USD=) *</b></td>
						<td><input name="dollar_to_euro" type="text" id="dollar_to_euro" value="<?php echo $dollar_to_euro; ?>" class="formbutton"></td>
					</tr>		


					<tr valign="top">
						<td><b>Peso to Pound (1 GBP=) *</b></td>
						<td><input name="peso_to_pound" type="text" id="peso_to_pound" value="<?php echo $peso_to_pound; ?>" class="formbutton"></td>
					</tr>
					<tr valign="top">
						<td><b>Dollar to Pound (1 USD=) *</b></td>
						<td><input name="dollar_to_pound" type="text" id="dollar_to_pound" value="<?php echo $dollar_to_pound; ?>" class="formbutton"></td>
					</tr>					
						
																								
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>