<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$date_time_inserted = date("Y-m-d H:i:s");
$rec_id = $_GET["id"];
$rec_bank_name = $_GET["bank_name"];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$rec_id = $_POST['rec_id'];
	$bank_name = $_POST['bank_name'];
	$bank_address = $_POST['bank_address'];
	$bank_account_number = $_POST['bank_account_number'];
	$bank_account_name = $_POST['bank_account_name'];
	$bank_account_account_type = $_POST['bank_account_account_type'];
	$bank_account_currency_code = $_POST['bank_account_currency_code'];
	$bank_account_sl_code = $_POST['bank_account_sl_code'];
	$bank_signed_cheque = $_POST['bank_signed_cheque'];
	$bank_deposit_in_transit = $_POST['bank_deposit_in_transit'];
	$bank_outstanding_cheque = $_POST['bank_outstanding_cheque'];
		
	$bank_name = sentenceCase($bank_name);
	
	if ($bank_name == "")	{ $display_msg = 1; $msg .= "Please enter bank name<br>"; }
	if ($bank_address == "")	{ $display_msg = 1; $msg .= "Please enter bank address<br>"; }
	if ($bank_account_number == "")	{ $display_msg = 1; $msg .= "Please enter account number<br>"; }
	if ($bank_account_name == "")	{ $display_msg = 1; $msg .= "Please enter account name<br>"; }
	if ($bank_account_account_type == "")	{ $display_msg = 1; $msg .= "Please select account type<br>"; }
	if ($bank_account_currency_code == "")	{ $display_msg = 1; $msg .= "Please select currency code<br>"; }
	#if ($bank_account_sl_code == "")	{ $display_msg = 1; $msg .= "Please select SL Code<br>"; }	
	
	if ($msg == "")
	{
		$sql = "UPDATE	tbl_bank
				SET		bank_name = '$bank_name',
						bank_address = '$bank_address',
						bank_account_name = '$bank_account_name',
						bank_account_number = '$bank_account_number',
						bank_account_account_type = '$bank_account_account_type',
						bank_account_currency_code = '$bank_account_currency_code',
						bank_account_sl_code = '$bank_account_sl_code',
						bank_signed_cheque = '$bank_signed_cheque',
						bank_deposit_in_transit = '$bank_deposit_in_transit',
						bank_outstanding_cheque = '$bank_outstanding_cheque'
				WHERE	bank_id = '$rec_id'";
		mysql_query($sql) or die("Query Error " .mysql_error());
		
		insertEventLog($user_id,$sql);
		header("location: /workspaceGOP/page.report.components/bank.list.php?msg=1");
	}
}

$sql = "	SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE				
					bank_id,
					bank_name,
					bank_address,
					bank_account_name,
					bank_account_number,
					bank_account_account_type,
					bank_account_currency_code,
					bank_account_sl_code,
					bank_signed_cheque,
					bank_deposit_in_transit,
					bank_outstanding_cheque,				
					bank_inserted_by,
					bank_date_inserted
			FROM 	tbl_bank
			WHERE	bank_id = '$rec_id'";
$rs = mysql_query($sql) or die("Error in sql in module: bank.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);
$rec_id = $rows["bank_id"];
$bank_name = $rows["bank_name"];
$bank_address = $rows["bank_address"];
$bank_account_name = $rows["bank_account_name"];
$bank_account_number = $rows["bank_account_number"];
$bank_account_account_type = $rows["bank_account_account_type"];
$bank_account_currency_code = $rows["bank_account_currency_code"];
$bank_account_sl_code = $rows["bank_account_sl_code"];
$bank_signed_cheque = $rows["bank_signed_cheque"];
$bank_deposit_in_transit = $rows["bank_deposit_in_transit"];
$bank_outstanding_cheque = $rows["bank_outstanding_cheque"];

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="system_users" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Bank </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input name="rec_id" type="hidden" class="formbutton" id="rec_id" value="<?php echo $rec_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="17%"><strong>Bank Name * </strong></td>
							<td width="83%"><label><input name="bank_name" type="text" class="formbutton" id="bank_name" value="<?php echo $bank_name; ?>"></label></td>
						</tr>
						<tr>
						  <td><strong>Account Number * </strong></td>
						  <td><label><input name="bank_account_number" type="text" id="bank_account_number" value="<?php echo $bank_account_number; ?>" class="formbutton"></label></td>
						</tr>
						<tr>
						  <td><strong>Account Name * </strong></td>
						  <td><label><input name="bank_account_name" type="text" id="bank_account_name" value="<?php echo $bank_account_name; ?>" class="formbutton"></label></td>
						</tr>
						<tr>
						  <td valign="top"><b>Bank Address *</b></td>
						  <td valign="top"><label><textarea name="bank_address" cols="20" rows="10" class='formbutton'><?php echo $bank_address; ?></textarea></label></td>
					  </tr>						
						<tr>
						  <td><b>Account Currency*</b></td>
						  <td>
								<?php
								
								$sql = "SELECT	SQL_BUFFER_RESULT
												SQL_CACHE
												cfg_id,
												cfg_value
										FROM 	tbl_config
										WHERE 	cfg_name = 'currency_type'";
								$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
								?>
								<select name='bank_account_currency_code' class='formbutton'>
								<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['cfg_id'];
									$column_name = $data['cfg_value'];
									?>
									<option value='<?php echo $column_id; ?>' <?php if ($column_id==$bank_account_currency_code) { echo "SELECTED"; } ?>>
									<?php echo $column_name; ?></option>
									<?php
								}
								?>
								</select>						  
							</td>
					  </tr>
						<tr>
						  <td><strong>Account Type * </strong></td>
						  <td>								
								<?php
								$sql = "SELECT	SQL_BUFFER_RESULT
												SQL_CACHE
												cfg_id,
												cfg_value
										FROM 	tbl_config
										WHERE 	cfg_name = 'bank_account_type'";
								$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
								?>
								<select name='bank_account_account_type' class='formbutton'>
								<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['cfg_id'];
									$column_name = $data['cfg_value'];
									?>
									<option value='<?php echo $column_id; ?>' <?php if ($column_id==$bank_account_account_type) { echo "SELECTED"; } ?>><?php echo $column_name; ?></option>
									<?php
								}
								?>
								</select>							</td>
						</tr>
						<tr>
						  <td><strong>SL Code*</strong></td>
						  <td>
								<?php
								$sql2 = "SELECT	SQL_BUFFER_RESULT
												SQL_CACHE
												sl_id,
												sl_account_code,
												sl_account_title
										FROM 	tbl_subsidiary_ledger
										WHERE	sl_account_title LIKE '%bank%'
										ORDER BY sl_account_title ASC";
								$rs2 = mysql_query($sql2) or die('Error ' . mysql_error()); 
								?>
								<select name='bank_account_sl_code' class='formbutton'>
								<?php
								while($data2=mysql_fetch_array($rs2))
								{
									$column_id = $data2['sl_id'];
									$column_name = $data2['sl_account_code'];
									$column_description = $data2['sl_account_title'];
									?>
									<option value='<?php echo $column_id; ?>' <?php if ($column_id==$bank_account_sl_code) { echo "SELECTED"; } ?>><?php echo $column_description; ?></option>
									<?php
								}
								?>	
								</select>							</td>
					  </tr>	
						<tr>
						  <td colspan="2"><label>
						  <div align="center">Include the following fields in Bank Reconciliation</div>
						  </label></td>
					    </tr>	
						<tr>
						  <td><strong>Signed disbursement vouchers for cheque preparation * </strong></td>
						  <td><input name="bank_signed_cheque" type="checkbox" value="1" <?php if ($bank_signed_cheque == 1) { echo "CHECKED"; } ?>/></td>
						</tr>
						<tr>
						  <td><strong>Deposit in transit * </strong></td>
						  <td><input name="bank_deposit_in_transit" type="checkbox" value="1" <?php if ($bank_deposit_in_transit == 1) { echo "CHECKED"; } ?>/></td>
						</tr>		
						<tr>
						  <td><strong>Outstanding cheque * </strong></td>
						  <td><input name="bank_outstanding_cheque" type="checkbox" value="1" <?php if ($bank_outstanding_cheque == 1) { echo "CHECKED"; } ?>/></td>
						</tr>																													          
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>