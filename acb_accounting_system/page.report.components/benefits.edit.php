<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$id = $_GET['id'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				benefits_id,
				benefits_name,
				benefits_description
		FROM	tbl_benefits
		WHERE	benefits_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: benefits.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["benefits_id"];
$benefits_name = $rows["benefits_name"];
$benefits_description = $rows["benefits_description"];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$record_id = $_POST["record_id"];
	$benefits_name = $_POST["benefits_name"];
	$benefits_description = $_POST["benefits_description"];
	
	if ($benefits_name == "") { $display_msg = 1; $msg .= "Please enter Benefits Name"; $msg .= "<br>"; }
	if ($benefits_description == "") { $display_msg = 1; $msg .= "Please select Benefits Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$benefits_name = addslashes($benefits_name);
		$benefits_description = addslashes($benefits_description);
		
		$sql = "UPDATE	tbl_benefits
				SET		benefits_name= '".trim($benefits_name)."',
						benefits_description = '".trim($benefits_description)."'
				WHERE 	benefits_id = '$record_id'";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		header("location: /workspaceGOP/page.report.components/benefits.list.php?msg=1");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="benefits" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Benefits Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Benefits Name* </b></td>
							<td width="80%"><label><input name="benefits_name" type="text" class="formbutton" id="benefits_name" size="80" value="<?php echo $benefits_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Benefits Description </b></td>
							<td><label><textarea name="benefits_description" cols="50" rows="10" class="formbutton"><?php echo $benefits_description; ?></textarea></label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>