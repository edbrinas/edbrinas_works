<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$id = $_GET['id'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				item_id,
				item_name,
				item_description
		FROM	tbl_item_code
		WHERE	item_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: item.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["item_id"];
$item_name = $rows["item_name"];
$item_description = $rows["item_description"];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$record_id = $_POST["record_id"];
	$item_name = $_POST["item_name"];
	$item_description = $_POST["item_description"];
	
	if ($item_name == "") { $display_msg = 1; $msg .= "Please enter items Name"; $msg .= "<br>"; }
	if ($item_description == "") { $display_msg = 1; $msg .= "Please select items Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$item_name = addslashes($item_name);
		$item_description = addslashes($item_description);
		
		$sql = "UPDATE	tbl_item_code
				SET		item_name= '".trim($item_name)."',
						item_description = '".trim($item_description)."'
				WHERE 	item_id = '$record_id'";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		header("location: /workspaceGOP/page.report.components/item.list.php?msg=1");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="item" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Item Code Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Item Name* </b></td>
							<td width="80%"><label><input name="item_name" type="text" class="formbutton" id="item_name" size="80" value="<?php echo $item_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Item Description </b></td>
							<td><label><textarea name="item_description" cols="50" rows="10" class="formbutton"><?php echo $item_description; ?></textarea></label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>