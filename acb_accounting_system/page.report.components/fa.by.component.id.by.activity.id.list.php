<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$page_size = getConfigurationValueByName('max_record_per_page');

$record_id = $_GET['record_id'];
$method = $_GET['method'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$delete_record = $_GET['delete_record'];

if($order=="")
{
  	$order = "fa_id";
}
if ($get_msg==1)
{
	$msg = "Record updated successfully!";
	$display_msg = 1;
}

if($cur_page=="")
{
   	$cur_page= 1;
}
if($cur_page==1)
{
	$count_page = 0;
}
else
{
  	$count_page = (($cur_page - 1) * $page_size);
}


if ($record_id != "" && $method == "confirm_delete")
{
?>
	<script type="text/javascript">
	var flg=confirm('Are you sure you want to delete this record?\n\nClick OK to continue. Otherwise click Cancel.\n');
	if (flg==true)
	{
		

		window.location = '/workspaceGOP/page.report.components/fa.by.component.id.by.activity.id.list.php?record_id=<?php echo $record_id; ?>&delete_record=1&method=delete'
	}
	else
	{
		window.location = '/workspaceGOP/page.report.components/fa.by.component.id.by.activity.id.list.php'
	}
	</script> 
    <?php
}

if ($delete_record==1 && $record_id != "" && $method == "delete")
{
	
	$sql = "DELETE FROM tbl_fund_analysis_by_component_by_activity
			WHERE 		fa_id = '$record_id'";
	mysql_query($sql) or die("Error in module: settings.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);
	
	$msg = "Record deleted successfully!";
	$display_msg = 1;
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
  <tr>
    <td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
			$sql1 = "	SELECT 	SQL_BUFFER_RESULT
								SQL_CACHE
								fa_id,
								fa_budget_year,
								fa_account_code,								
								fa_component_id,
								fa_activity_id,
								fa_budget_id,
								fa_item_id,
								fa_number_of_units,
								fa_cost_per_item,
								fa_amount,
								fa_date_insert
						FROM 	tbl_fund_analysis_by_component_by_activity
						ORDER BY $order DESC ";
			$rs2 = mysql_query($sql1) or die("Error in sql1 in module: tbl_fund_analysis_by_component_by_activity.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs2);
			$total_pages = ceil($total_records/$page_size);
			
			$sql2 = "	SELECT 	SQL_BUFFER_RESULT
								SQL_CACHE
								fa_id,
								fa_budget_year,
								fa_account_code,								
								fa_component_id,
								fa_activity_id,
								fa_budget_id,
								fa_item_id,
								fa_number_of_units,
								fa_cost_per_item,
								fa_amount,
								fa_date_insert
						FROM 	tbl_fund_analysis_by_component_by_activity
						ORDER BY $order DESC 
						LIMIT	$count_page, $page_size";
			$rs = mysql_query($sql2) or die("Error in sql2 in module: tbl_fund_analysis_by_component_by_activity.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs);	 
			?>
			<thead>
				<tr>
					<th scope="col" class="rounded-header"><a href="<?php echo $page; ?>?order=fa_component_id" class="whitelabel">Componet ID</a></th>
					<th scope="col" class="rounded-q1"><a href="<?php echo $page; ?>?order=fa_activity_id" class="whitelabel">Activity ID</a></th>
                    <th scope="col" class="rounded-q1"><a href="<?php echo $page; ?>?order=fa_budget_year" class="whitelabel">Budget Year</a></th>
                    <th scope="col" class="rounded-q1"><a href="<?php echo $page; ?>?order=fa_account_code" class="whitelabel">Account Code</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=fa_budget_id" class="whitelabel">Budget ID</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=fa_item_id" class="whitelabel">Item ID</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=fa_number_of_units" class="whitelabel">Number of units</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=fa_cost_per_item" class="whitelabel">Cost per item</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=fa_amount" class="whitelabel">Amount</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=fa_date_insert" class="whitelabel">Date Insert</a></th>
					<th scope="col" class="rounded-q4"><div class="whitelabel">Options</div></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3" class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td colspan="8" class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							
							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="11"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="11"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{
				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs);
					$id  = $rows["fa_id"];
					$fa_budget_year = $rows["fa_budget_year"];
					$fa_account_code = $rows["fa_account_code"];
					$fa_component_id = $rows["fa_component_id"];
					$fa_activity_id = $rows["fa_activity_id"];
					$fa_budget_id = $rows["fa_budget_id"];
					$fa_item_id = $rows["fa_item_id"];
					$fa_number_of_units = $rows["fa_number_of_units"];
					$fa_cost_per_item = $rows["fa_cost_per_item"];
					$fa_amount = $rows["fa_amount"];
					$fa_date_insert = $rows["fa_date_insert"];
					
					$fa_budget_amount = numberFormat($fa_budget_amount);
					$fa_account_code = getSubsidiaryLedgerAccountTitleByid($fa_account_code);
					$fa_component_id = getComponentName($fa_component_id);
					$fa_activity_id = getActivityName($fa_activity_id);
					$fa_item_id = getItemName($fa_item_id);
					#$fa_budget_id = getBudgetName($fa_budget_id);
					
					$fa_cost_per_item = numberFormat($fa_cost_per_item);
					$fa_amount = numberFormat($fa_amount);
				?>      
				 <tr>
					<td><?php echo $fa_component_id; ?>&nbsp;</td>
					<td><?php echo $fa_activity_id; ?>&nbsp;</td>
                    <td><?php echo $fa_budget_year; ?>&nbsp;</td>
                    <td><?php echo $fa_account_code; ?>&nbsp;</td>
					<td><?php echo $fa_budget_id; ?>&nbsp;</td>
					<td><?php echo $fa_item_id; ?>&nbsp;</td>
					<td align="center"><?php echo $fa_number_of_units; ?>&nbsp;</td>
					<td align="right"><?php echo $fa_cost_per_item; ?>&nbsp;</td>
					<td align="right"><?php echo $fa_amount; ?>&nbsp;</td>
					<td align="right"><?php echo $fa_date_insert; ?>&nbsp;</td>
					<td>
						  <?php
						  if ($_SESSION["level"]==1 || $_SESSION["level"]==5)
						  {
						  ?>
								<a href="fa.by.component.id.by.activity.id.edit.php?record_id=<?php echo encrypt($id); ?>">[Edit]</a>
								<a href="fa.by.component.id.by.activity.id.list.php?record_id=<?php echo $id; ?>&method=confirm_delete">[Delete]</a>
						  <?php
						  }
						  ?>					
					</td>
				</tr>
				<?php 
				} 
				?>
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	</td>
  </tr>
</table>
<?php include("./../includes/footer.main.php"); ?>