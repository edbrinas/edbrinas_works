<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$id = decrypt($id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				forex_id,
				forex_currency_code,
				forex_transaction_date,
				forex_currency_value,
				forex_inserted_by,
				forex_date_time_inserted
		FROM	tbl_forex
		WHERE	forex_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: forex.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["forex_id"];
$forex_currency_code = $rows["forex_currency_code"];
$forex_transaction_date = $rows["forex_transaction_date"];
$forex_currency_value = $rows["forex_currency_value"];
$forex_inserted_by = $rows["forex_inserted_by"];
$forex_date_time_inserted = $rows["forex_date_time_inserted"];

$forex_currency_code = getConfigurationValueById($forex_currency_code);
$forex_inserted_by = getFullName($forex_inserted_by,3);

$r_date = explode('-',$forex_transaction_date);
$month = $r_date[0];
$year = $r_date[1];
$forex_transaction_date = date("F - Y", mktime(0,0,0,$month+1, 0, $year));
					
if ($id == $record_id)
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Forex Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Currency Code</b></td>
			<td width="80%"><?php echo $forex_currency_code; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Transaction Date</b></td>
			<td><?php echo $forex_transaction_date; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Rate Value</b></td>
			<td><?php echo $forex_currency_value; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Inserted by</b></td>
			<td><?php echo $forex_inserted_by; ?>&nbsp;</td>
		</tr>	
		<tr>
			<td><b>Date/Time Inserted</b></td>
			<td><?php echo $forex_date_time_inserted; ?>&nbsp;</td>
		</tr>			
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: donor.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
