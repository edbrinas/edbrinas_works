<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$id = $_GET['id'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				activity_id,
				activity_name,
				activity_description
		FROM	tbl_activity
		WHERE	activity_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: activity.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["activity_id"];
$activity_name = $rows["activity_name"];
$activity_description = $rows["activity_description"];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$record_id = $_POST["record_id"];
	$activity_name = $_POST["activity_name"];
	$activity_description = $_POST["activity_description"];
	
	if ($activity_name == "") { $display_msg = 1; $msg .= "Please enter Activity Name"; $msg .= "<br>"; }
	if ($activity_description == "") { $display_msg = 1; $msg .= "Please select Activity Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$activity_name = addslashes($activity_name);
		$activity_description = addslashes($activity_description);
		
		$sql = "UPDATE	tbl_activity
				SET		activity_name = '".trim($activity_name)."',
						activity_description = '".trim($activity_description)."'
				WHERE 	activity_id = '$record_id'";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		header("location: /workspaceGOP/page.report.components/activity.list.php?msg=1");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="activity" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Activity Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
					<tr>
						<td width="20%"><b>Activity Name* </b></td>
						<td width="80%"><input name="activity_name" type="text" class="formbutton" id="activity_name" size="80" value="<?php echo $activity_name; ?>"/></td>
					</tr>	
						<tr valign="top">
							<td><b>Activity Description </b></td>
							<td><textarea name="activity_description" cols="50" rows="10" class="formbutton"><?php echo $activity_description; ?></textarea></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>