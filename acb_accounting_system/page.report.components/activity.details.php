<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$a_name = $_GET['activity_name'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				activity_id,
				activity_name,
				activity_description
		FROM	tbl_activity
		WHERE	activity_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: activity.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["activity_id"];
$activity_name = $rows["activity_name"];
$activity_description = $rows["activity_description"];

if ($id == $record_id && $a_name == md5($activity_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Activity Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Activity Name</b></td>
			<td width="80%"><?php echo $activity_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Activity Description</b></td>
			<td><?php echo $activity_description; ?>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: activity.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
?>
