<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$id = $_GET['id'];
$date_time_inserted = date("Y-m-d H:i:s");
$current_year = date("Y");
$id = decrypt($id);

$sql = "SELECT	SQL_BUFFER_RESULT
				SQL_CACHE	
				sl_id,
				sl_account_code,
				sl_account_title,			
				sl_budget_id,
				sl_donor_id,
				sl_component_id,
				sl_activity_id,
				sl_other_cost_id,
				sl_staff_id,
				sl_benefits_id,
				sl_vehicle_id,
				sl_equipment_id,
				sl_item_id
		FROM 	tbl_subsidiary_ledger
		WHERE	sl_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: subsidiary.ledger.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["sl_id"];
$sl_account_code = $rows["sl_account_code"];
$sl_account_title = $rows["sl_account_title"];
$sl_budget_id = $rows["sl_budget_id"];
$sl_donor_id = $rows["sl_donor_id"];
$sl_component_id = $rows["sl_component_id"];
$sl_activity_id = $rows["sl_activity_id"];
$sl_other_cost_id = $rows["sl_other_cost_id"];
$sl_staff_id = $rows["sl_staff_id"];
$sl_benefits_id = $rows["sl_benefits_id"];
$sl_vehicle_id = $rows["sl_vehicle_id"];
$sl_equipment_id = $rows["sl_equipment_id"];
$sl_item_id  = $rows["sl_item_id"];

if ($sl_budget_id == 1 ) { $sl_budget_id_state = "CHECKED"; }
if ($sl_donor_id == 1 ) { $sl_donor_id_state = "CHECKED"; }
if ($sl_component_id == 1 ) { $sl_component_id_state = "CHECKED"; }
if ($sl_activity_id == 1 ) { $sl_activity_id_state = "CHECKED"; }
if ($sl_other_cost_id == 1 ) { $sl_other_cost_id_state = "CHECKED"; }
if ($sl_staff_id == 1 ) { $sl_staff_id_state = "CHECKED"; }
if ($sl_benefits_id == 1 ) { $sl_benefits_id_state = "CHECKED"; }
if ($sl_vehicle_id == 1 ) { $sl_vehicle_id_state = "CHECKED"; }
if ($sl_equipment_id == 1 ) { $sl_equipment_id_state = "CHECKED"; }
if ($sl_item_id == 1 ) { $sl_item_id_state = "CHECKED"; }

/*
$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				sl_details_starting_balance,
				sl_details_ending_balance
		WHERE	sl_details_reference_number = '$record_id'";
$rs = mysql_query($sql) or die("Error in sql in module: activity.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);				
*/
$sl_starting_balance = $rows["sl_details_starting_balance"];
$sl_ending_balance = $rows["sl_details_ending_balance"];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$record_id = $_POST["record_id"];
	$sl_account_code = 	$_POST["sl_account_code"];
	$sl_account_title = $_POST["sl_account_title"];
	#$sl_starting_balance = $_POST["sl_starting_balance"];
	#$sl_ending_balance = $_POST["sl_ending_balance"];
	$sl_budget_id = $_POST["sl_budget_id"];
	$sl_donor_id = $_POST["sl_donor_id"];
	$sl_component_id = $_POST["sl_component_id"];
	$sl_activity_id = $_POST["sl_activity_id"];
	$sl_other_cost_id = $_POST["sl_other_cost_id"];
	$sl_staff_id = $_POST["sl_staff_id"];
	$sl_benefits_id = $_POST["sl_benefits_id"];
	$sl_vehicle_id = $_POST["sl_vehicle_id"];
	$sl_equipment_id = $_POST["sl_equipment_id"];
	$sl_item_id = $_POST["sl_item_id"];
	
	if ($sl_account_code == "") { $display_msg = 1; $msg .= "Please enter Account Code"; $msg .= "<br>"; }
	if ($sl_account_title == "") { $display_msg = 1; $msg .= "Please enter Account Title"; $msg .= "<br>"; }
	#if ($sl_starting_balance == "") { $display_msg = 1; $msg .= "Please enter Starting Balance"; $msg .= "<br>"; }
	#if ($sl_ending_balance == "") { $display_msg = 1; $msg .= "Please enter Ending Balance"; $msg .= "<br>"; }
	#if (checkIfNumber($sl_starting_balance) == 0)	{ $display_msg = 1; $msg .= "Starting balance must be a number"; $msg .= "<br>"; }
	#if (checkIfNumber($sl_ending_balance) == 0)	{ $display_msg = 1; $msg .= "Ending balance must be a number"; $msg .= "<br>"; }
	
	if ($sl_budget_id == 1 ) { $sl_budget_id_state = "CHECKED"; }
	if ($sl_donor_id == 1 ) { $sl_donor_id_state = "CHECKED"; }
	if ($sl_component_id == 1 ) { $sl_component_id_state = "CHECKED"; }
	if ($sl_activity_id == 1 ) { $sl_activity_id_state = "CHECKED"; }
	if ($sl_other_cost_id == 1 ) { $sl_other_cost_id_state = "CHECKED"; }
	if ($sl_staff_id == 1 ) { $sl_staff_id_state = "CHECKED"; }
	if ($sl_benefits_id == 1 ) { $sl_benefits_id_state = "CHECKED"; }
	if ($sl_vehicle_id == 1 ) { $sl_vehicle_id_state = "CHECKED"; }
	if ($sl_equipment_id == 1 ) { $sl_equipment_id_state = "CHECKED"; }
	if ($sl_item_id == 1 ) { $sl_item_id_state = "CHECKED"; }

	if ($msg == "")
	{
		$sql = "UPDATE 	tbl_subsidiary_ledger
				SET		sl_account_code = '".trim($sl_account_code)."',
						sl_account_title = '".trim($sl_account_title)."',
						sl_budget_id = '".trim($sl_budget_id)."',
						sl_donor_id = '".trim($sl_donor_id)."',
						sl_component_id = '".trim($sl_component_id)."',
						sl_activity_id = '".trim($sl_activity_id)."',
						sl_other_cost_id = '".trim($sl_other_cost_id)."',
						sl_staff_id = '".trim($sl_staff_id)."',
						sl_benefits_id = '".trim($sl_benefits_id)."',
						sl_vehicle_id = '".trim($sl_vehicle_id)."',
						sl_equipment_id = '".trim($sl_equipment_id)."',
						sl_item_id = '".trim($sl_item_id)."',
						sl_added_by = '$user_id',
						sl_date_added = '$date_time_inserted'
				WHERE	sl_id = '$record_id'";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		
		#$sql = "UPDATE 	tbl_subsidiary_ledger_details
		#		SET		sl_details_year = '$current_year',
		#				sl_details_starting_balance = '".trim($sl_starting_balance)."',
		#				sl_details_ending_balance = '".trim($sl_ending_balance)."'
		#		WHERE	sl_details_reference_number = '$record_id'";
		#mysql_query($sql) or die($sql.mysql_error());
		#insertEventLog($user_id,$sql);
		
		$display_msg = 1;
		header("location: /workspaceGOP/page.report.components/subsidiary.ledger.list.php?msg=1");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main" style='table-layout:fixed' >
	<tr>
		<td>
			<form name="subsidiary_ledger" method="post">
				<table id="rounded-add-entries" align="center" >
					<thead>
						<tr>
							<th colspan="2" scope="col" class="rounded-header"><div class="main" align="left"><strong>Subsidiary Ledger Details</strong></div></th>
							<th colspan="2" scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="2" class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td colspan="2" class="rounded-foot-right" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Account Code*</b></td>
							<td width="30%"><input name="sl_account_code" type="text" id="sl_account_code" value="<?php echo $sl_account_code; ?>" size="50" class="formbutton"></td>
							<td width="20%"><b>Account Title* </b></td>
							<td width="30%"><input name="sl_account_title" type="text" id="sl_account_title" value="<?php echo $sl_account_title; ?>" size="50" class="formbutton"></td>
						</tr>
						<!--
						<tr valign="top">
						  <td><b>Starting Balance</b></td>
					      <td><input name="sl_starting_balance" type="text" id="sl_starting_balance" value="<?php echo $sl_starting_balance; ?>" class="formbutton"></td>
					      <td><b>Ending Balance</b></td>
					      <td><input name="sl_ending_balance" type="text" id="sl_ending_balance" value="<?php echo $sl_ending_balance; ?>" class="formbutton"></td>
					  	</tr>
						-->
						<tr valign="top">
						  <td colspan="4">
						    <table id="rounded-add-entries" align="center">
                              <thead>
                                <tr>
                                  <th colspan="5" scope="col" class="rounded-header"><div class="main" align="left"><strong>Report Details</strong></div></th>
                                  <th colspan="5" scope="col" class="rounded-q4"><div class="main" align="right"><strong>Check the Required Fields</strong></div></th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td align="center">Budget ID</td>
                                  <td align="center">Donor ID</td>
                                  <td align="center">Component ID</td>
                                  <td align="center">Activity ID</td>
                                  <td align="center">Other Cost/Services ID</td>
                                  <td align="center">Staff ID</td>
                                  <td align="center">Benefits ID</td>
                                  <td align="center">Vehicle ID</td>
                                  <td align="center">Equipment ID</td>
                                  <td align="center">Item ID</td>
                                </tr>
                                <tr>
                                  <td align="center"><input type="checkbox" name="sl_budget_id" value="1" <?php echo $sl_budget_id_state; ?> /></td>
                                  <td align="center"><input type="checkbox" name="sl_donor_id" value="1" <?php echo $sl_donor_id_state; ?> /></td>
                                  <td align="center"><input type="checkbox" name="sl_component_id" value="1" <?php echo $sl_component_id_state; ?> /></td>
                                  <td align="center"><input type="checkbox" name="sl_activity_id" value="1" <?php echo $sl_activity_id_state; ?> /></td>
                                  <td align="center"><input type="checkbox" name="sl_other_cost_id" value="1" <?php echo $sl_other_cost_id_state; ?> /></td>
                                  <td align="center"><input type="checkbox" name="sl_staff_id" value="1" <?php echo $sl_staff_id_state; ?> /></td>
                                  <td align="center"><input type="checkbox" name="sl_benefits_id" value="1" <?php echo $sl_benefits_id_state; ?> /></td>
                                  <td align="center"><input type="checkbox" name="sl_vehicle_id" value="1" <?php echo $sl_vehicle_id_state; ?> /></td>
                                  <td align="center"><input type="checkbox" name="sl_equipment_id" value="1" <?php echo $sl_equipment_id_state; ?> /></td>
                                  <td align="center"><input type="checkbox" name="sl_item_id" value="1" <?php echo $sl_item_id_state; ?> /></td>
                                </tr>
                                <tr>
                                  <td align="center" colspan="10">&nbsp;</td>
                                </tr>
                              </tbody>
                            </table>						  </td>
					  </tr>
						<tr valign="top">
					  		<td colspan="4">&nbsp;</td>			  
					  	</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>