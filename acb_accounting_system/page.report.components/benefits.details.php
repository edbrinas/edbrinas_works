<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$b_name = $_GET['benefits_name'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				benefits_id,
				benefits_name,
				benefits_description
		FROM	tbl_benefits
		WHERE	benefits_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: benefits.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["benefits_id"];
$benefits_name = $rows["benefits_name"];
$benefits_description = $rows["benefits_description"];

if ($id == $record_id && $b_name == md5($benefits_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Benefits Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Benefits Name</b></td>
			<td width="80%"><?php echo $benefits_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Benefits Description</b></td>
			<td><?php echo $benefits_description; ?>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: benefits.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
