<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$c_name = $_GET['cs_name'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				cs_id,
				cs_name,
				cs_description
		FROM	tbl_other_cost_services
		WHERE	cs_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: cs.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["cs_id"];
$cs_name = $rows["cs_name"];
$cs_description = $rows["cs_description"];

if ($id == $record_id && $c_name == md5($cs_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Other/Cost Details</strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Other/Cost Name</b></td>
			<td width="80%"><?php echo $cs_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Other/Cost Description</b></td>
			<td><?php echo $cs_description; ?>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: cs.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
