<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$p_name = $_GET['payee_name'];
$id = decrypt($id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				payee_id,
				payee_name,
				payee_type,
				payee_contact_name,
				payee_contact_title,
				payee_address,
				payee_city,
				payee_phone_number,
				payee_fax_number
		FROM	tbl_payee
		WHERE	payee_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: settings.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["payee_id"];
$payee_name = $rows["payee_name"];
$payee_type = $rows["payee_type"];
$payee_contact_name = $rows["payee_contact_name"];
$payee_contact_title = $rows["payee_contact_title"];
$payee_address = $rows["payee_address"];
$payee_city = $rows["payee_city"];
$payee_phone_number = $rows["payee_phone_number"];
$payee_fax_number = $rows["payee_fax_number"];

$payee_name = stripslashes($payee_name);
$payee_contact_name = stripslashes($payee_contact_name);
$payee_address = stripslashes($payee_address);

$payee_type = getConfigurationValueById($payee_type);
$payee_contact_title = getConfigurationValueById($payee_contact_title);

if ($id == $record_id && $p_name == md5($payee_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Payee Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Payee Name </b></td>
			<td width="80%"><?php echo $payee_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Payee Type</b></td>
			<td><?php echo $payee_type; ?></td>
		</tr>
		<tr>
			<td><b>Title</b></td>
			<td><?php echo $payee_contact_title; ?></td>
		</tr>
		<tr>
			<td><b>Contact Name</b></td>
			<td><?php echo $payee_contact_name; ?>&nbsp;</td>
		</tr>	
		<tr>
			<td valign="top"><b>Contact Address</b></td>
			<td><?php echo $payee_address; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>City</b></td>
			<td><?php echo $payee_city; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Phone Number</b></td>
			<td><?php echo $payee_phone_number; ?>&nbsp;</td>
		</tr>				
		<tr>
			<td><b>Fax Number</b></td>
			<td><?php echo $payee_fax_number; ?>&nbsp;</td>
		</tr>																																
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: work.order.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
?>