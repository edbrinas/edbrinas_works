<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$s_name = $_GET['staff_name'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				staff_id,
				staff_name,
				staff_description
		FROM	tbl_staff
		WHERE	staff_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: staff.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["staff_id"];
$staff_name = $rows["staff_name"];
$staff_description = $rows["staff_description"];

if ($id == $record_id && $s_name == md5($staff_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Staff Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Staff Name</b></td>
			<td width="80%"><?php echo $staff_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Staff Description</b></td>
			<td><?php echo $staff_description; ?>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: staff.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
