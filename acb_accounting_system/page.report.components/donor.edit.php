<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$id = $_GET['id'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				donor_id,
				donor_name,
				donor_description
		FROM	tbl_donor
		WHERE	donor_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: donor.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["donor_id"];
$donor_name = $rows["donor_name"];
$donor_description = $rows["donor_description"];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$record_id = $_POST["record_id"];
	$donor_name = $_POST["donor_name"];
	$donor_description = $_POST["donor_description"];
	
	if ($donor_name == "") { $display_msg = 1; $msg .= "Please enter Donor Name"; $msg .= "<br>"; }
	if ($donor_description == "") { $display_msg = 1; $msg .= "Please select Donor Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$donor_name = addslashes($donor_name);
		$donor_description = addslashes($donor_description);
		
		$sql = "UPDATE	tbl_donor
				SET		donor_name= '".trim($donor_name)."',
						donor_description = '".trim($donor_description)."'
				WHERE 	donor_id = '$record_id'";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		header("location: /workspaceGOP/page.report.components/donor.list.php?msg=1");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="donor" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Donor Details  </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Donor Name* </b></td>
							<td width="80%"><label><input name="donor_name" type="text" class="formbutton" id="donor_name" size="80" value="<?php echo $donor_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Donor Description </b></td>
							<td><label><textarea name="donor_description" cols="50" rows="10" class="formbutton"><?php echo $donor_description; ?></textarea></label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>