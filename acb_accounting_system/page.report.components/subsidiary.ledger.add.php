<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$date_time_inserted = date("Y-m-d H:i:s");
$current_year = date("Y");

if ($_POST['Submit'] == 'Save')
{
	$msg = "";
	
	$sl_account_code = 	$_POST["sl_account_code"];
	$sl_account_title = $_POST["sl_account_title"];
	$sl_budget_id = $_POST["sl_budget_id"];
	$sl_donor_id = $_POST["sl_donor_id"];
	$sl_component_id = $_POST["sl_component_id"];
	$sl_activity_id = $_POST["sl_activity_id"];
	$sl_other_cost_id = $_POST["sl_other_cost_id"];
	$sl_staff_id = $_POST["sl_staff_id"];
	$sl_benefits_id = $_POST["sl_benefits_id"];
	$sl_vehicle_id = $_POST["sl_vehicle_id"];
	$sl_equipment_id = $_POST["sl_equipment_id"];
	$sl_item_id = $_POST["sl_item_id"];
	
	if ($sl_account_code == "") { $display_msg = 1; $msg .= "Please enter Account Code"; $msg .= "<br>"; }
	if ($sl_account_title == "") { $display_msg = 1; $msg .= "Please enter Account Title"; $msg .= "<br>"; }
	#if ($sl_starting_balance == "") { $display_msg = 1; $msg .= "Please enter Starting Balance"; $msg .= "<br>"; }
	#if (checkIfNumber($sl_starting_balance) == 0)	{ $display_msg = 1; $msg .= "Starting balance must be a number"; $msg .= "<br>"; }
		
	if ($sl_budget_id == 1 ) { $sl_budget_id_state = "CHECKED"; }
	if ($sl_donor_id == 1 ) { $sl_donor_id_state = "CHECKED"; }
	if ($sl_component_id == 1 ) { $sl_component_id_state = "CHECKED"; }
	if ($sl_activity_id == 1 ) { $sl_activity_id_state = "CHECKED"; }
	if ($sl_other_cost_id == 1 ) { $sl_other_cost_id_state = "CHECKED"; }
	if ($sl_staff_id == 1 ) { $sl_staff_id_state = "CHECKED"; }
	if ($sl_benefits_id == 1 ) { $sl_benefits_id_state = "CHECKED"; }
	if ($sl_vehicle_id == 1 ) { $sl_vehicle_id_state = "CHECKED"; }
	if ($sl_equipment_id == 1 ) { $sl_equipment_id_state = "CHECKED"; }
	if ($sl_item_id == 1 ) { $sl_item_id = "CHECKED"; }
	

	$sql = "SELECT	sl_account_code,
					sl_account_title
			FROM	tbl_subsidiary_ledger
			WHERE	sl_account_code = '$sl_account_code'
			AND		sl_account_title = '$sl_account_title'";
	$rs = mysql_query($sql) or die("Error in sql1 in module: user.list.php ".$sql." ".mysql_error());
	$total_records = mysql_num_rows($rs);
	
	if ($total_records >= 1) { $display_msg = 1; $msg .= "Account Title and Account Code already exists!"; $msg .= "<br>"; } 
			
	if ($msg == "")
	{
		$sql = "INSERT INTO tbl_subsidiary_ledger
							(
							sl_account_code,
							sl_account_title,
							sl_budget_id,
							sl_donor_id,
							sl_component_id,
							sl_activity_id,
							sl_other_cost_id,
							sl_staff_id,
							sl_benefits_id,
							sl_vehicle_id,
							sl_equipment_id,
							sl_item_id,
							sl_added_by,
							sl_date_added
							)
				VALUES
							(
							'".trim($sl_account_code)."',
							'".trim($sl_account_title)."',
							'".trim($sl_budget_id)."',
							'".trim($sl_donor_id)."',
							'".trim($sl_component_id)."',
							'".trim($sl_activity_id)."',
							'".trim($sl_other_cost_id)."',
							'".trim($sl_staff_id)."',
							'".trim($sl_benefits_id)."',
							'".trim($sl_vehicle_id)."',
							'".trim($sl_equipment_id)."',
							'".trim($sl_item_id)."',
							'$user_id',
							'$date_time_inserted'
							)";
		$rs = mysql_query($sql) or die($sql.mysql_error());	
		$sl_details_id = mysql_insert_id();
		insertEventLog($user_id,$sql);
		
		$sql2 = "	SELECT 	cfg_id
					FROM	tbl_config
					WHERE	cfg_name = 'currency_type'";
		$rs2 = mysql_query($sql2) or die("Error in sql in function".$sql2." ".mysql_error());	
		while($rows2=mysql_fetch_array($rs2))
		{
			$currency_code = $rows2['cfg_id'];
			$sql3 = "INSERT INTO tbl_subsidiary_ledger_details
								(
								sl_details_reference_number,
								sl_details_year,
								sl_details_currency_code,
								sl_details_starting_balance,
								sl_details_ending_balance
								)
					VALUES
								(
								'$sl_details_id',
								'$current_year',
								'$currency_code',
								'0',
								'0'							
								)";
			mysql_query($sql3) or die($sql3.mysql_error());	
		}		
		insertEventLog($user_id,$sql);
		
		$display_msg = 1;
		$msg = "Record inserted successfully!";
		
		$sl_account_code = "";
		$sl_account_title = "";
		$sl_starting_balance = "";
		$sl_budget_id_state = "";
		$sl_donor_id_state = "";
		$sl_component_id_state = "";
		$sl_activity_id_state = "";
		$sl_other_cost_id_state = "";
		$sl_staff_id_state = "";
		$sl_benefits_id_state = "";
		$sl_vehicle_id_state = "";
		$sl_equipment_id_state = "";
		$sl_item_id_state = "";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="subsidiary_ledger" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th colspan="2" scope="col" class="rounded-header"><div class="main" align="left"><strong>Subsidiary Ledger Details</strong></div></th>
							<th colspan="2" scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="2" class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Save" class="formbutton"></td>
							<td colspan="2" class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="10"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Account Code*</b></td>
							<td width="30%"><input name="sl_account_code" type="text" id="sl_account_code" value="<?php echo $sl_account_code; ?>" size="50" class="formbutton"></td>
							<td width="20%"><b>Account Title* </b></td>
							<td width="30%"><input name="sl_account_title" type="text" id="sl_account_title" value="<?php echo $sl_account_title; ?>" size="50" class="formbutton"></td>
						</tr>
						<tr valign="top">
					  		<td colspan="4">
								<table id="rounded-add-entries" align="center">
									<thead>
										<tr>
										<th colspan="5" scope="col" class="rounded-header"><div class="main" align="left"><strong>Report Details</strong></div></th>
										<th colspan="5" scope="col" class="rounded-q4"><div class="main" align="right"><strong>Check the Required Fields</strong></div></th>	
										</tr>
									</thead>
									<tbody>
										<tr>
											<td align="center">Budget ID</td>
											<td align="center">Donor ID</td>
											<td align="center">Component ID</td>
											<td align="center">Activity ID</td>
											<td align="center">Other Cost/Services ID</td>
											<td align="center">Staff ID</td>
											<td align="center">Benefits ID</td>
											<td align="center">Vehicle ID</td>
											<td align="center">Equipment ID</td>
                                            <td align="center">Item ID</td>
										</tr>
										<tr>
											<td align="center"><input type="checkbox" name="sl_budget_id" value="1" <?php echo $sl_budget_id_state; ?>></td>
											<td align="center"><input type="checkbox" name="sl_donor_id" value="1" <?php echo $sl_donor_id_state; ?>></td>
											<td align="center"><input type="checkbox" name="sl_component_id" value="1" <?php echo $sl_component_id_state; ?>></td>
											<td align="center"><input type="checkbox" name="sl_activity_id" value="1" <?php echo $sl_activity_id_state; ?>></td>
											<td align="center"><input type="checkbox" name="sl_other_cost_id" value="1" <?php echo $sl_other_cost_id_state; ?>></td>
											<td align="center"><input type="checkbox" name="sl_staff_id" value="1" <?php echo $sl_staff_id_state; ?>></td>
											<td align="center"><input type="checkbox" name="sl_benefits_id" value="1" <?php echo $sl_benefits_id_state; ?>></td>
											<td align="center"><input type="checkbox" name="sl_vehicle_id" value="1" <?php echo $sl_vehicle_id_state; ?>></td>
											<td align="center"><input type="checkbox" name="sl_equipment_id" value="1" <?php echo $sl_equipment_id_state; ?>></td>
                                            <td align="center"><input type="checkbox" name="sl_item_id" value="1" <?php echo $sl_item_state; ?>></td>
										</tr>	
										<tr>
											<td align="center" colspan="10">&nbsp;</td>
										</tr>																				
									</tbody>
								</table>							
							</td>			  
					  	</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>