<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$v_name = $_GET['item_name'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				item_id,
				item_name,
				item_description
		FROM	tbl_item_code
		WHERE	item_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: item.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["item_id"];
$item_name = $rows["item_name"];
$item_description = $rows["item_description"];

if ($id == $record_id && $v_name == md5($item_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>items Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Item Name</b></td>
			<td width="80%"><?php echo $item_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Item Description</b></td>
			<td><?php echo $item_description; ?>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: item.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
