<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$b_name = $_GET['budget_name'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				budget_id,
				budget_name,
				budget_description
		FROM	tbl_budget
		WHERE	budget_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: budget.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["budget_id"];
$budget_name = $rows["budget_name"];
$budget_description = $rows["budget_description"];

if ($id == $record_id && $b_name == md5($budget_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Donor Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Donor Name</b></td>
			<td width="80%"><?php echo $budget_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Donor Description</b></td>
			<td><?php echo $budget_description; ?>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: budget.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
