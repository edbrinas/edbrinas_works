<?php 
session_start();
include("./../includes/header.main.php");

$display_msg = 0;
$user_id = $_SESSION["id"];
$record_id = $_GET['record_id'];

$record_id = decrypt($record_id);
$year = date("Y");
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				fa_id,
				fa_budget_year,
				fa_account_code,
				fa_component_id,
				fa_budget
		FROM 	tbl_fund_analysis_by_component_by_budget
		WHERE 	fa_id = '$record_id'";
$rs = mysql_query($sql) or die("Error in sql2 in module: fa.by.awp.by.budget.line.edit ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);
$id = $rows["fa_id"];
$fa_budget_year = $rows["fa_budget_year"];
$fa_account_code = $rows["fa_account_code"];
$fa_component_id = $rows["fa_component_id"];
$fa_budget = $rows["fa_budget"];

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$record_id = $_POST['record_id'];
	$fa_budget_year = $_POST['fa_budget_year'];
	$fa_account_code = $_POST['fa_account_code'];
	$fa_component_id = $_POST['fa_component_id'];
	$fa_budget = $_POST['fa_budget'];
	
	if ($fa_account_code=="") { $display_msg = 1; $msg .= "Please select account code!"; $msg = "<br>"; }
	if ($fa_component_id=="") { $display_msg = 1; $msg .= "Please select component id!"; $msg = "<br>"; }
	if ($fa_budget=="") { $display_msg = 1; $msg .= "Please enter budget amount!"; $msg = "<br>"; }
	
	if (checkIfNumber($fa_budget) == 0)	{ $display_msg = 1; $msg .= "Budget must be a number"; $msg .= "<br>"; }
	
	if ($msg=="")
	{
		$sql = "UPDATE	tbl_fund_analysis_by_component_by_budget 
				SET		fa_account_code = '$fa_account_code',
						fa_budget_year = '$fa_budget_year',
						fa_component_id = '$fa_component_id',
						fa_budget = '$fa_budget'
				WHERE	fa_id = '$record_id'";
		mysql_query($sql) or die("Query Error " .mysql_error());
		insertEventLog($user_id,$sql);
		header("location: /workspaceGOP/page.report.components/fa.by.component.id.by.budget.id.list.php?msg=1");
	}

}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="system_users" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><b>Fund Analysis - Per Component ID, Per Budget Line </b></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input type="hidden" name="record_id" value="<?php echo $id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><strong>Account Code * </strong></td>
							<td width="80%">
							<?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											sl_id,
											sl_account_code,
											sl_account_title
									FROM 	tbl_subsidiary_ledger
									ORDER BY sl_account_title";
							$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
							?>
							<select name='fa_account_code' class='formbutton'>
							<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['sl_id'];
									$column_name = $data['sl_account_title'];
									$column_code = $data['sl_account_code'];
									?>
									<option value='<?php echo $column_id; ?>'
									<?php
									if ($column_id==$fa_account_code)
									{
									?>
										SELECTED
									<?php
									}
									?>
									><?php echo $column_name." [ ".$column_code." ]"; 
								}
								?></option>
							</select>	
							</td>
						</tr>
						<tr>
						  <td><b>Budget Year *</b></td>
						  <td>
                          		<select name='fa_budget_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $fa_budget_year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
                          
                          </td>
					  </tr>                           
						<tr>
						  <td><strong>Component ID* </strong></td>
						  <td>
							<?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											component_id,
											component_name,
											component_description
									FROM 	tbl_component
									ORDER BY component_name";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?>
							<select name='fa_component_id' class='formbutton'>
							<option value='' SELECTED>[Please Select]</option>
							<?php
							while($data=mysql_fetch_array($rs))
							{
								$column_id = $data['component_id'];
								$column_name = $data['component_name'];
								$column_description = $data['component_description'];
								?>
								<option value='<?php echo $column_id; ?>'
								<?php
								if ($fa_component_id == $column_id)
								{
								?>
									SELECTED
								<?php
								}
								?>
								><?php echo $column_name;
							}
							?></option>
							</select>						  
							</td>
						</tr>						
						<tr>
						  <td><strong>Alloted Budget* </strong></td>
						  <td><input name="fa_budget" type="text" value="<?php echo $fa_budget; ?>" class='formbutton'></td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>