<?php 
include("./../includes/header.report.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$v_name = $_GET['vehicle_name'];

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				vehicle_id,
				vehicle_name,
				vehicle_description
		FROM	tbl_vehicle
		WHERE	vehicle_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql in module: vehicle.details.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["vehicle_id"];
$vehicle_name = $rows["vehicle_name"];
$vehicle_description = $rows["vehicle_description"];

if ($id == $record_id && $v_name == md5($vehicle_name))
{
?>

<table id="rounded-add-entries" align="center">
	<thead>
		<tr>
			<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Vehicles Details </strong></div></th>
			<th scope="col" class="rounded-q4"><div class="main" align="right"><strong></strong></div></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td class="rounded-foot-left" align="left">&nbsp;</td>
			<td class="rounded-foot-right" align="right">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td width="20%"><b>Vehicles Name</b></td>
			<td width="80%"><?php echo $vehicle_name; ?>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Vehicles Description</b></td>
			<td><?php echo $vehicle_description; ?>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: vehicle.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
	</script>
	<?php
}
