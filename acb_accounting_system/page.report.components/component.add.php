<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

if ($_POST['Submit'] == 'Save')
{
	$msg = "";
	
	$component_name = $_POST["component_name"];
	$component_description = $_POST["component_description"];
	
	if ($component_name == "") { $display_msg = 1; $msg .= "Please enter Component Name"; $msg .= "<br>"; }
	if ($component_description == "") { $display_msg = 1; $msg .= "Please select Component Description"; $msg .= "<br>"; }

	if ($msg == "")
	{
		$component_name = addslashes($component_name);
		$component_description = addslashes($component_description);
		
		$sql = "INSERT INTO tbl_component
							(
							component_name,
							component_description
							)
				VALUES
							(
							'".trim($component_name)."',
							'".trim($component_description)."'
							)";
		mysql_query($sql) or die($sql.mysql_error());	
		
		insertEventLog($user_id,$sql);
		$display_msg = 1;
		$msg = "Record inserted successfully!";
		
		$component_name = "";
		$component_description = "";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="journal_voucher" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Component Details</strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Save" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Component Name* </b></td>
							<td width="80%"><label><input name="component_name" type="text" class="formbutton" id="component_name" size="80" value="<?php echo $component_name; ?>"/></label></td>
						</tr>
						<tr valign="top">
							<td><b>Component Description </b></td>
							<td><label><textarea name="component_description" cols="50" rows="10" class="formbutton"><?php echo $component_description; ?></textarea></label></td>
						</tr>																																
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>