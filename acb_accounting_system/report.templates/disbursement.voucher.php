<?php 
include("./../connections/cn.php");
include("./../includes/check.login.php");
include("./../includes/function.php"); 
include("./../includes/http.function.php"); 
include("./../includes/timer.includes.php");

$user_id = $_SESSION["id"];

$report_name = "DISBURSEMENT VOUCHER";
$rep_id = $_GET['id'];
$rep_dv_number = $_GET['dv_number'];
$rep_id = decrypt($rep_id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE	
				dv_id,
				dv_year,
				dv_sequence_number,
				dv_number,
				dv_type,
				dv_currency_code,
				dv_date,
				dv_payee,
				dv_description,
				dv_prepared_by,
				dv_prepared_by_date,
				dv_certified_by,
				dv_certified_by_date,
				dv_recommended_by,
				dv_recommended_by_date,
				dv_approved_by,
				dv_approved_by_date
		FROM 	tbl_disbursement_voucher
		WHERE	dv_id = '$rep_id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: disbursement.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$dv_id = $rows["dv_id"];
$dv_type = $rows["dv_type"];
$dv_number = $rows["dv_number"];
$dv_currency_code = $rows["dv_currency_code"];
$dv_date = $rows["dv_date"];
$dv_payee = $rows["dv_payee"];
$dv_description = $rows["dv_description"];
$dv_prepared_by = $rows["dv_prepared_by"];
$dv_prepared_by_date = $rows["dv_prepared_by_date"];
$dv_certified_by = $rows["dv_certified_by"];
$dv_certified_by_date = $rows["dv_certified_by_date"];
$dv_recommended_by = $rows["dv_recommended_by"];
$dv_recommended_by_date = $rows["dv_recommended_by_date"];
$dv_approved_by = $rows["dv_approved_by"];
$dv_approved_by_date = $rows["dv_approved_by_date"];
$dv_inserted_date = $rows["dv_inserted_date"];

$dv_payee = getPayeeById($dv_payee);
$dv_currency_name = getConfigurationDescriptionById($dv_currency_code);
$dv_currency_code = getConfigurationValueById($dv_currency_code);

if ($dv_prepared_by!=0) { $dv_prepared_by = getFullName($dv_prepared_by,3); } else { $dv_prepared_by=""; }
if ($dv_certified_by!=0) { $dv_certified_by = getFullName($dv_certified_by,3); } else { $dv_certified_by=""; }
if ($dv_recommended_by!=0) { $dv_recommended_by = getFullName($dv_recommended_by,3); } else { $dv_recommended_by=""; }
if ($dv_approved_by!=0) { $dv_approved_by = getFullName($dv_approved_by,3); } else { $dv_approved_by=""; }

if ($dv_certified_by_date == "0000-00-00 00:00:00") { $dv_certified_by_date = ""; }
if ($dv_recommended_by_date == "0000-00-00 00:00:00") { $dv_recommended_by_date = ""; }
if ($dv_approved_by_date == "0000-00-00 00:00:00")  { $dv_approved_by_date = ""; }
if ($dv_cheque_number_date == "0000-00-00 00:00:00")  { $dv_cheque_number_date = ""; }
if ($dv_prepared_by_date == "0000-00-00 00:00:00")  { $dv_prepared_by_date = ""; }

$report_name = upperCase($report_name);
$company_name = upperCase(getCompanyName());
$company_address = upperCase(getCompanyAddress());

if ($rep_id == $dv_id && $rep_dv_number == md5($dv_number))
{
insertEventLog($user_id,"Exported DV".$dv_number." to file.");
$string = "
<table width='100%' border='0' cellspacing='2' cellpadding='2' style='table-layout:fixed'>
	<tr>
		<td scope='col'>
		<!--Start of inner report table -->
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing'>
			  <tr>
				<th scope='col'><font face='Verdana' size='1'>
					$report_name<br>
					$company_name<br>
					$company_address<br>
					ACB-$dv_currency_code
					</font>
				</th>
			  </tr>
			  <tr>
				<th scope='col'>&nbsp;</th>
			  </tr>
			  <tr>
				<th scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td width='10%' rowspan='2' valign='top' scope='col'><font face='Verdana' size='1'><strong>Payee</strong></font></td>
					<td width='40%' rowspan='2' scope='col'><font face='Verdana' size='1'>$dv_payee</font></td>
					<td width='10%' scope='col'><div align='left'><font face='Verdana' size='1'><strong>DV Number</strong></font></div></td>
					<td width='40%' scope='col'><font face='Verdana' size='1'>$dv_number</font></td>
				  </tr>
				  <tr>
					<td><div align='left'><font face='Verdana' size='1'><strong>Date</strong></font></td>
					<td><font face='Verdana' size='1'>$dv_date</font></td>
				  </tr>
				</table>	
				</th>
			  </tr>
			  <tr>
				<td scope='col'>&nbsp;</td>
			  </tr>   
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<th scope='col' align='left'><font face='Verdana' size='1'><strong>Description/Particulars</strong></font></th>
				  </tr>
				  <tr>
					<td><font face='Verdana' size='1'>$dv_description&nbsp;</font></td>
				  </tr>
				</table>	
				</td>
			  </tr>
			  <tr>
				<td scope='col'>&nbsp;</td>
			  </tr>  
			  <tr>
				<td scope='col'>
				
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC' >
					<tr>
						<th width='5%' scope='col'><font face='Verdana' size='1'>Account Code</font></th>
						<th width='20%' scope='col'><font face='Verdana' size='1'>Account Title</font></th>
						<th width='6%' scope='col'><font face='Verdana' size='1'>Currency</font></th>
						<th width='10%' scope='col'><font face='Verdana' size='1'>Debit</font></th>
						<th width='10' scope='col'><font face='Verdana' size='1'>Credit</font></th>
						<th width='5%' scope='col'><font face='Verdana' size='1'>Budget ID</font></th>
						<th width='5%' scope='col'><font face='Verdana' size='1'>Donor ID</font></th>
						<th width='7%' scope='col'><font face='Verdana' size='1'>Component ID</font></th>
						<th width='5%' scope='col'><font face='Verdana' size='1'>Activity ID</font></th>
						<th width='6%' scope='col'><font face='Verdana' size='1'>Other Cost / Services ID</font></th>
						<th width='4%' scope='col'><font face='Verdana' size='1'>Staff ID</font></th>
						<th width='5%' scope='col'><font face='Verdana' size='1'>Benefits ID</font></th>
						<th width='5%' scope='col'><font face='Verdana' size='1'>Vehicle ID</font></th>
						<th width='7%' scope='col'><font face='Verdana' size='1'>Equipment ID</font></th>
					</tr>";
					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										dv_details_id,
										dv_details_account_code,
										dv_details_reference_number,
										dv_details_debit,
										dv_details_credit,
										dv_details_budget_id,
										dv_details_donor_id,
										dv_details_component_id,
										dv_details_activity_id,
										dv_details_other_cost_id,
										dv_details_staff_id,
										dv_details_benefits_id,
										dv_details_vehicle_id,
										dv_details_equipment_id
								FROM 	tbl_disbursement_voucher_details
								WHERE	dv_details_reference_number = '$dv_id'";
					$rs2 = mysql_query($sql2) or die("Error in sql2 in module: journal.voucher.details.php ".$sql2." ".mysql_error());
					$total_rows = mysql_num_rows($rs2);	
					if($total_rows == 0)
					{
						$string .="
						<tr>
						  <td colspan='14'><font face='Verdana' size='1'>No records found!</font></td>
						</tr>";
					}
					else
					{
						for($row_number=0;$row_number<$total_rows;$row_number++)
						{
							$rows2 = mysql_fetch_array($rs2);
							$dv_details_id = $rows2["dv_details_id"];
							$dv_details_reference_number = $rows2["dv_details_reference_number"];
							$dv_details_account_code = $rows2["dv_details_account_code"];
							$dv_details_debit = $rows2["dv_details_debit"];
							$dv_details_credit = $rows2["dv_details_credit"];
							$dv_details_budget_id = $rows2["dv_details_budget_id"];
							$dv_details_donor_id = $rows2["dv_details_donor_id"];
							$dv_details_component_id = $rows2["dv_details_component_id"];
							$dv_details_activity_id = $rows2["dv_details_activity_id"];
							$dv_details_other_cost_id = $rows2["dv_details_other_cost_id"];
							$dv_details_staff_id = $rows2["dv_details_staff_id"];
							$dv_details_benefits_id = $rows2["dv_details_benefits_id"];
							$dv_details_vehicle_id = $rows2["dv_details_vehicle_id"];
							$dv_details_equipment_id = $rows2["dv_details_equipment_id"];
							
							
							$dv_details_account_title = getSubsidiaryLedgerAccountTitleByid($dv_details_account_code);
							$dv_details_account_code = getSubsidiaryLedgerAccountCodeByid($dv_details_account_code);
							
							$dv_details_budget_id  = getBudgetName($dv_details_budget_id);
							$dv_details_donor_id  = getDonorName($dv_details_donor_id);
							$dv_details_component_id  = getComponentName($dv_details_component_id);
							$dv_details_activity_id = getActivityName($dv_details_activity_id);
							$dv_details_other_cost_id  = getOtherCostName($dv_details_other_cost_id);
							$dv_details_staff_id  = getStaffName($dv_details_staff_id);
							$dv_details_benefits_id  = getBenefitsName($dv_details_benefits_id);
							$dv_details_vehicle_id  = getVehicleName($dv_details_vehicle_id);
							$dv_details_equipment_id  = getEquipmentName($dv_details_equipment_id);
							
							$arr_debit[] = $dv_details_debit;
							$arr_credit[] = $dv_details_credit;
							
							$dv_details_debit = numberFormat($dv_details_debit);
							$dv_details_credit = numberFormat($dv_details_credit);
						$string .= "
						<tr>
							<td><font face='Verdana' size='1'>$dv_details_account_code</font></td>
							<td><font face='Verdana' size='1'>$dv_details_account_title</font></td>
							<td><font face='Verdana' size='1'>$dv_currency_code</font></td>
							<td align='right'><font face='Verdana' size='1'>$dv_details_debit</font></td>
							<td align='right'><font face='Verdana' size='1'>$dv_details_credit</font></td>
							<td><font face='Verdana' size='1'>$dv_details_budget_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$dv_details_donor_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$dv_details_component_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$dv_details_activity_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$dv_details_other_cost_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$dv_details_staff_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$dv_details_benefits_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$dv_details_vehicle_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$dv_details_equipment_id&nbsp;</font></td>
							";
						} 
					$string .="
					</tr>
					";
					} 
				if ($arr_credit != 0 && $arr_debit != 0)
				{
					$total_debit = array_sum($arr_debit);
					$total_credit = array_sum($arr_credit);
				}
				$transaction_total = numberFormat($total_debit);
				$total_debit = convertToWords($total_debit);
				$string .= "
				</table>	
				</td>
			  </tr>
			  <tr>
				<td scope='col'>&nbsp;</td>
			  </tr>
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td width='15%' valign='top' scope='col' align='left'><font face='Verdana' size='1'><strong>Prepared by</strong></font></td>
					<td width='30%' rowspan='2' scope='col'>&nbsp;</td>
					<td width='15%' scope='col' align='left'><font face='Verdana' size='1'><strong>Certified by</strong></font></td>
					<td width='30%' scope='col' align='center' valign='top'><font face='Verdana' size='1'>Expenditure legal and proper with supporting documents &amp; adequate funds</font></td>
				  </tr>
				  <tr>
					<td width='10%' valign='top' scope='col' align='left'>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					<td width='30%' scope='col'>&nbsp;</td>
					</tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					<td scope='col' align='center'><font face='Verdana' size='1'>$dv_prepared_by&nbsp;</font></td>
					<td align='left'>&nbsp;</td>
					<td align='center'><font face='Verdana' size='1'>$dv_certified_by&nbsp;</font></td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><font face='Verdana' size='1'><strong>Prepared Date</strong></font></td>
					<td scope='col' align='center'><font face='Verdana' size='1'>$dv_prepared_by_date&nbsp;</font></td>
					<td align='left'><font face='Verdana' size='1'><strong>Certified Date</strong></font></td>
					<td align='center'><font face='Verdana' size='1'>$dv_certified_by_date&nbsp;</font></td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><font face='Verdana' size='1'><strong>Recommended by</strong></font></td>
					<td rowspan='2' scope='col'>&nbsp;</td>
					<td align='left'><font face='Verdana' size='1'><strong>Approved by</strong></font></td>
					<td rowspan='2'>&nbsp;</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					</tr>
				  <tr>
					<td valign='top' scope='col'>&nbsp;</td>
					<td scope='col' align='center'><font face='Verdana' size='1'>$dv_recommended_by&nbsp;</font></td>
					<td align='left'>&nbsp;</td>
					<td align='center'><font face='Verdana' size='1'>$dv_approved_by&nbsp;</font></td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><font face='Verdana' size='1'><strong>Recommended Date</strong></font></td>
					<td scope='col' align='center'><font face='Verdana' size='1'>$dv_recommended_by_date&nbsp;</font></td>
					<td align='left'><font face='Verdana' size='1'><strong>Approved Date</strong></font></td>
					<td align='center'><font face='Verdana' size='1'>$dv_approved_by_date&nbsp;</font></td>
				  </tr>
				</table>
				</td>
			  </tr>
			  <tr>
				<td scope='col'>&nbsp;</td>
			  </tr>
			  <tr>
				<td scope='col'><table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td scope='col'><font face='Verdana' size='1'><div align='left'>Received from <b>$company_name</b> the sum of <b>$total_debit $dv_currency_name</b> ($dv_currency_code $transaction_total) in payment for the above account.</div></font></td>
				  </tr>
				</table>	
				</td>
			  </tr>
			  <tr>
				<td scope='col'>&nbsp;</td>
			  </tr>
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<th colspan='2' scope='col'><font face='Verdana' size='1'>DETAILS</font></th>
					<th colspan='2' scope='col'><font face='Verdana' size='1'>ACKNOWLEDGEMENT</font></th>
					</tr>
				  <tr>
					<td width='10%' scope='col'><div align='left'><font face='Verdana' size='1'>Amount</font></div></td>
					<td width='40%' scope='col'><div align='left'><font face='Verdana' size='1'>$dv_currency_code&nbsp;$transaction_total</font></div></td>
					<td width='10%' scope='col'><div align='left'><font face='Verdana' size='1'>Signature</font></div></td>
					<td width='40%' scope='col'>&nbsp;</td>
				  </tr>
				  <tr>
					<td scope='col'><div align='left'><font face='Verdana' size='1'>Check Number</font></div></td>
					<td scope='col'><div align='left'><font face='Verdana' size='1'>";
					
					$dv_cheque_number = getChequeNumber($dv_id);
					if (count($dv_cheque_number)!=0)
					{
						foreach ($dv_cheque_number AS $dv_cheque_number)
						{
							$string .= "$dv_cheque_number<br>";
						}
					}
					
					$string .="
					</font></div></td>
					<td scope='col'><div align='left'><font face='Verdana' size='1'>Printed Name</font></div></td>
					<td scope='col'>&nbsp;</td>
				  </tr>
				  <tr>
					<td scope='col'><div align='left'><font face='Verdana' size='1'>Check Date</font></div></td>
					<td scope='col'><div align='left'><font face='Verdana' size='1'>";
                    $dv_cheque_number_date = getChequeNumber($dv_id);
					if (count($dv_cheque_number_date)!=0)
					{
						foreach ($dv_cheque_number_date AS $dv_cheque_number_date)
						{
							$string .= "$dv_cheque_number_date"; 
						}
					}
					$string .= "</font></div></td>
					<td scope='col'><div align='left'><font face='Verdana' size='1'>Date</font></div></td>
					<td scope='col'>&nbsp;</td>
				  </tr>
				</table>
				</td>
			  </tr>
			</table>		
		<!-- End of inner report table -->
		</td>
	</tr>
</table>
";

$filename = "DV_".$dv_number."_".$dv_date.".doc";

header("Content-type: application/octet-stream"); 
header("Content-Disposition: attachment; filename=$filename"); 
header("Pragma: no-cache"); 
header("Expires: 0");
print $string;

}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: journal.voucher.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!");
		window.close();
	</script>
	<?php
}
?>