<?php 
include("./../connections/cn.php");
include("./../includes/check.login.php");
include("./../includes/function.php"); 
include("./../includes/http.function.php"); 
include("./../includes/timer.includes.php");

$user_id = $_SESSION["id"];

$report_name = "DISBURSEMENT VOUCHER";
$rep_id = $_GET['id'];
$rep_jv_number = $_GET['jv_number'];
$rep_id = decrypt($rep_id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE	
				jv_id,			
				jv_type,
				jv_number,
				jv_currency_code,
				jv_date,
				jv_payee,
				jv_description,
				jv_prepared_by,
				jv_certified_by,
				jv_certified_by_date,
				jv_recommended_by,
				jv_recommended_by_date,
				jv_approved_by,
				jv_approved_by_date,
				jv_inserted_date
		FROM 	tbl_journal_voucher
		WHERE	jv_id = '$rep_id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: journal.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$jv_id = $rows["jv_id"];
$jv_type = $rows["jv_type"];
$jv_number = $rows["jv_number"];
$jv_currency_code = $rows["jv_currency_code"];
$jv_date = $rows["jv_date"];
$jv_payee = $rows["jv_payee"];
$jv_description = $rows["jv_description"];
$jv_prepared_by = $rows["jv_prepared_by"];
$jv_certified_by = $rows["jv_certified_by"];
$jv_certified_by_date = $rows["jv_certified_by_date"];
$jv_recommended_by = $rows["jv_recommended_by"];
$jv_recommended_by_date = $rows["jv_recommended_by_date"];
$jv_approved_by = $rows["jv_approved_by"];
$jv_approved_by_date = $rows["jv_approved_by_date"];
$jv_inserted_date = $rows["jv_inserted_date"];

$jv_payee = getPayeeById($jv_payee);
$jv_currency_name = getConfigurationDescriptionById($jv_currency_code);
$jv_currency_code = getConfigurationValueById($jv_currency_code);

if ($jv_prepared_by!=0) { $jv_prepared_by = getFullName($jv_prepared_by,3); } else { $jv_prepared_by=""; }
if ($jv_certified_by!=0) { $jv_certified_by = getFullName($jv_certified_by,3); } else { $jv_certified_by=""; }
if ($jv_recommended_by!=0) { $jv_recommended_by = getFullName($jv_recommended_by,3); } else { $jv_recommended_by=""; }
if ($jv_approved_by!=0) { $jv_approved_by = getFullName($jv_approved_by,3); } else { $jv_approved_by=""; }

if ($jv_certified_by_date == "0000-00-00 00:00:00") { $jv_certified_by_date = ""; }
if ($jv_recommended_by_date == "0000-00-00 00:00:00") { $jv_recommended_by_date = ""; }
if ($jv_approved_by_date == "0000-00-00 00:00:00")  { $jv_approved_by_date = ""; }

$report_name = upperCase($report_name);
$company_name = upperCase(getCompanyName());
$company_address = upperCase(getCompanyAddress());

if ($rep_id == $jv_id && $rep_jv_number == md5($jv_number))
{
insertEventLog($user_id,"Exported JV".$jv_number." to file.");
$string = "
<table width='100%' border='0' cellspacing='2' cellpadding='2' style='table-layout:fixed'>
	<tr>
		<td scope='col'>
		<!--Start of inner report table -->
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing'>
			  <tr>
				<th scope='col'><font face='Verdana' size='1'>
					$report_name<br>
					$company_name<br>
					$company_address<br>
					ACB-$jv_currency_code
					</font>
				</th>
			  </tr>
			  <tr>
				<th scope='col'>&nbsp;</th>
			  </tr>
			  <tr>
				<th scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td width='10%' rowspan='2' valign='top' scope='col'><font face='Verdana' size='1'><strong>Payee</strong></font></td>
					<td width='40%' rowspan='2' scope='col'><font face='Verdana' size='1'>$jv_payee</font></td>
					<td width='10%' scope='col'><div align='left'><font face='Verdana' size='1'><strong>JV Number</strong></font></div></td>
					<td width='40%' scope='col'><font face='Verdana' size='1'>$jv_number</font></td>
				  </tr>
				  <tr>
					<td><div align='left'><font face='Verdana' size='1'><strong>Date</strong></font></td>
					<td><font face='Verdana' size='1'>$jv_date</font></td>
				  </tr>
				</table>	
				</th>
			  </tr>
			  <tr>
				<td scope='col'>&nbsp;</td>
			  </tr>   
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<th scope='col' align='left'><font face='Verdana' size='1'><strong>Description/Particulars</strong></font></th>
				  </tr>
				  <tr>
					<td><font face='Verdana' size='1'>$jv_description&nbsp;</font></td>
				  </tr>
				</table>	
				</td>
			  </tr>
			  <tr>
				<td scope='col'>&nbsp;</td>
			  </tr>  
			  <tr>
				<td scope='col'>
				
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC' >
					<tr>
						<th width='10%' scope='col'><font face='Verdana' size='1'>Account Code</font></th>
						<th width='12%' scope='col'><font face='Verdana' size='1'>Account Title</font></th>
						<th width='6%' scope='col'><font face='Verdana' size='1'>Currency</font></th>
						<th width='10%' scope='col'><font face='Verdana' size='1'>Debit</font></th>
						<th width='10%' scope='col'><font face='Verdana' size='1'>Credit</font></th>
						<th width='5%' scope='col'><font face='Verdana' size='1'>Budget ID</font></th>
						<th width='4%' scope='col'><font face='Verdana' size='1'>Donor ID</font></th>
						<th width='7%' scope='col'><font face='Verdana' size='1'>Component ID</font></th>
						<th width='5%' scope='col'><font face='Verdana' size='1'>Activity ID</font></th>
						<th width='9%' scope='col'><font face='Verdana' size='1'>Other Cost/Services ID</font></th>
						<th width='4%' scope='col'><font face='Verdana' size='1'>Staff ID</font></th>
						<th width='6%' scope='col'><font face='Verdana' size='1'>Benefits ID</font></th>
						<th width='5%' scope='col'><font face='Verdana' size='1'>Vehicle ID</font></th>
						<th width='7%' scope='col'><font face='Verdana' size='1'>Equipment ID</font></th>
					</tr>";
					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										jv_details_id,
										jv_details_account_code,
										jv_details_reference_number,
										jv_details_debit,
										jv_details_credit,
										jv_details_budget_id,
										jv_details_donor_id,
										jv_details_component_id,
										jv_details_activity_id,
										jv_details_other_cost_id,
										jv_details_staff_id,
										jv_details_benefits_id,
										jv_details_vehicle_id,
										jv_details_equipment_id
								FROM 	tbl_journal_voucher_details
								WHERE	jv_details_reference_number = '$jv_id'";
					$rs2 = mysql_query($sql2) or die("Error in sql2 in module: journal.voucher.details.php ".$sql2." ".mysql_error());
					$total_rows = mysql_num_rows($rs2);	
					if($total_rows == 0)
					{
						$string .="
						<tr>
						  <td colspan='14'><font face='Verdana' size='1'>No records found!</font></td>
						</tr>";
					}
					else
					{
						for($row_number=0;$row_number<$total_rows;$row_number++)
						{
							$rows2 = mysql_fetch_array($rs2);
							$jv_details_id = $rows2["jv_details_id"];
							$jv_details_reference_number = $rows2["jv_details_reference_number"];
							$jv_details_account_code = $rows2["jv_details_account_code"];
							$jv_details_debit = $rows2["jv_details_debit"];
							$jv_details_credit = $rows2["jv_details_credit"];
							$jv_details_budget_id = $rows2["jv_details_budget_id"];
							$jv_details_donor_id = $rows2["jv_details_donor_id"];
							$jv_details_component_id = $rows2["jv_details_component_id"];
							$jv_details_activity_id = $rows2["jv_details_activity_id"];
							$jv_details_other_cost_id = $rows2["jv_details_other_cost_id"];
							$jv_details_staff_id = $rows2["jv_details_staff_id"];
							$jv_details_benefits_id = $rows2["jv_details_benefits_id"];
							$jv_details_vehicle_id = $rows2["jv_details_vehicle_id"];
							$jv_details_equipment_id = $rows2["jv_details_equipment_id"];
							
							
							$jv_details_account_title = getSubsidiaryLedgerAccountTitleByid($jv_details_account_code);
							$jv_details_account_code = getSubsidiaryLedgerAccountCodeByid($jv_details_account_code);
							
							$jv_details_budget_id  = getBudgetName($jv_details_budget_id);
							$jv_details_donor_id  = getDonorName($jv_details_donor_id);
							$jv_details_component_id  = getComponentName($jv_details_component_id);
							$jv_details_activity_id = getActivityName($jv_details_activity_id);
							$jv_details_other_cost_id  = getOtherCostName($jv_details_other_cost_id);
							$jv_details_staff_id  = getStaffName($jv_details_staff_id);
							$jv_details_benefits_id  = getBenefitsName($jv_details_benefits_id);
							$jv_details_vehicle_id  = getVehicleName($jv_details_vehicle_id);
							$jv_details_equipment_id  = getEquipmentName($jv_details_equipment_id);
							
							$arr_debit[] = $jv_details_debit;
							$arr_credit[] = $jv_details_credit;
							
							$jv_details_debit = numberFormat($jv_details_debit);
							$jv_details_credit = numberFormat($jv_details_credit);
						$string .= "
						<tr>
							<td><font face='Verdana' size='1'>$jv_details_account_code</font></td>
							<td><font face='Verdana' size='1'>$jv_details_account_title</font></td>
							<td><font face='Verdana' size='1'>$jv_currency_code</font></td>
							<td align='right'><font face='Verdana' size='1'>$jv_details_debit</font></td>
							<td align='right'><font face='Verdana' size='1'>$jv_details_credit</font></td>
							<td><font face='Verdana' size='1'>$jv_details_budget_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$jv_details_donor_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$jv_details_component_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$jv_details_activity_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$jv_details_other_cost_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$jv_details_staff_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$jv_details_benefits_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$jv_details_vehicle_id&nbsp;</font></td>
							<td><font face='Verdana' size='1'>$jv_details_equipment_id&nbsp;</font></td>
							";
						} 
					$string .="
					</tr>
					";
					} 
				if ($arr_credit != 0 && $arr_debit != 0)
				{
					$total_debit = array_sum($arr_debit);
					$total_credit = array_sum($arr_credit);
				}
				$transaction_total = numberFormat($total_debit);
				$total_debit = convertToWords($total_debit);
				$string .= "
				</table>	
				</td>
			  </tr>
			  <tr>
				<td scope='col'>&nbsp;</td>
			  </tr>
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td width='15%' valign='top' scope='col' align='left'><font face='Verdana' size='1'><strong>Prepared by</strong></font></td>
					<td width='30%' rowspan='2' scope='col'>&nbsp;</td>
					<td width='15%' scope='col' align='left'><font face='Verdana' size='1'><strong>Certified by</strong></font></td>
					<td width='30%' rowspan='2' scope='col'>&nbsp;</td>
				  </tr>
				  <tr>
					<td width='10%' valign='top' scope='col' align='left'>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					</tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					<td scope='col' align='center'><font face='Verdana' size='1'>$jv_prepared_by&nbsp;</font></td>
					<td align='left'>&nbsp;</td>
					<td align='center'><font face='Verdana' size='1'>$jv_certified_by&nbsp;</font></td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><font face='Verdana' size='1'><strong>Prepared Date</strong></font></td>
					<td scope='col' align='center'><font face='Verdana' size='1'>$jv_inserted_date&nbsp;</font></td>
					<td align='left'><font face='Verdana' size='1'><strong>Certified Date</strong></font></td>
					<td align='center'><font face='Verdana' size='1'>$jv_certified_by_date&nbsp;</font></td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><font face='Verdana' size='1'><strong>Recommended by</strong></font></td>
					<td rowspan='2' scope='col'>&nbsp;</td>
					<td align='left'><font face='Verdana' size='1'><strong>Approved by</strong></font></td>
					<td rowspan='2'>&nbsp;</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					</tr>
				  <tr>
					<td valign='top' scope='col'>&nbsp;</td>
					<td scope='col' align='center'><font face='Verdana' size='1'>$jv_recommended_by&nbsp;</font></td>
					<td align='left'>&nbsp;</td>
					<td align='center'><font face='Verdana' size='1'>$jv_approved_by&nbsp;</font></td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><font face='Verdana' size='1'><strong>Recommended Date</strong></font></td>
					<td scope='col' align='center'><font face='Verdana' size='1'>$jv_recommended_by_date&nbsp;</font></td>
					<td align='left'><font face='Verdana' size='1'><strong>Approved Date</strong></font></td>
					<td align='center'><font face='Verdana' size='1'>$jv_approved_by_date&nbsp;</font></td>
				  </tr>
				</table>
				</td>
			  </tr>
			  <tr>
				<td scope='col'>&nbsp;</td>
			  </tr>
			  <tr>
				<td scope='col'><table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td scope='col'><font face='Verdana' size='1'><div align='left'>Received from <b>$company_name</b> the sum of <b>$total_debit $jv_currency_name</b> ($jv_currency_code $transaction_total) in payment for the above account.</div></font></td>
				  </tr>
				</table>	
				</td>
			  </tr>
			  <tr>
				<td scope='col'>&nbsp;</td>
			  </tr>
			</table>		
		<!-- End of inner report table -->
		</td>
	</tr>
</table>
";

$filename = "JV_".$jv_number."_".$jv_date.".doc";

header("Content-type: application/octet-stream"); 
header("Content-Disposition: attachment; filename=$filename"); 
header("Pragma: no-cache"); 
header("Expires: 0");
print $string;

}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: journal.voucher.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!");
		window.close();
	</script>
	<?php
}
?>