<?php 
$report_name = "Journal Voucher";
include("./../includes/header.report.php");
$report_name = "JOURNAL VOUCHER";
$rep_id = $_GET['id'];
$rep_jv_number = $_GET['jv_number'];
$rep_id = decrypt($rep_id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE	
				jv_id,
				jv_number,
				jv_type,
				jv_currency_code,
				jv_date,
				jv_payee,
				jv_description,
				jv_prepared_by,
				jv_certified_by,
				jv_recommended_by,
				jv_approved_by,
				jv_prepared_by_date,
				jv_certified_by_date,
				jv_recommended_by_date,
				jv_approved_by_date,
				jv_inserted_date
		FROM 	tbl_journal_voucher
		WHERE	jv_id = '$rep_id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: journal.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$jv_id = $rows["jv_id"];
$jv_type = $rows["jv_type"];
$jv_number = $rows["jv_number"];
$jv_currency_code = $rows["jv_currency_code"];
$jv_date = $rows["jv_date"];
$jv_payee = $rows["jv_payee"];
$jv_description = $rows["jv_description"];
$jv_prepared_by = $rows["jv_prepared_by"];
$jv_prepared_by_date = $rows["jv_prepared_by_date"];
$jv_certified_by = $rows["jv_certified_by"];
$jv_certified_by_date = $rows["jv_certified_by_date"];
$jv_recommended_by = $rows["jv_recommended_by"];
$jv_recommended_by_date = $rows["jv_recommended_by_date"];
$jv_approved_by = $rows["jv_approved_by"];
$jv_approved_by_date = $rows["jv_approved_by_date"];
$jv_inserted_date = $rows["jv_inserted_date"];

$jv_payee = getPayeeById($jv_payee);
$jv_description = stripslashes($jv_description);
$jv_currency_name = getConfigurationDescriptionById($jv_currency_code);
$jv_currency_code = getConfigurationValueById($jv_currency_code);

$jv_prepared_by_id = $jv_prepared_by;
$jv_certified_by_id = $jv_certified_by;
$jv_recommended_by_id = $jv_recommended_by;
$jv_approved_by_id = $jv_approved_by;

if ($jv_prepared_by!=0) { $jv_prepared_by = getFullName($jv_prepared_by,3); } else { $jv_prepared_by=""; }
if ($jv_certified_by!=0) { $jv_certified_by = getFullName($jv_certified_by,3); } else { $jv_certified_by=""; }
if ($jv_recommended_by!=0) { $jv_recommended_by = getFullName($jv_recommended_by,3); } else { $jv_recommended_by=""; }
if ($jv_approved_by!=0) { $jv_approved_by = getFullName($jv_approved_by,3); } else { $jv_approved_by=""; }

if ($jv_certified_by_date == "0000-00-00 00:00:00") { $jv_certified_by_date = ""; }
if ($jv_certified_by_date == "0000-00-00 00:00:00") { $jv_certified_by_date = ""; }
if ($jv_recommended_by_date == "0000-00-00 00:00:00") { $jv_recommended_by_date = ""; }
if ($jv_approved_by_date == "0000-00-00 00:00:00")  { $jv_approved_by_date = ""; }

if ($rep_id == $jv_id && $rep_jv_number == md5($jv_number))
{
?>
<table width='100%' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>
	<tr>
		<td scope='col'>
		<!--Start of inner report table -->
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing'>
			  <tr>
				<th scope='col'>
					<?php echo upperCase($report_name); ?><br>
					<?php echo upperCase(getCompanyName()); ?><br>
					<?php echo upperCase(getCompanyAddress()); ?><br>
					ACB-<?php echo $jv_currency_code; ?>				
				</th>
			  </tr>
			  <tr>
				<th scope='col'>&nbsp;</th>
			  </tr>
			  <tr>
				<th scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td width='10%' rowspan='2' valign='top' scope='col'><strong>Payee</strong></td>
					<td width='40%' rowspan='2' scope='col'><?php echo $jv_payee; ?>&nbsp;</td>
					<td width='10%' scope='col'><div align='left'><strong>JV Number </strong></div></td>
					<td width='40%' scope='col'><?php echo $jv_number; ?></td>
				  </tr>
				  <tr>
					<td><div align='left'><strong>Date</strong></div></td>
					<td><?php echo $jv_date; ?></td>
				  </tr>
				</table>				
				</th>
			  </tr>   
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<th scope='col' align='left'><strong>Description/Particulars</strong></th>
				  </tr>
				  <tr>
					<td><?php echo $jv_description; ?>&nbsp;</td>
				  </tr>
				</table>				
				</td>
			  </tr>  
			  <tr>
				<td scope='col'>
				
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC' >
					<tr>
						<th width="10%" scope='col'>Account Code </th>
						<th width="12%" scope='col'>Account Title </th>
						<th width="6%" scope='col'>Currency</th>
						<th width="10%" scope='col'>Debit</th>
						<th width="10%" scope='col'>Credit</th>
						<th width="5%" scope='col'>Budget ID </th>
						<th width="4%" scope='col'>Donor ID </th>
						<th width="7%" scope='col'>Component ID </th>
						<th width="5%" scope='col'>Activity ID </th>
						<th width="9%" scope='col'>Other Cost/Services ID </th>
						<th width="4%" scope='col'>Staff ID </th>
						<th width="6%" scope='col'>Benefits ID </th>
						<th width="5%" scope='col'>Vehicle ID </th>
						<th width="7%" scope='col'>Equipment ID </th>
					</tr>
				   <?php
			
					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										jv_details_id,
										jv_details_account_code,
										jv_details_reference_number,
										jv_details_debit,
										jv_details_credit,
										jv_details_budget_id,
										jv_details_donor_id,
										jv_details_component_id,
										jv_details_activity_id,
										jv_details_other_cost_id,
										jv_details_staff_id,
										jv_details_benefits_id,
										jv_details_vehicle_id,
										jv_details_equipment_id
								FROM 	tbl_journal_voucher_details
								WHERE	jv_details_reference_number = '$jv_id'
								ORDER BY jv_details_id ASC";
					$rs2 = mysql_query($sql2) or die("Error in sql2 in module: journal.voucher.details.php ".$sql2." ".mysql_error());
					$total_rows = mysql_num_rows($rs2);	
					if($total_rows == 0)
					{
						?>
						<tr>
						  <td colspan='14'><div class='redlabel' align='left'>No records found!</div></td>
						</tr>
						<?php
					}
					else
					{
						for($row_number=0;$row_number<$total_rows;$row_number++)
						{
							$rows2 = mysql_fetch_array($rs2);
							$jv_details_id = $rows2["jv_details_id"];
							$jv_details_reference_number = $rows2["jv_details_reference_number"];
							$jv_details_account_code = $rows2["jv_details_account_code"];
							$jv_details_debit = $rows2["jv_details_debit"];
							$jv_details_credit = $rows2["jv_details_credit"];
							$jv_details_budget_id = $rows2["jv_details_budget_id"];
							$jv_details_donor_id = $rows2["jv_details_donor_id"];
							$jv_details_component_id = $rows2["jv_details_component_id"];
							$jv_details_activity_id = $rows2["jv_details_activity_id"];
							$jv_details_other_cost_id = $rows2["jv_details_other_cost_id"];
							$jv_details_staff_id = $rows2["jv_details_staff_id"];
							$jv_details_benefits_id = $rows2["jv_details_benefits_id"];
							$jv_details_vehicle_id = $rows2["jv_details_vehicle_id"];
							$jv_details_equipment_id = $rows2["jv_details_equipment_id"];
							
							
							$jv_details_account_title = getSubsidiaryLedgerAccountTitleByid($jv_details_account_code);
							$jv_details_account_code = getSubsidiaryLedgerAccountCodeByid($jv_details_account_code);
							
							$jv_details_budget_id  = getBudgetName($jv_details_budget_id);
							$jv_details_donor_id  = getDonorName($jv_details_donor_id);
							$jv_details_component_id  = getComponentName($jv_details_component_id);
							$jv_details_activity_id = getActivityName($jv_details_activity_id);
							$jv_details_other_cost_id  = getOtherCostName($jv_details_other_cost_id);
							$jv_details_staff_id  = getStaffName($jv_details_staff_id);
							$jv_details_benefits_id  = getBenefitsName($jv_details_benefits_id);
							$jv_details_vehicle_id  = getVehicleName($jv_details_vehicle_id);
							$jv_details_equipment_id  = getEquipmentName($jv_details_equipment_id);
							
						?>
						<tr>
							<td><?php echo $jv_details_account_code; ?></td>
							<td><?php echo $jv_details_account_title; ?></td>
							<td><?php echo $jv_currency_code; ?></td>
							<td align='right'><?php echo numberFormat($jv_details_debit); ?></td>
							<td align='right'><?php echo numberFormat($jv_details_credit); ?></td>
							<td><?php echo $jv_details_budget_id; ?>&nbsp;</td>
							<td><?php echo $jv_details_donor_id; ?>&nbsp;</td>
							<td><?php echo $jv_details_component_id; ?>&nbsp;</td>
							<td><?php echo $jv_details_activity_id; ?>&nbsp;</td>
							<td><?php echo $jv_details_other_cost_id; ?>&nbsp;</td>
							<td><?php echo $jv_details_staff_id; ?>&nbsp;</td>
							<td><?php echo $jv_details_benefits_id; ?>&nbsp;</td>
							<td><?php echo $jv_details_vehicle_id; ?>&nbsp;</td>
							<td><?php echo $jv_details_equipment_id; ?>&nbsp;</td>
						<?php 
						} 
						?>
					</tr>
					<?php 
					} 
				?>
				</table>				
				</td>
			  </tr>
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td width='15%' valign='top' scope='col' align='left'><strong>Prepared by</strong></td>
					<td width='30%' rowspan='2' scope='col' align='center'>
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($jv_prepared_by_id);
							}
						?>
					</td>
					<td width='15%' scope='col' align='left'><strong>Certified by</strong></td>
					<td width='30%' scope='col' align="center">*Expenditure legal and proper with supporting documents &amp; adequate funds</td>
				  </tr>
				  <tr>
					<td width='10%' valign='top' scope='col' align='left'>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					<td width='30%' scope='col' align='center'>
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($jv_certified_by_id);
							}
						?>
					</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					<td scope='col' align='center'><b><?php echo $jv_prepared_by; ?></b>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					<td align='center'><b><?php echo $jv_certified_by; ?></b>&nbsp;</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><strong>Prepared Date </strong></td>
					<td scope='col' align='center'><?php echo $jv_prepared_by_date; ?>&nbsp;</td>
					<td align='left'><strong>Certified Date </strong></td>
					<td align='center'><?php echo $jv_certified_by_date; ?>&nbsp;</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><strong>Recommended by</strong></td>
					<td rowspan='2' scope='col' align='center'>
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($jv_recommended_by_id);
							}
						?>
					</td>
					<td align='left'><strong>Approved by </strong></td>
					<td rowspan='2' align='center'>
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($jv_approved_by_id);
							}
						?>
					</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					</tr>
				  <tr>
					<td valign='top' scope='col'>&nbsp;</td>
					<td scope='col' align='center'><b><?php echo $jv_recommended_by; ?></b>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					<td align='center'><b>
						<?php 
							echo $jv_approved_by;
							if($jv_approved_by_id == 21)
								//echo ' - Alternate OIC-ACB and FA Unit Head';		
								echo ' - FA Unit Head';						 
						?>
					</b>&nbsp;</td>				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><strong>Recommended Date</strong></td>
					<td scope='col' align='center'><?php echo $jv_recommended_by_date; ?>&nbsp;</td>
					<td align='left'><strong>Approved Date </strong></td>
					<td align='center'><?php echo $jv_approved_by_date; ?>&nbsp;</td>
				  </tr>
				</table>				
				</td>
			  </tr>
			</table>		
		<!-- End of inner report table -->
		</td>
	</tr>
</table>
<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
	<a href="/workspaceGOP/report.templates/journal.voucher.php?id=<?php echo encrypt($jv_id); ?>&jv_number=<?php echo md5($jv_number); ?>"><img src="/workspaceGOP/images/save.png" border="0" title="Export to word"></a>
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: journal.voucher.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!");
		window.close();
	</script>
	<?php
}
?>