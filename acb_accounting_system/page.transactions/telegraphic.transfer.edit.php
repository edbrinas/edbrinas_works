<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$display_msg = "";
$rep_id = $_GET['id'];
$rep_tt_number = $_GET['tt_number'];
$rep_id = decrypt($rep_id);

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$year = date("Y");
$month = date("m");
$day = date("d");
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;
$date_time_inserted = date("Y-m-d H:i:s");

if ($_POST['Submit'] == 'Update')
{
	$msg = "";
	
	$tt_id = $_POST['tt_id'];
	$tt_year = $_POST['tt_year'];
	$tt_month = $_POST['tt_month'];
	$tt_day = $_POST['tt_day'];
	$tt_country = $_POST['tt_country'];
	$tt_bank_from = $_POST['tt_bank_from'];
	$tt_bank_to = $_POST['tt_bank_to'];
	$tt_bank_to_address = $_POST['tt_bank_to_address'];
	$tt_bank_to_account_number = $_POST['tt_bank_to_account_number'];	
	$tt_currency = $_POST['tt_currency'];
	$tt_amount = $_POST['tt_amount'];
	$tt_swift_code = $_POST['tt_swift_code']; 
	$tt_iban = $_POST['tt_iban'];
	$tt_beneficiary = $_POST['tt_beneficiary'];
	$tt_purpose = $_POST['tt_purpose'];
	$tt_dv_number = $_POST['tt_dv_number'];
	$tt_amount_transferred = $_POST['tt_amount_transferred'];
	$tt_bank_charges = $_POST['tt_bank_charges'];

	$tt_number = getTelegraphicTransferSequenceNumber();
	
	$tt_date = dateFormat($tt_month,$tt_day,$tt_year);
	
	if ($tt_country == "") { $display_msg=1; $msg .= "Please select country"; $msg .="<br>"; }
	if ($tt_bank_from == "") { $display_msg=1; $msg .= "Please select bank from"; $msg .="<br>"; }
	if ($tt_bank_to == "") { $display_msg=1; $msg .= "Please enter bank to"; $msg .="<br>"; }
	if ($tt_bank_to_address == "") { $display_msg=1; $msg .= "Please enter bank to address"; $msg .="<br>"; }
	if ($tt_bank_to_account_number == "") { $display_msg=1; $msg .= "Please enter bank to account number"; $msg .="<br>"; }
	if ($tt_currency == "") { $display_msg=1; $msg .= "Please select currency"; $msg .="<br>"; }
	if ($tt_amount == "") { $display_msg=1; $msg .= "Please enter amount"; $msg .="<br>"; }
	if (checkIfNumber($tt_amount) == 0)	{ $display_msg = 1; $msg .= "Amount must be a number"; $msg .="<br>"; }
	if ($tt_amount_transferred!="" && checkIfNumber($tt_amount_transferred) == 0)	{ $display_msg = 1; $msg .= "Amount transferred must be a number"; $msg .="<br>"; }
	if ($tt_bank_charges!="" && checkIfNumber($tt_bank_charges) == 0)	{ $display_msg = 1; $msg .= "Bank charges must be a number"; $msg .="<br>"; }
	if ($tt_swift_code == "") { $display_msg=1; $msg .= "Please enter swift code"; $msg .="<br>"; }
	#if ($tt_iban == "") { $display_msg=1; $msg .= "Please enter iban"; $msg .="<br>"; }
	if ($tt_beneficiary == "") { $display_msg=1; $msg .= "Please enter beneficiary"; $msg .="<br>"; }
	if ($tt_purpose == "") { $display_msg=1; $msg .= "Please enter purpose"; $msg .="<br>"; }
	
	$tt_purpose = addslashes($tt_purpose);
	$tt_bank_to_address = addslashes($tt_bank_to_address);

	if ($msg=="")
	{
			$sql = "UPDATE	tbl_telegraphic_transfer 
					SET		tt_date = '$tt_date',
							tt_country = '$tt_country',
							tt_bank_from = '$tt_bank_from', 
							tt_bank_to = '$tt_bank_to',
							tt_bank_to_address = '$tt_bank_to_address',
							tt_bank_to_account_number = '$tt_bank_to_account_number',
							tt_currency = '$tt_currency',
							tt_amount = '$tt_amount',
							tt_swift_code = '$tt_swift_code',
							tt_iban = '$tt_iban',
							tt_beneficiary = '$tt_beneficiary',
							tt_purpose = '$tt_purpose',
							tt_dv_number = '$tt_dv_number',
							tt_amount_transferred = '$tt_amount_transferred',
							tt_bank_charges = '$tt_bank_charges'
					WHERE	tt_id = '$tt_id'";
			mysql_query($sql) or die("Error inserting values : module : cash.voucher.add.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
			$display_msg=1;
			$msg = "Record Updated Successfully!";
			header("location: /workspaceGOP/page.transactions/telegraphic.transfer.list.php?msg=1");
	}
}
$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE	
				tt_id,
				tt_year,
				tt_sequence_number,
				tt_number,
				tt_date,
				tt_country,
				tt_bank_from, 
				tt_bank_to,
				tt_bank_to_address,
				tt_bank_to_account_number,
				tt_currency,
				tt_amount,
				tt_swift_code,
				tt_iban,
				tt_beneficiary,
				tt_purpose
		FROM 	tbl_telegraphic_transfer
		WHERE	tt_id = '$rep_id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: cash.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);
$tt_id = $rows["tt_id"];
$tt_number = $rows["tt_number"];
$tt_beneficiary = $rows["tt_beneficiary"];
$tt_date = $rows["tt_date"];
$tt_country = $rows["tt_country"];
$tt_bank_from = $rows["tt_bank_from"];
$tt_bank_to	= $rows["tt_bank_to"];	
$tt_bank_to_address	= $rows["tt_bank_to_address"];
$tt_bank_to_account_number = $rows["tt_bank_to_account_number"];	
$tt_currency = $rows["tt_currency"];
$tt_amount = $rows["tt_amount"];
$tt_swift_code = $rows["tt_swift_code"];
$tt_iban = $rows["tt_iban"];
$tt_beneficiary = $rows["tt_beneficiary"];
$tt_purpose = $rows["tt_purpose"];
	
$tt_purpose = stripslashes($tt_purpose);
$tt_bank_to_address = stripslashes($tt_bank_to_address);

$arr_date = explode("-",$tt_date);

$year = $arr_date[0];
$month = $arr_date[1];
$day = $arr_date[2];

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="cash_voucher" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th width="20%" class="rounded-header" scope="col"><div class="main" align="left"><strong>Telegraphic Transfer[Update Record]</strong></div></th>
						  	<th width="80%" class="rounded-q4" scope="col"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update" class="formbutton"></td>
							<td class="rounded-foot-right" align="right"><input type='hidden' name="tt_id" value="<?php echo $tt_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
					    <tr>
					      <td scope="col"><b>Telegraphic Transfer Number</b></td>
					      <td scope="col"><?php echo $tt_number; ?></td>
				      </tr>
				      <tr>
						<td scope="col"><b>Date*</b></td>
						<td scope="col">
							<select name='tt_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='tt_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='tt_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day )
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>							
						</td>
					  </tr>
					  <tr>
						<td scope="col"><b>Country*</b></td>
						<td scope="col">
								<?php
									$sql1 = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_id,
													cfg_value
											FROM 	tbl_config
											WHERE	cfg_name = 'country'
											ORDER BY cfg_value";
									$rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
									?>
									<select name='tt_country' class='formbutton'>
									<option value=''>[SELECT COUNTRY]
									<?php
									while($data1=mysql_fetch_array($rs1))
									{
										$column_id = $data1['cfg_id'];
										$column_name = $data1['cfg_value'];
										?>
										<option value='<?php echo $column_id; ?>'
										<?php
										if ($tt_country == $column_id)
										{
											echo " SELECTED";
										}
										?>
										><?php echo $column_name; ?></option>
										<?php
									}
									?>
									</select>			
						</td>
					  </tr>
					  <tr>
						<td scope="col"><b>From Bank*</b></td>
						<td scope="col">
								<?php
									$sql1 = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													bank_id,
													bank_name,
													bank_account_number
											FROM 	tbl_bank
											ORDER BY bank_account_name";
									$rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
									?>
									<select name='tt_bank_from' class='formbutton'>
									<option value=''>[SELECT BANK]
									<?php
									while($data1=mysql_fetch_array($rs1))
									{
										$column_id = $data1['bank_id'];
										$column_name = $data1['bank_name'];
										$column_description = $data1['bank_account_number'];
										?>
										<option value='<?php echo $column_id; ?>'
										<?php
										if ($tt_bank_from == $column_id)
										{
											echo " SELECTED";
										}
										?>
										><?php echo $column_name." [".$column_description."]"; ?></option>
										<?php
									}
									?>
									</select>			
						</td>
					  </tr>		
					  <tr>
						<td scope="col"><b>To Bank*</b></td>
						<td scope="col"><input type="text" name="tt_bank_to" value="<?php echo $tt_bank_to; ?>" class="formbutton" /></td>
					  </tr>		
					  <tr>
					    <td scope="col" valign="top"><b>To Bank Address *</b></td>
					    <td scope="col"><textarea name="tt_bank_to_address" cols="40" rows="5" class="formbutton"><?php echo $tt_bank_to_address; ?></textarea></td>
				      </tr>
					  <tr>
					    <td scope="col"><b>To Bank Account Number *</b></td>
					    <td scope="col"><input type="text" name="tt_bank_to_account_number" value="<?php echo $tt_bank_to_account_number; ?>" class="formbutton" /></td>
				      </tr>
					  <tr>
						<td scope="col"><b>Transaction Currency*</b></td>
						<td scope="col">
								<?php
									$sql1 = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_id,
													cfg_value
											FROM 	tbl_config
											WHERE	cfg_name = 'currency_type'
											ORDER BY cfg_value";
									$rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
									?>
									<select name='tt_currency' class='formbutton'>
									<option value=''>[SELECT CURRENCY]
									<?php
									while($data1=mysql_fetch_array($rs1))
									{
										$column_id = $data1['cfg_id'];
										$column_name = $data1['cfg_value'];
										?>
										<option value='<?php echo $column_id; ?>'
										<?php
										if ($tt_currency == $column_id)
										{
											echo " SELECTED";
										}
										?>
										><?php echo $column_name; ?></option>
										<?php
									}
									?>
									</select>				
						</td>
					  </tr>		
					  <tr>
						<td scope="col"><b>Amount*</b></td>
						<td scope="col"><input type="text" name="tt_amount" value="<?php echo $tt_amount; ?>" class="formbutton"></td>
					  </tr>	
					  <tr>
						<td scope="col"><b>Swift Code*</b></td>
						<td scope="col"><input type="text" name="tt_swift_code" value="<?php echo $tt_swift_code; ?>" class="formbutton"></td>
					  </tr>	
					  <tr>
						<td scope="col"><b>Iban*</b></td>
						<td scope="col"><input type="text" name="tt_iban" value="<?php echo $tt_iban; ?>" class="formbutton"></td>
					  </tr>	
					  <tr>
						<td scope="col"><b>Beneficiary*</b></td>
						<td scope="col"><input type="text" name="tt_beneficiary" value="<?php echo $tt_beneficiary; ?>" class="formbutton"></td>
					  </tr>	
					  <tr>
						<td valign="top" scope="col"><b>Purpose*</b></td>
					    <td valign="top" scope="col"><label>
					      <textarea name="tt_purpose" cols="40" rows="5" class="formbutton"><?php echo $tt_purpose; ?></textarea>
					    </label></td>
				      </tr>
					  <tr>
						<td scope="col"><b>DV Number*</b></td>
						<td scope="col"><input type="text" name="tt_dv_number" value="<?php echo $tt_dv_number; ?>" class="formbutton"></td>
					  </tr>	
					  <tr>
						<td scope="col"><b>Amount Transferred*</b></td>
						<td scope="col"><input type="text" name="tt_amount_transferred" value="<?php echo $tt_amount_transferred; ?>" class="formbutton"></td>
					  </tr>	
					  <tr>
						<td scope="col"><b>Bank Charges*</b></td>
						<td scope="col"><input type="text" name="tt_bank_charges" value="<?php echo $tt_bank_charges; ?>" class="formbutton"></td>
					  </tr>						  					  					  
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>