<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$display_msg = 0;

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$year = date("Y");
$month = date("m");
$day = date("d");
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;
$date_time_inserted = date("Y-m-d H:i:s");


if ($_POST['Submit'] == 'Add Details')
{
	$msg = "";
	
	$dv_currency_code = $_POST['dv_currency_code'];
	$dv_year = $_POST['dv_year'];
	$dv_month = $_POST['dv_month'];
	$dv_day = $_POST['dv_day'];
	$dv_payee = $_POST['dv_payee'];
	$dv_description = $_POST['dv_description'];
	$dv_description = addslashes($dv_description);
	$dv_number = getVoucherSequenceNumber("DV",$dv_currency_code,$dv_month,$dv_year);
	$dv_sequence_number = substr($dv_number,-4);

	$dv_date = dateTimeFormat($hr="",$min="",$sec="",$dv_month,$dv_day,$dv_year,$dv_year);

	if ($dv_number == "") { $display_msg=1; $msg .= "Please enter DV Number"; $msg .="<br>"; }
	if ($dv_currency_code == "") { $display_msg=1; $msg .= "Please select Currency"; $msg .="<br>"; }
	if ($dv_description == "") { $display_msg=1; $msg .= "Please enter Description"; $msg .="<br>"; }

	$sql = "SELECT 	COUNT(dv_number) AS record_count
			FROM	tbl_disbursement_voucher
			WHERE	dv_number = '$dv_number'";
	$rs = mysql_query($sql) or die("Error verifying dv value : module : disbursement.voucher.add.php ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	$record_count = $rows["record_count"];
	if ($record_count != 0) { $display_msg=1; $msg .= "DV Number (".$dv_number.") already exists!"; $msg .="<br>"; }
	
	if ($msg=="")
	{
			$sql = "INSERT INTO tbl_disbursement_voucher 
								(
									dv_year,
									dv_sequence_number,
									dv_number,
									dv_type,
									dv_currency_code,
									dv_date,
									dv_payee,
									dv_description,
									dv_prepared_by,
									dv_prepared_by_date,
									dv_inserted_date
								)
					VALUES 		(
									'".trim($dv_year)."',
									'".trim($dv_sequence_number)."',
									'".trim($dv_number)."',
									'DV',
									'".trim($dv_currency_code)."',
									'".trim($dv_date)."',
									'".trim($dv_payee)."',
									'".trim($dv_description)."',
									'".trim($user_id)."',
									'".trim($date_time_inserted)."',
									'".trim($date_time_inserted)."'
								)";				
			mysql_query($sql) or die("Error inserting values : module : disbursement.voucher.add.php ".$sql." ".mysql_error());
			$record_number = mysql_insert_id();
			insertEventLog($user_id,$sql);
			$dv_number = encrypt($dv_number);
			$record_number = encrypt($record_number);
			header("location: /workspaceGOP/page.transactions/disbursement.voucher.add.details.php?dv_id=$record_number&dv_number=$dv_number");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="disbursement_voucher" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" colspan="2" class="rounded-header"><div class="main" align="left"><strong>Disbursement Voucher [Add Record]</strong></div></th>
							<th scope="col" colspan="2" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" colspan="2" align="left"><input type="submit" name="Submit" value="Add Details" class="formbutton"></td>
							<td class="rounded-foot-right" colspan="2" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
					  <tr>
						<td scope="col"><b>Currency*</b></td>
						<td scope="col">
								<?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_id,
													cfg_value
											FROM 	tbl_config
											WHERE 	cfg_name = 'currency_type'";
									$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
									$str .= "<select name='dv_currency_code' class='formbutton'>";
									while($data=mysql_fetch_array($rs))
									{
										$column_id = $data['cfg_id'];
										$column_name = $data['cfg_value'];
										$str .= "<option value='$column_id'>$column_name</option>";
									}
									$str .= "</select>&nbsp;";
									echo $str;
								?>						
						</td>
						<td scope="col"><b>Date*</b></td>
						<td scope="col">
							<select name='dv_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'><?php echo $yr; ?></option>
								<?php
								}
								?>
									</option>
							</select>
							<select name='dv_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
									?>
									<option value='<?php echo $mo; ?>'><?php echo $months[$mo-1]; ?></option>
									<?php
								}
								?>
									</option>
							</select>
							<select name='dv_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'><?php echo $dy; ?></option>
									<?php
								}
								?>
							</select>							
						</td>
					  </tr>
					  <tr>
						<td scope="col"><b>Payee*</b></td>
						<td colspan="3" scope="col">
								<?php
									$sql1 = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													payee_id,
													payee_name
											FROM 	tbl_payee
											ORDER BY payee_name";
									$rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
									$str1 .= "<select name='dv_payee' class='formbutton'>";
									$str1 .= "<option value=''>[SELECT PAYEE]";
									while($data1=mysql_fetch_array($rs1))
									{
										$column_id = $data1['payee_id'];
										$column_name = $data1['payee_name'];
										$str1 .= "<option value='$column_id'>$column_name</option>";
									}
									$str1 .= "</select>&nbsp;";
									echo $str1;
								?>						
						</td>
					  </tr>
					  <tr>
						<td valign="top" scope="col"><b>Particulars/Description*</b></td>
					    <td colspan="3" valign="top" scope="col"><label>
					      <textarea name="dv_description" cols="40" rows="5" class="formbutton"><?php echo $dv_description; ?></textarea>
					    </label></td>
				      </tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>