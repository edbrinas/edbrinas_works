<?php 
session_start();
include("./../includes/header.main.php");

$page_size = getConfigurationValueByName('max_record_per_page');

$user_id = $_SESSION["id"];
$record_id = $_GET['id'];
$cv_num = $_GET['cv_number'];
$action = $_GET['action'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$search_for = $_GET['search_for'];
$search_by = $_GET['search_by'];
$sort_by = $_GET['sort_by'];
$year_now = date("Y");

if ($order=="") { $order = "cv_date"; }
if ($get_msg==1) { $msg = "Record updated successfully!"; $display_msg = 1; }
if ($get_msg==2) { $msg = "Record inserted successfully!"; $display_msg = 1; }
if($cur_page=="") { $cur_page= 1; }
if($cur_page==1) { $count_page = 0; }
else { $count_page = (($cur_page - 1) * $page_size); }

if ($record_id != "" && $action == "confirm_delete" && $_SESSION["level"]==1)
{
?>
	<script type="text/javascript">
	var flg=confirm('Are you sure you want to delete this record?\n\nClick OK to continue. Otherwise click Cancel.\n');
	if (flg==true)
	{
		
		window.location = '/workspaceGOP/page.transactions/cash.voucher.list.php?id=<?php echo $record_id; ?>&cv_number=<?php echo $cv_num; ?>&action=delete'
	}
	else
	{
		window.location = '/workspaceGOP/page.transactions/cash.voucher.list.php'
	}
	</script> 
    <?php
}
elseif ($action == "confirm_delete" && $_SESSION["level"]!=1)
{
	$msg = "Only administrator can delete a record!";
	$display_msg = 1;
}

if ($record_id != "" && $cv_num != "" && $action == "delete" && $_SESSION["level"]==1)
{
	$record_id = decrypt($record_id);
	$cv_num = decrypt($cv_num);
	
	$sql = "DELETE FROM tbl_cash_voucher
			WHERE 	cv_id = '$record_id'
			AND 	cv_number = '$cv_num'";
	mysql_query($sql) or die("Error in module: cv.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);

	$sql = "DELETE 	FROM tbl_cash_voucher_details
			WHERE	cv_details_reference_number  = '$record_id'";
	mysql_query($sql) or die("Error in module: cv.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);
	
	$sql = "DELETE  FROM tbl_general_ledger
			WHERE 	gl_report_type = 'CV'	
			AND		gl_reference_number = '$record_id'";
	mysql_query($sql) or die("Error in module: cv.list.php ".$sql." ".mysql_error());
	insertEventLog($user_id,$sql);
	
	$msg = "Record deleted successfully!";
	$display_msg = 1;
}
elseif ($action == "delete" && $_SESSION["level"]!=1)
{
	$msg = "Only administrator can delete a record!";
	$display_msg = 1;
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main" style='table-layout:fixed'>
<tr>
	<td>
		<form method="get">
			<table id="rounded-search-table">
				<thead>
					<tr>
						<th class="rounded-header"><div class="whitelabel">Search</div></th>
						<th class="rounded-q4">&nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="rounded-foot-left"><input type="submit" name="action" value="Search" class="formbutton"></td>
						<td class="rounded-foot-right" align="right">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td><input name="search_for" type="text" id="search_for" class="formbutton" value="<?php echo $search_for; ?>"></td>
						<td>
							<select name="search_by" class="formbutton">
							  <option value="ALL" selected>ALL</option>
							  <option value="cv_number" <?php if ($search_by == 'cv_number') { echo "selected"; } ?> >CV Number</option>
							  <option value="cv_date" <?php if ($search_by == 'cv_date') { echo "selected"; } ?> >CV Date</option>
							  <option value="cv_description" <?php if ($search_by == 'cv_description') { echo "selected"; } ?> >CV Description</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</td>
</tr>
<tr>
	<td align="left">&nbsp;</td>
</tr>
<?php
if ($_SESSION["level"]==1 || $_SESSION["level"]==2 || $_SESSION["level"]==3 || $_SESSION["level"]==4)
{
?>
<tr>
	<td align="right">
		<div class="main">Sort by: 
			<a href="/workspaceGOP/page.transactions/cash.voucher.list.php">All</a>
			<?php
			if ($_SESSION["level"]==1 || $_SESSION["level"]==5)
			{
			?>
			|
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=obligate">Pending Obligate</a>
			<?php
			}
			if ($_SESSION["level"]==1 || $_SESSION["level"]==4)
			{
			?>
			|
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=certify">Pending Certify</a>
			<?php
			}
			if ($_SESSION["level"]==1 || $_SESSION["level"]==2 || $_SESSION["level"]==3)
			{			
			?>	
			|		
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=approval">Pending Approval</a>
			<?php
			}
			?>
		</div>
	</td>
</tr>
<tr>
	<td align="left">&nbsp;</td>
</tr>
<?php
}
?>
<tr>
	<td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
			if ($action == "Search" && $search_for != "" && $search_by != "")
			{
				if ($search_for != "" && ($search_by != "" && $search_by != "ALL"))	
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										cv_id,
										cv_type,
										cv_number,
										cv_currency_code,
										cv_date,
										cv_payee,
										cv_description, 
										cv_obligated_by,
										cv_obligated_by_date,
										cv_prepared_by,
										cv_prepared_by_date, 
										cv_certified_by,
										cv_certified_by_date,
										cv_approved_by,
										cv_approved_by_date
								FROM 	tbl_cash_voucher
								WHERE 	".$search_by." LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE cv_obligated_by = 0 
									AND cv_obligated_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	cv_certified_by = 0 
									AND cv_certified_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE  cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00'
									AND	cv_certified_by != 0 
									AND cv_certified_by_date != '0000-00-00 00:00:00' 
									AND	cv_approved_by = 0 
									AND cv_approved_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql1.= "	ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										cv_id,
										cv_type,
										cv_number,
										cv_currency_code,
										cv_date,
										cv_payee,
										cv_description, 
										cv_prepared_by,
										cv_prepared_by_date, 
										cv_certified_by,
										cv_certified_by_date,
										cv_approved_by,
										cv_approved_by_date
								FROM 	tbl_cash_voucher
								WHERE 	".$search_by." LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE cv_obligated_by = 0 
									AND cv_obligated_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	cv_certified_by = 0 
									AND cv_certified_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE  cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00'
									AND	cv_certified_by != 0 
									AND cv_certified_by_date != '0000-00-00 00:00:00' 
									AND	cv_approved_by = 0 
									AND cv_approved_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}							
					$sql2.= "	ORDER BY $order DESC ";
					$sql2.= "	LIMIT	$count_page, $page_size";		
				}
				if ($search_for == "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										cv_id,
										cv_type,
										cv_number,
										cv_currency_code,
										cv_date,
										cv_payee,
										cv_description, 
										cv_prepared_by, 
										cv_prepared_by_date,
										cv_obligated_by,
										cv_obligated_by_date,
										cv_certified_by,
										cv_certified_by_date,
										cv_approved_by,
										cv_approved_by_date
								FROM 	tbl_cash_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE cv_obligated_by = 0 
									AND cv_obligated_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	cv_certified_by = 0 
									AND cv_certified_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE  cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00'
									AND	cv_certified_by != 0 
									AND cv_certified_by_date != '0000-00-00 00:00:00' 
									AND	cv_approved_by = 0 
									AND cv_approved_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}							
					$sql1.= "	ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										cv_id,
										cv_type,
										cv_number
										cv_currency_code,
										cv_date,
										cv_payee,
										cv_description, 
										cv_prepared_by,
										cv_prepared_by_date, 
										cv_obligated_by,
										cv_obligated_by_date,
										cv_certified_by,
										cv_certified_by_date,
										cv_approved_by,
										cv_approved_by_date
								FROM 	tbl_cash_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE cv_obligated_by = 0 
									AND cv_obligated_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	cv_certified_by = 0 
									AND cv_certified_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE  cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00'
									AND	cv_certified_by != 0 
									AND cv_certified_by_date != '0000-00-00 00:00:00' 
									AND	cv_approved_by = 0 
									AND cv_approved_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}					$sql2.= "	ORDER BY $order DESC ";
					$sql2 .="	LIMIT	$count_page, $page_size";		
				}
				if ($search_for != "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										cv_id,
										cv_type,
										cv_number,
										cv_currency_code,
										cv_date,
										cv_payee,
										cv_description, 
										cv_prepared_by, 
										cv_prepared_by_date,
										cv_obligated_by,
										cv_obligated_by_date,
										cv_certified_by,
										cv_certified_by_date,
										cv_approved_by,
										cv_approved_by_date
								FROM 	tbl_cash_voucher
								WHERE	cv_number LIKE '%".$search_for."%'	
								OR cv_date LIKE '%".$search_for."%'
								OR cv_description LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE cv_obligated_by = 0 
									AND cv_obligated_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	cv_certified_by = 0 
									AND cv_certified_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE  cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00'
									AND	cv_certified_by != 0 
									AND cv_certified_by_date != '0000-00-00 00:00:00' 
									AND	cv_approved_by = 0 
									AND cv_approved_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql1.= "	ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										cv_id,
										cv_type,
										cv_number,
										cv_currency_code,
										cv_date,
										cv_payee,
										cv_description, 
										cv_prepared_by, 
										cv_prepared_by_date,
										cv_obligated_by,
										cv_obligated_by_date,
										cv_certified_by,
										cv_certified_by_date,
										cv_approved_by,
										cv_approved_by_date
								FROM 	tbl_cash_voucher
								WHERE	cv_number LIKE '%".$search_for."%'	
								OR cv_date LIKE '%".$search_for."%'
								OR cv_description LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE cv_obligated_by = 0 
									AND cv_obligated_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	cv_certified_by = 0 
									AND cv_certified_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE  cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00'
									AND	cv_certified_by != 0 
									AND cv_certified_by_date != '0000-00-00 00:00:00' 
									AND	cv_approved_by = 0 
									AND cv_approved_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql2.= "	ORDER BY $order DESC ";
					$sql2.= "	LIMIT	$count_page, $page_size";
				}
			}
			else
			{
				$sql1 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									cv_id,
									cv_type,
									cv_number
									cv_currency_code,
									cv_date,
									cv_payee,
									cv_description, 
									cv_prepared_by, 
									cv_prepared_by_date,
									cv_obligated_by,
									cv_obligated_by_date,
									cv_certified_by,
									cv_certified_by_date,
									cv_approved_by,
									cv_approved_by_date
							FROM 	tbl_cash_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE cv_obligated_by = 0 
									AND cv_obligated_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	cv_certified_by = 0 
									AND cv_certified_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE  cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00'
									AND	cv_certified_by != 0 
									AND cv_certified_by_date != '0000-00-00 00:00:00' 
									AND	cv_approved_by = 0 
									AND cv_approved_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql1.= "	ORDER BY $order DESC ";

				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									cv_id,
									cv_type,
									cv_number,
									cv_currency_code,
									cv_date,
									cv_payee,
									cv_description, 
									cv_prepared_by, 
									cv_prepared_by_date,
									cv_obligated_by,
									cv_obligated_by_date,
									cv_certified_by,
									cv_certified_by_date,
									cv_approved_by,
									cv_approved_by_date
							FROM 	tbl_cash_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE cv_obligated_by = 0 
									AND cv_obligated_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	cv_certified_by = 0 
									AND cv_certified_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE  cv_obligated_by != 0 
									AND	cv_obligated_by_date != '0000-00-00 00:00:00'
									AND	cv_certified_by != 0 
									AND cv_certified_by_date != '0000-00-00 00:00:00' 
									AND	cv_approved_by = 0 
									AND cv_approved_by_date = '0000-00-00 00:00:00' 
									AND cv_cancelled_by = 0 
									AND cv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql2.= "	ORDER BY $order DESC ";
					$sql2.= "	LIMIT	$count_page, $page_size";
			}	
			$rs1 = mysql_query($sql1) or die("Error in sql1 in module: cv.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs1);
			$total_pages = ceil($total_records/$page_size); 
			$rs2 = mysql_query($sql2) or die("Error in sql2 in module: cv.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs2);	
			?>
			<thead>
				<tr>
					<th width="10%" align="center" class="rounded-header" scope="col"><a href="<?php echo $page; ?>?order=cv_number" class="whitelabel">CV Number</a></th>
					<th width="5%" align="center" class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=cv_date" class="whitelabel">Transaction Date</a></th>
					<th width="25%" align="center" class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=cv_payee" class="whitelabel">Payee</a></th>
					<th width="50%" align="center" class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=cv_description" class="whitelabel">Particulars/Description</a></th>
					<th width="5%" align="center" class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=cv_currency_code" class="whitelabel">Currency</a></th>
				  <th width="5%" align="center" class="rounded-q4" scope="col"><div class="whitelabel">Transaction Total</div></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2" class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td colspan="4" class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							
							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="6"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="6"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{

				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs2);

					$cv_id = $rows["cv_id"];
					$cv_number = $rows["cv_number"];
					$cv_type = $rows["cv_type"];
					$cv_currency_code = $rows["cv_currency_code"];
					$cv_date = $rows["cv_date"];
					$cv_payee = $rows["cv_payee"];
					$cv_description = $rows["cv_description"];
					$cv_prepared_by = $rows["cv_prepared_by"];
					$cv_obligated_by = $rows["cv_obligated_by"];
					$cv_certified_by = $rows["cv_certified_by"];
					$cv_approved_by = $rows["cv_approved_by"];
					$cv_obligated_by_date = $rows["cv_obligated_by_date"];
					$cv_prepared_by_date = $rows["cv_prepared_by_date"];
					$cv_certified_by_date = $rows["cv_certified_by_date"];
					$cv_approved_by_date = $rows["cv_approved_by_date"];		
					$cv_cancelled_by = $rows["cv_cancelled_by"];
					$cv_cancelled_by_date = $rows["cv_cancelled_by_date"];
					
					$cv_prepared_by_id = $cv_prepared_by;
					$cv_payee = getPayeeById($cv_payee);
					$cv_currency_code = getConfigurationValueById($cv_currency_code);
					
					if ($cv_prepared_by!=0) { $cv_prepared_by = getFullName($cv_prepared_by,3); } else { $cv_prepared_by=""; }
					if ($cv_obligated_by!=0) { $cv_obligated_by = getFullName($cv_obligated_by,3); } else { $cv_obligated_by=""; }
					if ($cv_certified_by!=0) { $cv_certified_by = getFullName($cv_certified_by,3); } else { $cv_certified_by=""; }
					if ($cv_approved_by!=0) { $cv_approved_by = getFullName($cv_approved_by,3); } else { $cv_approved_by=""; }
					if ($cv_cancelled_by!=0) { $cv_cancelled_by = getFullName($cv_cancelled_by,3); } else { $cv_cancelled_by=""; }

					if ($cv_prepared_by_date == "0000-00-00 00:00:00") { $cv_prepared_by_date =""; }
					if ($cv_obligated_by_date=="0000-00-00 00:00:00") {  $cv_obligated_by_date = ""; }
					if ($cv_certified_by_date == "0000-00-00 00:00:00") { $cv_certified_by_date =""; }
					if ($cv_approved_by_date == "0000-00-00 00:00:00") { $cv_approved_by_date =""; }
					if ($cv_cancelled_by_date == "0000-00-00 00:00:00") { $cv_cancelled_by_date =""; }					
					
					$array_total_debit_credit = getTotalDebitCredit($cv_type,$cv_id);

					$cv_total_debit = $array_total_debit_credit[0]["total_debit"];
					$cv_total_credit = $array_total_debit_credit[0]["total_credit"];
					
					$cv_total_debit = numberFormat($cv_total_debit);
					$cv_total_credit = numberFormat($cv_total_credit);
					
					if ($cv_total_debit!=0)
					{
						$total_transaction = $cv_total_debit;
					}
					elseif ($cv_total_credit!=0)
					{
						$total_transaction = $cv_total_credit;
					}
					else
					{
						$total_transaction = 0;
						$total_transaction = numberFormat($total_transaction);
					}
					
					$cv_id = encrypt($cv_id);
				?>      
				 <tr>
					<td valign="top"><?php echo $cv_number; ?>&nbsp;</td>
					<td valign="top"><?php echo $cv_date; ?>&nbsp;</td>
					<td valign="top"><?php echo $cv_payee; ?>&nbsp;</td>
					<td valign="top"><?php echo $cv_description; ?>&nbsp;</td>
					<td valign="top"><?php echo $cv_currency_code; ?>&nbsp;</td>
					<td valign="top"><?php echo $total_transaction; ?>&nbsp;</td>
				</tr>
				<tr>
					<td>Prepared by</td>
					<td colspan="5"><?php echo $cv_prepared_by; ?>&nbsp;<?php echo $cv_prepared_by_date; ?></td>
				</tr>
				<tr>
				  <td>Obligated by</td>
				  <td colspan="5"><?php echo $cv_obligated_by; ?>&nbsp;<?php echo $cv_obligated_by_date; ?></td>
			  </tr>
				<tr>
					<td>Certified by</td>
					<td colspan="5"><?php echo $cv_certified_by; ?>&nbsp;<?php echo $cv_certified_by_date; ?></td>
				</tr>	
				<tr>
					<td>Approved by</td>
					<td colspan="5"><?php echo $cv_approved_by; ?>&nbsp;<?php echo $cv_approved_by_date; ?></td>
				</tr>
				<tr>
					<td>Options</td>
					<td colspan="5">
							<a href="javascript:void(0);" NAME="View Record" title="View Record" onClick="					MM_openBrWindow('cash.voucher.sl.details.php?id=<?php echo $cv_id; ?>&cv_number=<?php echo md5($cv_number); ?>','formtarget','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1080,height=700')">Voucher Details</a>
							|
							<a href="javascript:void(0);" NAME="View Record" title="View Record" onClick="MM_openBrWindow('cash.voucher.print.voucher.php?id=<?php echo $cv_id; ?>&cv_number=<?php echo md5($cv_number); ?>','formtarget','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1080,height=700')">Print Voucher</a>						
							|
							<a href="/workspaceGOP/report.templates/cash.voucher.php?id=<?php echo $cv_id; ?>&cv_number=<?php echo md5($cv_number); ?>">Export to word</a>
						<?php							
						if ($cv_obligated_by == "" && $cv_certified_by == "" && $cv_approved_by == "" && $cv_cancelled_by == "" && $cv_prepared_by_id == $user_id)
						{
					  	?>						
							|
							<a href="cash.voucher.edit.php?id=<?php echo $cv_id; ?>&cv_number=<?php echo encrypt($cv_number); ?>">Edit Record</a>
						<?php
						}
						?>
						<?php
					  	if ($cv_transaction_total != "0.00" && $cv_cancelled_by == "" && ($_SESSION["level"]==5 || $_SESSION["level"]==1))
						{
					  	?>
							|
							<a href="cash.voucher.obligate.php?id=<?php echo $cv_id; ?>&cv_number=<?php echo encrypt($cv_number); ?>">Obligate/Unobligate Record</a>
						<?php
						}
						?>                        
						<?php
					  	if (($_SESSION["level"]==4 || $_SESSION["level"]==1) && $cv_transaction_total != "0.00" && $cv_obligated_by != "" && $cv_cancelled_by == "")
						{
					  	?>
							|
							<a href="cash.voucher.certify.php?id=<?php echo $cv_id; ?>&cv_number=<?php echo encrypt($cv_number); ?>">Certify/Uncertify Record</a>
						<?php
						}
						?>			
						<?php	
						if (($_SESSION["level"]==3 || $_SESSION["level"]== 2 || $_SESSION["level"]==1)&& $cv_transaction_total != "0.00" && $cv_obligated_by != "" && $cv_certified_by != "" && $cv_cancelled_by == "")
						{
						?>						
							|
							<a href="cash.voucher.approve.php?id=<?php echo $cv_id; ?>&cv_number=<?php echo encrypt($cv_number); ?>">Approved/Disapproved Record</a>
						<?php
						}
						?>	
						<?php							
						if ($_SESSION["level"]==1 || $cv_prepared_by_id == $user_id && $cv_cancelled_by == "")
						{
					  	?>						
							|
							<a href="cash.voucher.cancel.voucher.php?id=<?php echo $cv_id; ?>&cv_number=<?php echo encrypt($cv_number); ?>">Cancel/uncancel Voucher</a>
						<?php
						}
						?>                        	
						<?php
						if ($_SESSION["level"]==1 && $cv_certified_by == "" && $cv_approved_by == "")
						{
						?>	
							|					
							<a href="cash.voucher.list.php?id=<?php echo $cv_id; ?>&cv_number=<?php echo encrypt($cv_number); ?>&action=confirm_delete">Delete Record</a>							
						<?php
						}
						?>					</td>
				</tr>	
				<tr>
					<td colspan="6"><hr></td>
				</tr>													
				<?php 
				} 
				?>
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	
	</td>
</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>