<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$display_msg = 0;
$cv_type = "CV";

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$year = date("Y");
$month = date("m");
$day = date("d");
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;
$date_time_inserted = date("Y-m-d H:i:s");

if ($_POST['Submit'] == 'Save')
{
	$msg = "";
	
	$cheque_voucher_number = $_POST['cheque_voucher_number'];
	$ck_year = $_POST['ck_year'];
	$ck_month = $_POST['ck_month'];
	$ck_day = $_POST['ck_day'];
	$cheque_sl_id = $_POST['cheque_sl_id'];
	$cheque_payee = $_POST['cheque_payee'];
	$cheque_number = $_POST['cheque_number'];
	$cheque_currency = $_POST['cheque_currency'];
	$cheque_amount = $_POST['cheque_amount'];

	$cheque_date = dateTimeFormat($hr="",$min="",$sec="",$ck_month,$ck_day,$ck_year);
	
	$sql_bank = "SELECT	SQL_BUFFER_RESULT
						SQL_CACHE
						bank_id,
						COUNT(bank_id) AS count_bank_id
				FROM	tbl_bank
				WHERE	bank_account_sl_code = '$cheque_sl_id'
				GROUP BY bank_account_sl_code";
	$rs_bank = mysql_query($sql_bank) or die("Error in sql  ".$sql_bank." ".mysql_error());	
	$rows_bank = mysql_fetch_array($rs_bank);
	
	$count_bank_id = $rows_bank["count_bank_id"];
	$bank_id = $rows_bank["bank_id"];
	
	if ($count_bank_id != 1) { $display_msg=1; $msg .= "Invalid Account Code!"; $msg .="<br>"; }	

	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE 
					COUNT(cheque_id) AS count_cheque_id
			FROM	tbl_cheque
			WHERE 	cheque_number = '$cheque_number'
			AND		cheque_bank_id = '$bank_id'";
	$rs = mysql_query($sql) or die("Error in sql ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	$count_cheque_id = $rows["count_cheque_id"];
	
	if ($count_cheque_id != 1) { $display_msg=1; $msg .= "Cheque number already exists!"; $msg .="<br>"; }
	
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE 
					COUNT( dv_id ) AS dv_count, 
					dv_id
			FROM	tbl_disbursement_voucher
			WHERE 	dv_number = '$cheque_voucher_number'
			GROUP BY dv_number";
	$rs = mysql_query($sql) or die("Error in sql ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	$dv_count = $rows["dv_count"];
	$dv_id = $rows["dv_id"];

	if ($dv_count != 1) { $display_msg=1; $msg .= "Voucher number does not exist in the Disbursement Voucher List!"; $msg .="<br>"; }
	
	$sql = "SELECT	SQL_BUFFER_RESULT
					SQL_CACHE 
					COUNT( cheque_id ) AS cheque_count, 
					cheque_id
			FROM	tbl_cheque
			WHERE 	cheque_reference_number  = '$dv_id'
			GROUP BY dv_number";
	$rs = mysql_query($sql) or die("Error in sql ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	$cheque_count = $rows["cheque_count"];
	$cheque_id = $rows["cheque_id"];
	
	if ($cheque_count >= 1) { $display_msg=1; $msg .= "Voucher number already exists in the Cheque List!"; $msg .="<br>"; }
	if (checkIfNumber($cheque_amount) == 0)	{ $display_msg = 1; $msg .= "Cheque amount must be a number!"; $msg .= "<br>"; }

	if ($msg=="")
	{
			$sql = "INSERT INTO tbl_cheque 
								(
								cheque_status,	
								cheque_voucher_type,	
								cheque_reference_number,
								cheque_sl_id,
								cheque_bank_id,
								cheque_number,
								cheque_date,
								cheque_payee,
								cheque_currency,
								cheque_amount
								)
					VALUES 		(
								'0',
								'DV',
								'$dv_id',
								'$cheque_sl_id',
								'$bank_id',
								'$cheque_number',
								'$cheque_date',
								'$cheque_payee',
								'$cheque_currency',
								'$cheque_amount'
								)";
			mysql_query($sql) or die("Error inserting values : module : cash.voucher.add.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
			header("location: /workspaceGOP/page.transactions/cheque.list.php?msg=1");	
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="cash_voucher" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Add Cheque [Manual Add Record]</strong></div></th>
						  	<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Save" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
					  <tr>
						<td width="20%" scope="col"><b>Voucher Number*</b></td>
						<td width="80%" scope="col"><input name="cheque_voucher_number" type="text" class='formbutton'></td>
					  </tr>
					  <tr>
						<td scope="col"><b>Cheque Date*</b></td>
						<td scope="col">
							<select name='ck_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='ck_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='ck_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day )
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>						  
						</td>
					  </tr>
					  <tr>
						<td valign="top" scope="col"><b>Account Code*</b></td>
					    <td valign="top" scope="col">
						<?php
                        $sql = "SELECT	SQL_BUFFER_RESULT
                                        SQL_CACHE
                                        sl_id,
                                        sl_account_code,
                                        sl_account_title
                                FROM 	tbl_subsidiary_ledger
                                ORDER BY sl_account_title";
                        $rs = mysql_query($sql) or die('Error ' . mysql_error()); 
                        ?>
                        <select name='cheque_sl_id' class='formbutton'>
                        <?php
                            while($data=mysql_fetch_array($rs))
                            {
                                $column_id = $data['sl_id'];
                                $column_name = $data['sl_account_title'];
                                $column_code = $data['sl_account_code'];
                                ?>
                                <option value='<?php echo $column_id; ?>'
                                <?php
                                if ($column_id==$cheque_sl_id)
                                {
                                ?>
                                    SELECTED
                                <?php
                                }
                                ?>
                                ><?php echo $column_name." [ ".$column_code." ]"; 
                            }
                            ?></option>
                        </select>
                        </td>
				      </tr>
					  <tr>
						<td scope="col"><b>Payee*</b></td>
						<td scope="col">
						<?php
                            $sql1 = "SELECT	SQL_BUFFER_RESULT
                                            SQL_CACHE
                                            payee_id,
                                            payee_name
                                    FROM 	tbl_payee
                                    ORDER BY payee_name";
                            $rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
                            ?>
							<select name='cheque_payee' class='formbutton'>
                            <option value=''>[SELECT PAYEE]
                            <?
                            while($data1=mysql_fetch_array($rs1))
                            {
                                $column_id = $data1['payee_id'];
                                $column_name = $data1['payee_name'];
								?>
                                <option value='<?php echo $column_id; ?>'
                                <?php
                                if ($dv_payee == $column_id)
                                {
									?>
                                    SELECTED
                                    <?php
                                }
                                ?>
								><?php echo $column_name; 
                            }
                            ?></option>
                        	</select>                    
                        </td>
					  </tr>
					  <tr>
						<td scope="col"><b>Cheque Number*</b></td>
						<td scope="col"><input name="cheque_number" type="text" class='formbutton'></td>
					  </tr>
 					  <tr>
						<td scope="col"><b>Currency*</b></td>
						<td scope="col">
							<?php
								$sql = "SELECT	SQL_BUFFER_RESULT
												SQL_CACHE
												cfg_id,
												cfg_value
										FROM 	tbl_config
										WHERE 	cfg_name = 'currency_type'";
								$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
								?>
								<select name='cheque_currency' class='formbutton'>
                                <option value=''>[SELECT CURRENCY]
                                <?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['cfg_id'];
									$column_name = $data['cfg_value'];
									?>
									<option value='<?php echo $column_id; ?>'
                                    <?php
									if ($dv_payee == $column_id)
									{
										?>
										SELECTED
                                        <?php
									}
									?>
									><?php echo $column_name;
								}
                                ?></option>
								</select>
                        </td>
					  </tr>                      
					  <tr>
						<td scope="col"><b>Amount*</b></td>
						<td scope="col"><input name="cheque_amount" type="text" class='formbutton'></td>
					  </tr>                                       
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>
