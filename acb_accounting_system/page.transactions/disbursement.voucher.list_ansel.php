<?php 
session_start();
include("./../includes/header.main.php");

$page_size = getConfigurationValueByName('max_record_per_page');

$user_id = $_SESSION["id"];
$record_id = $_GET['id'];
$dv_num = $_GET['dv_number'];
$action = $_GET['action'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$search_for = $_GET['search_for'];
$search_by = $_GET['search_by'];
$sort_by = $_GET['sort_by'];
$year_now = date("Y");

if ($order=="") { $order = "dv_date"; }
if ($get_msg==1) { $msg = "Record updated successfully!"; $display_msg = 1; }
if ($get_msg==2) { $msg = "Record inserted successfully!"; $display_msg = 1; }
if($cur_page=="") { $cur_page= 1; }
if($cur_page==1) { $count_page = 0; }
else { $count_page = (($cur_page - 1) * $page_size); }
	
if ($record_id != "" && $action == "confirm_delete" && $_SESSION["level"]==1)
{
?>
	<script type="text/javascript">
	var flg=confirm('Are you sure you want to delete this record?\n\nClick OK to continue. Otherwise click Cancel.\n');
	if (flg==true)
	{
		
		window.location = '/workspaceGOP/page.transactions/disbursement.voucher.list.php?id=<?php echo $record_id; ?>&dv_number=<?php echo $dv_num; ?>&action=delete'
	}
	else
	{
		window.location = '/workspaceGOP/page.transactions/disbursement.voucher.list.php'
	}
	</script> 
    <?php
}
elseif ($action == "confirm_delete" && $_SESSION["level"]!=1)
{
	$msg = "Only administrator can delete a record!";
	$display_msg = 1;
}

if ($record_id != "" && $dv_num != "" && $action == "delete" && $_SESSION["level"]==1)
{
	$record_id = decrypt($record_id);
	$dv_num = decrypt($dv_num);
	
	$sql = "DELETE FROM tbl_disbursement_voucher
			WHERE 	dv_id = '$record_id'
			AND 	dv_number = '$dv_num'";
	mysql_query($sql) or die("Error in module: dv.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);

	$sql = "DELETE 	FROM tbl_disbursement_voucher_details
			WHERE	dv_details_reference_number  = '$record_id'";
	mysql_query($sql) or die("Error in module: dv.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);
	
	$sql = "DELETE  FROM tbl_general_ledger
			WHERE 	gl_report_type = 'DV'	
			AND		gl_reference_number = '$record_id'";
	mysql_query($sql) or die("Error in module: dv.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);
	
	$msg = "Record deleted successfully!";
	$display_msg = 1;
}
elseif ($action == "delete" && $_SESSION["level"]!=1)
{
	$msg = "Only administrator can delete a record!";
	$display_msg = 1;
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border=0 align="center" cellpadding="2" cellspacing=0 class="main" style='table-layout:fixed'>
<tr>
	<td>
		<form method="get">
			<table id="rounded-search-table">
				<thead>
					<tr>
						<th class="rounded-header"><div class="whitelabel">Search</div></th>
						<th class="rounded-q4">&nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="rounded-foot-left"><input type="submit" name="action" value="Search" class="formbutton"></td>
						<td class="rounded-foot-right" align="right">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td><input name="search_for" type="text" id="search_for" class="formbutton" value="<?php echo $search_for; ?>"></td>
						<td>
							<select name="search_by" class="formbutton">
							  <option value="ALL" selected>ALL</option>
							  <option value="dv_number" <?php if ($search_by == 'dv_number') { echo "selected"; } ?> >DV Number</option>
							  <option value="dv_date" <?php if ($search_by == 'dv_date') { echo "selected"; } ?> >DV Date</option>
							  <option value="dv_description" <?php if ($search_by == 'dv_description') { echo "selected"; } ?> >DV Description</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</td>
</tr>
<tr>
	<td align="left">&nbsp;</td>
</tr>
<?php
if ($_SESSION["level"]==1 || $_SESSION["level"]==2 || $_SESSION["level"]==3 || $_SESSION["level"]==4)
{
?>
<tr>
	<td align="right">
		<div class="main">Sort by: 
			<a href="/workspaceGOP/page.transactions/disbursement.voucher.list.php">All</a>
			<?php
			if ($_SESSION["level"]==1 || $_SESSION["level"]==5)
			{
			?>
			|
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=obligate">Pending Obligate</a>
			<?php
			}
			if ($_SESSION["level"]==1 || $_SESSION["level"]==4)
			{
			?>
			|
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=certify">Pending Certify</a> 
			<?php
			}
			if ($_SESSION["level"]==1 || $_SESSION["level"]==3)
			{			
			?>
			|
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=recommended">Pending Recommend</a>
			<?php
			}
			if ($_SESSION["level"]==1 || $_SESSION["level"]==2)
			{			
			?>		
			|	
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=approval">Pending Approval</a>
			<?php
			}
			?>
		</div>
	</td>
</tr>
<tr>
	<td align="left">&nbsp;</td>
</tr>
<?php
}
?>
<tr>
	<td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
			if ($action == "Search" && $search_for != "" && $search_by != "")
			{
				if ($search_for != "" && ($search_by != "" && $search_by != "ALL"))	
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										dv_id,
										dv_type,
										dv_number,
										dv_currency_code,
										dv_date,
										dv_payee,
										dv_description, 
										dv_obligated_by,
										dv_obligated_by_date,
										dv_prepared_by,
										dv_prepared_by_date, 
										dv_certified_by,
										dv_certified_by_date,
										dv_recommended_by,
										dv_recommended_by_date,
										dv_approved_by,
										dv_approved_by_date
								FROM 	tbl_disbursement_voucher
								WHERE 	".$search_by." LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE dv_obligated_by = 0 
									AND dv_obligated_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	dv_certified_by = 0 
									AND dv_certified_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql1 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'  
									AND dv_recommended_by = 0 
									AND dv_recommended_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'  
									AND	dv_approved_by = 0 
									AND dv_approved_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql1.= "	ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										dv_id,
										dv_type,
										dv_number,
										dv_currency_code,
										dv_date,
										dv_payee,
										dv_description, 
										dv_prepared_by,
										dv_prepared_by_date, 
										dv_certified_by,
										dv_certified_by_date,
										dv_recommended_by,
										dv_recommended_by_date,
										dv_approved_by,
										dv_approved_by_date
								FROM 	tbl_disbursement_voucher
								WHERE 	".$search_by." LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE dv_obligated_by = 0 
									AND dv_obligated_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	dv_certified_by = 0 
									AND dv_certified_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql2 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'  
									AND dv_recommended_by = 0 
									AND dv_recommended_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'  
									AND	dv_approved_by = 0 
									AND dv_approved_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}									
					$sql2.= "	ORDER BY $order DESC ";
					$sql2.= "	LIMIT	$count_page, $page_size";	
				}
				if ($search_for == "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										dv_id,
										dv_type,
										dv_number,
										dv_currency_code,
										dv_date,
										dv_payee,
										dv_description, 
										dv_prepared_by, 
										dv_prepared_by_date,
										dv_obligated_by,
										dv_obligated_by_date,
										dv_certified_by,
										dv_certified_by_date,
										dv_recommended_by,
										dv_recommended_by_date,
										dv_approved_by,
										dv_approved_by_date
								FROM 	tbl_disbursement_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE dv_obligated_by = 0 
									AND dv_obligated_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	dv_certified_by = 0 
									AND dv_certified_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql1 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'  
									AND dv_recommended_by = 0 
									AND dv_recommended_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00' 
									AND dv_recommended_by != 0 
									AND dv_recommended_by_date != '0000-00-00 00:00:00' 									 
									AND	dv_approved_by = 0 
									AND dv_approved_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}									
					$sql1.= "	ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										dv_id,
										dv_type,
										dv_number
										dv_currency_code,
										dv_date,
										dv_payee,
										dv_description, 
										dv_prepared_by,
										dv_prepared_by_date, 
										dv_obligated_by,
										dv_obligated_by_date,
										dv_certified_by,
										dv_certified_by_date,
										dv_recommended_by,
										dv_recommended_by_date,
										dv_approved_by,
										dv_approved_by_date
								FROM 	tbl_disbursement_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE dv_obligated_by = 0 
									AND dv_obligated_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	dv_certified_by = 0 
									AND dv_certified_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql2 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'  
									AND dv_recommended_by = 0 
									AND dv_recommended_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'
									AND dv_recommended_by != 0 
									AND dv_recommended_by_date != '0000-00-00 00:00:00' 									  
									AND	dv_approved_by = 0 
									AND dv_approved_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}									
					$sql2.= "	ORDER BY $order DESC ";
					$sql2 .="	LIMIT	$count_page, $page_size";		
				}
				if ($search_for != "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										dv_id,
										dv_type,
										dv_number,
										dv_currency_code,
										dv_date,
										dv_payee,
										dv_description, 
										dv_prepared_by, 
										dv_prepared_by_date,
										dv_obligated_by,
										dv_obligated_by_date,
										dv_certified_by,
										dv_certified_by_date,
										dv_recommended_by,
										dv_recommended_by_date,
										dv_approved_by,
										dv_approved_by_date
								FROM 	tbl_disbursement_voucher
								WHERE	dv_number LIKE '%".$search_for."%'	
								OR dv_date LIKE '%".$search_for."%'
								OR dv_description LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE dv_obligated_by = 0 
									AND dv_obligated_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	dv_certified_by = 0 
									AND dv_certified_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql1 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'  
									AND dv_recommended_by = 0 
									AND dv_recommended_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'
									AND dv_recommended_by != 0 
									AND dv_recommended_by_date != '0000-00-00 00:00:00' 									  
									AND	dv_approved_by = 0 
									AND dv_approved_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}									
					$sql1.= "	ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										dv_id,
										dv_type,
										dv_number,
										dv_currency_code,
										dv_date,
										dv_payee,
										dv_description, 
										dv_prepared_by, 
										dv_prepared_by_date,
										dv_obligated_by,
										dv_obligated_by_date,
										dv_certified_by,
										dv_certified_by_date,
										dv_recommended_by,
										dv_recommended_by_date,
										dv_approved_by,
										dv_approved_by_date
								FROM 	tbl_disbursement_voucher
								WHERE	dv_number LIKE '%".$search_for."%'	
								OR dv_date LIKE '%".$search_for."%'
								OR dv_description LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE dv_obligated_by = 0 
									AND dv_obligated_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	dv_certified_by = 0 
									AND dv_certified_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql2 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'  
									AND dv_recommended_by = 0 
									AND dv_recommended_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'
									AND dv_recommended_by != 0 
									AND dv_recommended_by_date != '0000-00-00 00:00:00' 									  
									AND	dv_approved_by = 0 
									AND dv_approved_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql2.= "	ORDER BY $order DESC ";
					$sql2.= "	LIMIT	$count_page, $page_size";
				}
			}
			else
			{
				$sql1 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									dv_id,
									dv_type,
									dv_number
									dv_currency_code,
									dv_date,
									dv_payee,
									dv_description, 
									dv_prepared_by, 
									dv_prepared_by_date,
									dv_obligated_by,
									dv_obligated_by_date,
									dv_certified_by,
									dv_certified_by_date,
									dv_recommended_by,
									dv_recommended_by_date,
									dv_approved_by,
									dv_approved_by_date
							FROM 	tbl_disbursement_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE dv_obligated_by = 0 
									AND dv_obligated_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	dv_certified_by = 0 
									AND dv_certified_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql1 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'  
									AND dv_recommended_by = 0 
									AND dv_recommended_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'
									AND dv_recommended_by != 0 
									AND dv_recommended_by_date != '0000-00-00 00:00:00' 									  
									AND	dv_approved_by = 0 
									AND dv_approved_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}									
					$sql1.= "	ORDER BY $order DESC ";

				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									dv_id,
									dv_type,
									dv_number,
									dv_currency_code,
									dv_date,
									dv_payee,
									dv_description, 
									dv_prepared_by, 
									dv_prepared_by_date,
									dv_obligated_by,
									dv_obligated_by_date,
									dv_certified_by,
									dv_certified_by_date,
									dv_recommended_by,
									dv_recommended_by_date,
									dv_approved_by,
									dv_approved_by_date
							FROM 	tbl_disbursement_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE dv_obligated_by = 0 
									AND dv_obligated_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND	dv_certified_by = 0 
									AND dv_certified_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql2 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'  
									AND dv_recommended_by = 0 
									AND dv_recommended_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE dv_obligated_by != 0 
									AND dv_obligated_by_date != '0000-00-00 00:00:00' 
									AND dv_certified_by != 0 
									AND dv_certified_by_date != '0000-00-00 00:00:00'
									AND dv_recommended_by != 0 
									AND dv_recommended_by_date != '0000-00-00 00:00:00' 									  
									AND	dv_approved_by = 0 
									AND dv_approved_by_date = '0000-00-00 00:00:00' 
									AND dv_cancelled_by = 0 
									AND dv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}									
					$sql2.= "	ORDER BY $order DESC ";
					$sql2.= "	LIMIT	$count_page, $page_size";
			}	
			
			$rs1 = mysql_query(trim($sql1)) or die("Error in sql1 in module: dv.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs1);
			$total_pages = ceil($total_records/$page_size); 
			$rs2 = mysql_query(trim($sql2)) or die("Error in sql2 in module: dv.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs2);	
			?>
			<thead>
				<tr>
					<th width="5%" align="center" class="rounded-header" scope="col">
						<a href="<?php echo $page; ?>?order=dv_number" class="whitelabel">DV Number</a>                    </th>
					<th width="10%" align="center" class="rounded-q2" scope="col">
						<a href="<?php echo $page; ?>?order=dv_date" class="whitelabel">Transaction Date</a>                    </th>
					<th width="20%" align="center" class="rounded-q2" scope="col">
						<a href="<?php echo $page; ?>?order=dv_payee" class="whitelabel">Payee</a>                    </th>
					<th width="50%" align="center" class="rounded-q2" scope="col">
						<a href="<?php echo $page; ?>?order=dv_description" class="whitelabel">Particulars/Description</a>                    </th>
					<th width="5%" align="center" class="rounded-q2" scope="col">
						<a href="<?php echo $page; ?>?order=dv_currency_code" class="whitelabel">Currency</a>                    </th>
				  	<th width="10%" align="center" class="rounded-q4" scope="col">
				  		<div class="whitelabel">Transaction Total</div>                    </th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td colspan="5" class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							
							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order&sort_by=$sort_by\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order&sort_by=$sort_by\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order&sort_by=$sort_by\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order&sort_by=$sort_by\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order&sort_by=$sort_by\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order&sort_by=$sort_by\" class=\"main\">Next >></a>";
								} 
							?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="6"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="6"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{

				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs2);
					
					
					
					
					$dv_id = $rows["dv_id"];
					$dv_number = $rows["dv_number"];
					$dv_type = $rows["dv_type"];
					$dv_currency_code = $rows["dv_currency_code"];
					$dv_date = $rows["dv_date"];
					$dv_payee = $rows["dv_payee"];
					$dv_description = $rows["dv_description"];
					$dv_description = stripslashes($dv_description);
					$dv_payee = getPayeeById($dv_payee);
					
					$dv_currency_code = getConfigurationValueById($dv_currency_code);
					$array_total_debit_credit = getTotalDebitCredit($dv_type,$dv_id);

					$dv_total_debit = $array_total_debit_credit[0]["total_debit"];
					$dv_total_credit = $array_total_debit_credit[0]["total_credit"];
					
					if (numberFormat($dv_total_debit)!=0)
					{
						$dv_transaction_total = numberFormat($dv_total_debit);
					}
					else if (numberFormat($dv_total_credit)!=0)
					{
						$dv_transaction_total = numberFormat($dv_total_credit);
					}
					else
					{
						$dv_transaction_total = 0;
						$dv_transaction_total = numberFormat($dv_transaction_total);
					}
					
					/************************ansel*******************/
					
					
					if($user_id == 21 || $user_id == 22)
					{
						if($rows['dv_currency_code'] == 16)
						{
								$trans_month = date('m', strtotime($rows['dv_date']));
								$trans_year = date('Y', strtotime($rows['dv_date']));
								
								$curres = mysql_query("SELECT forex_currency_value FROM tbl_forex WHERE forex_transaction_date = '{$trans_month}-{$trans_year}' AND forex_currency_code = 35") or die(mysql_error());
								$quecnt = mysql_num_rows($curres);

								while($quecnt <= 0 && $trans_month >= 1)
								{	

									$trans_month -= 1;
									if($trans_month <= 9)
										$trans_month_text = '0'.$trans_month;
									else
										$trans_month_text = $trans_month;
										
									$curres = mysql_query("SELECT forex_currency_value FROM tbl_forex WHERE forex_transaction_date = '{$trans_month_text}-{$trans_year}' AND forex_currency_code = 35") or die(mysql_error());
									$quecnt = mysql_num_rows($curres);
								}
								
								$currow = mysql_fetch_array($curres);
								
								$currate = $currow[0];
								
								$totval = str_replace(',','',$dv_transaction_total) / $currate;
								
						}
						else
						{
							$totval = str_replace(',','',$dv_transaction_total);
						}
					}
					
					/******************************************************/
					
					$dv_id = encrypt($dv_id);					

					#Approvals, Prepared, Certify, Recommend
					$dv_prepared_by = $rows["dv_prepared_by"];
					$dv_obligated_by = $rows["dv_obligated_by"];
					$dv_certified_by = $rows["dv_certified_by"];
					$dv_recommended_by =$rows["dv_recommended_by"];
					$dv_approved_by = $rows["dv_approved_by"];
					$dv_cancelled_by = $rows["dv_cancelled_by"];
					$dv_prepared_by_id = $dv_prepared_by;
					
					if ($dv_prepared_by!=0) { $dv_prepared_by = getFullName($dv_prepared_by,3); } else { $dv_prepared_by=""; }
					if ($dv_obligated_by!=0) { $dv_obligated_by = getFullName($dv_obligated_by,3); } else { $dv_obligated_by=""; }
					if ($dv_certified_by!=0) { $dv_certified_by = getFullName($dv_certified_by,3); } else { $dv_certified_by=""; }
					if ($dv_recommended_by!=0) { $dv_recommended_by = getFullName($dv_recommended_by,3); } else { $dv_recommended_by=""; }
					if ($dv_approved_by!=0) { $dv_approved_by = getFullName($dv_approved_by,3); } else { $dv_approved_by=""; }
					if ($dv_cancelled_by!=0) { $dv_cancelled_by = getFullName($dv_cancelled_by,3); } else { $dv_cancelled_by=""; }	
										
					#Approvals, Prepared, Certify, Recommend dates			
					$dv_inserted_date = $rows["dv_inserted_date"];
					$dv_prepared_by_date = $rows["dv_prepared_by_date"];
					$dv_obligated_by_date = $rows["dv_obligated_by_date"];
					$dv_certified_by_date = $rows["dv_certified_by_date"];
					$dv_recommended_by_date = $rows["dv_recommended_by_date"];
					$dv_approved_by_date = $rows["dv_approved_by_date"];
					$dv_cancelled_by_date = $rows["dv_cancelled_by_date"];
					
					if ($dv_certified_by_date=="0000-00-00 00:00:00") {  $dv_certified_by_date = ""; }
					if ($dv_obligated_by_date=="0000-00-00 00:00:00") {  $dv_obligated_by_date = ""; }
					if ($dv_recommended_by_date=="0000-00-00 00:00:00") {  $dv_recommended_by_date = ""; }
					if ($dv_approved_by_date=="0000-00-00 00:00:00") {  $dv_approved_by_date = ""; }
					if ($dv_cancelled_by_date=="0000-00-00 00:00:00") {  $dv_cancelled_by_date = ""; }
					if ($dv_prepared_by_date=="0000-00-00 00:00:00") {  $dv_prepared_by_date = ""; }
	
				?>      
				 <tr>
					<td valign="top"><?php echo $dv_number; ?>&nbsp;</td>
					<td valign="top"><?php echo $dv_date; ?>&nbsp;</td>
					<td valign="top"><?php echo $dv_payee; ?>&nbsp;</td>
					<td valign="top"><?php echo $dv_description; ?>&nbsp;</td>
					<td valign="top"><?php echo $dv_currency_code; ?>&nbsp;</td>
					<td valign="top"><?php echo $dv_transaction_total; ?>&nbsp;</td>
				</tr>
				<tr>
				 	<td>Prepared by</td>
					<td valign="top" colspan="6"><?php echo $dv_prepared_by; ?>&nbsp;<?php echo $dv_prepared_by_date; ?></td>
				</tr>
				<tr>
				  <td>Obligated by</td>
				  <td valign="top" colspan="6"><?php echo $dv_obligated_by; ?>&nbsp;<?php echo $dv_obligated_by_date; ?></td>
			  </tr>
				<tr>
					<td>Certified by</td>
					<td valign="top" colspan="6"><?php echo $dv_certified_by; ?>&nbsp;<?php echo $dv_certified_by_date; ?></td>
				</tr>	
					<td>Recommended by</td>
					<td valign="top" colspan="6"><?php echo $dv_recommended_by; ?>&nbsp;<?php echo $dv_recommended_by_date; ?></td>
				</tr>
				<tr>					
					<td>Approved by</td>
					<td valign="top" colspan="6"><?php echo $dv_approved_by; ?>&nbsp;<?php echo $dv_approved_by_date; ?></td>
				</tr>																																											
				<tr>
						<td>Options</td>
						<td colspan="6">
						
							<a href="javascript:void(0);" NAME="View Record" title="View Record" onClick="					MM_openBrWindow('disbursement.voucher.sl.details.php?id=<?php echo $dv_id; ?>&dv_number=<?php echo md5($dv_number); ?>','formtarget','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1080,height=700')">Voucher Details</a>
							|
							<a href="javascript:void(0);" NAME="View Record" title="View Record" onClick="MM_openBrWindow('disbursement.voucher.print.voucher.php?id=<?php echo $dv_id; ?>&dv_number=<?php echo md5($dv_number); ?>','formtarget','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1080,height=700')">Print Voucher</a>	
							|
							<a href="/workspaceGOP/report.templates/disbursement.voucher.php?id=<?php echo $dv_id; ?>&dv_number=<?php echo md5($dv_number); ?>">Export to word</a>	
						<?php
					  	if ($dv_obligated_by == "" && $dv_approved_by == "" && $dv_certified_by == "" && $dv_recommended_by == "" && $dv_cancelled_by == "" && $dv_prepared_by_id==$user_id)
						{
					  	?>						
							|
							<a href="disbursement.voucher.edit.php?id=<?php echo $dv_id; ?>&dv_number=<?php echo encrypt($dv_number); ?>">Edit Record</a>
						<?php
						}
						?>		
						<?php
					  	if ($dv_transaction_total != "0.00" && $dv_cancelled_by == "" && ($_SESSION["level"]==5 || $_SESSION["level"]==1))
						{
					  	?>
							|
							<a href="disbursement.voucher.obligate.php?id=<?php echo $dv_id; ?>&dv_number=<?php echo encrypt($dv_number); ?>">Obligate/Unobligate Record</a>
						<?php
						}
						?>                      
						<?php
					  	if ($dv_obligated_by != "" && $dv_cancelled_by == "" && $dv_transaction_total != "0.00" && ($_SESSION["level"]==4 || $_SESSION["level"]==1))
						{
					  	?>
							|
							<a href="disbursement.voucher.certify.php?id=<?php echo $dv_id; ?>&dv_number=<?php echo encrypt($dv_number); ?>">Certify/Uncertify Record</a>
						<?php
						}
						?>	
						<?php
						if ($dv_obligated_by != "" && $dv_certified_by != "" && $dv_cancelled_by == "" && $dv_transaction_total != "0.00" && ($_SESSION["level"]==3 || $_SESSION["level"]==1))
						{
						?>
						
						
						<?php
							/***********************ansel************************/
							if(($user_id == 22 && $totval < 20000) || $user_id != 22)
							{
								echo "|<a href=\"disbursement.voucher.recommend.php?id=$dv_id&dv_number=".encrypt($dv_number)."\">Recommend/Unrecommend Record</a>";	
							}
							/****************************************************/
						?>
						
													
						<?php
						}
						?>	
						<?php
						if ($dv_obligated_by != "" && $dv_certified_by != "" && $dv_recommended_by != "" && $dv_cancelled_by == "" && $dv_transaction_total != "0.00" && ($_SESSION["level"]==2 || $_SESSION["level"]==1))
						{
						?>	
						
						<?php 
							/************************ansel***********************/
							if(($user_id == 21 && $totval < 20000) || $user_id != 21)
							{
								echo 
							"|
							<a href=\"disbursement.voucher.approve.php?id=$dv_id&dv_number=".encrypt($dv_number)."\">Approve/Disapprove Record</a>";
							}
							/*********************************************************/
						?>	
						
							
						<?php
						}
						?>	
						<?php
					  	if ($dv_prepared_by_id==$user_id || $_SESSION["level"]==1)
						{
					  	?>						
							|
							<a href="disbursement.voucher.cancel.voucher.php?id=<?php echo $dv_id; ?>&dv_number=<?php echo encrypt($dv_number); ?>">Cancel/Uncancel Voucher</a>
						<?php
						}
						?>
						<?php
						if ($_SESSION["level"]==1 && $dv_obligated_by == "" && $dv_approved_by == "" && $dv_certified_by == "" && $dv_recommended_by == "")
						{
						?>	
							|					
							<a href="disbursement.voucher.list.php?id=<?php echo $dv_id; ?>&dv_number=<?php echo encrypt($dv_number); ?>&action=confirm_delete">Delete Record</a>							
						<?php
						}
						?>                        </td>
					</tr>
					<tr>
						<td colspan="7"><hr></td>
					</tr>					
				<?php 
				} 
				?>
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	
	</td>
</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>