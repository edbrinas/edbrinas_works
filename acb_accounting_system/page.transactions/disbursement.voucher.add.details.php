<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$dv_id = $_GET["dv_id"];
$dv_number = $_GET["dv_number"];
$dv_details_id = $_GET["dv_details_id"];
$dv_details_reference_number = $_GET["dv_details_reference_number"];
$action = $_GET["action"];

$dv_id = decrypt($dv_id);
$dv_number = decrypt($dv_number);
$dv_details_id = decrypt($dv_details_id);
$dv_details_reference_number = decrypt($dv_details_reference_number);
$date_time_inserted = date("Y-m-d H:i:s");

$dv_debit = "0.0000";
$dv_credit = "0.0000";

if ($_SESSION["level"]==1)
{
	$sql = "SELECT 	SQL_BUFFER_RESULT
					SQL_CACHE
					dv_date,
					dv_currency_code,
					dv_approved_by,
					dv_recommended_by,
					dv_certified_by
			FROM	tbl_disbursement_voucher
			WHERE	dv_id = '$dv_id'";
	$rs = mysql_query($sql) or die("Error in page: disbursement.voucher.add.details.php ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	
	$dv_date = $rows["dv_date"];
	$dv_currency_code = $rows["dv_currency_code"];
	$dv_approved_by = $rows["dv_approved_by"];
	$dv_recommended_by = $rows["dv_recommended_by"];
	$dv_certified_by = $rows["dv_certified_by"];
}

if ($dv_details_id != "" && $dv_details_reference_number != "" && $action == "delete")
{
	$sql = "DELETE FROM	tbl_disbursement_voucher_details
			WHERE		dv_details_id = '$dv_details_id'
			AND			dv_details_reference_number = '$dv_details_reference_number'";
	mysql_query($sql) or die("Error in page: disbursement.voucher.add.details.php ".$sql." ".mysql_error());	
	
	$display_msg=1;
	$msg = "Record deleted successfully!";
}


if ($_POST['Submit'] == 'Add Entries')
{
	$msg = "";
	
	$dv_details_reference_number = $_POST['dv_id'];
	$dv_details_account_code = $_POST['dv_details_account_code'];
	$dv_debit = $_POST['dv_debit'];
	$dv_credit = $_POST['dv_credit'];
	$dv_details_budget_id = $_POST['dv_details_budget_id'];
	$dv_details_donor_id = $_POST['dv_details_donor_id'];
	$dv_details_component_id = $_POST['dv_details_component_id'];
	$dv_details_activity_id = $_POST['dv_details_activity_id'];
	$dv_details_other_cost_id = $_POST['dv_details_other_cost_id'];
	$dv_details_staff_id = $_POST['dv_details_staff_id'];
	$dv_details_benefits_id = $_POST['dv_details_benefits_id'];
	$dv_details_vehicle_id = $_POST['dv_details_vehicle_id'];
	$dv_details_equipment_id = $_POST['dv_details_equipment_id'];
	$dv_details_item_id = $_POST['dv_details_item_id'];

	$sl_budget_id = checkSubsidiaryLedgerRequirement($dv_details_account_code,"sl_budget_id");
	$sl_donor_id = checkSubsidiaryLedgerRequirement($dv_details_account_code,"sl_donor_id");
	$sl_component_id = checkSubsidiaryLedgerRequirement($dv_details_account_code,"sl_component_id");
	$sl_activity_id = checkSubsidiaryLedgerRequirement($dv_details_account_code,"sl_activity_id");
	$sl_other_cost_id = checkSubsidiaryLedgerRequirement($dv_details_account_code,"sl_other_cost_id");
	$sl_staff_id = checkSubsidiaryLedgerRequirement($dv_details_account_code,"sl_staff_id");
	$sl_benefits_id = checkSubsidiaryLedgerRequirement($dv_details_account_code,"sl_benefits_id");
	$sl_vehicle_id = checkSubsidiaryLedgerRequirement($dv_details_account_code,"sl_vehicle_id");
	$sl_equipment_id  = checkSubsidiaryLedgerRequirement($dv_details_account_code,"sl_equipment_id");
	$sl_item_id  = checkSubsidiaryLedgerRequirement($dv_details_account_code,"sl_item_id");	
	
	if ($sl_budget_id == 0) { $dv_details_budget_id = ""; }
	if ($sl_donor_id == 0) { $dv_details_donor_id = ""; }
	if ($sl_component_id == 0) { $dv_details_component_id = ""; }
	if ($sl_activity_id == 0) { $dv_details_activity_id = ""; }
	if ($sl_other_cost_id == 0) { $dv_details_other_cost_id = ""; }
	if ($sl_staff_id == 0) { $dv_details_staff_id = ""; }
	if ($sl_benefits_id == 0) { $dv_details_benefits_id = ""; }
	if ($sl_vehicle_id == 0) { $dv_details_vehicle_id = ""; }
	if ($sl_equipment_id == 0) { $dv_details_equipment_id = ""; }
	if ($sl_item_id == 0) { $dv_details_item_id = ""; }

	if ($sl_budget_id == true && $dv_details_budget_id =="") 
	{  
		$display_msg=1; 
		$msg .= "Please select Budget ID,  this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($dv_details_account_code);  
		$msg .="<br>"; 
	}
	if ($sl_donor_id == 1 && $dv_details_donor_id == "") 
	{  
		$display_msg=1; 
		$msg .= "Please select Donor ID,  this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($dv_details_account_code); 
		$msg .="<br>"; 
	}
	if ($sl_component_id == 1 && $dv_details_component_id == "") 
	{  
		$display_msg=1; 
		$msg .= "Please select Component ID,  this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($dv_details_account_code); 
		$msg .="<br>"; 
	}
	if ($sl_activity_id == 1 && $dv_details_activity_id == "") 
	{  
		$display_msg=1; 
		$msg .= "Please select Activity ID,  this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($dv_details_account_code); 
		$msg .="<br>"; 
	}
	if ($sl_other_cost_id == 1 && $dv_details_other_cost_id == "") 
	{  
		$display_msg=1; 
		$msg .= "Please select Other Cost/Services ID,  this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($dv_details_account_code); 
		$msg .="<br>"; 
	}
	if ($sl_staff_id == 1 && $dv_details_staff_id == "") 
	{  
		$display_msg=1; 
		$msg .= "Please select Staff ID,  this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($dv_details_account_code);
		$msg .="<br>"; 
	}
	if ($sl_benefits_id == 1 && $dv_details_benefits_id == "") 
	{  
		$display_msg=1; 
		$msg .= "Please select Benefits ID,  this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($dv_details_account_code); 
		$msg .="<br>"; 
	}
	if ($sl_vehicle_id == 1 && $dv_details_vehicle_id == "") 
	{  
		$display_msg=1; 
		$msg .= "Please select Vehicle ID,  this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($dv_details_account_code); 
		$msg .="<br>"; 
	}
	if ($sl_equipment_id == 1 && $dv_details_equipment_id == "") 
	{  
		$display_msg=1; 
		$msg .= "Please select Details ID,  this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($dv_details_account_code); 
		$msg .="<br>"; 
	}
	if ($sl_item_id == 1 && $dv_details_item_id == "") 
	{  
		$display_msg=1; 
		$msg .= "Please select ITEM ID,  this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($dv_details_account_code); 
		$msg .="<br>"; 
	}	
	
	if (checkIfNumber($dv_debit) == 0) { $display_msg=1; $msg .= "Debit field must be a NUMBER!"; $msg .="<br>"; }
	if (checkIfNumber($dv_credit) == 0) { $display_msg=1; $msg .= "Credit field must be a NUMBER!"; $msg .="<br>"; }
	if ($dv_debit == "0.0000" && $dv_credit == "0.0000") { $display_msg = 1; $msg .= "Both DEBIT and CREDIT fields must not be 0!"; $msg .="<br>"; }
	if (($dv_debit == "0.0000" && $dv_credit == "") || ($dv_debit == "" && $dv_credit == "0.0000")) { $display_msg = 1; $msg .= "There must be atleast one(1) zero value in debit or credit field!"; $msg .="<br>"; }
	
	if ($msg == "")	
	{
	
		$sql = "INSERT	INTO	tbl_disbursement_voucher_details
							(
								dv_details_account_code,
								dv_details_reference_number,
								dv_details_debit,
								dv_details_credit,
								dv_details_budget_id,
								dv_details_donor_id,
								dv_details_component_id,
								dv_details_activity_id,
								dv_details_other_cost_id,
								dv_details_staff_id,
								dv_details_benefits_id,
								dv_details_vehicle_id,
								dv_details_equipment_id,
								dv_details_item_id
							)
				VALUES 		(
								'".trim($dv_details_account_code)."',
								'".trim($dv_details_reference_number)."',
								'".trim($dv_debit)."',
								'".trim($dv_credit)."',
								'".trim($dv_details_budget_id)."',
								'".trim($dv_details_donor_id)."',
								'".trim($dv_details_component_id)."',
								'".trim($dv_details_activity_id)."',
								'".trim($dv_details_other_cost_id)."',
								'".trim($dv_details_staff_id)."',
								'".trim($dv_details_benefits_id)."',
								'".trim($dv_details_vehicle_id)."',
								'".trim($dv_details_equipment_id)."',
								'".trim($dv_details_item_id)."'
							)";
		mysql_query($sql) or die("Query Error " .mysql_error());
		
		#$dv_details_account_code = "";
		$dv_debit = "0.0000";
		$dv_credit = "0.0000";
		
		$dv_details_account_code = "";
		$dv_details_budget_id = "";
		$dv_details_donor_id = "";
		$dv_details_component_id = "";
		$dv_details_activity_id = "";
		$dv_details_other_cost_id = "";
		$dv_details_staff_id = "";
		$dv_details_benefits_id = "";
		$dv_details_vehicle_id = "";
		$dv_details_equipment_id = "";
		$dv_details_item_id = "";

		$sl_budget_id = "";
		$sl_donor_id = "";
		$sl_component_id = "";
		$sl_activity_id = "";
		$sl_other_cost_id = "";
		$sl_staff_id = "";
		$sl_benefits_id = "";
		$sl_vehicle_id = "";
		$sl_equipment_id  = "";
					
		insertEventLog($user_id,$sql);
		$display_msg = 1;
		$msg = "Record inserted successfully!";
		$_POST['Submit'] = "";
	}
}

if ($_POST['Submit'] == 'Done')
{
	$msg = "";
	$total_debit = $_POST['total_debit'];
	$total_credit = $_POST['total_credit'];
	
	$total_debit = numberFormat($total_debit);
	$total_credit = numberFormat($total_credit);
	
	#if ($total_debit == "0.0000" || $total_credit == "0.0000")
	#{
	#	$display_msg = 1;
	#	$msg = "Total DEBIT and Total CREDIT must not be 0!";		
	#}
	#else 
	if ($total_debit != $total_credit)
	{
		$display_msg = 1;
		$msg .= "Total DEBIT must be equal to Total CREDIT!";		
	}

	if ($msg == "")
	{
		header("location: /workspaceGOP/page.transactions/disbursement.voucher.list.php?msg=2");
	}
}
?>
<?php include("./../includes/menu.php"); ?>
<!--
<script>
function preventClose() 
{
	return "Bye Bye";
}
window.onbeforeunload = preventClose;
</script> 
-->
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main" style='table-layout:fixed'>
	<tr>
		<td>
			<form name="disbursement_voucher" method="post">
				<table id="rounded-add-entries" align="center" >
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Disbursement Voucher Details</strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left">&nbsp;</td>
							<td class="rounded-foot-right" align="right">
							<span class="rounded-foot-left">
								<input name="dv_date" type="hidden" id="dv_date" value="<?php echo $dv_date; ?>"/>
								<input name="dv_currency_code" type="hidden" id="dv_currency_code" value="<?php echo $dv_currency_code; ?>"/>
							  	<input type="submit" name="Submit" value="Done" class="formbutton" />
							</span>
							</td>
						</tr>
					</tfoot>
					<tbody >
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><strong>DV Number* </strong></td>
							<td width="80%"><?php echo $dv_number; ?>
								<input name="dv_id" type="hidden" class="formbutton" id="dv_id" value="<?php echo $dv_id; ?>"/>	

								
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center">						
							<!-- Start of Inner Entries Table -->
						    <table id="rounded-add-entries" align="center">
                              <thead>
                                <tr>
                                  <th scope="col" class="rounded-header" align="center"><b>Option</b></th>
                                  <th scope="col" align="center"><b>Account Code</b></th>
                                  <th scope="col" align="center"><b>Account Title</b></th>
                                  <th scope="col" align="center"><b>Debit</b></th>
                                  <th scope="col" align="center"><b>Credit</b></th>
                                  <th scope="col" align="center"><b>Budget ID</b></th>
                                  <th scope="col" align="center"><b>Donor ID</b></th>
                                  <th scope="col" align="center"><b>Component ID</b></th>
                                  <th scope="col" align="center"><b>Activity ID</b></th>
                                  <th scope="col" align="center"><b>Other Cost/Services</b></th>
                                  <th scope="col" align="center"><b>Staff ID</b></th>
                                  <th scope="col" align="center"><b>Benefits ID</b></th>
                                  <th scope="col" align="center"><b>Vehicles ID</b></th>
                                  <th scope="col" align="center"><b>Equipment ID</b></th>
                                  <th scope="col" class="rounded-q4" align="center"><b>Item ID</b></th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
								$sql = "	SELECT 	SQL_BUFFER_RESULT
													SQL_CACHE
													dv_details_id,
													dv_details_account_code,
													dv_details_reference_number,
													dv_details_debit,
													dv_details_credit,
													dv_details_budget_id,
													dv_details_donor_id,
													dv_details_component_id,
													dv_details_activity_id,
													dv_details_other_cost_id,
													dv_details_staff_id,
													dv_details_benefits_id,
													dv_details_vehicle_id,
													dv_details_equipment_id,
													dv_details_item_id
											FROM 	tbl_disbursement_voucher_details
											WHERE	dv_details_reference_number = '$dv_id'";
								$rs = mysql_query($sql) or die("Error in sql2 in module: journal.voucher.details.add.php ".$sql." ".mysql_error());
								$total_rows = mysql_num_rows($rs);	
								if($total_rows == 0)
								{
									?>
									<tr>
									  <td colspan="15"><div class="redlabel" align="left">No records found!</div></td>
									</tr>
									<?php
								}
								else
								{
									for($row_number=0;$row_number<$total_rows;$row_number++)
									{
										$rows = mysql_fetch_array($rs);
										$dv_details_id = $rows["dv_details_id"];
										$dv_details_reference_number = $rows["dv_details_reference_number"];
										$dv_details_account_code = $rows["dv_details_account_code"];
										$dv_details_debit = $rows["dv_details_debit"];
										$dv_details_credit = $rows["dv_details_credit"];
										$dv_details_budget_id = $rows["dv_details_budget_id"];
										$dv_details_donor_id = $rows["dv_details_donor_id"];
										$dv_details_component_id = $rows["dv_details_component_id"];
										$dv_details_activity_id = $rows["dv_details_activity_id"];
										$dv_details_other_cost_id = $rows["dv_details_other_cost_id"];
										$dv_details_staff_id = $rows["dv_details_staff_id"];
										$dv_details_benefits_id = $rows["dv_details_benefits_id"];
										$dv_details_vehicle_id = $rows["dv_details_vehicle_id"];
										$dv_details_equipment_id = $rows["dv_details_equipment_id"];
										$dv_details_item_id = $rows["dv_details_item_id"];
										
										$gl_account_code = $dv_details_account_code;
										$dv_details_account_title = getSubsidiaryLedgerAccountTitleByid($dv_details_account_code);
										$dv_details_account_code = getSubsidiaryLedgerAccountCodeByid($dv_details_account_code);
										
										$dv_details_budget_id  = getBudgetName($dv_details_budget_id);
										$dv_details_donor_id  = getDonorName($dv_details_donor_id);
										$dv_details_component_id  = getComponentName($dv_details_component_id);
										$dv_details_activity_id = getActivityName($dv_details_activity_id);
										$dv_details_other_cost_id  = getOtherCostName($dv_details_other_cost_id);
										$dv_details_staff_id  = getStaffName($dv_details_staff_id);
										$dv_details_benefits_id  = getBenefitsName($dv_details_benefits_id);
										$dv_details_vehicle_id  = getVehicleName($dv_details_vehicle_id);
										$dv_details_equipment_id  = getEquipmentName($dv_details_equipment_id);
										$dv_details_item_id  = getItemName($dv_details_item_id);
										
										$arr_debit[] = $dv_details_debit;
										$arr_credit[] = $dv_details_credit;
									?>
                                <tr>
                                  <td><a href="<?php echo $page; ?>?dv_id=<?php echo encrypt($dv_id); ?>&dv_number=<?php echo encrypt($dv_number); ?>&dv_details_id=<?php echo encrypt($dv_details_id); ?>&dv_details_reference_number=<?php echo encrypt($dv_details_reference_number); ?>&action=delete">[Delete]</a></td>
                                  <td>
									  	<input type="hidden" name="dv_details_budget_id_name[]" value="<?php echo $dv_details_budget_id; ?>">
										<input type="hidden" name="dv_details_donor_id_name[]" value="<?php echo $dv_details_donor_id; ?>">
										<input type="hidden" name="dv_details_component_id_name[]" value="<?php echo $dv_details_component_id; ?>">
										<input type="hidden" name="dv_details_activity_id_name[]" value="<?php echo $dv_details_activity_id; ?>">
										<input type="hidden" name="dv_details_other_cost_id_name[]" value="<?php echo $dv_details_other_cost_id; ?>">
										<input type="hidden" name="dv_details_staff_id_name[]" value="<?php echo $dv_details_staff_id; ?>">
										<input type="hidden" name="dv_details_benefits_id_name[]" value="<?php echo $dv_details_benefits_id; ?>">
										<input type="hidden" name="dv_details_vehicle_id_name[]" value="<?php echo $dv_details_vehicle_id; ?>">
										<input type="hidden" name="dv_details_equipment_id_name[]" value="<?php echo $dv_details_equipment_id; ?>">
										<input type="hidden" name="dv_details_item_id_name[]" value="<?php echo $dv_details_item_id; ?>">
                                        
									  	<input type="hidden" name="gl_currency_code[]" value="<?php echo $gl_currency_code; ?>">
										<input type="hidden" name="gl_reference_number[]" value="<?php echo $dv_details_reference_number;?>">
										<input type="hidden" name="gl_account_code[]" value="<?php echo $gl_account_code;?>">
										<input type="hidden" name="gl_debit[]" value="<?php echo $dv_details_debit;?>">
										<input type="hidden" name="gl_credit[]" value="<?php echo $dv_details_credit;?>">																		  								 		<?php echo $dv_details_account_code; ?>
								  </td>
                                  <td><?php echo $dv_details_account_title; ?></td>
                                  <td align="right"><?php echo numberFormat($dv_details_debit); ?></td>
                                  <td align="right"><?php echo numberFormat($dv_details_credit); ?></td>
                                  <td><?php echo $dv_details_budget_id; ?></td>
                                  <td><?php echo $dv_details_donor_id; ?></td>
                                  <td><?php echo $dv_details_component_id; ?></td>
                                  <td><?php echo $dv_details_activity_id; ?></td>
                                  <td><?php echo $dv_details_other_cost_id; ?></td>
                                  <td><?php echo $dv_details_staff_id; ?></td>
                                  <td><?php echo $dv_details_benefits_id; ?></td>
                                  <td><?php echo $dv_details_vehicle_id; ?></td>
                                  <td><?php echo $dv_details_equipment_id; ?></td>
                                  <td><?php echo $dv_details_item_id; ?></td>
                                  <?php 
									} 
									?>
                                </tr>
                              </tbody>
                              <?php 
								} 
								if ($arr_credit != 0 && $arr_debit != 0)
								{
									$total_debit = array_sum($arr_debit);
									$total_credit = array_sum($arr_credit);
								}
								?>
                              <tfoot>
                                <tr>
                                  <td class="rounded-foot-left"  colspan="3" align="right"><b>Total :</b></td>
                                  <td align="right"><input type="hidden" name="total_debit" value="<?php echo $total_debit; ?>" />
                                      <?php echo numberFormat($total_debit); ?> </td>
                                  <td align="right"><input type="hidden" name="total_credit" value="<?php echo $total_credit; ?>" />
                                      <?php echo numberFormat($total_credit);  ?> </td>
                                  <td class="rounded-foot-right"  colspan="11" align="right">&nbsp;</td>
                                </tr>
                              </tfoot>								
                            </table>
							<!-- End of Inner Entris Table -->							
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center">
							<!-- Start of Inner Entries Table -->
						    <table id="rounded-add-entries">
                              <thead>
                                <tr>
                                  <th width="145" class="rounded-header" scope="col"><b>Report Details</b></th>
                                  <th width="145" scope="col">&nbsp;</th>
                                  <th colspan="3" scope="col" class="rounded-q4">&nbsp;</th>
                                </tr>
                                <tr>
                                  <td scope="col"><b>Account Code</b></td>
                                  <td colspan="4" scope="col">
								  <?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													sl_id,
													sl_account_code,
													sl_account_title
											FROM 	tbl_subsidiary_ledger
											ORDER BY sl_account_title";
									$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
									?>
									<select name='dv_details_account_code' class='formbutton'>
									<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['sl_id'];
											$column_name = $data['sl_account_title'];
											$column_code = $data['sl_account_code'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($column_id==$dv_details_account_code)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_name." [ ".$column_code." ]"; 
										}
										?></option>
									</select>									</td>
                                </tr>
                                <tr>
                                  <td scope="col"><b>Debit</b></td>
                                  <td scope="col"><input name="dv_debit" type="text" class="formbutton" id="dv_debit" value="<?php echo $dv_debit; ?>" size="10" /></td>
                                  <td colspan="3" scope="col">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td scope="col"><b>Credit</b></td>
                                  <td scope="col"><input name="dv_credit" type="text" class="formbutton" id="dv_credit" value="<?php echo $dv_credit; ?>" size="10" /></td>
                                  <td colspan="3" scope="col">&nbsp;</td>
                                </tr>
                                <tr>
                                  <th scope="col"><b>Budget ID</b></th>
                                  <th scope="col"><b>Donor ID</b></th>
                                  <th width="145" scope="col"><b>Component ID</b></th>
                                  <th width="124" scope="col"><b>Activity ID</b></th>
                                  <th width="208" scope="col"><b>Staff ID</b></th>
                                </tr>
                              </thead>
                              <tfoot>
                                <tr>
                                  <th><b>Item ID</b></th>
                                  <th colspan="4">&nbsp;</th>
                                </tr>
                                <tr>
                                  <td colspan="5"><?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													item_id,
													item_name,
													item_description
											FROM 	tbl_item_code
											ORDER BY item_name";
									$rs = mysql_query($sql) or die('Error ' . mysql_error());
									?>
                                    <select name='dv_details_item_id' class='formbutton'>
                                      <option value='' selected="selected">[Please Select]</option>
                                      <?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['item_id'];
											$column_name = $data['item_name'];
											$column_description = $data['item_description'];
											?>
                                      <option value='<?php echo $column_id; ?>'
											<?php
											if ($dv_details_item_id == $column_id)
											{
											?>
												selected="selected"
											<?php
											}
											?>
											><?php echo $column_description;
										}
										?> </option>
                                    </select></td>
                                </tr>
                                <tr>
                                  	<td><span class="rounded-foot-left">&nbsp;</span></td>
									<td colspan="4"><div align="right"><span class="rounded-foot-right">
										<input type="submit" name="Submit" value="Add Entries" class="formbutton" />
                                  </span></div></td>
                                </tr>
                              </tfoot>
                              <tbody>
                                <tr>
                                  <td>
										<?php
										$sql = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														budget_id,
														budget_name,
														budget_description
												FROM 	tbl_budget
												ORDER BY budget_name";
										$rs = mysql_query($sql) or die('Error ' . mysql_error());
										?>
										<select name='dv_details_budget_id' class='formbutton'>
										<option value='' SELECTED>[Please Select]</option>
										<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['budget_id'];
											$column_name = $data['budget_name'];
											$column_description = $data['budget_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($dv_details_budget_id == $column_id)
											{
												?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_name;
										}
										?> </option>
										</select>									</td>
                                  <td>
										<?php
										$sql = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														donor_id,
														donor_name,
														donor_description
												FROM 	tbl_donor
												ORDER BY donor_name";
										$rs = mysql_query($sql) or die('Error ' . mysql_error());
										?>
										<select name='dv_details_donor_id' class='formbutton'>
										<option value='' SELECTED>[Please Select]</option>
										<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['donor_id'];
											$column_name = $data['donor_name'];
											$column_description = $data['donor_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($dv_details_donor_id == $column_id)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_name;
										}
										?> </option>
										</select>									</td>
                                  <td>
										<?php
										$sql = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														component_id,
														component_name,
														component_description
												FROM 	tbl_component
												ORDER BY component_name";
										$rs = mysql_query($sql) or die('Error ' . mysql_error());
										?>
										<select name='dv_details_component_id' class='formbutton'>
										<option value='' SELECTED>[Please Select]</option>
										<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['component_id'];
											$column_name = $data['component_name'];
											$column_description = $data['component_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($dv_details_component_id == $column_id)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_name;
										}
										?></option>
										</select>									</td>
                                  <td>
										<?php
										$sql = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														activity_id,
														activity_name,
														activity_description
												FROM 	tbl_activity
												ORDER BY activity_name";
										$rs = mysql_query($sql) or die('Error ' . mysql_error());
										?>
										<select name='dv_details_activity_id' class='formbutton'>
										<option value='' SELECTED>[Please Select]</option>
										<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['activity_id'];
											$column_name = $data['activity_name'];
											$column_description = $data['activity_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($dv_details_activity_id == $column_id)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_name;
										}
										?> </option>
										</select>									</td>
                                  <td>
										<?php
										$sql = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														staff_id,
														staff_name,
														staff_description
												FROM 	tbl_staff
												ORDER BY staff_description";
										$rs = mysql_query($sql) or die('Error ' . mysql_error());
										?>
										<select name='dv_details_staff_id' class='formbutton'>
										<option value='' SELECTED>[Please Select]</option>
										<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['staff_id'];
											$column_name = $data['staff_name'];
											$column_description = $data['staff_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($dv_details_staff_id == $column_id)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_description;
										}
										?> </option>
										</select>									</td>
                                </tr>
                                <tr>
                                  <th class="rounded-header"><b>Benefits ID</b></th>
                                  <th class="rounded-q2"><b>Vehicles ID</b></th>
                                  <th class="rounded-q2"><b>Equipment ID</b></th>
                                  <th colspan="2" class="rounded-q2"><b>Other Cost/Services ID</b></th>
                                </tr>
                                <tr>
                                  <td>
								  <?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													benefits_id,
													benefits_name,
													benefits_description
											FROM 	tbl_benefits
											ORDER BY benefits_description";
									$rs = mysql_query($sql) or die('Error ' . mysql_error());
									?>
									<select name='dv_details_benefits_id' class='formbutton'>
									<option value='' SELECTED>[Please Select]</option>
									<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['benefits_id'];
											$column_name = $data['benefits_name'];
											$column_description = $data['benefits_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($dv_details_benefits_id == $column_id)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_description;
										}
										?> </option>
									</select>									</td>
                                  <td>
								  <?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													vehicle_id,
													vehicle_name,
													vehicle_description
											FROM 	tbl_vehicle
											ORDER BY vehicle_description";
									$rs = mysql_query($sql) or die('Error ' . mysql_error());
									?>
									<select name='dv_details_vehicle_id' class='formbutton'>
									<option value='' SELECTED>[Please Select]</option>
									<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['vehicle_id'];
											$column_name = $data['vehicle_name'];
											$column_description = $data['vehicle_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($dv_details_vehicle_id == $column_id)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_description;
										}
										?> </option>
									</select>									</td>
                                  <td>
								  <?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													equipment_id,
													equipment_name,
													equipment_description
											FROM 	tbl_equipment
											ORDER BY equipment_name";
									$rs = mysql_query($sql) or die('Error ' . mysql_error());
									?>
									<select name='dv_details_equipment_id' class='formbutton'>
									<option value='' SELECTED>[Please Select]</option>
									<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['equipment_id'];
											$column_name = $data['equipment_name'];
											$column_description = $data['equipment_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($dv_details_equipment_id == $column_id)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_description;
										}
										?> </option>
									</select>									</td>
                                  <td colspan="2">
								  <?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cs_id,
													cs_name,
													cs_description
											FROM 	tbl_other_cost_services
											ORDER BY cs_name";
									$rs = mysql_query($sql) or die('Error ' . mysql_error());
									?>
									<select name='dv_details_other_cost_id' class='formbutton'>
									<option value='' SELECTED>[Please Select]</option>
									<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['cs_id'];
											$column_name = $data['cs_name'];
											$column_description = $data['cs_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($dv_details_cs_id == $column_id)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_description;
										}
										?> </option>
									</select>									</td>
                                </tr>
                              </tbody>
                            </table>
							<!-- End of Inner Entries Table -->							
							</td>
						</tr>						
					</tbody>
				</table>
	      </form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>