<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$rec_id = $_GET['id'];
$rec_dv_number = $_GET['dv_number'];
$display_msg = 0;
$rec_id = decrypt($rec_id);
$rec_dv_number = decrypt($rec_dv_number);

$date_time_approved = date("Y-m-d H:i:s");
$current_year = date("Y");

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE				
				dv_id,
				dv_number,
				dv_type,
				dv_currency_code,
				dv_date,
				dv_payee,
				dv_description,
				dv_obligated_by,
				dv_obligated_by_date,				
				dv_certified_by,
				dv_certified_by_date,
				dv_recommended_by,
				dv_recommended_by_date,
				dv_approved_by,
				dv_approved_by_date
		FROM 	tbl_disbursement_voucher
		WHERE	dv_id = '$rec_id'
		AND		dv_number = '$rec_dv_number'";
$rs = mysql_query($sql) or die("Error in sql1 in module: disbursement.voucher.certify.php ".$sql." ".mysql_error());
$total_rows = mysql_num_rows($rs);	
if($total_rows == 0)
{
	insertEventLog($user_id,"User trying to manipulate the page: disbursement.voucher.certify.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
		window.location = '/workspaceGOP/index.php?msg=4'
	</script>
	<?php
}
else
{
	$rows = mysql_fetch_array($rs);
	$record_id = $rows["dv_id"];
	$dv_number = $rows["dv_number"];
	$dv_type = $rows["dv_type"];
	$dv_currency_code = $rows["dv_currency_code"];
	$dv_date = $rows["dv_date"];
	$dv_payee = $rows["dv_payee"];
	$dv_description = $rows["dv_description"];
	$dv_prepared_by = $rows["dv_prepared_by"];
	$dv_obligated_by = $rows["dv_obligated_by"];
	$dv_obligated_by_date = $rows["dv_obligated_by_date"];	
	$dv_certified_by = $rows["dv_certified_by"];
	$dv_certified_by_date = $rows["dv_certified_by_date"];
	$dv_recommended_by =$rows["dv_recommended_by"];
	$dv_recommended_by_date = $rows["dv_recommended_by_date"];
	$dv_approved_by = $rows["dv_approved_by"];
	$dv_approved_by_date =$rows ["dv_approved_by_date"];
	
	$dv_payee = getPayeeById($dv_payee);
	$dv_description = stripslashes($dv_description);
	$gl_currency_code = $dv_currency_code;
	$dv_currency_name = getConfigurationDescriptionById($dv_currency_code);
	$dv_currency_code = getConfigurationValueById($dv_currency_code);
	
	
	if ($_POST['Submit'] == 'Update Record')
	{
		$msg = "";
		
		$gl_report_type = "DV";
		$record_id = $_POST['record_id'];
		$dv_certified_by = $_POST['dv_certified_by'];
		$dv_approved_by = $_POST['dv_approved_by'];
		$dv_recommended_by = $_POST['dv_recommended_by'];
		$dv_obligated_by = $_POST['dv_obligated_by'];
		
		if ($dv_obligated_by==0) { $dv_certified_by=0; $display_msg = 1; $msg .= "You cannot certify this record because this has not been obligated."; $msg .= "<br>"; }		
		if ($dv_certified_by==0) { $display_msg = 1; $msg .= "You cannot approve/dis-approve this record because this has not been certify."; $msg .= "<br>"; }
		if ($dv_recommended_by==0) { $display_msg = 1; $msg .= "You cannot approve/dis-approve this record because this has not been recommended."; $msg .= "<br>"; }
		
		if ($msg=="" && $dv_approved_by == 1 && $dv_certified_by!=0 && $dv_recommended_by!=0)
		{
			$dv_usr_authentication_password = $_POST['usr_authentication_password'];
			$dv_usr_authentication_password = md5($dv_usr_authentication_password);
			$dv_authenticate_user = authenticateApprover($user_id,$dv_usr_authentication_password);
			if ($dv_authenticate_user==1) { $display_msg = 1; $msg .= "Invalid authentication password!"; $msg .= "<br>"; $dv_approved_by=0; }			
			
			if ($msg == "")
			{
				$sql = "UPDATE	tbl_disbursement_voucher
						SET		dv_approved_by = '$user_id',
								dv_approved_by_date = '$date_time_approved'
						WHERE	dv_id = '$record_id'";
				mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());
				insertEventLog($user_id,$sql);
				
				$sql = "DELETE FROM tbl_general_ledger
						WHERE	gl_reference_number = '$record_id'
						AND		gl_report_type = '$gl_report_type'";
				mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());	
				insertEventLog($user_id,$sql);
	
				$sql = "SELECT	SQL_BUFFER_RESULT
								SQL_CACHE
								dv.dv_date AS dv_date,
								dv.dv_number AS dv_number,
								dv.dv_payee AS dv_payee,
								dv.dv_currency_code AS dv_currency_code,
								dv.dv_description AS dv_description,
								dv_details.dv_details_account_code AS dv_account_code,
								dv_details.dv_details_debit AS dv_debit,
								dv_details.dv_details_credit AS dv_credit,
								dv_details.dv_details_budget_id AS dv_budget_id,
								dv_details.dv_details_donor_id AS dv_donor_id,
								dv_details.dv_details_component_id AS dv_component_id,
								dv_details.dv_details_activity_id AS dv_activity_id,
								dv_details.dv_details_other_cost_id AS dv_other_cost,
								dv_details.dv_details_staff_id AS dv_staff_id,
								dv_details.dv_details_benefits_id AS dv_benefits_id,
								dv_details.dv_details_vehicle_id AS dv_vehicle_id,
								dv_details.dv_details_equipment_id AS dv_equipment_id,
								dv_details.dv_details_item_id AS dv_item_id							
						FROM	tbl_disbursement_voucher AS dv,
								tbl_disbursement_voucher_details AS dv_details
						WHERE 	dv.dv_type = '$gl_report_type'
						AND		dv.dv_id = '$record_id'
						AND		dv_details.dv_details_reference_number = '$record_id'";
				$rs = mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());	
				while($rows=mysql_fetch_array($rs))
				{
					$dv_date = $rows["dv_date"];
					$dv_number = $rows["dv_number"];
					$dv_payee = $rows["dv_payee"];
					$dv_currency_code = $rows["dv_currency_code"];
					$dv_description = $rows["dv_description"];
					$dv_account_code = $rows["dv_account_code"];
					$dv_debit = $rows["dv_debit"];
					$dv_credit = $rows["dv_credit"];
					$dv_budget_id = $rows["dv_budget_id"];
					$dv_donor_id = $rows["dv_donor_id"];
					$dv_component_id = $rows["dv_component_id"];
					$dv_activity_id = $rows["dv_activity_id"];
					$dv_other_cost = $rows["dv_other_cost"];
					$dv_staff_id = $rows["dv_staff_id"];
					$dv_benefits_id = $rows["dv_benefits_id"];
					$dv_vehicle_id = $rows["dv_vehicle_id"];
					$dv_equipment_id = $rows["dv_equipment_id"];
					$dv_item_id = $rows["dv_item_id"];
					
					$dv_description = stripslashes($dv_description);
					$dv_description = addslashes($dv_description);
					
					$sql_bank = "SELECT	SQL_BUFFER_RESULT
										SQL_CACHE
										bank_id,
										COUNT(bank_id) AS count_bank_id
								FROM	tbl_bank
								WHERE	bank_account_sl_code = '$dv_account_code'
								GROUP BY bank_account_sl_code";
					$rs_bank = mysql_query($sql_bank) or die("Error in $sql in module: cash.voucher.approve.php ".$sql_bank." ".mysql_error());	
					$rows_bank = mysql_fetch_array($rs_bank);
					
					$count_bank_id = $rows_bank["count_bank_id"];
					$bank_id = $rows_bank["bank_id"];
					
					if ($count_bank_id == 1 && $dv_currency_code == 16)
					{
						$sql_cheque = "INSERT INTO tbl_cheque 
												(	
												cheque_status,
												cheque_voucher_type,
												cheque_reference_number,
												cheque_sl_id,
												cheque_bank_id,
												cheque_payee, 
												cheque_currency,
												cheque_amount
												)
										VALUES
												(
												'0',
												'DV',
												'$record_id',
												'$dv_account_code',
												'$bank_id',
												'$dv_payee',
												'$dv_currency_code',
												'$dv_credit'
												)";
						mysql_query($sql_cheque) or die("Error in $sql in module: cash.voucher.approve.php ".$sql_cheque." ".mysql_error());	
						insertEventLog($user_id,$sql_cheque);
					}
					
					$sql = "SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									sl_details_id,
									sl_details_year,
									sl_details_ending_balance
							FROM	tbl_subsidiary_ledger_details
							WHERE	sl_details_reference_number = '$dv_account_code'
							AND		sl_details_year = '$current_year'
							AND		sl_details_currency_code = '$dv_currency_code'";
					$rs1 = mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());	
					$rows1 = mysql_fetch_array($rs1);
					$sl_details_id = $rows1["sl_details_id"];
					$sl_details_year = $rows1["sl_details_year"];
					$sl_ending_balance = $rows1["sl_details_ending_balance"];
	
					if ($dv_debit != "0.000000")
					{
						$ending_balance = $dv_debit + $sl_ending_balance;
					}
					if ($dv_credit != "0.000000")
					{
						$ending_balance = $sl_ending_balance - $dv_credit;
					}

					$sql = "UPDATE	tbl_subsidiary_ledger_details
							SET		sl_details_ending_balance = '$ending_balance'
							WHERE	sl_details_id = '$sl_details_id'";		
					mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());
					insertEventLog($user_id,$sql);	
					
					$sql = "INSERT INTO tbl_general_ledger
										(
										gl_date,
										gl_report_type,
										gl_report_number,
										gl_reference_number,
										gl_description,
										gl_account_code,
										gl_currency_code,
										gl_debit,
										gl_credit,
										gl_budget_id,
										gl_donor_id,
										gl_component_id,
										gl_activity_id,
										gl_other_cost_id,
										gl_staff_id,
										gl_benefits_id,
										gl_vehicle_id,
										gl_equipment_id,	
										gl_item_id,								
										gl_date_time_inserted
										)
							VALUES		(
										'".trim($dv_date)."',
										'".trim($gl_report_type)."',
										'".trim($dv_number)."',
										'".trim($record_id)."',
										'".trim($dv_description)."',
										'".trim($dv_account_code)."',
										'".trim($dv_currency_code)."',
										'".trim($dv_debit)."',
										'".trim($dv_credit)."',
										'".trim($dv_budget_id)."',
										'".trim($dv_donor_id)."',
										'".trim($dv_component_id)."',
										'".trim($dv_activity_id)."',
										'".trim($dv_other_cost)."',
										'".trim($dv_staff_id)."',
										'".trim($dv_benefits_id)."',
										'".trim($dv_vehicle_id)."',
										'".trim($dv_equipment_id)."',
										'".trim($dv_item_id)."',									
										'".trim($date_time_approved)."'								
										)";
					mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());	
					insertEventLog($user_id,$sql);						
				}
			}
		}
		elseif ($msg=="" && $dv_approved_by == 0)
		{
			$dv_usr_authentication_password = $_POST['usr_authentication_password'];
			$dv_usr_authentication_password = md5($dv_usr_authentication_password);
			$dv_authenticate_user = authenticateApprover($user_id,$dv_usr_authentication_password);
			if ($dv_authenticate_user==1) { $display_msg = 1; $msg .= "Invalid authentication password!"; $msg .= "<br>"; }			
			
			if ($msg == "")
			{
				$sql = "UPDATE	tbl_disbursement_voucher
						SET		dv_approved_by = '0',
								dv_approved_by_date = '0000-00-00 00:00:00'
						WHERE	dv_id = '$record_id'";
				mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());
				insertEventLog($user_id,$sql);
				
				$sql = "DELETE FROM tbl_general_ledger
						WHERE	gl_reference_number = '$record_id'
						AND		gl_report_type = '$gl_report_type'";
				mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());	
				insertEventLog($user_id,$sql);
				
				$sql = "DELETE FROM tbl_cheque
						WHERE	cheque_voucher_type = '$gl_report_type'
						AND		cheque_reference_number = '$record_id'";
				mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());	
				insertEventLog($user_id,$sql);
						
				$sql = "SELECT	SQL_BUFFER_RESULT
								SQL_CACHE
								dv.dv_date AS dv_date,
								dv.dv_number AS dv_number,
								dv.dv_currency_code AS dv_currency_code,
								dv.dv_description AS dv_description,
								dv_details.dv_details_account_code AS dv_account_code,
								dv_details.dv_details_debit AS dv_debit,
								dv_details.dv_details_credit AS dv_credit,
								dv_details.dv_details_budget_id AS dv_budget_id,
								dv_details.dv_details_donor_id AS dv_donor_id,
								dv_details.dv_details_component_id AS dv_component_id,
								dv_details.dv_details_activity_id AS dv_activity_id,
								dv_details.dv_details_other_cost_id AS dv_other_cost,
								dv_details.dv_details_staff_id AS dv_staff_id,
								dv_details.dv_details_benefits_id AS dv_benefits_id,
								dv_details.dv_details_vehicle_id AS dv_vehicle_id,
								dv_details.dv_details_equipment_id AS dv_equipment_id,
								dv_details.dv_details_item_id AS dv_item_id							
						FROM	tbl_disbursement_voucher AS dv,
								tbl_disbursement_voucher_details AS dv_details
						WHERE 	dv.dv_type = '$gl_report_type'
						AND		dv.dv_id = '$record_id'
						AND		dv_details.dv_details_reference_number = '$record_id'";
				$rs = mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());	
				while($rows=mysql_fetch_array($rs))
				{
					$dv_date = $rows["dv_date"];
					$dv_number = $rows["dv_number"];
					$dv_currency_code = $rows["dv_currency_code"];
					$dv_description = $rows["dv_description"];
					$dv_account_code = $rows["dv_account_code"];
					$dv_debit = $rows["dv_debit"];
					$dv_credit = $rows["dv_credit"];
					$dv_budget_id = $rows["dv_budget_id"];
					$dv_donor_id = $rows["dv_donor_id"];
					$dv_component_id = $rows["dv_component_id"];
					$dv_activity_id = $rows["dv_activity_id"];
					$dv_other_cost = $rows["dv_other_cost"];
					$dv_staff_id = $rows["dv_staff_id"];
					$dv_benefits_id = $rows["dv_benefits_id"];
					$dv_vehicle_id = $rows["dv_vehicle_id"];
					$dv_equipment_id = $rows["dv_equipment_id"];
					$dv_item_id = $rows["dv_item_id"];

					$sql = "SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									sl_details_id,
									sl_details_year,
									sl_details_ending_balance
							FROM	tbl_subsidiary_ledger_details
							WHERE	sl_details_reference_number = '$dv_account_code'
							AND		sl_details_year = '$current_year'
							AND		sl_details_currency_code = '$dv_currency_code'";
					$rs1 = mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());	
					$rows1 = mysql_fetch_array($rs1);
					$sl_details_id = $rows1["sl_details_id"];
					$sl_details_year = $rows1["sl_details_year"];
					$sl_ending_balance = $rows1["sl_details_ending_balance"];
	
					if ($dv_debit != "0.000000")
					{
						$ending_balance = $dv_debit - $sl_ending_balance;
					}
					if ($dv_credit != "0.000000")
					{
						$ending_balance = $sl_ending_balance + $dv_credit;
					}

					$sql = "UPDATE	tbl_subsidiary_ledger_details
							SET		sl_details_ending_balance = '$ending_balance'
							WHERE	sl_details_id = '$sl_details_id'";		
					mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.approve.php ".$sql." ".mysql_error());
					insertEventLog($user_id,$sql);	
				}
			}
		}		
		if ($msg == "")
		{
			header("location: /workspaceGOP/page.transactions/disbursement.voucher.list.php?msg=1&search_for=&search_by=&action=&cur_page=&order=dv_id&sort_by=approval");
		}
	}
	
	?>
	<?php include("./../includes/menu.php"); ?>
	<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
		<tr>
			<td>
				<form name="disbursement_voucher" method="post">
					<table id="rounded-add-entries" align="center">
						<thead>
							<tr>
								<th scope="col" colspan="2" class="rounded-header"><div class="main" align="left"><strong>Disbursement Voucher [Approve]</strong></div></th>
								<th scope="col" colspan="2" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td class="rounded-foot-left" colspan="2" align="left"><input type="submit" name="Submit" value="Update Record" class="formbutton"></td>
								<td class="rounded-foot-right" colspan="2" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
							</tr>
						</tfoot>
						<tbody>
						<?php
						if ($display_msg==1)
						{
						?>
							<tr>
								 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
							</tr>
							<?php
						}
						?>
						  <tr>
							<td width="10%" scope="col"><b>DV Number</b></td>
							<td width="40%" scope="col"><?php echo $dv_number; ?></td>
							<td width="10%" scope="col"><b>Report Type</b></td>
							<td width="40%" scope="col"><?php echo $dv_type; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Currency</b></td>
							<td scope="col"><?php echo $dv_currency_name; ?></td>
							<td scope="col"><b>Date</b></td>
							<td scope="col"><?php echo $dv_date; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Payee</b></td>
							<td colspan="3" scope="col"><? echo $dv_payee; ?></td>
						  </tr>
						  <tr>
							<td valign="top" scope="col"><b>Particulars/Description</b></td>
							<td colspan="3" valign="top" scope="col"><?php echo $dv_description; ?></td>
						  </tr>
						  <tr>
							<td colspan="4" valign="top" scope="col">
	
								<table id="rounded-add-entries" align="center">
								  <thead>
									<tr>
									  <th scope="col" class="rounded-header" align="center"><b>Account Code</b></th>
									  <th scope="col" align="center"><b>Account Title</b></th>
									   <th scope="col" align="center"><b>Currency</b></th>
									  <th scope="col" align="center"><b>Debit</b></th>
									  <th scope="col" align="center"><b>Credit</b></th>
									  <th scope="col" align="center"><b>Budget ID</b></th>
									  <th scope="col" align="center"><b>Donor ID</b></th>
									  <th scope="col" align="center"><b>Component ID</b></th>
									  <th scope="col" align="center"><b>Activity ID</b></th>
									  <th scope="col" align="center"><b>Other Cost/Services</b></th>
									  <th scope="col" align="center"><b>Staff ID</b></th>
									  <th scope="col" align="center"><b>Benefits ID</b></th>
									  <th scope="col" align="center"><b>Vehicles ID</b></th>
									  <th scope="col" class="rounded-q4" align="center"><b>Equipment ID</b></th>
									</tr>
								  </thead>
								  <tbody>
									<?php
									$sql = "	SELECT 	SQL_BUFFER_RESULT
														SQL_CACHE
														dv_details_id,
														dv_details_account_code,
														dv_details_reference_number,
														dv_details_debit,
														dv_details_credit,
														dv_details_budget_id,
														dv_details_donor_id,
														dv_details_component_id,
														dv_details_activity_id,
														dv_details_other_cost_id,
														dv_details_staff_id,
														dv_details_benefits_id,
														dv_details_vehicle_id,
														dv_details_equipment_id,
														dv_details_item_id
												FROM 	tbl_disbursement_voucher_details
												WHERE	dv_details_reference_number = '$record_id'";
									$rs = mysql_query($sql) or die("Error in sql2 in module: disbursement.voucher.details.add.php ".$sql." ".mysql_error());
									$total_rows = mysql_num_rows($rs);	
									if($total_rows == 0)
									{
										?>
										<tr>
										  <td colspan="14"><div class="redlabel" align="left">No records found!</div></td>
										</tr>
										<?php
									}
									else
									{
										for($row_number=0;$row_number<$total_rows;$row_number++)
										{
											$rows = mysql_fetch_array($rs);
											$dv_details_id = $rows["dv_details_id"];
											$dv_details_reference_number = $rows["dv_details_reference_number"];
											$dv_details_account_code = $rows["dv_details_account_code"];
											$dv_details_debit = $rows["dv_details_debit"];
											$dv_details_credit = $rows["dv_details_credit"];
											$dv_details_budget_id = $rows["dv_details_budget_id"];
											$dv_details_donor_id = $rows["dv_details_donor_id"];
											$dv_details_component_id = $rows["dv_details_component_id"];
											$dv_details_activity_id = $rows["dv_details_activity_id"];
											$dv_details_other_cost_id = $rows["dv_details_other_cost_id"];
											$dv_details_staff_id = $rows["dv_details_staff_id"];
											$dv_details_benefits_id = $rows["dv_details_benefits_id"];
											$dv_details_vehicle_id = $rows["dv_details_vehicle_id"];
											$dv_details_equipment_id = $rows["dv_details_equipment_id"];
											$dv_details_item_id = $rows["dv_details_item_id"];
											
											$gl_account_code = $dv_details_account_code;
											$dv_details_account_title = getSubsidiaryLedgerAccountTitleByid($dv_details_account_code);
											$dv_details_account_code = getSubsidiaryLedgerAccountCodeByid($dv_details_account_code);
											
											$dv_details_budget_id_name  = getBudgetName($dv_details_budget_id);
											$dv_details_donor_id_name  = getDonorName($dv_details_donor_id);
											$dv_details_component_id_name  = getComponentName($dv_details_component_id);
											$dv_details_activity_id_name = getActivityName($dv_details_activity_id);
											$dv_details_other_cost_id_name  = getOtherCostName($dv_details_other_cost_id);
											$dv_details_staff_id_name  = getStaffName($dv_details_staff_id);
											$dv_details_benefits_id_name  = getBenefitsName($dv_details_benefits_id);
											$dv_details_vehicle_id_name  = getVehicleName($dv_details_vehicle_id);
											$dv_details_equipment_id_name  = getEquipmentName($dv_details_equipment_id);
											$dv_details_item_id_name  = getItemName($dv_details_item_id);
											
											$arr_debit[] = $dv_details_debit;
											$arr_credit[] = $dv_details_credit;
											?>
									<tr>
									  <td><?php echo $dv_details_account_code; ?></td>
									  <td><?php echo $dv_details_account_title; ?></td>
									  <td><?php echo $dv_currency_code; ?></td>
									  <td align="right"><?php echo numberFormat($dv_details_debit); ?></td>
									  <td align="right"><?php echo numberFormat($dv_details_credit); ?></td>
									  <td><?php echo $dv_details_budget_id_name; ?></td>
									  <td><?php echo $dv_details_donor_id_name; ?></td>
									  <td><?php echo $dv_details_component_id_name; ?></td>
									  <td><?php echo $dv_details_activity_id_name; ?></td>
									  <td><?php echo $dv_details_other_cost_id_name; ?></td>
									  <td><?php echo $dv_details_staff_id_name; ?></td>
									  <td><?php echo $dv_details_benefits_id_name; ?></td>
									  <td><?php echo $dv_details_vehicle_id_name; ?></td>
									  <td><?php echo $dv_details_equipment_id_name; ?></td>
									  <?php 
										} 
										?>
									</tr>
								  <?php 
									} 
									if ($arr_credit != 0 && $arr_debit != 0)
									{
										$total_debit = array_sum($arr_debit);
										$total_credit = array_sum($arr_credit);
									}
									?>
								  <tfoot>
									<tr>
									  <td class="rounded-foot-left" colspan="3" align="right"><b>Total :</b></td>
									  <td align="right"><?php echo numberFormat($total_debit); ?> </td>
									  <td align="right"><?php echo numberFormat($total_credit);  ?> </td>
									  <td class="rounded-foot-right" colspan="11" align="right">&nbsp;</td>
									</tr>
								  </tfoot>								
								</table>
	
							</td>
						  </tr>	
							<td valign="top" scope="col">
							<b>Authentication Password *</b>
							</td>
							<td colspan="3" valign="top" scope="col">
                              <label>
                              <input name="usr_authentication_password" type="password" id="usr_authentication_password" class="formbutton" />
                              </label>							
							</td>
						  </tr>							  						  
						  <tr>
							<td colspan="4" valign="top" scope="col">
                            	<input type="hidden" name="dv_obligated_by" value="<?php echo $dv_obligated_by; ?>">
								<input type="hidden" name="dv_certified_by" value="<?php echo $dv_certified_by; ?>">
								<input type="hidden" name="dv_recommended_by" value="<?php echo $dv_recommended_by; ?>">
								<input type="hidden" name="dv_date" value="<?php echo $dv_date; ?>">
							  	<input name="dv_approved_by" type="checkbox" id="dv_approved_by" value="1" <?php if ($dv_approved_by!=0) { echo "CHECKED";} ?>/>
								* Approve this record.</td>
						  </tr>					  					  					  
			
						</tbody>
					</table>
				</form>
			</td>
		</tr>
	</table>
	<?php include("./../includes/footer.main.php"); ?>
<?php
}
?>