<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$page_size = getConfigurationValueByName('max_record_per_page');

$record_id = $_GET['id'];
$action = $_GET['action'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$search_for = $_GET['search_for'];
$search_by = $_GET['search_by'];

if($order=="")
{
  	$order = "transaction_id";
}
if ($get_msg==2)
{
	$msg = "Record updated successfully!";
	$display_msg = 1;
}

if($cur_page=="")
{
   	$cur_page= 1;
}
if($cur_page==1)
{
	$count_page = 0;
}
else
{
  	$count_page = (($cur_page - 1) * $page_size);
}


if ($record_id != "" && $action == "delete")
{
	$sql = "DELETE FROM tbl_other_transactions
			WHERE 		transaction_id = '$record_id'";
	mysql_query($sql) or die("Error in module: tt.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);
	
	$msg = "Record deleted successfully!";
	$display_msg = 1;
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
				$sql1 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									transaction_bank_id,
									transaction_date,
									transaction_description,
									transaction_type,
									transaction_amount
							FROM 	tbl_other_transactions
							ORDER BY $order DESC ";

				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									transaction_bank_id,
									transaction_date,
									transaction_description,
									transaction_type,
									transaction_amount
							FROM 	tbl_other_transactions
							ORDER BY $order DESC 
							LIMIT	$count_page, $page_size";
			$rs1 = mysql_query($sql1) or die("Error in sql1 in module: tt.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs1);
			$total_pages = ceil($total_records/$page_size); 
			$rs2 = mysql_query($sql2) or die("Error in sql2 in module: tt.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs2);	
			?>
			<thead>
				<tr>
					<th scope="col" class="rounded-header"><a href="<?php echo $page; ?>?order=tt_number" class="whitelabel">Bank ID</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_beneficiary" class="whitelabel">Transaction Date</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_date" class="whitelabel">Description</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_country" class="whitelabel">Type</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_bank_from" class="whitelabel">Amount</a></th>																		
					<th scope="col" class="rounded-q4"><div class="whitelabel">Options</div></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2" class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td colspan="7" class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							
							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="9"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="9"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{

				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs2);
					$id = $rows["transaction_id"];
					$transaction_bank_id = $rows["transaction_bank_id"];
					$transaction_date = $rows["transaction_date"];
					$transaction_description = $rows["transaction_description"];
					$transaction_type = $rows["transaction_type"];
					$transaction_amount = $rows["transaction_amount"];			
					
					$transaction_bank_id = getBankName($transaction_bank_id);
					$transaction_amount = numberFormat($transaction_amount);
					$transaction_type = getConfigurationValueById($transaction_type);
					$id = encrypt($id);
				?>      
				 <tr>
					<td><?php echo $transaction_bank_id; ?>&nbsp;</td>
					<td><?php echo $transaction_date; ?>&nbsp;</td>
					<td><?php echo $transaction_description; ?>&nbsp;</td>
					<td><?php echo $transaction_type; ?>&nbsp;</td>
					<td><?php echo $transaction_amount; ?>&nbsp;</td>
					<td>
						  <?php
						  if ($_SESSION["level"]==1 || $_SESSION["level"]==5)
						  {
						  ?>		
								<a href="bank.reconciliation.edit.php?id=<?php echo $id; ?>">[Edit Record]</a>
						  <?php
						  }
						  ?>
						  <?php
						  if ($_SESSION["level"]==1)
						  {
						  ?>
								<a href="bank.reconciliation.list.php?id=<?php echo $id; ?>&action=delete">[Delete]</a>							
						  <?php
						  }
						  ?>					
					</td>
				<?php 
				} 
				?>
			</tr>
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	</td>
</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>