<?php 
$report_name = "Cash Voucher";
include("./../includes/header.report.php");
$report_name = "CASH VOUCHER";
$rep_id = $_GET['id'];
$rep_cv_number = $_GET['cv_number'];
$rep_id = decrypt($rep_id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE	
				cv_id,			
				cv_type,
				cv_number,
				cv_currency_code,
				cv_date,
				cv_payee,
				cv_description,
				cv_prepared_by,
				cv_prepared_by_date,
				cv_certified_by,
				cv_certified_by_date,
				cv_approved_by,
				cv_approved_by_date,
				cv_inserted_date
		FROM 	tbl_cash_voucher
		WHERE	cv_id = '$rep_id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: cash.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$cv_id = $rows["cv_id"];
$cv_type = $rows["cv_type"];
$cv_number = $rows["cv_number"];
$cv_currency_code = $rows["cv_currency_code"];
$cv_date = $rows["cv_date"];
$cv_payee = $rows["cv_payee"];
$cv_description = $rows["cv_description"];
$cv_prepared_by = $rows["cv_prepared_by"];
$cv_prepared_by_date = $rows["cv_prepared_by_date"];
$cv_certified_by = $rows["cv_certified_by"];
$cv_certified_by_date = $rows["cv_certified_by_date"];
$cv_approved_by = $rows["cv_approved_by"];
$cv_approved_by_date = $rows["cv_approved_by_date"];
$cv_inserted_date = $rows["cv_inserted_date"];

$cv_description = stripslashes($cv_description);
$cv_payee = getPayeeById($cv_payee);
$cv_currency_name = getConfigurationDescriptionById($cv_currency_code);
$cv_currency_code = getConfigurationValueById($cv_currency_code);

$cv_prepared_by_id = $cv_prepared_by;
$cv_certified_by_id = $cv_certified_by;
$cv_recommended_by_id = $cv_recommended_by;
$cv_approved_by_id = $cv_approved_by;

if ($cv_prepared_by!=0) { $cv_prepared_by = getFullName($cv_prepared_by,3); } else { $cv_prepared_by=""; }
if ($cv_certified_by!=0) { $cv_certified_by = getFullName($cv_certified_by,3); } else { $cv_certified_by=""; }
if ($cv_approved_by!=0) { $cv_approved_by = getFullName($cv_approved_by,3); } else { $cv_approved_by=""; }

if ($cv_certified_by_date == "0000-00-00 00:00:00") { $cv_certified_by_date = ""; }
if ($cv_approved_by_date == "0000-00-00 00:00:00")  { $cv_approved_by_date = ""; }
if ($cv_prepared_by_date == "0000-00-00 00:00:00")  { $cv_prepared_by_date = ""; }

if ($rep_id == $cv_id && $rep_cv_number == md5($cv_number))
{
?>
<table width='100%' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>
	<tr>
		<td scope='col'>
		<!--Start of inner report table -->
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing'>
			  <tr>
				<th scope='col'>
					<?php echo upperCase($report_name); ?><br>
					<?php echo upperCase(getCompanyName()); ?><br>
					<?php echo upperCase(getCompanyAddress()); ?><br>
					ACB-<?php echo $cv_currency_code; ?>				
				</th>
			  </tr>
			  <tr>
				<th scope='col'>&nbsp;</th>
			  </tr>
			  <tr>
				<th scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td width='10%' rowspan='2' valign='top' scope='col'><strong>Payee</strong></td>
					<td width='40%' rowspan='2' scope='col'><?php echo $cv_payee; ?>&nbsp;</td>
					<td width='10%' scope='col'><div align='left'><strong>CV Number </strong></div></td>
					<td width='40%' scope='col'><?php echo $cv_number; ?></td>
				  </tr>
				  <tr>
					<td><div align='left'><strong>Date</strong></div></td>
					<td><?php echo $cv_date; ?></td>
				  </tr>
				</table>				
				</th>
			  </tr>   
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<th scope='col' align='left'><strong>Description/Particulars</strong></th>
				  </tr>
				  <tr>
					<td><?php echo $cv_description; ?>&nbsp;</td>
				  </tr>
				</table>				
				</td>
			  </tr>  
			  <tr>
				<td scope='col'>
				
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC' >
					<tr>
						<th width="10%" scope='col'>Account Code </th>
						<th width="12%" scope='col'>Account Title </th>
						<th width="6%" scope='col'>Currency</th>
						<th width="10%" scope='col'>Debit</th>
						<th width="10%" scope='col'>Credit</th>
						<th width="5%" scope='col'>Budget ID </th>
						<th width="4%" scope='col'>Donor ID </th>
						<th width="7%" scope='col'>Component ID </th>
						<th width="5%" scope='col'>Activity ID </th>
						<th width="9%" scope='col'>Other Cost/Services ID </th>
						<th width="4%" scope='col'>Staff ID </th>
						<th width="6%" scope='col'>Benefits ID </th>
						<th width="5%" scope='col'>Vehicle ID </th>
						<th width="7%" scope='col'>Equipment ID </th>
					</tr>
				   <?php
			
					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										cv_details_id,
										cv_details_account_code,
										cv_details_reference_number,
										cv_details_debit,
										cv_details_credit,
										cv_details_budget_id,
										cv_details_donor_id,
										cv_details_component_id,
										cv_details_activity_id,
										cv_details_other_cost_id,
										cv_details_staff_id,
										cv_details_benefits_id,
										cv_details_vehicle_id,
										cv_details_equipment_id
								FROM 	tbl_cash_voucher_details
								WHERE	cv_details_reference_number = '$cv_id'
								ORDER BY cv_details_id ASC";
					$rs2 = mysql_query($sql2) or die("Error in sql2 in module: cash.voucher.details.php ".$sql2." ".mysql_error());
					$total_rows = mysql_num_rows($rs2);	
					if($total_rows == 0)
					{
						?>
						<tr>
						  <td colspan='14'><div class='redlabel' align='left'>No records found!</div></td>
						</tr>
						<?php
					}
					else
					{
						for($row_number=0;$row_number<$total_rows;$row_number++)
						{
							$rows2 = mysql_fetch_array($rs2);
							$cv_details_id = $rows2["cv_details_id"];
							$cv_details_reference_number = $rows2["cv_details_reference_number"];
							$cv_details_account_code = $rows2["cv_details_account_code"];
							$cv_details_debit = $rows2["cv_details_debit"];
							$cv_details_credit = $rows2["cv_details_credit"];
							$cv_details_budget_id = $rows2["cv_details_budget_id"];
							$cv_details_donor_id = $rows2["cv_details_donor_id"];
							$cv_details_component_id = $rows2["cv_details_component_id"];
							$cv_details_activity_id = $rows2["cv_details_activity_id"];
							$cv_details_other_cost_id = $rows2["cv_details_other_cost_id"];
							$cv_details_staff_id = $rows2["cv_details_staff_id"];
							$cv_details_benefits_id = $rows2["cv_details_benefits_id"];
							$cv_details_vehicle_id = $rows2["cv_details_vehicle_id"];
							$cv_details_equipment_id = $rows2["cv_details_equipment_id"];
							
							if (checkIfPettyCash($cv_details_account_code)==1)
							{
								$arr_petty_cash[] = $cv_details_credit;
							}	
														
							$cv_details_account_title = getSubsidiaryLedgerAccountTitleByid($cv_details_account_code);
							$cv_details_account_code = getSubsidiaryLedgerAccountCodeByid($cv_details_account_code);
							
							$cv_details_budget_id  = getBudgetName($cv_details_budget_id);
							$cv_details_donor_id  = getDonorName($cv_details_donor_id);
							$cv_details_component_id  = getComponentName($cv_details_component_id);
							$cv_details_activity_id = getActivityName($cv_details_activity_id);
							$cv_details_other_cost_id  = getOtherCostName($cv_details_other_cost_id);
							$cv_details_staff_id  = getStaffName($cv_details_staff_id);
							$cv_details_benefits_id  = getBenefitsName($cv_details_benefits_id);
							$cv_details_vehicle_id  = getVehicleName($cv_details_vehicle_id);
							$cv_details_equipment_id  = getEquipmentName($cv_details_equipment_id);
						?>
						<tr>
							<td><?php echo $cv_details_account_code; ?></td>
							<td><?php echo $cv_details_account_title; ?></td>
							<td><?php echo $cv_currency_code; ?></td>
							<td align='right'><?php echo numberFormat($cv_details_debit); ?></td>
							<td align='right'><?php echo numberFormat($cv_details_credit); ?></td>
							<td><?php echo $cv_details_budget_id; ?>&nbsp;</td>
							<td><?php echo $cv_details_donor_id; ?>&nbsp;</td>
							<td><?php echo $cv_details_component_id; ?>&nbsp;</td>
							<td><?php echo $cv_details_activity_id; ?>&nbsp;</td>
							<td><?php echo $cv_details_other_cost_id; ?>&nbsp;</td>
							<td><?php echo $cv_details_staff_id; ?>&nbsp;</td>
							<td><?php echo $cv_details_benefits_id; ?>&nbsp;</td>
							<td><?php echo $cv_details_vehicle_id; ?>&nbsp;</td>
							<td><?php echo $cv_details_equipment_id; ?>&nbsp;</td>
						<?php 
						} 
						?>
					</tr>
					<?php 
					} 
					if (count($arr_petty_cash)!=0) { $total_petty_cash = array_sum($arr_petty_cash); }
				?>
				</table>				
				</td>
			  </tr>
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td width='15%' valign='top' scope='col' align='left'><strong>Prepared by</strong></td>
					<td width='30%' rowspan='2' scope='col' align='center'>
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($cv_prepared_by_id);
							}
						?>
					</td>
					<td width='15%' scope='col' align='left' valign="top"><strong>Certified by</strong></td>
					<td width='30%' scope='col' align="center">*Expenditure legal and proper with supporting documents &amp; adequate funds</td>
				  </tr>
				  <tr>
					<td width='15%' valign='top' scope='col' align='left'>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					<td width='30%' scope='col' align='center'>
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($cv_certified_by_id);
							}
						?>					
					</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					<td scope='col' align='center'><b><?php echo $cv_prepared_by; ?></b>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					<td align='center'><b><?php echo $cv_certified_by; ?></b>&nbsp;</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><strong>Prepared Date </strong></td>
					<td scope='col' align='center'><?php echo $cv_prepared_by_date; ?>&nbsp;</td>
					<td align='left'><strong>Certified Date </strong></td>
					<td align='center'><?php echo $cv_certified_by_date; ?>&nbsp;</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><strong>Approved by </strong></td>
					<td colspan="3" rowspan='2' scope='col' align='center'>
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($cv_approved_by_id);
							}
						?>
					</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					</tr>
				  <tr>
					<td valign='top' scope='col'>&nbsp;</td>
					<td colspan="3" align='center' scope='col'><b><?php echo $cv_approved_by; ?></b>&nbsp;</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><strong>Approved Date</strong></td>
					<td colspan="3" align='center' scope='col'><?php echo $cv_approved_by_date; ?>&nbsp;</td>
				  </tr>
				</table>				
				</td>
			  </tr>
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td scope='col'><div align='left'>Received from <b><?php echo upperCase(getCompanyName()); ?></b> the sum of <b><?php echo convertToWords($total_petty_cash)." ".$cv_currency_name; ?></b> (<?php echo $cv_currency_code." ".numberFormat($total_petty_cash); ?>) in payment for the above account.</div></td>
				  </tr>
				</table>	
				</td>
			  </tr>
			  <tr>
				<td scope='col'>
				  <table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
                    <tr>
                      <th colspan='2' scope='col'>Disbursed By </th>
                      <th colspan='2' scope='col'>ACKNOWLEDGEMENT</th>
                    </tr>
                    <tr>
                      <td width='10%' scope='col'><div align='left'>Name</div></td>
                      <td width='40%' scope='col'>Jocelyn M. Bigueras</td>
                      <td width='10%' scope='col'><div align='left'>Signature</div></td>
                      <td width='40%' scope='col'>&nbsp;</td>
                    </tr>
                    <tr>
                      <td scope='col'><div align='left'>Date</div></td>
                      <td scope='col'>&nbsp;</td>
                      <td scope='col'><div align='left'>Printed Name</div></td>
                      <td scope='col'>&nbsp;</td>
                    </tr>
                    <tr>
                      <td scope='col'><div align='left'>&nbsp;</div></td>
                      <td scope='col'>&nbsp;</td>
                      <td scope='col'><div align='left'>Date</div></td>
                      <td scope='col'>&nbsp;</td>
                    </tr>
                  </table>				
				  </td>
			  </tr>
			</table>		
		<!-- End of inner report table -->
		</td>
	</tr>
</table>
<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
	<a href="/workspaceGOP/report.templates/cash.voucher.php?id=<?php echo encrypt($cv_id); ?>&cv_number=<?php echo md5($cv_number); ?>"><img src="/workspaceGOP/images/save.png" border="0" title="Export to word"></a>
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: cash.voucher.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!");
		window.close();
	</script>
	<?php
}
?>