<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$page_size = getConfigurationValueByName('max_record_per_page');

$record_id = $_GET['cheque_id'];
$action = $_GET['action'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$search_for = $_GET['search_for'];
$search_by = $_GET['search_by'];

if($order=="")
{
  	$order = "dv.dv_id,dv.dv_date";
}
if ($get_msg==1)
{
	$msg = "Record updated successfully!";
	$display_msg = 1;
}

if($cur_page=="")
{
   	$cur_page= 1;
}
if($cur_page==1)
{
	$count_page = 0;
}
else
{
  	$count_page = (($cur_page - 1) * $page_size);
}


if ($record_id != "" && $action == "delete")
{
	$record_id = decrypt($record_id);
	
	$sql = "DELETE FROM tbl_activity
			WHERE 		activity_id = '$record_id'";
	mysql_query($sql) or die("Error in module: cheque.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);
	
	$msg = "Record deleted successfully!";
	$display_msg = 1;
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
<tr>
	<td>
		<form method="get">
			<table id="rounded-search-table">
				<thead>
					<tr>
						<th class="rounded-header"><div class="whitelabel">Search</div></th>
						<th class="rounded-q4">&nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="rounded-foot-left"><input type="submit" name="action" value="Search" class="formbutton"></td>
						<td class="rounded-foot-right" align="right">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td><input name="search_for" type="text" id="search_for" class="formbutton" value="<?php echo $search_for; ?>"></td>
						<td>
							<select name="search_by" class="formbutton">
							  <option value="ALL" selected>ALL</option>
							  <option value="dv.dv_number" <?php if ($search_by == 'dv.dv_number') { echo "selected"; } ?> >DV Number</option>
							  <option value="cheque.cheque_number" <?php if ($search_by == 'cheque.cheque_number') { echo "selected"; } ?> >Cheque Number</option>
                              <option value="bank.bank_account_name" <?php if ($search_by == 'bank.bank_name') { echo "selected"; } ?> >Bank Name</option>
                              <option value="bank.bank_account_number" <?php if ($search_by == 'bank.bank_name') { echo "selected"; } ?> >Account Number</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</td>
</tr>

<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
			if ($action == "Search" && $search_for != "" && $search_by != "")
			{
				if ($search_for != "" && ($search_by != "" && $search_by != "ALL"))	
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE        
										cheque.cheque_id AS cheque_id,		
										cheque.cheque_status AS cheque_status,
										cheque.cheque_voucher_type AS cheque_voucher_type,
										cheque.cheque_reference_number AS cheque_reference_number,
										cheque.cheque_sl_id AS cheque_sl_id,
										cheque.cheque_bank_id AS cheque_bank_id,
										cheque.cheque_number AS cheque_number,
										cheque.cheque_date AS cheque_date,
										cheque.cheque_date_cleared AS cheque_date_cleared,
										cheque.cheque_date_released AS cheque_date_released,
										cheque.cheque_date_cancelled AS cheque_date_cancelled,
										cheque.cheque_date_approved_cancel AS cheque_date_approved_cancel,
										cheque.cheque_cancelled_by AS cheque_cancelled_by,
										cheque.cheque_approve_cancelled_by AS cheque_approve_cancelled_by,
										cheque.cheque_payee AS cheque_payee,
										cheque.cheque_currency AS cheque_currency,
										cheque.cheque_amount AS cheque_amount
								FROM	tbl_cheque AS cheque,
										tbl_bank AS bank,
										tbl_disbursement_voucher AS dv
								WHERE	dv.dv_id = cheque.cheque_reference_number
								AND 	cheque.cheque_bank_id = bank.bank_id
								AND 	".$search_by." LIKE '%".$search_for."%'
								ORDER BY $order DESC";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										cheque.cheque_id AS cheque_id,		
										cheque.cheque_status AS cheque_status,
										cheque.cheque_voucher_type AS cheque_voucher_type,
										cheque.cheque_reference_number AS cheque_reference_number,
										cheque.cheque_sl_id AS cheque_sl_id,
										cheque.cheque_bank_id AS cheque_bank_id,
										cheque.cheque_number AS cheque_number,
										cheque.cheque_date AS cheque_date,
										cheque.cheque_date_cleared AS cheque_date_cleared,
										cheque.cheque_date_released AS cheque_date_released,
										cheque.cheque_date_cancelled AS cheque_date_cancelled,
										cheque.cheque_date_approved_cancel AS cheque_date_approved_cancel,
										cheque.cheque_cancelled_by AS cheque_cancelled_by,
										cheque.cheque_approve_cancelled_by AS cheque_approve_cancelled_by,
										cheque.cheque_payee AS cheque_payee,
										cheque.cheque_currency AS cheque_currency,
										cheque.cheque_amount AS cheque_amount
								FROM	tbl_cheque AS cheque,
										tbl_bank AS bank,
										tbl_disbursement_voucher AS dv
								WHERE	dv.dv_id = cheque.cheque_reference_number
								AND 	cheque.cheque_bank_id = bank.bank_id
								AND		".$search_by." LIKE '%".$search_for."%'
								ORDER BY $order DESC
								LIMIT	$count_page, $page_size";	
				}
				if ($search_for == "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										cheque.cheque_id AS cheque_id,		
										cheque.cheque_status AS cheque_status,
										cheque.cheque_voucher_type AS cheque_voucher_type,
										cheque.cheque_reference_number AS cheque_reference_number,
										cheque.cheque_sl_id AS cheque_sl_id,
										cheque.cheque_bank_id AS cheque_bank_id,
										cheque.cheque_number AS cheque_number,
										cheque.cheque_date AS cheque_date,
										cheque.cheque_date_cleared AS cheque_date_cleared,
										cheque.cheque_date_released AS cheque_date_released,
										cheque.cheque_date_cancelled AS cheque_date_cancelled,
										cheque.cheque_date_approved_cancel AS cheque_date_approved_cancel,
										cheque.cheque_cancelled_by AS cheque_cancelled_by,
										cheque.cheque_approve_cancelled_by AS cheque_approve_cancelled_by,
										cheque.cheque_payee AS cheque_payee,
										cheque.cheque_currency AS cheque_currency,
										cheque.cheque_amount AS cheque_amount
								FROM	tbl_cheque AS cheque,
										tbl_bank AS bank,
										tbl_disbursement_voucher AS dv
								WHERE	dv.dv_id = cheque.cheque_reference_number
								AND 	cheque.cheque_bank_id = bank.bank_id
								ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										cheque.cheque_id AS cheque_id,		
										cheque.cheque_status AS cheque_status,
										cheque.cheque_voucher_type AS cheque_voucher_type,
										cheque.cheque_reference_number AS cheque_reference_number,
										cheque.cheque_sl_id AS cheque_sl_id,
										cheque.cheque_bank_id AS cheque_bank_id,
										cheque.cheque_number AS cheque_number,
										cheque.cheque_date AS cheque_date,
										cheque.cheque_date_cleared AS cheque_date_cleared,
										cheque.cheque_date_released AS cheque_date_released,
										cheque.cheque_date_cancelled AS cheque_date_cancelled,
										cheque.cheque_date_approved_cancel AS cheque_date_approved_cancel,
										cheque.cheque_cancelled_by AS cheque_cancelled_by,
										cheque.cheque_approve_cancelled_by AS cheque_approve_cancelled_by,
										cheque.cheque_payee AS cheque_payee,
										cheque.cheque_currency AS cheque_currency,
										cheque.cheque_amount AS cheque_amount
								FROM	tbl_cheque AS cheque,
										tbl_bank AS bank,
										tbl_disbursement_voucher AS dv
								WHERE	dv.dv_id = cheque.cheque_reference_number
								AND 	cheque.cheque_bank_id = bank.bank_id
								LIMIT	$count_page, $page_size";		
				}
				if ($search_for != "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										cheque.cheque_id AS cheque_id,		
										cheque.cheque_status AS cheque_status,
										cheque.cheque_voucher_type AS cheque_voucher_type,
										cheque.cheque_reference_number AS cheque_reference_number,
										cheque.cheque_sl_id AS cheque_sl_id,
										cheque.cheque_bank_id AS cheque_bank_id,
										cheque.cheque_number AS cheque_number,
										cheque.cheque_date AS cheque_date,
										cheque.cheque_date_cleared AS cheque_date_cleared,
										cheque.cheque_date_released AS cheque_date_released,
										cheque.cheque_date_cancelled AS cheque_date_cancelled,
										cheque.cheque_date_approved_cancel AS cheque_date_approved_cancel,
										cheque.cheque_cancelled_by AS cheque_cancelled_by,
										cheque.cheque_approve_cancelled_by AS cheque_approve_cancelled_by,
										cheque.cheque_payee AS cheque_payee,
										cheque.cheque_currency AS cheque_currency,
										cheque.cheque_amount AS cheque_amount
								FROM	tbl_cheque AS cheque,
										tbl_bank AS bank,
										tbl_disbursement_voucher AS dv
								WHERE	dv.dv_id = cheque.cheque_reference_number
								AND 	cheque.cheque_bank_id = bank.bank_id
								AND		dv.dv_number LIKE '%".$search_for."%'
								OR		cheque.cheque_number LIKE '%".$search_for."%'
								OR		bank.bank_account_name LIKE '%".$search_for."%'
								OR		bank.bank_account_number LIKE '%".$search_for."%'									
								ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										cheque.cheque_id AS cheque_id,		
										cheque.cheque_status AS cheque_status,
										cheque.cheque_voucher_type AS cheque_voucher_type,
										cheque.cheque_reference_number AS cheque_reference_number,
										cheque.cheque_sl_id AS cheque_sl_id,
										cheque.cheque_bank_id AS cheque_bank_id,
										cheque.cheque_number AS cheque_number,
										cheque.cheque_date AS cheque_date,
										cheque.cheque_date_cleared AS cheque_date_cleared,
										cheque.cheque_date_released AS cheque_date_released,
										cheque.cheque_date_cancelled AS cheque_date_cancelled,
										cheque.cheque_date_approved_cancel AS cheque_date_approved_cancel,
										cheque.cheque_cancelled_by AS cheque_cancelled_by,
										cheque.cheque_approve_cancelled_by AS cheque_approve_cancelled_by,
										cheque.cheque_payee AS cheque_payee,
										cheque.cheque_currency AS cheque_currency,
										cheque.cheque_amount AS cheque_amount
								FROM	tbl_cheque AS cheque,
										tbl_bank AS bank,
										tbl_disbursement_voucher AS dv
								WHERE	dv.dv_id = cheque.cheque_reference_number
								AND 	cheque.cheque_bank_id = bank.bank_id
								AND		dv.dv_number LIKE '%".$search_for."%'
								OR		cheque.cheque_number LIKE '%".$search_for."%'
								OR		bank.bank_account_name LIKE '%".$search_for."%'
								OR		bank.bank_account_number LIKE '%".$search_for."%'									
								ORDER BY $order DESC 
								LIMIT	$count_page, $page_size";
				}
			}
			else
			{
				$sql1 = "SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE        
									cheque.cheque_id AS cheque_id,		
									cheque.cheque_status AS cheque_status,
									cheque.cheque_voucher_type AS cheque_voucher_type,
									cheque.cheque_reference_number AS cheque_reference_number,
									cheque.cheque_sl_id AS cheque_sl_id,
									cheque.cheque_bank_id AS cheque_bank_id,
									cheque.cheque_number AS cheque_number,
									cheque.cheque_date AS cheque_date,
									cheque.cheque_date_cleared AS cheque_date_cleared,
									cheque.cheque_date_released AS cheque_date_released,
									cheque.cheque_date_cancelled AS cheque_date_cancelled,
									cheque.cheque_date_approved_cancel AS cheque_date_approved_cancel,
									cheque.cheque_cancelled_by AS cheque_cancelled_by,
									cheque.cheque_approve_cancelled_by AS cheque_approve_cancelled_by,
									cheque.cheque_payee AS cheque_payee,
									cheque.cheque_currency AS cheque_currency,
									cheque.cheque_amount AS cheque_amount
							FROM	tbl_cheque AS cheque,
									tbl_bank AS bank,
									tbl_disbursement_voucher AS dv
							WHERE	dv.dv_id = cheque.cheque_reference_number
							AND 	cheque.cheque_bank_id = bank.bank_id
							ORDER BY $order DESC ";
							
				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE        
									cheque.cheque_id AS cheque_id,		
									cheque.cheque_status AS cheque_status,
									cheque.cheque_voucher_type AS cheque_voucher_type,
									cheque.cheque_reference_number AS cheque_reference_number,
									cheque.cheque_sl_id AS cheque_sl_id,
									cheque.cheque_bank_id AS cheque_bank_id,
									cheque.cheque_number AS cheque_number,
									cheque.cheque_date AS cheque_date,
									cheque.cheque_date_cleared AS cheque_date_cleared,
									cheque.cheque_date_released AS cheque_date_released,
									cheque.cheque_date_cancelled AS cheque_date_cancelled,
									cheque.cheque_date_approved_cancel AS cheque_date_approved_cancel,
									cheque.cheque_cancelled_by AS cheque_cancelled_by,
									cheque.cheque_approve_cancelled_by AS cheque_approve_cancelled_by,
									cheque.cheque_payee AS cheque_payee,
									cheque.cheque_currency AS cheque_currency,
									cheque.cheque_amount AS cheque_amount
							FROM	tbl_cheque AS cheque,
									tbl_bank AS bank,
									tbl_disbursement_voucher AS dv
							WHERE	dv.dv_id = cheque.cheque_reference_number
							AND 	cheque.cheque_bank_id = bank.bank_id
							ORDER BY $order DESC 
							LIMIT	$count_page, $page_size";
			}	
			$rs1 = mysql_query($sql1) or die("Error in sql1 in module: cheque.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs1);
			$total_pages = ceil($total_records/$page_size); 
			$rs2 = mysql_query($sql2) or die("Error in sql2 in module: cheque.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs2);	
			?>
			<thead>
				<tr>
					<th scope="col" class="rounded-header"><a href="<?php echo $page; ?>?order=dv_number" class="whitelabel">DV Number</a></th>
					<th scope="col" class="rounded-q1"><a href="<?php echo $page; ?>?order=dv_payee" class="whitelabel">DV Payee</a></th>
                    <th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=bank_name" class="whitelabel">Bank Name</a></th>
                    <th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=cheque_status" class="whitelabel">Cheque Status</a></th>
                    <th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=cheque_number" class="whitelabel">Cheque Number</a></th>
                    <th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=cheque_date" class="whitelabel">Cheque Date</a></th>
                    <th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=cheque_amount" class="whitelabel">Amount</a></th>
                    <th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=cheque_date_cleared" class="whitelabel">Date Cleared</a></th>
                    <th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=cheque_date_cancelled" class="whitelabel">Date Cancelled</a></th>
                    <th scope="col" class="rounded-q4"><a href="<?php echo $page; ?>?order=cheque_cancelled_by" class="whitelabel">Cancelled By</a></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2" class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td>&nbsp;</td>
					<td colspan="7" class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							
							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>		
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="10"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="10"><div class="redlabel" align="center">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{

				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs2);
					
					$cheque_id = $rows["cheque_id"];
					$cheque_status = $rows["cheque_status"];
					$cheque_voucher_type = $rows["cheque_voucher_type"];
					$cheque_reference_number = $rows["cheque_reference_number"];
					$cheque_sl_id = $rows["cheque_sl_id"];
					$cheque_bank_id = $rows["cheque_bank_id"];
					$cheque_number = $rows["cheque_number"];
					$cheque_date = $rows["cheque_date"];
					$cheque_date_cleared = $rows["cheque_date_cleared"];
					$cheque_date_released = $rows["cheque_date_released"];
					$cheque_date_cancelled = $rows["cheque_date_cancelled"];
					$cheque_date_approved_cancel = $rows["cheque_date_approved_cancel"];
					$cheque_cancelled_by = $rows["cheque_cancelled_by"];
					$cheque_approve_cancelled_by = $rows["cheque_approve_cancelled_by"];
					$cheque_payee = $rows["cheque_payee"];
					$cheque_currency = $rows["cheque_currency"];
					$cheque_amount = $rows["cheque_amount"];
					
					/*
					$cheque_status = $rows["cheque_status"];
					$cheque_number = $rows["cheque_number"];
					$cheque_currency = $rows["cheque_currency"];
					$cheque_amount = $rows["cheque_amount"];
					$cheque_date = $rows["cheque_date"];
					$cheque_date_cleared = $rows["cheque_date_cleared"];
					$cheque_date_cancelled = $rows["cheque_date_cancelled"];
					$cheque_cancelled_by = $rows["cheque_cancelled_by"];
					*/
					
					$sql_dv = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										dv_id,
										dv_number,
										dv_payee
								FROM 	tbl_disbursement_voucher
								WHERE	dv_id = '$cheque_reference_number'";
					$rows_dv = mysql_query($sql_dv) or die("Error in sql ".$sql_dv." ".mysql_error());
					$rows_dv = mysql_fetch_array($rows_dv);
					
					$dv_id = $rows_dv["dv_id"];
					$dv_number = $rows_dv["dv_number"];
					$dv_payee = $rows_dv["dv_payee"];

					$sql_bank = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										bank_name,
										bank_account_number
								FROM 	tbl_bank
								WHERE	bank_id = '$cheque_bank_id'";
					$rows_bank = mysql_query($sql_bank) or die("Error in sql ".$sql_bank." ".mysql_error());
					$rows_bank = mysql_fetch_array($rows_bank);
					
					$bank_name = $rows_bank["bank_name"];
					$bank_account_number = $rows_bank["bank_account_number"];					
					
					
					$bank = $bank_name."<br>[".$bank_account_number."]";
					$cheque_currency = getConfigurationValueById($cheque_currency);
					$cheque_cancelled_by = getFullName($cheque_cancelled_by,3);
					$dv_payee = getPayeeById($dv_payee);
					
					if ($cheque_date_cancelled != "0000-00-00 00:00:00" && $cheque_cancelled_by != "")
					{
						$cheque_status=4;
					}
					
					if ($cheque_status==0) { $cheque_stat = "No cheque issued"; }
					elseif ($cheque_status==1) { $cheque_stat = "Cheque issued, but not yet cleared"; }
					elseif ($cheque_status==2) { $cheque_stat = "Cheque cleared"; }
					elseif ($cheque_status==3) { $cheque_stat = "Cheque cancelled, but not yet approved"; }
					elseif ($cheque_status==4 ) { $cheque_stat = "Cheque cancelled"; }
					elseif ($cheque_status==5) { $cheque_stat = "Cheque released"; }
					else { $check_stat = "N/A"; }
					
					if ($cheque_date=="0000-00-00") {  $cheque_date = ""; }					
					if ($cheque_date_cleared=="0000-00-00 00:00:00") {  $cheque_date_cleared = ""; }
					if ($cheque_date_cancelled=="0000-00-00 00:00:00") {  $cheque_date_cancelled = ""; }
					
					$cheque_id = encrypt($cheque_id);
					$dv_id = encrypt($dv_id);
				?>      
				 <tr>
                    <td><?php echo $dv_number; ?></td>
                    <td><?php echo $dv_payee; ?></td>
                    <td><?php echo $bank; ?></td>
                    <td><?php echo $cheque_stat; ?></td>
                    <td><?php echo $cheque_number; ?></td>
                    <td><?php echo $cheque_date; ?></td>
                    <td align="right"><?php echo $cheque_currency." ".numberFormat($cheque_amount); ?></td>
                    <td><?php echo $cheque_date_cleared; ?></td>
                    <td><?php echo $cheque_date_cancelled; ?></td>
                    <td><?php echo $cheque_cancelled_by; ?></td>
				 </tr>
			   <tr>
					<td>Options</td>
					<td colspan="9">
					  <?php
					  if ($_SESSION["level"]==1 || $_SESSION["level"]==5 || $_SESSION["level"]==6)
					  {
							if ($cheque_status==0 || $cheque_status==1 && $cheque_status!=2 && $cheque_status!=3 && $cheque_status!=4) 
							{
							?>	
								<a href="cheque.add.php?cheque_id=<?php echo $cheque_id; ?>&dv_id=<?php echo $dv_id; ?>">[Add/Edit Cheque Number]</a>
							<?php
							}
							if ($cheque_status==5 && $cheque_status!=2 && $cheque_status!=3 && $cheque_status!=4) 
							{
							?>
								<a href="cheque.clear.php?cheque_id=<?php echo $cheque_id; ?>&dv_id=<?php echo $dv_id; ?>">[Clear/Unclear Cheque]</a>
							<?php
							}
							if ($cheque_status==1 && $cheque_status!=2 && $cheque_status!=3 && $cheque_status!=4 && $cheque_status!=5) 
							{
							?>
								<a href="cheque.cancel.php?cheque_id=<?php echo $cheque_id; ?>&dv_id=<?php echo $dv_id; ?>">[Cancel/Uncancel Cheque]</a>
							<?php
							}
							if ($cheque_status==1 && $cheque_status!=2 && $cheque_status!=3 && $cheque_status!=4) 
							{
							?>
								<a href="cheque.release.php?cheque_id=<?php echo $cheque_id; ?>&dv_id=<?php echo $dv_id; ?>">[Release/Unrelease Cheque]</a>
							<?php
							}
					  }
					  if ($cheque_status==3 && $_SESSION["level"]==2 || $_SESSION["level"]==3)
					  { 
						?>
							<a href="cheque.cancel.approve.php?cheque_id=<?php echo $cheque_id; ?>&dv_id=<?php echo $dv_id; ?>">[Approve Cancelled Cheque]</a>
						<?php
					  }
					  ?>						
					
					</td>
				</tr>				 
				<?php 
				} 
				?>
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	
	</td>
</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>