<?php 
session_start();
include("./../includes/header.main.php");

$page_size = getConfigurationValueByName('max_record_per_page');

$user_id = $_SESSION["id"];
$record_id = $_GET['id'];
$jv_num = $_GET['jv_number'];
$action = $_GET['action'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$search_for = $_GET['search_for'];
$search_by = $_GET['search_by'];
$sort_by = $_GET['sort_by'];
$year_now = date("Y");

if ($order=="") { $order = "jv_date"; }
if ($get_msg==1) { $msg = "Record updated successfully!"; $display_msg = 1; }
if ($get_msg==2) { $msg = "Record inserted successfully!"; $display_msg = 1; }
if($cur_page=="") { $cur_page= 1; }
if($cur_page==1) { $count_page = 0; }
else { $count_page = (($cur_page - 1) * $page_size); }

if ($record_id != "" && $action == "confirm_delete" && $_SESSION["level"]==1)
{
?>
	<script type="text/javascript">
	var flg=confirm('Are you sure you want to delete this record?\n\nClick OK to continue. Otherwise click Cancel.\n');
	if (flg==true)
	{
		
		window.location = '/workspaceGOP/page.transactions/journal.voucher.list.php?id=<?php echo $record_id; ?>&jv_number=<?php echo $jv_num; ?>&action=delete'
	}
	else
	{
		window.location = '/workspaceGOP/page.transactions/journal.voucher.list.php'
	}
	</script> 
    <?php
}
elseif ($action == "confirm_delete" && $_SESSION["level"]!=1)
{
	$msg = "Only administrator can delete a record!";
	$display_msg = 1;
}

if ($record_id != "" && $jv_num != "" && $action == "delete" && $_SESSION["level"]==1)
{
	$record_id = decrypt($record_id);
	$jv_num = decrypt($jv_num);
	
	$sql = "DELETE FROM tbl_journal_voucher
			WHERE 	jv_id = '$record_id'
			AND 	jv_number = '$jv_num'";
	mysql_query($sql) or die("Error in module: jv.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);

	$sql = "DELETE 	FROM tbl_journal_voucher_details
			WHERE	jv_details_reference_number  = '$record_id'";
	mysql_query($sql) or die("Error in module: jv.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);
	
	$sql = "DELETE  FROM tbl_general_ledger
			WHERE 	gl_report_type = 'JV'	
			AND		gl_reference_number = '$record_id'";
	mysql_query($sql) or die("Error in module: jv.list.php ".$sql." ".mysql_error());
	insertEventLog($user_id,$sql);
	
	$msg = "Record deleted successfully!";
	$display_msg = 1;
}
elseif ($action == "delete" && $_SESSION["level"]!=1)
{
	$msg = "Only administrator can delete a record!";
	$display_msg = 1;
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main" style='table-layout:fixed'>
<tr>
	<td>
		<form method="get">
			<table id="rounded-search-table">
				<thead>
					<tr>
						<th class="rounded-header"><div class="whitelabel">Search</div></th>
						<th class="rounded-q4">&nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="rounded-foot-left"><input type="submit" name="action" value="Search" class="formbutton"></td>
						<td class="rounded-foot-right" align="right">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td><input name="search_for" type="text" id="search_for" class="formbutton" value="<?php echo $search_for; ?>"></td>
						<td>
							<select name="search_by" class="formbutton">
							  <option value="ALL" selected>ALL</option>
							  <option value="jv_number" <?php if ($search_by == 'jv_number') { echo "selected"; } ?> >JV Number</option>
							  <option value="jv_date" <?php if ($search_by == 'jv_date') { echo "selected"; } ?> >JV Date</option>
							  <option value="jv_description" <?php if ($search_by == 'jv_description') { echo "selected"; } ?> >JV Description</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</td>
</tr>
<tr>
	<td align="left">&nbsp;</td>
</tr>
<?php
if ($_SESSION["level"]==1 || $_SESSION["level"]==2 || $_SESSION["level"]==3 || $_SESSION["level"]==4)
{
?>
<tr>
	<td align="right">
		<div class="main">Sort by: 
			<a href="/workspaceGOP/page.transactions/journal.voucher.list.php">All</a>
			<?php
			if ($_SESSION["level"]==1 || $_SESSION["level"]==5)
			{
			?>
			|
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=obligate">Pending Obligate</a>
			<?php
			}
			if ($_SESSION["level"]==1 || $_SESSION["level"]==4)
			{
			?>
			|
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=certify">Pending Certify</a>
			<?php
			}
			if ($_SESSION["level"]==1 || $_SESSION["level"]==3)
			{			
			?>
			|
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=recommended">Pending Recommendation</a>
			<?php
			}
			if ($_SESSION["level"]==1 || $_SESSION["level"]==2)
			{			
			?>			
			|
			<a href="<?php echo $page; ?>?search_for=<?php echo $search_for; ?>&search_by=<?php echo $search_by; ?>&action=<?php echo $action; ?>&cur_page=<?php echo $next_page; ?>&order=<?php echo $order; ?>&sort_by=approval">Pending Approval</a>
			<?php
			}
			?>
		</div>
	</td>
</tr>
<tr>
	<td align="left">&nbsp;</td>
</tr>
<?php
}
?>
<tr>
	<td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
			if ($action == "Search" && $search_for != "" && $search_by != "")
			{
				if ($search_for != "" && ($search_by != "" && $search_by != "ALL"))	
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										jv_id,
										jv_type,
										jv_number,
										jv_currency_code,
										jv_date,
										jv_payee,
										jv_description, 
										jv_obligated_by,
										jv_obligated_by_date,
										jv_prepared_by,
										jv_prepared_by_date, 
										jv_certified_by,
										jv_certified_by_date,
										jv_recommended_by,
										jv_recommended_by_date,
										jv_approved_by,
										jv_approved_by_date
								FROM 	tbl_journal_voucher
								WHERE 	".$search_by." LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE jv_obligated_by = 0 
									AND jv_obligated_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by = 0 
									AND jv_certified_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql1 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by = 0 
									AND jv_recommended_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by != 0 
									AND jv_recommended_by_date != '0000-00-00 00:00:00' 
									AND jv_approved_by = 0 
									AND jv_approved_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql1.= "	ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										jv_id,
										jv_type,
										jv_number,
										jv_currency_code,
										jv_date,
										jv_payee,
										jv_description, 
										jv_prepared_by,
										jv_prepared_by_date, 
										jv_certified_by,
										jv_certified_by_date,
										jv_recommended_by,
										jv_recommended_by_date,
										jv_approved_by,
										jv_approved_by_date
								FROM 	tbl_journal_voucher
								WHERE 	".$search_by." LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE jv_obligated_by = 0 
									AND jv_obligated_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by = 0 
									AND jv_certified_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql2 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by = 0 
									AND jv_recommended_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by != 0 
									AND jv_recommended_by_date != '0000-00-00 00:00:00' 
									AND jv_approved_by = 0 
									AND jv_approved_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql2.= "	ORDER BY $order DESC ";
					$sql2.= "	LIMIT	$count_page, $page_size";
				}
				if ($search_for == "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										dv_id,
										dv_type,
										dv_number,
										dv_currency_code,
										dv_date,
										dv_payee,
										dv_description, 
										dv_prepared_by, 
										dv_prepared_by_date,
										dv_obligated_by,
										dv_obligated_by_date,
										dv_certified_by,
										dv_certified_by_date,
										dv_recommended_by,
										dv_recommended_by_date,
										dv_approved_by,
										dv_approved_by_date
								FROM 	tbl_disbursement_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE jv_obligated_by = 0 
									AND jv_obligated_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by = 0 
									AND jv_certified_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql1 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by = 0 
									AND jv_recommended_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by != 0 
									AND jv_recommended_by_date != '0000-00-00 00:00:00' 
									AND jv_approved_by = 0 
									AND jv_approved_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql1.= "	ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										dv_id,
										dv_type,
										dv_number
										dv_currency_code,
										dv_date,
										dv_payee,
										dv_description, 
										dv_prepared_by,
										dv_prepared_by_date, 
										dv_obligated_by,
										dv_obligated_by_date,
										dv_certified_by,
										dv_certified_by_date,
										dv_recommended_by,
										dv_recommended_by_date,
										dv_approved_by,
										dv_approved_by_date
								FROM 	tbl_disbursement_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE jv_obligated_by = 0 
									AND jv_obligated_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by = 0 
									AND jv_certified_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql2 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by = 0 
									AND jv_recommended_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by != 0 
									AND jv_recommended_by_date != '0000-00-00 00:00:00' 
									AND jv_approved_by = 0 
									AND jv_approved_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql2.= "	ORDER BY $order DESC ";
					$sql2 .="	LIMIT	$count_page, $page_size";		
				}
				if ($search_for != "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										jv_id,
										jv_type,
										jv_number,
										jv_currency_code,
										jv_date,
										jv_payee,
										jv_description, 
										jv_prepared_by, 
										jv_prepared_by_date,
										jv_obligated_by,
										jv_obligated_by_date,
										jv_certified_by,
										jv_certified_by_date,
										jv_recommended_by,
										jv_recommended_by_date,
										jv_approved_by,
										jv_approved_by_date
								FROM 	tbl_journal_voucher
								WHERE	jv_number LIKE '%".$search_for."%'	
								OR jv_date LIKE '%".$search_for."%'
								OR jv_description LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE jv_obligated_by = 0 
									AND jv_obligated_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by = 0 
									AND jv_certified_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql1 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by = 0 
									AND jv_recommended_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by != 0 
									AND jv_recommended_by_date != '0000-00-00 00:00:00' 
									AND jv_approved_by = 0 
									AND jv_approved_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql1.= "	ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										jv_id,
										jv_type,
										jv_number,
										jv_currency_code,
										jv_date,
										jv_payee,
										jv_description, 
										jv_prepared_by, 
										jv_prepared_by_date,
										jv_obligated_by,
										jv_obligated_by_date,
										jv_certified_by,
										jv_certified_by_date,
										jv_recommended_by,
										jv_recommended_by_date,
										jv_approved_by,
										jv_approved_by_date
								FROM 	tbl_journal_voucher
								WHERE	jv_number LIKE '%".$search_for."%'	
								OR jv_date LIKE '%".$search_for."%'
								OR jv_description LIKE '%".$search_for."%' ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE jv_obligated_by = 0 
									AND jv_obligated_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by = 0 
									AND jv_certified_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql2 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by = 0 
									AND jv_recommended_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by != 0 
									AND jv_recommended_by_date != '0000-00-00 00:00:00' 
									AND jv_approved_by = 0 
									AND jv_approved_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}								
					$sql2.= "	ORDER BY $order DESC ";
					$sql2.= "	LIMIT	$count_page, $page_size";
				}
			}
			else
			{
				$sql1 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									jv_id,
									jv_type,
									jv_number
									jv_currency_code,
									jv_date,
									jv_payee,
									jv_description, 
									jv_prepared_by, 
									jv_prepared_by_date,
									jv_obligated_by,
									jv_obligated_by_date,
									jv_certified_by,
									jv_certified_by_date,
									jv_recommended_by,
									jv_recommended_by_date,
									jv_approved_by,
									jv_approved_by_date
							FROM 	tbl_journal_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql1 .= " 	WHERE jv_obligated_by = 0 
									AND jv_obligated_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql1 .= " 	WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by = 0 
									AND jv_certified_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql1 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by = 0 
									AND jv_recommended_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql1 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by != 0 
									AND jv_recommended_by_date != '0000-00-00 00:00:00' 
									AND jv_approved_by = 0 
									AND jv_approved_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}							
					$sql1.= "	ORDER BY $order DESC ";

				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									jv_id,
									jv_type,
									jv_number,
									jv_currency_code,
									jv_date,
									jv_payee,
									jv_description, 
									jv_prepared_by, 
									jv_prepared_by_date,
									jv_obligated_by,
									jv_obligated_by_date,
									jv_certified_by,
									jv_certified_by_date,
									jv_recommended_by,
									jv_recommended_by_date,
									jv_approved_by,
									jv_approved_by_date
							FROM 	tbl_journal_voucher ";
					if ($sort_by == "obligate") 
					{
						$sql2 .= " 	WHERE jv_obligated_by = 0 
									AND jv_obligated_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "certify") 
					{
						$sql2 .= " 	WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by = 0 
									AND jv_certified_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "recommended") 
					{ 
						$sql2 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by = 0 
									AND jv_recommended_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' "; 
					}
					if ($sort_by == "approval") 
					{ 
						$sql2 .= "  WHERE jv_obligated_by != 0 
									AND jv_obligated_by_date != '0000-00-00 00:00:00'
									AND jv_certified_by != 0 
									AND jv_certified_by_date != '0000-00-00 00:00:00' 
									AND jv_recommended_by != 0 
									AND jv_recommended_by_date != '0000-00-00 00:00:00' 
									AND jv_approved_by = 0 
									AND jv_approved_by_date = '0000-00-00 00:00:00' 
									AND jv_cancelled_by = 0 
									AND jv_cancelled_by_date = '0000-00-00 00:00:00' ";
					}							
					$sql2.= "	ORDER BY $order DESC ";
					$sql2.= "	LIMIT	$count_page, $page_size";
			}	
			$rs1 = mysql_query($sql1) or die("Error in sql1 in module: jv.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs1);
			$total_pages = ceil($total_records/$page_size); 
			$rs2 = mysql_query($sql2) or die("Error in sql2 in module: jv.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs2);	
			?>
			<thead>
				<tr>
					<th width="10%" align="center" class="rounded-header" scope="col"><a href="<?php echo $page; ?>?order=jv_number" class="whitelabel">JV Number</a></th>
					<th width="5%" align="center" class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=jv_date" class="whitelabel">Transaction Date</a></th>
					<th width="25%" align="center" class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=jv_payee" class="whitelabel">Payee</a></th>
					<th width="50%" align="center" class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=jv_description" class="whitelabel">Particulars/Description</a></th>
					<th width="5%" align="center" class="rounded-q2" scope="col"><a href="<?php echo $page; ?>?order=jv_currency_code" class="whitelabel">Currency</a></th>
				  <th width="5%" align="center" class="rounded-q4" scope="col"><div class="whitelabel">Transaction Total</div></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2" class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td colspan="4" class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							
							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="6"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="6"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{

				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs2);

					$jv_id = $rows["jv_id"];
					$jv_number = $rows["jv_number"];
					$jv_type = $rows["jv_type"];
					$jv_currency_code = $rows["jv_currency_code"];
					$jv_date = $rows["jv_date"];
					$jv_payee = $rows["jv_payee"];
					$jv_description = $rows["jv_description"];
					$jv_prepared_by = $rows["jv_prepared_by"];
					$jv_certified_by = $rows["jv_certified_by"];
					$jv_recommended_by =$rows["jv_recommended_by"];
					$jv_approved_by = $rows["jv_approved_by"];
					$jv_prepared_by_date = $rows["jv_prepared_by_date"];
					$jv_obligated_by = $rows["jv_obligated_by"];
					$jv_obligated_by_date = $rows["jv_obligated_by_date"];
					$jv_certified_by_date = $rows["jv_certified_by_date"];
					$jv_recommended_by_date = $rows["jv_recommended_by_date"];
					$jv_approved_by_date = $rows["jv_approved_by_date"];
					$jv_cancelled_by = $rows["jv_cancelled_by"];
					$jv_cancelled_by_date = $rows["jv_cancelled_by_date"];

					$jv_prepared_by_id = $jv_prepared_by;
					$jv_description = stripslashes($jv_description);
					$jv_payee = getPayeeById($jv_payee);
					$jv_currency_code = getConfigurationValueById($jv_currency_code);
					
					if ($jv_prepared_by_date=="0000-00-00 00:00:00") { $jv_prepared_by_date = ""; }
					if ($jv_obligated_by_date=="0000-00-00 00:00:00") {  $jv_obligated_by_date = ""; }
					if ($jv_certified_by_date=="0000-00-00 00:00:00") { $jv_certified_by_date = ""; }
					if ($jv_recommended_by_date=="0000-00-00 00:00:00") { $jv_recommended_by_date = ""; }
					if ($jv_approved_by_date=="0000-00-00 00:00:00") { $jv_approved_by_date = ""; }
					if ($jv_cancelled_by_date=="0000-00-00 00:00:00") { $jv_cancelled_by_date = ""; }
										
					if ($jv_prepared_by!=0) { $jv_prepared_by = getFullName($jv_prepared_by,3); } else { $jv_prepared_by=""; }
					if ($jv_obligated_by!=0) { $jv_obligated_by = getFullName($jv_obligated_by,3); } else { $jv_obligated_by=""; }
					if ($jv_certified_by!=0) { $jv_certified_by = getFullName($jv_certified_by,3); } else { $jv_certified_by=""; }
					if ($jv_recommended_by!=0) { $jv_recommended_by = getFullName($jv_recommended_by,3); } else { $jv_recommended_by=""; }
					if ($jv_approved_by!=0) { $jv_approved_by = getFullName($jv_approved_by,3); } else { $jv_approved_by=""; }
					if ($jv_cancelled_by!=0) { $jv_cancelled_by = getFullName($jv_cancelled_by,3); } else { $jv_cancelled_by=""; }
					
					$array_total_debit_credit = getTotalDebitCredit($jv_type,$jv_id);

					$jv_total_debit = $array_total_debit_credit[0]["total_debit"];
					$jv_total_credit = $array_total_debit_credit[0]["total_credit"];
					
					$jv_total_debit = numberFormat($jv_total_debit);
					$jv_total_credit = numberFormat($jv_total_credit);
					
					if ($jv_total_debit!=0)
					{
						$jv_transaction_total = $jv_total_debit;
					}
					elseif ($jv_total_credit!=0)
					{
						$jv_transaction_total = $jv_total_credit;
					}
					else
					{
						$jv_transaction_total = 0;
						$jv_transaction_total = numberFormat($jv_transaction_total);
					}


					/************************ansel*******************/
					
					
					if($user_id == 21 || $user_id == 22)
					{
						if($rows['jv_currency_code'] == 16)
						{
								$trans_month = date('m', strtotime($rows['jv_date']));
								$trans_year = date('Y', strtotime($rows['jv_date']));
								
								$curres = mysql_query("SELECT forex_currency_value FROM tbl_forex WHERE forex_transaction_date = '{$trans_month}-{$trans_year}' AND forex_currency_code = 35") or die(mysql_error());
								$quecnt = mysql_num_rows($curres);
								
								while($quecnt <= 0 && $trans_month > 1)
								{									
									
									$trans_month -= 1;
									if($trans_month <= 9)
										$trans_month_text = '0'.$trans_month;
									else
										$trans_month_text = $trans_month;
										
									$curres = mysql_query("SELECT forex_currency_value FROM tbl_forex WHERE forex_transaction_date = '{$trans_month_text}-{$trans_year}' AND forex_currency_code = 35") or die(mysql_error());
									$quecnt = mysql_num_rows($curres);
								}
								$currow = mysql_fetch_array($curres);
								
								$currate = $currow[0];
								
								$totval = str_replace(',','',$jv_transaction_total) / $currate;
								
						}
						else
						{
							$totval = str_replace(',','',$jv_transaction_total);
						}
					}
					
					/******************************************************/
					
					$jv_id = encrypt($jv_id);
				?>      
				 <tr>
					<td valign="top"><?php echo $jv_number; ?>&nbsp;</td>
					<td valign="top"><?php echo $jv_date; ?>&nbsp;</td>
					<td valign="top"><?php echo $jv_payee; ?>&nbsp;</td>
					<td valign="top"><?php echo $jv_description; ?>&nbsp;</td>
					<td valign="top"><?php echo $jv_currency_code; ?>&nbsp;</td>
					<td valign="top"><?php echo $jv_transaction_total; ?>&nbsp;</td>
				</tr>
				<tr>
					<td>Prepared by</td>
					<td colspan="5"><?php echo $jv_prepared_by; ?>&nbsp;<?php echo $jv_prepared_by_date; ?></td>
				</tr>
				<tr>
				  <td>Obligated by</td>
				  <td colspan="5"><?php echo $jv_obligated_by; ?>&nbsp;<?php echo $jv_obligated_by_date; ?></td>
			  </tr>
				<tr>
					<td>Certified by</td>
					<td colspan="5"><?php echo $jv_certified_by; ?>&nbsp;<?php echo $jv_certified_by_date; ?></td>
				</tr>	
				<tr>
					<td>Recommended by</td>
					<td colspan="5"><?php echo $jv_recommended_by; ?>&nbsp;<?php echo $jv_recommended_by_date; ?></td>
				</tr>	
				<tr>
					<td>Approved by</td>
					<td colspan="5"><?php echo $jv_approved_by; ?>&nbsp;<?php echo $jv_approved_by_date; ?></td>
				</tr>
				<tr>
					<td>Options</td>
					<td colspan="5">
							<a href="javascript:void(0);" NAME="View Record" title="View Record" onClick="					MM_openBrWindow('journal.voucher.sl.details.php?id=<?php echo $jv_id; ?>&jv_number=<?php echo md5($jv_number); ?>','formtarget','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1080,height=700')">Voucher Details</a>
							|
							<a href="javascript:void(0);" NAME="View Record" title="View Record" onClick="MM_openBrWindow('journal.voucher.print.voucher.php?id=<?php echo $jv_id; ?>&jv_number=<?php echo md5($jv_number); ?>','formtarget','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1080,height=700')">Print Voucher</a>						
							|
							<a href="/workspaceGOP/report.templates/journal.voucher.php?id=<?php echo $jv_id; ?>&jv_number=<?php echo md5($jv_number); ?>">Export to word</a>
						<?php
												
						if ($jv_obligated_by == "" && $jv_certified_by == "" && $jv_recommended_by == "" && $jv_approved_by == "" && $jv_cancelled_by == "" && $jv_prepared_by_id == $user_id)
						{
					  	?>						
							|
							<a href="journal.voucher.edit.php?id=<?php echo $jv_id; ?>&jv_number=<?php echo encrypt($jv_number); ?>">Edit Record</a>
						<?php
						}
						?>
						<?php
					  	if ($jv_transaction_total != "0.00" && $jv_cancelled_by == "" && ($_SESSION["level"]==5 || $_SESSION["level"]==1))
						{
					  	?>
							|
							<a href="journal.voucher.obligate.php?id=<?php echo $jv_id; ?>&jv_number=<?php echo encrypt($jv_number); ?>">Obligate/Unobligate Record</a>
						<?php
						}
						?>                        
						<?php
					  	if (($_SESSION["level"]==4 || $_SESSION["level"]==1) && $jv_transaction_total != "0.00" && $jv_obligated_by != "" && $jv_cancelled_by == "")
						{
					  	?>
							|
							<a href="journal.voucher.certify.php?id=<?php echo $jv_id; ?>&jv_number=<?php echo encrypt($jv_number); ?>">Certify/Uncertify Record</a>
						<?php
						}
						?>		
						<?php
						if (($_SESSION["level"]==3 || $_SESSION["level"]==1) && $jv_transaction_total != "0.00" && $jv_obligated_by != "" && $jv_certified_by != "" && $jv_cancelled_by == "")
						{
						?>
						
						<?php
							/****************************ansel************************/
							//if(($user_id == 22 && $totval < 20000) || $user_id != 22)
							//{
								echo "|<a href=\"journal.voucher.recommend.php?id=$jv_id&jv_number=".encrypt($jv_number)."\">Recommend/Unrecommend Record</a>";						
							//}
							/*********************************************************/
						?>
								
						<?php
						}
						?>	
						<?php	
						if (($_SESSION["level"]==2 || $_SESSION["level"]==1) && $jv_transaction_total != "0.00" && $jv_obligated_by != "" && $jv_certified_by != "" && $jv_recommended_by != "" && $jv_cancelled_by == "")
						{
						?>				
						
						<?php
							/**************************ansel************************/
							//if(($user_id == 21 && $totval < 20000) || $user_id != 21)				
							//{
								echo "|<a href=\"journal.voucher.approve.php?id=$jv_id&jv_number=".encrypt($jv_number)."\">Approved/Disapproved Record</a>";
							//}
							/*******************************************************/
						?>
							
						<?php
						}
						?>	
						<?php							
						if ($_SESSION["level"]==1 || $jv_prepared_by_id == $user_id)
						{
					  	?>						
							|
							<a href="journal.voucher.cancel.voucher.php?id=<?php echo $jv_id; ?>&jv_number=<?php echo encrypt($jv_number); ?>">Cancel/uncancel Voucher</a>
						<?php
						}
						?> 
						<?php							
						if ($_SESSION["level"]==1 || $jv_prepared_by_id == $user_id)
						{
					  	?>						
							|
							<a href="journal.voucher.add.to.bank.reconciliation.php?id=<?php echo $jv_id; ?>&jv_number=<?php echo encrypt($jv_number); ?>">Add/Remove to Bank Reconciliation Report</a>
						<?php
						}
						?>                                                	
						<?php
						if ($_SESSION["level"]==1 && $jv_certified_by == "" && $jv_recommended_by == "" && $jv_approved_by == "")
						{
						?>	
							|					
							<a href="journal.voucher.list.php?id=<?php echo $jv_id; ?>&jv_number=<?php echo encrypt($jv_number); ?>&action=confirm_delete">Delete Record</a>							
						<?php
						}
						?>					</td>
				</tr>	
				<tr>
					<td colspan="6"><hr></td>
				</tr>													
				<?php 
				} 
				?>
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	
	</td>
</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>