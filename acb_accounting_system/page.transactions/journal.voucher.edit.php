<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$display_msg = 0;
$jv_type = "JV";
$id = decrypt($id);

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;
$date_time_inserted = date("Y-m-d H:i:s");

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE				
				jv_id,
				jv_year,
				jv_sequence_number,				
				jv_number,
				jv_type,
				jv_currency_code,
				jv_date,
				jv_payee,
				jv_description,
				jv_certified_by,
				jv_certified_by_date,
				jv_recommended_by,
				jv_recommended_by_date,
				jv_approved_by,
				jv_approved_by_date
		FROM 	tbl_journal_voucher
		WHERE	jv_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: journal.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["jv_id"];
$jv_number = $rows["jv_number"];
$jv_type = $rows["jv_type"];
$jv_currency_code = $rows["jv_currency_code"];
$jv_date = $rows["jv_date"];
$jv_payee = $rows["jv_payee"];
$jv_description = $rows["jv_description"];
$jv_prepared_by = $rows["jv_prepared_by"];
$jv_certified_by = $rows["jv_certified_by"];
$jv_certified_by_date = $rows["jv_certified_by_date"];
$jv_recommended_by =$rows["jv_recommended_by"];
$jv_recommended_by_date = $rows["jv_recommended_by_date"];
$jv_approved_by = $rows["jv_approved_by"];
$jv_approved_by_date =$rows ["jv_approved_by_date"];

$jv_description = stripslashes($jv_description);
$chunk_date = explode("-",$jv_date);
$year = $chunk_date[0];
$month = $chunk_date[1];
$day = $chunk_date[2];

if ($_POST['Submit'] == 'Update Details')
{
	$msg = "";
	
	$record_id = $_POST['record_id'];
	$jv_currency_code = $_POST['jv_currency_code'];
	$jv_number = $_POST['jv_number'];
	$jv_year = $_POST['jv_year'];
	$jv_month = $_POST['jv_month'];
	$jv_day = $_POST['jv_day'];
	$jv_payee = $_POST['jv_payee'];
	$jv_description = $_POST['jv_description'];
	$jv_description = addslashes($jv_description);

	$currency_name = getVoucherId($jv_currency_code);
	$arr_old_currency = stringtoarray($jv_number);
	$cur_old = $arr_old_currency[0];

	if ($cur_old!=$currency_name)
	{
		$jv_number = getVoucherSequenceNumber("JV",$jv_currency_code,$jv_month,$jv_year);
		$jv_sequence_number = substr($jv_number,-4);
		$set_new_jv = 1;
	}	

	$jv_date = dateTimeFormat($hr="",$min="",$sec="",$jv_month,$jv_day,$jv_year);
	
	if ($jv_currency_code == "") { $display_msg=1; $msg .= "Please select Currency"; $msg .="<br>"; }
	if ($jv_description == "") { $display_msg=1; $msg .= "Please enter Description"; $msg .="<br>"; }
	
	if ($msg=="")
	{
		$sql = "UPDATE 	tbl_journal_voucher
				SET		jv_date = '".trim($jv_date)."',
						jv_payee = '".trim($jv_payee)."',
						jv_description = '".trim($jv_description)."',
						jv_inserted_date = '".trim($date_time_inserted)."'";
		if ($set_new_jv == 1)
		{
			$sql .= ",";
			$sql .= "jv_number = '".trim($jv_number)."',";
			$sql .= "jv_sequence_number = '".trim($jv_sequence_number)."',";
			$sql .= "jv_currency_code = '".trim($jv_currency_code)."'";
		}
		$sql .= "WHERE	jv_id = '$record_id'";

		mysql_query($sql) or die("Error inserting values : module : journal.voucher.add.php ".$sql." ".mysql_error());
		insertEventLog($user_id,$sql);
		$jv_number = encrypt($jv_number);
		$record_id = encrypt($record_id);
		header("location:/workspaceGOP/page.transactions/journal.voucher.add.details.php?jv_id=$record_id&jv_number=$jv_number");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="journal_voucher" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" colspan="2" class="rounded-header"><div class="main" align="left"><strong>Journal Voucher [Edit Record]</strong></div></th>
							<th scope="col" colspan="2" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" colspan="2" align="left"><input type="submit" name="Submit" value="Update Details" class="formbutton"></td>
							<td class="rounded-foot-right" colspan="2" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
					  <tr>
						<td scope="col"><b>JV Number*</b></td>
						<td scope="col" colspan="3">
							<input name="jv_number" type="hidden" class="formbutton" id="jv_number" value="<?php echo $jv_number; ?>" />
							<?php echo $jv_number; ?>
						</td>
					  </tr>
					  <tr>
						<td scope="col"><b>Currency*</b></td>
						<td scope="col">
								<?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_id,
													cfg_value
											FROM 	tbl_config
											WHERE 	cfg_name = 'currency_type'";
									$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
									$str .= "<select name='jv_currency_code' class='formbutton'>";
									while($data=mysql_fetch_array($rs))
									{
										$column_id = $data['cfg_id'];
										$column_name = $data['cfg_value'];
										$str .= "<option value='$column_id'";
										if ($jv_currency_code == $column_id)
										{
											$str .= " SELECTED";
										}
										$str .= ">".$column_name."";
									}
									$str .= "</select>&nbsp;";
									echo $str;
								?>						</td>
						<td scope="col"><b>Date*</b></td>
						<td scope="col">
							<select name='jv_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='jv_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='jv_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day )
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>						  
						</td>
					  </tr>
					  <tr>
						<td scope="col"><b>Payee*</b></td>
						<td colspan="3" scope="col">
								<?php
									$sql1 = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													payee_id,
													payee_name
											FROM 	tbl_payee
											ORDER BY payee_name";
									$rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
									$str1 .= "<select name='jv_payee' class='formbutton'>";
									$str1 .= "<option value=''>[SELECT PAYEE]";
									while($data1=mysql_fetch_array($rs1))
									{
										$column_id = $data1['payee_id'];
										$column_name = $data1['payee_name'];
										$str1 .= "<option value='$column_id'";
										if ($jv_payee == $column_id)
										{
											$str1 .= " SELECTED";
										}
										$str1 .= ">".$column_name."";
									}
									$str1 .= "</select>&nbsp;";
									echo $str1;
								?>						
						</td>
					  </tr>
					  <tr>
						<td valign="top" scope="col"><b>Particulars/Description*</b></td>
					    <td colspan="3" valign="top" scope="col">
					    <textarea name="jv_description" cols="40" rows="5" class="formbutton"><?php echo $jv_description; ?></textarea>
					    </td>
				      </tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>