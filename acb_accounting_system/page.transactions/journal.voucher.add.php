<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$display_msg = 0;
$jv_type = "JV";

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$year = date("Y");
$month = date("m");
$day = date("d");
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;
$date_time_inserted = date("Y-m-d H:i:s");

if ($_POST['Submit'] == 'Add Details')
{
	$msg = "";
	
	$jv_currency_code = $_POST['jv_currency_code'];
	$jv_year = $_POST['jv_year'];
	$jv_month = $_POST['jv_month'];
	$jv_day = $_POST['jv_day'];
	$jv_payee = $_POST['jv_payee'];
	$jv_description = $_POST['jv_description'];
	$jv_description = addslashes($jv_description);
	
	$jv_number = getVoucherSequenceNumber("JV",$jv_currency_code,$jv_month,$jv_year);
	$jv_sequence_number = substr($jv_number,-4);
	
	$jv_date = dateTimeFormat($hr="",$min="",$sec="",$jv_month,$jv_day,$jv_year);

	if ($jv_currency_code == "") { $display_msg=1; $msg .= "Please select Currency"; $msg .="<br>"; }
	if ($jv_description == "") { $display_msg=1; $msg .= "Please enter Description"; $msg .="<br>"; }
	
	$sql = "SELECT 	COUNT(jv_number) AS record_count
			FROM	tbl_journal_voucher
			WHERE	jv_number = '$jv_number'";
	$rs = mysql_query($sql) or die("Error verifying jv value : module : journal.voucher.add.php ".$sql." ".mysql_error());
	$rows = mysql_fetch_array($rs);
	$record_count = $rows["record_count"];
	if ($record_count != 0) { $display_msg=1; $msg .= "JV Number (".$jv_number.") already exists!"; $msg .="<br>"; }
	
	if ($msg=="")
	{
			$sql = "INSERT INTO tbl_journal_voucher 
								(
									jv_year,
									jv_sequence_number,
									jv_number,
									jv_type,
									jv_currency_code,
									jv_date,
									jv_payee,
									jv_description,
									jv_prepared_by,
									jv_prepared_by_date,
									jv_inserted_date
								)
					VALUES 		(
									'".trim($jv_year)."',
									'".trim($jv_sequence_number)."',
									'".trim($jv_number)."',
									'JV',
									'".trim($jv_currency_code)."',
									'".trim($jv_date)."',
									'".trim($jv_payee)."',
									'".trim($jv_description)."',
									'".trim($user_id)."',
									'".trim($date_time_inserted)."',
									'".trim($date_time_inserted)."'
								)";
			mysql_query($sql) or die("Error inserting values : module : journal.voucher.add.php ".$sql." ".mysql_error());
			$record_number = mysql_insert_id();
			insertEventLog($user_id,$sql);
			$jv_number = encrypt($jv_number);
			$record_number = encrypt($record_number);
			header("location: /workspaceGOP/page.transactions/journal.voucher.add.details.php?jv_id=$record_number&jv_number=$jv_number");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="journal_voucher" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" colspan="2" class="rounded-header"><div class="main" align="left"><strong>Journal Voucher [Add Record]</strong></div></th>
							<th scope="col" colspan="2" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" colspan="2" align="left"><input type="submit" name="Submit" value="Add Details" class="formbutton"></td>
							<td class="rounded-foot-right" colspan="2" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
					  <tr>
						<td scope="col"><b>Currency*</b></td>
						<td scope="col">						
								<?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_id,
													cfg_value
											FROM 	tbl_config
											WHERE 	cfg_name = 'currency_type'";
									$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
									?>
									<select name='jv_currency_code' class='formbutton'>
									<?php
									while($data=mysql_fetch_array($rs))
									{
										$column_id = $data['cfg_id'];
										$column_name = $data['cfg_value'];
										?>
										<option value='<?php echo $column_id; ?>'
										<?php
										if ($jv_currency_code == $column_id)
										{
											?>
											SELECTED
											<?php
										}
										?>
										><?php echo $column_name;
									}
									?>
									</select>&nbsp;
	
						</td>
						<td scope="col"><b>Date*</b></td>
						<td scope="col">
							<select name='jv_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='jv_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='jv_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day )
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>						  
							</td>
					  </tr>
					  <tr>
						<td scope="col"><b>Payee*</b></td>
						<td colspan="3" scope="col">
								<?php
									$sql1 = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													payee_id,
													payee_name
											FROM 	tbl_payee
											ORDER BY payee_name";
									$rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
									$str1 .= "<select name='jv_payee' class='formbutton'>";
									$str1 .= "<option value=''>[SELECT PAYEE]";
									while($data1=mysql_fetch_array($rs1))
									{
										$column_id = $data1['payee_id'];
										$column_name = $data1['payee_name'];
										$str1 .= "<option value='$column_id'";
										if ($jv_payee == $column_id)
										{
											$str1 .= " SELECTED";
										}
										$str1 .= ">".$column_name."";
									}
									$str1 .= "</select>&nbsp;";
									echo $str1;
								?>						
						</td>
					  </tr>
					  <tr>
						<td valign="top" scope="col"><b>Particulars/Description*</b></td>
					    <td colspan="3" valign="top" scope="col"><label>
					      <textarea name="jv_description" cols="40" rows="5" class="formbutton"><?php echo $jv_description; ?></textarea>
					    </label></td>
				      </tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>