<?php 
$report_name = "Journal Voucher";
include("./../includes/header.report.php");
$report_name = "JOURNAL VOUCHER";
$rep_id = $_GET['id'];
$rep_jv_number = $_GET['jv_number'];
$rep_id = decrypt($rep_id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE	
				jv_id,			
				jv_type,
				jv_number,
				jv_currency_code,
				jv_date,
				jv_payee,
				jv_description,
				jv_prepared_by,
				jv_certified_by,
				jv_recommended_by,
				jv_approved_by
		FROM 	tbl_journal_voucher
		WHERE	jv_id = '$rep_id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: journal.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$jv_id = $rows["jv_id"];
$jv_type = $rows["jv_type"];
$jv_number = $rows["jv_number"];
$jv_currency_code = $rows["jv_currency_code"];
$jv_date = $rows["jv_date"];
$jv_payee = $rows["jv_payee"];
$jv_description = $rows["jv_description"];
$jv_prepared_by = $rows["jv_prepared_by"];
$jv_certified_by = $rows["jv_certified_by"];
$jv_recommended_by = $rows["jv_recommended_by"];
$jv_approved_by = $rows["jv_approved_by"];

$jv_payee = getPayeeById($jv_payee);
$jv_description = stripslashes($jv_description);
$jv_currency_name = getConfigurationDescriptionById($jv_currency_code);
$jv_currency_code = getConfigurationValueById($jv_currency_code);

if ($jv_prepared_by!=0) { $jv_prepared_by = getFullName($jv_prepared_by,3); } else { $jv_prepared_by=""; }
if ($jv_certified_by!=0) { $jv_certified_by = getFullName($jv_certified_by,3); } else { $jv_certified_by=""; }
if ($jv_recommended_by!=0) { $jv_recommended_by = getFullName($jv_recommended_by,3); } else { $jv_recommended_by=""; }
if ($jv_approved_by!=0) { $jv_approved_by = getFullName($jv_approved_by,3); } else { $jv_approved_by=""; }

if ($rep_id == $jv_id && $rep_jv_number == md5($jv_number))
{
?>
<table width='100%' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>
	<tr>
		<td scope='col'>
		<!-- Start of Inner Table -->
			<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC' class='report_printing'>
			  <tr>
				<th rowspan='3' scope='col'>Transaction Date </th>
				<th rowspan='3' scope='col'>Voucher Number </th>
				<th rowspan='3' scope='col'>Payee</th>
				<th rowspan='3' scope='col'>Local Amount </th>
				<th rowspan='3' scope='col'>Particulars / Description </th>
				<th rowspan='3' scope='col'>Debit / Credit</th>
				<th rowspan='3' scope='col'>Account Code </th>
				<th colspan='9' scope='col'>SL CODE </th>
				<th rowspan='3' scope='col'>Amount in Words </th>
				<th rowspan='3' scope='col'>Acknowledgement Portion </th>
				<th rowspan='3' scope='col'>Prepared By </th>
				<th rowspan='3' scope='col'>Certified By </th>
				<th rowspan='3' scope='col'>Recommended by </th>
				<th rowspan='3' scope='col'>Approved by </th>
			  </tr>
			  <tr>
				<th>SLC1</th>
				<th>SLC2</th>
				<th>SLC3</th>
				<th>SLC4</th>
				<th>SLC5</th>
				<th>SLC6</th>
				<th>SLC7</th>
				<th>SLC8</th>
				<th>SLC9</th>
			  </tr>
			  <tr>
				<th>Budget ID </th>
				<th>Donor ID </th>
				<th>Component ID </th>
				<th>Activity ID </th>
				<th>Other Cost / Services ID </th>
				<th>Staff ID </th>
				<th>Benefits ID </th>
				<th>Vehicle ID </th>
				<th>Equipment ID </th>
			  </tr>
			  <tr>
				<td valign='top'><?php echo $jv_date; ?>&nbsp;</td>
				<td valign='top'><?php echo $jv_number; ?>&nbsp;</td>
				<td valign='top'><?php echo $jv_payee; ?>&nbsp;</td>
				<td valign='top'><?php echo $jv_currency_code; ?>&nbsp;</td>
				<td valign='top'><?php echo $jv_description; ?>&nbsp;</td>
				<?php
				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									jv_details_id,
									jv_details_account_code,
									jv_details_reference_number,
									jv_details_debit,
									jv_details_credit,
									jv_details_budget_id,
									jv_details_donor_id,
									jv_details_component_id,
									jv_details_activity_id,
									jv_details_other_cost_id,
									jv_details_staff_id,
									jv_details_benefits_id,
									jv_details_vehicle_id,
									jv_details_equipment_id
							FROM 	tbl_journal_voucher_details
							WHERE	jv_details_reference_number = '$jv_id'";
				$rs2 = mysql_query($sql2) or die("Error in sql2 in module: journal.voucher.details.php ".$sql2." ".mysql_error());
				$total_rows = mysql_num_rows($rs2);	
				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows2 = mysql_fetch_array($rs2);
					$jv_details_id[] = $rows2['jv_details_id'];
					$jv_details_reference_number[] = $rows2['jv_details_reference_number'];
					$jv_details_account_code[] = $rows2['jv_details_account_code'];
					$jv_details_debit = $rows2['jv_details_debit'];
					$jv_details_credit = $rows2['jv_details_credit'];
					$jv_details_budget_id[] = $rows2['jv_details_budget_id'];
					$jv_details_donor_id[] = $rows2['jv_details_donor_id'];
					$jv_details_component_id[] = $rows2['jv_details_component_id'];
					$jv_details_activity_id[] = $rows2['jv_details_activity_id'];
					$jv_details_other_cost_id[] = $rows2['jv_details_other_cost_id'];
					$jv_details_staff_id[] = $rows2['jv_details_staff_id'];
					$jv_details_benefits_id[] = $rows2['jv_details_benefits_id'];
					$jv_details_vehicle_id[] = $rows2['jv_details_vehicle_id'];
					$jv_details_equipment_id[] = $rows2['jv_details_equipment_id'];
					
					if (numberFormat($jv_details_debit)!=0)
					{
						$jv_transaction_total[] = numberFormat($jv_details_debit);
					}
					else if (numberFormat($jv_details_credit)!=0)
					{
						$jv_transaction_total[] = numberFormat($jv_details_credit);
					}
				}
				?>
					<td valign='top'>
						<?php 
						foreach ($jv_transaction_total as $jv_transaction_total)
						{
							echo $jv_transaction_total;
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($jv_details_account_code as $jv_details_account_code)
						{
							echo getSubsidiaryLedgerAccountTitleByid($jv_details_account_code);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($jv_details_budget_id as $jv_details_budget_id)
						{
							echo getBudgetDescription($jv_details_budget_id);;
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($jv_details_donor_id as $jv_details_donor_id)
						{
							echo getDonorDescription($jv_details_donor_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($jv_details_component_id as $jv_details_component_id)
						{
							echo getComponentDescription($jv_details_component_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($jv_details_activity_id as $jv_details_activity_id)
						{
							echo getActivityDescription($jv_details_activity_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>																				
					<td valign='top'>
						<?php 
						foreach ($jv_details_other_cost_id as $jv_details_other_cost_id)
						{
							echo getOtherCostDescription($jv_details_other_cost_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($jv_details_staff_id as $jv_details_staff_id)
						{
							echo getStaffDescription($jv_details_staff_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>	
					<td valign='top'>
						<?php 
						foreach ($jv_details_benefits_id as $jv_details_benefits_id)
						{
							echo getBenefitsDescription($jv_details_benefits_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($jv_details_vehicle_id as $jv_details_vehicle_id)
						{
							echo getVehicleDescription($jv_details_vehicle_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>	
					<td valign='top'>
						<?php 
						foreach ($jv_details_equipment_id as $jv_details_equipment_id)
						{
							echo getEquipmentDescription($jv_details_equipment_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>																				
				<td valign='top'><?php echo convertToWords($jv_transaction_total).' '.$jv_currency_name; ?>&nbsp;</td>
				<td valign='top'>&nbsp;</td>
				<td valign='top'><?php echo $jv_prepared_by; ?>&nbsp;</td>
				<td valign='top'><?php echo $jv_certified_by; ?>&nbsp;</td>
				<td valign='top'><?php echo $jv_recommended_by; ?>&nbsp;</td>
				<td valign='top'><?php echo $jv_approved_by; ?>&nbsp;</td>
			  </tr>
        </table>
		<!-- End of Inner Table -->		
		</td>
	</tr>
</table>
<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
	<a href="/workspaceGOP/report.templates/journal.voucher.php?id=<?php echo encrypt($jv_id); ?>&jv_number=<?php echo md5($jv_number); ?>"><img src="/workspaceGOP/images/save.png" border="0" title="Export to word"></a>
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: journal.voucher.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!");
		window.close();
	</script>
	<?php
}
?>