<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$rec_id = $_GET['id'];
$rec_cv_number = $_GET['cv_number'];
$display_msg = 0;
$rec_id = decrypt($rec_id);
$rec_cv_number = decrypt($rec_cv_number);

$date_time_approved = date("Y-m-d H:i:s");
$current_year = date("Y");

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE				
				cv_id,
				cv_number,
				cv_type,
				cv_currency_code,
				cv_date,
				cv_payee,
				cv_description,
				cv_certified_by,
				cv_certified_by_date,
				cv_recommended_by,
				cv_recommended_by_date,
				cv_approved_by,
				cv_approved_by_date,
				cv_cancelled_by,
				cv_cancelled_by_date
		FROM 	tbl_cash_voucher
		WHERE	cv_id = '$rec_id'
		AND		cv_number = '$rec_cv_number'";
$rs = mysql_query($sql) or die("Error in sql1 in module: cash.voucher.certify.php ".$sql." ".mysql_error());
$total_rows = mysql_num_rows($rs);	
if($total_rows == 0)
{
	insertEventLog($user_id,"User trying to manipulate the page: cash.voucher.certify.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
		window.location = '/workspaceGOP/index.php?msg=4'
	</script>
	<?php
}
else
{
	$rows = mysql_fetch_array($rs);
	$record_id = $rows["cv_id"];
	$cv_number = $rows["cv_number"];
	$cv_type = $rows["cv_type"];
	$cv_currency_code = $rows["cv_currency_code"];
	$cv_date = $rows["cv_date"];
	$cv_payee = $rows["cv_payee"];
	$cv_description = $rows["cv_description"];
	$cv_prepared_by = $rows["cv_prepared_by"];
	$cv_certified_by = $rows["cv_certified_by"];
	$cv_certified_by_date = $rows["cv_certified_by_date"];
	$cv_recommended_by =$rows["cv_recommended_by"];
	$cv_recommended_by_date = $rows["cv_recommended_by_date"];
	$cv_approved_by = $rows["cv_approved_by"];
	$cv_approved_by_date =$rows ["cv_approved_by_date"];
	$cv_cancelled_by = $rows["cv_cancelled_by"];
	$cv_cancelled_by_date = $rows["cv_cancelled_by_date"];	
	
	$cv_payee = getPayeeById($cv_payee);
	$cv_description = stripslashes($cv_description);
	$gl_currency_code = $cv_currency_code;
	$cv_currency_name = getConfigurationDescriptionById($cv_currency_code);
	$cv_currency_code = getConfigurationValueById($cv_currency_code);
	
	
	if ($_POST['Submit'] == 'Update Record')
	{
		$msg = "";

		$gl_report_type = "CV";
		$record_id = $_POST['record_id'];
		$cv_approved_by = $_POST['cv_approved_by'];
		$cv_cancelled_by = $_POST['cv_cancelled_by'];
		
		if ($msg=="" && $cv_cancelled_by == 1)
		{
			$sql = "UPDATE	tbl_cash_voucher
					SET		cv_cancelled_by = '$user_id',
							cv_cancelled_by_date = '$date_time_approved'
					WHERE	cv_id = '$record_id'";
			mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
			
			$sql = "SELECT	SQL_BUFFER_RESULT
							SQL_CACHE
							cv.cv_date AS cv_date,
							cv.cv_number AS cv_number,
							cv.cv_currency_code AS cv_currency_code,
							cv.cv_description AS cv_description,
							cv_details.cv_details_account_code AS cv_account_code,
							cv_details.cv_details_debit AS cv_debit,
							cv_details.cv_details_credit AS cv_credit,
							cv_details.cv_details_budget_id AS cv_budget_id,
							cv_details.cv_details_donor_id AS cv_donor_id,
							cv_details.cv_details_component_id AS cv_component_id,
							cv_details.cv_details_activity_id AS cv_activity_id,
							cv_details.cv_details_other_cost_id AS cv_other_cost,
							cv_details.cv_details_staff_id AS cv_staff_id,
							cv_details.cv_details_benefits_id AS cv_benefits_id,
							cv_details.cv_details_vehicle_id AS cv_vehicle_id,
							cv_details.cv_details_equipment_id AS cv_equipment_id,
							cv_details.cv_details_item_id AS cv_item_id							
					FROM	tbl_cash_voucher AS cv,
							tbl_cash_voucher_details AS cv_details
					WHERE 	cv.cv_type = '$gl_report_type'
					AND		cv.cv_id = '$record_id'
					AND		cv_details.cv_details_reference_number = '$record_id'";
			$rs = mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());	
			while($rows=mysql_fetch_array($rs))
			{
				$cv_date = $rows["cv_date"];
				$cv_number = $rows["cv_number"];
				$cv_currency_code = $rows["cv_currency_code"];
				$cv_description = $rows["cv_description"];
				$cv_account_code = $rows["cv_account_code"];
				$cv_debit = $rows["cv_debit"];
				$cv_credit = $rows["cv_credit"];
				$cv_budget_id = $rows["cv_budget_id"];
				$cv_donor_id = $rows["cv_donor_id"];
				$cv_component_id = $rows["cv_component_id"];
				$cv_activity_id = $rows["cv_activity_id"];
				$cv_other_cost = $rows["cv_other_cost"];
				$cv_staff_id = $rows["cv_staff_id"];
				$cv_benefits_id = $rows["cv_benefits_id"];
				$cv_vehicle_id = $rows["cv_vehicle_id"];
				$cv_equipment_id = $rows["cv_equipment_id"];
				$cv_item_id = $rows["cv_item_id"];
				
				$cv_description = stripslashes($cv_description);
				$cv_description = addslashes($cv_description);				
				
				$sql = "SELECT 	SQL_BUFFER_RESULT
								SQL_CACHE
								sl_details_id,
								sl_details_year,
								sl_details_ending_balance
						FROM	tbl_subsidiary_ledger_details
						WHERE	sl_details_reference_number = '$cv_account_code'
						AND		sl_details_year = '$current_year'
						AND		sl_details_currency_code = '$cv_currency_code'";
				$rs1 = mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());	
				$rows1 = mysql_fetch_array($rs1);
				$sl_details_id = $rows1["sl_details_id"];
				$sl_details_year = $rows1["sl_details_year"];
				$sl_ending_balance = $rows1["sl_details_ending_balance"];

				if ($cv_debit != "0.000000")
				{
					$ending_balance = $cv_debit - $sl_ending_balance;
				}
				if ($cv_credit != "0.000000")
				{
					$ending_balance = $sl_ending_balance + $cv_credit;
				}

				$sql = "UPDATE	tbl_subsidiary_ledger_details
						SET		sl_details_ending_balance = '$ending_balance'
						WHERE	sl_details_id = '$sl_details_id'";		
				mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());
				insertEventLog($user_id,$sql);	
				
				$sql = "INSERT INTO tbl_general_ledger
									(
									gl_date,
									gl_report_type,
									gl_report_number,
									gl_reference_number,
									gl_description,
									gl_account_code,
									gl_currency_code,
									gl_debit,
									gl_credit,
									gl_budget_id,
									gl_donor_id,
									gl_component_id,
									gl_activity_id,
									gl_other_cost_id,
									gl_staff_id,
									gl_benefits_id,
									gl_vehicle_id,
									gl_equipment_id,	
									gl_item_id,
									gl_date_time_inserted
									)
						VALUES		(
									'".trim($cv_date)."',
									'".trim($gl_report_type)."',
									'".trim($cv_number)."',
									'".trim($record_id)."',
									'".trim($cv_description)."',
									'".trim($cv_account_code)."',
									'".trim($cv_currency_code)."',
									'".trim($cv_credit)."',
									'".trim($cv_debit)."',
									'".trim($cv_budget_id)."',
									'".trim($cv_donor_id)."',
									'".trim($cv_component_id)."',
									'".trim($cv_activity_id)."',
									'".trim($cv_other_cost)."',
									'".trim($cv_staff_id)."',
									'".trim($cv_benefits_id)."',
									'".trim($cv_vehicle_id)."',
									'".trim($cv_equipment_id)."',
									'".trim($cv_item_id)."',									
									'".trim($date_time_approved)."'								
									)";
				mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());	
				insertEventLog($user_id,$sql);						
			}			
			
		}
		elseif ($msg=="" && $cv_cancelled_by == 0)
		{
			$sql = "UPDATE	tbl_cash_voucher
					SET		cv_cancelled_by = '0',
							cv_cancelled_by_date = '0000-00-00 00:00:00'
					WHERE	cv_id = '$record_id'";
			mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
			
			$sql = "SELECT	SQL_BUFFER_RESULT
							SQL_CACHE
							cv.cv_date AS cv_date,

							cv.cv_number AS cv_number,
							cv.cv_currency_code AS cv_currency_code,
							cv.cv_description AS cv_description,
							cv_details.cv_details_account_code AS cv_account_code,
							cv_details.cv_details_debit AS cv_debit,
							cv_details.cv_details_credit AS cv_credit,
							cv_details.cv_details_budget_id AS cv_budget_id,
							cv_details.cv_details_donor_id AS cv_donor_id,
							cv_details.cv_details_component_id AS cv_component_id,
							cv_details.cv_details_activity_id AS cv_activity_id,
							cv_details.cv_details_other_cost_id AS cv_other_cost,
							cv_details.cv_details_staff_id AS cv_staff_id,
							cv_details.cv_details_benefits_id AS cv_benefits_id,
							cv_details.cv_details_vehicle_id AS cv_vehicle_id,
							cv_details.cv_details_equipment_id AS cv_equipment_id,
							cv_details.cv_details_item_id AS cv_item_id							
					FROM	tbl_cash_voucher AS cv,
							tbl_cash_voucher_details AS cv_details
					WHERE 	cv.cv_type = '$gl_report_type'
					AND		cv.cv_id = '$record_id'
					AND		cv_details.cv_details_reference_number = '$record_id'";
			$rs = mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());	
			while($rows=mysql_fetch_array($rs))
			{
				$cv_date = $rows["cv_date"];
				$cv_number = $rows["cv_number"];
				$cv_currency_code = $rows["cv_currency_code"];
				$cv_description = $rows["cv_description"];
				$cv_account_code = $rows["cv_account_code"];
				$cv_debit = $rows["cv_debit"];
				$cv_credit = $rows["cv_credit"];
				$cv_budget_id = $rows["cv_budget_id"];
				$cv_donor_id = $rows["cv_donor_id"];
				$cv_component_id = $rows["cv_component_id"];
				$cv_activity_id = $rows["cv_activity_id"];
				$cv_other_cost = $rows["cv_other_cost"];
				$cv_staff_id = $rows["cv_staff_id"];
				$cv_benefits_id = $rows["cv_benefits_id"];
				$cv_vehicle_id = $rows["cv_vehicle_id"];
				$cv_equipment_id = $rows["cv_equipment_id"];
				$cv_equipment_id = $rows["cv_item_id"];
				
				$sql = "SELECT 	SQL_BUFFER_RESULT
								SQL_CACHE
								sl_details_id,
								sl_details_year,
								sl_details_ending_balance
						FROM	tbl_subsidiary_ledger_details
						WHERE	sl_details_reference_number = '$cv_account_code'
						AND		sl_details_year = '$current_year'
						AND		sl_details_currency_code = '$cv_currency_code'";
				$rs1 = mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());	
				$rows1 = mysql_fetch_array($rs1);
				$sl_details_id = $rows1["sl_details_id"];
				$sl_details_year = $rows1["sl_details_year"];
				$sl_ending_balance = $rows1["sl_details_ending_balance"];

				if ($cv_debit != "0.000000")
				{
					$ending_balance = $cv_debit + $sl_ending_balance;
				}
				if ($cv_credit != "0.000000")
				{
					$ending_balance = $sl_ending_balance - $cv_credit;
				}

				$sql = "UPDATE	tbl_subsidiary_ledger_details
						SET		sl_details_ending_balance = '$ending_balance'
						WHERE	sl_details_id = '$sl_details_id'";			
				mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());
				insertEventLog($user_id,$sql);	
				
				$sql = "INSERT INTO tbl_general_ledger
									(
									gl_date,
									gl_report_type,
									gl_report_number,
									gl_reference_number,
									gl_description,
									gl_account_code,
									gl_currency_code,
									gl_debit,
									gl_credit,
									gl_budget_id,
									gl_donor_id,
									gl_component_id,
									gl_activity_id,
									gl_other_cost_id,
									gl_staff_id,
									gl_benefits_id,
									gl_vehicle_id,
									gl_equipment_id,	
									gl_item_id,								
									gl_date_time_inserted
									)
						VALUES		(
									'".trim($cv_date)."',
									'".trim($gl_report_type)."',
									'".trim($cv_number)."',
									'".trim($record_id)."',
									'".trim($cv_description)."',
									'".trim($cv_account_code)."',
									'".trim($cv_currency_code)."',
									'".trim($cv_debit)."',
									'".trim($cv_credit)."',
									'".trim($cv_budget_id)."',
									'".trim($cv_donor_id)."',
									'".trim($cv_component_id)."',
									'".trim($cv_activity_id)."',
									'".trim($cv_other_cost)."',
									'".trim($cv_staff_id)."',
									'".trim($cv_benefits_id)."',
									'".trim($cv_vehicle_id)."',
									'".trim($cv_equipment_id)."',
									'".trim($cv_item_id)."',									
									'".trim($date_time_approved)."'								
									)";
				mysql_query($sql) or die("Error in $sql in module: cash.voucher.approve.php ".$sql." ".mysql_error());	
				insertEventLog($user_id,$sql);						
			}			
		}		
		if ($msg == "")
		{
			header("location: /workspaceGOP/page.transactions/cash.voucher.list.php?msg=1");
		}
	}
	
	?>
	<?php include("./../includes/menu.php"); ?>
	<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
		<tr>
			<td>
				<form name="cash_voucher" method="post">
					<table id="rounded-add-entries" align="center">
						<thead>
							<tr>
								<th scope="col" colspan="2" class="rounded-header"><div class="main" align="left"><strong>Cash Voucher [Cancel Voucher]</strong></div></th>
								<th scope="col" colspan="2" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td class="rounded-foot-left" colspan="2" align="left"><input type="submit" name="Submit" value="Update Record" class="formbutton"></td>
								<td class="rounded-foot-right" colspan="2" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
							</tr>
						</tfoot>
						<tbody>
						<?php
						if ($display_msg==1)
						{
						?>
							<tr>
								 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
							</tr>
							<?php
						}
						?>
						  <tr>
							<td width="10%" scope="col"><b>CV Number</b></td>
							<td width="40%" scope="col"><?php echo $cv_number; ?></td>
							<td width="10%" scope="col"><b>Report Type</b></td>
							<td width="40%" scope="col"><?php echo $cv_type; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Currency</b></td>
							<td scope="col"><?php echo $cv_currency_name; ?></td>
							<td scope="col"><b>Date</b></td>
							<td scope="col"><?php echo $cv_date; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Payee</b></td>
							<td colspan="3" scope="col"><? echo $cv_payee; ?></td>
						  </tr>
						  <tr>
							<td valign="top" scope="col"><b>Particulars/Description</b></td>
							<td colspan="3" valign="top" scope="col"><?php echo $cv_description; ?></td>
						  </tr>
						  <tr>
							<td colspan="4" valign="top" scope="col">
	
								<table id="rounded-add-entries" align="center">
								  <thead>
									<tr>
									  <th scope="col" class="rounded-header" align="center"><b>Account Code</b></th>
									  <th scope="col" align="center"><b>Account Title</b></th>
									   <th scope="col" align="center"><b>Currency</b></th>
									  <th scope="col" align="center"><b>Debit</b></th>
									  <th scope="col" align="center"><b>Credit</b></th>
									  <th scope="col" align="center"><b>Budget ID</b></th>
									  <th scope="col" align="center"><b>Donor ID</b></th>
									  <th scope="col" align="center"><b>Component ID</b></th>
									  <th scope="col" align="center"><b>Activity ID</b></th>
									  <th scope="col" align="center"><b>Other Cost/Services</b></th>
									  <th scope="col" align="center"><b>Staff ID</b></th>
									  <th scope="col" align="center"><b>Benefits ID</b></th>
									  <th scope="col" align="center"><b>Vehicles ID</b></th>
									  <th scope="col" align="center"><b>Equipment ID</b></th>
                                      <th scope="col" class="rounded-q4" align="center"><b>Item ID</b></th>
									</tr>
								  </thead>
								  <tbody>
									<?php
									$sql = "	SELECT 	SQL_BUFFER_RESULT
														SQL_CACHE
														cv_details_id,
														cv_details_account_code,
														cv_details_reference_number,
														cv_details_debit,
														cv_details_credit,
														cv_details_budget_id,
														cv_details_donor_id,
														cv_details_component_id,
														cv_details_activity_id,
														cv_details_other_cost_id,
														cv_details_staff_id,
														cv_details_benefits_id,
														cv_details_vehicle_id,
														cv_details_equipment_id,
														cv_details_item_id
												FROM 	tbl_cash_voucher_details
												WHERE	cv_details_reference_number = '$record_id'";
									$rs = mysql_query($sql) or die("Error in sql2 in module: cash.voucher.details.add.php ".$sql." ".mysql_error());
									$total_rows = mysql_num_rows($rs);	
									if($total_rows == 0)
									{
										?>
										<tr>
										  <td colspan="14"><div class="redlabel" align="left">No records found!</div></td>
										</tr>
										<?php
									}
									else
									{
										for($row_number=0;$row_number<$total_rows;$row_number++)
										{
											$rows = mysql_fetch_array($rs);
											$cv_details_id = $rows["cv_details_id"];
											$cv_details_reference_number = $rows["cv_details_reference_number"];
											$cv_details_account_code = $rows["cv_details_account_code"];
											$cv_details_debit = $rows["cv_details_debit"];
											$cv_details_credit = $rows["cv_details_credit"];
											$cv_details_budget_id = $rows["cv_details_budget_id"];
											$cv_details_donor_id = $rows["cv_details_donor_id"];
											$cv_details_component_id = $rows["cv_details_component_id"];
											$cv_details_activity_id = $rows["cv_details_activity_id"];
											$cv_details_other_cost_id = $rows["cv_details_other_cost_id"];
											$cv_details_staff_id = $rows["cv_details_staff_id"];
											$cv_details_benefits_id = $rows["cv_details_benefits_id"];
											$cv_details_vehicle_id = $rows["cv_details_vehicle_id"];
											$cv_details_equipment_id = $rows["cv_details_equipment_id"];
											$cv_details_item_id = $rows["cv_details_item_id"];
											
											$gl_account_code = $cv_details_account_code;
											$cv_details_account_title = getSubsidiaryLedgerAccountTitleByid($cv_details_account_code);
											$cv_details_account_code = getSubsidiaryLedgerAccountCodeByid($cv_details_account_code);
											
											$cv_details_budget_id_name  = getBudgetName($cv_details_budget_id);
											$cv_details_donor_id_name  = getDonorName($cv_details_donor_id);
											$cv_details_component_id_name  = getComponentName($cv_details_component_id);
											$cv_details_activity_id_name = getActivityName($cv_details_activity_id);
											$cv_details_other_cost_id_name  = getOtherCostName($cv_details_other_cost_id);
											$cv_details_staff_id_name  = getStaffName($cv_details_staff_id);
											$cv_details_benefits_id_name  = getBenefitsName($cv_details_benefits_id);
											$cv_details_vehicle_id_name  = getVehicleName($cv_details_vehicle_id);
											$cv_details_equipment_id_name  = getEquipmentName($cv_details_equipment_id);
											$cv_details_item_id_name  = getEquipmentName($cv_details_item_id);
											
											$arr_debit[] = $cv_details_debit;
											$arr_credit[] = $cv_details_credit;
											?>
									<tr>
									  <td><?php echo $cv_details_account_code; ?></td>
									  <td><?php echo $cv_details_account_title; ?></td>
									  <td><?php echo $cv_currency_code; ?></td>
									  <td align="right"><?php echo numberFormat($cv_details_debit); ?></td>
									  <td align="right"><?php echo numberFormat($cv_details_credit); ?></td>
									  <td><?php echo $cv_details_budget_id_name; ?></td>
									  <td><?php echo $cv_details_donor_id_name; ?></td>
									  <td><?php echo $cv_details_component_id_name; ?></td>
									  <td><?php echo $cv_details_activity_id_name; ?></td>
									  <td><?php echo $cv_details_other_cost_id_name; ?></td>
									  <td><?php echo $cv_details_staff_id_name; ?></td>
									  <td><?php echo $cv_details_benefits_id_name; ?></td>
									  <td><?php echo $cv_details_vehicle_id_name; ?></td>
									  <td><?php echo $cv_details_equipment_id_name; ?></td>
                                      <td><?php echo $cv_details_item_id_name; ?></td>
									  <?php 
										} 
										?>
									</tr>
								  <?php 
									} 
									if ($arr_credit != 0 && $arr_debit != 0)
									{
										$total_debit = array_sum($arr_debit);
										$total_credit = array_sum($arr_credit);
									}
									?>
								  <tfoot>
									<tr>
									  <td class="rounded-foot-left" colspan="3" align="right"><b>Total :</b></td>
									  <td align="right"><?php echo numberFormat($total_debit); ?> </td>
									  <td align="right"><?php echo numberFormat($total_credit);  ?> </td>
									  <td class="rounded-foot-right" colspan="12" align="right">&nbsp;</td>
									</tr>
								  </tfoot>								
								</table>
	
							</td>
						  </tr>								  						  
						  <tr>
							<td colspan="4" valign="top" scope="col">
								<input type="hidden" name="cv_approved_by" value="<?php echo $cv_approved_by; ?>">
							  	<input name="cv_cancelled_by" type="checkbox" id="cv_cancelled_by" value="1" <?php if ($cv_cancelled_by!=0) { echo "CHECKED";} ?>/>
								* Save Changes							</td>
						  </tr>					  					  					  
						</tbody>
					</table>
				</form>
			</td>
		</tr>
	</table>
	<?php include("./../includes/footer.main.php"); ?>
<?php
}
?>
