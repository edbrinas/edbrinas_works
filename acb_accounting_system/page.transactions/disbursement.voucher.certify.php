<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$rec_id = $_GET['id'];
$rec_dv_number = $_GET['dv_number'];
$display_msg = 0;
$rec_id = decrypt($rec_id);
$rec_dv_number = decrypt($rec_dv_number);

$date_time_certify = date("Y-m-d H:i:s");

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE				
				dv_id,
				dv_number,
				dv_type,
				dv_currency_code,
				dv_date,
				dv_payee,
				dv_description,
				dv_obligated_by,
				dv_obligated_by_date,
				dv_certified_by,
				dv_certified_by_date,
				dv_recommended_by,
				dv_recommended_by_date,
				dv_approved_by,
				dv_approved_by_date
		FROM 	tbl_disbursement_voucher
		WHERE	dv_id = '$rec_id'
		AND		dv_number = '$rec_dv_number'";
$rs = mysql_query($sql) or die("Error in sql1 in module: disbursement.voucher.certify.php ".$sql." ".mysql_error());
$total_rows = mysql_num_rows($rs);	
if($total_rows == 0)
{
	insertEventLog($user_id,"User trying to manipulate the page: disbursement.voucher.certify.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
		window.location = '/workspaceGOP/index.php?msg=4'
	</script>
	<?php
}
else
{
	$rows = mysql_fetch_array($rs);
	$record_id = $rows["dv_id"];
	$dv_number = $rows["dv_number"];
	$dv_type = $rows["dv_type"];
	$dv_currency_code = $rows["dv_currency_code"];
	$dv_date = $rows["dv_date"];
	$dv_payee = $rows["dv_payee"];
	$dv_description = $rows["dv_description"];
	$dv_prepared_by = $rows["dv_prepared_by"];
	$dv_obligated_by = $rows["dv_obligated_by"];
	$dv_obligated_by_date = $rows["dv_obligated_by_date"];
	$dv_certified_by = $rows["dv_certified_by"];
	$dv_certified_by_date = $rows["dv_certified_by_date"];
	$dv_recommended_by =$rows["dv_recommended_by"];
	$dv_recommended_by_date = $rows["dv_recommended_by_date"];
	$dv_approved_by = $rows["dv_approved_by"];
	$dv_approved_by_date =$rows ["dv_approved_by_date"];
	
	$dv_payee = getPayeeById($dv_payee);
	$dv_description = stripslashes($dv_description);
	$dv_currency_name = getConfigurationDescriptionById($dv_currency_code);
	$dv_currency_code = getConfigurationValueById($dv_currency_code);
	
	
	if ($_POST['Submit'] == 'Update Record')
	{
		$msg = "";
		
		$record_id = $_POST['record_id'];
		$dv_certified_by = $_POST['dv_certified_by'];
		$dv_approved_by = $_POST['dv_approved_by'];
		$dv_recommended_by = $_POST['dv_recommended_by'];
		$dv_obligated_by = $_POST['dv_obligated_by'];
		
		if ($dv_obligated_by==0) { $dv_certified_by=0; $display_msg = 1; $msg .= "You cannot certify this record because this has not been obligated."; $msg .= "<br>"; }
		
		if ($dv_recommended_by!=0) { $dv_certified_by=0; $display_msg = 1; $msg .= "You cannot certify/un-certify this record because this has been recommended."; $msg .= "<br>"; }
		if ($dv_approved_by!=0) { $dv_certified_by=0; $display_msg = 1; $msg .= "You cannot certify/un-certify this record because this has been approved."; $msg .= "<br>"; }
		
		if ($msg=="" && $dv_certified_by == 1)
		{
			$sql = "UPDATE	tbl_disbursement_voucher
					SET		dv_certified_by = '$user_id',
							dv_certified_by_date = '$date_time_certify'
					WHERE	dv_id = '$record_id'";
			mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.certify.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
		}
		if ($msg=="" && $dv_certified_by == 0)
		{
			$sql = "UPDATE	tbl_disbursement_voucher
					SET		dv_certified_by = '0',
							dv_certified_by_date = '0000-00-00 00:00:00'
					WHERE	dv_id = '$record_id'";
			mysql_query($sql) or die("Error in $sql in module: disbursement.voucher.recommend.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
		}	
		if ($msg == "")
		{
			header("location: /workspaceGOP/page.transactions/disbursement.voucher.list.php?msg=1");
		}
	}
	
	?>
	<?php include("./../includes/menu.php"); ?>
	<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
		<tr>
			<td>
				<form name="disbursement_voucher" method="post">
					<table id="rounded-add-entries" align="center">
						<thead>
							<tr>
								<th scope="col" colspan="2" class="rounded-header"><div class="main" align="left"><strong>Disbursement Voucher [Certify]</strong></div></th>
								<th scope="col" colspan="2" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td class="rounded-foot-left" colspan="2" align="left"><input type="submit" name="Submit" value="Update Record" class="formbutton"></td>
								<td class="rounded-foot-right" colspan="2" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>" class="formbutton"></td>
							</tr>
						</tfoot>
						<tbody>
						<?php
						if ($display_msg==1)
						{
						?>
							<tr>
								 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
							</tr>
							<?php
						}
						?>
						  <tr>
							<td width="10%" scope="col"><b>DV Number</b></td>
							<td width="40%" scope="col"><?php echo $dv_number; ?>
								<input name="dv_number" type="hidden" class="formbutton" id="dv_number" value="<?php echo $dv_number; ?>" />
							</td>
							<td width="10%" scope="col"><b>Report Type</b></td>
							<td width="40%" scope="col"><?php echo $dv_type; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Currency</b></td>
							<td scope="col"><?php echo $dv_currency_name; ?></td>
							<td scope="col"><b>Date</b></td>
							<td scope="col"><?php echo $dv_date; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Payee</b></td>
							<td colspan="3" scope="col"><? echo $dv_payee; ?></td>
						  </tr>
						  <tr>
							<td valign="top" scope="col"><b>Particulars/Description</b></td>
							<td colspan="3" valign="top" scope="col"><?php echo $dv_description; ?></td>
						  </tr>
						  <tr>
							<td colspan="4" valign="top" scope="col">
	
								<table id="rounded-add-entries" align="center">
								  <thead>
									<tr>
									  <th scope="col" class="rounded-header" align="center"><b>Account Code</b></th>
									  <th scope="col" align="center"><b>Account Title</b></th>
									   <th scope="col" align="center"><b>Currency</b></th>
									  <th scope="col" align="center"><b>Debit</b></th>
									  <th scope="col" align="center"><b>Credit</b></th>
									  <th scope="col" align="center"><b>Budget ID</b></th>
									  <th scope="col" align="center"><b>Donor ID</b></th>
									  <th scope="col" align="center"><b>Component ID</b></th>
									  <th scope="col" align="center"><b>Activity ID</b></th>
									  <th scope="col" align="center"><b>Other Cost/Services</b></th>
									  <th scope="col" align="center"><b>Staff ID</b></th>
									  <th scope="col" align="center"><b>Benefits ID</b></th>
									  <th scope="col" align="center"><b>Vehicles ID</b></th>
									  <th scope="col" class="rounded-q4" align="center"><b>Equipment ID</b></th>
									</tr>
								  </thead>
								  <tbody>
									<?php
									$sql = "	SELECT 	SQL_BUFFER_RESULT
														SQL_CACHE
														dv_details_id,
														dv_details_account_code,
														dv_details_reference_number,
														dv_details_debit,
														dv_details_credit,
														dv_details_budget_id,
														dv_details_donor_id,
														dv_details_component_id,
														dv_details_activity_id,
														dv_details_other_cost_id,
														dv_details_staff_id,
														dv_details_benefits_id,
														dv_details_vehicle_id,
														dv_details_equipment_id
												FROM 	tbl_disbursement_voucher_details
												WHERE	dv_details_reference_number = '$record_id'";
									$rs = mysql_query($sql) or die("Error in sql2 in module: journal.voucher.details.add.php ".$sql." ".mysql_error());
									$total_rows = mysql_num_rows($rs);	
									if($total_rows == 0)
									{
										?>
										<tr>
										  <td colspan="14"><div class="redlabel" align="left">No records found!</div></td>
										</tr>
										<?php
									}
									else
									{
										for($row_number=0;$row_number<$total_rows;$row_number++)
										{
											$rows = mysql_fetch_array($rs);
											$dv_details_id = $rows["dv_details_id"];
											$dv_details_reference_number = $rows["dv_details_reference_number"];
											$dv_details_account_code = $rows["dv_details_account_code"];
											$dv_details_debit = $rows["dv_details_debit"];
											$dv_details_credit = $rows["dv_details_credit"];
											$dv_details_budget_id = $rows["dv_details_budget_id"];
											$dv_details_donor_id = $rows["dv_details_donor_id"];
											$dv_details_component_id = $rows["dv_details_component_id"];
											$dv_details_activity_id = $rows["dv_details_activity_id"];
											$dv_details_other_cost_id = $rows["dv_details_other_cost_id"];
											$dv_details_staff_id = $rows["dv_details_staff_id"];
											$dv_details_benefits_id = $rows["dv_details_benefits_id"];
											$dv_details_vehicle_id = $rows["dv_details_vehicle_id"];
											$dv_details_equipment_id = $rows["dv_details_equipment_id"];
											
											
											$dv_details_account_title = getSubsidiaryLedgerAccountTitleByid($dv_details_account_code);
											$dv_details_account_code = getSubsidiaryLedgerAccountCodeByid($dv_details_account_code);
											
											$dv_details_budget_id  = getBudgetName($dv_details_budget_id);
											$dv_details_donor_id  = getDonorName($dv_details_donor_id);
											$dv_details_component_id  = getComponentName($dv_details_component_id);
											$dv_details_activity_id = getActivityName($dv_details_activity_id);
											$dv_details_other_cost_id  = getOtherCostName($dv_details_other_cost_id);
											$dv_details_staff_id  = getStaffName($dv_details_staff_id);
											$dv_details_benefits_id  = getBenefitsName($dv_details_benefits_id);
											$dv_details_vehicle_id  = getVehicleName($dv_details_vehicle_id);
											$dv_details_equipment_id  = getEquipmentName($dv_details_equipment_id);
											
											$arr_debit[] = $dv_details_debit;
											$arr_credit[] = $dv_details_credit;
										?>
									<tr>
									  <td><?php echo $dv_details_account_code; ?></td>
									  <td><?php echo $dv_details_account_title; ?></td>
									  <td><?php echo $dv_currency_code; ?></td>
									  <td align="right"><?php echo numberFormat($dv_details_debit); ?></td>
									  <td align="right"><?php echo numberFormat($dv_details_credit); ?></td>
									  <td><?php echo $dv_details_budget_id; ?></td>
									  <td><?php echo $dv_details_donor_id; ?></td>
									  <td><?php echo $dv_details_component_id; ?></td>
									  <td><?php echo $dv_details_activity_id; ?></td>
									  <td><?php echo $dv_details_other_cost_id; ?></td>
									  <td><?php echo $dv_details_staff_id; ?></td>
									  <td><?php echo $dv_details_benefits_id; ?></td>
									  <td><?php echo $dv_details_vehicle_id; ?></td>
									  <td><?php echo $dv_details_equipment_id; ?></td>
									  <?php 
										} 
										?>
									</tr>
								  </tbody>
								  <?php 
									} 
									if ($arr_credit != 0 && $arr_debit != 0)
									{
										$total_debit = array_sum($arr_debit);
										$total_credit = array_sum($arr_credit);
									}
									?>
								  <tfoot>
									<tr>
									  <td class="rounded-foot-left" colspan="3" align="right"><b>Total :</b></td>
									  <td align="right"><?php echo numberFormat($total_debit); ?> </td>
									  <td align="right"><?php echo numberFormat($total_credit);  ?> </td>
									  <td class="rounded-foot-right" colspan="11" align="right">&nbsp;</td>
									</tr>
								  </tfoot>								
								</table>
	
							</td>
						  </tr>							  
						  <tr>
							<td colspan="4" valign="top" scope="col">
								<input type="hidden" name="dv_obligated_by" value="<?php echo $dv_obligated_by; ?>">
                                <input type="hidden" name="dv_approved_by" value="<?php echo $dv_approved_by; ?>">
								<input type="hidden" name="dv_recommended_by" value="<?php echo $dv_recommended_by; ?>">
							  	<input name="dv_certified_by" type="checkbox" id="dv_certified_by" value="1" <?php if ($dv_certified_by!=0) { echo "CHECKED";} ?>/>
								* Certify this record.							</td>
						  </tr>					  					  					  
			
						</tbody>
					</table>
				</form>
			</td>
		</tr>
	</table>
	<?php include("./../includes/footer.main.php"); ?>
<?php
}
?>