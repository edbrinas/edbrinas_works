<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$rec_id = $_GET['id'];
$rec_jv_number = $_GET['jv_number'];
$display_msg = 0;
$rec_id = decrypt($rec_id);
$rec_jv_number = decrypt($rec_jv_number);

$date_time_certify = date("Y-m-d H:i:s");

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE				
				jv_id,
				jv_number,
				jv_type,
				jv_currency_code,
				jv_date,
				jv_payee,
				jv_description,
				jv_certified_by,
				jv_certified_by_date,
				jv_recommended_by,
				jv_recommended_by_date,
				jv_approved_by,
				jv_approved_by_date
		FROM 	tbl_journal_voucher
		WHERE	jv_id = '$rec_id'
		AND		jv_number = '$rec_jv_number'";
$rs = mysql_query($sql) or die("Error in sql1 in module: journal.voucher.certify.php ".$sql." ".mysql_error());
$total_rows = mysql_num_rows($rs);	
if($total_rows == 0)
{
	insertEventLog($user_id,"User trying to manipulate the page: journal.voucher.certify.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
		window.location = '/workspaceGOP/index.php?msg=4'
	</script>
	<?php
}
else
{
	$rows = mysql_fetch_array($rs);
	$record_id = $rows["jv_id"];
	$jv_number = $rows["jv_number"];
	$jv_type = $rows["jv_type"];
	$jv_currency_code = $rows["jv_currency_code"];
	$jv_date = $rows["jv_date"];
	$jv_payee = $rows["jv_payee"];
	$jv_description = $rows["jv_description"];
	$jv_prepared_by = $rows["jv_prepared_by"];
	$jv_certified_by = $rows["jv_certified_by"];
	$jv_certified_by_date = $rows["jv_certified_by_date"];
	$jv_recommended_by =$rows["jv_recommended_by"];
	$jv_recommended_by_date = $rows["jv_recommended_by_date"];
	$jv_approved_by = $rows["jv_approved_by"];
	$jv_approved_by_date =$rows ["jv_approved_by_date"];
	
	$jv_payee = getPayeeById($jv_payee);
	$jv_description = stripslashes($jv_description);
	$jv_currency_name = getConfigurationDescriptionById($jv_currency_code);
	$jv_currency_code = getConfigurationValueById($jv_currency_code);
	
	
	if ($_POST['Submit'] == 'Update Record')
	{
		$msg = "";
		
		$record_id = $_POST['record_id'];
		$jv_certified_by = $_POST['jv_certified_by'];
		$jv_approved_by = $_POST['jv_approved_by'];
		$jv_recommended_by = $_POST['jv_recommended_by'];
		
		if ($jv_recommended_by!=0) { $jv_certified_by=0; $display_msg = 1; $msg .= "You cannot certify/un-certify this record because this has been recommended."; $msg .= "<br>"; }
		if ($jv_approved_by!=0) { $jv_certified_by=0; $display_msg = 1; $msg .= "You cannot certify/un-certify this record because this has been approved."; $msg .= "<br>"; }
		
		if ($msg=="" && $jv_certified_by == 1)
		{
			$sql = "UPDATE	tbl_journal_voucher
					SET		jv_certified_by = '$user_id',
							jv_certified_by_date = '$date_time_certify'
					WHERE	jv_id = '$record_id'";
			mysql_query($sql) or die("Error in $sql in module: journal.voucher.certify.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
			#$display_msg = 1;
			#$msg = "Record updated succesfully!";
		}
		if ($msg=="" && $jv_certified_by == 0)
		{
			$sql = "UPDATE	tbl_journal_voucher
					SET		jv_certified_by = '0',
							jv_certified_by_date = '0000-00-00 00:00:00'
					WHERE	jv_id = '$record_id'";
			mysql_query($sql) or die("Error in $sql in module: journal.voucher.recommend.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
			#$display_msg = 1;
			#$msg = "Record updated succesfully!";		
		}	
		if ($msg == "")
		{
			header("location: /workspaceGOP/page.transactions/journal.voucher.list.php?msg=1");
		}
	}
	
	?>
	<?php include("./../includes/menu.php"); ?>
	<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
		<tr>
			<td>
				<form name="journal_voucher" method="post">
					<table id="rounded-add-entries" align="center">
						<thead>
							<tr>
								<th scope="col" colspan="2" class="rounded-header"><div class="main" align="left"><strong>Journal Voucher [Certify]</strong></div></th>
								<th scope="col" colspan="2" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td class="rounded-foot-left" colspan="2" align="left"><input type="submit" name="Submit" value="Update Record" class="formbutton"></td>
								<td class="rounded-foot-right" colspan="2" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>" class="formbutton"></td>
							</tr>
						</tfoot>
						<tbody>
						<?php
						if ($display_msg==1)
						{
						?>
							<tr>
								 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
							</tr>
							<?php
						}
						?>
						  <tr>
							<td width="10%" scope="col"><b>JV Number</b></td>
							<td width="40%" scope="col"><?php echo $jv_number; ?>
								<input name="jv_number" type="hidden" class="formbutton" id="jv_number" value="<?php echo $jv_number; ?>" />
							</td>
							<td width="10%" scope="col"><b>Report Type</b></td>
							<td width="40%" scope="col"><?php echo $jv_type; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Currency</b></td>
							<td scope="col"><?php echo $jv_currency_name; ?></td>
							<td scope="col"><b>Date</b></td>
							<td scope="col"><?php echo $jv_date; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Payee</b></td>
							<td colspan="3" scope="col"><? echo $jv_payee; ?></td>
						  </tr>
						  <tr>
							<td valign="top" scope="col"><b>Particulars/Description</b></td>
							<td colspan="3" valign="top" scope="col"><?php echo $jv_description; ?></td>
						  </tr>
						  <tr>
							<td colspan="4" valign="top" scope="col">
	
								<table id="rounded-add-entries" align="center">
								  <thead>
									<tr>
									  <th scope="col" class="rounded-header" align="center"><b>Account Code</b></th>
									  <th scope="col" align="center"><b>Account Title</b></th>
									   <th scope="col" align="center"><b>Currency</b></th>
									  <th scope="col" align="center"><b>Debit</b></th>
									  <th scope="col" align="center"><b>Credit</b></th>
									  <th scope="col" align="center"><b>Budget ID</b></th>
									  <th scope="col" align="center"><b>Donor ID</b></th>
									  <th scope="col" align="center"><b>Component ID</b></th>
									  <th scope="col" align="center"><b>Activity ID</b></th>
									  <th scope="col" align="center"><b>Other Cost/Services</b></th>
									  <th scope="col" align="center"><b>Staff ID</b></th>
									  <th scope="col" align="center"><b>Benefits ID</b></th>
									  <th scope="col" align="center"><b>Vehicles ID</b></th>
									  <th scope="col" class="rounded-q4" align="center"><b>Equipment ID</b></th>
									</tr>
								  </thead>
								  <tbody>
									<?php
									$sql = "	SELECT 	SQL_BUFFER_RESULT
														SQL_CACHE
														jv_details_id,
														jv_details_account_code,
														jv_details_reference_number,
														jv_details_debit,
														jv_details_credit,
														jv_details_budget_id,
														jv_details_donor_id,
														jv_details_component_id,
														jv_details_activity_id,
														jv_details_other_cost_id,
														jv_details_staff_id,
														jv_details_benefits_id,
														jv_details_vehicle_id,
														jv_details_equipment_id
												FROM 	tbl_journal_voucher_details
												WHERE	jv_details_reference_number = '$record_id'";
									$rs = mysql_query($sql) or die("Error in sql2 in module: journal.voucher.details.add.php ".$sql." ".mysql_error());
									$total_rows = mysql_num_rows($rs);	
									if($total_rows == 0)
									{
										?>
										<tr>
										  <td colspan="14"><div class="redlabel" align="left">No records found!</div></td>
										</tr>
										<?php
									}
									else
									{
										for($row_number=0;$row_number<$total_rows;$row_number++)
										{
											$rows = mysql_fetch_array($rs);
											$jv_details_id = $rows["jv_details_id"];
											$jv_details_reference_number = $rows["jv_details_reference_number"];
											$jv_details_account_code = $rows["jv_details_account_code"];
											$jv_details_debit = $rows["jv_details_debit"];
											$jv_details_credit = $rows["jv_details_credit"];
											$jv_details_budget_id = $rows["jv_details_budget_id"];
											$jv_details_donor_id = $rows["jv_details_donor_id"];
											$jv_details_component_id = $rows["jv_details_component_id"];
											$jv_details_activity_id = $rows["jv_details_activity_id"];
											$jv_details_other_cost_id = $rows["jv_details_other_cost_id"];
											$jv_details_staff_id = $rows["jv_details_staff_id"];
											$jv_details_benefits_id = $rows["jv_details_benefits_id"];
											$jv_details_vehicle_id = $rows["jv_details_vehicle_id"];
											$jv_details_equipment_id = $rows["jv_details_equipment_id"];
											
											
											$jv_details_account_title = getSubsidiaryLedgerAccountTitleByid($jv_details_account_code);
											$jv_details_account_code = getSubsidiaryLedgerAccountCodeByid($jv_details_account_code);
											
											$jv_details_budget_id  = getBudgetName($jv_details_budget_id);
											$jv_details_donor_id  = getDonorName($jv_details_donor_id);
											$jv_details_component_id  = getComponentName($jv_details_component_id);
											$jv_details_activity_id = getActivityName($jv_details_activity_id);
											$jv_details_other_cost_id  = getOtherCostName($jv_details_other_cost_id);
											$jv_details_staff_id  = getStaffName($jv_details_staff_id);
											$jv_details_benefits_id  = getBenefitsName($jv_details_benefits_id);
											$jv_details_vehicle_id  = getVehicleName($jv_details_vehicle_id);
											$jv_details_equipment_id  = getEquipmentName($jv_details_equipment_id);
											
											$arr_debit[] = $jv_details_debit;
											$arr_credit[] = $jv_details_credit;
										?>
									<tr>
									  <td><?php echo $jv_details_account_code; ?></td>
									  <td><?php echo $jv_details_account_title; ?></td>
									  <td><?php echo $jv_currency_code; ?></td>
									  <td align="right"><?php echo numberFormat($jv_details_debit); ?></td>
									  <td align="right"><?php echo numberFormat($jv_details_credit); ?></td>
									  <td><?php echo $jv_details_budget_id; ?></td>
									  <td><?php echo $jv_details_donor_id; ?></td>
									  <td><?php echo $jv_details_component_id; ?></td>
									  <td><?php echo $jv_details_activity_id; ?></td>
									  <td><?php echo $jv_details_other_cost_id; ?></td>
									  <td><?php echo $jv_details_staff_id; ?></td>
									  <td><?php echo $jv_details_benefits_id; ?></td>
									  <td><?php echo $jv_details_vehicle_id; ?></td>
									  <td><?php echo $jv_details_equipment_id; ?></td>
									  <?php 
										} 
										?>
									</tr>
								  </tbody>
								  <?php 
									} 
									if ($arr_credit != 0 && $arr_debit != 0)
									{
										$total_debit = array_sum($arr_debit);
										$total_credit = array_sum($arr_credit);
									}
									?>
								  <tfoot>
									<tr>
									  <td class="rounded-foot-left" colspan="3" align="right"><b>Total :</b></td>
									  <td align="right"><?php echo numberFormat($total_debit); ?> </td>
									  <td align="right"><?php echo numberFormat($total_credit);  ?> </td>
									  <td class="rounded-foot-right" colspan="11" align="right">&nbsp;</td>
									</tr>
								  </tfoot>								
								</table>
	
							</td>
						  </tr>							  
						  <tr>
							<td colspan="4" valign="top" scope="col">
								<input type="hidden" name="jv_approved_by" value="<?php echo $jv_approved_by; ?>">
								<input type="hidden" name="jv_recommended_by" value="<?php echo $jv_recommended_by; ?>">
							  	<input name="jv_certified_by" type="checkbox" id="jv_certified_by" value="1" <?php if ($jv_certified_by!=0) { echo "CHECKED";} ?>/>
								* Certify this record.</td>
						  </tr>					  					  					  
			
						</tbody>
					</table>
				</form>
			</td>
		</tr>
	</table>
	<?php include("./../includes/footer.main.php"); ?>
<?php
}
?>