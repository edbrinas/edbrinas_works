<?php 
$report_name = "Disbursement Voucher";
include("./../includes/header.report.php");

$rep_id = $_GET['id'];
$rep_dv_number = $_GET['dv_number'];
$rep_id = decrypt($rep_id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE	
				dv_id,
				dv_year,
				dv_sequence_number,
				dv_number,
				dv_type,
				dv_currency_code,
				dv_date,
				dv_payee,
				dv_description,
				dv_prepared_by,
				dv_prepared_by_date,
				dv_certified_by,
				dv_certified_by_date,
				dv_recommended_by,
				dv_recommended_by_date,
				dv_approved_by,
				dv_approved_by_date
		FROM 	tbl_disbursement_voucher
		WHERE	dv_id = '$rep_id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: disbursement.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$dv_id = $rows["dv_id"];
$dv_type = $rows["dv_type"];
$dv_number = $rows["dv_number"];
$dv_currency_code = $rows["dv_currency_code"];
$dv_date = $rows["dv_date"];
$dv_payee = $rows["dv_payee"];
$dv_description = $rows["dv_description"];
$dv_prepared_by = $rows["dv_prepared_by"];
$dv_prepared_by_date = $rows["dv_prepared_by_date"];
$dv_certified_by = $rows["dv_certified_by"];
$dv_certified_by_date = $rows["dv_certified_by_date"];
$dv_recommended_by = $rows["dv_recommended_by"];
$dv_recommended_by_date = $rows["dv_recommended_by_date"];
$dv_approved_by = $rows["dv_approved_by"];
$dv_approved_by_date = $rows["dv_approved_by_date"];
$dv_inserted_date = $rows["dv_inserted_date"];

$dv_payee = getPayeeById($dv_payee);
$dv_description = stripslashes($dv_description);
$dv_currency_name = getConfigurationDescriptionById($dv_currency_code);
$dv_currency_code = getConfigurationValueById($dv_currency_code);

$dv_prepared_by_id = $dv_prepared_by;
$dv_certified_by_id = $dv_certified_by;
$dv_recommended_by_id = $dv_recommended_by;
$dv_approved_by_id = $dv_approved_by;

if ($dv_prepared_by!=0) { $dv_prepared_by = getFullName($dv_prepared_by,3); } else { $dv_prepared_by=""; }
if ($dv_certified_by!=0) { $dv_certified_by = getFullName($dv_certified_by,3); } else { $dv_certified_by=""; }
if ($dv_recommended_by!=0) { $dv_recommended_by = getFullName($dv_recommended_by,3); } else { $dv_recommended_by=""; }
if ($dv_approved_by!=0) { $dv_approved_by = getFullName($dv_approved_by,3); } else { $dv_approved_by=""; }

if ($dv_certified_by_date == "0000-00-00 00:00:00") { $dv_certified_by_date = ""; }
if ($dv_recommended_by_date == "0000-00-00 00:00:00") { $dv_recommended_by_date = ""; }
if ($dv_approved_by_date == "0000-00-00 00:00:00")  { $dv_approved_by_date = ""; }
if ($dv_cheque_number_date == "0000-00-00 00:00:00")  { $dv_cheque_number_date = ""; }
if ($dv_prepared_by_date == "0000-00-00 00:00:00")  { $dv_prepared_by_date = ""; }

if ($rep_id == $dv_id && $rep_dv_number == md5($dv_number))
{
?>
<table width='100%' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>
	<tr>
		<td scope='col'>
		<!--Start of inner report table -->
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing'>
			  <tr>
				<th scope='col'>
					<?php echo upperCase($report_name); ?><br>
					<?php echo upperCase(getCompanyName()); ?><br>
					<?php echo upperCase(getCompanyAddress()); ?><br>
					ACB-<?php echo $dv_currency_code; ?>				
				</th>
			  </tr>
			  <tr>
				<th scope='col'>&nbsp;</th>
			  </tr>
			  <tr>
				<th scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td width='10%' rowspan='2' valign='top' scope='col'><strong>Payee</strong></td>
					<td width='40%' rowspan='2' scope='col'><?php echo $dv_payee; ?>&nbsp;</td>
					<td width='10%' scope='col'><div align='left'><strong>DV Number </strong></div></td>
					<td width='40%' scope='col'><?php echo $dv_number; ?></td>
				  </tr>
				  <tr>
					<td><div align='left'><strong>Date</strong></div></td>
					<td><?php echo $dv_date; ?></td>
				  </tr>
				</table>				
				</th>
			  </tr>   
			  <tr>
				<td scope='col'>
					<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
					  <tr>
						<th scope='col' align='left'><strong>Description/Particulars</strong></th>
					  </tr>
					  <tr>
						<td><?php echo $dv_description; ?>&nbsp;</td>
					  </tr>
					</table>				
				</td>
			  </tr>  
			  <tr>
				<td scope='col'>
				
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC' >
					<tr>
						<th width="5%" scope='col'>Account Code </th>
						<th width="20%" scope='col'>Account Title </th>
						<th width="6%" scope='col'>Currency</th>
						<th width="10%" scope='col'>Debit</th>
						<th width="10%" scope='col'>Credit</th>
						<th width="5%" scope='col'>Budget ID </th>
						<th width="5%" scope='col'>Donor ID </th>
						<th width="7%" scope='col'>Component ID </th>
						<th width="5%" scope='col'>Activity ID </th>
						<th width="6%" scope='col'>Other Cost /Services ID </th>
						<th width="4%" scope='col'>Staff ID </th>
						<th width="5%" scope='col'>Benefits ID </th>
						<th width="5%" scope='col'>Vehicle ID </th>
						<th width="7%" scope='col'>Equipment ID </th>
					</tr>
				   <?php
			
					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										dv_details_id,
										dv_details_account_code,
										dv_details_reference_number,
										dv_details_debit,
										dv_details_credit,
										dv_details_budget_id,
										dv_details_donor_id,
										dv_details_component_id,
										dv_details_activity_id,
										dv_details_other_cost_id,
										dv_details_staff_id,
										dv_details_benefits_id,
										dv_details_vehicle_id,
										dv_details_equipment_id
								FROM 	tbl_disbursement_voucher_details
								WHERE	dv_details_reference_number = '$dv_id'
								ORDER BY dv_details_id ASC";
					$rs2 = mysql_query($sql2) or die("Error in sql2 in module: journal.voucher.details.php ".$sql2." ".mysql_error());
					$total_rows = mysql_num_rows($rs2);	
					if($total_rows == 0)
					{
						?>
						<tr>
						  <td colspan='14'><div class='redlabel' align='left'>No records found!</div></td>
						</tr>
						<?php
					}
					else
					{
						for($row_number=0;$row_number<$total_rows;$row_number++)
						{
							$rows2 = mysql_fetch_array($rs2);
							$dv_details_id = $rows2["dv_details_id"];
							$dv_details_reference_number = $rows2["dv_details_reference_number"];
							$dv_details_account_code = $rows2["dv_details_account_code"];
							$dv_details_debit = $rows2["dv_details_debit"];
							$dv_details_credit = $rows2["dv_details_credit"];
							$dv_details_budget_id = $rows2["dv_details_budget_id"];
							$dv_details_donor_id = $rows2["dv_details_donor_id"];
							$dv_details_component_id = $rows2["dv_details_component_id"];
							$dv_details_activity_id = $rows2["dv_details_activity_id"];
							$dv_details_other_cost_id = $rows2["dv_details_other_cost_id"];
							$dv_details_staff_id = $rows2["dv_details_staff_id"];
							$dv_details_benefits_id = $rows2["dv_details_benefits_id"];
							$dv_details_vehicle_id = $rows2["dv_details_vehicle_id"];
							$dv_details_equipment_id = $rows2["dv_details_equipment_id"];

							if (checkIfCashInBank($dv_details_account_code)==1)
							{
								$arr_cash_in_bank[] = $dv_details_credit;
							}							
							
							$dv_details_account_title = getSubsidiaryLedgerAccountTitleByid($dv_details_account_code);
							$dv_details_account_code = getSubsidiaryLedgerAccountCodeByid($dv_details_account_code);
							
							$dv_details_budget_id  = getBudgetName($dv_details_budget_id);
							$dv_details_donor_id  = getDonorName($dv_details_donor_id);
							$dv_details_component_id  = getComponentName($dv_details_component_id);
							$dv_details_activity_id = getActivityName($dv_details_activity_id);
							$dv_details_other_cost_id  = getOtherCostName($dv_details_other_cost_id);
							$dv_details_staff_id  = getStaffName($dv_details_staff_id);
							$dv_details_benefits_id  = getBenefitsName($dv_details_benefits_id);
							$dv_details_vehicle_id  = getVehicleName($dv_details_vehicle_id);
							$dv_details_equipment_id  = getEquipmentName($dv_details_equipment_id);
						?>
						<tr>
							<td><?php echo $dv_details_account_code; ?></td>
							<td><?php echo $dv_details_account_title; ?></td>
							<td><?php echo $dv_currency_code; ?></td>
							<td align='right'><?php echo numberFormat($dv_details_debit); ?></td>
							<td align='right'><?php echo numberFormat($dv_details_credit); ?></td>
							<td><?php echo $dv_details_budget_id; ?>&nbsp;</td>
							<td><?php echo $dv_details_donor_id; ?>&nbsp;</td>
							<td><?php echo $dv_details_component_id; ?>&nbsp;</td>
							<td><?php echo $dv_details_activity_id; ?>&nbsp;</td>
							<td><?php echo $dv_details_other_cost_id; ?>&nbsp;</td>
							<td><?php echo $dv_details_staff_id; ?>&nbsp;</td>
							<td><?php echo $dv_details_benefits_id; ?>&nbsp;</td>
							<td><?php echo $dv_details_vehicle_id; ?>&nbsp;</td>
							<td><?php echo $dv_details_equipment_id; ?>&nbsp;</td>
						<?php 
						} 
						?>
					</tr>
					<?php 
					} 
					if (count($arr_cash_in_bank)!=0) { $total_cash_in_bank = array_sum($arr_cash_in_bank); }
					?>
				</table>				
				</td>
			  </tr>
			  <tr>
				<td scope='col'>

				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td width='15%' valign='top' scope='col' align='left'><strong>Prepared by</strong></td>
					<td width='30%' rowspan='2' scope='col' valign='top' align='center'>
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($dv_prepared_by_id);
							}
						?>
					</td>
					<td width='15%' scope='col' align='left' valign="top"><strong>Certified</strong></td>
					<td width='30%' scope='col' align="center" valign="top">*Expenditure legal and proper with supporting documents &amp; adequate funds</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					<td scope='col' valign='top' align='center'>
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($dv_certified_by_id);    // edited by ansel 03/01/2011 changed $dv_certified_by_id to $dv_certified_by_id 
							}
						?>
						</div>						
					</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					<td scope='col' align='center'><b><?php echo $dv_prepared_by; ?></b>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					<td align='center'><b><?php echo $dv_certified_by; ?></b>&nbsp;</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><strong>Prepared Date </strong></td>
					<td scope='col' align='center'><?php echo $dv_prepared_by_date; ?>&nbsp;</td>
					<td align='left'><strong>Certified Date </strong></td>
					<td align='center'><?php echo $dv_certified_by_date; ?>&nbsp;</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><strong>Recommended by</strong></td>
					<td rowspan='2' scope='col' align='center'>
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($dv_recommended_by_id);
							}
						?>
					</td>
					<td align='left' valign="top"><strong>Approved by </strong></td>
					<td rowspan='2' align="center" valign="top">
						<?php
							$enable_digital_signature = enableDigitalSignature();
							if ($enable_digital_signature == 1)
							{
								echo getUserSignature($dv_approved_by_id);
							}
						?>
					</td>
				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					</tr>
				  <tr>
					<td valign='top' scope='col'>&nbsp;</td>
					<td scope='col' align='center'><b><?php echo $dv_recommended_by; ?></b>&nbsp;</td>
					<td align='left'>&nbsp;</td>
					<td align='center'><b>
						<?php 
							echo $dv_approved_by;
							if($dv_approved_by_id == 21)
								// echo ' - Alternate OIC-ACB and FA Unit Head';	
								echo ' - FA Unit Head';								 
						?>
					</b>&nbsp;</td>				  </tr>
				  <tr>
					<td valign='top' scope='col' align='left'><strong>Recommended Date</strong></td>
					<td scope='col' align='center'><?php echo $dv_recommended_by_date; ?>&nbsp;</td>
					<td align='left'><strong>Approved Date </strong></td>
					<td align='center'><?php echo $dv_approved_by_date; ?>&nbsp;</td>
				  </tr>
				</table>
			
				</td>
			  </tr>
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<td scope='col'><div align='left'>Received from <b><?php echo upperCase(getCompanyName()); ?></b> the sum of <b><?php echo convertToWords($total_cash_in_bank)." ".$dv_currency_name; ?></b> (<?php echo $dv_currency_code." ".numberFormat($total_cash_in_bank); ?>) in payment for the above account.</div></td>
				  </tr>
				</table>	
				</td>
			  </tr>
			  <tr>
				<td scope='col'>
				<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC'>
				  <tr>
					<th colspan='2' scope='col'>DETAILS</th>
					<th colspan='2' scope='col'>ACKNOWLEDGEMENT</th>
					</tr>
				  <tr>
					<td width='20%' rowspan="2" scope='col' valign="top"><div align='left'>Amount</div></td>
					<td width='30%' rowspan="2" scope='col' valign="top"><b><?php echo $dv_currency_code." ".numberFormat($total_cash_in_bank); ?></b></td>
					<td width='20%' scope='col'><div align='left'>Signature</div></td>
					<td width='30%' rowspan="2" scope='col'>&nbsp;</td>
				  </tr>
				  <tr>
				    <td scope='col'>&nbsp;</td>
			      </tr>
				  <tr>
					<td scope='col'><div align='left'>Check Number</div></td>
					<td scope='col'>
					<?php
                    $dv_cheque_number = getChequeNumber($dv_id);
					if (count($dv_cheque_number)!=0)
					{
						foreach ($dv_cheque_number AS $dv_cheque_number)
						{
							echo $dv_cheque_number."<br>"; 
						}
					}
					?>
                    &nbsp;
                    </td>
					<td scope='col'><div align='left'>Printed Name</div></td>
					<td scope='col'>&nbsp;</td>
				  </tr>
				  <tr>
					<td scope='col'><div align='left'>Check Date</div></td>
					<td scope='col'>
					<?php 
                    $dv_cheque_number_date = getChequeNumberDate($dv_id);
					if (count($dv_cheque_number_date)!=0)
					{
						foreach ($dv_cheque_number_date AS $dv_cheque_number_date)
						{
							echo $dv_cheque_number_date."<br>"; 
						}
					}					
					?>&nbsp;
                    </td>
					<td scope='col'><div align='left'>Date</div></td>
					<td scope='col'>&nbsp;</td>
				  </tr>
				</table>
				</td>
			  </tr>
			</table>		
		<!-- End of inner report table -->
		</td>
	</tr>
</table>
<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
	<a href="/workspaceGOP/report.templates/disbursement.voucher.php?id=<?php echo encrypt($dv_id); ?>&dv_number=<?php echo md5($dv_number); ?>"><img src="/workspaceGOP/images/save.png" border="0" title="Export to word"></a>
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: journal.voucher.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!");
		window.close();
	</script>
	<?php
}
?>