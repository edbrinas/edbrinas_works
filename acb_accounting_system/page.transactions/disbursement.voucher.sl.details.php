<?php 
$report_name = "Disbursement Voucher";
include("./../includes/header.report.php");
$report_name = "DISBURSEMENT VOUCHER";
$rep_id = $_GET['id'];
$rep_dv_number = $_GET['dv_number'];
$rep_id = decrypt($rep_id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE	
				dv_id,			
				dv_type,
				dv_number,
				dv_currency_code,
				dv_date,
				dv_payee,
				dv_description,
				dv_prepared_by,
				dv_certified_by,
				dv_recommended_by,
				dv_approved_by
		FROM 	tbl_disbursement_voucher
		WHERE	dv_id = '$rep_id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: disbursement.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$dv_id = $rows["dv_id"];
$dv_type = $rows["dv_type"];
$dv_number = $rows["dv_number"];
$dv_currency_code = $rows["dv_currency_code"];
$dv_date = $rows["dv_date"];
$dv_payee = $rows["dv_payee"];
$dv_description = $rows["dv_description"];
$dv_prepared_by = $rows["dv_prepared_by"];
$dv_certified_by = $rows["dv_certified_by"];
$dv_recommended_by = $rows["dv_recommended_by"];
$dv_approved_by = $rows["dv_approved_by"];

$dv_payee = getPayeeById($dv_payee);
$dv_description = stripslashes($dv_description);
$dv_currency_name = getConfigurationDescriptionById($dv_currency_code);
$dv_currency_code = getConfigurationValueById($dv_currency_code);

if ($dv_prepared_by!=0) { $dv_prepared_by = getFullName($dv_prepared_by,3); } else { $dv_prepared_by=""; }
if ($dv_certified_by!=0) { $dv_certified_by = getFullName($dv_certified_by,3); } else { $dv_certified_by=""; }
if ($dv_recommended_by!=0) { $dv_recommended_by = getFullName($dv_recommended_by,3); } else { $dv_recommended_by=""; }
if ($dv_approved_by!=0) { $dv_approved_by = getFullName($dv_approved_by,3); } else { $dv_approved_by=""; }

if ($rep_id == $dv_id && $rep_dv_number == md5($dv_number))
{
?>
<table width='100%' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>
	<tr>
		<td scope='col'>
		<!-- Start of Inner Table -->
			<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC' class='report_printing'>
			  <tr>
				<th width="6%" rowspan='3' scope='col'> Date </th>
				<th width="4%" rowspan='3' scope='col'>Voucher Number </th>
				<th width="3%" rowspan='3' scope='col'>Payee</th>
				<th width="4%" rowspan='3' scope='col'>Cur rency </th>
				<th width="6%" rowspan='3' scope='col'>Parti culars / Des cription </th>
				<th width="3%" rowspan='3' scope='col'>Debit / Credit</th>
				<th width="4%" rowspan='3' scope='col'>Account Code </th>
				<th colspan='9' scope='col'>SL CODE </th>
				<th width="4%" rowspan='3' scope='col'>Amount in Words </th>
				<th width="6%" rowspan='3' scope='col'>Acknowled gement Portion </th>
				<th width="5%" rowspan='3' scope='col'>Prepared By </th>
				<th width="3%" rowspan='3' scope='col'>Cert ified By </th>
				<th width="4%" rowspan='3' scope='col'>Recom mended by </th>
				<th width="4%" rowspan='3' scope='col'>App roved by </th>
			  </tr>
			  <tr>
				<th width="4%">SLC1</th>
				<th width="3%">SLC2</th>
				<th width="6%">SLC3</th>
				<th width="4%">SLC4</th>
				<th width="5%">SLC5</th>
				<th width="6%">SLC6</th>
				<th width="4%">SLC7</th>
				<th width="6%">SLC8</th>
				<th width="6%">SLC9</th>
			  </tr>
			  <tr>
				<th>Budget ID </th>
				<th>Donor ID </th>
				<th>Component ID </th>
				<th>Activity ID </th>
				<th>Other Cost / Services ID </th>
				<th>Staff ID </th>
				<th>Benefits ID </th>
				<th>Vehicle ID </th>
				<th>Equipment ID </th>
			  </tr>
			  <tr>
				<td valign='top'><?php echo $dv_date; ?>&nbsp;</td>
				<td valign='top'><?php echo $dv_number; ?>&nbsp;</td>
				<td valign='top'><?php echo $dv_payee; ?>&nbsp;</td>
				<td valign='top'><?php echo $dv_currency_code; ?>&nbsp;</td>
				<td valign='top'><?php echo $dv_description; ?>&nbsp;</td>
				<?php
				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									dv_details_id,
									dv_details_account_code,
									dv_details_reference_number,
									dv_details_debit,
									dv_details_credit,
									dv_details_budget_id,
									dv_details_donor_id,
									dv_details_component_id,
									dv_details_activity_id,
									dv_details_other_cost_id,
									dv_details_staff_id,
									dv_details_benefits_id,
									dv_details_vehicle_id,
									dv_details_equipment_id
							FROM 	tbl_disbursement_voucher_details
							WHERE	dv_details_reference_number = '$dv_id'";
				$rs2 = mysql_query($sql2) or die("Error in sql2 in module: journal.voucher.details.php ".$sql2." ".mysql_error());
				$total_rows = mysql_num_rows($rs2);	
				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows2 = mysql_fetch_array($rs2);
					$dv_details_id[] = $rows2['dv_details_id'];
					$dv_details_reference_number[] = $rows2['dv_details_reference_number'];
					$dv_details_account_code[] = $rows2['dv_details_account_code'];
					$dv_details_debit = $rows2['dv_details_debit'];
					$dv_details_credit = $rows2['dv_details_credit'];
					$dv_details_budget_id[] = $rows2['dv_details_budget_id'];
					$dv_details_donor_id[] = $rows2['dv_details_donor_id'];
					$dv_details_component_id[] = $rows2['dv_details_component_id'];
					$dv_details_activity_id[] = $rows2['dv_details_activity_id'];
					$dv_details_other_cost_id[] = $rows2['dv_details_other_cost_id'];
					$dv_details_staff_id[] = $rows2['dv_details_staff_id'];
					$dv_details_benefits_id[] = $rows2['dv_details_benefits_id'];
					$dv_details_vehicle_id[] = $rows2['dv_details_vehicle_id'];
					$dv_details_equipment_id[] = $rows2['dv_details_equipment_id'];
					
					if (numberFormat($dv_details_debit)!=0)
					{
						$dv_transaction_total[] = numberFormat($dv_details_debit);
					}
					else if (numberFormat($dv_details_credit)!=0)
					{
						$dv_transaction_total[] = numberFormat($dv_details_credit);
					}
				}
				?>
					<td valign='top'>
						<?php 
						foreach ($dv_transaction_total as $dv_transaction_total)
						{
							echo $dv_transaction_total;
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($dv_details_account_code as $dv_details_account_code)
						{
							echo getSubsidiaryLedgerAccountTitleByid($dv_details_account_code);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($dv_details_budget_id as $dv_details_budget_id)
						{
							echo getBudgetDescription($dv_details_budget_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($dv_details_donor_id as $dv_details_donor_id)
						{
							echo getDonorDescription($dv_details_donor_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($dv_details_component_id as $dv_details_component_id)
						{
							echo getComponentDescription($dv_details_component_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($dv_details_activity_id as $dv_details_activity_id)
						{
							echo getActivityDescription($dv_details_activity_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>																				
					<td valign='top'>
						<?php 
						foreach ($dv_details_other_cost_id as $dv_details_other_cost_id)
						{
							echo getOtherCostDescription($dv_details_other_cost_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($dv_details_staff_id as $dv_details_staff_id)
						{
							echo getStaffDescription($dv_details_staff_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>	
					<td valign='top'>
						<?php 
						foreach ($dv_details_benefits_id as $dv_details_benefits_id)
						{
							echo getBenefitsDescription($dv_details_benefits_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($dv_details_vehicle_id as $dv_details_vehicle_id)
						{
							echo getVehicleDescription($dv_details_vehicle_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>	
					<td valign='top'>
						<?php 
						foreach ($dv_details_equipment_id as $dv_details_equipment_id)
						{
							echo getEquipmentDescription($dv_details_equipment_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>																				
				<td valign='top'><?php echo convertToWords($dv_transaction_total).' '.$dv_currency_name; ?>&nbsp;</td>
				<td valign='top'>&nbsp;</td>
				<td valign='top'><?php echo $dv_prepared_by; ?>&nbsp;</td>
				<td valign='top'><?php echo $dv_certified_by; ?>&nbsp;</td>
				<td valign='top'><?php echo $dv_recommended_by; ?>&nbsp;</td>
				<td valign='top'><?php echo $dv_approved_by; ?>&nbsp;</td>
			  </tr>
        </table>
		<!-- End of Inner Table -->		
		</td>
	</tr>
</table>
<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
	<a href="/workspaceGOP/report.templates/disbursement.voucher.php?id=<?php echo encrypt($dv_id); ?>&dv_number=<?php echo md5($dv_number); ?>"><img src="/workspaceGOP/images/save.png" border="0" title="Export to word"></a>
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: journal.voucher.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!");
		window.close();
	</script>
	<?php
}
?>