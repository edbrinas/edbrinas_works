<?php 
$report_name = "TELEGRAPHIC TRANSFER";
include("./../includes/header.report.php");

$rep_id = $_GET['id'];
$rep_tt_number = $_GET['tt_number'];
$rep_id = decrypt($rep_id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE	
				tt_id,
				tt_year,
				tt_sequence_number,
				tt_number,
				tt_date,
				tt_country,
				tt_bank_from, 
				tt_bank_to,
				tt_bank_to_address,
				tt_bank_to_account_number,
				tt_currency,
				tt_amount,
				tt_swift_code,
				tt_iban,
				tt_beneficiary,
				tt_purpose
		FROM 	tbl_telegraphic_transfer
		WHERE	tt_id = '$rep_id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: cash.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);
$tt_id = $rows["tt_id"];
$tt_number = $rows["tt_number"];
$tt_beneficiary = $rows["tt_beneficiary"];
$tt_date = $rows["tt_date"];
$tt_country = $rows["tt_country"];
$tt_bank_from = $rows["tt_bank_from"];
$tt_bank_to	= $rows["tt_bank_to"];	
$tt_bank_to_address	= $rows["tt_bank_to_address"];
$tt_bank_to_account_number = $rows["tt_bank_to_account_number"];	
$tt_currency = $rows["tt_currency"];
$tt_amount = $rows["tt_amount"];
$tt_swift_code = $rows["tt_swift_code"];
$tt_iban = $rows["tt_iban"];
$tt_beneficiary = $rows["tt_beneficiary"];
$tt_purpose = $rows["tt_purpose"];
	
$tt_country = getConfigurationValueById($tt_country);
$tt_bank_from_name = getBankName($tt_bank_from);
$tt_bank_from_address = getBankAddress($tt_bank_from_address);
$tt_bank_from_account_number = getAccountNumber($tt_bank_from);
$tt_currency = getConfigurationValueById($tt_currency);
$tt_amount = numberFormat($tt_amount);
$tt_purpose = stripslashes($tt_purpose);

if ($rep_id == $tt_id && $rep_tt_number == md5($tt_number))
{
?>
<table width='100%' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>
	<tr>
		<td scope='col'>
		<!--Start of inner report table -->
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing'>
			  <tr>
				<th scope='col' align="center">
					<table width="90%" border="0" cellpadding="0" cellspacing="0">
					  <tr>
						<td width="30%" align="right"><img src="/workspaceGOP/UploadedFiles/Image/acb_logo.jpg" width="158" height="110" /></td>
						<td width="70%" align="center">					
						<?php echo upperCase(getCompanyName()); ?><br>
						<?php echo upperCase(getCompanyAddress()); ?><br>
						<?php echo "Telefax: ".getCompanyFaxNumber()."; Tel".getCompanyPhoneNumber(); ?><br>
						<?php echo getCompanyEmailAddress()." * ".getCompanyWebsite(); ?>
						</td>
					  </tr>
				  </table>
				</th>
			  </tr>
			  <tr>
				<th scope='col'><hr></th>
			  </tr>
			  <tr>
				<td scope='col' align="center">
					<table width="90%" border="0" cellpadding="1" cellspacing="1">
			  <tr>
						<td width="25%">&nbsp;</td>
						<td width="25%">&nbsp;</td>
					  <td width="50%">&nbsp;</td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td align="left">Date :&nbsp;<?php echo $tt_date; ?></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td align="left">Calyon Bank T/T No. :&nbsp;<b><u><?php echo $tt_number; ?></u></b></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left"><b>THE MANAGER</b></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td colspan="2" align="left"><?php echo $tt_bank_from_name; ?></td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left"><?php echo $tt_bank_from_address; ?></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Dear Sir:</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td colspan="3"><div align="center"><b>TELEGRAPHIC TRANSFER OF FUNDS TO:&nbsp;&nbsp;&nbsp;<u><?php echo $tt_country; ?></u></b></div></td>
					  </tr>
					<tr>
					  <td colspan="3"><div align="center"><b>FROM OUR EURO ACCOUNT NO.&nbsp;&nbsp;&nbsp;<?php echo $tt_bank_from_account_number; ?></b></div></td>
					  </tr>
					<tr>
					  <td colspan="3"><div align="center"><b>TO THEIR ACCOUNT NO.&nbsp;&nbsp;&nbsp;<u><?php echo $tt_bank_to_account_number; ?></u></b></div></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td colspan="2" align="left">Please would you kindly telegraphic transfer funds as follows: </td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Amount in words:</td>
					  <td colspan="2" align="left"><b><u>***<?php echo convertToWords($tt_amount)." ".$tt_currency; ?>***</u></b></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Amount in figures:</td>
					  <td colspan="2" align="left"><b><u><?php echo $tt_currency." ".$tt_amount; ?></u></b></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Bank  :</td>
					  <td colspan="2" align="left"><b><?php echo $tt_bank_to; ?></b></td>
					  </tr>
					<tr>
					  <td valign="top">&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td valign="top" align="left">Address:</td>
					  <td colspan="2" align="left"><?php echo $tt_bank_to_address; ?></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Account No.:</td>
					  <td colspan="2" align="left"><?php echo $tt_bank_to_account_number; ?></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Swift Code:</td>
					  <td colspan="2" align="left"><?php echo $tt_swift_code; ?></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Beneficiary:</td>
					  <td colspan="2" align="left"><?php echo $tt_beneficiary; ?></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Currency:</td>
					  <td colspan="2" align="left"><?php echo $tt_currency; ?></td>
					  </tr>
					<tr valign="top">
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr valign="top">
					  <td align="left">Purpose:</td>
					  <td colspan="2" align="left"><?php echo $tt_purpose; ?></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td colspan="3" align="left">Please debit our account including bank charges accordingly in euro at exchange rate of date of transaction. Transfer charges are to be borne by our account here, and not the beneficiary.</td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Yours sincerely,</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td colspan="2"><div align="center">Approved:</div></td>
					  </tr>
					<tr>
					  <td align="left"><b>WILFREDO J. OBIEN</b></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Head, Finance and Admin.</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td colspan="2"><div align="center"><b>MA. CONSUELO D. GARCIA / RODRIGO U. FUENTES</b></div></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td colspan="2"><div align="center">Director, BIM / Executive Director</div></td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Certified Funds Available:</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left"><b>FROYLA A. VEGA</b></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr>
					  <td align="left">Accountant</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					</table>
				</td>
			  </tr>
			</table>		
		<!-- End of inner report table -->
		</td>
	</tr>
</table>
<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: cash.voucher.details.php");	
	#session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!");
		window.close();
	</script>
	<?php
}
?>