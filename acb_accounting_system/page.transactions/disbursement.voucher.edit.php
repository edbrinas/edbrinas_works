<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$display_msg = 0;
$id = decrypt($id);

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;
$date_time_inserted = date("Y-m-d H:i:s");

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE				
				dv_id,
				dv_number,
				dv_currency_code,
				dv_date,
				dv_payee,
				dv_description
		FROM 	tbl_disbursement_voucher
		WHERE	dv_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: disbursement.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["dv_id"];
$dv_number = $rows["dv_number"];
$dv_currency_code = $rows["dv_currency_code"];
$dv_date = $rows["dv_date"];
$dv_payee = $rows["dv_payee"];
$dv_description = $rows["dv_description"];

$dv_description = stripslashes($dv_description);
$chunk_date = explode("-",$dv_date);
$year = $chunk_date[0];
$month = $chunk_date[1];
$day = $chunk_date[2];

if ($_POST['Submit'] == 'Update Details')
{
	$msg = "";
	$set_new_dv = "";
	
	$record_id = $_POST['record_id'];
	$dv_number = $_POST['dv_number'];
	$dv_currency_code = $_POST['dv_currency_code'];
	$dv_year = $_POST['dv_year'];
	$dv_month = $_POST['dv_month'];
	$dv_day = $_POST['dv_day'];
	$dv_payee = $_POST['dv_payee'];
	$dv_description = $_POST['dv_description'];
	$dv_description = addslashes($dv_description);
	
	$currency_name = getVoucherId($dv_currency_code);	
	$arr_old_currency = stringtoarray($dv_number);
	$cur_old = $arr_old_currency[0];

	if ($cur_old!=$currency_name)
	{
		$dv_number = getVoucherSequenceNumber("DV",$dv_currency_code,$dv_month,$dv_year);
		$dv_sequence_number = substr($dv_number,-4);
		$set_new_dv = 1;
	}	

	$dv_date = dateTimeFormat($hr="",$min="",$sec="",$dv_month,$dv_day,$dv_year);
	
	if ($dv_number == "") { $display_msg=1; $msg .= "Please enter DV Number"; $msg .="<br>"; }
	if ($dv_currency_code == "") { $display_msg=1; $msg .= "Please select Currency"; $msg .="<br>"; }
	#if ($dv_payee == "") { $display_msg=1; $msg .= "Please select Payee"; $msg .="<br>"; }
	if ($dv_description == "") { $display_msg=1; $msg .= "Please enter Description"; $msg .="<br>"; }
	#if ($dv_cheque_number == "") { $display_msg=1; $msg .= "Please enter Check/Invoice Numebr"; $msg .="<br>"; }
	
	if ($msg=="")
	{
		$sql = "	UPDATE 	tbl_disbursement_voucher
					SET		dv_date = '".trim($dv_date)."',
							dv_payee = '".trim($dv_payee)."',
							dv_description = '".trim($dv_description)."',
							dv_inserted_date = '".trim($date_time_inserted)."'";
		if ($set_new_dv == 1)
		{
			$sql .= ",";
			$sql .= "dv_number = '".trim($dv_number)."',";
			$sql .= "dv_sequence_number = '".trim($dv_sequence_number)."',";
			$sql .= "dv_currency_code = '".trim($dv_currency_code)."'";
		}
		$sql .= "	WHERE	dv_id = '$record_id'";

		mysql_query($sql) or die("Error inserting values : module : disbursement.voucher.edit.php ".$sql." ".mysql_error());
		insertEventLog($user_id,$sql);
		$dv_number = encrypt($dv_number);
		$record_id = encrypt($record_id);
		header("location:/workspaceGOP/page.transactions/disbursement.voucher.add.details.php?dv_id=$record_id&dv_number=$dv_number");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="disbursement_voucher" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" colspan="2" class="rounded-header"><div class="main" align="left"><strong>Disbursement Voucher [Edit Record]</strong></div></th>
							<th scope="col" colspan="2" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" colspan="2" align="left"><input type="submit" name="Submit" value="Update Details" class="formbutton"></td>
							<td class="rounded-foot-right" colspan="2" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
					  <tr>
						<td scope="col"><b>DV Number</b></td>
						<td colspan="3" scope="col"><?php echo $dv_number; ?>
						<input name="dv_number" type="hidden" class="formbutton" id="dv_number" value="<?php echo $dv_number; ?>"></td>
					  </tr>
					  <tr>
						<td scope="col"><b>Currency*</b></td>
						<td scope="col">
								<?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_id,
													cfg_value
											FROM 	tbl_config
											WHERE 	cfg_name = 'currency_type'";
									$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
									$str .= "<select name='dv_currency_code' class='formbutton'>";
									while($data=mysql_fetch_array($rs))
									{
										$column_id = $data['cfg_id'];
										$column_name = $data['cfg_value'];
										$str .= "<option value='$column_id'";
										if ($dv_currency_code == $column_id)
										{
											$str .= " SELECTED";
										}
										$str .= ">".$column_name."";
									}
									$str .= "</select>&nbsp;";
									echo $str;
								?>						</td>
						<td scope="col"><b>Date*</b></td>
						<td scope="col">
							<select name='dv_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='dv_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='dv_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day )
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>						</td>
					  </tr>
					  <tr>
						<td scope="col"><b>Payee*</b></td>
						<td colspan="3" scope="col">
								<?php
									$sql1 = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													payee_id,
													payee_name
											FROM 	tbl_payee
											ORDER BY payee_name";
									$rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
									$str1 .= "<select name='dv_payee' class='formbutton'>";
									$str1 .= "<option value=''>[SELECT PAYEE]";
									while($data1=mysql_fetch_array($rs1))
									{
										$column_id = $data1['payee_id'];
										$column_name = $data1['payee_name'];
										$str1 .= "<option value='$column_id'";
										if ($dv_payee == $column_id)
										{
											$str1 .= " SELECTED";
										}
										$str1 .= ">".$column_name."";
									}
									$str1 .= "</select>&nbsp;";
									echo $str1;
								?>						</td>
					  </tr>
					  <tr>
						<td valign="top" scope="col"><b>Particulars/Description*</b></td>
					    <td colspan="3" valign="top" scope="col">
					    <textarea name="dv_description" cols="40" rows="5" class="formbutton"><?php echo $dv_description; ?></textarea>					    </td>
				      </tr>
					</tbody>
				</table>
		  </form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>