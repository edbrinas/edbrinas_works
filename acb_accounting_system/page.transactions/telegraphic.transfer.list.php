<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$page_size = getConfigurationValueByName('max_record_per_page');

$record_id = $_GET['id'];
$action = $_GET['action'];
$get_msg = $_GET["msg"];
$order = $_GET["order"];
$cur_page= $_GET["cur_page"]; 
$search_for = $_GET['search_for'];
$search_by = $_GET['search_by'];

if($order=="")
{
  	$order = "tt_id";
}
if ($get_msg==1)
{
	$msg = "Record updated successfully!";
	$display_msg = 1;
}

if($cur_page=="")
{
   	$cur_page= 1;
}
if($cur_page==1)
{
	$count_page = 0;
}
else
{
  	$count_page = (($cur_page - 1) * $page_size);
}

if ($record_id != "" && $action == "confirm_delete" && $_SESSION["level"]==1)
{
?>
	<script type="text/javascript">
	var flg=confirm('Are you sure you want to delete this record?\n\nClick OK to continue. Otherwise click Cancel.\n');
	if (flg==true)
	{
		
		window.location = '/workspaceGOP/page.transactions/telegraphic.transfer.list.php?id=<?php echo $record_id; ?>&action=delete'
	}
	else
	{
		window.location = '/workspaceGOP/page.transactions/telegraphic.transfer.list.php'
	}
	</script> 
    <?php
}
elseif ($action == "confirm_delete" && $_SESSION["level"]!=1)
{
	$msg = "Only administrator can delete a record!";
	$display_msg = 1;
}

if ($record_id != "" && $action == "delete")
{
	$sql = "DELETE FROM tbl_telegraphic_transfer
			WHERE 		tt_id = '".decrypt($record_id)."'";
	mysql_query($sql) or die("Error in module: tt.list.php ".$sql." ".mysql_error());
	
	insertEventLog($user_id,$sql);
	
	$msg = "Record deleted successfully!";
	$display_msg = 1;
}
?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
<tr>
	<td>
		<form method="get">
			<table id="rounded-search-table">
				<thead>
					<tr>
						<th class="rounded-header"><div class="whitelabel">Search</div></th>
						<th class="rounded-q4">&nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td class="rounded-foot-left"><input type="submit" name="action" value="Search" class="formbutton"></td>
						<td class="rounded-foot-right" align="right">&nbsp;</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<td><input name="search_for" type="text" id="search_for" class="formbutton" value="<?php echo $search_for; ?>"></td>
						<td>
							<select name="search_by" class="formbutton">
							  <option value="ALL" selected>ALL</option>
							  <option value="tt_number" <?php if ($search_by == 'tt_name') { echo "selected"; } ?> >Telegraphic Transfer Number</option>
							  <option value="tt_description" <?php if ($search_by == 'tt_description') { echo "selected"; } ?> >tt Description</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
		<!-- START OF TABLE -->
		<table id="rounded-add-entries">
		<?php
			if ($action == "Search" && $search_for != "" && $search_by != "")
			{
				if ($search_for != "" && ($search_by != "" && $search_by != "ALL"))	
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE				
										tt_id,
										tt_year,
										tt_sequence_number,
										tt_number,
										tt_date,
										tt_country,
										tt_bank_from,
										tt_bank_to,
										tt_bank_to_address,
										tt_bank_to_account_number,
										tt_currency,
										tt_amount,
										tt_swift_code,
										tt_iban,
										tt_beneficiary,
										tt_purpose
								FROM 	tbl_telegraphic_transfer
								WHERE 	".$search_by." LIKE '%".$search_for."%'
								ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										tt_id,
										tt_year,
										tt_sequence_number,
										tt_number,
										tt_date,
										tt_country,
										tt_bank_from,
										tt_bank_to,
										tt_bank_to_address,
										tt_bank_to_account_number,
										tt_currency,
										tt_amount,
										tt_swift_code,
										tt_iban,
										tt_beneficiary,
										tt_purpose
								FROM 	tbl_telegraphic_transfer
								WHERE 	".$search_by." LIKE '%".$search_for."%'
								ORDER BY $order DESC 
								LIMIT	$count_page, $page_size";	
				}
				if ($search_for == "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE										
										tt_id,
										tt_year,
										tt_sequence_number,
										tt_number,
										tt_date,
										tt_country,
										tt_bank_from,
										tt_bank_to,
										tt_bank_to_address,
										tt_bank_to_account_number,
										tt_currency,
										tt_amount,
										tt_swift_code,
										tt_iban,
										tt_beneficiary,
										tt_purpose
								FROM 	tbl_telegraphic_transfer
								ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										tt_id,
										tt_year,
										tt_sequence_number,
										tt_number,
										tt_date,
										tt_country,
										tt_bank_from,
										tt_bank_to,
										tt_bank_to_address,
										tt_bank_to_account_number,
										tt_currency,
										tt_amount,
										tt_swift_code,
										tt_iban,
										tt_beneficiary,
										tt_purpose
								FROM 	tbl_telegraphic_transfer
								ORDER BY $order DESC 
								LIMIT	$count_page, $page_size";		
				}
				if ($search_for != "" && $search_by == "ALL")
				{
					$sql1 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										tt_id,
										tt_year,
										tt_sequence_number,
										tt_number,
										tt_date,
										tt_country,
										tt_bank_from,
										tt_bank_to,
										tt_bank_to_address,
										tt_bank_to_account_number,
										tt_currency,
										tt_amount,
										tt_swift_code,
										tt_iban,
										tt_beneficiary,
										tt_purpose
								FROM 	tbl_telegraphic_transfer
								WHERE	tt_year LIKE '%".$search_for."%'
								OR		tt_sequence_number LIKE '%".$search_for."%'
								OR		tt_number LIKE '%".$search_for."%'
								OR		tt_date LIKE '%".$search_for."%'
								OR		tt_country LIKE '%".$search_for."%'
								OR		tt_bank_from  LIKE '%".$search_for."%'
								OR		tt_bank_to LIKE '%".$search_for."%'
								OR		tt_currency LIKE '%".$search_for."%'
								OR		tt_amount LIKE '%".$search_for."%'
								OR		tt_swift_code LIKE '%".$search_for."%'
								OR		tt_iban LIKE '%".$search_for."%'
								OR		tt_beneficiary LIKE '%".$search_for."%'
								OR		tt_purpose LIKE '%".$search_for."%'						
								ORDER BY $order DESC ";

					$sql2 = "	SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										tt_id,
										tt_year,
										tt_sequence_number,
										tt_number,
										tt_date,
										tt_country,
										tt_bank_from,
										tt_bank_to,
										tt_bank_to_address,
										tt_bank_to_account_number,										
										tt_currency,
										tt_amount,
										tt_swift_code,
										tt_iban,
										tt_beneficiary,
										tt_purpose
								FROM 	tbl_telegraphic_transfer
								WHERE	tt_year LIKE '%".$search_for."%'
								OR		tt_sequence_number LIKE '%".$search_for."%'
								OR		tt_number LIKE '%".$search_for."%'
								OR		tt_date LIKE '%".$search_for."%'
								OR		tt_country LIKE '%".$search_for."%'
								OR		tt_bank_from  LIKE '%".$search_for."%'
								OR		tt_bank_to LIKE '%".$search_for."%'
								OR		tt_currency LIKE '%".$search_for."%'
								OR		tt_amount LIKE '%".$search_for."%'
								OR		tt_swift_code LIKE '%".$search_for."%'
								OR		tt_iban LIKE '%".$search_for."%'
								OR		tt_beneficiary LIKE '%".$search_for."%'
								OR		tt_purpose LIKE '%".$search_for."%'	
								ORDER BY $order DESC 
								LIMIT	$count_page, $page_size";
				}
			}
			else
			{
				$sql1 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									tt_id,
									tt_year,
									tt_sequence_number,
									tt_number,
									tt_date,
									tt_country,
									tt_bank_from,
									tt_bank_to,
									tt_bank_to_address,
									tt_bank_to_account_number,										
									tt_currency,
									tt_amount,
									tt_swift_code,
									tt_iban,
									tt_beneficiary,
									tt_purpose
							FROM 	tbl_telegraphic_transfer
							ORDER BY $order DESC ";

				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									tt_id,
									tt_year,
									tt_sequence_number,
									tt_number,
									tt_date,
									tt_country,
									tt_bank_from,
									tt_bank_to,
									tt_bank_to_address,
									tt_bank_to_account_number,										
									tt_currency,
									tt_amount,
									tt_swift_code,
									tt_iban,
									tt_beneficiary,
									tt_purpose
							FROM 	tbl_telegraphic_transfer
							ORDER BY $order DESC 
							LIMIT	$count_page, $page_size";
			}	
			$rs1 = mysql_query($sql1) or die("Error in sql1 in module: tt.list.php ".$sql1." ".mysql_error());
			$total_records = mysql_num_rows($rs1);
			$total_pages = ceil($total_records/$page_size); 
			$rs2 = mysql_query($sql2) or die("Error in sql2 in module: tt.list.php ".$sql2." ".mysql_error());
			$total_rows = mysql_num_rows($rs2);	
			?>
			<thead>
				<tr>
					<th scope="col" class="rounded-header"><a href="<?php echo $page; ?>?order=tt_number" class="whitelabel">TT Number</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_beneficiary" class="whitelabel">Beneficiary</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_date" class="whitelabel">TT Date</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_country" class="whitelabel">TT Country</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_bank_from" class="whitelabel">Bank From</a></th>
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_bank_to" class="whitelabel">Bank To</a></th>					
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_currency" class="whitelabel">Currency</a></th>	
					<th scope="col" class="rounded-q2"><a href="<?php echo $page; ?>?order=tt_amount" class="whitelabel">Amount</a></th>																			
					<th scope="col" class="rounded-q4"><div class="whitelabel">Options</div></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2" class="rounded-foot-left">Total No. of Records : <?php echo $total_records;?></td>
					<td colspan="7" class="rounded-foot-right" align="right">					
						<?php 

							$next_page = $cur_page + 1;
							$previous_page = $cur_page - 1;
							$next_previous_numbers = 5;
							
							if($cur_page>1)
							{
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$previous_page&order=$order\" class=\"main\"><< Previous</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=1&order=$order\" class=\"main\">1</a>";
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
							} 

					
							if ($cur_page >= $next_previous_numbers)
							{
								$num = $previous_page;
								$numprevnum = $num - $next_previous_numbers;
								for($y=$numprevnum;$y<=$num;$y++)
								{
									if ($y != 0 && $y != -1 && $y != 1)
									{
										echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$y&order=$order\" class=\"main\">$y</a>";
										echo "&nbsp;";
									}
								}
							}
							
							echo "<span class=\"main\"><b>$cur_page</b></span>";
							
							$totalb = $cur_page + $next_previous_numbers;
							if ($totalb >= $total_pages)
							{
								$totalb = $total_pages;
							}
							
							for($z=$next_page;$z<=$totalb;$z++)
							{
								if ($z != $total_pages)
								{
									echo "&nbsp;";
									echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$z&order=$order\" class=\"main\">$z</a>";
								}
							}
							
								
							if($cur_page<$total_pages)
							{
								echo "&nbsp;";
								echo "...";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$total_pages&order=$order\" class=\"main\">$total_pages</a>";
								echo "&nbsp;";
								echo "<a href=\"$page?search_for=$search_for&search_by=$search_by&action=$action&cur_page=$next_page&order=$order\" class=\"main\">Next >></a>";
								} 
							?>		
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			if ($display_msg==1)
			{
			?>
				<tr>
					<td colspan="9"><div class="redlabel" align="center"><?php echo $msg; ?></div></td>
				</tr>
			<?php
			}
			if($total_rows == 0)
			{
				?>
				<tr>
					<td colspan="9"><div class="redlabel" align="left">No records found!</div></td>
				</tr>
				<?php
			}
			else
			{

				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows = mysql_fetch_array($rs2);
					$id = $rows["tt_id"];
					$tt_number = $rows["tt_number"];
					$tt_beneficiary = $rows["tt_beneficiary"];
					$tt_date = $rows["tt_date"];
					$tt_country = $rows["tt_country"];
					$tt_bank_from = $rows["tt_bank_from"];
					$tt_bank_to	= $rows["tt_bank_to"];	
					$tt_bank_to_address	= $rows["tt_bank_to_address"];
					$tt_bank_to_account_number	= $rows["tt_bank_to_account_number"];										
					$tt_currency = $rows["tt_currency"];
					$tt_amount = $rows["tt_amount"];				
					
					$tt_country = getConfigurationValueById($tt_country);
					$tt_currency = getConfigurationValueById($tt_currency);
					$to_bank_account_number = getAccountNumber($tt_bank_to);
					$to_bank_name = getBankName($tt_bank_to);
					
					$from_bank_account_number = getAccountNumber($tt_bank_from);
					$from_bank_name = getBankName($tt_bank_from);
					$tt_bank_from = $from_bank_name." [".$from_bank_account_number."]";					
					
					$tt_amount = numberFormat($tt_amount);
					$id = encrypt($id);
				?>      
				 <tr>
					<td><?php echo $tt_number; ?>&nbsp;</td>
					<td><?php echo $tt_beneficiary; ?>&nbsp;</td>
					<td><?php echo $tt_date; ?>&nbsp;</td>
					<td><?php echo $tt_country; ?>&nbsp;</td>
					<td><?php echo $tt_bank_from; ?>&nbsp;</td>
					<td><?php echo $tt_bank_to." [".$tt_bank_to_account_number."]"; ?>&nbsp;</td>
					<td><?php echo $tt_currency; ?>&nbsp;</td>
					<td><?php echo $tt_amount; ?>&nbsp;</td>
					<td>
								<a href="javDESCript:void(0);" NAME="View Record" title="View Record" onClick=window.open("telegraphic.transfer.details.php?id=<?php echo $id; ?>&tt_number=<?php echo md5($tt_number); ?>","Ratting","toolbar=no,resizable=no,scrollbars=yes,height=650,width=1024");>View</a>
						  <?php
						  if ($_SESSION["level"]==1 || $_SESSION["level"]==5)
						  {
						  ?>		
								<br><a href="telegraphic.transfer.edit.php?id=<?php echo $id; ?>">Edit Record</a>
						  <?php
						  }
						  ?>
						  <?php
						  if ($_SESSION["level"]==1)
						  {
						  ?>
								<br><a href="telegraphic.transfer.list.php?id=<?php echo $id; ?>&action=confirm_delete">Delete</a>							
						  <?php
						  }
						  ?>
					</td>
				<?php 
				} 
				?>
			</tr>
			</tbody>
			<?php 
			} 
			?>
	  </table>
	<!-- END OF TABLE-->	
	</td>
</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>