<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$rec_id = $_GET['id'];
$rec_cv_number = $_GET['cv_number'];
$display_msg = 0;
$rec_id = decrypt($rec_id);
$rec_cv_number = decrypt($rec_cv_number);

$date_time_certify = date("Y-m-d H:i:s");

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE				
				cv_id,
				cv_number,
				cv_type,
				cv_currency_code,
				cv_date,
				cv_payee,
				cv_description,
				cv_obligated_by,
				cv_obligated_by_date,				
				cv_certified_by,
				cv_certified_by_date,
				cv_approved_by,
				cv_approved_by_date
		FROM 	tbl_cash_voucher
		WHERE	cv_id = '$rec_id'
		AND		cv_number = '$rec_cv_number'";
$rs = mysql_query($sql) or die("Error in sql1 in module: cash.voucher.certify.php ".$sql." ".mysql_error());
$total_rows = mysql_num_rows($rs);	
if($total_rows == 0)
{
	insertEventLog($user_id,"User trying to manipulate the page: cash.voucher.certify.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
		window.location = '/workspaceGOP/index.php?msg=4'
	</script>
	<?php
}
else
{
	$rows = mysql_fetch_array($rs);
	$record_id = $rows["cv_id"];
	$cv_number = $rows["cv_number"];
	$cv_type = $rows["cv_type"];
	$cv_currency_code = $rows["cv_currency_code"];
	$cv_date = $rows["cv_date"];
	$cv_payee = $rows["cv_payee"];
	$cv_description = $rows["cv_description"];
	$cv_prepared_by = $rows["cv_prepared_by"];
	$cv_obligated_by = $rows["cv_obligated_by"];
	$cv_obligated_by_date = $rows["cv_obligated_by_date"];	
	$cv_certified_by = $rows["cv_certified_by"];
	$cv_certified_by_date = $rows["cv_certified_by_date"];
	$cv_approved_by = $rows["cv_approved_by"];
	$cv_approved_by_date =$rows ["cv_approved_by_date"];
	
	$cv_payee = getPayeeById($cv_payee);
	$cv_currency_name = getConfigurationDescriptionById($cv_currency_code);
	$cv_currency_code = getConfigurationValueById($cv_currency_code);
	
	
	if ($_POST['Submit'] == 'Update Record')
	{
		$msg = "";
		
		$record_id = $_POST['record_id'];
		$cv_certified_by = $_POST['cv_certified_by'];
		$cv_approved_by = $_POST['cv_approved_by'];
		$cv_obligated_by = $_POST['cv_obligated_by'];
		
		if ($cv_obligated_by==0) 
		{
			$cv_certified_by=0; 
			$display_msg = 1; 
			$msg .= "You cannot certify this record because this has not been obligated."; $msg .= "<br>"; 
		}		
		if ($cv_approved_by!=0) 
		{ 
			$cv_certified_by=0; 
			$display_msg = 1; 
			$msg .= "You cannot certify/un-certify this record because this has been approved."; $msg .= "<br>"; 
		}
		
		if ($msg=="" && $cv_certified_by == 1)
		{
			$sql = "UPDATE	tbl_cash_voucher
					SET		cv_certified_by = '$user_id',
							cv_certified_by_date = '$date_time_certify'
					WHERE	cv_id = '$record_id'";
			mysql_query($sql) or die("Error in $sql in module: cash.voucher.certify.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
		}
		if ($msg=="" && $cv_certified_by == 0)
		{
			$sql = "UPDATE	tbl_cash_voucher
					SET		cv_certified_by = '0',
							cv_certified_by_date = '0000-00-00 00:00:00'
					WHERE	cv_id = '$record_id'";
			mysql_query($sql) or die("Error in $sql in module: cash.voucher.recommend.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);
		}	
		if ($msg == "")
		{
			header("location: /workspaceGOP/page.transactions/cash.voucher.list.php?msg=1");
		}
	}
	
	?>
	<?php include("./../includes/menu.php"); ?>
	<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
		<tr>
			<td>
				<form name="cash_voucher" method="post">
					<table id="rounded-add-entries" align="center">
						<thead>
							<tr>
								<th scope="col" colspan="2" class="rounded-header"><div class="main" align="left"><strong>Cash Voucher [Certify]</strong></div></th>
								<th scope="col" colspan="2" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td class="rounded-foot-left" colspan="2" align="left"><input type="submit" name="Submit" value="Update Record" class="formbutton"></td>
								<td class="rounded-foot-right" colspan="2" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>" class="formbutton"></td>
							</tr>
						</tfoot>
						<tbody>
						<?php
						if ($display_msg==1)
						{
						?>
							<tr>
								 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
							</tr>
							<?php
						}
						?>
						  <tr>
							<td width="10%" scope="col"><b>CV Number</b></td>
							<td width="40%" scope="col"><?php echo $cv_number; ?>
								<input name="cv_number" type="hidden" class="formbutton" id="cv_number" value="<?php echo $cv_number; ?>" />
							</td>
							<td width="10%" scope="col"><b>Report Type</b></td>
							<td width="40%" scope="col"><?php echo $cv_type; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Currency</b></td>
							<td scope="col"><?php echo $cv_currency_name; ?></td>
							<td scope="col"><b>Date</b></td>
							<td scope="col"><?php echo $cv_date; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Payee</b></td>
							<td colspan="3" scope="col"><? echo $cv_payee; ?></td>
						  </tr>
						  <tr>
							<td valign="top" scope="col"><b>Particulars/Description</b></td>
							<td colspan="3" valign="top" scope="col"><?php echo $cv_description; ?></td>
						  </tr>
						  <tr>
							<td colspan="4" valign="top" scope="col">
	
								<table id="rounded-add-entries" align="center">
								  <thead>
									<tr>
									  <th scope="col" class="rounded-header" align="center"><b>Account Code</b></th>
									  <th scope="col" align="center"><b>Account Title</b></th>
									   <th scope="col" align="center"><b>Currency</b></th>
									  <th scope="col" align="center"><b>Debit</b></th>
									  <th scope="col" align="center"><b>Credit</b></th>
									  <th scope="col" align="center"><b>Budget ID</b></th>
									  <th scope="col" align="center"><b>Donor ID</b></th>
									  <th scope="col" align="center"><b>Component ID</b></th>
									  <th scope="col" align="center"><b>Activity ID</b></th>
									  <th scope="col" align="center"><b>Other Cost/Services</b></th>
									  <th scope="col" align="center"><b>Staff ID</b></th>
									  <th scope="col" align="center"><b>Benefits ID</b></th>
									  <th scope="col" align="center"><b>Vehicles ID</b></th>
									  <th scope="col" class="rounded-q4" align="center"><b>Equipment ID</b></th>
									</tr>
								  </thead>
								  <tbody>
									<?php
									$sql = "	SELECT 	SQL_BUFFER_RESULT
														SQL_CACHE
														cv_details_id,
														cv_details_account_code,
														cv_details_reference_number,
														cv_details_debit,
														cv_details_credit,
														cv_details_budget_id,
														cv_details_donor_id,
														cv_details_component_id,
														cv_details_activity_id,
														cv_details_other_cost_id,
														cv_details_staff_id,
														cv_details_benefits_id,
														cv_details_vehicle_id,
														cv_details_equipment_id
												FROM 	tbl_cash_voucher_details
												WHERE	cv_details_reference_number = '$record_id'";
									$rs = mysql_query($sql) or die("Error in sql2 in module: cash.voucher.details.add.php ".$sql." ".mysql_error());
									$total_rows = mysql_num_rows($rs);	
									if($total_rows == 0)
									{
										?>
										<tr>
										  <td colspan="14"><div class="redlabel" align="left">No records found!</div></td>
										</tr>
										<?php
									}
									else
									{
										for($row_number=0;$row_number<$total_rows;$row_number++)
										{
											$rows = mysql_fetch_array($rs);
											$cv_details_id = $rows["cv_details_id"];
											$cv_details_reference_number = $rows["cv_details_reference_number"];
											$cv_details_account_code = $rows["cv_details_account_code"];
											$cv_details_debit = $rows["cv_details_debit"];
											$cv_details_credit = $rows["cv_details_credit"];
											$cv_details_budget_id = $rows["cv_details_budget_id"];
											$cv_details_donor_id = $rows["cv_details_donor_id"];
											$cv_details_component_id = $rows["cv_details_component_id"];
											$cv_details_activity_id = $rows["cv_details_activity_id"];
											$cv_details_other_cost_id = $rows["cv_details_other_cost_id"];
											$cv_details_staff_id = $rows["cv_details_staff_id"];
											$cv_details_benefits_id = $rows["cv_details_benefits_id"];
											$cv_details_vehicle_id = $rows["cv_details_vehicle_id"];
											$cv_details_equipment_id = $rows["cv_details_equipment_id"];
											
											
											$cv_details_account_title = getSubsidiaryLedgerAccountTitleByid($cv_details_account_code);
											$cv_details_account_code = getSubsidiaryLedgerAccountCodeByid($cv_details_account_code);
											
											$cv_details_budget_id  = getBudgetName($cv_details_budget_id);
											$cv_details_donor_id  = getDonorName($cv_details_donor_id);
											$cv_details_component_id  = getComponentName($cv_details_component_id);
											$cv_details_activity_id = getActivityName($cv_details_activity_id);
											$cv_details_other_cost_id  = getOtherCostName($cv_details_other_cost_id);
											$cv_details_staff_id  = getStaffName($cv_details_staff_id);
											$cv_details_benefits_id  = getBenefitsName($cv_details_benefits_id);
											$cv_details_vehicle_id  = getVehicleName($cv_details_vehicle_id);
											$cv_details_equipment_id  = getEquipmentName($cv_details_equipment_id);
											
											$arr_debit[] = $cv_details_debit;
											$arr_credit[] = $cv_details_credit;
										?>
									<tr>
									  <td><?php echo $cv_details_account_code; ?></td>
									  <td><?php echo $cv_details_account_title; ?></td>
									  <td><?php echo $cv_currency_code; ?></td>
									  <td align="right"><?php echo numberFormat($cv_details_debit); ?></td>
									  <td align="right"><?php echo numberFormat($cv_details_credit); ?></td>
									  <td><?php echo $cv_details_budget_id; ?></td>
									  <td><?php echo $cv_details_donor_id; ?></td>
									  <td><?php echo $cv_details_component_id; ?></td>
									  <td><?php echo $cv_details_activity_id; ?></td>
									  <td><?php echo $cv_details_other_cost_id; ?></td>
									  <td><?php echo $cv_details_staff_id; ?></td>
									  <td><?php echo $cv_details_benefits_id; ?></td>
									  <td><?php echo $cv_details_vehicle_id; ?></td>
									  <td><?php echo $cv_details_equipment_id; ?></td>
									  <?php 
										} 
										?>
									</tr>
								  </tbody>
								  <?php 
									} 
									if ($arr_credit != 0 && $arr_debit != 0)
									{
										$total_debit = array_sum($arr_debit);
										$total_credit = array_sum($arr_credit);
									}
									?>
								  <tfoot>
									<tr>
									  <td class="rounded-foot-left" colspan="3" align="right"><b>Total :</b></td>
									  <td align="right"><?php echo numberFormat($total_debit); ?> </td>
									  <td align="right"><?php echo numberFormat($total_credit);  ?> </td>
									  <td class="rounded-foot-right" colspan="11" align="right">&nbsp;</td>
									</tr>
								  </tfoot>								
								</table>
	
							</td>
						  </tr>							  
						  <tr>
							<td colspan="4" valign="top" scope="col">
                            	<input type="hidden" name="cv_obligated_by" value="<?php echo $cv_obligated_by; ?>">
								<input type="hidden" name="cv_approved_by" value="<?php echo $cv_approved_by; ?>">
							  	<input name="cv_certified_by" type="checkbox" id="cv_certified_by" value="1" <?php if ($cv_certified_by!=0) { echo "CHECKED";} ?>/>
								* Certify this record.</td>
						  </tr>					  					  					  
			
						</tbody>
					</table>
				</form>
			</td>
		</tr>
	</table>
	<?php include("./../includes/footer.main.php"); ?>
<?php
}
?>