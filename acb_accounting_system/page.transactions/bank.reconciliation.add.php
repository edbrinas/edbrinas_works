<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$display_msg = 0;

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$year = date("Y");
$month = date("m");
$day = date("d");
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;
$date_time_inserted = date("Y-m-d H:i:s");

if ($_POST['Submit'] == 'Save')
{
	$msg = "";
	
	$transaction_year = $_POST['transaction_year'];
	$transaction_month = $_POST['transaction_month'];
	$transaction_day = $_POST['transaction_day'];
	$transaction_type = $_POST['transaction_type'];
	$transaction_description = $_POST['transaction_description'];
	$transaction_amount = $_POST['transaction_amount'];
	$transaction_bank_id = $_POST['transaction_bank_id'];

	$transaction_date = dateFormat($transaction_month,$transaction_day,$transaction_year);
	
	if ($transaction_type == "") { $display_msg=1; $msg .= "Please select transaction type"; $msg .="<br>"; }
	if ($transaction_description == "") { $display_msg=1; $msg .= "Please enter description"; $msg .="<br>"; }
	if ($transaction_amount == "") { $display_msg=1; $msg .= "Please enter amount"; $msg .="<br>"; }
	if ($transaction_bank_id == "") { $display_msg=1; $msg .= "Please select bank id"; $msg .="<br>"; }
	if (checkIfNumber($transaction_amount) == 0) { $display_msg=1; $msg .= "Transaction amount must be a number"; $msg .="<br>"; }	
	
	if ($msg=="")
	{
			$sql = "INSERT INTO tbl_other_transactions 
								(
									transaction_bank_id,
									transaction_date,
									transaction_description,
									transaction_type,
									transaction_amount
								)
					VALUES 		(
									'$transaction_bank_id',
									'$transaction_date',
									'$transaction_description',
									'$transaction_type',
									'$transaction_amount'
								)";
			mysql_query($sql) or die("Error inserting values ".$sql." ".mysql_error());
			insertEventLog($user_id,"Inserted $transaction_bank_id,$transaction_date,$transaction_description,$transaction_type,$transaction_amount to tbl_other_transactions");
			
			$display_msg=1;
			$msg = "Record added successfully";
			
			$transaction_year = "";
			$transaction_month = "";
			$transaction_day = "";
			$transaction_type = "";
			$transaction_description = "";
			$transaction_amount = "";
			$transaction_bank_id = "";
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="cash_voucher" method="post">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th width="20%" class="rounded-header" scope="col"><div class="main" align="left"><strong>Bank Reconciliation Details [Add Record]</strong></div></th>
						  	<th width="80%" class="rounded-q4" scope="col"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Save" class="formbutton"></td>
							<td class="rounded-foot-right" align="right">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
				      <tr>
						<td scope="col"><b>Date*</b></td>
						<td scope="col">
							<select name='transaction_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='transaction_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='transaction_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day )
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>						</td>
					  </tr>
					  <tr>
						<td scope="col"><b>Type*</b></td>
						<td scope="col">
								<?php
									$sql1 = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_id,
													cfg_value
											FROM 	tbl_config
											WHERE	cfg_name = 'bank_reconciliation'
											ORDER BY cfg_value";
									$rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
									?>
									<select name='transaction_type' class='formbutton'>
									<option value=''>[SELECT TRANSACTION]
									<?php
									while($data1=mysql_fetch_array($rs1))
									{
										$column_id = $data1['cfg_id'];
										$column_name = $data1['cfg_value'];
										?>
										<option value='<?php echo $column_id; ?>'
										<?php
										if ($transaction_type == $column_id)
										{
											echo " SELECTED";
										}
										?>
										><?php echo $column_name; ?></option>
										<?php
									}
									?>
									</select>						</td>
					  </tr>		
					  <tr>
					    <td scope="col" valign="top"><b>Bank Name*</b></td>
					    <td scope="col">
                        	<?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											bank_id,
											bank_name,
											bank_account_number
									FROM 	tbl_bank
									ORDER BY bank_name ASC";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?> 
							<select name='transaction_bank_id' class='formbutton'>
							<?php
							while($data=mysql_fetch_array($rs))
							{
								$column_id = $data['bank_id'];
								$column_name = $data['bank_name'];
								$column_description = $data['bank_account_number'];
								?>
								<option value='<?php echo $column_id; ?>' <?php if ($transaction_bank_id == $column_id) { echo "SELECTED"; } ?>><?php echo $column_name." [ ".$column_description." ]"; ?></option>
								<?php
							}
							?>
							</select>
                        </td>
				      </tr>
					  <tr>
					    <td scope="col" valign="top"><b>Description *</b></td>
					    <td scope="col"><textarea name="transaction_description" cols="40" rows="5" class="formbutton"><?php echo $transaction_description; ?></textarea></td>
				      </tr>		
					  <tr>
						<td scope="col"><b>Amount*</b></td>
						<td scope="col"><input type="text" name="transaction_amount" value="<?php echo $transaction_amount; ?>" class="formbutton"></td>
					  </tr>
					</tbody>
				</table>
		  </form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>