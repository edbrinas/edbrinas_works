<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$id = $_GET['id'];
$display_msg = 0;
$cv_type = "CV";
$id = decrypt($id);

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$startYear = date("Y") - 1;
$endYear = date("Y") + 2;
$date_time_inserted = date("Y-m-d H:i:s");

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE				
				cv_id,
				cv_year,
				cv_sequence_number,
				cv_budget_id,
				cv_number,
				cv_type,
				cv_currency_code,
				cv_date,
				cv_payee,
				cv_description,
				cv_certified_by,
				cv_certified_by_date,
				cv_approved_by,
				cv_approved_by_date
		FROM 	tbl_cash_voucher
		WHERE	cv_id = '$id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: cash.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$record_id = $rows["cv_id"];
$cv_number = $rows["cv_number"];
$cv_date = $rows["cv_date"];
$cv_payee = $rows["cv_payee"];
$cv_description = $rows["cv_description"];
$cv_year = $rows["cv_year"];
$cv_sequence_number = $rows["cv_sequence_number"];
$cv_budget_id = $rows["cv_budget_id"];
$cv_prepared_by = $rows["cv_prepared_by"];
$cv_certified_by = $rows["cv_certified_by"];
$cv_certified_by_date = $rows["cv_certified_by_date"];
$cv_approved_by = $rows["cv_approved_by"];
$cv_approved_by_date =$rows ["cv_approved_by_date"];
$cv_description = stripslashes($cv_description);

$chunk_date = explode("-",$cv_date);
$year = $chunk_date[0];
$month = $chunk_date[1];
$day = $chunk_date[2];

if ($_POST['Submit'] == 'Update Details')
{
	$msg = "";
	
	$record_id = $_POST['record_id'];
	$cv_budget_id = $_POST['cv_budget_id'];
	$cv_number = $_POST['cv_number'];
	$cv_year = $_POST['cv_year'];
	$cv_month = $_POST['cv_month'];
	$cv_day = $_POST['cv_day'];
	$cv_payee = $_POST['cv_payee'];
	$cv_description = $_POST['cv_description'];
	$cv_description = addslashes($cv_description);
	
	if ($cv_budget_id==1) { $donor_name = "E"; }
	elseif ($cv_budget_id==2) { $donor_name = "A"; }
	
	$arr_old_budget_id = stringtoarray($cv_number);
	$cur_old = $arr_old_budget_id[0];

	if ($cur_old!=$donor_name)
	{
		if ($cv_budget_id==1 || $cv_budget_id==2)
		{
			$cv_number = getCashVoucherSequenceNumber($cv_budget_id,$cv_month,$cv_year);
			$cv_sequence_number = substr($cv_number,-4);
			$set_new_cv = 1;
		}
		else
		{
			$display_msg=1; 
			$msg .= "Invalid budget id, Please contact your administrator!"; 
			$msg .="<br>";
		}
	}	

	$cv_date = dateTimeFormat($hr="",$min="",$sec="",$cv_month,$cv_day,$cv_year);
	
	if ($cv_description == "") { $display_msg=1; $msg .= "Please enter Description"; $msg .="<br>"; }
	
	if ($msg=="")
	{
		$sql = "UPDATE 	tbl_cash_voucher
				SET		cv_date = '".trim($cv_date)."',
						cv_payee = '".trim($cv_payee)."',
						cv_description = '".trim($cv_description)."' ";
		if ($set_new_cv == 1)
		{
			$sql .= ",";
			$sql .= "cv_budget_id = '".trim($cv_budget_id)."',";
			$sql .= "cv_number = '".trim($cv_number)."',";
			$sql .= "cv_sequence_number = '".trim($cv_sequence_number)."' ";
		}
		$sql .="WHERE	cv_id = '$record_id'";
		mysql_query($sql) or die("Error inserting values : module : cash.voucher.add.php ".$sql." ".mysql_error());
		insertEventLog($user_id,$sql);
		$cv_number = encrypt($cv_number);
		$record_id = encrypt($record_id);
		header("location:/workspaceGOP/page.transactions/cash.voucher.add.details.php?cv_id=$record_id&cv_number=$cv_number");
	}
}

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
			<form name="cash_voucher" method="post">
			  <table id="rounded-add-entries" align="center">
                <thead>
                  <tr>
                    <th scope="col" class="rounded-header"><div class="main" align="left"><strong>Cash Voucher [Edit Record]</strong></div></th>
                    <th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <td class="rounded-foot-left" align="left"><input type="submit" name="Submit" value="Update Details" class="formbutton" /></td>
                    <td class="rounded-foot-right" align="right"><input type="hidden" name="record_id" id="record_id" value="<?php echo $record_id; ?>"></td>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
					if ($display_msg==1)
					{
					?>
                  <tr>
                    <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
                  </tr>
                  <?php
					}
					?>
                  <tr>
                    <td width="20%" scope="col"><b>CV Number</b></td>
                    <td width="80%" scope="col">
						<input type="hidden" name="cv_number" id="cv_number" value="<?php echo $cv_number; ?>">
						<?php echo $cv_number; ?>
					</td>
                  </tr>
                  <tr>
                    <td scope="col"><b>Budget ID*</b></td>
                    <td scope="col"><?php
					echo $cv_budget_id ." ".$column_id;
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											budget_id,
											budget_name,
											budget_description
									FROM 	tbl_budget";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?>
                        <select name='cv_budget_id' class='formbutton'>
                          <?php
							while($data=mysql_fetch_array($rs))
							{
								$column_id = $data['budget_id'];
								$column_name = $data['budget_name'];
								$column_description = $data['budget_description'];
								?>
                         			<option value='<?php echo $column_id; ?>'
								<?php
								if ($cv_budget_id == $column_id)
								{
									?>
									SELECTED
								<?php
								}
								?>
								><?php echo $column_name; ?></option>
								<?php
							}
							?> 
                        </select>                    </td>
                  </tr>
                  <tr>
                    <td scope="col"><b>Date*</b></td>
                    <td scope="col"><select name='cv_year' class="formbutton">
                        <?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
                        <option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
                        <?php
								}
								?>
                        </option>
                      </select>
                        <select name='cv_month' class="formbutton">
                          <?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
                          <option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
                          <?php
								}
								?>
                          </option>
                        </select>
                        <select name='cv_day' class="formbutton">
                          <?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
                          <option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day )
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
                          <?php
								}
								?>
                          </option>
                        </select>                    </td>
                  </tr>
                  <tr>
                    <td scope="col"><b>Payee*</b></td>
                    <td scope="col"><?php
									$sql1 = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													payee_id,
													payee_name
											FROM 	tbl_payee
											ORDER BY payee_name";
									$rs1 = mysql_query($sql1) or die('Error ' . mysql_error()); 
									?>
									<select name='cv_payee' class='formbutton'>
									<option value=''>[SELECT PAYEE]
									<?php
									while($data1=mysql_fetch_array($rs1))
									{
										$column_id = $data1['payee_id'];
										$column_name = $data1['payee_name'];
										?>
										<option value='<?php echo $column_id; ?>'
										<?php
										if ($cv_payee == $column_id)
										{
											?>
											SELECTED
											<?php
										}
										?>
										><?php echo $column_name; ?></option>
										<?php
									}
									?></select>&nbsp;</td>
                  </tr>
                  <tr>
                    <td valign="top" scope="col"><b>Particulars/Description*</b></td>
                    <td valign="top" scope="col"><label>
                      <textarea name="cv_description" cols="40" rows="5" class="formbutton"><?php echo $cv_description; ?></textarea>
                    </label></td>
                  </tr>
                </tbody>
              </table>
			</form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>