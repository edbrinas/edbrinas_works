<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];
$rec_id = $_GET['id'];
$rec_cv_number = $_GET['cv_number'];
$display_msg = 0;
$rec_id = decrypt($rec_id);
$rec_cv_number = decrypt($rec_cv_number);

$date_time_obligated = date("Y-m-d H:i:s");

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE				
				cv_id,
				cv_number,
				cv_type,
				cv_currency_code,
				cv_date,
				cv_payee,
				cv_description,
				cv_obligated_by,
				cv_certified_by,
				cv_certified_by_date,
				cv_recommended_by,
				cv_recommended_by_date,
				cv_approved_by,
				cv_approved_by_date
		FROM 	tbl_cash_voucher
		WHERE	cv_id = '$rec_id'
		AND		cv_number = '$rec_cv_number'";
$rs = mysql_query($sql) or die("Error in sql1 in module: cash.voucher.obligate.php ".$sql." ".mysql_error());
$total_rows = mysql_num_rows($rs);	
if($total_rows == 0)
{
	insertEventLog($user_id,"User trying to manipulate the page: cash.voucher.obligate.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!")
		window.location = '/workspaceGOP/index.php?msg=4'
	</script>
	<?php
}
else
{
	$rows = mysql_fetch_array($rs);
	$record_id = $rows["cv_id"];
	$cv_number = $rows["cv_number"];
	$cv_type = $rows["cv_type"];
	$cv_currency_code = $rows["cv_currency_code"];
	$cv_date = $rows["cv_date"];
	$cv_payee = $rows["cv_payee"];
	$cv_description = $rows["cv_description"];
	$cv_prepared_by = $rows["cv_prepared_by"];
	$cv_obligated_by = $rows["cv_obligated_by"];
	$cv_certified_by = $rows["cv_certified_by"];
	$cv_certified_by_date = $rows["cv_certified_by_date"];
	$cv_recommended_by =$rows["cv_recommended_by"];
	$cv_recommended_by_date = $rows["cv_recommended_by_date"];
	$cv_approved_by = $rows["cv_approved_by"];
	$cv_approved_by_date =$rows ["cv_approved_by_date"];
	
	$cv_payee = getPayeeById($cv_payee);
	$cv_description = stripslashes($cv_description);
	$arr_currency_code = $cv_currency_code;
	$cv_currency_name = getConfigurationDescriptionById($cv_currency_code);
	$cv_currency_code = getConfigurationValueById($cv_currency_code);
	
	if ($_POST['Submit'] == 'Update Record')
	{
		$msg = "";
		
		$cv_number = $_POST["cv_number"];
		$record_id = $_POST['record_id'];
		$bc_id = $_POST['bc_id'];
		$cv_details_id = $_POST['cv_details_id'];
		$cv_obligated_by = $_POST['cv_obligated_by'];
		$cv_certified_by = $_POST['cv_certified_by'];
		$cv_approved_by = $_POST['cv_approved_by'];
		$cv_recommended_by = $_POST['cv_recommended_by'];
		$cv_currency_code = $_POST['cv_currency_code'];
		$cv_date = $_POST['cv_date'];
		$cv_obligate_debit = $_POST['cv_obligate_debit'];
		$cv_obligate_credit = $_POST['cv_obligate_credit'];
		$cv_description = $_POST['cv_description'];

		$cv_details_account_code = $_POST['cv_details_account_code'];
		$cv_details_budget_id = $_POST['cv_details_budget_id'];
		$cv_details_donor_id = $_POST['cv_details_donor_id'];
		$cv_details_component_id = $_POST['cv_details_component_id'];
		$cv_details_activity_id = $_POST['cv_details_activity_id'];
		$cv_details_other_cost_id = $_POST['cv_details_other_cost_id'];
		$cv_details_staff_id = $_POST['cv_details_staff_id'];
		$cv_details_benefits_id = $_POST['cv_details_benefits_id'];
		$cv_details_vehicle_id = $_POST['cv_details_vehicle_id'];
		$cv_details_equipment_id = $_POST['cv_details_equipment_id'];
		$cv_details_item_id = $_POST['cv_details_item_id'];	
		
		if (count($cv_obligate_debit)==0) { $cv_obligated_by=0; $display_msg = 1; $msg .= "Please enter debit obligate value"; $msg .= "<br>"; }
		if (count($cv_obligate_credit)==0) { $cv_obligated_by=0; $display_msg = 1; $msg .= "Please enter credit obligate value"; $msg .= "<br>"; }
		if ($cv_certified_by!=0) { $cv_obligated_by=0; $display_msg = 1; $msg .= "You cannot obligate this record because this has been certified."; $msg .= "<br>"; }
		if ($cv_recommended_by!=0) { $cv_obligated_by=0; $display_msg = 1; $msg .= "You cannot obligate this record because this has been recommended."; $msg .= "<br>"; }
		if ($cv_approved_by!=0) { $cv_obligated_by=0; $display_msg = 1; $msg .= "You cannot obligate this record because this has been approved."; $msg .= "<br>"; }
		
		if ($cv_obligated_by==1 && $msg=="")
		{
			for ($x=0;$x<=count($cv_details_account_code)-1;$x++)
			{
				$a = $x+1;
				
				$sl_budget_id[$x] = checkSubsidiaryLedgerRequirement($cv_details_account_code[$x],"sl_budget_id");
				$sl_donor_id[$x] = checkSubsidiaryLedgerRequirement($cv_details_account_code[$x],"sl_donor_id");
				$sl_component_id[$x] = checkSubsidiaryLedgerRequirement($cv_details_account_code[$x],"sl_component_id");
				$sl_activity_id[$x] = checkSubsidiaryLedgerRequirement($cv_details_account_code[$x],"sl_activity_id");
				$sl_other_cost_id[$x] = checkSubsidiaryLedgerRequirement($cv_details_account_code[$x],"sl_other_cost_id");
				$sl_staff_id[$x] = checkSubsidiaryLedgerRequirement($cv_details_account_code[$x],"sl_staff_id");
				$sl_benefits_id[$x] = checkSubsidiaryLedgerRequirement($cv_details_account_code[$x],"sl_benefits_id");
				$sl_vehicle_id[$x] = checkSubsidiaryLedgerRequirement($cv_details_account_code[$x],"sl_vehicle_id");
				$sl_equipment_id[$x]  = checkSubsidiaryLedgerRequirement($cv_details_account_code[$x],"sl_equipment_id");
				$sl_item_id[$x]  = checkSubsidiaryLedgerRequirement($cv_details_account_code[$x],"sl_item_id");	

				if ($sl_budget_id[$x] == 0) { $cv_details_budget_id[$x] = ""; }
				if ($sl_donor_id[$x] == 0) { $cv_details_donor_id[$x] = ""; }
				if ($sl_component_id[$x] == 0) { $cv_details_component_id[$x] = ""; }
				if ($sl_activity_id[$x] == 0) { $cv_details_activity_id[$x] = ""; }
				if ($sl_other_cost_id[$x] == 0) { $cv_details_other_cost_id[$x] = ""; }
				if ($sl_staff_id[$x] == 0) { $cv_details_staff_id[$x] = ""; }
				if ($sl_benefits_id[$x] == 0) { $cv_details_benefits_id[$x] = ""; }
				if ($sl_vehicle_id[$x] == 0) { $cv_details_vehicle_id[$x] = ""; }
				if ($sl_equipment_id[$x] == 0) { $cv_details_equipment_id[$x] = ""; }
				if ($sl_item_id[$x] == 0) { $cv_details_item_id[$x] = ""; }
			
				if ($sl_budget_id[$x] == 1 && $cv_details_budget_id[$x] =="") 
				{  
					$display_msg=1; 
					$msg .= "Error in record number: ".$a."<br>";
					$msg .= "Please select Budget ID, this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($cv_details_account_code[$x]); 
					$msg .="<br>"; 
					break;
				}
				if ($sl_donor_id[$x] == 1 && $cv_details_donor_id[$x] == "") 
				{  
					$display_msg=1; 
					$msg .= "Error in record number: ".$a."<br>";
					$msg .= "Please select Donor ID, this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($cv_details_account_code[$x]);   
					$msg .="<br>"; 
					break;
				}
				if ($sl_component_id[$x] == 1 && $cv_details_component_id[$x] == "") 
				{  
					$display_msg=1; 
					$msg .= "Error in record number: ".$a."<br>";
					$msg .= "Please select Component ID, this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($cv_details_account_code[$x]);  
					$msg .="<br>"; 
					break;
				}
				if ($sl_activity_id[$x] == 1 && $cv_details_activity_id[$x] == "") 
				{  
					$display_msg=1; 
					$msg .= "Error in record number: ".$a."<br>";
					$msg .= "Please select Activity ID, this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($cv_details_account_code[$x]);  
					$msg .="<br>"; 
					break;
				}
				if ($sl_other_cost_id[$x] == 1 && $cv_details_other_cost_id[$x] == "") 
				{  
					$display_msg=1; 
					$msg .= "Error in record number: ".$a."<br>";
					$msg .= "Please select Other Cost/Services ID, this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($cv_details_account_code[$x]); 
					$msg .="<br>"; 
					break;
				}
				if ($sl_staff_id[$x] == 1 && $cv_details_staff_id[$x] == "") 
				{  
					$display_msg=1; 
					$msg .= "Error in record number: ".$a."<br>";
					$msg .= "Please select Staff ID, this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($cv_details_account_code[$x]);  
					$msg .="<br>"; 
					break;
				}
				if ($sl_benefits_id[$x] == 1 && $cv_details_benefits_id[$x] == "") 
				{  
					$display_msg=1; 
					$msg .= "Error in record number: ".$a."<br>";
					$msg .= "Please select Benefits ID, this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($cv_details_account_code[$x]); 
					$msg .="<br>"; 
					break;
				}
				if ($sl_vehicle_id[$x] == 1 && $cv_details_vehicle_id[$x] == "") 
				{  
					$display_msg=1; 
					$msg .= "Error in record number: ".$a."<br>";
					$msg .= "Please select Vehicle ID, this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($cv_details_account_code[$x]);  
					$msg .="<br>"; 
					break;
				}
				if ($sl_equipment_id[$x] == 1 && $cv_details_equipment_id[$x] == "") 
				{  
					$display_msg=1; 
					$msg .= "Error in record number: ".$a."<br>";
					$msg .= "Please select Details ID, this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($cv_details_account_code[$x]);  
					$msg .="<br>"; 
					break;
				}
				if ($sl_item_id[$x] == 1 && $cv_details_item_id[$x] == "") 
				{  
					$display_msg=1; 
					$msg .= "Error in record number: ".$a."<br>";
					$msg .= "Please select ITEM ID, this is required in account code : ".getSubsidiaryLedgerAccountCodeByid($cv_details_account_code[$x]); 
					$msg .="<br>"; 
					break;
				}
				
				if ($msg=="")
				{
					$arr_sql[] = array("bc_voucher_type" => "CV",
									   "bc_voucher_number" => $cv_number,
									   "bc_voucher_id"	 => $record_id,
									   "bc_id" => $bc_id[$x],
									   "bc_voucher_details_id" => $cv_details_id[$x],
									   "bc_date" => $cv_date,
									   "bc_account_code" => $cv_details_account_code[$x],
									   "bc_description" => $cv_description,
									   "bc_currency_code" => $cv_currency_code,
									   "bc_debit" => $cv_obligate_debit[$x],
									   "bc_credit" => $cv_obligate_credit[$x],
									   "bc_budget_id" => $cv_details_budget_id[$x],
									   "bc_donor_id" => $cv_details_donor_id[$x],
									   "bc_component_id" => $cv_details_component_id[$x],
									   "bc_activity_id" => $cv_details_activity_id[$x],
									   "bc_other_cost_id" => $cv_details_other_cost_id[$x],
									   "bc_staff_id" => $cv_details_staff_id[$x],
									   "bc_benefits_id" => $cv_details_benefits_id[$x],
									   "bc_vehicle_id" => $cv_details_vehicle_id[$x],
									   "bc_equipment_id" => $cv_details_equipment_id[$x],
									   "bc_item_id" => $cv_details_item_id[$x],
									   "bc_inserted_by" => $user_id);
				}				
			}
			
			if ($msg=="")
			{
				foreach ($arr_sql as $key=>$value)
				{
					$sql = "SELECT 	bc_id
							FROM 	tbl_budget_controller
							WHERE	bc_voucher_type = '".$arr_sql[$key]["bc_voucher_type"]."'
							AND		bc_voucher_id = '".$arr_sql[$key]["bc_voucher_id"]."'
							AND		bc_id = '".$arr_sql[$key]["bc_id"]."'";
					$rs = mysql_query($sql) or die("Error in $sql in module: cash.voucher.obligate.php ".$sql." ".mysql_error());
					$total_rows = mysql_num_rows($rs);	
					if($total_rows == 0)
					{								
						$sql = "INSERT INTO tbl_budget_controller (	bc_voucher_type,
																	bc_voucher_number,
																	bc_voucher_id,
																	bc_voucher_details_id,
																	bc_date,
																	bc_account_code,
																	bc_description,
																	bc_currency_code,
																	bc_debit,
																	bc_credit,
																	bc_budget_id,
																	bc_donor_id,
																	bc_component_id,
																	bc_activity_id,
																	bc_other_cost_id,
																	bc_staff_id,
																	bc_benefits_id,
																	bc_vehicle_id, 
																	bc_equipment_id,
																	bc_item_id, 
																	bc_inserted_by)
								VALUES ('".$arr_sql[$key]["bc_voucher_type"]."',
										'".$arr_sql[$key]["bc_voucher_number"]."',
                                        '".$arr_sql[$key]["bc_voucher_id"]."',
                                        '".$arr_sql[$key]["bc_voucher_details_id"]."',
                                        '".$arr_sql[$key]["bc_date"]."',
                                        '".$arr_sql[$key]["bc_account_code"]."',
										'".addslashes($arr_sql[$key]["bc_description"])."',
                                        '".$arr_sql[$key]["bc_currency_code"]."',
                                        '".$arr_sql[$key]["bc_debit"]."',
                                        '".$arr_sql[$key]["bc_credit"]."',
                                        '".$arr_sql[$key]["bc_budget_id"]."',
                                        '".$arr_sql[$key]["bc_donor_id"]."',
                                        '".$arr_sql[$key]["bc_component_id"]."',
                                        '".$arr_sql[$key]["bc_activity_id"]."',
                                        '".$arr_sql[$key]["bc_other_cost_id"]."',
                                        '".$arr_sql[$key]["bc_staff_id"]."',
                                        '".$arr_sql[$key]["bc_benefits_id"]."',
                                        '".$arr_sql[$key]["bc_vehicle_id"]."',
                                        '".$arr_sql[$key]["bc_equipment_id"]."',
                                        '".$arr_sql[$key]["bc_item_id"]."',
                                        '".$arr_sql[$key]["bc_inserted_by"]."')";
					}
					else
					{
						$sql = "UPDATE 	tbl_budget_controller 
								SET 	bc_voucher_number = '".$arr_sql[$key]["bc_voucher_number"]."',
										bc_account_code = '".$arr_sql[$key]["bc_account_code"]."',
										bc_description = '".addslashes($arr_sql[$key]["bc_description"])."',
										bc_debit = '".$arr_sql[$key]["bc_debit"]."',
										bc_credit = '".$arr_sql[$key]["bc_credit"]."',
										bc_budget_id = '".$arr_sql[$key]["bc_budget_id"]."',
										bc_donor_id = '".$arr_sql[$key]["bc_donor_id"]."',
										bc_component_id = '".$arr_sql[$key]["bc_component_id"]."',
										bc_activity_id = '".$arr_sql[$key]["bc_activity_id"]."',
										bc_other_cost_id = '".$arr_sql[$key]["bc_other_cost_id"]."',
										bc_staff_id = '".$arr_sql[$key]["bc_staff_id"]."',
										bc_benefits_id = '".$arr_sql[$key]["bc_benefits_id"]."',
										bc_vehicle_id = '".$arr_sql[$key]["bc_vehicle_id"]."', 
										bc_equipment_id = '".$arr_sql[$key]["bc_equipment_id"]."',
										bc_item_id = '".$arr_sql[$key]["bc_item_id"]."'										
								WHERE 	bc_id = '".$arr_sql[$key]["bc_id"]."'";
					}
					mysql_query($sql) or die("Error in $sql in module: cash.voucher.obligate.php ".$sql." ".mysql_error());	
					insertEventLog($user_id,$sql);					
					
				}
				$sql = "UPDATE	tbl_cash_voucher
						SET		cv_obligated_by = '$user_id',
								cv_obligated_by_date = '$date_time_obligated'
						WHERE	cv_id = '$record_id'";
				mysql_query($sql) or die("Error in $sql in module: cash.voucher.obligate.php ".$sql." ".mysql_error());
				insertEventLog($user_id,$sql);
				header("location: /workspaceGOP/page.transactions/cash.voucher.list.php?msg=1&sort_by=obligate");
			}
		}
		if ($msg=="" && $cv_obligated_by == 0)
		{
			$sql = "DELETE FROM tbl_budget_controller 
					WHERE	bc_voucher_id = '$record_id' 
					AND		bc_voucher_type = 'CV'";
			mysql_query($sql) or die("Error in $sql in module: cash.voucher.obligate.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);	
				
			$sql = "UPDATE	tbl_cash_voucher
					SET		cv_obligated_by = '0',
							cv_obligated_by_date = '0000-00-00 00:00:00'
					WHERE	cv_id = '$record_id'";
			mysql_query($sql) or die("Error in $sql in module: cash.voucher.obligate.php ".$sql." ".mysql_error());
			insertEventLog($user_id,$sql);		
			header("location: /workspaceGOP/page.transactions/cash.voucher.list.php?msg=1&sort_by=obligate");
		}
	}
	
	?>
	<?php include("./../includes/menu.php"); ?>
	<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
		<tr>
			<td>
				<form name="cash_voucher" method="post">
					<table id="rounded-add-entries" align="center">
						<thead>
							<tr>
								<th scope="col" colspan="2" class="rounded-header"><div class="main" align="left"><strong>Cash Voucher [Obligate]</strong></div></th>
								<th scope="col" colspan="2" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td class="rounded-foot-left" colspan="2" align="left"><input type="submit" name="Submit" value="Update Record" class="formbutton"></td>
								<td class="rounded-foot-right" colspan="2" align="right"><input type="hidden" name="record_id" value="<?php echo $record_id; ?>" ></td>
							</tr>
						</tfoot>
						<tbody>
						<?php
						if ($display_msg==1)
						{
						?>
							<tr>
								 <td colspan="4"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
							</tr>
							<?php
						}
						?>
						  <tr>
							<td width="10%" scope="col"><b>CV Number</b></td>
						  <td width="40%" scope="col"><?php echo $cv_number; ?>
								<input name="cv_number" type="hidden" class="formbutton" id="cv_number" value="<?php echo $cv_number; ?>" />
							</td>
							<td width="10%" scope="col"><b>Report Type</b></td>
							<td width="40%" scope="col"><?php echo $cv_type; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Currency</b></td>
							<td scope="col"><?php echo $cv_currency_name; ?></td>
							<td scope="col"><b>Date</b></td>
							<td scope="col"><?php echo $cv_date; ?></td>
						  </tr>
						  <tr>
							<td scope="col"><b>Payee</b></td>
							<td colspan="3" scope="col"><? echo $cv_payee; ?></td>
						  </tr>
						  <tr>
							<td valign="top" scope="col"><b>Particulars/Description</b></td>
							<td colspan="3" valign="top" scope="col"><?php echo $cv_description; ?></td>
						  </tr>
						  <tr>
							<td colspan="4" valign="top" scope="col">
	
								<table id="rounded-add-entries" align="center">
								  <thead>
									<tr>
									  <th scope="col" class="rounded-header" align="center"><b>Account Code</b></th>
									  <th scope="col" align="center"><b>Debit</b></th>
									  <th scope="col" align="center"><b>Credit</b></th>
									  <th scope="col" align="center"><b>Obligate Value</b></th>
                                      <th scope="col" align="center">&nbsp;</th>
									  <th scope="col" class="rounded-q4" align="center">&nbsp;</th>
								    </tr>
								  </thead>
								  <tbody>
									<?php
									$sql2 = "	SELECT 	SQL_BUFFER_RESULT
														SQL_CACHE
														cv_details_id,
														cv_details_account_code,
														cv_details_reference_number,
														cv_details_debit,
														cv_details_credit,
														cv_details_budget_id,
														cv_details_donor_id,
														cv_details_component_id,
														cv_details_activity_id,
														cv_details_other_cost_id,
														cv_details_staff_id,
														cv_details_benefits_id,
														cv_details_vehicle_id,
														cv_details_equipment_id,
														cv_details_item_id
												FROM 	tbl_cash_voucher_details
												WHERE	cv_details_reference_number = '$record_id'";
                                    $rs = mysql_query($sql2) or die("Error in sql2 in module: cash.voucher.obligate.php ".$sql2." ".mysql_error());
                                    while($rows=mysql_fetch_array($rs))
                                    {
										$cv_details_id = $rows["cv_details_id"];
										$cv_details_account_code = $rows["cv_details_account_code"];
										$cv_details_debit = $rows["cv_details_debit"];
										$cv_details_credit = $rows["cv_details_credit"];
										$cv_details_budget_id = $rows["cv_details_budget_id"];
										$cv_details_donor_id = $rows["cv_details_donor_id"];
										$cv_details_component_id = $rows["cv_details_component_id"];
										$cv_details_activity_id = $rows["cv_details_activity_id"];
										$cv_details_other_cost_id = $rows["cv_details_other_cost_id"];
										$cv_details_staff_id = $rows["cv_details_staff_id"];
										$cv_details_benefits_id = $rows["cv_details_benefits_id"];
										$cv_details_vehicle_id = $rows["cv_details_vehicle_id"];
										$cv_details_equipment_id = $rows["cv_details_equipment_id"];
										$cv_details_item_id = $rows["cv_details_item_id"];			
										$arr_debit[] = $cv_details_debit;
										$arr_credit[] = $cv_details_credit;		
										
										$sql_bc = "SELECT 	SQL_BUFFER_RESULT
															SQL_CACHE
															bc_id,
															bc_voucher_id,
															bc_account_code,
															bc_voucher_details_id,
															bc_debit,
															bc_credit,
															bc_budget_id,
															bc_donor_id,
															bc_component_id,
															bc_activity_id,
															bc_other_cost_id,
															bc_staff_id,
															bc_benefits_id,
															bc_vehicle_id,
															bc_equipment_id,
															bc_item_id															
													FROM	tbl_budget_controller
													WHERE	bc_voucher_type = 'CV'
													AND		bc_voucher_id = '$record_id'
													AND		bc_voucher_details_id = '$cv_details_id'";
										$rs_bc = mysql_query($sql_bc) or die("Error in sql2 in module: cash.voucher.details.add.php ".$sql." ".mysql_error());
										$total_rows = mysql_num_rows($rs_bc);	
										if($total_rows != 0)										
										{
											$rows_bc = mysql_fetch_array($rs_bc);										
											$bc_id = $rows_bc["bc_id"];
											$cv_details_id = $rows_bc["bc_voucher_details_id"];
											$cv_details_account_code = $rows_bc["bc_account_code"];
											$cv_details_budget_id = $rows_bc["bc_budget_id"];
											$cv_details_donor_id = $rows_bc["bc_donor_id"];
											$cv_details_component_id = $rows_bc["bc_component_id"];
											$cv_details_activity_id = $rows_bc["bc_activity_id"];
											$cv_details_other_cost_id = $rows_bc["bc_other_cost_id"];
											$cv_details_staff_id = $rows_bc["bc_staff_id"];
											$cv_details_benefits_id = $rows_bc["bc_benefits_id"];
											$cv_details_vehicle_id = $rows_bc["bc_vehicle_id"];
											$cv_details_equipment_id = $rows_bc["bc_equipment_id"];
											$cv_details_item_id = $rows_bc["bc_item_id"];												
											$bc_debit = $rows_bc["bc_debit"];
											$bc_credit = $rows_bc["bc_credit"];
										}																					
                                    ?>

 									<tr>
									  <td scope="col" align="center">
                                      <input type="hidden" name="bc_id[]" value="<?php echo $bc_id; ?>" />
                                      <input type="hidden" name="cv_details_id[]" value="<?php echo $cv_details_id; ?>" />
									  <?php
                                        $sql_a = "SELECT	SQL_BUFFER_RESULT
															SQL_CACHE
															sl_id,
															sl_account_code,
															sl_account_title
													FROM 	tbl_subsidiary_ledger
													ORDER BY sl_account_title";
                                        $rs_a = mysql_query($sql_a) or die('Error ' . mysql_error()); 
                                        ?>
                                        <select name='cv_details_account_code[]' class='formbutton'>
                                        <?php
                                            while($data_a=mysql_fetch_array($rs_a))
                                            {
                                                $column_id = $data_a['sl_id'];
                                                $column_name = $data_a['sl_account_title'];
                                                $column_code = $data_a['sl_account_code'];
                                                ?>
                                                <option value='<?php echo $column_id; ?>'
                                                <?php
                                                if ($column_id==$cv_details_account_code)
                                                {
                                                ?>
                                                    SELECTED
                                                <?php
                                                }
                                                ?>
                                                ><?php echo $column_name." [ ".$column_code." ]"; 
                                            }
                                            ?></option>
                                        </select> 
                                      </td>
                                      <td align="right"><?php echo numberFormat($cv_details_debit); ?></td>
                                      <td align="right"><?php echo numberFormat($cv_details_credit); ?></td>
									  <td scope="col" align="center"><br />
									  <?php 
                                            if ($cv_details_debit!=0) 
                                            { 
                                                echo "
                                                <input type='text' name='cv_obligate_debit[]' value='$bc_debit' class='formbutton' size='10'>";
                                            }  			
											else
											{
                                                echo "
                                                <input type='hidden' name='cv_obligate_debit[]' value='0' class='formbutton' size='10'>";

											}									
                                            if ($cv_details_credit!=0) 
                                            { 
                                                echo "
                                                <input type='text' name='cv_obligate_credit[]' value='$bc_credit' class='formbutton' size='10'>";
                                            }  
											else
											{
                                                echo "
                                                <input type='hidden' name='cv_obligate_credit[]' value='0' class='formbutton' size='10'>";

											}												
                                      ?>									  </td>
                                      <td scope="col" align="center"><b>Budget ID</b></td>
									  <td scope="col" align="center"><b>Donor ID</b></td>
								    </tr> 
 									<tr>
									  <td colspan="4" rowspan="11" align="center" scope="col">&nbsp;</td>
									  <td scope="col" align="center">
										<?php
										$sql_b = "SELECT	SQL_BUFFER_RESULT
															SQL_CACHE
															budget_id,
															budget_name,
															budget_description
													FROM 	tbl_budget
													ORDER BY budget_name";
										$rs_b = mysql_query($sql_b) or die('Error ' . mysql_error());
										?>
										<select name='cv_details_budget_id[]' class='formbutton'>
										<option value='' SELECTED>[Please Select]</option>
										<?php
										while($data_b=mysql_fetch_array($rs_b))
										{
											$column_id = $data_b['budget_id'];
											$column_name = $data_b['budget_name'];
											$column_description = $data_b['budget_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($cv_details_budget_id == $column_id)
											{
												?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_name;
										}
										?> </option></select>                                      </td>
									  <td scope="col" align="center">
										<?php
										$sql_c = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														donor_id,
														donor_name,
														donor_description
												FROM 	tbl_donor
												ORDER BY donor_name";
										$rs_c = mysql_query($sql_c) or die('Error ' . mysql_error());
										?>
										<select name='cv_details_donor_id[]' class='formbutton'>
										<option value='' SELECTED>[Please Select]</option>
										<?php
										while($data_c=mysql_fetch_array($rs_c))
										{
											$column_id = $data_c['donor_id'];
											$column_name = $data_c['donor_name'];
											$column_description = $data_c['donor_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($cv_details_donor_id == $column_id)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_name;
										}
										?> </option></select>                                        </td>
								    </tr> 
 									<tr>
 									  <td scope="col" align="center"><b>Staff ID</b></td>
									  <td scope="col" align="center"><b>Benefits ID</b></td>
								    </tr> 
 									<tr>
 									  <td scope="col" align="center">
										<?php
										$sql_f = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														staff_id,
														staff_name,
														staff_description
												FROM 	tbl_staff
												ORDER BY staff_description";
										$rs_f = mysql_query($sql_f) or die('Error ' . mysql_error());
										?>
										<select name='cv_details_staff_id[]' class='formbutton'>
										<option value='' SELECTED>[Please Select]</option>
										<?php
										while($data_f=mysql_fetch_array($rs_f))
										{
											$column_id = $data_f['staff_id'];
											$column_name = $data_f['staff_name'];
											$column_description = $data_f['staff_description'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($cv_details_staff_id == $column_id)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_description;
										}
										?> </option></select>                                      </td>
									  <td scope="col" align="center">
										<?php
                                        $sql_g = "SELECT	SQL_BUFFER_RESULT
                                                        SQL_CACHE
                                                        benefits_id,
                                                        benefits_name,
                                                        benefits_description
                                                FROM 	tbl_benefits
                                                ORDER BY benefits_description";
                                        $rs_g = mysql_query($sql_g) or die('Error ' . mysql_error());
                                        ?>
                                        <select name='cv_details_benefits_id[]' class='formbutton'>
                                        <option value='' SELECTED>[Please Select]</option>
                                        <?php
                                            while($data_g=mysql_fetch_array($rs_g))
                                            {
                                                $column_id = $data_g['benefits_id'];
                                                $column_name = $data_g['benefits_name'];
                                                $column_description = $data_g['benefits_description'];
                                                ?>
                                                <option value='<?php echo $column_id; ?>'
                                                <?php
                                                if ($cv_details_benefits_id == $column_id)
                                                {
                                                ?>
                                                    SELECTED
                                                <?php
                                                }
                                                ?>
                                                ><?php echo $column_description;
                                            }
                                            ?> </option></select>                                            </td>
								    </tr>  
 									<tr>
									  <td align="center" scope="col"><b>Component ID</b></td>
									  <td align="center" scope="col"><b>Activity ID</b></td>
 									</tr>  									
                                    <tr>
									  <td align="center" scope="col">
									  <?php
										$sql_d = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														component_id,
														component_name,
														component_description
												FROM 	tbl_component
												ORDER BY component_name";
										$rs_d = mysql_query($sql_d) or die('Error ' . mysql_error());
										?>
                                        <select name='cv_details_component_id[]' class='formbutton'>
                                          <option value='' selected="selected">[Please Select]</option>
                                          <?php
										while($data_d=mysql_fetch_array($rs_d))
										{
											$column_id = $data_d['component_id'];
											$column_name = $data_d['component_name'];
											$column_description = $data_d['component_description'];
											?>
                                          <option value='<?php echo $column_id; ?>'
											<?php
											if ($cv_details_component_id == $column_id)
											{
											?>
												selected="selected"
											<?php
											}
											?>
											><?php echo $column_name;
										}
										?></option>
                                      </select></td>
									  <td align="center" scope="col"><?php
										$sql_e = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														activity_id,
														activity_name,
														activity_description
												FROM 	tbl_activity
												ORDER BY activity_name";
										$rs_e = mysql_query($sql_e) or die('Error ' . mysql_error());
										?>
                                        <select name='cv_details_activity_id[]' class='formbutton'>
                                          <option value='' selected="selected">[Please Select]</option>
                                          <?php
										while($data_e=mysql_fetch_array($rs_e))
										{
											$column_id = $data_e['activity_id'];
											$column_name = $data_e['activity_name'];
											$column_description = $data_e['activity_description'];
											?>
                                          <option value='<?php echo $column_id; ?>'
											<?php
											if ($cv_details_activity_id == $column_id)
											{
											?>
												selected="selected"
											<?php
											}
											?>
											><?php echo $column_name;
										}
										?> </option>
                                      </select>                                      </td>
                                    </tr>  									
                                    <tr>
									  <td align="center" scope="col"><b>Vehicles ID</b></td>
									  <td align="center" scope="col"><b>Equipment ID</b></td>
 									</tr>  									
                                    <tr>
									  <td align="center" scope="col">
										<?php
                                        $sql_f = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														vehicle_id,
														vehicle_name,
														vehicle_description
												FROM 	tbl_vehicle
												ORDER BY vehicle_description";
                                        $rs_f = mysql_query($sql_f) or die('Error ' . mysql_error());
                                        ?>
                                        <select name='cv_details_vehicle_id[]' class='formbutton'>
                                        <option value='' SELECTED>[Please Select]</option>
                                        <?php
                                        while($data_f=mysql_fetch_array($rs_f))
                                        {
                                            $column_id = $data_f['vehicle_id'];
                                            $column_name = $data_f['vehicle_name'];
                                            $column_description = $data_f['vehicle_description'];
                                            ?>
                                            <option value='<?php echo $column_id; ?>'
                                            <?php
                                            if ($cv_details_vehicle_id == $column_id)
                                            {
                                            ?>
                                                SELECTED
                                            <?php
                                            }
                                            ?>
                                            ><?php echo $column_description;
                                        }
                                        ?> </option></select>                                      </td>
									  <td align="center" scope="col">
										<?php
                                        $sql_g = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														equipment_id,
														equipment_name,
														equipment_description
												FROM 	tbl_equipment
												ORDER BY equipment_name";
                                        $rs_g = mysql_query($sql_g) or die('Error ' . mysql_error());
                                        ?>
                                        <select name='cv_details_equipment_id[]' class='formbutton'>
                                        <option value='' SELECTED>[Please Select]</option>
                                        <?php
                                        while($data_g=mysql_fetch_array($rs_g))
                                        {
                                            $column_id = $data_g['equipment_id'];
                                            $column_name = $data_g['equipment_name'];
                                            $column_description = $data_g['equipment_description'];
                                            ?>
                                            <option value='<?php echo $column_id; ?>'
                                            <?php
                                            if ($cv_details_equipment_id == $column_id)
                                            {
                                            ?>
                                                SELECTED
                                            <?php
                                            }
                                            ?>
                                            ><?php echo $column_description;
                                        }
                                        ?> </option></select>                                      </td>
                                    </tr>                                    
                                    <tr>
									  <td colspan="2" align="center" scope="col"><b>Other Cost/Services</b></td>
								    </tr>  									
                                    <tr>
                                      <td colspan="2" align="center" scope="col"><?php
                                        $sql_h = "SELECT	SQL_BUFFER_RESULT
														SQL_CACHE
														cs_id,
														cs_name,
														cs_description
												FROM 	tbl_other_cost_services
												ORDER BY cs_name";
                                        $rs_h = mysql_query($sql_h) or die('Error ' . mysql_error());
                                        ?>
                                        <select name='cv_details_other_cost_id[]' class='formbutton'>
                                          <option value='' selected="selected">[Please Select]</option>
                                          <?php
                                        while($data_h=mysql_fetch_array($rs_h))
                                        {
                                            $column_id = $data_h['cs_id'];
                                            $column_name = $data_h['cs_name'];
                                            $column_description = $data_h['cs_description'];
                                            ?>
                                          <option value='<?php echo $column_id; ?>'
                                            <?php
                                            if ($cv_details_other_cost_id == $column_id)
                                            {
                                            ?>
                                                selected="selected"
                                            <?php
                                            }
                                            ?>
                                            ><?php echo $column_description;
                                        }
                                        ?> </option>
                                      </select></td>
                                    </tr>
                                    <tr>
                                      <td colspan="2" align="center" scope="col"><b>Item ID</b></td>
                                    </tr>
                                    <tr>
									  <td colspan="2" align="center" scope="col">
										<?php
                                        $sql_i = "SELECT	SQL_BUFFER_RESULT
                                                    SQL_CACHE
                                                    item_id,
                                                    item_name,
                                                    item_description
                                            FROM 	tbl_item_code
                                            ORDER BY item_name";
                                        $rs_i = mysql_query($sql_i) or die('Error ' . mysql_error());
                                        ?>
                                        <select name='cv_details_item_id[]' class='formbutton'>
                                        <option value='' selected="selected">[Please Select]</option>
                                        <?php
                                        while($data_i=mysql_fetch_array($rs_i))
                                        {
                                            $column_id = $data_i['item_id'];
                                            $column_name = $data_i['item_name'];
                                            $column_description = $data_i['item_description'];
                                            ?>
                                        <option value='<?php echo $column_id; ?>'
                                            <?php
                                            if ($cv_details_item_id == $column_id)
                                            {
                                            ?>
                                                selected="selected"
                                            <?php
                                            }
                                            ?>
                                            ><?php echo $column_description;
                                        }
                                        ?> </option></select>                                      </td>
								    </tr>                                                                                                     	
                                    <?php 
                                    } 
									?>                                  
								  </tbody>
								  <?php 
									if ($arr_credit != 0 && $arr_debit != 0)
									{
										$total_debit = array_sum($arr_debit);
										$total_credit = array_sum($arr_credit);
									}
									?>
								  <tfoot>
									<tr>
									  <td class="rounded-foot-left" align="right"><b>Total :</b></td>
									  <td align="right"><?php echo numberFormat($total_debit); ?> </td>
									  <td align="right"><?php echo numberFormat($total_credit);  ?> </td>
									  <td class="rounded-foot-right" colspan="6" align="right">&nbsp;</td>
									</tr>
								  </tfoot>								
								</table>
	
							</td>
						  </tr>							  
						  <tr>
							<td colspan="4" valign="top" scope="col">
                                <input type="hidden" name="cv_date" value="<?php echo $cv_date; ?>">
                                <input type="hidden" name="cv_description" value="<?php echo $cv_description; ?>">
                            	<input type="hidden" name="cv_currency_code" value="<?php echo $arr_currency_code; ?>">
								<input type="hidden" name="cv_certified_by" value="<?php echo $cv_certified_by; ?>">
                                <input type="hidden" name="cv_approved_by" value="<?php echo $cv_approved_by; ?>">
								<input type="hidden" name="cv_recommended_by" value="<?php echo $cv_recommended_by; ?>">
							  	<input name="cv_obligated_by" type="checkbox" id="cv_obligated_by" value="1" <?php if ($cv_obligated_by!=0) { echo "CHECKED";} ?>/>
								* Obligate this record.                                </td>
						  </tr>					  					  					  
			
						</tbody>
					</table>
				</form>
			</td>
		</tr>
	</table>
	<?php include("./../includes/footer.main.php"); ?>
<?php
}
?>