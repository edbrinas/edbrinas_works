<?php 
$report_name = "Cash Voucher";
include("./../includes/header.report.php");
$report_name = "CASH VOUCHER";
$rep_id = $_GET['id'];
$rep_cv_number = $_GET['cv_number'];
$rep_id = decrypt($rep_id);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE	
				cv_id,			
				cv_type,
				cv_number,
				cv_currency_code,
				cv_date,
				cv_payee,
				cv_description,
				cv_prepared_by,
				cv_certified_by,
				cv_approved_by
		FROM 	tbl_cash_voucher
		WHERE	cv_id = '$rep_id'";
$rs = mysql_query($sql) or die("Error in sql1 in module: cash.voucher.edit.php ".$sql." ".mysql_error());
$rows = mysql_fetch_array($rs);

$cv_id = $rows["cv_id"];
$cv_type = $rows["cv_type"];
$cv_number = $rows["cv_number"];
$cv_currency_code = $rows["cv_currency_code"];
$cv_date = $rows["cv_date"];
$cv_payee = $rows["cv_payee"];
$cv_description = $rows["cv_description"];
$cv_prepared_by = $rows["cv_prepared_by"];
$cv_certified_by = $rows["cv_certified_by"];
$cv_approved_by = $rows["cv_approved_by"];

$cv_payee = getPayeeById($cv_payee);
$cv_description = stripslashes($cv_description);
$cv_currency_name = getConfigurationDescriptionById($cv_currency_code);
$cv_currency_code = getConfigurationValueById($cv_currency_code);

if ($cv_prepared_by!=0) { $cv_prepared_by = getFullName($cv_prepared_by,3); } else { $cv_prepared_by=""; }
if ($cv_certified_by!=0) { $cv_certified_by = getFullName($cv_certified_by,3); } else { $cv_certified_by=""; }
if ($cv_approved_by!=0) { $cv_approved_by = getFullName($cv_approved_by,3); } else { $cv_approved_by=""; }

if ($rep_id == $cv_id && $rep_cv_number == md5($cv_number))
{
?>
<table width='100%' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>
	<tr>
		<td scope='col'>
		<!-- Start of Inner Table -->
			<table width='100%' border='1' cellpadding='2' cellspacing='0' bordercolor='#CCCCCC' class='report_printing'>
			  <tr>
				<th rowspan='3' scope='col'>Transaction Date </th>
				<th rowspan='3' scope='col'>Voucher Number </th>
				<th rowspan='3' scope='col'>Payee</th>
				<th rowspan='3' scope='col'>Local Amount </th>
				<th rowspan='3' scope='col'>Particulars / Description </th>
				<th rowspan='3' scope='col'>Debit / Credit</th>
				<th rowspan='3' scope='col'>Account Code </th>
				<th colspan='9' scope='col'>SL CODE </th>
				<th rowspan='3' scope='col'>Amount in Words </th>
				<th rowspan='3' scope='col'>Acknowledgement Portion </th>
				<th rowspan='3' scope='col'>Prepared By </th>
				<th rowspan='3' scope='col'>Certified By </th>
				<th rowspan='3' scope='col'>Recommended by </th>
				<th rowspan='3' scope='col'>Approved by </th>
			  </tr>
			  <tr>
				<th>SLC1</th>
				<th>SLC2</th>
				<th>SLC3</th>
				<th>SLC4</th>
				<th>SLC5</th>
				<th>SLC6</th>
				<th>SLC7</th>
				<th>SLC8</th>
				<th>SLC9</th>
			  </tr>
			  <tr>
				<th>Budget ID </th>
				<th>Donor ID </th>
				<th>Component ID </th>
				<th>Activity ID </th>
				<th>Other Cost / Services ID </th>
				<th>Staff ID </th>
				<th>Benefits ID </th>
				<th>Vehicle ID </th>
				<th>Equipment ID </th>
			  </tr>
			  <tr>
				<td valign='top'><?php echo $cv_date; ?>&nbsp;</td>
				<td valign='top'><?php echo $cv_number; ?>&nbsp;</td>
				<td valign='top'><?php echo $cv_payee; ?>&nbsp;</td>
				<td valign='top'><?php echo $cv_currency_code; ?>&nbsp;</td>
				<td valign='top'><?php echo $cv_description; ?>&nbsp;</td>
				<?php
				$sql2 = "	SELECT 	SQL_BUFFER_RESULT
									SQL_CACHE
									cv_details_id,
									cv_details_account_code,
									cv_details_reference_number,
									cv_details_debit,
									cv_details_credit,
									cv_details_budget_id,
									cv_details_donor_id,
									cv_details_component_id,
									cv_details_activity_id,
									cv_details_other_cost_id,
									cv_details_staff_id,
									cv_details_benefits_id,
									cv_details_vehicle_id,
									cv_details_equipment_id
							FROM 	tbl_cash_voucher_details
							WHERE	cv_details_reference_number = '$cv_id'";
				$rs2 = mysql_query($sql2) or die("Error in sql2 in module: journal.voucher.details.php ".$sql2." ".mysql_error());
				$total_rows = mysql_num_rows($rs2);	
				for($row_number=0;$row_number<$total_rows;$row_number++)
				{
					$rows2 = mysql_fetch_array($rs2);
					$cv_details_id[] = $rows2['cv_details_id'];
					$cv_details_reference_number[] = $rows2['cv_details_reference_number'];
					$cv_details_account_code[] = $rows2['cv_details_account_code'];
					$cv_details_debit = $rows2['cv_details_debit'];
					$cv_details_credit = $rows2['cv_details_credit'];
					$cv_details_budget_id[] = $rows2['cv_details_budget_id'];
					$cv_details_donor_id[] = $rows2['cv_details_donor_id'];
					$cv_details_component_id[] = $rows2['cv_details_component_id'];
					$cv_details_activity_id[] = $rows2['cv_details_activity_id'];
					$cv_details_other_cost_id[] = $rows2['cv_details_other_cost_id'];
					$cv_details_staff_id[] = $rows2['cv_details_staff_id'];
					$cv_details_benefits_id[] = $rows2['cv_details_benefits_id'];
					$cv_details_vehicle_id[] = $rows2['cv_details_vehicle_id'];
					$cv_details_equipment_id[] = $rows2['cv_details_equipment_id'];
					
					if (numberFormat($cv_details_debit)!=0)
					{
						$cv_transaction_total[] = numberFormat($cv_details_debit);
					}
					else if (numberFormat($cv_details_credit)!=0)
					{
						$cv_transaction_total[] = numberFormat($cv_details_credit);
					}
				}
				?>
					<td valign='top'>
						<?php 
						foreach ($cv_transaction_total as $cv_transaction_total)
						{
							echo $cv_transaction_total;
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($cv_details_account_code as $cv_details_account_code)
						{
							echo getSubsidiaryLedgerAccountTitleByid($cv_details_account_code);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($cv_details_budget_id as $cv_details_budget_id)
						{
							echo getBudgetDescription($cv_details_budget_id);;
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($cv_details_donor_id as $cv_details_donor_id)
						{
							echo getDonorDescription($cv_details_donor_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($cv_details_component_id as $cv_details_component_id)
						{
							echo getComponentDescription($cv_details_component_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($cv_details_activity_id as $cv_details_activity_id)
						{
							echo getActivityDescription($cv_details_activity_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>																				
					<td valign='top'>
						<?php 
						foreach ($cv_details_other_cost_id as $cv_details_other_cost_id)
						{
							echo getOtherCostDescription($cv_details_other_cost_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($cv_details_staff_id as $cv_details_staff_id)
						{
							echo getStaffDescription($cv_details_staff_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>	
					<td valign='top'>
						<?php 
						foreach ($cv_details_benefits_id as $cv_details_benefits_id)
						{
							echo getBenefitsDescription($cv_details_benefits_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>
					<td valign='top'>
						<?php 
						foreach ($cv_details_vehicle_id as $cv_details_vehicle_id)
						{
							echo getVehicleDescription($cv_details_vehicle_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>	
					<td valign='top'>
						<?php 
						foreach ($cv_details_equipment_id as $cv_details_equipment_id)
						{
							echo getEquipmentDescription($cv_details_equipment_id);
							echo '<br>';
						} 
						?>&nbsp;
					</td>																				
				<td valign='top'><?php echo convertToWords($cv_transaction_total).' '.$cv_currency_name; ?>&nbsp;</td>
				<td valign='top'>&nbsp;</td>
				<td valign='top'><?php echo $cv_prepared_by; ?>&nbsp;</td>
				<td valign='top'><?php echo $cv_certified_by; ?>&nbsp;</td>
				<td valign='top'><?php echo $cv_recommended_by; ?>&nbsp;</td>
				<td valign='top'><?php echo $cv_approved_by; ?>&nbsp;</td>
			  </tr>
        </table>
		<!-- End of Inner Table -->		
		</td>
	</tr>
</table>
<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
	<a href="/workspaceGOP/report.templates/cash.voucher.php?id=<?php echo encrypt($cv_id); ?>&cv_number=<?php echo md5($cv_number); ?>"><img src="/workspaceGOP/images/save.png" border="0" title="Export to word"></a>
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>
<?php
}
else
{
	insertEventLog($user_id,"User trying to manipulate the page: journal.voucher.details.php");	
	session_destroy();
	?>
	<script language="javascript" type="text/javascript">
		alert("WARNING! You're trying to manipulate the system! This action will be reported to IS/IT!");
		window.close();
	</script>
	<?php
}
?>