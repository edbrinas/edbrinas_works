<?php
include("./../connections/cn.php");
include("./../includes/function.php");
$date_time_inserted = date("Y-m-d H:i:s");
$show_msg = 0;
$err = 0;

if ($_POST['Submit'] == 'Upload File')
{
	$user_file = $_FILES['file_upload']['tmp_name'];
	if(isset($user_file))
	{
		$temp_file = $_FILES['file_upload']['tmp_name'];
		$file_name = $_FILES['file_upload']['name'];
		$handle = fopen($temp_file, "r");
		$filesize = filesize($temp_file);	
		$file_extention = substr($file_name, strrpos($file_name, '.'));

		if ($file_extention==".csv")
		{
			$x = 0;
			
			if ($_POST["report_type"]=="dv") { $table_name = "tbl_disbursement_voucher"; }
			if ($_POST["report_type"]=="jv") { $table_name = "tbl_journal_voucher"; }
			if ($_POST["report_type"]=="cv") { $table_name = "tbl_cash_voucher"; }

			$sql_del = "DELETE FROM tbl_budget_controller
						WHERE	bc_voucher_type = 'DV'
						AND		bc_date BETWEEN '2010-01-01' AND '2010-07-28'";
			mysql_query($sql_del) or die($sql.mysql_error());
			
			$sql_del = "DELETE FROM tbl_budget_controller
						WHERE	bc_voucher_type = 'CV'
						AND		bc_date BETWEEN '2010-01-01' AND '2010-07-28'";
			mysql_query($sql_del) or die($sql.mysql_error());		
				
			$sql_del = "DELETE FROM tbl_budget_controller
						WHERE	bc_voucher_type = 'JV'
						AND		bc_date BETWEEN '2010-01-01' AND '2010-07-31'";
			mysql_query($sql_del) or die($sql.mysql_error());			
						
			while($data = fgetcsv($handle, $filesize, ","))
			{
				/*
				0 Voucher Number	
				1 Account Code	
				2 Debit 	 
				3 Credit 	
				4 Budget ID	
				5 Donor ID	
				6 Component ID	
				7 Activity ID	
				8 Cost ID	
				9 Staff ID	
				10 Benefits ID	
				11 Vehicle ID	
				12 Equipment ID	
				13 Item Code
				*/
				$account_code = trim($data[1]);
				$budget_id = trim($data[4]);
				$donor_id = trim($data[5]);
				#$component_id = trim($data[6]);
				$new_component_id = trim($data[6]);
				$activity_id = trim($data[7]);
				$cost_id = trim($data[8]);
				$staff_id = trim($data[9]);
				$benefits_id = trim($data[10]);
				$vehicle_id = trim($data[11]);
				$equipment_id = trim($data[12]);
				$item_id = trim($data[13]);
				
				$x = $x + 1;
				
				if ($x!=1 && $data[0]!="EOF")
				{
					$voucher_id = "";
					
					$sql = "SELECT 	".$_POST["report_type"]."_id AS voucher_id,
									".$_POST["report_type"]."_date AS voucher_date,
									".$_POST["report_type"]."_currency_code AS voucher_currency,
									".$_POST["report_type"]."_description AS voucher_description
							FROM 	".$table_name."
							WHERE	".$_POST["report_type"]."_number = '".trim($data[0])."'
							AND		".$_POST["report_type"]."_type = '".ucwords($_POST["report_type"])."'
							LIMIT 1";
					$rs = mysql_query($sql) or die("Error in sql ".$sql." ".mysql_error());
					$record_count = mysql_num_rows($rs);

					if ($record_count!=0)
					{
						$rows=mysql_fetch_array($rs);
						
						$voucher_id = $rows["voucher_id"];
						$voucher_date = $rows["voucher_date"];
						$voucher_currency = $rows["voucher_currency"];
						$voucher_description = $rows["voucher_description"];

						$account_code = getSlIdByAccountCode($account_code);
						
						if ($donor_id!="")
						{						
							$new_donor_id = getDonorID($donor_id);
							if ($new_donor_id=="")
							{
								$show_msg=1; 
								$msg .= "Error on row  ".$x.". Donor ID (".$donor_id.") does not exists"; 
								$msg .="<br>"; 					
							}
						}
						if ($budget_id!="")
						{						
							$new_budget_id = getBudgetID($budget_id);
							if ($new_budget_id=="")
							{
								$show_msg=1; 
								$msg .= "Error on row  ".$x.". Donor ID (".$budget_id.") does not exists"; 
								$msg .="<br>"; 					
							}
						}
						/*					
						if ($component_id!="")
						{						
							$new_component_id = getComponentID($component_id);
							if ($new_component_id=="")
							{
								$show_msg=1; 
								$msg .= "Error on row  ".$x.". Component ID (".$component_id.") does not exists"; 
								$msg .="<br>"; 					
							}
						}
						*/					
						if ($activity_id!="")
						{						
							$new_activity_id = getActivityID($activity_id);
							if ($new_activity_id=="")
							{
								$show_msg=1; 
								$msg .= "Error on row  ".$x.". Activity ID (".$activity_id.") does not exists"; 
								$msg .="<br>";				
							}
						}					
						if ($other_cost_id!="")
						{						
							$new_cost_id = getOtherCostID($other_cost_id);
							if ($new_cost_id=="")
							{
								$show_msg=1; 
								$msg .= "Error on row  ".$x.". Other Cost ID (".$other_cost_id.") does not exists"; 
								$msg .="<br>"; 					
							}
						}					
						if ($staff_id!="")
						{						
							$new_staff_id = getStaffIDByID($staff_id);
							if ($new_staff_id=="")
							{
								$show_msg=1; 
								$msg .= "Error on row  ".$x.". Staff ID (".$staff_id.") does not exists"; 
								$msg .="<br>"; 				
							}
						}					
						if ($benefits_id!="")
						{						
							$new_benefits_id = getBenefitsID($benefits_id);
							if ($new_benefits_id=="")
							{
								$show_msg=1; 
								$msg .= "Error on row  ".$x.". Benefits ID (".$benefits_id.") does not exists"; 
								$msg .="<br>";				
							}
						}
						if ($vehicle_id!="")
						{						
							$new_vehicle_id = getVehicleID($vehicle_id);
							if ($new_vehicle_id=="")
							{
								$show_msg=1; 
								$msg .= "Error on row  ".$x.". Vehicle ID (".$vehicle_id.") does not exists"; 
								$msg .="<br>"; 				
							}
						}
						if ($equipment_id!="")
						{						
							$new_equipment_id = getEquipmentID($equipment_id);
							if ($new_equipment_id=="")
							{
								$show_msg=1; 
								$msg .= "Error on row  ".$x.". Equipment ID (".$equipment_id.") does not exists"; 
								$msg .="<br>"; 		
							}
						}
						if ($item_id!="")
						{						
							$new_item_id = getItemId($item_id);
							if ($new_item_id=="")
							{
								$show_msg=1; 
								$msg .= "Error on row  ".$x.". Item ID (".$item_id.") does not exists"; 
								$msg .="<br>"; 					
							}
						}								
						
	
									
						$sql = "INSERT INTO tbl_budget_controller 
											(	
											bc_voucher_type,
											bc_voucher_number,
											bc_voucher_id,
											bc_date,
											bc_description,
											bc_account_code,
											bc_currency_code,
											bc_debit,
											bc_credit,
											bc_budget_id,
											bc_donor_id,
											bc_component_id,
											bc_activity_id,
											bc_other_cost_id,
											bc_staff_id,
											bc_benefits_id,
											bc_vehicle_id,
											bc_equipment_id,
											bc_item_id,
											bc_inserted_by
											)
								VALUES (	'".ucwords($_POST["report_type"])."',
											'".trim($data[0])."',
											'".$voucher_id."',
											'".$voucher_date."',
											'".addslashes($voucher_description)."',
											'".$account_code."',
											'".$voucher_currency."',
											'".trim($data[2])."',
											'".trim($data[3])."',
											'".trim($new_budget_id)."',
											'".trim($new_donor_id)."',
											'".trim($new_component_id)."',
											'".trim($new_activity_id)."',
											'".trim($new_cost_id)."',
											'".trim($new_staff_id)."',
											'".trim($new_benefits_id)."',
											'".trim($new_vehicle_id)."',
											'".trim($new_equipment_id)."',
											'".trim($new_item_id)."',											
											'1')";
						mysql_query($sql) or die($sql.mysql_error());
						
						$new_account_code = "";
						$new_budget_id = "";
						$new_donor_id = "";
						$new_component_id = "";
						$new_activity_id = "";
						$new_cost_id = "";
						$new_staff_id = "";
						$new_benefits_id = "";
						$new_vehicle_id = "";
						$new_equipment_id = "";
						$new_item_id = "";						
					}
					else
					{
						$show_msg = 1;
						$msg .= "Voucher Number Not Found.";
						break;
					}
				} #END OF CHECKING OF EOF
			} #END OF WHILE LOOP
		} #END OF CHECKING IF CSV
		else
		{
			$show_msg = 1;
			$msg = "Invalid file name.";
		}		
	}
}
?>
<form method="post" enctype="multipart/form-data">
<table align="center" border="1">
    <thead>
        <tr>
            <th scope="col" colspan="2">Upload Obligation Report</th>
        </tr>
        <?php
		if ($show_msg==1)
		{
		?>
        <tr>
            <th scope="col" colspan="2"><?php echo $msg; ?></th>
        </tr>        
        <?php
        }
        ?>
    </thead>
    <tfoot>
        <tr>
            <td align="right">&nbsp;</td>
            <td align="left"><input type="submit" name="Submit" value="Upload File" class="formbutton"></td>
        </tr>
    </tfoot>
    <tbody>				
        <tr>
          <td align="right" valign="top">Report Type</td>
          <td align="left">
          <select name="report_type">
			<option value="dv" <?php if ($_POST["report_type"]=="DV") { echo "SELECTED"; } ?>>DV</option>
			<option value="jv" <?php if ($_POST["report_type"]=="JV") { echo "SELECTED"; } ?>>JV</option>
			<option value="cv" <?php if ($_POST["report_type"]=="CV") { echo "SELECTED"; } ?>>CV</option>
          </select>
          </td>
        </tr>
        <tr>
            <td align="right" valign="top"><b>File:*</b></td>
            <td align="left"><input name='file_upload' type='file' class="formbutton"/></td>
        </tr>			
    </tbody>
</table>	  
</form>
