<?php 
$report_name = "CHEQUE CANCELLED STATEMENT";
include("./../includes/header.report.php");

$cc_start_year = $_POST['cc_start_year'];
$cc_start_month = $_POST['cc_start_month'];
$cc_start_day = $_POST['cc_start_day'];
$cc_end_year = $_POST['cc_end_year'];
$cc_end_month = $_POST['cc_end_month'];
$cc_end_day = $_POST['cc_end_day'];
$cc_bank_id = $_POST['cc_bank_id'];

$start_date = dateFormat($cc_start_month,$cc_start_day,$cc_start_year);
$end_date = dateFormat($cc_end_month,$cc_end_day,$cc_end_year);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				bank_name,
				bank_account_number,
				bank_account_currency_code,
				bank_account_sl_code
		FROM	tbl_bank
		WHERE	bank_id = '$cc_bank_id'";
$rs = mysql_query($sql) or die("Query Error" .mysql_error());
$rows = mysql_fetch_array($rs);
$bank_name = $rows["bank_name"];
$bank_account_number =	$rows["bank_account_number"];
$bank_account_currency_code = $rows["bank_account_currency_code"];
$bank_account_sl_code = $rows["bank_account_sl_code"];
$bank_account_ending_balance = $rows["bank_account_ending_balance"];
$bank_account_currency_code = getConfigurationValueById($bank_account_currency_code);



?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br />
					<?php echo upperCase(getCompanyName()); ?>
					<br />
					<?php echo upperCase(getCompanyAddress()); ?><br />
					For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
				  <td scope='col'>
				  <!-- Start of inner table -->
				  
					<table width="100%" border="0" cellspacing="2" cellpadding="2">
						<tr>
							<td width="10%"><b>Bank</b></td>
							<td width="90%"><?php echo $bank_name; ?></td>
						</tr>
						<tr>
							<td><b>Account Number</b></td>
							<td><?php echo $bank_account_number; ?></td>
						</tr>
						<tr>
							<td><b>Period Covered</b></td>
							<td>
							<?php
								echo $start_date." to ".$end_date;
							?>
							</td>
						</tr>
						<tr>
						  <td colspan="2">
						  
						  <table width="100%" border="1" cellspacing="0" cellpadding="2">
                          	<thead>
						    <tr>
                              <th width="10%" scope="col">Cheque Number </th>
                              <th width="10%" scope="col">DV Number </th>
                              <th width="8%" scope="col">Check Date </th>
                              <th width="12%" scope="col">Check Status </th>
                              <th width="20%" scope="col">Payee</th>
                              <th width="30%" scope="col">Description</th>
                              <th width="10%" scope="col">Amount</th>
                            </tr>
							</thead>
							<tbody>
								<?php
									$sql = "SELECT 	SQL_BUFFER_RESULT
													SQL_CACHE
													dv.dv_number AS dv_number,
													dv.dv_payee AS dv_payee,
													dv.dv_description AS dv_description,
													cheque.cheque_number AS cheque_number,
													cheque.cheque_date AS cheque_date,
													cheque.cheque_status AS cheque_status,
													cheque.cheque_amount AS cheque_amount
											FROM	tbl_disbursement_voucher AS dv,
													tbl_cheque AS cheque
											WHERE 	dv.dv_id = cheque_reference_number
											AND		cheque.cheque_sl_id = '$bank_account_sl_code'
											AND		cheque.cheque_number != ''
											AND		(cheque.cheque_status = 3
											OR		cheque.cheque_status = 4)
											AND		cheque.cheque_date
											BETWEEN	'$start_date'
											AND		'$end_date'
											ORDER BY cheque.cheque_date,cheque.cheque_number,dv.dv_number DESC";
								$rs = mysql_query($sql) or die("Error in sql in function getGLSumDebitSumCredit ".$sql." ".mysql_error());	
								$total_rows = mysql_num_rows($rs);
								if ($total_rows==0)
								{
									?>
										<script language="javascript" type="text/javascript">
											alert("No records found!");
											window.close();
										</script>
									<?php
								}
								else
								{								
									while($rows=mysql_fetch_array($rs))
									{		
										$cheque_date = $rows["cheque_date"];
										$dv_number = $rows["dv_number"];
										$cheque_number = $rows["cheque_number"];	
										$cheque_status = $rows["cheque_status"];
										$dv_payee = $rows["dv_payee"];
										$dv_description = $rows["dv_description"];
										$cheque_amount = $rows["cheque_amount"];
										$arr_cheque_amount[] = $cheque_amount;
										$dv_payee = getPayeeById($dv_payee);
	
										if ($cheque_status==0) { $cheque_stat = "No cheque issued"; }
										elseif ($cheque_status==1) { $cheque_stat = "Cheque issued, but not yet cleared"; }
										elseif ($cheque_status==2) { $cheque_stat = "Cheque cleared"; }
										elseif ($cheque_status==3) { $cheque_stat = "Cheque cancelled, but not yet approved"; }
										elseif ($cheque_status==4) { $cheque_stat = "Cheque cancelled"; }
										elseif ($cheque_status==5) { $cheque_stat = "Cheque released"; }
										else { $check_stat = "N/A"; }
									?>
										<tr>
										  <td scope="col" valign="top"><?php echo $cheque_number; ?></td>
										  <td scope="col" valign="top"><?php echo $dv_number; ?></td>
										  <td scope="col" valign="top"><?php echo $cheque_date; ?></td>
										  <td scope="col" valign="top"><?php echo $cheque_stat; ?></td>
										  <td scope="col" valign="top"><?php echo $dv_payee; ?></td>
										  <td scope="col" valign="top"><?php echo $dv_description; ?></td>
										  <td scope="col" align="right" valign="top"><?php echo numberFormat($cheque_amount); ?></td>
										</tr>
									<?php
									}
								}
								if (count($arr_cheque_amount)!=0)
								{
									$check_cancelled_total = array_sum($arr_cheque_amount);
								}
								else
								{
									$check_cancelled_total = 0;
								}
								$check_cancelled_total = numberFormat($check_cancelled_total);
								?>
							</tbody>
							<tfoot>
								<tr>
								  <td scope="col" colspan="6" align="right">Cheque Total :</td>
									  <td scope="col" align="right"><?php echo $check_cancelled_total; ?></td>
								</tr>
							</tfoot>
                          </table>
						  
						  </td>
						</tr>	
						<tr>
						  <td colspan="2" align="center">&nbsp;</td>
						</tr>					
					</table>

				  
				  <!-- End of inner table -->
				  </td>
				</tr>
			</table>	
		</td>
	</tr>
</table>


<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>



