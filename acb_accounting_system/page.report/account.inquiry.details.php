<?php 
$report_name = 'ACCOUNT INQUIRY REPORT';
include('./../includes/header.report.php');

$ai_currency_code = $_POST['ai_currency_code'];
$ai_start_year = $_POST['ai_start_year'];
$ai_start_month = $_POST['ai_start_month'];
$ai_start_day = $_POST['ai_start_day'];
$ai_end_year = $_POST['ai_end_year'];
$ai_end_month = $_POST['ai_end_month'];
$ai_end_day = $_POST['ai_end_day'];
$ai_details_account_code = $_POST['ai_details_account_code'];
$ai_budget_id = $_POST['ai_budget_id'];
$ai_donor_id = $_POST['ai_donor_id'];
$consolidate_reports = $_POST['consolidate_reports'];
$ai_staff_id = $_POST['ai_staff_id'];
$ai_equipment_id = $_POST['ai_equipment_id'];
$ai_vehicle_id = $_POST['ai_vehicle_id'];
$ai_benefits_id = $_POST['ai_benefits_id'];
$ai_other_cost_id = $_POST['ai_other_cost_id'];
$ai_benefits_id = $_POST['ai_benefits_id'];
$ai_vehicle_id = $_POST['ai_vehicle_id'];
$ai_equipment_id = $_POST['ai_equipment_id'];
$ai_item_id = $_POST['ai_item_id'];
$use_obligated_value = $_POST['use_obligated_value'];

$system_start_date = date("Y-m-d", mktime(0,0,0,1,1,2008));
$selected_start_date_year = date("Y-m-d", mktime(0,0,0,1,1,$ai_start_year));
$start_date_minus_1 = date("Y-m-d", mktime(0,0,0,$ai_start_month,$ai_start_day-1, $ai_start_year));

$start_date = date("Y-m-d", mktime(0,0,0,$ai_start_month,$ai_start_day, $ai_start_year));
$end_date = date("Y-m-d", mktime(0,0,0,$ai_end_month,$ai_end_day,$ai_end_year));

$nominal_account = checkIfNominalAccount($ai_details_account_code);
$ai_account_code = getSubsidiaryLedgerAccountCodeById($ai_details_account_code);
$ai_account_title = getSubsidiaryLedgerAccountTitleById($ai_details_account_code);

$sl_other_cost_id = checkSubsidiaryLedgerRequirement($ai_details_account_code,"sl_other_cost_id");
$sl_staff_id = checkSubsidiaryLedgerRequirement($ai_details_account_code,"sl_staff_id");
$sl_benefits_id = checkSubsidiaryLedgerRequirement($ai_details_account_code,"sl_benefits_id");
$sl_vehicle_id = checkSubsidiaryLedgerRequirement($ai_details_account_code,"sl_vehicle_id");
$sl_equipment_id  = checkSubsidiaryLedgerRequirement($ai_details_account_code,"sl_equipment_id");

if ($sl_staff_id == 0) { $ai_staff_id = ""; }
if ($sl_other_cost_id == 0) { $ai_other_cost_id = ""; }
if ($sl_benefits_id == 0) { $ai_benefits_id = ""; }
if ($sl_vehicle_id == 0) { $ai_vehicle_id = ""; }
if ($sl_equipment_id == 0) { $ai_equipment_id = ""; }

$ai_currency_name = getConfigurationValueById($ai_currency_code);
?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?><br>
					<?php echo upperCase(getCompanyName()); ?><br>
					<?php echo upperCase(getCompanyAddress()); ?><br>
					For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?><br>
					Account Code: <?php echo $ai_account_code; ?><br>
					Account Title: <?php echo $ai_account_title; ?><br>
					ACB-<?php echo $ai_currency_name; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
				  <td scope='col'>
				  <!-- Start of Report Table -->
				  <table width="100%" border="1" cellspacing="0" cellpadding="2">
				  <thead>
                    <tr>
                      <th scope="col">Vou cher Date</th>
					  <th scope="col">Vou cher Type</th>
                      <th scope="col">Voucher Number</th>
                      <th scope="col">Payee</th>
                      <th scope="col">Remarks / Description</th>
                      <th scope="col">Account Code</th>
                      <th scope="col">Bud get ID</th>
                      <th scope="col">Do nor ID</th>
                      <th scope="col">Comp onent ID</th>
                      <th scope="col">Co st / Ser vi ces</th>
                      <th scope="col">Acti vity ID</th>
                      <th scope="col">Staff ID</th>
                      <th scope="col">Ben efits ID</th>
                      <th scope="col">Veh icle ID</th>
					  <th scope="col">Equi pment ID</th>
                      <th scope="col">Item ID</th>
                      <th scope="col">Debit</th>
                      <th scope="col">Credit</th>
                      <th scope="col">Balance</th>
                    </tr>
					</thead>
					<?php
						if ($ai_start_month==1 && $ai_start_day == 1 && checkIfNominalAccount($ai_details_account_code)==true)
						{
							$report_starting_balance = 0;
						}
						elseif ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($ai_details_account_code)==true)
						{
							$report_starting_balance = getDebitCreditSumValue($ai_details_account_code,$ai_currency_code,$selected_start_date_year,$start_date_minus_1,$ai_budget_id,$ai_donor_id,$component_id="",$activity_id="",$ai_other_cost_id,$ai_staff_id,$ai_benefits_id,$ai_vehicle_id,$ai_equipment_id,$ai_item_id,$consolidate_reports,$use_obligated_value);		
						}
						else
						{
							$report_starting_balance = getDebitCreditSumValue($ai_details_account_code,$ai_currency_code,$system_start_date,$start_date_minus_1,$ai_budget_id,$ai_donor_id,$component_id="",$activity_id="",$ai_other_cost_id,$ai_staff_id,$ai_benefits_id,$ai_vehicle_id,$ai_equipment_id,$ai_item_id,$consolidate_reports,$use_obligated_value);	
						}			
						$report_main_balance = $report_starting_balance;	
					?>
					<tbody>
                    <tr>
                      <td colspan="18" align="right"><?php echo $ai_start_year." starting balance"; ?></td>
                      <td align="right"><b><?php echo numberFormat($report_starting_balance); ?></b></td>
                    </tr>
					<?php
						if ($use_obligated_value==1)
						{
							$sql = "SELECT		SQL_BUFFER_RESULT
												SQL_CACHE 
												bc_id AS gl_id,
												bc_voucher_id  AS gl_reference_number,
												bc_date AS gl_date,
												bc_voucher_type AS gl_report_type,
												bc_voucher_number  AS gl_report_number,
												bc_description AS gl_description,
												bc_account_code AS gl_account_code,
												bc_currency_code AS gl_currency_code,
												bc_debit AS gl_debit,
												bc_credit AS gl_credit,
												bc_budget_id AS gl_budget_id,
												bc_donor_id AS gl_donor_id,
												bc_component_id  AS gl_component_id,
												bc_activity_id AS gl_activity_id,
												bc_other_cost_id AS gl_other_cost_id,
												bc_staff_id AS gl_staff_id,
												bc_benefits_id AS gl_benefits_id,
												bc_vehicle_id AS gl_vehicle_id,
												bc_equipment_id AS gl_equipment_id,
												bc_item_id AS gl_item_id
									FROM		tbl_budget_controller
									WHERE		bc_date	BETWEEN '$start_date' AND '$end_date'
									AND			bc_account_code = '$ai_details_account_code' ";
							if ($consolidate_reports != 1)
							{
								$sql .= "AND	bc_currency_code = '$ai_currency_code' ";
							}
							if ($ai_budget_id!="")
							{
								$sql .= "AND	bc_budget_id = '$ai_budget_id' ";
							}
							if ($ai_donor_id!="")
							{
								$sql .= "AND	bc_donor_id = '$ai_donor_id' ";
							}								
							if ($ai_staff_id!="")
							{
								$sql .= "AND	bc_staff_id = '$ai_staff_id' ";
							}
							if ($ai_other_cost_id!="")
							{
								$sql .= "AND	bc_other_cost_id  = '$ai_other_cost_id' ";
							}	
							if ($ai_benefits_id!="")
							{
								$sql .= "AND	bc_benefits_id  = '$ai_benefits_id' ";
							}	
							if ($ai_vehicle_id!="")
							{
								$sql .= "AND	bc_vehicle_id  = '$ai_vehicle_id' ";
							}
							if ($ai_equipment_id!="")
							{
								$sql .= "AND	bc_equipment_id  = '$ai_equipment_id' ";
							}																								
							$sql 	.="ORDER BY bc_date,bc_voucher_type,bc_voucher_number ASC";						
						}
						else
						{
							$sql = "SELECT		SQL_BUFFER_RESULT
												SQL_CACHE 
												gl_id AS gl_id,
												gl_reference_number AS gl_reference_number,
												gl_date AS gl_date,
												gl_report_type AS gl_report_type,
												gl_report_number AS gl_report_number,
												gl_description AS gl_description,
												gl_account_code AS gl_account_code,
												gl_currency_code AS gl_currency_code,
												gl_debit AS gl_debit,
												gl_credit AS gl_credit,
												gl_budget_id AS gl_budget_id,
												gl_donor_id AS gl_donor_id,
												gl_component_id AS gl_component_id,
												gl_activity_id AS gl_activity_id,
												gl_other_cost_id AS gl_other_cost_id,
												gl_staff_id AS gl_staff_id,
												gl_benefits_id AS gl_benefits_id,
												gl_vehicle_id AS gl_vehicle_id,
												gl_equipment_id AS gl_equipment_id,
												gl_item_id AS gl_item_id
									FROM		tbl_general_ledger
									WHERE		gl_date
									BETWEEN		'$start_date'
									AND			'$end_date'
									AND			gl_account_code = '$ai_details_account_code' ";
							if ($consolidate_reports != 1)
							{
								$sql .= "AND	gl_currency_code = '$ai_currency_code' ";
							}
							if ($ai_budget_id!="")
							{
								$sql .= "AND	gl_budget_id = '$ai_budget_id' ";
							}
							if ($ai_donor_id!="")
							{
								$sql .= "AND	gl_donor_id = '$ai_donor_id' ";
							}								
							if ($ai_staff_id!="")
							{
								$sql .= "AND	gl_staff_id = '$ai_staff_id' ";
							}
							if ($ai_other_cost_id!="")
							{
								$sql .= "AND	gl_other_cost_id  = '$ai_other_cost_id' ";
							}	
							if ($ai_benefits_id!="")
							{
								$sql .= "AND	gl_benefits_id  = '$ai_benefits_id' ";
							}	
							if ($ai_vehicle_id!="")
							{
								$sql .= "AND	gl_vehicle_id  = '$ai_vehicle_id' ";
							}
							if ($ai_equipment_id!="")
							{
								$sql .= "AND	gl_equipment_id  = '$ai_equipment_id' ";
							}																								
							$sql 	.="ORDER BY gl_date,gl_report_type,gl_report_number ASC";
						}
						$rs = mysql_query($sql) or die("Error in sql : module : account.inquiry.details.php ".$sql." ".mysql_error());
						$total_rows = mysql_num_rows($rs);	
						if($total_rows == 0)
						{
						?>
							<script language="javascript" type="text/javascript">
								alert("No records found!");
								window.close();
							</script>
						<?php
						}
						else
						{
							for($row_number=0;$row_number<$total_rows;$row_number++)
							{	
								$rows = mysql_fetch_array($rs);
								$gl_date = $rows['gl_date'];
								$gl_reference_number = $rows['gl_reference_number'];
								$gl_report_type = $rows['gl_report_type'];
								$gl_report_number = $rows['gl_report_number'];
								$gl_description = $rows['gl_description'];
								$gl_account_code  = $rows['gl_account_code'];
								$gl_currency_code = $rows['gl_currency_code'];
								$gl_debit = $rows['gl_debit'];
								$gl_credit = $rows['gl_credit'];
								$gl_budget_id = $rows['gl_budget_id'];
								$gl_donor_id = $rows['gl_donor_id'];
								$gl_component_id = $rows['gl_component_id'];
								$gl_activity_id  = $rows['gl_activity_id'];
								$gl_other_cost_id = $rows['gl_other_cost_id'];
								$gl_staff_id = $rows['gl_staff_id'];
								$gl_benefits_id = $rows['gl_benefits_id'];
								$gl_vehicle_id = $rows['gl_vehicle_id'];
								$gl_equipment_id = $rows['gl_equipment_id'];
								$gl_item_id = $rows['gl_item_id'];
								
								$gl_account_code = getSubsidiaryLedgerAccountTitleByid($gl_account_code);
								$gl_budget_id =  getBudgetDescription($gl_budget_id);
								$gl_donor_id =  getDonorDescription($gl_donor_id);
								$gl_component_id =  getComponentDescription($gl_component_id);
								$gl_activity_id = getActivityName($gl_activity_id);
								$gl_other_cost_id =  getOtherCostDescription($gl_other_cost_id);
								$gl_staff_id =  getStaffDescription($gl_staff_id);
								$gl_benefits_id =  getBenefitsDescription($gl_benefits_id);
								$gl_vehicle_id =  getVehicleDescription($gl_vehicle_id);
								$gl_equipment_id =  getEquipmentDescription($gl_equipment_id);
								$gl_item_id = getItemDescription($gl_item_id);

								if ($gl_currency_code != $ai_currency_code && $consolidate_reports == 1)
								{
									#covertExchangeRate($from_currency,$to_currency,$rate_date,$rate_value)
									$gl_debit = covertExchangeRate($gl_currency_code,$ai_currency_code,$gl_date,$gl_debit);
									$gl_credit = covertExchangeRate($gl_currency_code,$ai_currency_code,$gl_date,$gl_credit);
									
								}
								$arr_gl_debit[] = $gl_debit;
								$arr_gl_credit[] = $gl_credit;
								
								switch ($gl_report_type)
								{
									case "DV":
										$sql2 = "SELECT SQL_BUFFER_RESULT
														SQL_CACHE
														dv_payee,
														dv_description,
														dv_date
												FROM	tbl_disbursement_voucher
												WHERE	dv_id = '$gl_reference_number'";
										$rs2 = mysql_query($sql2) or die("Error in sql : module : account.inquiry.details.php ".$sql2." ".mysql_error());
										$rows2 = mysql_fetch_array($rs2);
										$payee = $rows2['dv_payee'];
										$description = $rows2['dv_description'];		
										$record_payee = getPayeeById($payee);									
									break;
									case "JV":
										$sql3 = "SELECT SQL_BUFFER_RESULT
														SQL_CACHE
														jv_payee,
														jv_description,
														jv_date
												FROM	tbl_journal_voucher
												WHERE	jv_id = '$gl_reference_number'";
										$rs3 = mysql_query($sql3) or die("Error in sql : module : account.inquiry.details.php ".$sql3." ".mysql_error());
										$rows3 = mysql_fetch_array($rs3);
										$payee = $rows3['jv_payee'];
										$description = $rows3['jv_description'];											
										$record_payee = getPayeeById($payee);									
									break;
									case "CV":
										$sql4 = "SELECT SQL_BUFFER_RESULT
														SQL_CACHE
														cv_payee,
														cv_description,
														cv_date
												FROM	tbl_cash_voucher
												WHERE	cv_id = '$gl_reference_number'";
										$rs4 = mysql_query($sql4) or die("Error in sql : module : account.inquiry.details.php ".$sql4." ".mysql_error());
										$rows4 = mysql_fetch_array($rs4);
										$payee = $rows4['cv_payee'];
										$description = $rows4['cv_description'];
										$record_payee = getPayeeById($payee);									
									break;																				
								}
								if ($gl_debit==0 && $gl_credit!=0)
								{
									$report_ending_balance = $report_starting_balance-$gl_credit;
								}
								elseif ($gl_debit!=0 && $gl_credit==0)
								{
									$report_ending_balance = $report_starting_balance+$gl_debit;
								}
								$report_starting_balance = $report_ending_balance;
							?>						
						<tr>
						  <td valign="top"><?php echo $gl_date; ?></td>
						  <td valign="top"><?php echo $gl_report_type; ?></td>
						  <td valign="top"><?php echo $gl_report_number; ?></td>
						  <td valign="top"><?php echo $record_payee; ?></td>
						  <td valign="top"><?php echo stripslashes($gl_description); ?></td>
						  <td valign="top"><?php echo $gl_account_code; ?></td>
						  <td valign="top"><?php echo $gl_budget_id; ?>&nbsp;</td>
						  <td valign="top"><?php echo $gl_donor_id; ?>&nbsp;</td>
						  <td valign="top"><?php echo $gl_component_id; ?>&nbsp;</td>
						  <td valign="top"><?php echo $gl_other_cost_id; ?>&nbsp;</td>
						  <td valign="top"><?php echo $gl_activity_id; ?>&nbsp;</td>
						  <td valign="top"><?php echo $gl_staff_id; ?>&nbsp;</td>
						  <td valign="top"><?php echo $gl_benefits_id; ?>&nbsp;</td>
						  <td valign="top"><?php echo $gl_vehicle_id; ?>&nbsp;</td>
						  <td valign="top"><?php echo $gl_equipment_id; ?>&nbsp;</td>
                          <td valign="top"><?php echo $gl_item_id; ?>&nbsp;</td>
						  <td align="right" valign="top"><?php echo numberFormat($gl_debit); ?></td>
						  <td align="right" valign="top"><?php echo numberFormat($gl_credit); ?></td>
						  <td align="right" valign="top"><?php echo numberFormat($report_ending_balance); ?></td>
						</tr>
					<?php
						}
						$total_debit = array_sum($arr_gl_debit);
						$total_credit = array_sum($arr_gl_credit);
					}
					?>
					</tbody>
					<tfoot>
						<tr>
						  <td colspan="16" align='right'><b>Report Totals:</b></td>
						  <td align='right'><b><?php echo numberFormat($total_debit); ?></b></td>
						  <td align='right'><b><?php echo numberFormat($total_credit); ?></b></td>
						  <td>&nbsp;</td>
						</tr>	
						<tr>
						  <td colspan="16" align='right'><b>Beginning Balance</b></td>
						  <td align='right'><b><?php echo numberFormat($report_main_balance); ?></b></td>
						  <td align='right'><b>Ending Balance:</b></td>
						  <td align='right'><b><?php echo numberFormat($report_ending_balance); ?></b></td>
						</tr>											
					</tfoot>
                  </table>
				  <!-- End of Report Table -->				  
				  </td>
				</tr>
			</table>	
		</td>
	</tr>
</table>
<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to 'File' -> 'Page Setup...' then erase the text in the 'Headers and Footers' field.
	</em>
</div>