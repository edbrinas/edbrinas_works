<?php 
$report_name = 'Aging of Advances to Officers and Employees';
include('./../includes/header.report.php');

$rep_currency_code = $_POST['aoe_currency_code'];
$aoe_end_year = $_POST['aoe_end_year'];
$aoe_end_month = $_POST['aoe_end_month'];
$aoe_end_day = $_POST['aoe_end_day'];
$consolidate_reports = $_POST['consolidate_reports'];
/*
$aoe_start_year = $_POST['aoe_start_year'];
$aoe_start_month = $_POST['aoe_start_month'];
$aoe_start_day = $_POST['aoe_start_day'];
$start_date = dateFormat($aoe_start_month,$aoe_start_day,$aoe_start_year);
*/
$end_date = dateFormat($aoe_end_month,$aoe_end_day,$aoe_end_year);
$rep_currency_name = getConfigurationValueById($rep_currency_code);

$minus_60_days = date("Y-m-d", mktime(0, 0, 0, $aoe_end_month, $aoe_end_day-60, $aoe_end_year));
$minus_30_days = date("Y-m-d", mktime(0, 0, 0, $aoe_end_month, $aoe_end_day-30, $aoe_end_year));
$minus_365_days = date("Y-m-d", mktime(0, 0, 0, $aoe_end_month, $aoe_end_day, $aoe_end_year-1));

$still_due_start_date = $minus_60_days;
$still_due_end_date = $minus_30_days;
?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br />
					<?php echo upperCase(getCompanyName()); ?>
					<br />
					<?php echo upperCase(getCompanyAddress()); ?><br />
					As of <?php echo $end_date; ?><br>
					ACB-<?php echo $rep_currency_name; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
				  <td scope='col' valign=top>
				  <!-- Start of Inner Table -->
				  <table width="100%" border="0" cellspacing="2" cellpadding="2">
                    <thead>
						<tr>
						  <th rowspan="2" scope="col">Name</th>
						  <th rowspan="2" scope="col">Amount</th>
						  <th scope="col">Not Due</th>
						  <th scope="col">Still Due</th>
						  <th colspan="2" scope="col">Past Due (over 60 days)</th>
					    </tr>
						<tr>
						  <th scope="col">(01-30 days)</th>
						  <th scope="col">(31-60 days)</th>
						  <th scope="col">Current Year</th>
						  <th scope="col">Previous Years</th>
						</tr>
					</thead>
					<tbody>
					<?php
						$aoe_sl_id = getSlIdByAccountTitle("Advances to Officers & Employees");
						$sql = "SELECT 		SQL_BUFFER_RESULT
											SQL_CACHE  	
								DISTINCT 	a.gl_staff_id AS gl_staff_id
								FROM		tbl_general_ledger a,
											tbl_staff b
								WHERE		a.gl_account_code = '$aoe_sl_id'
								AND			a.gl_date <= '$end_date' ";
						if ($consolidate_reports == "")
						{
							$sql .= "AND	a.gl_currency_code = '$rep_currency_code'";
						}	
						$sql .= " ORDER BY b.staff_description ASC";		
												
						$rs = mysql_query($sql) or die("Error in sql ".$sql." ".mysql_error());										
						while($rows=mysql_fetch_array($rs))
						{
							$gl_staff_id = $rows['gl_staff_id'];
							
							$sql2 = "	SELECT 	SQL_BUFFER_RESULT
												SQL_CACHE 
												gl_date, 	
												gl_currency_code,
												gl_debit,
												gl_credit
										FROM 	tbl_general_ledger
										WHERE 	gl_staff_id = '$gl_staff_id'
										AND		gl_account_code = '$aoe_sl_id' 
										AND		gl_date <= '$end_date' ";
							if ($consolidate_reports == "")
							{
								$sql2 .= "AND	gl_currency_code = '$rep_currency_code'";
							}	
							$rs2 = mysql_query($sql2) or die("Error in sql ".$sql2." ".mysql_error());	

							$arr_gl_debit = "";
							$arr_gl_credit = "";

							while($rows2=mysql_fetch_array($rs2))
							{
								$gl_date = $rows2['gl_date'];
								$gl_currency_code = $rows2['gl_currency_code'];
								$gl_debit = $rows2['gl_debit'];
								$gl_credit = $rows2['gl_credit'];
								
								if ($rep_currency_code != $gl_currency_code && $consolidate_reports == 1)
								{
									$gl_debit = covertExchangeRate($gl_currency_code,$rep_currency_code,$gl_date,$gl_debit);
									$gl_credit = covertExchangeRate($gl_currency_code,$rep_currency_code,$gl_date,$gl_credit);
								}
									
								$arr_gl_debit[] = $gl_debit;
								$arr_gl_credit[] = $gl_credit;
							}	
							
							if ($arr_gl_debit!=0)
							{
								$total_gl_debit = array_sum($arr_gl_debit);
							}
							else
							{
								$total_gl_debit = 0;
							}
							if ($arr_gl_credit!=0)
							{
								$total_gl_credit = array_sum($arr_gl_credit);									
							}
							else
							{
								$total_gl_credit = 0;
							}
							
							$gl_staff_name = getStaffDescription($gl_staff_id);
							$amount_total = $total_gl_debit-$total_gl_credit;
							$arr_amount_total[] = $amount_total;
							$amt_total = numberFormat($amount_total);
							if ($amt_total!=0)
							{
							?>
							<tr>
							  <td><?php echo $gl_staff_name; ?></td>
							  <td align="right"><?php echo $amt_total; ?></td>
							  <td align="right">&nbsp;
							  <?php
								$sql3 = "	SELECT 	SQL_BUFFER_RESULT
													SQL_CACHE 
													gl_date, 	
													gl_currency_code,
													gl_debit,
													gl_credit
											FROM 	tbl_general_ledger
											WHERE 	gl_staff_id = '$gl_staff_id'
											AND		gl_account_code = '$aoe_sl_id'
											AND		gl_date
											BETWEEN	'$minus_30_days'
											AND		'$end_date' ";
								if ($consolidate_reports == "")
								{
									$sql3 .= "AND	gl_currency_code = '$rep_currency_code'";
								}

								$rs3 = mysql_query($sql3) or die("Error in sql ".$sql3." ".mysql_error());	
								$total_rows_3 = mysql_num_rows($rs3);
								if ($total_rows_3!=0)
								{
									$arr_gl_debit_not_due = "";
									$arr_gl_credit_not_due = "";
									while($rows3=mysql_fetch_array($rs3))
									{
										$gl_date_not_due = $rows3['gl_date'];
										$gl_currency_code_not_due = $rows3['gl_currency_code'];
										$gl_debit_not_due = $rows3['gl_debit'];
										$gl_credit_not_due = $rows3['gl_credit'];
										
										if ($rep_currency_code != $gl_currency_code_not_due && $consolidate_reports == 1)
										{
											$gl_debit_not_due = covertExchangeRate($gl_currency_code_not_due,$rep_currency_code,$gl_date_not_due,$gl_debit_not_due);
											$gl_credit_not_due = covertExchangeRate($gl_currency_code_not_due,$rep_currency_code,$gl_date_not_due,$gl_credit_not_due);
										}
											
										$arr_gl_debit_not_due[] = $gl_debit_not_due;
										$arr_gl_credit_not_due[] = $gl_credit_not_due;
									}	
									$total_gl_debit_not_due = array_sum($arr_gl_debit_not_due);
									$total_gl_credit_not_due = array_sum($arr_gl_credit_not_due);
									$amount_total_not_due = $total_gl_debit_not_due-$total_gl_credit_not_due;
								}
								else
								{
									$amount_total_not_due = 0;
								}
								$arr_amount_total_not_due[] = $amount_total_not_due;
								$amt_total_not_due = numberFormat($amount_total_not_due);
								echo $amt_total_not_due;																  
							  ?>
							  </td>
							  <td align="right">&nbsp;
							  <?php
								$sql4 = "	SELECT 	SQL_BUFFER_RESULT
													SQL_CACHE 
													gl_date, 	
													gl_currency_code,
													gl_debit,
													gl_credit
											FROM 	tbl_general_ledger
											WHERE 	gl_staff_id = '$gl_staff_id'
											AND		gl_account_code = '$aoe_sl_id'
											AND		gl_date
											BETWEEN	'$still_due_start_date'
											AND		'$still_due_end_date' ";
								if ($consolidate_reports == "")
								{
									$sql4 .= "AND	gl_currency_code = '$rep_currency_code'";
								}												
								$rs4 = mysql_query($sql4) or die("Error in sql ".$sql4." ".mysql_error());	
								$total_rows_4 = mysql_num_rows($rs4);

								if ($total_rows_4!=0)
								{
									$arr_gl_debit_still_due = "";
									$arr_gl_credit_still_due = "";
									while($rows4=mysql_fetch_array($rs4))
									{
										$gl_date_still_due = $rows4['gl_date'];
										$gl_currency_code_still_due = $rows4['gl_currency_code'];
										$gl_debit_still_due = $rows4['gl_debit'];
										$gl_credit_still_due = $rows4['gl_credit'];

										if ($rep_currency_code != $gl_currency_code_still_due && $consolidate_reports == 1)
										{
											$gl_debit_still_due = covertExchangeRate($gl_currency_code_still_due,$rep_currency_code,$gl_date_still_due,$gl_debit_still_due);
											$gl_credit_still_due = covertExchangeRate($gl_currency_code_still_due,$rep_currency_code,$gl_date_still_due,$gl_credit_still_due);
										}
											
										$arr_gl_debit_still_due[] = $gl_debit_still_due;
										$arr_gl_credit_still_due[] = $gl_credit_still_due;
									}
									$total_gl_debit_still_due = array_sum($arr_gl_debit_still_due);
									$total_gl_credit_still_due = array_sum($arr_gl_credit_still_due);
									$amount_total_still_due = $total_gl_debit_still_due-$total_gl_credit_still_due;
								}	
								else
								{
									$amount_total_still_due = 0;
								}
								$arr_amount_total_still_due[] = $amount_total_still_due;
								$amt_total_still_due = numberFormat($amount_total_still_due);
								echo $amt_total_still_due;						  
							  ?>
							  </td>
							  <td align="right">&nbsp;
							  <?php
								$sql5 = "	SELECT 	SQL_BUFFER_RESULT
													SQL_CACHE 
													gl_date, 	
													gl_currency_code,
													gl_debit,
													gl_credit
											FROM 	tbl_general_ledger
											WHERE 	gl_staff_id = '$gl_staff_id'
											AND		gl_account_code = '$aoe_sl_id'
											AND		gl_date
											BETWEEN	'$minus_365_days'
											AND		'$minus_60_days' ";
								if ($consolidate_reports == "")
								{
									$sql5 .= "AND	gl_currency_code = '$rep_currency_code'";
								}															
								$rs5 = mysql_query($sql5) or die("Error in sql ".$sql5." ".mysql_error());	
								$total_rows_5 = mysql_num_rows($rs5);

								if ($total_rows_5!=0)
								{
									$arr_gl_debit_past_current_due = "";
									$arr_gl_credit_past_current_due = "";
									while($rows5=mysql_fetch_array($rs5))
									{
										$gl_date_past_current_due = $rows5['gl_date'];
										$gl_currency_code_past_current_due = $rows5['gl_currency_code'];
										$gl_debit_past_current_due = $rows5['gl_debit'];
										$gl_credit_past_current_due = $rows5['gl_credit'];

										if ($rep_currency_code != $gl_currency_code_past_current_due && $consolidate_reports == 1)
										{
											$gl_debit_past_current_due = covertExchangeRate($gl_currency_code_past_current_due,$rep_currency_code,$gl_date_past_current_due,$gl_debit_past_current_due);
											$gl_credit_past_current_due = covertExchangeRate($gl_currency_code_past_current_due,$rep_currency_code,$gl_date_past_current_due,$gl_credit_past_current_due);
										}
											
										$arr_gl_debit_past_current_due[] = $gl_debit_past_current_due;
										$arr_gl_credit_past_current_due[] = $gl_credit_past_current_due;
									}
									$total_gl_debit_past_current_due = array_sum($arr_gl_debit_past_current_due);
									$total_gl_credit_past_current_due = array_sum($arr_gl_credit_past_current_due);
									$amount_total_past_current_due = $total_gl_debit_past_current_due-$total_gl_credit_past_current_due;
								}	
								else
								{
									$amount_total_past_current_due = 0;
								}
								$arr_amount_total_past_current_due[] = $amount_total_past_current_due;
								$amt_total_past_current_due = numberFormat($amount_total_past_current_due);
								echo $amt_total_past_current_due;						  
							  ?>									  
							  </td>
							  <td align="right">&nbsp;
							  <?php
								$sql6 = "	SELECT 	SQL_BUFFER_RESULT
													SQL_CACHE 
													gl_date, 	
													gl_currency_code,
													gl_debit,
													gl_credit
											FROM 	tbl_general_ledger
											WHERE 	gl_staff_id = '$gl_staff_id'
											AND		gl_account_code = '$aoe_sl_id'
											AND		gl_date <= '$minus_365_days' ";
								if ($consolidate_reports == "")
								{
									$sql6 .= "AND	gl_currency_code = '$rep_currency_code'";
								}															
								$rs6 = mysql_query($sql6) or die("Error in sql ".$sql6." ".mysql_error());	
								$total_rows_6 = mysql_num_rows($rs6);

								if ($total_rows_6!=0)
								{
									$arr_gl_debit_past_previous_due = "";
									$arr_gl_credit_past_previous_due = "";
									while($rows6=mysql_fetch_array($rs6))
									{
										$gl_date_past_previous_due = $rows6['gl_date'];
										$gl_currency_code_past_previous_due = $rows6['gl_currency_code'];
										$gl_debit_past_previous_due = $rows6['gl_debit'];
										$gl_credit_past_previous_due = $rows6['gl_credit'];

										if ($rep_currency_code != $gl_currency_code_past_previous_due && $consolidate_reports == 1)
										{
											$gl_debit_past_previous_due = covertExchangeRate($gl_currency_code_past_previous_due,$rep_currency_code,$gl_date_past_previous_due,$gl_debit_past_previous_due);
											$gl_credit_past_previous_due = covertExchangeRate($gl_currency_code_past_previous_due,$rep_currency_code,$gl_date_past_previous_due,$gl_credit_past_previous_due);
										}
											
										$arr_gl_debit_past_previous_due[] = $gl_debit_past_previous_due;
										$arr_gl_credit_past_previous_due[] = $gl_credit_past_previous_due;
									}
									$total_gl_debit_past_previous_due = array_sum($arr_gl_debit_past_previous_due);
									$total_gl_credit_past_previous_due = array_sum($arr_gl_credit_past_previous_due);
									$amount_total_past_previous_due = $total_gl_debit_past_previous_due-$total_gl_credit_past_previous_due;
								}	
								else
								{
									$amount_total_past_previous_due = 0;
								}
								$arr_amount_total_past_previous_due[] = $amount_total_past_previous_due;
								$amt_total_past_previous_due = numberFormat($amount_total_past_previous_due);
								echo $amt_total_past_previous_due;						  
							  ?>									  
							  </td>
							</tr>										
							<?php
							}
						}			
					?>
					</tbody>
					<tfoot>
					<?php
						if (count($arr_amount_total)!=0)
						{
							$amount_grand_total = array_sum($arr_amount_total);
						}
						else
						{
							$amount_grand_total = 0;
						}
						$amount_grand_total = numberFormat($amount_grand_total);
						
						
						if (count($arr_amount_total_not_due)!=0)
						{
							$amount_not_due_grand_total = array_sum($arr_amount_total_not_due);
						}
						else
						{
							$amount_not_due_grand_total = 0;
						}
						$amount_not_due_grand_total = numberFormat($amount_not_due_grand_total);

						if (count($arr_amount_total_still_due)!=0)
						{
							$amount_still_due_grand_total = array_sum($arr_amount_total_still_due);
						}
						else
						{
							$amount_still_due_grand_total = 0;
						}						
						$amount_still_due_grand_total = numberFormat($amount_still_due_grand_total);
						
						if (count($arr_amount_total_past_current_due)!=0)
						{
							$amount_past_current_due_grand_total = array_sum($arr_amount_total_past_current_due);
						}
						else
						{
							$amount_past_current_due_grand_total = 0;
						}
						$amount_past_current_due_grand_total = numberFormat($amount_past_current_due_grand_total);
						
						if (count($arr_amount_total_past_previous_due)!=0)
						{
							$amount_past_previous_due_grand_total = array_sum($arr_amount_total_past_previous_due);
						}
						else
						{
							$amount_past_previous_due_grand_total = 0;
						}						
						$amount_past_previous_due_grand_total = numberFormat($amount_past_previous_due_grand_total);
						
					?>
						<tr>
						  <td>&nbsp;</td>
						   <td align="right"><hr></td>
						   <td align="right"><hr></td>
						   <td align="right"><hr></td>
						   <td align="right"><hr></td>
						   <td align="right"><hr></td>
						</tr>					
						<tr>
						   <td align="right">Total <?php echo $rep_currency_name; ?> :</td>
						   <td align="right"><?php echo $amount_grand_total; ?></td>
						   <td align="right"><?php echo $amount_not_due_grand_total; ?></td>
						   <td align="right"><?php echo $amount_still_due_grand_total; ?></td>
						   <td align="right"><?php echo $amount_past_current_due_grand_total; ?></td>
						   <td align="right"><?php echo $amount_past_previous_due_grand_total; ?></td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						   <td align="right"><hr><hr></td>
						   <td align="right"><hr><hr></td>
						   <td align="right"><hr><hr></td>
						   <td align="right"><hr><hr></td>
						   <td align="right"><hr><hr></td>
						</tr>						
					</tfoot>						
                  </table>
				  <!-- End of Inner Table -->				  
				  </td>
				</tr>
			</table>	
		</td>
	</tr>
</table>


<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>



