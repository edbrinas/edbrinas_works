<?php 
$report_name = 'FUND ANALYSIS - by DONOR by Activity';
include('./../includes/header.report.php');

$fa_currency_code = $_POST['currency_code'];
$report_currency_code = $fa_currency_code;
$fa_start_year = $_POST['start_year'];
$fa_start_month = $_POST['start_month'];
$fa_start_day = $_POST['start_day'];
$fa_end_year = $_POST['end_year'];
$fa_end_month = $_POST['end_month'];
$fa_end_day = $_POST['end_day'];
$consolidate_reports = $_POST['consolidate_reports'];
$fa_component_id = $_POST['component_id'];
$use_obligated_values = $_POST['use_obligated_values'];

$reference_date = dateFormat(1,1,$fa_start_year);
$start_date = dateFormat($fa_start_month,$fa_start_day,$fa_start_year);
$end_date = dateFormat($fa_end_month,$fa_end_day,$fa_end_year);
$budget_identification = getBudgetIdentification($fa_start_year);

$fa_currency_name = getConfigurationValueById($fa_currency_code);
$eu_budget_name = getBudgetIdByName("EU");
$ams_budget_name = getBudgetIdByName("AMS");
$donor_budget_name = getBudgetIdByName("DONOR");
?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br />
					<?php echo upperCase(getCompanyName()); ?>
					<br />
					<?php echo upperCase(getCompanyAddress()); ?>
					<br />
					For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?>
					<br />
					ACB-<?php echo $fa_currency_name; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
				  <td scope='col' valign=top>
				  <!-- Start of Inner Table -->
					<table width="100%" border="1" cellspacing="0" cellpadding="2">
						<thead>
						  <tr>
							<th width="21%" rowspan="2" scope="col">Activities</th>
							<th colspan="5" scope="col">Budget</th>
							<th colspan="4" scope="col">Expenditures</th>
							<th width="5%" rowspan="2" scope="col">Fund Balance <?php echo $budget_identification; ?> Budget</th>
							<th width="6%" rowspan="2" scope="col">% of Utilization</th>
						  </tr>
						  <tr>
							<th width="5%" scope="col">Budget Line </th>
							<th width="4%" scope="col"># of Units </th>
							<th width="15%" scope="col">Item</th>
							<th width="7%" scope="col">Cost / Item </th>
							<th width="5%" scope="col">Amount</th>
							<th width="8%" scope="col">Total EU Expenditures</th>
							<th width="8%" scope="col">Total AMS Expenditures </th>
							<th width="8%" scope="col">Total DONOR Expenditures </th>
							<th width="8%" scope="col">Total Expenditures  to date</th>
						  </tr>
						</thead>	
						<tbody>	
						<?php
						  	$sql = "SELECT 	SQL_BUFFER_RESULT
											SQL_CACHE
											DISTINCT(fa_activity_id)
									FROM	tbl_fund_analysis_by_component_by_activity
									WHERE	fa_component_id = '$fa_component_id'
									AND		fa_budget_year = '$fa_start_year'
									ORDER BY fa_activity_id,fa_budget_id";
							$rs = mysql_query($sql) or die("Error in sql in ".$sql." ".mysql_error());	
							while($rows=mysql_fetch_array($rs))
							{
								$activity_id = $rows['fa_activity_id'];
								$activity_name = getActivityName($activity_id);
								$activity_description = getActivityDescription($activity_id);		
								$activity_count = strlen($activity_name);
							?>
							  <tr>
								<td colspan="5" bgcolor="#FFFF00"><?php echo $activity_name." [".$activity_description."]"; ?></td>
								<td bgcolor="#FFFF00">&nbsp;</td>
								<td bgcolor="#FFFF00">&nbsp;</td>
								<td bgcolor="#FFFF00">&nbsp;</td>
								<td bgcolor="#FFFF00">&nbsp;</td>
								<td bgcolor="#FFFF00">&nbsp;</td>
								<td bgcolor="#FFFF00">&nbsp;</td>
								<td bgcolor="#FFFF00">&nbsp;</td>
							  </tr>
							  <?php
							  $sql_a = "SELECT 	SQL_BUFFER_RESULT
												SQL_CACHE
												fa_activity_id,
												fa_budget_id,
												fa_account_code,
												fa_item_id,
												fa_number_of_units,
												fa_cost_per_item,
												fa_amount
										FROM	tbl_fund_analysis_by_component_by_activity
										WHERE	fa_activity_id = '$activity_id'
										AND		fa_budget_year = '$fa_start_year'
										ORDER BY fa_id";
							  $rs_a = mysql_query($sql_a) or die("Error in sql in ".$sql_a." ".mysql_error());	
							  $total_rows = mysql_num_rows($rs_a);
							  $arr_eu_expenditures = "";
							  $arr_ams_expenditures = "";
							  $arr_donor_expenditure = "";
							  for($x=0;$x<$total_rows;$x++)
							  {	
									$rows2=mysql_fetch_array($rs_a);			
									$fa_activity_id = $rows2['fa_activity_id'];
									$fa_budget_id = $rows2['fa_budget_id'];
									$fa_account_code = $rows2['fa_account_code'];
									$fa_item_id = $rows2['fa_item_id'];
									$fa_number_of_units = $rows2['fa_number_of_units'];
									$fa_cost_per_item = $rows2['fa_cost_per_item'];
									$fa_amount = $rows2['fa_amount'];
									$fa_activity_name = getActivityName($fa_activity_id);
									$fa_activity_description = getActivityDescription($fa_activity_id);
									$fa_item_name = getItemDescription($fa_item_id);
									$arr_fa_grand_total_amount[] = $fa_amount;
							  ?>
							  <tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php #echo $fa_activity_id." ".$fa_activity_description; ?></td>
								<td><?php echo $fa_budget_id; ?></td>
								<td><?php echo $fa_number_of_units; ?></td>
								<td><?php echo $fa_item_name; ?></td>
								<td align="right"><?php echo numberFormat($fa_cost_per_item); ?></td>
								<td align="right"><?php echo numberFormat($fa_amount) ?></td>
								<td align="right">
								<?php
									if ($fa_start_month!=1)
									{
										$eu_expenditures = getDebitCreditSumValue($fa_account_code,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$fa_component_id,$fa_activity_id,"","","","","",$fa_item_id,$consolidate_reports,$use_obligated_values);
									}
									else
									{										
										$eu_expenditures = getDebitCreditSumValue($fa_account_code,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$fa_component_id,$fa_activity_id,"","","","","",$fa_item_id,$consolidate_reports,$use_obligated_values);																	
									}
									$arr_eu_expenditures[] = $eu_expenditures;
									$arr_total_eu_expenditures[] = $eu_expenditures;
									echo numberFormat($eu_expenditures);
								?>								</td>
								<td align="right">
								<?php
									if ($fa_start_month!=1)
									{		
										$ams_expenditures = getDebitCreditSumValue($fa_account_code,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$fa_component_id,$fa_activity_id,"","","","","",$fa_item_id,$consolidate_reports,$use_obligated_values);							
									}								
									else
									{
										$ams_expenditures = getDebitCreditSumValue($fa_account_code,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$fa_component_id,$fa_activity_id,"","","","","",$fa_item_id,$consolidate_reports,$use_obligated_values);									
									}
									$arr_ams_expenditures[] = $ams_expenditures;
									$arr_total_ams_expenditures[] = $ams_expenditures;
									echo numberFormat($ams_expenditures);
								?>								</td>
								<td align="right">
								<?php
									if ($fa_start_month!=1)
									{
										$donor_expenditures = getDebitCreditSumValue($fa_account_code,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$fa_component_id,$fa_activity_id,"","","","","",$fa_item_id,$consolidate_reports,$use_obligated_values);										
									}
									else
									{
										$donor_expenditures = getDebitCreditSumValue($fa_account_code,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$fa_component_id,$fa_activity_id,"","","","","",$fa_item_id,$consolidate_reports,$use_obligated_values);			
									}
									$arr_donor_expenditures[] = $donor_expenditures;
									$arr_total_donor_expenditures[] = $donor_expenditures;
									echo numberFormat($donor_expenditures);
								?>								</td>
								<td align="right">
								<?php
									$expenditure_to_date = $eu_expenditures+$ams_expenditures+$donor_expenditures; 
									echo numberFormat($expenditure_to_date); 
								?>								</td>
								<td align="right"><?php $fa_fund_balance = $fa_amount-$expenditure_to_date; echo numberFormat($fa_fund_balance); ?></td>
								<td align="right">
								<?php
								if ($fa_amount!=0)
								{
									$utilization = ($expenditure_to_date/$fa_amount)*100;
								}
								else
								{
									$utilization = 0;
								}
								echo numberFormat($utilization);
								?>&nbsp;%								</td>
							  </tr>

						  <?php
						  	 }
							 if ($activity_name == $fa_activity_name)
							 {
							 	$sql1 = "SELECT SQL_BUFFER_RESULT
												SQL_CACHE
												SUM(fa_amount) AS sum_fa_amount 
										 FROM	tbl_fund_analysis_by_component_by_activity 
										 WHERE	fa_activity_id = '$activity_id'";
								$rs1 = mysql_query($sql1) or die("Error in sql in ".$sql1." ".mysql_error());
								$rows1=mysql_fetch_array($rs1);
								$sum_fa_amount = $rows1['sum_fa_amount'];																					
								
								$total_eu_expenditures = array_sum($arr_eu_expenditures);
								$total_ams_expenditures = array_sum($arr_ams_expenditures);
								$total_donor_expenditures = array_sum($arr_donor_expenditures);
							 ?>
							  <tr>
								<td colspan="5" align="right" bgcolor="#FFFF00"><b>Total:</b></td>
								<td align="right" bgcolor="#FFFF00"><b>
								  <?php
								echo numberFormat($sum_fa_amount);
								?>
								</b></td>
								<td align="right" bgcolor="#FFFF00">
								<b>
								<?php
								echo numberFormat($total_eu_expenditures);
								?>
								</b>								</td>
								<td align="right" bgcolor="#FFFF00">
								<b>
								<?php
								echo numberFormat($total_ams_expenditures);
								?>
								</b>								</td>
								<td align="right" bgcolor="#FFFF00">
								<b>
								<?php
								echo numberFormat($total_donor_expenditures);
								?>
								</b>								</td>
								<td align="right" bgcolor="#FFFF00">
								<b>
								<?php
									$total_expenditures = $total_eu_expenditures+$total_ams_expenditures+$total_donor_expenditures;
									echo numberFormat($total_expenditures);
								?>
								</b>								</td>
								<td align="right" bgcolor="#FFFF00">
								<b>
								<?php
									$total_fund_balance = $sum_fa_amount-$total_expenditures;
									echo numberFormat($total_fund_balance);
								?>
								</b>								</td>
								<td align="right" bgcolor="#FFFF00">
								<b>
								<?php
								if ($sum_fa_amount!=0)
								{
									$total_utilization = ($total_expenditures/$sum_fa_amount)*100;
								}
								else
								{
									$total_utilization = 0;
								}
								echo numberFormat($total_utilization);
								?>&nbsp;%</b></td>
							  </tr>								 
							 <?php
							 }
						  }
						?>					  							
						</tbody>
						<tfoot>
							  <tr bgcolor="#FFFF00">
								<td colspan="5" align="right"><b>Grand Total:</b></td>
								<td align="right"><b>
								  <?php
								if ($arr_fa_grand_total_amount!=0)
								{
									$fa_grand_total_amount = array_sum($arr_fa_grand_total_amount); 
								}
								echo numberFormat($fa_grand_total_amount);
								?>
								</b></td>
								<td align="right">
								<b>
								<?php
								if ($arr_total_eu_expenditures!=0)
								{								
									$fa_grand_total_eu_expenditures = array_sum($arr_total_eu_expenditures);
								}
								echo numberFormat($fa_grand_total_eu_expenditures);
								?>	
								</b>								</td>
								<td align="right">
								<b>
								<?php	
								if ($arr_total_ams_expenditures!=0)
								{							
									$fa_grand_total_ams_expenditures = array_sum($arr_total_ams_expenditures);
								}
								echo numberFormat($fa_grand_total_ams_expenditures);
								?>	
								</b>								</td>
								<td align="right">
								<b>
								<?php	
								if ($arr_total_donor_expenditures)
								{							
									$fa_grand_total_donor_expenditures = array_sum($arr_total_donor_expenditures);
								}
								echo numberFormat($fa_grand_total_donor_expenditures);
								?>		
								</b>								</td>
								<td align="right">
								<b>
								<?php
								$fa_grand_total_expenditures = $fa_grand_total_eu_expenditures+$fa_grand_total_ams_expenditures+$fa_grand_total_donor_expenditures;
								echo numberFormat($fa_grand_total_expenditures); 
								?>
								</b>								</td>
								<td align="right">								
								<b>
								<?php
								$fa_grand_total_fund_balance = $fa_grand_total_amount-$fa_grand_total_expenditures;
								echo numberFormat($fa_grand_total_fund_balance); 
								?>
								</b>								</td>
								<td align="right">
								<b>
								<?php
								if ($fa_grand_total_amount!=0)
								{
									$fa_grand_total_utilization = $fa_grand_total_expenditures/$fa_grand_total_amount;
								}
								else
								{
									$fa_grand_total_utilization = 0;
								}
								echo numberFormat($fa_grand_total_utilization); 
								?>&nbsp;%								</b>								</td>
							  </tr>					 
						</tfoot>
					</table>

				  
				  <!-- End of Inner Table -->
				  </td>
				</tr>
			</table>	
		</td>
	</tr>
</table>


<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>



