<?php 
$report_name = "TRIAL BALANCE";
include("./../includes/header.report.php");

$tb_currency_code = $_POST['tb_currency_code'];
$tb_start_year = $_POST['tb_start_year'];
$tb_start_month = $_POST['tb_start_month'];
$tb_start_day = $_POST['tb_start_day'];
$tb_end_year = $_POST['tb_end_year'];
$tb_end_month = $_POST['tb_end_month'];
$tb_end_day = $_POST['tb_end_day'];
$tb_budget_id = $_POST['tb_budget_id'];
$tb_donor_id = $_POST['tb_donor_id'];
$consolidate_reports = $_POST['consolidate_reports'];

$system_start_date = date("Y-m-d", mktime(0,0,0,1,1,2008));
$start_date_minus_1 = date("Y-m-d", mktime(0,0,0,$tb_start_month,$tb_start_day-1,$tb_start_year));

$start_date = date("Y-m-d", mktime(0,0,0,$tb_start_month,$tb_start_day,$tb_start_year));
$end_date = date("Y-m-d", mktime(0,0,0,$tb_end_month,$tb_end_day,$tb_end_year));

$tb_currency_name = getConfigurationValueById($tb_currency_code);
?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br />
					<?php echo upperCase(getCompanyName()); ?>
					<br />
					<?php echo upperCase(getCompanyAddress()); ?><br />
					For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?><br>
					ACB-<?php echo $tb_currency_name; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
				  <td scope='col'>
				  	<!--Start of inner table -->
				  	<table width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#CCCCCC">
                      <thead>
                        <tr>
                          <th width="10%" scope="col">Account Code </th>
                          <th width="30%" scope="col">Account Description </th>
                          <th width="15%" scope="col">Beginning Balance </th>
                          <th width="15%" scope="col">Debits</th>
                          <th width="15%" scope="col">Credits</th>
                          <th width="15%" scope="col">Balance</th>
                        </tr>
                      </thead>
                      <tbody>
						<?php
						$sql = "SELECT		SQL_BUFFER_RESULT
											SQL_CACHE 
											sl_id,
											sl_account_code,
											sl_account_title
								FROM		tbl_subsidiary_ledger
								ORDER BY	sl_account_code ASC";
						$rs = mysql_query(trim($sql)) or die("Error in sql : module : trial.balance.details.php ".$sql." ".mysql_error());
						while ($rows=mysql_fetch_array($rs))
						{
							?>
							<tr>
                            	<td><?php echo $rows["sl_account_code"]; ?></td>
                                <td><?php echo $rows["sl_account_title"]; ?></td>
                                <td align="right">
								<?php 
								if ($tb_start_month==1 && $tb_start_day == 1 && checkIfNominalAccount($rows["sl_id"])==true)
								{
									$starting_balance = 0;
								}
								else
								{
									$starting_balance = getDebitCreditSumValue($rows["sl_id"],$tb_currency_code,$system_start_date,$start_date_minus_1,$budget_id="",$donor_id="",$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_values); 								
								}
								echo numberFormat($starting_balance);
								?></td>
                                <td align="right">
								<?php 
								$debit_amount = getDebitOrCredit("DEBIT",$rows["sl_id"],$tb_currency_code,$start_date,$end_date,$budget_id="",$donor_id="",$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_values); 
								echo numberFormat($debit_amount);
								$arr_grand_total_debit[] = $debit_amount;
								?>
                                </td>
                                <td align="right">
								<?php 
								$credit_amount = getDebitOrCredit("CREDIT",$rows["sl_id"],$tb_currency_code,$start_date,$end_date,$budget_id="",$donor_id="",$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_values);
								echo numberFormat($credit_amount);
								$arr_grand_total_credit[] = $credit_amount;
								?></td>
                                <td align="right"><?php echo numberFormat($starting_balance+($debit_amount-$credit_amount)); ?></td>
                            </tr>                            
                            <?php
						}
						?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="3" scope="col">&nbsp;</td>
                          <td scope="col"><hr /></td>
                          <td scope="col"><hr /></td>
                          <td scope="col"><hr /></td>
                        </tr>
                        <?php
								if ($arr_grand_total_debit > 0 && $arr_grand_total_credit > 0)
								{
									$grand_total_debit = array_sum($arr_grand_total_debit);
									$grand_total_credit = array_sum($arr_grand_total_credit);
								}
							?>
                        <tr>
                          <td colspan="3" scope="col" align="right"><b>Report Totals:</b></td>
                          <td scope="col" align="right"><b><?php echo numberFormat($grand_total_debit); ?></b></td>
                          <td scope="col" align="right"><b><?php echo numberFormat($grand_total_credit); ?></b></td>
                          <td scope="col" align="right"><b><?php echo numberFormat(abs($grand_total_debit-$grand_total_credit)); ?></b></td>
                        </tr>
                      </tfoot>
				  	  <tfoot>
                      </tfoot>
			  	    </table>
			  	  <!--End of inner table --></td>
				</tr>
			</table>	
		</td>
	</tr>
</table>


<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>