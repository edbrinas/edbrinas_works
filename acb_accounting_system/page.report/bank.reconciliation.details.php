<?php 
$report_name = "BANK RECONCILIATION STATEMENT";
include("./../includes/header.report.php");

$br_start_year = $_POST['br_start_year'];
$br_start_month = $_POST['br_start_month'];
$br_start_day = $_POST['br_start_day'];
$br_end_year = $_POST['br_end_year'];
$br_end_month = $_POST['br_end_month'];
$br_end_day = $_POST['br_end_day'];
$br_bank_id = $_POST['br_bank_id'];
$bank_account_ending_balance = $_POST['bank_account_ending_balance'];

$start_date = dateFormat($br_start_month,$br_start_day,$br_start_year);
$start_date_minus_1 = dateFormat($br_start_month,$br_start_day-1,$br_start_year);
$end_date = dateFormat($br_end_month,$br_end_day,$br_end_year);

$sql = "SELECT 	SQL_BUFFER_RESULT
				SQL_CACHE
				bank_id,
				bank_name,
				bank_account_name,
				bank_account_number,
				bank_account_account_type,
				bank_account_sl_code,
				bank_account_currency_code,
				bank_signed_cheque,
				bank_deposit_in_transit,
				bank_outstanding_cheque,				
				bank_inserted_by,
				bank_date_inserted
		FROM	tbl_bank
		WHERE	bank_id = '$br_bank_id'";
$rs = mysql_query($sql) or die("Query Error" .mysql_error());
$rows = mysql_fetch_array($rs);
$bank_id = $rows["bank_id"];
$bank_name = $rows["bank_name"];
$bank_account_number = $rows["bank_account_number"];
$bank_account_currency_code = $rows["bank_account_currency_code"];
$bank_currency_code_id = $rows["bank_account_currency_code"];
$bank_account_sl_code = $rows["bank_account_sl_code"];
$bank_signed_cheque = $rows["bank_signed_cheque"];
$bank_deposit_in_transit = $rows["bank_deposit_in_transit"];
$bank_outstanding_cheque = $rows["bank_outstanding_cheque"];
$bank_account_currency_code = getConfigurationValueById($bank_account_currency_code);

						
$sql_a1 = "	SELECT 	sl_details_reference_number,
					sl_details_starting_balance
			FROM	tbl_subsidiary_ledger_details
			WHERE	sl_details_year = '$br_start_year'
			AND		sl_details_reference_number = '$bank_account_sl_code'
			AND		sl_details_currency_code = '$bank_currency_code_id'";

$rs_a1 = mysql_query($sql_a1) or die("Error in sql : module : bank.reconciliation.details.php ".$sql_a." ".mysql_error());
$rows_a1 = mysql_fetch_array($rs_a1);
$gl_account_code = $rows_a1['sl_details_reference_number'];
$sl_details_starting_balance = $rows_a1['sl_details_starting_balance'];

#$sl_details_ending_balance = getEndingBalanceById($bank_account_sl_code,$br_start_year,$start_date_minus_1,$bank_currency_code_id,$donor_id="",$budget_id="",1);

$sl_details_ending_balance = getDebitCreditSumValue($bank_account_sl_code,$bank_currency_code_id,$br_start_year,$start_date_minus_1,$budget_id="",$donor_id="",$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_report="",$use_obligated_values)

$sum_sb_eb = $sl_details_starting_balance - $sl_details_ending_balance;

$sql_b2 = "	SELECT 	SUM(gl_debit) AS sum_debit,
						SUM(gl_credit) AS sum_credit
				FROM 	tbl_general_ledger
				WHERE 	gl_date 
				BETWEEN '$start_date'
				AND		'$end_date'
				AND		gl_account_code = '$bank_account_sl_code'
				AND		gl_currency_code = '$bank_currency_code_id'";
$rs_b2 = mysql_query($sql_b2) or die("Error in sql : module : bank.reconciliation.details.php ".$sql_b." ".mysql_error());
$rows_b2 = mysql_fetch_array($rs_b2);
$sum_debit = $rows_b2['sum_debit'];
$sum_credit = $rows_b2['sum_credit'];			

$sum_debit_credit = $sum_credit-$sum_debit;
$sl_report_balance = $sum_sb_eb-$sum_debit_credit;
?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br />
					<?php echo upperCase(getCompanyName()); ?>
					<br />
					<?php echo upperCase(getCompanyAddress()); ?><br />
					For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
				  <td scope='col'>
				  <!-- Start of inner table -->
				  
					<table width="100%" border="0" cellspacing="2" cellpadding="2">
						<tr>
							<td width="10%"><b>Bank</b></td>
							<td width="90%"><?php echo $bank_name; ?></td>
						</tr>
						<tr>
							<td><b>Account Number</b></td>
							<td><?php echo $bank_account_number; ?></td>
						</tr>
						<tr>
							<td><b>Period Covered</b></td>
							<td>
							<?php
								echo $start_date." to ".$end_date;
							?>
							</td>
						</tr>
						<tr>
						  <td colspan="2"><table width="100%" border="1" cellspacing="0" cellpadding="2">
                            <tr>
                              <td scope="col"><table width="100%" border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                    <td scope="col" width="80%"><b>BALANCE PER BOOK</b></td>
                                    <td scope="col" width="10%" align="right"><?php echo $bank_account_currency_code; ?></td>
                                    <td scope="col" width="10%" align="right">
										<b>
										<?php
											$sl_details_balance = numberFormat($sl_report_balance);
											echo $sl_details_balance; 
										?></b>                                    </td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td scope="col">ADD (DEDUCT) ADJUSTMENTS </td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="3" scope="col" align="center">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="3" scope="col" align="center">
                                   		<table width="70%" border="0" cellspacing="2" cellpadding="2">
                                      	<tbody>
                                            <?php
											/*
											$sql_a = "SELECT 	SQL_BUFFER_RESULT
																SQL_CACHE
																transaction_description,
																transaction_amount
														FROM	tbl_other_transactions
														WHERE	transaction_bank_id = '$br_bank_id'
														AND		transaction_type = '44'
														AND		transaction_clear = '0'";
											*/
											$sql_a = "SELECT 	SQL_BUFFER_RESULT
																SQL_CACHE
																transaction_description,
																transaction_amount
														FROM	tbl_other_transactions
														WHERE	transaction_bank_id = '$br_bank_id'
														AND		transaction_type = '44'
														AND		transaction_clear = '0'";
											$rs_a = mysql_query($sql_a) or die("Error in sql ".$sql." ".mysql_error());
											while($rows_a=mysql_fetch_array($rs_a))
											{
												$arr_transaction_amount_a[] = $rows_a["transaction_amount"];
												$transaction_amount_a = $rows_a["transaction_amount"];
												$transaction_description_a = $rows_a["transaction_description"];
												?>
                                                <tr>
                                                    <td><?php echo $transaction_description_a; ?></td>
                                                    <td align="right"><?php echo $transaction_amount_a; ?></td>                                        
                                                </tr>
                                                <?php
											}
											?>
                                        </tbody>
                                    	</table>                                    
                                   	</td>
                                  </tr>
                                  <tr>
                                    <td scope="col" align="center">&nbsp;</td>
                                    <td scope="col" align="center">&nbsp;</td>
                                    <td scope="col" align="right">										
									<?php
											if (count($arr_transaction_amount_a)!=0)
											{
												$total_other_transaction_a = array_sum($arr_transaction_amount_a);
											}
											else
											{
												$total_other_transaction_a = 0;
											}
											$other_transaction_total_a = numberFormat($total_other_transaction_a);
											echo $other_transaction_total_a;
									?>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="3" scope="col" align="center">
									<?php
									if ($bank_signed_cheque==1)
									{
									?>
									<table width="70%" border="0" cellspacing="2" cellpadding="2">
                                        <tr>
                                          <th width="15%" scope="col">&nbsp;</th>
                                          <th colspan="2" scope="col">Signed disbursement vouchers for cheque preparation</th>
                                          <th width="20%" scope="col">&nbsp;</th>
                                        </tr>
                                        <tr>
                                          <th scope="col">Date</th>
                                          <th width="15%" scope="col">DV Number </th>
                                          <th width="50%" scope="col">Payee</th>
                                          <th scope="col">Amount</th>
                                        </tr>
                                        <?php
											/*
											$sql3 = "	SELECT 	SQL_BUFFER_RESULT
																SQL_CACHE
																dv.dv_id,
																dv.dv_cheque_status,
																dv.dv_number AS dv_number,
																dv.dv_cheque_number,
																dv.dv_payee AS dv_payee,
																dv.dv_approved_by,
																dv.dv_approved_by_date,
																gl.gl_reference_number,
																gl.gl_report_type,
																gl.gl_date AS gl_date,
																gl.gl_account_code,
																gl.gl_debit AS gl_debit,
																gl.gl_credit AS gl_credit
														FROM 	tbl_disbursement_voucher AS dv, 
																tbl_general_ledger AS gl
														WHERE 	gl.gl_account_code = '$bank_account_sl_code'
														AND		dv.dv_cheque_status != 3
														AND		dv.dv_cheque_status != 4
														AND		gl.gl_report_type = 'DV'
														AND		dv.dv_id = gl.gl_reference_number
														AND		dv.dv_cheque_number = ''
														AND 	dv.dv_approved_by != 0
														AND		dv.dv_approved_by_date != '0000-00-00 00:00:00'
														AND		gl.gl_date
														BETWEEN	'$start_date'
														AND		'$end_date'";
											*/
											$sql3 = "	SELECT 	SQL_BUFFER_RESULT
																SQL_CACHE
																dv.dv_date AS dv_date,
																dv.dv_number AS dv_number,
																dv.dv_payee AS dv_payee,
																cheque.cheque_amount AS amount
														FROM	tbl_disbursement_voucher AS dv,
																tbl_cheque AS cheque
														WHERE	dv.dv_id = cheque.cheque_reference_number
														AND		cheque.cheque_number = ''
														AND		dv.dv_approved_by != 0
														AND		dv.dv_approved_by_date != '0000-00-00 00:00:00'
														AND		cheque.cheque_bank_id = '$bank_account_sl_code'
														AND		cheque.cheque_status = '1'
														AND 	cheque.cheque_date
														BETWEEN	'$start_date'
														AND		'$end_date'
														ORDER BY dv.dv_id DESC";		
											$rs3 = mysql_query($sql3) or die("Error in sql3 in module: bank.recon.php ".$sql3." ".mysql_error());
											$total_rows = mysql_num_rows($rs3);	
											if ($total_rows==0)
											{
												?>
												<tr>
												  <td colspan="4">&nbsp;</td>
												</tr>
												<?php
											}
											else
											{
												for($row_number=0;$row_number<$total_rows;$row_number++)
												{
													$rows3 = mysql_fetch_array($rs3);
													$dv_number = $rows3['dv_number'];
													$dv_payee = $rows3['dv_payee'];
													$dv_date = $rows3['dv_date'];
													$amount = $rows3['amount'];
													$arr_amount[] = $amount;
													$transaction_total = numberFormat($amount);
													$dv_payee = getPayeeById($dv_payee);
												?>
													<tr>
													  <td scope="col" align="center"><?php echo $gl_date; ?></td>
													  <td scope="col" align="center"><?php echo $dv_number; ?></td>
													  <td scope="col" align="left"><?php echo $dv_payee; ?></td>
													  <td scope="col" align="right"><?php echo $transaction_total; ?></td>
													</tr>
													<?php
												}
												$total_add_deduct_adjustments = array_sum($arr_amount);
											}?>
                                    </table>
									<?php
							  		}
							  		?>									</td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col" align="right"><?php echo $bank_account_currency_code; ?></td>
                                    <td scope="col" align="right">
										<?php
											$add_deduct_adjustments_total = numberFormat($total_add_deduct_adjustments);
											echo $add_deduct_adjustments_total;
										?>                                    </td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col"><hr></td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td scope="col"><b>ADJUSTED BOOK BALANCE</b></td>
                                    <td scope="col" align="right"><?php echo $bank_account_currency_code; ?></td>
                                    <td scope="col" align="right">
										<b><?php
											$total_adjusted_book_balance = $sl_report_balance+$total_add_deduct_adjustments+$total_other_transaction_a;
											$adjusted_book_balance_total = numberFormat($total_adjusted_book_balance);
											echo $adjusted_book_balance_total;
										?></b>                                    </td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col"><hr><hr></td>
                                  </tr>
                              </table>
							  </td>
                            </tr>
                            <tr>
                              <td scope="col">
							  <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                    <td scope="col" width="80%"><b>BALANCE PER BANK </b></td>
                                    <td scope="col" width="10%" align="right"><?php echo $bank_account_currency_code; ?></td>
                                    <td scope="col" width="10%" align="right">
										<b><?php
												$sl_details_balance = numberFormat($bank_account_ending_balance);
												echo $sl_details_balance; 
										?></b>                                    </td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td scope="col">ADD (DEDUCT) ADJUSTMENTS </td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="3" scope="col" align="center"><table width="70%" border="0" cellspacing="2" cellpadding="2">
                                      <tbody>
                                        <?php
											$sql_b = "SELECT 	SQL_BUFFER_RESULT
																SQL_CACHE
																transaction_description,
																transaction_amount
														FROM	tbl_other_transactions
														WHERE	transaction_bank_id = '$br_bank_id'
														AND		transaction_type = '45'
														AND		transaction_clear = '0'";
											$rs_b = mysql_query($sql_b) or die("Error in sql ".$sql." ".mysql_error());
											while($rows_a=mysql_fetch_array($rs_b))
											{
												$arr_transaction_amount_b[] = $rows_b["transaction_amount"];
												$transaction_amount_b = $rows_b["transaction_amount"];
												$transaction_description_b = $rows_b["transaction_description"];
												?>
                                        <tr>
                                          <td><?php echo $transaction_description_b; ?></td>
                                          <td align="right"><?php echo $transaction_amount_b; ?></td>
                                        </tr>
                                        <?php
											}
											?>
                                      </tbody>
                                    </table></td>
                                  </tr>
                                  <tr>
                                    <td scope="col" align="center">&nbsp;</td>
                                    <td scope="col" align="center">&nbsp;</td>
                                    <td scope="col" align="right">
                                    <?php
											if (count($arr_transaction_amount_b)!=0)
											{
												$total_other_transaction_b = array_sum($arr_transaction_amount_b);
											}
											else
											{
												$total_other_transaction_b = 0;
											}
											$other_transaction_total_b = numberFormat($total_other_transaction_b);
											echo $other_transaction_total_b;
									?>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="3" scope="col" align="center">
									<?php
									if ($bank_deposit_in_transit==1)
									{
									?>
									<table width="70%" border="0" cellspacing="2" cellpadding="2">
                                        <tr>
                                          <th width="15%" scope="col">&nbsp;</th>
                                          <th colspan="2" scope="col">Deposit in transit </th>
                                          <th width="20%" scope="col">&nbsp;</th>
                                        </tr>
                                        <tr>
                                          <th scope="col">Date</th>
                                          <th width="15%" scope="col">JV Number </th>
                                          <th width="50%" scope="col">Payee</th>
                                          <th scope="col">Amount</th>
                                        </tr>
                                        <?php
											$sql3 = "	SELECT	SQL_BUFFER_RESULT
																SQL_CACHE
																jv.jv_id,
																jv.jv_number AS jv_number,
																jv.jv_payee AS jv_payee,
																jv.jv_approved_by,
																jv.jv_approved_by_date,
																gl.gl_reference_number,
																gl.gl_date AS gl_date,
																gl.gl_report_type,
																gl.gl_account_code,
																gl.gl_debit AS gl_debit,

																gl.gl_credit AS gl_credit
														FROM	tbl_general_ledger AS gl,
																tbl_journal_voucher AS jv
														WHERE	gl.gl_account_code = '$bank_account_sl_code'
														AND		jv.jv_id = gl.gl_reference_number
														AND		gl.gl_report_type = 'JV'
														AND		jv.jv_add_to_bank_reconciliation = '1'
														AND		gl.gl_date
														BETWEEN	'$start_date'
														AND		'$end_date'";
											$rs3 = mysql_query($sql3) or die("Error in sql3 in module: bank.recon.php ".$sql3." ".mysql_error());
											$total_rows = mysql_num_rows($rs3);	
											if ($total_rows==0)
											{
												?>
												<tr>
												  <td colspan="4">&nbsp;</td>
												</tr>
												<?php
											}
											else
											{
												for($row_number=0;$row_number<$total_rows;$row_number++)
												{
													$rows3 = mysql_fetch_array($rs3);
													$jv_number = $rows3['jv_number'];
													$jv_payee = $rows3['jv_payee'];
													$gl_date = $rows3['gl_date'];
													$gl_debit = $rows3['gl_debit'];
													$gl_credit = $rows3['gl_credit'];
													$arr_gl_debit[] = $gl_debit;
													$transaction_total = numberFormat($gl_debit);
													$jv_payee = getPayeeById($jv_payee);
												?>
												<tr>
												  <td scope="col" align="center"><?php echo $gl_date; ?></td>
												  <td scope="col" align="center"><?php echo $jv_number; ?></td>
												  <td scope="col" align="left"><?php echo $jv_payee; ?></td>
												  <td scope="col" align="right"><?php echo $transaction_total; ?></td>
												</tr>
												<?php
												}
												$total_deposit_in_transit = array_sum($arr_gl_debit);
											}?>
                                    </table>
									<?php
									}
									?>									</td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col" align="right"><?php echo $bank_account_currency_code; ?></td>
                                    <td scope="col" align="right"> 
										<?php
									  	$deposit_in_transit_total = numberFormat($total_deposit_in_transit);
										echo $deposit_in_transit_total;
									  	?>									</td>
                                  </tr>
                                  <tr>
                                    <td colspan="3" scope="col" align="center">
									<?php
									if ($bank_outstanding_cheque==1)
									{
									?>
									<table width="70%" border="0" cellspacing="2" cellpadding="2">
                                        <tr>
                                          <th width="15%" scope="col">&nbsp;</th>
                                          <th colspan="2" scope="col">Outstanding Cheque</th>
                                          <th width="20%" scope="col">&nbsp;</th>
                                        </tr>
                                        <tr>
                                          <th scope="col">Date</th>
                                          <th width="15%" scope="col">Cheque Number</th>
                                          <th width="50%" scope="col">Payee</th>
                                          <th scope="col">Amount</th>
                                        </tr>
                                        <?php
											/*
												$sql4 = "	SELECT 	SQL_BUFFER_RESULT
																	SQL_CACHE
																	dv.dv_id,
																	dv.dv_cheque_status,
																	dv.dv_number AS dv_number,
																	dv.dv_cheque_number AS cheque_number,
																	dv.dv_payee AS dv_payee,
																	dv.dv_approved_by,
																	dv.dv_approved_by_date,
																	gl.gl_reference_number,
																	gl.gl_report_type,
																	gl.gl_date AS gl_date,
																	gl.gl_account_code,
																	gl.gl_debit AS gl_debit,
																	gl.gl_credit AS gl_credit
															FROM 	tbl_disbursement_voucher AS dv, 
																	tbl_general_ledger AS gl
															WHERE 	gl.gl_account_code = '$bank_account_sl_code'
															AND		dv.dv_cheque_status != 3
															AND		dv.dv_cheque_status != 4
															AND		gl.gl_report_type = 'DV'
															AND		dv.dv_id = gl.gl_reference_number
															AND 	dv.dv_approved_by != 0
															AND		dv.dv_approved_by_date != '0000-00-00 00:00:00'
															AND		dv.dv_cheque_number != ''
															AND		dv.dv_cheque_cleared_by = 0
															AND		dv.dv_cheque_cleared_by_date = '0000-00-00 00:00:00'";
												*/
												$sql4 = "	SELECT 	SQL_BUFFER_RESULT
																	SQL_CACHE
																	dv.dv_date AS dv_date,
																	dv.dv_number AS dv_number,
																	dv.dv_payee AS dv_payee,
																	cheque.cheque_number AS cheque_number,
																	cheque.cheque_amount AS amount
															FROM	tbl_disbursement_voucher AS dv,
																	tbl_cheque AS cheque
															WHERE	dv.dv_id = cheque.cheque_reference_number
															AND		cheque.cheque_number != ''
															AND		dv.dv_approved_by != 0
															AND		dv.dv_approved_by_date != '0000-00-00 00:00:00'
															AND		cheque.cheque_bank_id = '$bank_account_sl_code'
															AND		cheque.cheque_status = '5'
															AND 	cheque.cheque_date
															BETWEEN	'$start_date'
															AND		'$end_date'
															ORDER BY dv.dv_id DESC";											
												$rs4 = mysql_query($sql4) or die("Error in sql3 in module: bank.recon.php ".$sql4." ".mysql_error());
												$total_rows4 = mysql_num_rows($rs4);	
												if ($total_rows4==0)
												{
													?>
													<tr>
													  <td colspan="4">&nbsp;</td>
													</tr>
													<?php
												}
												else
												{
													for($row_number4=0;$row_number4<$total_rows4;$row_number4++)
													{
														$rows4 = mysql_fetch_array($rs4);
														$dv_number = $rows4['dv_number'];
														$cheque_number = $rows4['cheque_number'];
														$dv_date = $rows4['dv_date'];
														$dv_payee = $rows4['dv_payee'];
														$amount = $rows4['amount'];
														$arr_amount[] = $amount;
														$transaction_total = numberFormat($amount);
														$dv_payee = getPayeeById($dv_payee);														
														?>
														<tr>
														  <td scope="col" align="center"><?php echo $dv_date; ?></td>
														  <td scope="col" align="center"><?php echo $cheque_number; ?></td>
														  <td scope="col" align="left"><?php echo $dv_payee; ?></td>
														  <td scope="col" align="right"><?php echo $transaction_total; ?></td>
														</tr>
														<?php
													}
													$total_outstanding_cheques = array_sum($arr_amount);
												}?>
                                    </table>
																		</td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col" align="right"><?php echo $bank_account_currency_code; ?></td>
                                    <td scope="col" align="right"> 
										(<?php
											$outstanding_cheques_total = numberFormat($total_outstanding_cheques);
											echo $outstanding_cheques_total;
										?>)									</td>
                                  </tr>
                                  <?php
									}
									?>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col"><hr></td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td scope="col"><b>ADJUSTED BANK BALANCE</b></td>
                                    <td scope="col" align="right"><?php echo $bank_account_currency_code; ?></td>
                                    <td scope="col" align="right">
										<b><?php
											$total_adjusted_bank_balance = $bank_account_ending_balance+$total_deposit_in_transit-$total_outstanding_cheques+$total_other_transaction_b;
											$adjusted_bank_balance_total = numberFormat($total_adjusted_bank_balance);
											echo $adjusted_bank_balance_total;
										?></b>                                    </td>
                                  </tr>
                                  <tr>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col">&nbsp;</td>
                                    <td scope="col"><hr>
                                        <hr></td>
                                  </tr>
                              </table></td>
                            </tr>
                          </table>
						  </td>
						</tr>	
						<tr>
							<td colspan="2" align="center">
							<table width="80%" border="0" cellspacing="2" cellpadding="2">
                              <tr>
                                <td width="50%" scope="col">Prepared by:</td>
                                <td width="50%" scope="col">Certified Correct by:</td>
                              </tr>
                              <tr>
                                <td scope="col">&nbsp;</td>
                                <td scope="col">&nbsp;</td>
                              </tr>
                              <tr>
                                <td scope="col"><b>Maybellene C. Mendoza</b></td>
                                <td scope="col"><b>Froyla A. Vega</b></td>
                              </tr>
                              <tr>
                                <td scope="col">Bookkeeper</td>
                                <td scope="col">Accountant</td>
                              </tr>
                              <tr>
                                <td scope="col">&nbsp;</td>
                                <td scope="col">&nbsp;</td>
                              </tr>
                              <tr>
                                <td scope="col">Noted by:</td>
                                <td scope="col">&nbsp;</td>
                              </tr>
                              <tr>
                                <td scope="col">&nbsp;</td>
                                <td scope="col">&nbsp;</td>
                              </tr>
                              <tr>
                                <td scope="col"><b>Wilfredo J. Obien </b></td>
                                <td scope="col">&nbsp;</td>
                              </tr>
                              <tr>
                                <td scope="col">Head, Finance and Admin</td>
                                <td scope="col">&nbsp;</td>
                              </tr>
                            </table>
							</td>
						</tr>					
					</table>

				  
				  <!-- End of inner table -->
				  </td>
				</tr>
			</table>	
		</td>
	</tr>
</table>


<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>



