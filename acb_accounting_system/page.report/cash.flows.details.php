<?php 
include('./../includes/header.report.php');

$report_name = 'STATEMENT OF CASH FLOWS';

$rep_currency_code = $_POST['cf_currency_code'];
$cf_end_year = $_POST['cf_end_year'];
$cf_end_month = $_POST['cf_end_month'];
$cf_end_day = $_POST['cf_end_day'];
$cf_start_year = $_POST['cf_start_year'];
$cf_start_month = $_POST['cf_start_month'];
$cf_start_day = $_POST['cf_start_day'];
$consolidate_reports = $_POST['consolidate_reports'];

$start_date = dateFormat($cf_start_month,$cf_start_day,$cf_start_year);
$end_date = dateFormat($cf_end_month,$cf_end_day,$cf_end_year);
$rep_currency_name = getConfigurationValueById($rep_currency_code);

?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br>
					<?php echo upperCase(getCompanyName()); ?>
					<br>
					<?php echo upperCase(getCompanyAddress()); ?><br />
					For the period of <?php echo $start_date." - ".$end_date; ?>
					<br>
					ACB-<?php echo $rep_currency_name; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
				  <td scope='col' valign=top align="center">
				  <!-- Start of Inner Table -->
					<table width="60%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td colspan="3"><b>Cash Inflows</b></td>
						</tr>
						<tr>
							<td width="12%">&nbsp;</td>
							<td width="73%">Receipt from the EC Delegation</td>
							<td width="15%" align="right">
							<?php
							$ftfb_sl_id = getSlIdByAccountTitle("Fund Transfer from Brussels");
							$a2 = getGLSumCredit($ftfb_sl_id,$start_date,$end_date,$rep_currency_code);
							echo numberFormat($a2);
							?>							
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Receipt from the AMS</td>
							<td align="right">
							<?php
							$amc_brunei	= getSlIdByAccountTitle("AMC Contribution - Brunei");
							$amc_cambodia = getSlIdByAccountTitle("AMC Contribution - Cambodia"); 
							$amc_indonesia = getSlIdByAccountTitle("AMC Contribution - Indonesia");
							$amc_lao = getSlIdByAccountTitle("AMC Contribution - Lao PDR"); 
							$amc_malaysia = getSlIdByAccountTitle("AMC Contribution - Malaysia");
							$amc_myanmar = getSlIdByAccountTitle("AMC Contribution - Myanmar"); 
							$amc_philippines = getSlIdByAccountTitle("AMC Contribution - Philippines");
							$amc_singapore = getSlIdByAccountTitle("AMC Contribution - Singapore"); 
							$amc_thailand = getSlIdByAccountTitle("AMC Contribution - Thailand"); 
							$amc_vietnam = getSlIdByAccountTitle("AMC Contribution - Vietnam"); 
							
							$amc_brunei = getGLSumCredit($amc_brunei,$start_date,$end_date,$rep_currency_code);
							$amc_cambodia = getGLSumCredit($amc_cambodia,$start_date,$end_date,$rep_currency_code);
							$amc_indonesia = getGLSumCredit($amc_indonesia,$start_date,$end_date,$rep_currency_code);
							$amc_lao = getGLSumCredit($amc_lao,$start_date,$end_date,$rep_currency_code);
							$amc_malaysia = getGLSumCredit($amc_malaysia,$start_date,$end_date,$rep_currency_code);
							$amc_myanmar = getGLSumCredit($amc_myanmar,$start_date,$end_date,$rep_currency_code);
							$amc_philippines = getGLSumCredit($amc_philippines,$start_date,$end_date,$rep_currency_code);
							$amc_singapore = getGLSumCredit($amc_singapore,$start_date,$end_date,$rep_currency_code);
							$amc_thailand = getGLSumCredit($amc_thailand,$start_date,$end_date,$rep_currency_code);
							$amc_vietnam = getGLSumCredit($amc_vietnam,$start_date,$end_date,$rep_currency_code);
							
							$a3 = $amc_brunei+$amc_cambodia+$amc_indonesia+$amc_lao+$amc_malaysia+$amc_myanmar+$amc_philippines+$amc_singapore+$amc_thailand+$amc_vietnam;
							echo numberFormat($a3);
							?>	
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Collection of Receivables</td>
							<td align="right">
							<?php
							$a4 = 0;
							echo numberFormat($a4);
							?>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Receipt of refunds of cash advances and overpayments of expenses</td>
							<td align="right">
							<?php
							$a5 = 0;
							echo numberFormat($a5);
							?>							
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Receipt from sale of books and publications</td>
							<td align="right">
							<?php
							$income_a	= getSlIdByAccountTitle("Income from sale of scrap materials");
							$income_b = getSlIdByAccountTitle("Income from sale of books/publications"); 
							$income_c = getSlIdByAccountTitle("Income from sale of keychains/tokens");
							$income_d = getSlIdByAccountTitle("Other Income"); 
							
							$income_a = getGLSumCredit($income_a,$start_date,$end_date,$rep_currency_code);
							$income_b = getGLSumCredit($income_b,$start_date,$end_date,$rep_currency_code);
							$income_c = getGLSumCredit($income_c,$start_date,$end_date,$rep_currency_code);
							$income_d = getGLSumCredit($income_d,$start_date,$end_date,$rep_currency_code);
							
							$a6 = $income_a+$income_b+$income_c+$income_d;
							echo numberFormat($a6);
							?>								
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>Receipt of grants and donations</td>
							<td align="right">
							<?php
							$donor_a = getSlIdByAccountTitle("Donor Funds 1");
							$donor_b = getSlIdByAccountTitle("Donor Funds 2"); 
							$donor_c = getSlIdByAccountTitle("Donor Funds 3"); 
							
							$donor_a = getGLSumCredit($donor_a,$start_date,$end_date,$rep_currency_code);
							$donor_b = getGLSumCredit($donor_b,$start_date,$end_date,$rep_currency_code);
							$donor_c = getGLSumCredit($donor_c,$start_date,$end_date,$rep_currency_code);
							
							$a7 = $donor_a+$donor_b+$donor_c;
							echo numberFormat($a7);
							?>								
							</td>
						</tr>																																				
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2"><b>Total Cash Inflows</b></td>
							<td align="right">
							<b>
							<?php
							$a8 = $a2+$a3+$a4+$a5+$a6+$a7;
							echo numberFormat($a8);
							?>
							</b>
							</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
					  </tr>
						<tr>
						  <td colspan="3"><b>Cash Outflows</b></td>
					  </tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>Cash payment of operating expenses</td>
						  <td align="right">
							<?php
							$a11 = 0;
							echo numberFormat($a11);
							?>							  
						  </td>
					  </tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>Cash purchase of inventories</td>
						  <td align="right">
							<?php
 							$cash_purchase_a = getSlIdByAccountTitle("Unrestricted-Purchase or rent of vehicles");
							$cash_purchase_b = getSlIdByAccountTitle("Unrestricted-Furniture, Computer Equipment"); 
							$cash_purchase_c = getSlIdByAccountTitle("Unrestricted-Other Equipment"); 
							$cash_purchase_d = getSlIdByAccountTitle("Consumables-Office Supplies"); 
							
							$cash_purchase_a = getGLSumCredit($cash_purchase_a,$start_date,$end_date,$rep_currency_code);
							$cash_purchase_b = getGLSumCredit($cash_purchase_b,$start_date,$end_date,$rep_currency_code);
							$cash_purchase_c = getGLSumCredit($cash_purchase_c,$start_date,$end_date,$rep_currency_code);
							$cash_purchase_d = getGLSumCredit($cash_purchase_d,$start_date,$end_date,$rep_currency_code);
												
							$a12 = $cash_purchase_a+$cash_purchase_b+$cash_purchase_b+$cash_purchase_b;
							echo numberFormat($a12);
							?>							  
						  </td>
					  </tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>Granting of cash advances/petty cash fund</td>
						  <td align="right">
							<?php
 							$petty_cash_a = getSlIdByAccountTitle("EU Petty cash Peso");
							$petty_cash_b = getSlIdByAccountTitle("GOP Petty cash Peso"); 
							
							$petty_cash_a = getGLSumCredit($petty_cash_a,$start_date,$end_date,$rep_currency_code);
							$petty_cash_b = getGLSumCredit($petty_cash_b,$start_date,$end_date,$rep_currency_code);
												
							$a13 = $petty_cash_a+$petty_cash_b;
							echo numberFormat($a13);
							?>						  
						  </td>
					  </tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>Remittance of withholding taxes except thru Tax Remittance Advice</td>
						  <td align="right">
							<?php
							$a14 = 0;
							echo numberFormat($a14);
							?>							  
						  </td>
					  </tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>Remittance of SSS/PAG-IBIG/Philhealth Payable</td>
						  <td align="right">
							<?php
							$a15 = 0;
							echo numberFormat($a15);
							?>							  
						  </td>
					  </tr>
						<tr>
						  <td colspan="2"><b>Total Cash Outflows</b></td>
						  <td align="right">
							<b>
							<?php
							$a16 = $a11+$a12+$a13+$a14+$a15;
							echo numberFormat($a16);
							?>
							</b>						  
						  </td>
					  </tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
					  </tr>
						<tr>
						  <td colspan="2"><b>Total Cash</b></td>
						  <td>&nbsp;</td>
					  </tr>
						<tr>
						  <td>&nbsp;</td>
						  <td><b>Add: Cash Balance, Beginning</b></td>
						  <td>&nbsp;</td>
					  </tr>
						<tr>
						  <td>&nbsp;</td>
						  <td><b>Cash Balance Ending</b></td>
						  <td>&nbsp;</td>
					  </tr>
					</tfoot>
					</table>

				  <!-- End of Inner Table -->
				  </td>
				</tr>
			</table>	
		</td>
	</tr>
</table>


<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>



