<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$year = date("Y");
$month = date("m");
$day = date("d");
$startYear = getProgramStartYear();
$endYear = date("Y");
$date_time_inserted = date("Y-m-d H:i:s");

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
		<form action="/workspaceGOP/page.report/cash.flows.details.php" method="post" name="form1" target="formtarget" onSubmit="MM_openBrWindow('about:blank','formtarget','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1024,height=700')">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th width="20%" class="rounded-header" scope="col"><div class="main" align="left"><span style="font-weight: bold">Cash Flows  </span></div></th>
							<th width="80%" class="rounded-q4" scope="col"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left">&nbsp;</td>
							<td class="rounded-foot-right" align="right"><input type="submit" name="Submit" value="Generate Report" class="formbutton"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr valign="top">
						  <td><b>Start Date *</b></td>
						  <td>
							<select name='cf_start_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='cf_start_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='cf_start_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>							
						</td>
					  </tr>
						<tr valign="top">
						  <td><b>End Date *</b></td>
						  <td>
							<select name='cf_end_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='cf_end_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='cf_end_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>							
						</td>
					  </tr>					  
						<tr valign="top">
						  <td><b>Report Currency*</b></td>
						  <td>
							<?php
								$sql = "SELECT	SQL_BUFFER_RESULT
												SQL_CACHE
												cfg_id,
												cfg_value
										FROM 	tbl_config
										WHERE 	cfg_name = 'currency_type'";
								$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
							?>
							<select name='cf_currency_code' class='formbutton'>
							<?php
							while($data=mysql_fetch_array($rs))
							{
								$column_id = $data['cfg_id'];
								$column_name = $data['cfg_value'];
								?>
								<option value='<?php echo $column_id; ?>'><?php echo $column_name; ?></option>
								<?php
							}
							?>
							</select>
						</td>
					  </tr>	
						<tr valign="top">
						  <td><b>Consolidate Reports *</b></td>
						  <td><input name="consolidate_reports" type="checkbox" value="1"></td>
					  </tr>					  																															
					</tbody>
				</table>
		  </form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>