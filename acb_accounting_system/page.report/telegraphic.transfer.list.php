<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$year = date("Y");
$month = date("m");
$day = date("d");
$startYear = getProgramStartYear();
$endYear = date("Y");
$date_time_inserted = date("Y-m-d H:i:s");

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
		<form action="/workspaceGOP/page.report/telegraphic.transfer.details.php" method="post" name="form1" target="formtarget" onSubmit="MM_openBrWindow('about:blank','formtarget','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1024,height=700')">

				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Telegraphic Transfer Logs </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left">&nbsp;</td>
							<td class="rounded-foot-right" align="right"><input type="submit" name="Submit" value="Generate Report" class="formbutton"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Start Date* </b></td>
							<td width="80%">
							
							<select name='tt_start_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='tt_start_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='tt_start_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>							</td>
						</tr>
						<tr valign="top">
						  <td><b>End Date*</b></td>
						  <td>
							<select name='tt_end_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='tt_end_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='tt_end_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>							</td>
					  </tr>												  																															
					</tbody>
				</table>
		  </form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>