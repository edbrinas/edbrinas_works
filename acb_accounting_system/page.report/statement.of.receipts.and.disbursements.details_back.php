<?php 
$report_name = 'STATEMENT OF RECEIPTS AND DISBURSEMENTS';
include('./../includes/header.report.php');

$srd_currency_code = $_POST['srd_currency_code'];
$srd_start_year = $_POST['srd_start_year'];
$srd_start_month = $_POST['srd_start_month'];
$srd_start_day = $_POST['srd_start_day'];
$srd_end_year = $_POST['srd_end_year'];
$srd_end_month = $_POST['srd_end_month'];
$srd_end_day = $_POST['srd_end_day'];
$srd_budget_id = $_POST['srd_budget_id'];
$srd_donor_id = $_POST['srd_donor_id'];
$consolidate_reports = $_POST['consolidate_reports'];
$use_obligated_value = $_POST['use_obligated_value'];

$system_start_date = date("Y-m-d", mktime(0,0,0,1,1,$srd_start_year));
$start_date_minus_1 = date("Y-m-d", mktime(0,0,0,$srd_start_month,$srd_start_day-1,$srd_start_year));

$start_date = date("Y-m-d", mktime(0,0,0,$srd_start_month,$srd_start_day,$srd_start_year));
$end_date = date("Y-m-d", mktime(0,0,0,$srd_end_month,$srd_end_day,$srd_end_year));
$srd_currency_name = getConfigurationValueById($srd_currency_code);

$ftfb_sl_id = getSlIdByAccountCode(20100);
if ($ftfb_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($ftfb_sl_id)==true)
	{
		$arr_sum_ftfb_starting = 0;
	}
	else
	{
		$arr_sum_ftfb_starting = getDebitCreditSumValue($ftfb_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
		$arr_sum_ftfb_ending = getDebitCreditSumValue($ftfb_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ftfe_sl_id = getSlIdByAccountCode(20200);
if ($ftfe_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($ftfe_sl_id)==true)
	{
		$arr_sum_ftfe_starting = 0;
	}
	else
	{
		$arr_sum_ftfe_starting = getDebitCreditSumValue($ftfe_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
		$arr_sum_ftfe_ending = getDebitCreditSumValue($ftfe_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ftfu_sl_id = getSlIdByAccountCode(20300);
if ($ftfu_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($ftfu_sl_id)==true)
	{
		$arr_sum_ftfu_starting = 0;
	}
	else
	{
		$arr_sum_ftfu_starting = getDebitCreditSumValue($ftfu_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_ftfu_ending = getDebitCreditSumValue($ftfu_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ftfup_sl_id = getSlIdByAccountCode(20400);
if ($ftfup_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($ftfup_sl_id)==true)
	{
		$arr_sum_ftfup_starting = 0;
	}
	else
	{
		$arr_sum_ftfup_starting = getDebitCreditSumValue($ftfup_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_ftfup_ending = getDebitCreditSumValue($ftfup_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ftfap_sl_id = getSlIdByAccountCode(20500);
if ($ftfap_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($ftfap_sl_id)==true)
	{
		$arr_sum_ftfap_starting = 0;
	}
	else
	{
		$arr_sum_ftfap_starting = getDebitCreditSumValue($ftfap_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
		$arr_sum_ftfap_ending = getDebitCreditSumValue($ftfap_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$fgl_sl_id = getSlIdByAccountCode(21100);
if ($fgl_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($fgl_sl_id)==true)
	{
		$arr_sum_fgl_starting = 0;
	}
	else
	{
		$arr_sum_fgl_starting = getDebitCreditSumValue($fgl_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_fgl_ending = getDebitCreditSumValue($fgl_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$bi_sl_id = getSlIdByAccountCode(21200);
if ($bi_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($bi_sl_id)==true)
	{
		$arr_sum_bi_starting = 0;
	}
	else
	{
		$arr_sum_bi_starting = getDebitCreditSumValue($bi_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_bi_ending = getDebitCreditSumValue($bi_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$acb_sl_id = getSlIdByAccountCode(22110);
if ($acb_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($acb_sl_id)==true)
	{
		$arr_sum_acb_starting = 0;
	}
	else
	{
		$arr_sum_acb_starting = getDebitCreditSumValue($acb_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
		$arr_sum_acb_ending = getDebitCreditSumValue($acb_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);
}
$acc_sl_id = getSlIdByAccountCode(22111);
if ($acc_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($acc_sl_id)==true)
	{
		$arr_sum_acc_starting = 0;
	}
	else
	{
		$arr_sum_acc_starting = getDebitCreditSumValue($acc_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_acc_ending = getDebitCreditSumValue($acc_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$aci_sl_id = getSlIdByAccountCode(22112);
if ($aci_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($aci_sl_id)==true)
	{
		$arr_sum_aci_starting = 0;
	}
	else
	{
		$arr_sum_aci_starting = getDebitCreditSumValue($aci_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_aci_ending = getDebitCreditSumValue($aci_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$aclp_sl_id = getSlIdByAccountCode(22113);
if ($aclp_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($aclp_sl_id)==true)
	{
		$arr_sum_aclp_starting = 0;
	}
	else
	{
		$arr_sum_aclp_starting = getDebitCreditSumValue($aclp_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_aclp_ending = getDebitCreditSumValue($aclp_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$acmal_sl_id = getSlIdByAccountCode(22114);
if ($acmal_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($acmal_sl_id)==true)
	{
		$arr_sum_acmal_starting = 0;
	}
	else
	{
		$arr_sum_acmal_starting = getDebitCreditSumValue($acmal_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_acmal_ending = getDebitCreditSumValue($acmal_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$acmy_sl_id = getSlIdByAccountCode(22115);
if ($acmy_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($acmy_sl_id)==true)
	{
		$arr_sum_acmy_starting = 0;
	}
	else
	{
		$arr_sum_acmy_starting = getDebitCreditSumValue($acmy_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_acmy_ending = getDebitCreditSumValue($acmy_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$acp_sl_id = getSlIdByAccountCode(22116);
if ($acp_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($acp_sl_id)==true)
	{
		$arr_sum_acp_starting = 0;
	}
	else
	{
		$arr_sum_acp_starting = getDebitCreditSumValue($acp_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_acp_ending = getDebitCreditSumValue($acp_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$acs_sl_id = getSlIdByAccountCode(22117);
if ($acs_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($acs_sl_id)==true)
	{
		$arr_sum_acs_starting = 0;
	}
	else
	{
		$arr_sum_acs_starting = getDebitCreditSumValue($acs_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_acs_ending = getDebitCreditSumValue($acs_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$act_sl_id = getSlIdByAccountCode(22118);
if ($act_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($act_sl_id)==true)
	{
		$arr_sum_act_starting = 0;
	}
	else
	{
		$arr_sum_act_starting = getDebitCreditSumValue($act_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_act_ending = getDebitCreditSumValue($act_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$acv_sl_id = getSlIdByAccountCode(22119);
if ($acv_sl_id!="")
{
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($acv_sl_id)==true)
	{
		$arr_sum_acv_starting = 0;
	}
	else
	{
		$arr_sum_acv_starting = getDebitCreditSumValue($acv_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_acv_ending = getDebitCreditSumValue($acv_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}

$df_sl_id_a = getSlIdByAccountCode(23100);
$df_sl_id_b = getSlIdByAccountCode(23200);
$df_sl_id_c = getSlIdByAccountCode(23300);
$df_sl_id_d = getSlIdByAccountCode(23400);
$df_sl_id_e = getSlIdByAccountCode(23500);
$df_sl_id_f = getSlIdByAccountCode(23600);
$df_sl_id_g = getSlIdByAccountCode(23700);
$df_sl_id_h = getSlIdByAccountCode(23800);
$df_sl_id_i = getSlIdByAccountCode(23900);
if ($df_sl_id_a!="" && $df_sl_id_b != "" && $df_sl_id_c != "" && $df_sl_id_d != "" && $df_sl_id_e != "" && $df_sl_id_f != "" && $df_sl_id_g != "" && $df_sl_id_h != "" && $df_sl_id_i != "")
{
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($df_sl_id_a)==true)
	{
		$sum_df_s_a = 0;
	}
	else
	{
		$sum_df_s_a = getDebitCreditSumValue($df_sl_id_a,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($df_sl_id_b)==true)
	{
		$sum_df_s_b = 0;
	}
	else
	{
		$sum_df_s_b = getDebitCreditSumValue($df_sl_id_b,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($df_sl_id_c)==true)
	{
		$sum_df_s_c = 0;
	}
	else
	{
		$sum_df_s_c = getDebitCreditSumValue($df_sl_id_c,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($df_sl_id_d)==true)
	{
		$sum_df_s_d = 0;
	}
	else
	{
		$sum_df_s_d = getDebitCreditSumValue($df_sl_id_d,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($df_sl_id_e)==true)
	{
		$sum_df_s_e = 0;
	}
	else
	{
		$sum_df_s_e = getDebitCreditSumValue($df_sl_id_e,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($df_sl_id_f)==true)
	{
		$sum_df_s_f = 0;
	}
	else
	{
		$sum_df_s_f = getDebitCreditSumValue($df_sl_id_f,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($df_sl_id_g)==true)
	{
		$sum_df_s_g = 0;
	}
	else
	{
		$sum_df_s_g = getDebitCreditSumValue($df_sl_id_g,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}	
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($df_sl_id_h)==true)
	{
		$sum_df_s_h = 0;
	}
	else
	{
		$sum_df_s_h = getDebitCreditSumValue($df_sl_id_h,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}	
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($df_sl_id_i)==true)
	{
		$sum_df_s_i = 0;
	}
	else
	{
		$sum_df_s_i = getDebitCreditSumValue($df_sl_id_i,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}	

		$sum_df_e_a = getDebitCreditSumValue($df_sl_id_a,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		$sum_df_e_b = getDebitCreditSumValue($df_sl_id_b,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		$sum_df_e_c = getDebitCreditSumValue($df_sl_id_c,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		$sum_df_e_d = getDebitCreditSumValue($df_sl_id_d,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		$sum_df_e_e = getDebitCreditSumValue($df_sl_id_e,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		$sum_df_e_f = getDebitCreditSumValue($df_sl_id_f,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		$sum_df_e_g = getDebitCreditSumValue($df_sl_id_g,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		$sum_df_e_h = getDebitCreditSumValue($df_sl_id_h,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		$sum_df_e_i = getDebitCreditSumValue($df_sl_id_i,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		
	$arr_sum_df_starting = $sum_df_s_a + $sum_df_s_b + $sum_df_s_c + $sum_df_s_d + $sum_df_s_e + $sum_df_s_f + $sum_df_s_g + $sum_df_s_h + $sum_df_s_i;
	$arr_sum_df_ending = $sum_df_e_a + $sum_df_e_b + $sum_df_e_c + $sum_df_e_d + $sum_df_e_e + $sum_df_e_f + $sum_df_e_g + $sum_df_e_h + $sum_df_e_i;	
}
 
$sls_sl_id_a = getSlIdByAccountCode(24100);
$sls_sl_id_b = getSlIdByAccountCode(24200);
$sls_sl_id_c = getSlIdByAccountCode(24300);
if ($sls_sl_id_a!="" && $sls_sl_id_b != "" && $sls_sl_id_c != "")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($sls_sl_id_a)==true)
	{
		$sum_sls_s_a = 0;
	}
	else
	{
		$sum_sls_s_a = getDebitCreditSumValue($sls_sl_id_a,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($sls_sl_id_b)==true)
	{
		$sum_sls_s_b = 0;
	}
	else
	{
		$sum_sls_s_b = getDebitCreditSumValue($sls_sl_id_b,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}	

	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($sls_sl_id_c)==true)
	{
		$sum_sls_s_c = 0;
	}
	else
	{
		$sum_sls_s_c = getDebitCreditSumValue($sls_sl_id_c,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}	
		$sum_sls_e_a = getDebitCreditSumValue($sls_sl_id_a,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		$sum_sls_e_b = getDebitCreditSumValue($sls_sl_id_b,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	$sum_sls_e_c = getDebitCreditSumValue($sls_sl_id_c,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	

	$arr_sum_sls_starting = $sum_sls_s_a + $sum_sls_s_b + $sum_sls_s_c;
	$arr_sum_sls_ending = $sum_sls_e_a + $sum_sls_e_b + $sum_sls_e_c;
}
$oi_sl_id_a = getSlIdByAccountCode(24400);
$oi_sl_id_b = getSlIdByAccountCode(24500);
if ($oi_sl_id_a!="" && $oi_sl_id_b != "")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($oi_sl_id_a)==true)
	{
		$sum_oi_s_a = 0;
	}
	else
	{
		$sum_oi_s_a = getDebitCreditSumValue($oi_sl_id_a,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($oi_sl_id_b)==true)
	{
		$sum_oi_s_b = 0;
	}
	else
	{
		$sum_oi_s_b = getDebitCreditSumValue($oi_sl_id_b,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}	

		$sum_oi_e_a = getDebitCreditSumValue($oi_sl_id_a,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
		$sum_oi_e_b = getDebitCreditSumValue($oi_sl_id_b,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	

	$arr_sum_oi_starting = $sum_oi_s_a + $sum_oi_s_b;
	$arr_sum_oi_ending = $sum_oi_e_a + $sum_oi_e_b;
}

$total_receipts_starting = 	abs($arr_sum_ftfb_starting) +
							abs($arr_sum_ftfe_starting) +
							abs($arr_sum_ftfu_starting) +
							abs($arr_sum_ftfup_starting) +
							abs($arr_sum_ftfap_starting) +
							abs($arr_sum_bi_starting) +
							abs($arr_sum_acb_starting) +
							abs($arr_sum_acc_starting) +
							abs($arr_sum_aci_starting) +
							abs($arr_sum_aclp_starting) +
							abs($arr_sum_acmal_starting) +
							abs($arr_sum_acmy_starting) +
							abs($arr_sum_acp_starting) +
							abs($arr_sum_acs_starting) +
							abs($arr_sum_act_starting) +
							abs($arr_sum_acv_starting) +
							abs($arr_sum_df_starting) +
							abs($arr_sum_sls_starting) +
							abs($arr_sum_oi_starting) + 
							(-abs($arr_sum_fgl_starting));
							
$total_receipts_ending = 	abs($arr_sum_ftfb_ending) +
							abs($arr_sum_ftfe_ending) +
							abs($arr_sum_ftfu_ending) +
							abs($arr_sum_ftfup_ending) +
							abs($arr_sum_ftfap_ending) +
							abs($arr_sum_bi_ending) +
							abs($arr_sum_acb_ending) +
							abs($arr_sum_acc_ending) +
							abs($arr_sum_aci_ending) +
							abs($arr_sum_aclp_ending) +
							abs($arr_sum_acmal_ending) +
							abs($arr_sum_acmy_ending) +
							abs($arr_sum_acp_ending) +
							abs($arr_sum_acs_ending) +
							abs($arr_sum_act_ending) +
							abs($arr_sum_acv_ending) +
							abs($arr_sum_df_ending) +
							abs($arr_sum_sls_ending) +
							abs($arr_sum_oi_ending) + 
							(-abs($arr_sum_fgl_ending));

$ed_sl_id = getSlIdByAccountCode(30010);
if ($ed_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($ed_sl_id)==true)
	{
		$arr_sum_ed_starting = 0;
	}
	else
	{
		$arr_sum_ed_starting = getDebitCreditSumValue($ed_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_ed_ending = getDebitCreditSumValue($ed_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$dars_sl_id = getSlIdByAccountCode(30020);
if ($dars_sl_id!="")

{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($dars_sl_id)==true)
	{
		$arr_sum_dars_starting = 0;
	}
	else
	{
		$arr_sum_dars_starting = getDebitCreditSumValue($dars_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_dars_ending = getDebitCreditSumValue($dars_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$faar_sl_id = getSlIdByAccountCode(30030);
if ($faar_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($faar_sl_id)==true)
	{
		$arr_sum_faar_starting = 0;
	}
	else
	{
		$arr_sum_faar_starting = getDebitCreditSumValue($faar_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_faar_ending = getDebitCreditSumValue($faar_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$oedpa_sl_id = getSlIdByAccountCode(30040);
if ($oedpa_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($oedpa_sl_id)==true)
	{
		$arr_sum_oedpa_starting = 0;
	}
	else
	{
		$arr_sum_oedpa_starting = getDebitCreditSumValue($oedpa_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_oedpa_ending = getDebitCreditSumValue($oedpa_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ssst_sl_id = getSlIdByAccountCode(30050);
if ($ssst_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($ssst_sl_id)==true)
	{
		$arr_sum_ssst_starting = 0;
	}
	else
	{
		$arr_sum_ssst_starting = getDebitCreditSumValue($ssst_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_ssst_ending = getDebitCreditSumValue($ssst_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$secstaff_sl_id = getSlIdByAccountCode(30060);
if ($secstaff_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($secstaff_sl_id)==true)
	{
		$arr_sum_secstaff_starting = 0;
	}
	else
	{
		$arr_sum_secstaff_starting = getDebitCreditSumValue($secstaff_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_secstaff_ending = getDebitCreditSumValue($secstaff_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$sssa_sl_id = getSlIdByAccountCode(30070);
if ($sssa_sl_id!="")
{	
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($sssa_sl_id)==true)
	{
		$arr_sum_sssa_starting = 0;
	}
	else
	{
		$arr_sum_sssa_starting = getDebitCreditSumValue($sssa_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_sssa_ending = getDebitCreditSumValue($sssa_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$gss_sl_id = getSlIdByAccountCode(30080);
if ($gss_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($gss_sl_id)==true)
	{
		$arr_sum_gss_starting = 0;
	}
	else
	{
		$arr_sum_gss_starting = getDebitCreditSumValue($gss_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_gss_ending = getDebitCreditSumValue($gss_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}

$arr_sum_sw_starting = $arr_sum_ed_starting + $arr_sum_dars_starting + $arr_sum_faar_starting + $arr_sum_oedpa_starting + $arr_sum_ssst_starting + $arr_sum_secstaff_starting + $arr_sum_sssa_starting + $arr_sum_gss_starting;

$arr_sum_sw_ending = $arr_sum_ed_ending + $arr_sum_dars_ending + $arr_sum_faar_ending + $arr_sum_oedpa_ending + $arr_sum_ssst_ending + $arr_sum_secstaff_ending + $arr_sum_sssa_ending + $arr_sum_gss_ending;


$staffallow_sl_id = getSlIdByAccountCode(30090);
if ($staffallow_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($staffallow_sl_id)==true)
	{
		$arr_sum_staffallow_starting = 0;
	}
	else
	{
		$arr_sum_staffallow_starting = getDebitCreditSumValue($staffallow_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_staffallow_ending = getDebitCreditSumValue($staffallow_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$sal_sl_id = getSlIdByAccountCode(30100);
if ($sal_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($sal_sl_id)==true)
	{
		$arr_sum_sal_starting = 0;
	}
	else
	{
		$arr_sum_sal_starting = getDebitCreditSumValue($sal_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_sal_ending = getDebitCreditSumValue($sal_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$aps_sl_id = getSlIdByAccountCode(30200);
if ($aps_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($aps_sl_id)==true)
	{
		$arr_sum_aps_starting = 0;
	}
	else
	{
		$arr_sum_aps_starting = getDebitCreditSumValue($aps_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_aps_ending = getDebitCreditSumValue($aps_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$lacps_sl_id = getSlIdByAccountCode(30300);
if ($lacps_sl_id!="")
{			
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($lacps_sl_id)==true)
	{
		$arr_sum_lacps_starting = 0;
	}
	else
	{
		$arr_sum_lacps_starting = getDebitCreditSumValue($lacps_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_lacps_ending = getDebitCreditSumValue($lacps_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$arr_sum_pdmt_starting = $arr_sum_aps_starting+$arr_sum_lacps_starting;
$arr_sum_pdmt_ending = $arr_sum_aps_ending+$arr_sum_lacps_ending;

$arr_sum_hr_starting = $arr_sum_sw_starting+$arr_sum_staffallow_starting+$arr_sum_sal_starting+$arr_sum_pdmt_starting;
$arr_sum_hr_ending = $arr_sum_sw_ending+$arr_sum_staffallow_ending+$arr_sum_sal_ending+$arr_sum_pdmt_ending;

$intercon_sl_id = getSlIdByAccountCode(31100);
if ($intercon_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($intercon_sl_id)==true)
	{
		$arr_sum_intercon_starting = 0;
	}
	else
	{
		$arr_sum_intercon_starting = getDebitCreditSumValue($intercon_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_intercon_ending = getDebitCreditSumValue($intercon_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$intl_sl_id = getSlIdByAccountCode(31200);
if ($intl_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($intl_sl_id)==true)
	{
		$arr_sum_intl_starting = 0;
	}
	else
	{
		$arr_sum_intl_starting = getDebitCreditSumValue($intl_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_intl_ending = getDebitCreditSumValue($intl_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}

$arr_sum_travel_starting = $arr_sum_intercon_starting+$arr_sum_intl_starting;
$arr_sum_travel_ending = $arr_sum_intercon_ending+$arr_sum_intl_ending;

$uprv_sl_id = getSlIdByAccountCode(32100);
if ($uprv_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($uprv_sl_id)==true)
	{
		$arr_sum_uprv_starting = 0;
	}
	else
	{
		$arr_sum_uprv_starting = getDebitCreditSumValue($uprv_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_uprv_ending = getDebitCreditSumValue($uprv_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$prv_sl_id = getSlIdByAccountCode(32110);
if ($prv_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($prv_sl_id)==true)
	{
		$arr_sum_prv_starting = 0;
	}
	else
	{
		$arr_sum_prv_starting = getDebitCreditSumValue($prv_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_prv_ending = getDebitCreditSumValue($prv_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$rfce_sl_id = getSlIdByAccountCode(32220);
if ($rfce_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($rfce_sl_id)==true)
	{
		$arr_sum_rfce_starting = 0;
	}
	else
	{
		$arr_sum_rfce_starting = getDebitCreditSumValue($rfce_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_rfce_ending = getDebitCreditSumValue($rfce_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ufce_sl_id = getSlIdByAccountCode(32200);
if ($ufce_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($ufce_sl_id)==true)
	{
		$arr_sum_ufce_starting = 0;
	}
	else
	{
		$arr_sum_ufce_starting = getDebitCreditSumValue($ufce_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_ufce_ending = getDebitCreditSumValue($ufce_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$roe_sl_id = getSlIdByAccountCode(32330);
if ($roe_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($roe_sl_id)==true)
	{
		$arr_sum_roe_starting = 0;
	}
	else
	{
		$arr_sum_roe_starting = getDebitCreditSumValue($roe_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_roe_ending = getDebitCreditSumValue($roe_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$uoe_sl_id = getSlIdByAccountCode(32300);
if ($uoe_sl_id!="")
{					
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($uoe_sl_id)==true)
	{
		$arr_sum_uoe_starting = 0;
	}
	else
	{
		$arr_sum_uoe_starting = getDebitCreditSumValue($uoe_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_uoe_ending = getDebitCreditSumValue($uoe_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$lm_sl_id = getSlIdByAccountCode(32400);
if ($lm_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($lm_sl_id)==true)
	{
		$arr_sum_lm_starting = 0;
	}
	else
	{
		$arr_sum_lm_starting = getDebitCreditSumValue($lm_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_lm_ending = getDebitCreditSumValue($lm_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}

$arr_sum_ses_starting = $arr_sum_uprv_starting+
						$arr_sum_prv_starting+
						$arr_sum_rfce_starting+
						$arr_sum_ufce_starting+
						$arr_sum_roe_starting+
						$arr_sum_uoe_starting+
						$arr_sum_lm_starting;
						
$arr_sum_ses_ending = $arr_sum_uprv_ending+
						$arr_sum_prv_ending+
						$arr_sum_rfce_ending+
						$arr_sum_ufce_ending+
						$arr_sum_roe_ending+
						$arr_sum_uoe_ending+
						$arr_sum_lm_ending;

$vc_sl_id = getSlIdByAccountCode(33100);
if ($vc_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($vc_sl_id)==true)
	{
		$arr_sum_vc_starting = 0;
	}
	else
	{
		$arr_sum_vc_starting = getDebitCreditSumValue($vc_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_vc_ending = getDebitCreditSumValue($vc_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$cos_sl_id = getSlIdByAccountCode(33200);
if ($cos_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($cos_sl_id)==true)
	{
		$arr_sum_cos_starting = 0;
	}
	else
	{
		$arr_sum_cos_starting = getDebitCreditSumValue($cos_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_cos_ending = getDebitCreditSumValue($cos_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$moe_sl_id = getSlIdByAccountCode(33300);
if ($moe_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($moe_sl_id)==true)
	{
		$arr_sum_moe_starting = 0;
	}
	else
	{
		$arr_sum_moe_starting = getDebitCreditSumValue($moe_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_moe_ending = getDebitCreditSumValue($moe_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$oserv_sl_id = getSlIdByAccountCode(33400);
if ($oserv_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($oserv_sl_id)==true)
	{
		$arr_sum_oserv_starting = 0;
	}
	else
	{
		$arr_sum_oserv_starting = getDebitCreditSumValue($oserv_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_oserv_ending = getDebitCreditSumValue($oserv_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$orent_sl_id = getSlIdByAccountCode(33500);
if ($orent_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($orent_sl_id)==true)
	{
		$arr_sum_orent_starting = 0;
	}
	else
	{
		$arr_sum_orent_starting = getDebitCreditSumValue($orent_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_orent_ending = getDebitCreditSumValue($orent_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$arr_sum_lopc_starting =	$arr_sum_vc_starting +
							$arr_sum_cos_starting +
							$arr_sum_moe_starting +
							$arr_sum_oserv_starting +
							$arr_sum_orent_starting;
							
$arr_sum_lopc_ending = 		$arr_sum_vc_ending +
							$arr_sum_cos_ending +
							$arr_sum_moe_ending +
							$arr_sum_oserv_ending +
							$arr_sum_orent_ending;

$guipub_sl_id = getSlIdByAccountCode(34101);
if ($guipub_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($guipub_sl_id)==true)
	{
		$arr_sum_guipub_starting = 0;
	}
	else
	{
		$arr_sum_guipub_starting = getDebitCreditSumValue($guipub_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_guipub_ending = getDebitCreditSumValue($guipub_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$abmag_sl_id = getSlIdByAccountCode(34102);
if ($abmag_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($abmag_sl_id)==true)
	{
		$arr_sum_abmag_starting = 0;
	}
	else
	{
		$arr_sum_abmag_starting = getDebitCreditSumValue($abmag_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_abmag_ending = getDebitCreditSumValue($abmag_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$gdve_sl_id = getSlIdByAccountCode(34103);
if ($gdve_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($gdve_sl_id)==true)
	{
		$arr_sum_gdve_starting = 0;
	}
	else
	{
		$arr_sum_gdve_starting = getDebitCreditSumValue($gdve_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_gdve_ending = getDebitCreditSumValue($gdve_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$peav_sl_id = getSlIdByAccountCode(34104);
if ($peav_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($peav_sl_id)==true)
	{
		$arr_sum_peav_starting = 0;
	}
	else
	{
		$arr_sum_peav_starting = getDebitCreditSumValue($peav_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_peav_ending = getDebitCreditSumValue($peav_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$studre_sl_id = getSlIdByAccountCode(34105);
if ($studre_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($studre_sl_id)==true)
	{
		$arr_sum_studre_starting = 0;
	}
	else
	{
		$arr_sum_studre_starting = getDebitCreditSumValue($studre_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_studre_ending = getDebitCreditSumValue($studre_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$supstud_sl_id = getSlIdByAccountCode(34106);
if ($supstud_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($supstud_sl_id)==true)
	{
		$arr_sum_supstud_starting = 0;
	}
	else
	{
		$arr_sum_supstud_starting = getDebitCreditSumValue($supstud_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_supstud_ending = getDebitCreditSumValue($supstud_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$lste_sl_id = getSlIdByAccountCode(34107);
if ($lste_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($lste_sl_id)==true)
	{
		$arr_sum_lste_starting = 0;
	}
	else
	{
		$arr_sum_lste_starting = getDebitCreditSumValue($lste_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_lste_ending = getDebitCreditSumValue($lste_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ilssfp_sl_id = getSlIdByAccountCode(34108);
if ($ilssfp_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($ilssfp_sl_id)==true)
	{
		$arr_sum_ilssfp_starting = 0;
	}
	else
	{
		$arr_sum_ilssfp_starting = getDebitCreditSumValue($ilssfp_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_ilssfp_ending = getDebitCreditSumValue($ilssfp_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$corts2wk_sl_id = getSlIdByAccountCode(34109);
if ($corts2wk_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($corts2wk_sl_id)==true)
	{
		$arr_sum_corts2wk_starting = 0;
	}
	else
	{
		$arr_sum_corts2wk_starting = getDebitCreditSumValue($corts2wk_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_corts2wk_ending = getDebitCreditSumValue($corts2wk_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$corts1wk_sl_id = getSlIdByAccountCode(34110);
if ($corts1wk_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($corts1wk_sl_id)==true)
	{
		$arr_sum_corts1wk_starting = 0;
	}
	else
	{
		$arr_sum_corts1wk_starting = getDebitCreditSumValue($corts1wk_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_corts1wk_ending = getDebitCreditSumValue($corts1wk_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}

$cosrts1w_sl_id = getSlIdByAccountCode(34111);
if ($cosrts1w_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($cosrts1w_sl_id)==true)
	{
		$arr_sum_cosrts1w_starting = 0;
	}
	else
	{
		$arr_sum_cosrts1w_starting = getDebitCreditSumValue($cosrts1w_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_cosrts1w_ending = getDebitCreditSumValue($cosrts1w_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$costwor_sl_id = getSlIdByAccountCode(34112);
if ($costwor_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($costwor_sl_id)==true)
	{
		$arr_sum_costwor_starting = 0;
	}
	else
	{
		$arr_sum_costwor_starting = getDebitCreditSumValue($costwor_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_costwor_ending = getDebitCreditSumValue($costwor_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$suptohiged_sl_id = getSlIdByAccountCode(34113);
if ($suptohiged_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($suptohiged_sl_id)==true)
	{
		$arr_sum_suptohiged_starting = 0;
	}
	else
	{
		$arr_sum_suptohiged_starting = getDebitCreditSumValue($suptohiged_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_suptohiged_ending = getDebitCreditSumValue($suptohiged_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ateac_sl_id = getSlIdByAccountCode(34114);
if ($ateac_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($ateac_sl_id)==true)
	{
		$arr_sum_ateac_starting = 0;
	}
	else
	{
		$arr_sum_ateac_starting = getDebitCreditSumValue($ateac_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_ateac_ending = getDebitCreditSumValue($ateac_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$evast_sl_id = getSlIdByAccountCode(34115);
if ($evast_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($evast_sl_id)==true)
	{
		$arr_sum_evast_starting = 0;
	}
	else
	{
		$arr_sum_evast_starting = getDebitCreditSumValue($evast_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_evast_ending = getDebitCreditSumValue($evast_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$evacost_sl_id = getSlIdByAccountCode(34116);
if ($evast_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($evacost_sl_id)==true)
	{
		$arr_sum_evacost_starting = 0;
	}
	else
	{
		$arr_sum_evacost_starting = getDebitCreditSumValue($evacost_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_evacost_ending = getDebitCreditSumValue($evacost_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$pcsct_sl_id = getSlIdByAccountCode(34117);
if ($pcsct_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($pcsct_sl_id)==true)
	{
		$arr_sum_pcsct_starting = 0;
	}
	else
	{
		$arr_sum_pcsct_starting = getDebitCreditSumValue($pcsct_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_pcsct_ending = getDebitCreditSumValue($pcsct_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}

$arr_sum_ocostser_starting = 	$arr_sum_guipub_starting +
								$arr_sum_abmag_starting +
								$arr_sum_gdve_starting +
								$arr_sum_peav_starting +
								$arr_sum_studre_starting +
								$arr_sum_supstud_starting +
								$arr_sum_lste_starting +
								$arr_sum_ilssfp_starting +
								$arr_sum_corts2wk_starting +
								$arr_sum_corts1wk_starting +
								$arr_sum_cosrts1w_starting +
								$arr_sum_costwor_starting +
								$arr_sum_suptohiged_starting +
								$arr_sum_ateac_starting +
								$arr_sum_evast_starting +
								$arr_sum_evacost_starting +
								$arr_sum_pcsct_starting;

$arr_sum_ocostser_ending = 		$arr_sum_guipub_ending +
								$arr_sum_abmag_ending +
								$arr_sum_gdve_ending +
								$arr_sum_peav_ending +
								$arr_sum_studre_ending +
								$arr_sum_supstud_ending +
								$arr_sum_lste_ending +
								$arr_sum_ilssfp_ending +
								$arr_sum_corts2wk_ending +
								$arr_sum_corts1wk_ending +
								$arr_sum_cosrts1w_ending +
								$arr_sum_costwor_ending +
								$arr_sum_suptohiged_ending +
								$arr_sum_ateac_ending +
								$arr_sum_evast_ending +
								$arr_sum_evacost_ending +
								$arr_sum_pcsct_ending;
								
$auccost_sl_id = getSlIdByAccountCode(35100);
if ($auccost_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($auccost_sl_id)==true)
	{
		$arr_sum_auccost_starting = 0;
	}
	else
	{
		$arr_sum_auccost_starting = getDebitCreditSumValue($auccost_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_auccost_ending = getDebitCreditSumValue($auccost_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$reccost_sl_id = getSlIdByAccountCode(35200);
if ($reccost_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($reccost_sl_id)==true)
	{
		$arr_sum_reccost_starting = 0;
	}
	else
	{
		$arr_sum_reccost_starting = getDebitCreditSumValue($reccost_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_reccost_ending = getDebitCreditSumValue($reccost_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}					
$arr_sum_arcost_starting = $arr_sum_auccost_starting+$arr_sum_reccost_starting;
$arr_sum_arcost_ending = $arr_sum_auccost_ending+$arr_sum_reccost_ending;

$admincost_sl_id = getSlIdByAccountCode(36100);
if ($admincost_sl_id!="")
{								  
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($admincost_sl_id)==true)
	{
		$arr_sum_admincost_starting = 0;
	}
	else
	{
		$arr_sum_admincost_starting = getDebitCreditSumValue($admincost_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_admincost_ending = getDebitCreditSumValue($admincost_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}	
$cntngncy_sl_id = getSlIdByAccountCode(36200);
if ($cntngncy_sl_id!="")
{		
	if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($cntngncy_sl_id)==true)
	{
		$arr_sum_cntngncy_starting = 0;
	}
	else
	{
		$arr_sum_cntngncy_starting = getDebitCreditSumValue($cntngncy_sl_id,$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_sum_cntngncy_ending = getDebitCreditSumValue($cntngncy_sl_id,$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}	

$arr_sum_totdis_starting =	$arr_sum_hr_starting +
							$arr_sum_travel_starting +
							$arr_sum_ses_starting +
							$arr_sum_lopc_starting +
							$arr_sum_ocostser_starting +
							$arr_sum_arcost_starting +
							$arr_sum_admincost_starting +
							$arr_sum_cntngncy_starting;
$arr_sum_totdis_ending =	$arr_sum_hr_ending +
							$arr_sum_travel_ending +
							$arr_sum_ses_ending +
							$arr_sum_lopc_ending +
							$arr_sum_ocostser_ending +
							$arr_sum_arcost_ending +
							$arr_sum_admincost_ending +
							$arr_sum_cntngncy_ending;
							
$arr_sum_eorod_starting = (-abs($arr_sum_totdis_starting))+$total_receipts_starting;
$arr_sum_eorod_ending = (-abs($arr_sum_totdis_ending))+$total_receipts_ending;
?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br />
					<?php echo upperCase(getCompanyName()); ?>
					<br />
					<?php echo upperCase(getCompanyAddress()); ?><br />
					For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?>
					<br>
					ACB-<?php echo $srd_currency_name; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
				  <td scope='col' valign=top>
				  <!-- Start of Inner Table -->
				  <table width='100%' border='0' cellspacing='2' cellpadding='2' class="report_printing">
				  	<thead>
						<tr>
						  <th width="10%" scope='col'>&nbsp;</th>
						  <th width="30%" scope='col'>&nbsp;</th>
						  <th width="15%" scope='col'><?php echo $start_date_minus_1; ?></th>
						  <th width="15%" scope='col'><?php echo $end_date; ?></th>
						  <th width="15%" scope='col'>Movement</th>
						  <th width="15%" scope='col'>Movement % </th>
						</tr>
					</thead>
					<tbody>
						<tr>
						  <td><b>Receipts</b></td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>Funds Transferred from Brussels</td>
                            <td scope="col" align="right">
                            <?php
                            $ftfb_starting = numberFormat(abs($arr_sum_ftfb_starting));
                            echo $ftfb_starting;
                            ?>                            </td>
                            <td scope="col" align="right">
                            <?php
                            $ftfb_ending = numberFormat(abs($arr_sum_ftfb_ending));  									
                            echo $ftfb_ending;
                            ?>                            </td>
                            <td scope="col" align="right">
                            <?php
                            $total_ftfb_movement = $arr_sum_ftfb_ending-$arr_sum_ftfb_starting;
                            $ftfb_movement = numberFormat(abs($total_ftfb_movement)); 
                            echo $ftfb_movement;
                            ?>                            </td>
                            <td scope="col" align="right">
                            <?php
                            if ($arr_sum_ftfb_starting!=0)
                            {
                                $total_ftfb_pct_movement = ($total_ftfb_movement/$arr_sum_ftfb_starting)*100;
                            }
                            else
                            {
                                $total_ftfb_pct_movement = 0;
                            }
                            $pcf_ftfb_movement = numberFormat($total_ftfb_pct_movement);
                            echo $pcf_ftfb_movement;
                            ?>&nbsp;%                            </td>
						</tr>  
						<tr>
						  <td>&nbsp;</td>
						  <td>Fund Transferred From Euro</td>
                         <td scope="col" align="right">
                          <?php
                            $ftfe_starting = numberFormat(abs($arr_sum_ftfe_starting));
                            echo $ftfe_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ftfe_ending = numberFormat(abs($arr_sum_ftfe_ending));  									
                            echo $ftfe_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ftfe_movement = $arr_sum_ftfe_ending - $arr_sum_ftfe_starting;
                            $ftfe_movement = numberFormat(abs($total_ftfe_movement)); 
                            echo $ftfe_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ftfe_starting!=0)
                            {
                                $total_ftfe_pct_movement = ($total_ftfe_movement/$arr_sum_ftfe_starting)*100;
                            }
                            else
                            {
                                $total_ftfe_pct_movement = 0;
                            }
                            $pcf_ftfe_movement = numberFormat($total_ftfe_pct_movement);
                            echo $pcf_ftfe_movement;
                          ?>&nbsp;%                          </td>
						</tr>  
						<tr>
						  <td>&nbsp;</td>
						  <td>Fund Transfer from USD</td>
                         <td scope="col" align="right">
                          <?php
                            $ftfu_starting = numberFormat(abs($arr_sum_ftfu_starting));
                            echo $ftfu_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ftfu_ending = numberFormat(abs($arr_sum_ftfu_ending));  									
                            echo $ftfu_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ftfu_movement = $arr_sum_ftfu_ending-$arr_sum_ftfu_starting;
                            $ftfu_movement = numberFormat(abs($total_ftfu_movement)); 
                            echo $ftfu_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ftfu_starting!=0)
                            {
                                $total_ftfu_pct_movement = ($total_ftfu_movement/$arr_sum_ftfu_starting)*100;
                            }
                            else
                            {
                                $total_ftfu_pct_movement = 0;
                            }
                            $pcf_ftfu_movement = numberFormat($total_ftfu_pct_movement);
                            echo $pcf_ftfu_movement;
                          ?>&nbsp;%                          </td>
						</tr> 
						<tr>
						  <td>&nbsp;</td>
						  <td>Fund Transfer from EU-PESO</td>
                         <td scope="col" align="right">
                          <?php
                            $ftfup_starting = numberFormat(abs($arr_sum_ftfup_starting));
                            echo $ftfup_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ftfup_ending = numberFormat(abs($arr_sum_ftfup_ending));  									
                            echo $ftfup_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ftfup_movement = $arr_sum_ftfup_ending-$arr_sum_ftfup_starting;
                            $ftfup_movement = numberFormat(abs($total_ftfup_movement)); 
                            echo $ftfup_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ftfup_starting!=0)
                            {
                                $total_ftfup_pct_movement = ($total_ftfup_movement/$arr_sum_ftfup_starting)*100;
                            }
                            else
                            {
                                $total_ftfup_pct_movement = 0;
                            }
                            $pcf_ftfup_movement = numberFormat($total_ftfup_pct_movement);
                            echo $pcf_ftfup_movement;
                          ?>&nbsp;%                          </td>
						</tr> 
						<tr>
						  <td>&nbsp;</td>
						  <td>Fund Transfer from AMS-PESO</td>
                         <td scope="col" align="right">
                          <?php
                            $ftfap_starting = numberFormat(abs($arr_sum_ftfap_starting));
                            echo $ftfap_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ftfap_ending = numberFormat(abs($arr_sum_ftfap_ending));  									
                            echo $ftfap_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ftfap_movement = $arr_sum_ftfap_ending-$arr_sum_ftfap_starting;
                            $ftfap_movement = numberFormat(abs($total_ftfap_movement)); 
                            echo $ftfap_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ftfap_starting!=0)
                            {
                                $total_ftfap_pct_movement = ($total_ftfap_movement/$arr_sum_ftfap_starting)*100;
                            }
                            else
                            {
                                $total_ftfap_pct_movement = 0;
                            }
                            $pcf_ftfap_movement = numberFormat($total_ftfap_pct_movement);
                            echo $pcf_ftfap_movement;
                          ?>&nbsp;%                          </td>
						</tr>  
						<tr>
						  <td>&nbsp;</td>
						  <td>Forex Gain or Loss</td>
                         <td scope="col" align="right">
                          <?php
                            $fgl_starting = numberFormat(-abs($arr_sum_fgl_starting));
                            echo $fgl_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $fgl_ending = numberFormat(-abs($arr_sum_fgl_ending));  									
                            echo $fgl_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_fgl_movement = -abs($arr_sum_fgl_ending)-(-abs($arr_sum_fgl_starting));
                            $fgl_movement = numberFormat($total_fgl_movement); 
                            echo $fgl_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_fgl_starting!=0)
                            {
                                $total_fgl_pct_movement = ($total_fgl_movement/$arr_sum_fgl_starting)*100;
                            }
                            else
                            {
                                $total_fgl_pct_movement = 0;
                            }
                            $pcf_fgl_movement = numberFormat($total_fgl_pct_movement);
                            echo $pcf_fgl_movement;
                          ?>&nbsp;%                          </td>
						</tr>						
                        <tr>
						  <td>&nbsp;</td>
						  <td>Bank Interest</td>
                         <td scope="col" align="right">
                          <?php
                            $bi_starting = numberFormat(abs($arr_sum_bi_starting));
                            echo $bi_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $bi_ending = numberFormat(abs($arr_sum_bi_ending));  									
                            echo $bi_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_bi_movement = $arr_sum_bi_ending-$arr_sum_bi_starting;
                            $bi_movement = numberFormat(abs($total_bi_movement)); 
                            echo $bi_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_bi_starting!=0)
                            {
                                $total_bi_pct_movement = ($total_bi_movement/$arr_sum_bi_starting)*100;
                            }
                            else
                            {
                                $total_bi_pct_movement = 0;
                            }
                            $pcf_bi_movement = numberFormat($total_bi_pct_movement);
                            echo $pcf_bi_movement;
                          ?>&nbsp;%                          </td>
						</tr>
                        
                        <tr>
						  <td colspan="2"><b>AMS Contribution</b></td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>AMS Contribution - Brunei</td>
                         <td scope="col" align="right">
                          <?php
                            $acb_starting = numberFormat(abs($arr_sum_acb_starting));
                            echo $acb_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $acb_ending = numberFormat(abs($arr_sum_acb_ending));  									
                            echo $acb_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_acb_movement = $arr_sum_acb_ending-$arr_sum_acb_starting;
                            $acb_movement = numberFormat(abs($total_acb_movement)); 
                            echo $acb_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_acb_starting!=0)
                            {
                                $total_acb_pct_movement = ($total_acb_movement/$arr_sum_acb_starting)*100;
                            }
                            else
                            {
                                $total_acb_pct_movement = 0;
                            }
                            $pcf_acb_movement = numberFormat(abs($total_acb_pct_movement));
                            echo $pcf_acb_movement;
                          ?>&nbsp;%                          </td>
						</tr>
                        <tr>
						  <td>&nbsp;</td>
						  <td>AMS Contribution - Cambodia</td>
                         <td scope="col" align="right">
                          <?php
                            $acc_starting = numberFormat(abs($arr_sum_acc_starting));
                            echo $acc_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $acc_ending = numberFormat(abs($arr_sum_acc_ending));  									
                            echo $acc_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_acc_movement = $arr_sum_acc_ending-$arr_sum_acc_starting;
                            $acc_movement = numberFormat(abs($total_acc_movement)); 
                            echo $acc_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_acc_starting!=0)
                            {
                                $total_acc_pct_movement = ($total_acc_movement/$arr_sum_acc_starting)*100;
                            }
                            else
                            {
                                $total_acc_pct_movement = 0;
                            }
                            $pcf_acc_movement = numberFormat(abs($total_acc_pct_movement));
                            echo $pcf_acc_movement;
                          ?>&nbsp;%                          </td>
						</tr>
                        <tr>
						  <td>&nbsp;</td>
						  <td>AMS Contribution - Indonesia</td>
                         <td scope="col" align="right">
                          <?php
                            $aci_starting = numberFormat(abs($arr_sum_aci_starting));
                            echo $aci_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $aci_ending = numberFormat(abs($arr_sum_aci_ending));  									
                            echo $aci_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_aci_movement = $arr_sum_aci_ending-$arr_sum_aci_starting;
                            $aci_movement = numberFormat(abs($total_aci_movement)); 
                            echo $aci_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_aci_starting!=0)
                            {
                                $total_aci_pct_movement = ($total_aci_movement/$arr_sum_aci_starting)*100;
                            }
                            else
                            {
                                $total_aci_pct_movement = 0;
                            }
                            $pcf_aci_movement = numberFormat(abs($total_aci_pct_movement));
                            echo $pcf_aci_movement;
                          ?>&nbsp;%                          </td>
						</tr>
                        <tr>
						  <td>&nbsp;</td>
						  <td>AMS Contribution - Lao PDR</td>
                         <td scope="col" align="right">
                          <?php
                            $aclp_starting = numberFormat(abs($arr_sum_aclp_starting));
                            echo $aclp_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $aclp_ending = numberFormat(abs($arr_sum_aclp_ending));  									
                            echo $aclp_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_aclp_movement = $arr_sum_aclp_ending-$arr_sum_aclp_starting;
                            $aclp_movement = numberFormat(abs($total_aclp_movement)); 
                            echo $aclp_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_aclp_starting!=0)
                            {
                                $total_aclp_pct_movement = ($total_aclp_movement/$arr_sum_aclp_starting)*100;
                            }
                            else
                            {
                                $total_aclp_pct_movement = 0;
                            }
                            $pcf_aclp_movement = numberFormat(abs($total_aclp_pct_movement));
                            echo $pcf_aclp_movement;
                          ?>&nbsp;%                          </td>
						</tr>	
                        <tr>
						  <td>&nbsp;</td>
						  <td>AMS Contribution - Malaysia</td>
                         <td scope="col" align="right">
                          <?php
                            $acmal_starting = numberFormat(abs($arr_sum_acmal_starting));
                            echo $acmal_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $acmal_ending = numberFormat(abs($arr_sum_acmal_ending));  									
                            echo $acmal_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_acmal_movement = $arr_sum_acmal_ending-$arr_sum_acmal_starting;
                            $acmal_movement = numberFormat(abs($total_acmal_movement)); 
                            echo $acmal_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_acmal_starting!=0)
                            {
                                $total_acmal_pct_movement = ($total_acmal_movement/$arr_sum_acmal_starting)*100;
                            }
                            else
                            {
                                $total_acmal_pct_movement = 0;
                            }
                            $pcf_acmal_movement = numberFormat(abs($total_acmal_pct_movement));
                            echo $pcf_acmal_movement;
                          ?>&nbsp;%                          </td>
						</tr>																																																																																																																																																																																																																																																										
                        <tr>
						  <td>&nbsp;</td>
						  <td>AMS Contribution - Myanmar</td>
                         <td scope="col" align="right">
                          <?php
                            $acmy_starting = numberFormat(abs($arr_sum_acmy_starting));
                            echo $acmy_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $acmy_ending = numberFormat(abs($arr_sum_acmy_ending));  									
                            echo $acmy_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_acmy_movement = $arr_sum_acmy_ending-$arr_sum_acmy_starting;
                            $acmy_movement = numberFormat(abs($total_acmy_movement)); 
                            echo $acmy_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_acmy_starting!=0)
                            {
                                $total_acmy_pct_movement = ($total_acmy_movement/$arr_sum_acmy_starting)*100;
                            }
                            else
                            {
                                $total_acmy_pct_movement = 0;
                            }
                            $pcf_acmy_movement = numberFormat(abs($total_acmy_pct_movement));
                            echo $pcf_acmy_movement;
                          ?>&nbsp;%                          </td>
						</tr>																																																																																																																																																																																																																																																										
                        <tr>
						  <td>&nbsp;</td>
						  <td>AMS Contribution - Philippines</td>
                         <td scope="col" align="right">
                          <?php
                            $acp_starting = numberFormat(abs($arr_sum_acp_starting));
                            echo $acp_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $acp_ending = numberFormat(abs($arr_sum_acp_ending));  									
                            echo $acp_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_acp_movement = $arr_sum_acp_ending-$arr_sum_acp_starting;
                            $acp_movement = numberFormat(abs($total_acp_movement)); 
                            echo $acp_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_acp_starting!=0)
                            {
                                $total_acp_pct_movement = ($total_acp_movement/$arr_sum_acp_starting)*100;
                            }
                            else
                            {
                                $total_acp_pct_movement = 0;
                            }
                            $pcf_acp_movement = numberFormat(abs($total_acp_pct_movement));
                            echo $pcf_acp_movement;
                          ?>&nbsp;%                          </td>
						</tr>																																																																																																																																																																																																																																																										
                        <tr>
						  <td>&nbsp;</td>
						  <td>AMS Contribution - Singapore</td>
                         <td scope="col" align="right">
                          <?php
                            $acs_starting = numberFormat(abs($arr_sum_acs_starting));
                            echo $acs_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $acs_ending = numberFormat(abs($arr_sum_acs_ending));  									
                            echo $acs_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_acs_movement = $arr_sum_acs_ending-$arr_sum_acs_starting;
                            $acs_movement = numberFormat(abs($total_acs_movement)); 
                            echo $acs_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_acs_starting!=0)
                            {
                                $total_acs_pct_movement = ($total_acs_movement/$arr_sum_acs_starting)*100;
                            }
                            else
                            {
                                $total_acs_pct_movement = 0;
                            }
                            $pcf_acs_movement = numberFormat(abs($total_acs_pct_movement));
                            echo $pcf_acs_movement;
                          ?>&nbsp;%                          </td>
						</tr>																																																																																																																																																																																																																																																										
                        <tr>
						  <td>&nbsp;</td>
						  <td>AMS Contribution - Thailand</td>
                         <td scope="col" align="right">
                          <?php
                            $act_starting = numberFormat(abs($arr_sum_act_starting));
                            echo $act_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $act_ending = numberFormat(abs($arr_sum_act_ending));  									
                            echo $act_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_act_movement = $arr_sum_act_ending-$arr_sum_act_starting;
                            $act_movement = numberFormat(abs($total_act_movement)); 
                            echo $act_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_act_starting!=0)
                            {
                                $total_act_pct_movement = ($total_act_movement/$arr_sum_act_starting)*100;
                            }
                            else
                            {
                                $total_act_pct_movement = 0;
                            }
                            $pcf_act_movement = numberFormat(abs($total_act_pct_movement));
                            echo $pcf_act_movement;
                          ?>&nbsp;%                          </td>
						</tr>																																																																																																																																																																																																																																																										
                        <tr>
						  <td>&nbsp;</td>
						  <td>AMS Contribution - Vietnam</td>
                         <td scope="col" align="right">
                          <?php
                            $acv_starting = numberFormat(abs($arr_sum_acv_starting));
                            echo $acv_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $acv_ending = numberFormat(abs($arr_sum_acv_ending));  									
                            echo $acv_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_acv_movement = $arr_sum_acv_ending-$arr_sum_acv_starting;
                            $acv_movement = numberFormat(abs($total_acv_movement)); 
                            echo $acv_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_acv_starting!=0)
                            {
                                $total_acv_pct_movement = ($total_acv_movement/$arr_sum_acv_starting)*100;
                            }
                            else
                            {
                                $total_acv_pct_movement = 0;
                            }
                            $pcf_acv_movement = numberFormat(abs($total_acv_pct_movement));
                            echo $pcf_acv_movement;
                          ?>&nbsp;%                          </td>
						</tr>	
						<tr>
						  <td><b>Donor Funds</b></td>
						  <td>&nbsp;</td>
                         <td scope="col" align="right">
                          <?php
                            $df_starting = numberFormat(abs($arr_sum_df_starting));
                            echo $df_starting;
                          ?>                          </td>
                          <td scope="col" align="right"><?php
                            $df_ending = numberFormat(abs($arr_sum_df_ending));  									
                            echo $df_ending;
                          ?></td>
                          <td scope="col" align="right">
                          <?php
                            $total_df_movement = $arr_sum_df_ending-$arr_sum_df_starting;
                            $df_movement = numberFormat(abs($total_df_movement)); 
                            echo $df_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_df_starting!=0)
                            {
                                $total_df_pct_movement = ($total_df_movement/$arr_sum_df_starting)*100;
                            }
                            else
                            {
                                $total_df_pct_movement = 0;
                            }
                            $pcf_df_movement = numberFormat(abs($total_df_pct_movement));
                            echo $pcf_df_movement;
                          ?>&nbsp;%                          </td>
						</tr>  
						<tr>
						  <td><b>Sales</b></td>
						  <td>&nbsp;</td>
                         <td scope="col" align="right">
                          <?php
                            $sls_starting = numberFormat(abs($arr_sum_sls_starting));
                            echo $sls_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $sls_ending = numberFormat(abs($arr_sum_sls_ending));  									
                            echo $sls_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_sls_movement = $arr_sum_sls_ending-$arr_sum_sls_starting;
                            $sls_movement = numberFormat(abs($total_sls_movement)); 
                            echo $sls_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_sls_starting!=0)
                            {
                                $total_sls_pct_movement = ($total_sls_movement/$arr_sum_sls_starting)*100;
                            }
                            else
                            {
                                $total_sls_pct_movement = 0;
                            }
                            $pcf_sls_movement = numberFormat(abs($total_sls_pct_movement));
                            echo $pcf_sls_movement;
                          ?>&nbsp;%                          </td>
						</tr>   
						<tr>
						  <td><b>Other Income</b></td>
						  <td>&nbsp;</td>
                         <td scope="col" align="right">
                          <?php
                            $oi_starting = numberFormat(abs($arr_sum_oi_starting));
                            echo $oi_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $oi_ending = numberFormat(abs($arr_sum_oi_ending));  									
                            echo $oi_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_oi_movement = $arr_sum_oi_ending-$arr_sum_oi_starting;
                            $oi_movement = numberFormat(abs($total_oi_movement)); 
                            echo $oi_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_oi_starting!=0)
                            {
                                $total_oi_pct_movement = ($total_oi_movement/$arr_sum_oi_starting)*100;
                            }
                            else
                            {
                                $total_oi_pct_movement = 0;
                            }
                            $pcf_oi_movement = numberFormat(abs($total_oi_pct_movement));
                            echo $pcf_oi_movement;
                          ?>&nbsp;%                          </td>
						</tr>                                                                     																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												
 						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
						</tr>	
						<tr>
						  <td colspan="2"><b>Total Receipts</b></td>
						  <td align="right">
                          <?php
							echo numberFormat(abs($total_receipts_starting));
							?>                          </td>
						  <td align="right">
                          <?php
							echo numberFormat(abs($total_receipts_ending));
							?>                          </td>
						  <td align="right">
                          <?php
                            $total_receipts_movement = $total_receipts_ending-$total_receipts_starting;
                            echo numberFormat(abs($total_receipts_movement)); 
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($total_receipts_starting!=0)
                            {
                                $total_receipts_pct_movement = ($total_receipts_movement/$total_receipts_starting)*100;
                            }
                            else
                            {
                                $total_receipts_pct_movement = 0;
                            }
                            echo numberFormat(abs($total_receipts_pct_movement));
                          ?>&nbsp;%</td>
						</tr>	
						<tr>
						  <td>&nbsp;</td>                        
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td colspan="2"><b>Human Resources</b></td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>Salaries and Wages</td>
                         <td scope="col" align="right">
                          <?php
                            $sw_starting = numberFormat($arr_sum_sw_starting*-1);
                            echo $sw_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $sw_ending = numberFormat($arr_sum_sw_ending*-1);  									
                            echo $sw_ending;
                          ?>                          </td>
                          <td scope="col" align="right"><?php
                            $total_sw_movement = $arr_sum_sw_ending-$arr_sum_sw_starting;
                            $sw_movement = numberFormat(($total_sw_movement*-1)); 
                            echo $sw_movement;
                          ?></td>
                      <td scope="col" align="right"><?php
                            if ($arr_sum_sw_starting!=0)
                            {
                                $total_sw_pct_movement = ($total_sw_movement/$arr_sum_sw_starting)*100;
                            }
                            else
                            {
                                $total_sw_pct_movement = 0;
                            }
                            $pcf_sw_movement = numberFormat($total_sw_pct_movement);
                            echo $pcf_sw_movement;
                          ?>&nbsp;%						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;&nbsp;Executive Director</td>
                         <td scope="col" align="right">
                          <?php
                            $ed_starting = numberFormat($arr_sum_ed_starting*-1);
                            echo $ed_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ed_ending = numberFormat($arr_sum_ed_ending*-1);  									
                            echo $ed_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ed_movement = $arr_sum_ed_ending-$arr_sum_ed_starting;
                            $ed_movement = numberFormat(($total_ed_movement*-1)); 
                            echo $ed_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ed_starting!=0)
                            {
                                $total_ed_pct_movement = ($total_ed_movement/$arr_sum_ed_starting)*100;
                            }
                            else
                            {
                                $total_ed_pct_movement = 0;
                            }
                            $pcf_ed_movement = numberFormat($total_ed_pct_movement);
                            echo $pcf_ed_movement;
                          ?>&nbsp;%                          </td>
						</tr>  
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;&nbsp;Director - ASEAN Recruited Staff</td>
                         <td scope="col" align="right">
                          <?php
                            $dars_starting = numberFormat($arr_sum_dars_starting*-1);
                            echo $dars_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $dars_ending = numberFormat($arr_sum_dars_ending*-1);  									
                            echo $dars_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_dars_movement = $arr_sum_dars_ending-$arr_sum_dars_starting;
                            $dars_movement = numberFormat(($total_dars_movement*-1)); 
                            echo $dars_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_dars_starting!=0)
                            {
                                $total_dars_pct_movement = ($total_dars_movement/$arr_sum_dars_starting)*100;
                            }
                            else
                            {
                                $total_dars_pct_movement = 0;
                            }
                            $pcf_dars_movement = numberFormat($total_dars_pct_movement);
                            echo $pcf_dars_movement;
                          ?>&nbsp;%                          </td>
						</tr> 
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;&nbsp;Finance Administrator - ASEAN Recruited</td>
                         <td scope="col" align="right">
                          <?php
                            $faar_starting = numberFormat($arr_sum_faar_starting*-1);
                            echo $faar_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $faar_ending = numberFormat($arr_sum_faar_ending*-1);  									
                            echo $faar_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_faar_movement = $arr_sum_faar_ending-$arr_sum_faar_starting;
                            $faar_movement = numberFormat(($total_faar_movement*-1)); 
                            echo $faar_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_faar_starting!=0)
                            {
                                $total_faar_pct_movement = ($total_faar_movement/$arr_sum_faar_starting)*100;
                            }
                            else
                            {
                                $total_faar_pct_movement = 0;
                            }
                            $pcf_faar_movement = numberFormat($total_faar_pct_movement);
                            echo $pcf_faar_movement;
                          ?>&nbsp;%                          </td>
						</tr>	
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;&nbsp;Office of the ED - Public Awareness</td>
                         <td scope="col" align="right">
                          <?php
                            $oedpa_starting = numberFormat($arr_sum_oedpa_starting*-1);
                            echo $oedpa_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $oedpa_ending = numberFormat($arr_sum_oedpa_ending*-1);  									
                            echo $oedpa_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_oedpa_movement = $arr_sum_oedpa_ending-$arr_sum_oedpa_starting;
                            $oedpa_movement = numberFormat(($total_oedpa_movement*-1)); 
                            echo $oedpa_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_oedpa_starting!=0)
                            {
                                $total_oedpa_pct_movement = ($total_oedpa_movement/$arr_sum_oedpa_starting)*100;
                            }
                            else
                            {
                                $total_oedpa_pct_movement = 0;
                            }
                            $pcf_oedpa_movement = numberFormat($total_oedpa_pct_movement);
                            echo $pcf_oedpa_movement;
                          ?>&nbsp;%                          </td>
						</tr>  
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;&nbsp;Specialist Support Staff (Technical)</td>
                         <td scope="col" align="right">
                          <?php
                            $ssst_starting = numberFormat($arr_sum_ssst_starting*-1);
                            echo $ssst_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ssst_ending = numberFormat($arr_sum_ssst_ending*-1);  									
                            echo $ssst_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ssst_movement = $arr_sum_ssst_ending-$arr_sum_ssst_starting;
                            $ssst_movement = numberFormat(($total_ssst_movement*-1)); 
                            echo $ssst_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ssst_starting!=0)
                            {
                                $total_ssst_pct_movement = ($total_ssst_movement/$arr_sum_ssst_starting)*100;
                            }
                            else
                            {
                                $total_ssst_pct_movement = 0;
                            }
                            $pcf_ssst_movement = numberFormat($total_ssst_pct_movement);
                            echo $pcf_ssst_movement;
                          ?>&nbsp;%                          </td>
						</tr>						
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;&nbsp;Seconded Staff</td>
                         <td scope="col" align="right">
                          <?php
                            $secstaff_starting = numberFormat($arr_sum_secstaff_starting*-1);
                            echo $secstaff_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $secstaff_ending = numberFormat($arr_sum_secstaff_ending*-1);  									
                            echo $secstaff_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_secstaff_movement = $arr_sum_secstaff_ending-$arr_sum_secstaff_starting;
                            $secstaff_movement = numberFormat(($total_secstaff_movement*-1)); 
                            echo $secstaff_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_secstaff_starting!=0)
                            {
                                $total_secstaff_pct_movement = ($total_secstaff_movement/$arr_sum_secstaff_starting)*100;
                            }
                            else
                            {
                                $total_secstaff_pct_movement = 0;
                            }
                            $pcf_secstaff_movement = numberFormat($total_secstaff_pct_movement);
                            echo $pcf_secstaff_movement;
                          ?>&nbsp;%                          </td>
						</tr>						
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;&nbsp;Specialist Support Staff (Admin)</td>
                         <td scope="col" align="right">
                          <?php
                            $sssa_starting = numberFormat($arr_sum_sssa_starting*-1);
                            echo $sssa_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $sssa_ending = numberFormat($arr_sum_sssa_ending*-1);  									
                            echo $sssa_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_sssa_movement = $arr_sum_sssa_ending-$arr_sum_sssa_starting;
                            $sssa_movement = numberFormat(($total_sssa_movement*-1)); 
                            echo $sssa_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_sssa_starting!=0)
                            {
                                $total_sssa_pct_movement = ($total_sssa_movement/$arr_sum_sssa_starting)*100;
                            }
                            else
                            {
                                $total_sssa_pct_movement = 0;
                            }
                            $pcf_sssa_movement = numberFormat($total_sssa_pct_movement);
                            echo $pcf_sssa_movement;
                          ?>&nbsp;%                          </td>
						</tr>
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;&nbsp;General Support Staff</td>
                         <td scope="col" align="right">
                          <?php
                            $gss_starting = numberFormat($arr_sum_gss_starting*-1);
                            echo $gss_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $gss_ending = numberFormat($arr_sum_gss_ending*-1);  									
                            echo $gss_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_gss_movement = $arr_sum_gss_ending-$arr_sum_gss_starting;
                            $gss_movement = numberFormat(($total_gss_movement*-1)); 
                            echo $gss_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_gss_starting!=0)
                            {
                                $total_gss_pct_movement = ($total_gss_movement/$arr_sum_gss_starting)*100;
                            }
                            else
                            {
                                $total_gss_pct_movement = 0;
                            }
                            $pcf_gss_movement = numberFormat($total_gss_pct_movement);
                            echo $pcf_gss_movement;
                          ?>&nbsp;%                          </td>
						</tr>							
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>				
                        <tr>
						  <td>&nbsp;</td>
						  <td>Staff Allowances</td>
                         <td scope="col" align="right">
                          <?php
                            $staffallow_starting = numberFormat($arr_sum_staffallow_starting*-1);
                            echo $staffallow_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $staffallow_ending = numberFormat($arr_sum_staffallow_ending*-1);  									
                            echo $staffallow_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_staffallow_movement = $arr_sum_staffallow_ending-$arr_sum_staffallow_starting;
                            $staffallow_movement = numberFormat(($total_staffallow_movement*-1)); 
                            echo $staffallow_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_staffallow_starting!=0)
                            {
                                $total_staffallow_pct_movement = ($total_staffallow_movement/$arr_sum_staffallow_starting)*100;
                            }
                            else
                            {
                                $total_staffallow_pct_movement = 0;
                            }
                            $pcf_staffallow_movement = numberFormat($total_staffallow_pct_movement);
                            echo $pcf_staffallow_movement;
                          ?>&nbsp;%                          </td>
						</tr>  
                        <tr>
						  <td>&nbsp;</td>
						  <td>Salaries (gross amounts, expat/int. staff) ETAT</td>
                         <td scope="col" align="right">
                          <?php
                            $sal_starting = numberFormat($arr_sum_sal_starting*-1);
                            echo $sal_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $sal_ending = numberFormat($arr_sum_sal_ending*-1);  									
                            echo $sal_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_sal_movement = $arr_sum_sal_ending-$arr_sum_sal_starting;
                            $sal_movement = numberFormat(($total_sal_movement*-1)); 
                            echo $sal_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_sal_starting!=0)
                            {
                                $total_sal_pct_movement = ($total_sal_movement/$arr_sum_sal_starting)*100;
                            }
                            else
                            {
                                $total_sal_pct_movement = 0;
                            }
                            $pcf_sal_movement = numberFormat($total_sal_pct_movement);
                            echo $pcf_sal_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>Per Diems for missions/travels</td>
                         <td scope="col" align="right">
                          <?php
                            $pdmt_starting = numberFormat($arr_sum_pdmt_starting*-1);
                            echo $pdmt_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $pdmt_ending = numberFormat($arr_sum_pdmt_ending*-1);  									
                            echo $pdmt_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_pdmt_movement = $arr_sum_pdmt_ending-$arr_sum_pdmt_starting;
                            $pdmt_movement = numberFormat(($total_pdmt_movement*-1)); 
                            echo $pdmt_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_pdmt_starting!=0)
                            {
                                $total_pdmt_pct_movement = ($total_pdmt_movement/$arr_sum_pdmt_starting)*100;
                            }
                            else
                            {
                                $total_pdmt_pct_movement = 0;
                            }
                            $pcf_pdmt_movement = numberFormat($total_pdmt_pct_movement);
                            echo $pcf_pdmt_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;&nbsp;Abroad (project staff)</td>
                         <td scope="col" align="right">
                          <?php
                            $aps_starting = numberFormat($arr_sum_aps_starting*-1);
                            echo $aps_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $aps_ending = numberFormat($arr_sum_aps_ending*-1);  									
                            echo $aps_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_aps_movement = $arr_sum_aps_ending-$arr_sum_aps_starting;
                            $aps_movement = numberFormat(($total_aps_movement*-1)); 
                            echo $aps_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_aps_starting!=0)
                            {
                                $total_aps_pct_movement = ($total_aps_movement/$arr_sum_aps_starting)*100;
                            }
                            else
                            {
                                $total_aps_pct_movement = 0;
                            }
                            $pcf_aps_movement = numberFormat($total_aps_pct_movement);
                            echo $pcf_aps_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;&nbsp;Local ASEAN countries (project staff)</td>
                         <td scope="col" align="right">
                          <?php
                            $lacps_starting = numberFormat($arr_sum_lacps_starting*-1);
                            echo $lacps_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $lacps_ending = numberFormat($arr_sum_lacps_ending*-1);  									
                            echo $lacps_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_lacps_movement = $arr_sum_lacps_ending-$arr_sum_lacps_starting;
                            $lacps_movement = numberFormat(($total_lacps_movement*-1)); 
                            echo $lacps_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_lacps_starting!=0)
                            {
                                $total_lacps_pct_movement = ($total_lacps_movement/$arr_sum_lacps_starting)*100;
                            }
                            else
                            {
                                $total_lacps_pct_movement = 0;
                            }
                            $pcf_lacps_movement = numberFormat($total_lacps_pct_movement);
                            echo $pcf_lacps_movement;
                          ?>&nbsp;%                          </td>
						</tr> 						
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
						</tr>							
                        <tr>
						  <td colspan="2"><b>Subtotal Human Resources</b></td>
						  <td scope="col" align="right">
                          <?php
                            $hr_starting = numberFormat($arr_sum_hr_starting*-1);
                            echo $hr_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $hr_ending = numberFormat($arr_sum_hr_ending*-1);  									
                            echo $hr_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_hr_movement = $arr_sum_hr_ending-$arr_sum_hr_starting;
                            $hr_movement = numberFormat(($total_hr_movement*-1)); 
                            echo $hr_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_hr_starting!=0)
                            {
                                $total_hr_pct_movement = ($total_hr_movement/$arr_sum_hr_starting)*100;
                            }
                            else
                            {
                                $total_hr_pct_movement = 0;
                            }
                            $pcf_hr_movement = numberFormat($total_hr_pct_movement);
                            echo $pcf_hr_movement;
                          ?>&nbsp;%                          </td>
						</tr>							
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>	
                        <tr>
						  <td><b>Travel</b></td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>Intercontinental Travel</td>
                         <td scope="col" align="right">
                          <?php
                            $intercon_starting = numberFormat($arr_sum_intercon_starting*-1);
                            echo $intercon_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $intercon_ending = numberFormat($arr_sum_intercon_ending*-1);  									
                            echo $intercon_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_intercon_movement = $arr_sum_intercon_ending-$arr_sum_intercon_starting;
                            $intercon_movement = numberFormat(($total_intercon_movement*-1)); 
                            echo $intercon_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_intercon_starting!=0)
                            {
                                $total_intercon_pct_movement = ($total_intercon_movement/$arr_sum_intercon_starting)*100;
                            }
                            else
                            {
                                $total_intercon_pct_movement = 0;
                            }
                            $pcf_intercon_movement = numberFormat($total_intercon_pct_movement);
                            echo $pcf_intercon_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>International Travel</td>
                         <td scope="col" align="right">
                          <?php
                            $intl_starting = numberFormat($arr_sum_intl_starting*-1);
                            echo $intl_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $intl_ending = numberFormat($arr_sum_intl_ending*-1);  									
                            echo $intl_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_intl_movement = $arr_sum_intl_ending-$arr_sum_intl_starting;
                            $intl_movement = numberFormat(($total_intl_movement*-1)); 
                            echo $intl_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_intl_starting!=0)
                            {
                                $total_intl_pct_movement = ($total_intl_movement/$arr_sum_intl_starting)*100;
                            }
                            else
                            {
                                $total_intl_pct_movement = 0;
                            }
                            $pcf_intl_movement = numberFormat($total_intl_pct_movement);
                            echo $pcf_intl_movement;
                          ?>&nbsp;%                          </td>
						</tr>
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
						</tr>                        						
                        <tr>
						  <td><b>Subtotal Travel</b></td>
						  <td>&nbsp;</td>
                         <td scope="col" align="right">
                          <?php
                            $travel_starting = numberFormat($arr_sum_travel_starting*-1);
                            echo $travel_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $travel_ending = numberFormat($arr_sum_travel_ending*-1);  									
                            echo $travel_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_travel_movement = $arr_sum_travel_ending-$arr_sum_travel_starting;
                            $travel_movement = numberFormat(($total_travel_movement*-1)); 
                            echo $travel_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_travel_starting!=0)
                            {
                                $total_travel_pct_movement = ($total_travel_movement/$arr_sum_travel_starting)*100;
                            }
                            else
                            {
                                $total_travel_pct_movement = 0;
                            }
                            $pcf_travel_movement = numberFormat($total_travel_pct_movement);
                            echo $pcf_travel_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
                        <tr>
						  <td colspan="2"><b>Equipment and Supplies</b></td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>						
                        <tr>
						  <td>&nbsp;</td>
						  <td>Unrestricted-Purchase or rent of vehicles</td>
                         <td scope="col" align="right">
                          <?php
                            $uprv_starting = numberFormat($arr_sum_uprv_starting*-1);
                            echo $uprv_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $uprv_ending = numberFormat($arr_sum_uprv_ending*-1);  									
                            echo $uprv_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_uprv_movement = ($arr_sum_uprv_ending)-$arr_sum_uprv_starting;
                            $uprv_movement = numberFormat(($total_uprv_movement*-1)); 
                            echo $uprv_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_uprv_starting!=0)
                            {
                                $total_uprv_pct_movement = ($total_uprv_movement/$arr_sum_uprv_starting)*100;
                            }
                            else
                            {
                                $total_uprv_pct_movement = 0;
                            }
                            $pcf_uprv_movement = numberFormat($total_uprv_pct_movement);
                            echo $pcf_uprv_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>Purchase or rent of vehicles</td>
                         <td scope="col" align="right">
                          <?php
                            $prv_starting = numberFormat($arr_sum_prv_starting*-1);
                            echo $prv_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $prv_ending = numberFormat($arr_sum_prv_ending*-1);  									
                            echo $prv_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_prv_movement = ($arr_sum_prv_ending)-$arr_sum_prv_starting;
                            $prv_movement = numberFormat(($total_prv_movement*-1)); 
                            echo $prv_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_prv_starting!=0)
                            {
                                $total_prv_pct_movement = ($total_prv_movement/$arr_sum_prv_starting)*100;
                            }
                            else
                            {
                                $total_prv_pct_movement = 0;
                            }
                            $pcf_prv_movement = numberFormat($total_prv_pct_movement);
                            echo $pcf_prv_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>Restricted-Furniture, Computer Equipment</td>
                         <td scope="col" align="right">
                          <?php
                            $rfce_starting = numberFormat($arr_sum_rfce_starting*-1);
                            echo $rfce_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $rfce_ending = numberFormat($arr_sum_rfce_ending*-1);  									
                            echo $rfce_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_rfce_movement = ($arr_sum_rfce_ending)-$arr_sum_rfce_starting;
                            $rfce_movement = numberFormat(($total_rfce_movement*-1)); 
                            echo $rfce_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_rfce_starting!=0)
                            {
                                $total_rfce_pct_movement = ($total_rfce_movement/$arr_sum_rfce_starting)*100;
                            }
                            else
                            {
                                $total_rfce_pct_movement = 0;
                            }
                            $pcf_rfce_movement = numberFormat($total_rfce_pct_movement);
                            echo $pcf_rfce_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>Unrestricted-Furniture, Computer Equipment</td>
                         <td scope="col" align="right">
                          <?php
                            $ufce_starting = numberFormat($arr_sum_ufce_starting*-1);
                            echo $ufce_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ufce_ending = numberFormat($arr_sum_ufce_ending*-1);  									
                            echo $ufce_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ufce_movement = ($arr_sum_ufce_ending)-$arr_sum_ufce_starting;
                            $ufce_movement = numberFormat(($total_ufce_movement*-1)); 
                            echo $ufce_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ufce_starting!=0)
                            {
                                $total_ufce_pct_movement = ($total_ufce_movement/$arr_sum_ufce_starting)*100;
                            }
                            else
                            {
                                $total_ufce_pct_movement = 0;
                            }
                            $pcf_ufce_movement = numberFormat($total_ufce_pct_movement);
                            echo $pcf_ufce_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>Restricted-Other Equipment</td>
                         <td scope="col" align="right">
                          <?php
                            $roe_starting = numberFormat($arr_sum_roe_starting*-1);
                            echo $roe_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $roe_ending = numberFormat($arr_sum_roe_ending*-1);  									
                            echo $roe_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_roe_movement = ($arr_sum_roe_ending)-$arr_sum_roe_starting;
                            $roe_movement = numberFormat(($total_roe_movement*-1)); 
                            echo $roe_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_roe_starting!=0)
                            {
                                $total_roe_pct_movement = ($total_roe_movement/$arr_sum_roe_starting)*100;
                            }
                            else
                            {
                                $total_roe_pct_movement = 0;
                            }
                            $pcf_roe_movement = numberFormat($total_roe_pct_movement);
                            echo $pcf_roe_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>Unrestricted-Other Equipment</td>
                         <td scope="col" align="right">
                          <?php
                            $uoe_starting = numberFormat($arr_sum_uoe_starting*-1);
                            echo $uoe_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $uoe_ending = numberFormat($arr_sum_uoe_ending*-1);  									
                            echo $uoe_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_uoe_movement = ($arr_sum_uoe_ending)-$arr_sum_uoe_starting;
                            $uoe_movement = numberFormat(($total_uoe_movement*-1)); 
                            echo $uoe_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_uoe_starting!=0)
                            {
                                $total_uoe_pct_movement = ($total_uoe_movement/$arr_sum_uoe_starting)*100;
                            }
                            else
                            {
                                $total_uoe_pct_movement = 0;
                            }
                            $pcf_uoe_movement = numberFormat($total_uoe_pct_movement);
                            echo $pcf_uoe_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>Library materials</td>
                         <td scope="col" align="right">
                          <?php
                            $lm_starting = numberFormat($arr_sum_lm_starting*-1);
                            echo $lm_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $lm_ending = numberFormat($arr_sum_lm_ending*-1);  									
                            echo $lm_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_lm_movement = ($arr_sum_lm_ending)-$arr_sum_lm_starting;
                            $lm_movement = numberFormat(($total_lm_movement*-1)); 
                            echo $lm_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_lm_starting!=0)
                            {
                                $total_lm_pct_movement = ($total_lm_movement/$arr_sum_lm_starting)*100;
                            }
                            else
                            {
                                $total_lm_pct_movement = 0;
                            }
                            $pcf_lm_movement = numberFormat($total_lm_pct_movement);
                            echo $pcf_lm_movement;
                          ?>&nbsp;%                          </td>
						</tr>	
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
						</tr>                         					
                        <tr>
						  <td colspan="2"><b>Subtotal Equipment and Supplies</b></td>
						  <td scope="col" align="right">
                          <?php
                            $ses_starting = numberFormat($arr_sum_ses_starting*-1);
                            echo $ses_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ses_ending = numberFormat($arr_sum_ses_ending*-1);  									
                            echo $ses_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ses_movement = ($arr_sum_ses_ending)-$arr_sum_ses_starting;
                            $ses_movement = numberFormat(($total_ses_movement*-1)); 
                            echo $ses_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ses_starting!=0)
                            {
                                $total_ses_pct_movement = ($total_ses_movement/$arr_sum_ses_starting)*100;
                            }
                            else
                            {
                                $total_ses_pct_movement = 0;
                            }
                            $pcf_ses_movement = numberFormat($total_ses_pct_movement);
                            echo $pcf_ses_movement;
                          ?>&nbsp;%                          </td>
						</tr>						
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>						
                        <tr>
						  <td colspan="2"><b>Local Office / Project Cost</b></td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>                         <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;Vehicle Costs</td>
                         <td scope="col" align="right">
                          <?php
                            $vc_starting = numberFormat($arr_sum_vc_starting*-1);
                            echo $vc_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $vc_ending = numberFormat($arr_sum_vc_ending*-1);  									
                            echo $vc_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_vc_movement = ($arr_sum_vc_ending)-$arr_sum_vc_starting;
                            $vc_movement = numberFormat(($total_vc_movement*-1)); 
                            echo $vc_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_vc_starting!=0)
                            {
                                $total_vc_pct_movement = ($total_vc_movement/$arr_sum_vc_starting)*100;
                            }
                            else
                            {
                                $total_vc_pct_movement = 0;
                            }
                            $pcf_vc_movement = numberFormat($total_vc_pct_movement);
                            echo $pcf_vc_movement;
                          ?>&nbsp;%                          </td>
						</tr>	                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;Consumables - Office Supplies</td>
                         <td scope="col" align="right">
                          <?php
                            $cos_starting = numberFormat($arr_sum_cos_starting*-1);
                            echo $cos_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $cos_ending = numberFormat($arr_sum_cos_ending*-1);  									
                            echo $cos_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_cos_movement = ($arr_sum_cos_ending)-$arr_sum_cos_starting;
                            $cos_movement = numberFormat(($total_cos_movement*-1)); 
                            echo $cos_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_cos_starting!=0)
                            {
                                $total_cos_pct_movement = ($total_cos_movement/$arr_sum_cos_starting)*100;
                            }
                            else
                            {
                                $total_cos_pct_movement = 0;
                            }
                            $pcf_cos_movement = numberFormat($total_cos_pct_movement);
                            echo $pcf_cos_movement;
                          ?>&nbsp;%                          </td>
						</tr>	                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;Maintenance of Computer Equipment</td>
                         <td scope="col" align="right">
                          <?php
                            $moe_starting = numberFormat($arr_sum_moe_starting*-1);
                            echo $moe_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $moe_ending = numberFormat($arr_sum_moe_ending*-1);  									
                            echo $moe_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_moe_movement = ($arr_sum_moe_ending)-$arr_sum_moe_starting;
                            $moe_movement = numberFormat(($total_moe_movement*-1)); 
                            echo $moe_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_moe_starting!=0)
                            {
                                $total_moe_pct_movement = ($total_moe_movement/$arr_sum_moe_starting)*100;
                            }
                            else
                            {
                                $total_moe_pct_movement = 0;
                            }
                            $pcf_moe_movement = numberFormat($total_moe_pct_movement);
                            echo $pcf_moe_movement;
                          ?>&nbsp;%                          </td>
						</tr>	                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;Other Services (tel/fax, electricity/heating)</td>
                         <td scope="col" align="right">
                          <?php
                            $oserv_starting = numberFormat($arr_sum_oserv_starting*-1);
                            echo $oserv_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $oserv_ending = numberFormat($arr_sum_oserv_ending*-1);  									
                            echo $oserv_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_oserv_movement = ($arr_sum_oserv_ending)-$arr_sum_oserv_starting;
                            $oserv_movement = numberFormat(($total_oserv_movement*-1)); 
                            echo $oserv_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_oserv_starting!=0)
                            {
                                $total_oserv_pct_movement = ($total_oserv_movement/$arr_sum_oserv_starting)*100;
                            }
                            else
                            {
                                $total_oserv_pct_movement = 0;
                            }
                            $pcf_oserv_movement = numberFormat($total_oserv_pct_movement);
                            echo $pcf_oserv_movement;
                          ?>&nbsp;%                          </td>
						</tr>	                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;Office Rent</td>
                         <td scope="col" align="right">
                          <?php
                            $orent_starting = numberFormat($arr_sum_orent_starting*-1);
                            echo $orent_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $orent_ending = numberFormat($arr_sum_orent_ending*-1);  									
                            echo $orent_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_orent_movement = ($arr_sum_orent_ending)-$arr_sum_orent_starting;
                            $orent_movement = numberFormat(($total_orent_movement*-1)); 
                            echo $orent_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_orent_starting!=0)
                            {
                                $total_orent_pct_movement = ($total_orent_movement/$arr_sum_orent_starting)*100;
                            }
                            else
                            {
                                $total_orent_pct_movement = 0;
                            }
                            $pcf_orent_movement = numberFormat($total_orent_pct_movement);
                            echo $pcf_orent_movement;
                          ?>&nbsp;%                          </td>
						</tr> 						
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><hr></td>
						  <td><hr></td>
						  <td><hr></td>
						  <td><hr></td>
						</tr>
                        <tr>
						  <td colspan="2"><b>Subtotal Local Office / Project Cost</b></td>
                         <td scope="col" align="right">
                          <?php
                            $lopc_starting = numberFormat($arr_sum_lopc_starting*-1);
                            echo $lopc_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $lopc_ending = numberFormat($arr_sum_lopc_ending*-1);  									
                            echo $lopc_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_lopc_movement = ($arr_sum_lopc_ending)-$arr_sum_lopc_starting;
                            $lopc_movement = numberFormat(($total_lopc_movement*-1)); 
                            echo $lopc_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_lopc_starting!=0)
                            {
                                $total_lopc_pct_movement = ($total_lopc_movement/$arr_sum_lopc_starting)*100;
                            }
                            else
                            {
                                $total_lopc_pct_movement = 0;
                            }
                            $pcf_lopc_movement = numberFormat($total_lopc_pct_movement);
                            echo $pcf_lopc_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td colspan="2"><b>Other Cost, services</b></td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>  
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Guidelines Publications</td>
                         <td scope="col" align="right">
                          <?php
                            $guipub_starting = numberFormat($arr_sum_guipub_starting*-1);
                            echo $guipub_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $guipub_ending = numberFormat($arr_sum_guipub_ending*-1);  									
                            echo $guipub_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_guipub_movement = ($arr_sum_guipub_ending)-$arr_sum_guipub_starting;
                            $guipub_movement = numberFormat(($total_guipub_movement*-1)); 
                            echo $guipub_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_guipub_starting!=0)
                            {
                                $total_guipub_pct_movement = ($total_guipub_movement/$arr_sum_guipub_starting)*100;
                            }
                            else
                            {
                                $total_guipub_pct_movement = 0;
                            }
                            $pcf_guipub_movement = numberFormat($total_guipub_pct_movement);
                            echo $pcf_guipub_movement;
                          ?>&nbsp;%                          </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>ASEAN Biodiversity Magazine</td>
                         <td scope="col" align="right">
                          <?php
                            $abmag_starting = numberFormat($arr_sum_abmag_starting*-1);
                            echo $abmag_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $abmag_ending = numberFormat($arr_sum_abmag_ending*-1);  									
                            echo $abmag_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_abmag_movement = ($arr_sum_abmag_ending)-$arr_sum_abmag_starting;
                            $abmag_movement = numberFormat(($total_abmag_movement*-1)); 
                            echo $abmag_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_abmag_starting!=0)
                            {
                                $total_abmag_pct_movement = ($total_abmag_movement/$arr_sum_abmag_starting)*100;
                            }
                            else
                            {
                                $total_abmag_pct_movement = 0;
                            }
                            $pcf_abmag_movement = numberFormat($total_abmag_pct_movement);
                            echo $pcf_abmag_movement;
                          ?>&nbsp;%                          </td>                             
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Graphic design, video and e-newsletter</td>
                         <td scope="col" align="right">
                          <?php
                            $gdve_starting = numberFormat($arr_sum_gdve_starting*-1);
                            echo $gdve_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $gdve_ending = numberFormat($arr_sum_gdve_ending*-1);  									
                            echo $gdve_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_gdve_movement = ($arr_sum_gdve_ending)-$arr_sum_gdve_starting;
                            $gdve_movement = numberFormat(($total_gdve_movement*-1)); 
                            echo $gdve_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_gdve_starting!=0)
                            {
                                $total_gdve_pct_movement = ($total_gdve_movement/$arr_sum_gdve_starting)*100;
                            }
                            else
                            {
                                $total_gdve_pct_movement = 0;
                            }
                            $pcf_gdve_movement = numberFormat($total_gdve_pct_movement);
                            echo $pcf_gdve_movement;
                          ?>&nbsp;%                          </td>                             
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Promotion of EU actions visibility</td>
                         <td scope="col" align="right">
                          <?php
                            $peav_starting = numberFormat($arr_sum_peav_starting*-1);
                            echo $peav_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $peav_ending = numberFormat($arr_sum_peav_ending*-1);  									
                            echo $peav_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_peav_movement = ($arr_sum_peav_ending)-$arr_sum_peav_starting;
                            $peav_movement = numberFormat(($total_peav_movement*-1)); 
                            echo $peav_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_peav_starting!=0)
                            {
                                $total_peav_pct_movement = ($total_peav_movement/$arr_sum_peav_starting)*100;
                            }
                            else
                            {
                                $total_peav_pct_movement = 0;
                            }
                            $pcf_peav_movement = numberFormat($total_peav_pct_movement);
                            echo $pcf_peav_movement;
                          ?>&nbsp;%                          </td>                              
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Studies, research</td>
                         <td scope="col" align="right">
                          <?php
                            $studre_starting = numberFormat($arr_sum_studre_starting*-1);
                            echo $studre_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $studre_ending = numberFormat($arr_sum_studre_ending*-1);  									
                            echo $studre_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_studre_movement = ($arr_sum_studre_ending)-$arr_sum_studre_starting;
                            $studre_movement = numberFormat(($total_studre_movement*-1)); 
                            echo $studre_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_studre_starting!=0)
                            {
                                $total_studre_pct_movement = ($total_studre_movement/$arr_sum_studre_starting)*100;
                            }
                            else
                            {
                                $total_studre_pct_movement = 0;
                            }
                            $pcf_studre_movement = numberFormat($total_studre_pct_movement);
                            echo $pcf_studre_movement;
                          ?>&nbsp;%                          </td>                             
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Supplemental studies</td>
                         <td scope="col" align="right">
                          <?php
                            $supstud_starting = numberFormat($arr_sum_supstud_starting*-1);
                            echo $supstud_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $supstud_ending = numberFormat($arr_sum_supstud_ending*-1);  									
                            echo $supstud_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_supstud_movement = ($arr_sum_supstud_ending)-$arr_sum_supstud_starting;
                            $supstud_movement = numberFormat(($total_supstud_movement*-1)); 
                            echo $supstud_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_supstud_starting!=0)
                            {
                                $total_supstud_pct_movement = ($total_supstud_movement/$arr_sum_supstud_starting)*100;
                            }
                            else
                            {
                                $total_supstud_pct_movement = 0;
                            }
                            $pcf_supstud_movement = numberFormat($total_supstud_pct_movement);
                            echo $pcf_supstud_movement;
                          ?>&nbsp;%                          </td>                             
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Local short term expert</td>
                         <td scope="col" align="right">
                          <?php
                            $lste_starting = numberFormat($arr_sum_lste_starting*-1);
                            echo $lste_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $lste_ending = numberFormat($arr_sum_lste_ending*-1);  									
                            echo $lste_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_lste_movement = ($arr_sum_lste_ending)-$arr_sum_lste_starting;
                            $lste_movement = numberFormat(($total_lste_movement*-1)); 
                            echo $lste_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_lste_starting!=0)
                            {
                                $total_lste_pct_movement = ($total_lste_movement/$arr_sum_lste_starting)*100;
                            }
                            else
                            {
                                $total_lste_pct_movement = 0;
                            }
                            $pcf_lste_movement = numberFormat($total_lste_pct_movement);
                            echo $pcf_lste_movement;
                          ?>&nbsp;%                          </td>                            
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>IT Local Specialist staff (Focal points)</td>
                         <td scope="col" align="right">
                          <?php
                            $ilssfp_starting = numberFormat($arr_sum_ilssfp_starting*-1);
                            echo $ilssfp_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ilssfp_ending = numberFormat($arr_sum_ilssfp_ending*-1);  									
                            echo $ilssfp_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ilssfp_movement = ($arr_sum_ilssfp_ending)-$arr_sum_ilssfp_starting;
                            $ilssfp_movement = numberFormat(($total_ilssfp_movement*-1)); 
                            echo $ilssfp_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ilssfp_starting!=0)
                            {
                                $total_ilssfp_pct_movement = ($total_ilssfp_movement/$arr_sum_ilssfp_starting)*100;
                            }
                            else
                            {
                                $total_ilssfp_pct_movement = 0;
                            }
                            $pcf_ilssfp_movement = numberFormat($total_ilssfp_pct_movement);
                            echo $pcf_ilssfp_movement;
                          ?>&nbsp;%                          </td>                              
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Cost of Regional training session (2 weeks, 20)</td>
                         <td scope="col" align="right">
                          <?php
                            $corts2wk_starting = numberFormat($arr_sum_corts2wk_starting*-1);
                            echo $corts2wk_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $corts2wk_ending = numberFormat($arr_sum_corts2wk_ending*-1);  									
                            echo $corts2wk_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_corts2wk_movement = ($arr_sum_corts2wk_ending)-$arr_sum_corts2wk_starting;
                            $corts2wk_movement = numberFormat(($total_corts2wk_movement*-1)); 
                            echo $corts2wk_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_corts2wk_starting!=0)
                            {
                                $total_corts2wk_pct_movement = ($total_corts2wk_movement/$arr_sum_corts2wk_starting)*100;
                            }
                            else
                            {
                                $total_corts2wk_pct_movement = 0;
                            }
                            $pcf_corts2wk_movement = numberFormat($total_corts2wk_pct_movement);
                            echo $pcf_corts2wk_movement;
                          ?>&nbsp;%                          </td>                            
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Cost of Regional Training Session (1 week, 20)</td>
                         <td scope="col" align="right">
                          <?php
                            $corts1wk_starting = numberFormat($arr_sum_corts1wk_starting*-1);
                            echo $corts1wk_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $corts1wk_ending = numberFormat($arr_sum_corts1wk_ending*-1);  									
                            echo $corts1wk_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_corts1wk_movement = ($arr_sum_corts1wk_ending)-$arr_sum_corts1wk_starting;
                            $corts1wk_movement = numberFormat(($total_corts1wk_movement*-1)); 
                            echo $corts1wk_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_corts1wk_starting!=0)
                            {
                                $total_corts1wk_pct_movement = ($total_corts1wk_movement/$arr_sum_corts1wk_starting)*100;
                            }
                            else
                            {
                                $total_corts1wk_pct_movement = 0;
                            }
                            $pcf_corts1wk_movement = numberFormat($total_corts1wk_pct_movement);
                            echo $pcf_corts1wk_movement;
                          ?>&nbsp;%                          </td>                          
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Cost of Sub Regional training session (1 week)</td>
                         <td scope="col" align="right">
                          <?php
                            $cosrts1w_starting = numberFormat($arr_sum_cosrts1w_starting*-1);
                            echo $cosrts1w_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $cosrts1w_ending = numberFormat($arr_sum_cosrts1w_ending*-1);  									
                            echo $cosrts1w_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_cosrts1w_movement = ($arr_sum_cosrts1w_ending)-$arr_sum_cosrts1w_starting;
                            $cosrts1w_movement = numberFormat(($total_cosrts1w_movement*-1)); 
                            echo $cosrts1w_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_cosrts1w_starting!=0)
                            {
                                $total_cosrts1w_pct_movement = ($total_cosrts1w_movement/$arr_sum_cosrts1w_starting)*100;
                            }
                            else
                            {
                                $total_cosrts1w_pct_movement = 0;
                            }
                            $pcf_cosrts1w_movement = numberFormat($total_cosrts1w_pct_movement);
                            echo $pcf_cosrts1w_movement;
                          ?>&nbsp;%                          </td>                            
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td>Cost of Workshops</td>
                         <td scope="col" align="right">
                          <?php
                            $costwor_starting = numberFormat($arr_sum_costwor_starting*-1);
                            echo $costwor_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $costwor_ending = numberFormat($arr_sum_costwor_ending*-1);  									
                            echo $costwor_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_costwor_movement = ($arr_sum_costwor_ending)-$arr_sum_costwor_starting;
                            $costwor_movement = numberFormat(($total_costwor_movement*-1)); 
                            echo $costwor_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_costwor_starting!=0)
                            {
                                $total_costwor_pct_movement = ($total_costwor_movement/$arr_sum_costwor_starting)*100;
                            }
                            else
                            {
                                $total_costwor_pct_movement = 0;
                            }
                            $pcf_costwor_movement = numberFormat($total_costwor_pct_movement);
                            echo $pcf_costwor_movement;
                          ?>&nbsp;%                          </td>                            
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Support to higher education</td>
                        <td scope="col" align="right">
                          <?php
                            $suptohiged_starting = numberFormat($arr_sum_suptohiged_starting*-1);
                            echo $suptohiged_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $suptohiged_ending = numberFormat($arr_sum_suptohiged_ending*-1);  									
                            echo $suptohiged_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_suptohiged_movement = ($arr_sum_suptohiged_ending)-$arr_sum_suptohiged_starting;
                            $suptohiged_movement = numberFormat(($total_suptohiged_movement*-1)); 
                            echo $suptohiged_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_suptohiged_starting!=0)
                            {
                                $total_suptohiged_pct_movement = ($total_suptohiged_movement/$arr_sum_suptohiged_starting)*100;
                            }
                            else
                            {
                                $total_suptohiged_pct_movement = 0;
                            }
                            $pcf_suptohiged_movement = numberFormat($total_suptohiged_pct_movement);
                            echo $pcf_suptohiged_movement;
                          ?>&nbsp;%                          </td>                              
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Attendance to external ASEAN/EU courses</td>
                         <td scope="col" align="right">
                          <?php
                            $ateac_starting = numberFormat($arr_sum_ateac_starting*-1);
                            echo $ateac_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ateac_ending = numberFormat($arr_sum_ateac_ending*-1);  									
                            echo $ateac_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ateac_movement = ($arr_sum_ateac_ending)-$arr_sum_ateac_starting;
                            $ateac_movement = numberFormat(($total_ateac_movement*-1)); 
                            echo $ateac_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ateac_starting!=0)
                            {
                                $total_ateac_pct_movement = ($total_ateac_movement/$arr_sum_ateac_starting)*100;
                            }
                            else
                            {
                                $total_ateac_pct_movement = 0;
                            }
                            $pcf_ateac_movement = numberFormat($total_ateac_pct_movement);
                            echo $pcf_ateac_movement;
                          ?>&nbsp;%                          </td>                             
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Exchange visits and study tours (ASEAN and EU)</td>
                         <td scope="col" align="right">
                          <?php
                            $evast_starting = numberFormat($arr_sum_evast_starting*-1);
                            echo $evast_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $evast_ending = numberFormat($arr_sum_evast_ending*-1);  									
                            echo $evast_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_evast_movement = ($arr_sum_evast_ending)-$arr_sum_evast_starting;
                            $evast_movement = numberFormat(($total_evast_movement*-1)); 
                            echo $evast_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_evast_starting!=0)
                            {
                                $total_evast_pct_movement = ($total_evast_movement/$arr_sum_evast_starting)*100;
                            }
                            else
                            {
                                $total_evast_pct_movement = 0;
                            }
                            $pcf_evast_movement = numberFormat($total_evast_pct_movement);
                            echo $pcf_evast_movement;
                          ?>&nbsp;%                          </td>                              
                        </tr>
                        <tr>
                          <td>&nbsp;</td>                            
                          <td>Evaluation Costs</td>
                         <td scope="col" align="right">
                          <?php
                            $evacost_starting = numberFormat($arr_sum_evacost_starting*-1);
                            echo $evacost_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $evacost_ending = numberFormat($arr_sum_evacost_ending*-1);  									
                            echo $evacost_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_evacost_movement = ($arr_sum_evacost_ending)-$arr_sum_evacost_starting;
                            $evacost_movement = numberFormat(($total_evacost_movement*-1)); 
                            echo $evacost_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_evacost_starting!=0)
                            {
                                $total_evacost_pct_movement = ($total_evacost_movement/$arr_sum_evacost_starting)*100;
                            }
                            else
                            {
                                $total_evacost_pct_movement = 0;
                            }
                            $pcf_evacost_movement = numberFormat($total_evacost_pct_movement);
                            echo $pcf_evacost_movement;
                          ?>&nbsp;%                          </td>                              
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                          <td>PSC Committee and sub commitees, Trustees</td>
                         <td scope="col" align="right">
                          <?php
                            $pcsct_starting = numberFormat($arr_sum_pcsct_starting*-1);
                            echo $pcsct_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $pcsct_ending = numberFormat($arr_sum_pcsct_ending*-1);  									
                            echo $pcsct_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_pcsct_movement = ($arr_sum_pcsct_ending)-$arr_sum_pcsct_starting;
                            $pcsct_movement = numberFormat(($total_pcsct_movement*-1)); 
                            echo $pcsct_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_pcsct_starting!=0)
                            {
                                $total_pcsct_pct_movement = ($total_pcsct_movement/$arr_sum_pcsct_starting)*100;
                            }
                            else
                            {
                                $total_pcsct_pct_movement = 0;
                            }
                            $pcf_pcsct_movement = numberFormat($total_pcsct_pct_movement);
                            echo $pcf_pcsct_movement;
                          ?>&nbsp;%                          </td>                              
                        </tr>                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
						</tr> 						
                        <tr>
						  <td colspan="2"><b>Subtotal Other Cost, services</b></td>
                         <td scope="col" align="right">
                          <?php
                            $ocostser_starting = numberFormat($arr_sum_ocostser_starting*-1);
                            echo $ocostser_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $ocostser_ending = numberFormat($arr_sum_ocostser_ending*-1);  									
                            echo $ocostser_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_ocostser_movement = ($arr_sum_ocostser_ending)-$arr_sum_ocostser_starting;
                            $ocostser_movement = numberFormat(($total_ocostser_movement*-1)); 
                            echo $ocostser_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_ocostser_starting!=0)
                            {
                                $total_ocostser_pct_movement = ($total_ocostser_movement/$arr_sum_ocostser_starting)*100;
                            }
                            else
                            {
                                $total_ocostser_pct_movement = 0;
                            }
                            $pcf_ocostser_movement = numberFormat($total_ocostser_pct_movement);
                            echo $pcf_ocostser_movement;
                          ?>&nbsp;%                          </td>
						</tr> 
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>                          
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>                        						
                        <tr>
						  <td colspan="2"><b>Auditing and Recruitment Costs</b></td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr> 						
                        <tr>
                          <td>&nbsp;</td>
						  <td>Auditing Cost</td>
                         <td scope="col" align="right">
                          <?php
                            $auccost_starting = numberFormat($arr_sum_auccost_starting*-1);
                            echo $auccost_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $auccost_ending = numberFormat($arr_sum_auccost_ending*-1);  									
                            echo $auccost_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_auccost_movement = ($arr_sum_auccost_ending)-$arr_sum_auccost_starting;
                            $auccost_movement = numberFormat(($total_auccost_movement*-1)); 
                            echo $auccost_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_auccost_starting!=0)
                            {
                                $total_auccost_pct_movement = ($total_auccost_movement/$arr_sum_auccost_starting)*100;
                            }
                            else
                            {
                                $total_auccost_pct_movement = 0;
                            }
                            $pcf_auccost_movement = numberFormat($total_auccost_pct_movement);
                            echo $pcf_auccost_movement;
                          ?>&nbsp;%                          </td>
						</tr>                        
                        <tr>
                          <td>&nbsp;</td>
						  <td>Recruitment Cost</td>
                         <td scope="col" align="right">
                          <?php
                            $reccost_starting = numberFormat($arr_sum_reccost_starting*-1);
                            echo $reccost_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $reccost_ending = numberFormat($arr_sum_reccost_ending*-1);  									
                            echo $reccost_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_reccost_movement = ($arr_sum_reccost_ending)-$arr_sum_reccost_starting;
                            $reccost_movement = numberFormat(($total_reccost_movement*-1)); 
                            echo $reccost_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_reccost_starting!=0)
                            {
                                $total_reccost_pct_movement = ($total_reccost_movement/$arr_sum_reccost_starting)*100;
                            }
                            else
                            {
                                $total_reccost_pct_movement = 0;
                            }
                            $pcf_reccost_movement = numberFormat($total_reccost_pct_movement);
                            echo $pcf_reccost_movement;
                          ?>&nbsp;%                          </td>
						</tr>
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
						</tr>                         						
                        <tr>
						  <td colspan="2"><b>Subtotal Auditing and Recruitment Costs</b></td>
                         <td scope="col" align="right">
                          <?php
                            $arcost_starting = numberFormat($arr_sum_arcost_starting*-1);
                            echo $arcost_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $arcost_ending = numberFormat($arr_sum_arcost_ending*-1);  									
                            echo $arcost_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_arcost_movement = ($arr_sum_arcost_ending)-$arr_sum_arcost_starting;
                            $arcost_movement = numberFormat(($total_arcost_movement*-1)); 
                            echo $arcost_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_arcost_starting!=0)
                            {
                                $total_arcost_pct_movement = ($total_arcost_movement/$arr_sum_arcost_starting)*100;
                            }
                            else
                            {
                                $total_arcost_pct_movement = 0;
                            }
                            $pcf_arcost_movement = numberFormat($total_arcost_pct_movement);
                            echo $pcf_arcost_movement;
                          ?>&nbsp;%                          </td>
						</tr> 						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>							
                        <tr>
						  <td>&nbsp;</td>
						  <td>Administrative Costs</td>
                         <td scope="col" align="right">
                          <?php
                            $admincost_starting = numberFormat($arr_sum_admincost_starting*-1);
                            echo $admincost_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $admincost_ending = numberFormat($arr_sum_admincost_ending*-1);  									
                            echo $admincost_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_admincost_movement = $arr_sum_admincost_ending-$arr_sum_admincost_starting;
                            $admincost_movement = numberFormat(($total_admincost_movement*-1)); 
                            echo $admincost_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_admincost_starting!=0)
                            {
                                $total_admincost_pct_movement = ($total_admincost_movement/$arr_sum_admincost_starting)*100;
                            }
                            else
                            {
                                $total_admincost_pct_movement = 0;
                            }
                            $pcf_admincost_movement = numberFormat($total_admincost_pct_movement);
                            echo $pcf_admincost_movement;
                          ?>&nbsp;%                          </td>
						</tr>							
                        <tr>
						  <td>&nbsp;</td>
						  <td>Contingencies</td>
                         <td scope="col" align="right">
                          <?php
                            $cntngncy_starting = numberFormat($arr_sum_cntngncy_starting*-1);
                            echo $cntngncy_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $cntngncy_ending = numberFormat($arr_sum_cntngncy_ending*-1);  									
                            echo $cntngncy_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_cntngncy_movement = $arr_sum_cntngncy_ending-$arr_sum_cntngncy_starting;
                            $cntngncy_movement = numberFormat(($total_cntngncy_movement*-1)); 
                            echo $cntngncy_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_cntngncy_starting!=0)
                            {
                                $total_cntngncy_pct_movement = ($total_cntngncy_movement/$arr_sum_cntngncy_starting)*100;
                            }
                            else
                            {
                                $total_cntngncy_pct_movement = 0;
                            }
                            $pcf_cntngncy_movement = numberFormat($total_cntngncy_pct_movement);
                            echo $pcf_cntngncy_movement;
                          ?>&nbsp;%                          </td>
						</tr>	                        
                        <tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
                          <td><hr></td>
						</tr>                        
                        <tr>
						  <td colspan="2"><b>Total Disbursements</b></td>
                         <td scope="col" align="right">
                          <?php
                            $totdis_starting = numberFormat($arr_sum_totdis_starting*-1);
                            echo $totdis_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $totdis_ending = numberFormat($arr_sum_totdis_ending*-1);  									
                            echo $totdis_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_totdis_movement = ($arr_sum_totdis_ending)-$arr_sum_totdis_starting;
                            $totdis_movement = numberFormat(($total_totdis_movement*-1)); 
                            echo $totdis_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_totdis_starting!=0)
                            {
                                $total_totdis_pct_movement = ($total_totdis_movement/$arr_sum_totdis_starting)*100;
                            }
                            else
                            {
                                $total_totdis_pct_movement = 0;
                            }
                            $pcf_totdis_movement = numberFormat($total_totdis_pct_movement);
                            echo $pcf_totdis_movement;
                          ?>&nbsp;%                          </td>
						</tr>			
                        <tr>			  
                        <td colspan="2"><b>Excess of Receipts over Disbursements</b></td>
                         <td scope="col" align="right">
                          <?php
                            $eorod_starting = numberFormat($arr_sum_eorod_starting);
                            echo $eorod_starting;
                          ?>                          </td>
                          <td scope="col" align="right">
                            <?php
                            $eorod_ending = numberFormat($arr_sum_eorod_ending);  									
                            echo $eorod_ending;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            $total_eorod_movement = ($arr_sum_eorod_ending)-$arr_sum_eorod_starting;
                            $eorod_movement = numberFormat($total_eorod_movement); 
                            echo $eorod_movement;
                          ?>                          </td>
                          <td scope="col" align="right">
                          <?php
                            if ($arr_sum_eorod_starting!=0)
                            {
                                $total_eorod_pct_movement = ($total_eorod_movement/$arr_sum_eorod_starting)*100;
                            }
                            else
                            {
                                $total_eorod_pct_movement = 0;
                            }
                            $pcf_eorod_movement = numberFormat($total_eorod_pct_movement);
                            echo $pcf_eorod_movement;
                          ?>&nbsp;%                          </td>                    
						</tr>                                                                                                                                            
					</tbody>
					<tfoot>
						<tr>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
						</tr>				
					</tfoot>
                  </table>
				  <!-- End of Inner Table -->
				  </td>
				</tr>
			</table>	
		</td>
	</tr>
</table>


<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>
