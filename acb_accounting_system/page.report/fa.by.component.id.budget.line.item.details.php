<?php 
$report_name = 'FUND ANALYSIS - Per Component ID, Per Budget Line';
include('./../includes/header.report.php');

$fa_currency_code = $_POST['currency_code'];
$fa_start_year = $_POST['start_year'];
$fa_start_month = $_POST['start_month'];
$fa_start_day = $_POST['start_day'];
$fa_end_year = $_POST['end_year'];
$fa_end_month = $_POST['end_month'];
$fa_end_day = $_POST['end_day'];
$consolidate_reports = $_POST['consolidate_reports'];
$component_id = $_POST['component_id'];
$use_obligated_values = $_POST['use_obligated_values'];

$reference_date = dateFormat(1,1,$fa_start_year);
$start_date = dateFormat($fa_start_month,$fa_start_day,$fa_start_year);
$end_date = dateFormat($fa_end_month,$fa_end_day,$fa_end_year);
$budget_identification = getBudgetIdentification($fa_start_year);

$fa_currency_name = getConfigurationValueById($fa_currency_code);
$eu_budget_name = getBudgetIdByName("EU");
$ams_budget_name = getBudgetIdByName("AMS");
$donor_budget_name = getBudgetIdByName("DONOR");

$dars_sl_id = getSlIdByAccountCode("30020");
if ($dars_sl_id!="")
{								  
	$c11 = getFAByComponentByBudget($dars_sl_id,$component_id,$fa_start_year);
	$arr_dars_eu_expenditures[] = getDebitCreditSumValue($dars_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_dars_ams_expenditures[] = getDebitCreditSumValue($dars_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_dars_donor_expenditures[] = getDebitCreditSumValue($dars_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	
	$d11 = array_sum($arr_dars_eu_expenditures);
	$e11 = array_sum($arr_dars_ams_expenditures);
	$f11 = array_sum($arr_dars_donor_expenditures);
	
	if ($fa_start_month!=1)
	{
		$tetd_eu_dars[] = getDebitCreditSumValue($dars_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_dars[] = getDebitCreditSumValue($dars_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_dars[] = getDebitCreditSumValue($dars_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
		$dars_tetd = array_sum($tetd_eu_dars) + array_sum($tetd_ams_dars) + array_sum($tetd_donor_dars);
	}
} 
$ss_sl_id = getSlIdByAccountCode("30060");
if ($ss_sl_id!="")
{								  
	$c12 = getFAByComponentByBudget($ss_sl_id,$component_id,$fa_start_year);
	$arr_ss_eu_expenditures[] = getDebitCreditSumValue($ss_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ss_ams_expenditures[] = getDebitCreditSumValue($ss_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ss_donor_expenditures[] = getDebitCreditSumValue($ss_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
	$d12 = array_sum($arr_ss_eu_expenditures);
	$e12 = array_sum($arr_ss_ams_expenditures);
	$f12 = array_sum($arr_ss_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_ss[] = getDebitCreditSumValue($ss_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_ss[] = getDebitCreditSumValue($ss_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_ss[] = getDebitCreditSumValue($ss_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$ss_tetd = array_sum($tetd_eu_ss) + array_sum($tetd_ams_ss) + array_sum($tetd_donor_ss);
	}	
}
$sss_sl_id = getSlIdByAccountCode("30050");
if ($sss_sl_id!="")
{								  
	$c13 = getFAByComponentByBudget($sss_sl_id,$component_id,$fa_start_year);
	$arr_sss_eu_expenditures[] = getDebitCreditSumValue($sss_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sss_ams_expenditures[] = getDebitCreditSumValue($sss_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sss_donor_expenditures[] = getDebitCreditSumValue($sss_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d13 = array_sum($arr_sss_eu_expenditures);
	$e13 = array_sum($arr_sss_ams_expenditures);
	$f13 = array_sum($arr_sss_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_sss[] = getDebitCreditSumValue($sss_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_sss[] = getDebitCreditSumValue($sss_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_sss[] = getDebitCreditSumValue($sss_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
			
		$sss_tetd = array_sum($tetd_eu_sss) + array_sum($tetd_ams_sss) + array_sum($tetd_donor_sss);
	}	
} 

$ed_ars_sl_id = getSlIdByAccountCode("30010");
if ($ed_ars_sl_id!="")
{								  
	$c15 = getFAByComponentByBudget($ed_ars_sl_id,$component_id,$fa_start_year);
	$arr_ed_ars_eu_expenditures[] = getDebitCreditSumValue($ed_ars_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ed_ars_ams_expenditures[] = getDebitCreditSumValue($ed_ars_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ed_ars_donor_expenditures[] = getDebitCreditSumValue($ed_ars_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d15 = array_sum($arr_ed_ars_eu_expenditures);
	$e15 = array_sum($arr_ed_ars_ams_expenditures);
	$f15 = array_sum($arr_ed_ars_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_ed_ars[] = getDebitCreditSumValue($ed_ars_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_ed_ars[] = getDebitCreditSumValue($ed_ars_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_ed_ars[] = getDebitCreditSumValue($ed_ars_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);					
		$ed_ars_tetd = array_sum($tetd_eu_ed_ars) + array_sum($tetd_ams_ed_ars) + array_sum($tetd_donor_ed_ars);
	}	
}
$fa_ars_sl_id = getSlIdByAccountCode("30030");
if ($fa_ars_sl_id!="")
{								  
	$c16 = getFAByComponentByBudget($fa_ars_sl_id,$component_id,$fa_start_year);
	$arr_fa_ars_eu_expenditures[] = getDebitCreditSumValue($fa_ars_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_fa_ars_ams_expenditures[] = getDebitCreditSumValue($fa_ars_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_fa_ars_donor_expenditures[] = getDebitCreditSumValue($fa_ars_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d16 = array_sum($arr_fa_ars_eu_expenditures);
	$e16 = array_sum($arr_fa_ars_ams_expenditures);
	$f16 = array_sum($arr_fa_ars_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_fa_ars[] = getDebitCreditSumValue($fa_ars_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_fa_ars[] = getDebitCreditSumValue($fa_ars_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_fa_ars[] = getDebitCreditSumValue($fa_ars_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);					
		$fa_ars_tetd = array_sum($tetd_eu_fa_ars) + array_sum($tetd_ams_fa_ars) + array_sum($tetd_donor_fa_ars);
	}	
}
$oed_pa_sl_id = getSlIdByAccountCode("30040");
if ($oed_pa_sl_id!="")
{								  
	$c17 = getFAByComponentByBudget($oed_pa_sl_id,$component_id,$fa_start_year);
	$arr_oed_pa_eu_expenditures[] = getDebitCreditSumValue($oed_pa_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_oed_pa_ams_expenditures[] = getDebitCreditSumValue($oed_pa_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_oed_pa_donor_expenditures[] = getDebitCreditSumValue($oed_pa_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d17 = array_sum($arr_oed_pa_eu_expenditures);
	$e17 = array_sum($arr_oed_pa_ams_expenditures);
	$f17 = array_sum($arr_oed_pa_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_oed_pa[] = getDebitCreditSumValue($oed_pa_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_oed_pa[] = getDebitCreditSumValue($oed_pa_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_oed_pa[] = getDebitCreditSumValue($oed_pa_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
					
		$oed_pa_tetd = array_sum($tetd_eu_oed_pa) + array_sum($tetd_ams_oed_pa) + array_sum($tetd_donor_oed_pa);
	}	
}
$sss_admin_sl_id = getSlIdByAccountCode("30070");
if ($sss_admin_sl_id!="")
{								  
	$c18 = getFAByComponentByBudget($sss_admin_sl_id,$component_id,$fa_start_year);
	$arr_sss_admin_eu_expenditures[] = getDebitCreditSumValue($sss_admin_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sss_admin_ams_expenditures[] = getDebitCreditSumValue($sss_admin_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sss_admin_donor_expenditures[] = getDebitCreditSumValue($sss_admin_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d18 = array_sum($arr_sss_admin_eu_expenditures);
	$e18 = array_sum($arr_sss_admin_ams_expenditures);
	$f18 = array_sum($arr_sss_admin_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_sss_admin[] = getDebitCreditSumValue($sss_admin_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_sss_admin[] = getDebitCreditSumValue($sss_admin_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_sss_admin[] = getDebitCreditSumValue($sss_admin_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
					
		$sss_admin_tetd = array_sum($tetd_eu_sss_admin) + array_sum($tetd_ams_sss_admin) + array_sum($tetd_donor_sss_admin);
	}	
}
$gss_sl_id = getSlIdByAccountCode("30080");
if ($gss_sl_id!="")
{								  
	$c19 = getFAByComponentByBudget($gss_sl_id,$component_id,$fa_start_year);
	$arr_gss_eu_expenditures[] = getDebitCreditSumValue($gss_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_gss_ams_expenditures[] = getDebitCreditSumValue($gss_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_gss_donor_expenditures[] = getDebitCreditSumValue($gss_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d19 = array_sum($arr_gss_eu_expenditures);
	$e19 = array_sum($arr_gss_ams_expenditures);
	$f19 = array_sum($arr_gss_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_gss[] = getDebitCreditSumValue($gss_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_gss[] = getDebitCreditSumValue($gss_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_gss[] = getDebitCreditSumValue($gss_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

		$gss_tetd = array_sum($tetd_eu_gss) + array_sum($tetd_ams_gss) + array_sum($tetd_donor_gss);
	}	
}

$c14 = $c15+$c16+$c17+$c18+$c19;
$d14 = $d15+$d16+$d17+$d18+$d19;
$e14 = $e15+$e16+$e17+$e18+$e19;
$f14 = $f15+$f16+$f17+$f18+$f19;
$ed_fa_oed_sss_gss_tetd = ed_ars_tetd+$fa_ars_tetd+$oed_pa_tetd+$sss_admin_tetd+$gss_tetd;

$sa_sl_id = getSlIdByAccountCode("30090");
if ($sa_sl_id!="")
{								  
	$c20 = getFAByComponentByBudget($sa_sl_id,$component_id,$fa_start_year);
	$arr_sa_eu_expenditures[] = getDebitCreditSumValue($sa_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sa_ams_expenditures[] = getDebitCreditSumValue($sa_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sa_donor_expenditures[] = getDebitCreditSumValue($sa_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d20 = array_sum($arr_sa_eu_expenditures);
	$e20 = array_sum($arr_sa_ams_expenditures);
	$f20 = array_sum($arr_sa_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_sa[] = getDebitCreditSumValue($sa_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_sa[] = getDebitCreditSumValue($sa_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_sa[] = getDebitCreditSumValue($sa_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		

		$sa_tetd = array_sum($tetd_eu_sa) + array_sum($tetd_ams_sa) + array_sum($tetd_donor_sa);
	}	
}
$seis_sl_id = getSlIdByAccountCode("30100");
if ($seis_sl_id!="")
{								  
	$c21 = getFAByComponentByBudget($seis_sl_id,$component_id,$fa_start_year);
	$arr_seis_eu_expenditures[] = getDebitCreditSumValue($seis_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_seis_ams_expenditures[] = getDebitCreditSumValue($seis_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_seis_donor_expenditures[] = getDebitCreditSumValue($seis_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d21 = array_sum($arr_seis_eu_expenditures);
	$e21 = array_sum($arr_seis_ams_expenditures);
	$f21 = array_sum($arr_seis_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_seis[] = getDebitCreditSumValue($seis_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_seis[] = getDebitCreditSumValue($seis_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_seis[] = getDebitCreditSumValue($seis_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
			
		$seis_tetd = array_sum($tetd_eu_seis) + array_sum($tetd_ams_seis) + array_sum($tetd_donor_seis);
	}		
}
$aps_sl_id = getSlIdByAccountCode("30200");
if ($aps_sl_id!="")
{								  
	$c23 = getFAByComponentByBudget($aps_sl_id,$component_id,$fa_start_year);
	$arr_aps_eu_expenditures[] = getDebitCreditSumValue($aps_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_aps_ams_expenditures[] = getDebitCreditSumValue($aps_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_aps_donor_expenditures[] = getDebitCreditSumValue($aps_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
		
	$d23 = array_sum($arr_aps_eu_expenditures);
	$e23 = array_sum($arr_aps_ams_expenditures);
	$f23 = array_sum($arr_aps_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_aps[] = getDebitCreditSumValue($aps_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_aps[] = getDebitCreditSumValue($aps_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_aps[] = getDebitCreditSumValue($aps_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
		$aps_tetd = array_sum($tetd_eu_aps) + array_sum($tetd_ams_aps) + array_sum($tetd_donor_aps);
	}		
}

$lacc_ps_sl_id = getSlIdByAccountCode("30300");
if ($lacc_ps_sl_id!="")
{								  
	$c24 = getFAByComponentByBudget($lacc_ps_sl_id,$component_id,$fa_start_year);
	$arr_lacc_ps_eu_expenditures[] = getDebitCreditSumValue($lacc_ps_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_lacc_ps_ams_expenditures[] = getDebitCreditSumValue($lacc_ps_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_lacc_ps_donor_expenditures[] = getDebitCreditSumValue($lacc_ps_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d24 = array_sum($arr_lacc_ps_eu_expenditures);
	$e24 = array_sum($arr_lacc_ps_ams_expenditures);
	$f24 = array_sum($arr_lacc_ps_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_lacc_ps[] = getDebitCreditSumValue($lacc_ps_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_lacc_ps[] = getDebitCreditSumValue($lacc_ps_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_lacc_ps[] = getDebitCreditSumValue($lacc_ps_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$lacc_ps_tetd = array_sum($tetd_eu_lacc_ps) + array_sum($tetd_ams_lacc_ps) + array_sum($tetd_donor_lacc_ps);
	}		
}

$c22 = $c23+$c24;
$d22 = $d23+$d24;
$e22 = $e23+$e24;
$f22 = $f23+$f24;

$intercon_travel_sl_id = getSlIdByAccountCode("31100");
if ($intercon_travel_sl_id!="")
{								  
	$c27 = getFAByComponentByBudget($intercon_travel_sl_id,$component_id,$fa_start_year);	
	$arr_intercon_travel_eu_expenditures[] = getDebitCreditSumValue($intercon_travel_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_intercon_travel_ams_expenditures[] = getDebitCreditSumValue($intercon_travel_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_intercon_travel_donor_expenditures[] = getDebitCreditSumValue($intercon_travel_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d27 = array_sum($arr_intercon_travel_eu_expenditures);
	$e27 = array_sum($arr_intercon_travel_ams_expenditures);
	$f27 = array_sum($arr_intercon_travel_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_intercon_travel[] = getDebitCreditSumValue($intercon_travel_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_intercon_travel[] = getDebitCreditSumValue($intercon_travel_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_intercon_travel[] = getDebitCreditSumValue($intercon_travel_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$intercon_travel_tetd = array_sum($tetd_eu_intercon_travel) + array_sum($tetd_ams_intercon_travel) + array_sum($tetd_donor_intercon_travel);
	}		
}
$intl_travel_sl_id = getSlIdByAccountCode("31200");
if ($intl_travel_sl_id!="")
{								  
	$c28 = getFAByComponentByBudget($intl_travel_sl_id,$component_id,$fa_start_year);
	$arr_intl_travel_eu_expenditures[] = getDebitCreditSumValue($intl_travel_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_intl_travel_ams_expenditures[] = getDebitCreditSumValue($intl_travel_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_intl_travel_donor_expenditures[] = getDebitCreditSumValue($intl_travel_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d28 = array_sum($arr_intl_travel_eu_expenditures);
	$e28 = array_sum($arr_intl_travel_ams_expenditures);
	$f28 = array_sum($arr_intl_travel_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_intl_travel[] = getDebitCreditSumValue($intl_travel_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_intl_travel[] = getDebitCreditSumValue($intl_travel_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_intl_travel[] = getDebitCreditSumValue($intl_travel_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
		$intl_travel_tetd = array_sum($tetd_eu_intl_travel) + array_sum($tetd_ams_intl_travel) + array_sum($tetd_donor_intl_travel);
	}		
}
$uprv_sl_id = getSlIdByAccountCode("32100");
if ($uprv_sl_id!="")
{								  
	$c31_1 = getFAByComponentByBudget($uprv_sl_id,$component_id,$fa_start_year);
	$arr_uprv_eu_expenditures[] = getDebitCreditSumValue($uprv_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_uprv_ams_expenditures[] = getDebitCreditSumValue($uprv_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_uprv_donor_expenditures[] = getDebitCreditSumValue($uprv_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d31_1 = array_sum($arr_uprv_eu_expenditures);
	$e31_1 = array_sum($arr_uprv_ams_expenditures);
	$f31_1 = array_sum($arr_uprv_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_uprv[] = getDebitCreditSumValue($uprv_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_uprv[] = getDebitCreditSumValue($uprv_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_uprv[] = getDebitCreditSumValue($uprv_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
			
		$uprv_tetd = array_sum($tetd_eu_uprv) + array_sum($tetd_ams_uprv) + array_sum($tetd_donor_uprv);
	}		
}

$rprv_sl_id = getSlIdByAccountCode("32110");
if ($rprv_sl_id!="")
{								  
	$c32_1 = getFAByComponentByBudget($rprv_sl_id,$component_id,$fa_start_year);
	$arr_rprv_eu_expenditures[] = getDebitCreditSumValue($rprv_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_rprv_ams_expenditures[] = getDebitCreditSumValue($rprv_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_rprv_donor_expenditures[] = getDebitCreditSumValue($rprv_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d31_2 = array_sum($arr_rprv_eu_expenditures);
	$e31_2 = array_sum($arr_rprv_ams_expenditures);
	$f31_2 = array_sum($arr_rprv_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_rprv[] = getDebitCreditSumValue($rprv_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_rprv[] = getDebitCreditSumValue($rprv_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_rprv[] = getDebitCreditSumValue($rprv_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);					
		$rprv_tetd = array_sum($tetd_eu_rprv) + array_sum($tetd_ams_rprv) + array_sum($tetd_donor_rprv);
	}		
}

$c31 = $c31_1+$c31_2;
$d31 = $d31_1+$d31_2;
$e31 = $e31_1+$e31_2;
$f31 = $f31_1+$f31_2;
$uprv_rprc_tetd = $uprv_tetd+$rprv_tetd;

$ufce_sl_id = getSlIdByAccountCode("32200");
if ($ufce_sl_id!="")
{								  
	$c32_1 = getFAByComponentByBudget($ufce_sl_id,$component_id,$fa_start_year);
	$arr_ufce_eu_expenditures[] = getDebitCreditSumValue($ufce_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ufce_ams_expenditures[] = getDebitCreditSumValue($ufce_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ufce_donor_expenditures[] = getDebitCreditSumValue($ufce_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d32_1 = array_sum($arr_ufce_eu_expenditures);
	$e32_1 = array_sum($arr_ufce_ams_expenditures);
	$f32_1 = array_sum($arr_ufce_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_ufce[] = getDebitCreditSumValue($ufce_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_ufce[] = getDebitCreditSumValue($ufce_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_ufce[] = getDebitCreditSumValue($ufce_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$ufce_tetd = array_sum($tetd_eu_ufce) + array_sum($tetd_ams_ufce) + array_sum($tetd_donor_ufce);
	}		
}

$rfce_sl_id = getSlIdByAccountCode("32220");
if ($rfce_sl_id!="")
{								  
	$c32_2 = getFAByComponentByBudget($rfce_sl_id,$component_id,$fa_start_year);
		$arr_rfce_eu_expenditures[] = getDebitCreditSumValue($rfce_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$arr_rfce_ams_expenditures[] = getDebitCreditSumValue($rfce_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$arr_rfce_donor_expenditures[] = getDebitCreditSumValue($rfce_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d32_2 = array_sum($arr_rfce_eu_expenditures);
	$e32_2 = array_sum($arr_rfce_ams_expenditures);
	$f32_2 = array_sum($arr_rfce_donor_expenditures);
	
	if ($fa_start_month!=1)
	{
		$tetd_eu_rfce[] = getDebitCreditSumValue($rfce_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_rfce[] = getDebitCreditSumValue($rfce_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_rfce[] = getDebitCreditSumValue($rfce_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
		$rfce_tetd = array_sum($tetd_eu_rfce) + array_sum($tetd_ams_rfce) + array_sum($tetd_donor_rfce);
	}	
}
$uoe_sl_id = getSlIdByAccountCode("32300");
if ($uoe_sl_id!="")
{								  
	$c32_3 = getFAByComponentByBudget($uoe_sl_id,$component_id,$fa_start_year);
	$arr_uoe_eu_expenditures[] = getDebitCreditSumValue($uoe_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_uoe_ams_expenditures[] = getDebitCreditSumValue($uoe_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_uoe_donor_expenditures[] = getDebitCreditSumValue($uoe_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d32_3 = array_sum($arr_uoe_eu_expenditures);
	$e32_3 = array_sum($arr_uoe_ams_expenditures);
	$f32_3 = array_sum($arr_uoe_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_uoe[] = getDebitCreditSumValue($uoe_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_uoe[] = getDebitCreditSumValue($uoe_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_uoe[] = getDebitCreditSumValue($uoe_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$uoe_tetd = array_sum($tetd_eu_uoe) + array_sum($tetd_ams_uoe) + array_sum($tetd_donor_uoe);
	}		
}
$roe_sl_id = getSlIdByAccountCode("32330");
if ($roe_sl_id!="")
{								  
	$c32_4 = getFAByComponentByBudget($roe_sl_id,$component_id,$fa_start_year);
	$arr_roe_eu_expenditures[] = getDebitCreditSumValue($roe_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_roe_ams_expenditures[] = getDebitCreditSumValue($roe_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_roe_donor_expenditures[] = getDebitCreditSumValue($roe_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	$d32_4 = array_sum($arr_roe_eu_expenditures);
	$e32_4 = array_sum($arr_roe_ams_expenditures);
	$f32_4 = array_sum($arr_roe_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_roe[] = getDebitCreditSumValue($roe_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_roe[] = getDebitCreditSumValue($roe_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_roe[] = getDebitCreditSumValue($roe_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
	
		$roe_tetd = array_sum($tetd_eu_roe) + array_sum($tetd_ams_roe) + array_sum($tetd_donor_roe);
	}		
}

$c32 = $c32_1+$c32_2+$c32_3+$c32_4;
$d32 = $d32_1+$d32_2+$d32_3+$d32_4;
$e32 = $e32_1+$e32_2+$e32_3+$e32_4;
$f32 = $f32_1+$f32_2+$f32_3+$f32_4;
$ufce_rfce_uoe_roe_tetd = $ufce_tetd+$rfce_tetd+$uoe_tetd+$roe_tetd;

//
$alm_sl_id = getSlIdByAccountCode("32400");
if ($alm_sl_id!="")
{								  
	$c33 = getFAByComponentByBudget($alm_sl_id,$component_id,$fa_start_year);
	$arr_alm_eu_expenditures[] = getDebitCreditSumValue($alm_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_alm_ams_expenditures[] = getDebitCreditSumValue($alm_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_alm_donor_expenditures[] = getDebitCreditSumValue($alm_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d33 = array_sum($arr_alm_eu_expenditures);
	$e33 = array_sum($arr_alm_ams_expenditures);
	$f33 = array_sum($arr_alm_donor_expenditures);
	
	if ($fa_start_month!=1)
	{
		$tetd_eu_alm[] = getDebitCreditSumValue($alm_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_alm[] = getDebitCreditSumValue($alm_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_alm[] = getDebitCreditSumValue($alm_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$alm_tetd = array_sum($tetd_eu_alm) + array_sum($tetd_ams_alm) + array_sum($tetd_donor_alm);
	}	
}
$vc_sl_id = getSlIdByAccountCode("33100");
if ($vc_sl_id!="")
{		
	$c36 = getFAByComponentByBudget($vc_sl_id,$component_id,$fa_start_year);
	$arr_vc_eu_expenditures[] = getDebitCreditSumValue($vc_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_vc_ams_expenditures[] = getDebitCreditSumValue($vc_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_vc_donor_expenditures[] = getDebitCreditSumValue($vc_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d36 = array_sum($arr_vc_eu_expenditures);
	$e36 = array_sum($arr_vc_ams_expenditures);
	$f36 = array_sum($arr_vc_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_vc[] = getDebitCreditSumValue($vc_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_vc[] = getDebitCreditSumValue($vc_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_vc[] = getDebitCreditSumValue($vc_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$vc_tetd = array_sum($tetd_eu_vc) + array_sum($tetd_ams_vc) + array_sum($tetd_donor_vc);
	}		
}
$mce_sl_id = getSlIdByAccountCode("33300");
if ($mce_sl_id!="")
{								  
	$c37 = getFAByComponentByBudget($mce_sl_id,$component_id,$fa_start_year);
	$arr_mce_eu_expenditures[] = getDebitCreditSumValue($mce_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_mce_ams_expenditures[] = getDebitCreditSumValue($mce_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_mce_donor_expenditures[] = getDebitCreditSumValue($mce_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d37 = array_sum($arr_mce_eu_expenditures);
	$e37 = array_sum($arr_mce_ams_expenditures);
	$f37 = array_sum($arr_mce_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_mce[] = getDebitCreditSumValue($mce_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_mce[] = getDebitCreditSumValue($mce_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_mce[] = getDebitCreditSumValue($mce_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
		$mce_tetd = array_sum($tetd_eu_mce) + array_sum($tetd_ams_mce) + array_sum($tetd_donor_mce);
	}		
}
$orent_sl_id = getSlIdByAccountCode("33500");
if ($orent_sl_id!="")
{								  
	$c38 = getFAByComponentByBudget($orent_sl_id,$component_id,$fa_start_year);
	$arr_orent_eu_expenditures[] = getDebitCreditSumValue($orent_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_orent_ams_expenditures[] = getDebitCreditSumValue($orent_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_orent_donor_expenditures[] = getDebitCreditSumValue($orent_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d38 = array_sum($arr_orent_eu_expenditures);
	$e38 = array_sum($arr_orent_ams_expenditures);
	$f38 = array_sum($arr_orent_donor_expenditures);
	
	if ($fa_start_month!=1)
	{
		$tetd_eu_orent[] = getDebitCreditSumValue($orent_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_orent[] = getDebitCreditSumValue($orent_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_orent[] = getDebitCreditSumValue($orent_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
			
		$orent_tetd = array_sum($tetd_eu_orent) + array_sum($tetd_ams_orent) + array_sum($tetd_donor_orent);
	}	
}
$cos_sl_id = getSlIdByAccountCode("33200");
if ($cos_sl_id!="")
{								  
	$c39 = getFAByComponentByBudget($cos_sl_id,$component_id,$fa_start_year);
	$arr_cos_eu_expenditures[] = getDebitCreditSumValue($cos_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_cos_ams_expenditures[] = getDebitCreditSumValue($cos_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_cos_donor_expenditures[] = getDebitCreditSumValue($cos_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d39 = array_sum($arr_cos_eu_expenditures);
	$e39 = array_sum($arr_cos_ams_expenditures);
	$f39 = array_sum($arr_cos_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_cos[] = getDebitCreditSumValue($cos_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_cos[] = getDebitCreditSumValue($cos_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_cos[] = getDebitCreditSumValue($cos_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$cos_tetd = array_sum($tetd_eu_cos) + array_sum($tetd_ams_cos) + array_sum($tetd_donor_cos);
	}		
}
$osoc_sl_id = getSlIdByAccountCode("33400");
if ($osoc_sl_id!="")
{								  
	$c40 = getFAByComponentByBudget($osoc_sl_id,$component_id,$fa_start_year);
	$arr_osoc_eu_expenditures[] = getDebitCreditSumValue($osoc_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_osoc_ams_expenditures[] = getDebitCreditSumValue($osoc_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_osoc_donor_expenditures[] = getDebitCreditSumValue($osoc_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d40 = array_sum($arr_osoc_eu_expenditures);
	$e40 = array_sum($arr_osoc_ams_expenditures);
	$f40 = array_sum($arr_osoc_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_osoc[] = getDebitCreditSumValue($osoc_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_osoc[] = getDebitCreditSumValue($osoc_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_osoc[] = getDebitCreditSumValue($osoc_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$osoc_tetd = array_sum($tetd_eu_osoc) + array_sum($tetd_ams_osoc) + array_sum($tetd_donor_osoc);
	}		
}

$gp_sl_id = getSlIdByAccountCode("34101");
if ($gp_sl_id!="")
{								  
	$c43 = getFAByComponentByBudget($gp_sl_id,$component_id,$fa_start_year);
	$arr_gp_eu_expenditures[] = getDebitCreditSumValue($gp_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_gp_ams_expenditures[] = getDebitCreditSumValue($gp_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_gp_donor_expenditures[] = getDebitCreditSumValue($gp_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d43 = array_sum($arr_gp_eu_expenditures);
	$e43 = array_sum($arr_gp_ams_expenditures);
	$f43 = array_sum($arr_gp_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_gp[] = getDebitCreditSumValue($gp_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_gp[] = getDebitCreditSumValue($gp_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_gp[] = getDebitCreditSumValue($gp_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$gp_tetd = array_sum($tetd_eu_gp) + array_sum($tetd_ams_gp) + array_sum($tetd_donor_gp);
	}		
}
$abm_sl_id = getSlIdByAccountCode("34102");
if ($abm_sl_id!="")
{								  
	$c44 = getFAByComponentByBudget($abm_sl_id,$component_id,$fa_start_year);
	$arr_abm_eu_expenditures[] = getDebitCreditSumValue($abm_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_abm_ams_expenditures[] = getDebitCreditSumValue($abm_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_abm_donor_expenditures[] = getDebitCreditSumValue($abm_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	
	$d44 = array_sum($arr_abm_eu_expenditures);
	$e44 = array_sum($arr_abm_ams_expenditures);
	$f44 = array_sum($arr_abm_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_abm[] = getDebitCreditSumValue($abm_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_abm[] =getDebitCreditSumValue($abm_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_abm[] = getDebitCreditSumValue($abm_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);				
		$abm_tetd = array_sum($tetd_eu_abm) + array_sum($tetd_ams_abm) + array_sum($tetd_donor_abm);
	}		
}
$gdvep_sl_id = getSlIdByAccountCode("34103");
if ($gdvep_sl_id!="")
{								  
	$c45 = getFAByComponentByBudget($gdvep_sl_id,$component_id,$fa_start_year);
	$arr_gdvep_eu_expenditures[] = getDebitCreditSumValue($gdvep_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_gdvep_ams_expenditures[] = getDebitCreditSumValue($gdvep_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_gdvep_donor_expenditures[] = getDebitCreditSumValue($gdvep_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d45 = array_sum($arr_gdvep_eu_expenditures);
	$e45 = array_sum($arr_gdvep_ams_expenditures);
	$f45 = array_sum($arr_gdvep_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_gdvep[] = getDebitCreditSumValue($gdvep_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_gdvep[] = getDebitCreditSumValue($gdvep_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_gdvep[] = getDebitCreditSumValue($gdvep_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
			
		$gdvep_tetd = array_sum($tetd_eu_gdvep) + array_sum($tetd_ams_gdvep) + array_sum($tetd_donor_gdvep);
	}		
}
$peav_sl_id = getSlIdByAccountCode("34104");
if ($peav_sl_id!="")
{								  
	$c46 = getFAByComponentByBudget($peav_sl_id,$component_id,$fa_start_year);
	$arr_peav_eu_expenditures[] = getDebitCreditSumValue($peav_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_peav_ams_expenditures[] = getDebitCreditSumValue($peav_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_peav_donor_expenditures[] = getDebitCreditSumValue($peav_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d46 = array_sum($arr_peav_eu_expenditures);
	$e46 = array_sum($arr_peav_ams_expenditures);
	$f46 = array_sum($arr_peav_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_peav[] = getDebitCreditSumValue($peav_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_peav[] = getDebitCreditSumValue($peav_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_peav[] = getDebitCreditSumValue($peav_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
		$peav_tetd = array_sum($tetd_eu_peav) + array_sum($tetd_ams_peav) + array_sum($tetd_donor_peav);
	}		
}
$sr_sl_id = getSlIdByAccountCode("34105");
if ($sr_sl_id!="")
{								  
	$c47 = getFAByComponentByBudget($sr_sl_id,$component_id,$fa_start_year);
	$arr_sr_eu_expenditures[] = getDebitCreditSumValue($sr_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sr_ams_expenditures[] = getDebitCreditSumValue($sr_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sr_donor_expenditures[] = getDebitCreditSumValue($sr_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d47 = array_sum($arr_sr_eu_expenditures);
	$e47 = array_sum($arr_sr_ams_expenditures);
	$f47 = array_sum($arr_sr_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_sr[] = getDebitCreditSumValue($sr_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_sr[] = getDebitCreditSumValue($sr_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_sr[] = getDebitCreditSumValue($sr_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$sr_tetd = array_sum($tetd_eu_sr) + array_sum($tetd_ams_sr) + array_sum($tetd_donor_sr);
	}		
}
$sup_stud_sl_id = getSlIdByAccountCode("34106");
if ($sup_stud_sl_id!="")
{								  
	$c48 = getFAByComponentByBudget($sup_stud_sl_id,$component_id,$fa_start_year);
	$arr_sup_stud_eu_expenditures[] = getDebitCreditSumValue($sup_stud_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sup_stud_ams_expenditures[] = getDebitCreditSumValue($sup_stud_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sup_stud_donor_expenditures[] = getDebitCreditSumValue($sup_stud_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d48 = array_sum($arr_sup_stud_eu_expenditures);
	$e48 = array_sum($arr_sup_stud_ams_expenditures);
	$f48 = array_sum($arr_sup_stud_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_sup_stud[] = getDebitCreditSumValue($sup_stud_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_sup_stud[] = getDebitCreditSumValue($sup_stud_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_sup_stud[] = getDebitCreditSumValue($sup_stud_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$sup_stud_tetd = array_sum($tetd_eu_sup_stud) + array_sum($tetd_ams_sup_stud) + array_sum($tetd_donor_sup_stud);
	}		
}
$lste_sl_id = getSlIdByAccountCode("34107");
if ($lste_sl_id!="")
{								  
	$c49 = getFAByComponentByBudget($lste_sl_id,$component_id,$fa_start_year);
	$arr_lste_eu_expenditures[] = getDebitCreditSumValue($lste_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_lste_ams_expenditures[] = getDebitCreditSumValue($lste_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_lste_donor_expenditures[] = getDebitCreditSumValue($lste_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d49 = array_sum($arr_lste_eu_expenditures);
	$e49 = array_sum($arr_lste_ams_expenditures);
	$f49 = array_sum($arr_lste_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_lste[] = getDebitCreditSumValue($lste_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_lste[] = getDebitCreditSumValue($lste_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_lste[] = getDebitCreditSumValue($lste_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$lste_tetd = array_sum($tetd_eu_lste) + array_sum($tetd_ams_lste) + array_sum($tetd_donor_lste);
	}		
}
$ilss_fp_sl_id = getSlIdByAccountCode("34108");
if ($ilss_fp_sl_id!="")
{								  
	$c50 = getFAByComponentByBudget($ilss_fp_sl_id,$component_id,$fa_start_year);
	$arr_ilss_fp_eu_expenditures[] = getDebitCreditSumValue($ilss_fp_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ilss_fp_ams_expenditures[] = getDebitCreditSumValue($ilss_fp_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ilss_fp_donor_expenditures[] = getDebitCreditSumValue($ilss_fp_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d50 = array_sum($arr_ilss_fp_eu_expenditures);
	$e50 = array_sum($arr_ilss_fp_ams_expenditures);
	$f50 = array_sum($arr_ilss_fp_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_ilss_fp[] = getDebitCreditSumValue($ilss_fp_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_ilss_fp[] = getDebitCreditSumValue($ilss_fp_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_ilss_fp[] = getDebitCreditSumValue($ilss_fp_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$ilss_fp_tetd = array_sum($tetd_eu_ilss_fp) + array_sum($tetd_ams_ilss_fp) + array_sum($tetd_donor_ilss_fp);
	}		
}
$corts_2wks_sl_id = getSlIdByAccountCode("34109");
if ($corts_2wks_sl_id!="")
{								  
	$c51 = getFAByComponentByBudget($corts_2wks_sl_id,$component_id,$fa_start_year);
	$arr_corts_2wks_eu_expenditures[] = getDebitCreditSumValue($corts_2wks_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_corts_2wks_ams_expenditures[] = getDebitCreditSumValue($corts_2wks_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_corts_2wks_donor_expenditures[] = getDebitCreditSumValue($corts_2wks_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d51 = array_sum($arr_corts_2wks_eu_expenditures);
	$e51 = array_sum($arr_corts_2wks_ams_expenditures);
	$f51 = array_sum($arr_corts_2wks_donor_expenditures);
	
	if ($fa_start_month!=1)
	{
		$tetd_eu_corts_2wks[] = getDebitCreditSumValue($corts_2wks_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_corts_2wks[] = getDebitCreditSumValue($corts_2wks_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_corts_2wks[] = getDebitCreditSumValue($corts_2wks_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$corts_2wks_tetd = array_sum($tetd_eu_corts_2wks) + array_sum($tetd_ams_corts_2wks) + array_sum($tetd_donor_corts_2wks);
	}	
}
$corts_1wk_sl_id = getSlIdByAccountCode("34110");
if ($corts_1wk_sl_id!="")
{								  
	$c52 = getFAByComponentByBudget($corts_1wk_sl_id,$component_id,$fa_start_year);
	$arr_corts_1wk_eu_expenditures[] = getDebitCreditSumValue($corts_1wk_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_corts_1wk_ams_expenditures[] = getDebitCreditSumValue($corts_1wk_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_corts_1wk_donor_expenditures[] = getDebitCreditSumValue($corts_1wk_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d52 = array_sum($arr_corts_1wk_eu_expenditures);
	$e52 = array_sum($arr_corts_1wk_ams_expenditures);
	$f52 = array_sum($arr_corts_1wk_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_corts_1wk[] = getDebitCreditSumValue($corts_1wk_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_corts_1wk[] = getDebitCreditSumValue($corts_1wk_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_corts_1wk[] = getDebitCreditSumValue($corts_1wk_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);					
		$corts_1wk_tetd = array_sum($tetd_eu_corts_1wk) + array_sum($tetd_ams_corts_1wk) + array_sum($tetd_donor_corts_1wk);
	}		
}
$cosrts_sl_id = getSlIdByAccountCode("34111");
if ($cosrts_sl_id!="")
{								  
	$c53 = getFAByComponentByBudget($cosrts_sl_id,$component_id,$fa_start_year);
	$arr_cosrts_eu_expenditures[] = getDebitCreditSumValue($cosrts_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_cosrts_ams_expenditures[] = getDebitCreditSumValue($cosrts_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_cosrts_donor_expenditures[] = getDebitCreditSumValue($cosrts_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	$d53 = array_sum($arr_cosrts_eu_expenditures);
	$e53 = array_sum($arr_cosrts_ams_expenditures);
	$f53 = array_sum($arr_cosrts_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_cosrts[] =getDebitCreditSumValue($cosrts_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_cosrts[] = getDebitCreditSumValue($cosrts_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_cosrts[] = getDebitCreditSumValue($cosrts_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$cosrts_tetd = array_sum($tetd_eu_cosrts) + array_sum($tetd_ams_cosrts) + array_sum($tetd_donor_cosrts);
	}		
}
$cows_sl_id = getSlIdByAccountCode("34112");
if ($cows_sl_id!="")
{								  
	$c54 = getFAByComponentByBudget($cows_sl_id,$component_id,$fa_start_year);
	$arr_cows_eu_expenditures[] = getDebitCreditSumValue($cows_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_cows_ams_expenditures[] = getDebitCreditSumValue($cows_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_cows_donor_expenditures[] = getDebitCreditSumValue($cows_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d54 = array_sum($arr_cows_eu_expenditures);
	$e54 = array_sum($arr_cows_ams_expenditures);
	$f54 = array_sum($arr_cows_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_cows[] = getDebitCreditSumValue($cows_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_cows[] = getDebitCreditSumValue($cows_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_cows[] = getDebitCreditSumValue($cows_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$cows_tetd = array_sum($tetd_eu_cows) + array_sum($tetd_ams_cows) + array_sum($tetd_donor_cows);
	}		
}
$sthe_sl_id = getSlIdByAccountCode("34113");
if ($sthe_sl_id!="")
{								  
	$c55 = getFAByComponentByBudget($sthe_sl_id,$component_id,$fa_start_year);
	$arr_sthe_eu_expenditures[] = getDebitCreditSumValue($sthe_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sthe_ams_expenditures[] = getDebitCreditSumValue($sthe_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_sthe_donor_expenditures[] = getDebitCreditSumValue($sthe_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d55 = array_sum($arr_sthe_eu_expenditures);
	$e55 = array_sum($arr_sthe_ams_expenditures);
	$f55 = array_sum($arr_sthe_donor_expenditures);
	
	if ($fa_start_month!=1)
	{
		$tetd_eu_sthe[] = getDebitCreditSumValue($sthe_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_sthe[] = getDebitCreditSumValue($sthe_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_sthe[] = getDebitCreditSumValue($sthe_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);				
		$sthe_tetd = array_sum($tetd_eu_sthe) + array_sum($tetd_ams_sthe) + array_sum($tetd_donor_sthe);
	}
}
$ateac_sl_id = getSlIdByAccountCode("34114");
if ($ateac_sl_id!="")
{								  
	$c56 = getFAByComponentByBudget($ateac_sl_id,$component_id,$fa_start_year);
	$arr_ateac_eu_expenditures[] = getDebitCreditSumValue($ateac_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ateac_ams_expenditures[] = getDebitCreditSumValue($ateac_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ateac_donor_expenditures[] = getDebitCreditSumValue($ateac_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
		
	$d56 = array_sum($arr_ateac_eu_expenditures);
	$e56 = array_sum($arr_ateac_ams_expenditures);
	$f56 = array_sum($arr_ateac_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_ateac[] = getDebitCreditSumValue($ateac_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_ateac[] = getDebitCreditSumValue($ateac_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_ateac[] = getDebitCreditSumValue($ateac_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$ateac_tetd = array_sum($tetd_eu_ateac) + array_sum($tetd_ams_ateac) + array_sum($tetd_donor_ateac);
	}		
}
$evst_sl_id = getSlIdByAccountCode("34115");
if ($evst_sl_id!="")
{								  
	$c57 = getFAByComponentByBudget($evst_sl_id,$component_id,$fa_start_year);
	$arr_evst_eu_expenditures[] = getDebitCreditSumValue($evst_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_evst_ams_expenditures[] = getDebitCreditSumValue($evst_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_evst_donor_expenditures[] = getDebitCreditSumValue($evst_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d57 = array_sum($arr_evst_eu_expenditures);
	$e57 = array_sum($arr_evst_ams_expenditures);
	$f57 = array_sum($arr_evst_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_evst[] = getDebitCreditSumValue($evst_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_evst[] = getDebitCreditSumValue($evst_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_evst[] = getDebitCreditSumValue($evst_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);					
		$evst_tetd = array_sum($tetd_eu_evst) + array_sum($tetd_ams_evst) + array_sum($tetd_donor_evst);
	}		
}
$ec_sl_id = getSlIdByAccountCode("34116");
if ($ec_sl_id!="")
{								  
	$c58 = getFAByComponentByBudget($ec_sl_id,$component_id,$fa_start_year);
	$arr_ec_eu_expenditures[] = getDebitCreditSumValue($ec_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ec_ams_expenditures[] = getDebitCreditSumValue($ec_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_ec_donor_expenditures[] = getDebitCreditSumValue($ec_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d58 = array_sum($arr_ec_eu_expenditures);
	$e58 = array_sum($arr_ec_ams_expenditures);
	$f58 = array_sum($arr_ec_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_ec[] = getDebitCreditSumValue($ec_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_ec[] = getDebitCreditSumValue($ec_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_ec[] = getDebitCreditSumValue($ec_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);		
		$ec_tetd = array_sum($tetd_eu_ec) + array_sum($tetd_ams_ec) + array_sum($tetd_donor_ec);
	}		
}
$psctm_sl_id = getSlIdByAccountCode("34117");
if ($psctm_sl_id!="")
{								  
	$c59 = getFAByComponentByBudget($psctm_sl_id,$component_id,$fa_start_year);
	$arr_psctm_eu_expenditures[] = getDebitCreditSumValue($psctm_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_psctm_ams_expenditures[] = getDebitCreditSumValue($psctm_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_psctm_donor_expenditures[] = getDebitCreditSumValue($psctm_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d59 = array_sum($arr_psctm_eu_expenditures);
	$e59 = array_sum($arr_psctm_ams_expenditures);
	$f59 = array_sum($arr_psctm_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_psctm[] = getDebitCreditSumValue($psctm_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_psctm[] = getDebitCreditSumValue($psctm_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_psctm[] = getDebitCreditSumValue($psctm_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);				
		$psctm_tetd = array_sum($tetd_eu_psctm) + array_sum($tetd_ams_psctm) + array_sum($tetd_donor_psctm);
	}		
}
$audit_cost_sl_id = getSlIdByAccountCode("35100");
if ($audit_cost_sl_id!="")
{								  
	$c62 = getFAByComponentByBudget($audit_cost_sl_id,$component_id,$fa_start_year);
	$arr_audit_cost_eu_expenditures[] = getDebitCreditSumValue($audit_cost_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_audit_cost_ams_expenditures[] = getDebitCreditSumValue($audit_cost_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_audit_cost_donor_expenditures[] =getDebitCreditSumValue($audit_cost_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	

	$d62 = array_sum($arr_audit_cost_eu_expenditures);
	$e62 = array_sum($arr_audit_cost_ams_expenditures);
	$f62 = array_sum($arr_audit_cost_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_audit_cost[] = getDebitCreditSumValue($audit_cost_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_audit_cost[] = getDebitCreditSumValue($audit_cost_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_audit_cost[] =getDebitCreditSumValue($audit_cost_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
			
		$audit_cost_tetd = array_sum($tetd_eu_audit_cost) + array_sum($tetd_ams_audit_cost) + array_sum($tetd_donor_audit_cost);
	}		
}
$rt_cost_sl_id = getSlIdByAccountCode("35200");
if ($rt_cost_sl_id!="")
{								  
	$c63 = getFAByComponentByBudget($rt_cost_sl_id,$component_id,$fa_start_year);
	$arr_rt_cost_eu_expenditures[] = getDebitCreditSumValue($rt_cost_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_rt_cost_ams_expenditures[] = getDebitCreditSumValue($rt_cost_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_rt_cost_donor_expenditures[] = getDebitCreditSumValue($rt_cost_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);

	$d63 = array_sum($arr_rt_cost_eu_expenditures);
	$e63 = array_sum($arr_rt_cost_ams_expenditures);
	$f63 = array_sum($arr_rt_cost_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_rt_cost[] = getDebitCreditSumValue($rt_cost_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_rt_cost[] = getDebitCreditSumValue($rt_cost_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_rt_cost[] = getDebitCreditSumValue($rt_cost_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);			
		$rt_cost_tetd = array_sum($tetd_eu_rt_cost) + array_sum($tetd_ams_rt_cost) + array_sum($tetd_donor_rt_cost);
	}		
}
$con_sl_id = getSlIdByAccountCode("36200");
if ($con_sl_id!="")
{								  
	$c67 = getFAByComponentByBudget($con_sl_id,$component_id,$fa_start_year);
	$arr_con_eu_expenditures[] = getDebitCreditSumValue($con_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_con_ams_expenditures[] = getDebitCreditSumValue($con_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	$arr_con_donor_expenditures[] = getDebitCreditSumValue($con_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
	
	$d67 = array_sum($arr_con_eu_expenditures);
	$e67 = array_sum($arr_con_ams_expenditures);
	$f67 = array_sum($arr_con_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_con_sl[] = getDebitCreditSumValue($con_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_con_sl[] = getDebitCreditSumValue($con_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_con_sl[] = getDebitCreditSumValue($con_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);					
		$con_sl_tetd = array_sum($tetd_eu_con_sl) + array_sum($tetd_ams_con_sl) + array_sum($tetd_donor_con_sl);
	}		
}
$admin_cost_sl_id = getSlIdByAccountCode("36100");
if ($admin_cost_sl_id!="")
{								  
	$c70 = getFAByComponentByBudget($admin_cost_sl_id,$component_id,$fa_start_year);
$arr_admin_cost_eu_expenditures[] = getDebitCreditSumValue($admin_cost_sl_id,$fa_currency_code,$start_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
$arr_admin_cost_ams_expenditures[] = getDebitCreditSumValue($admin_cost_sl_id,$fa_currency_code,$start_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
$arr_admin_cost_donor_expenditures[] = getDebitCreditSumValue($admin_cost_sl_id,$fa_currency_code,$start_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
	
	$d70 = array_sum($arr_admin_cost_eu_expenditures);
	$e70 = array_sum($arr_admin_cost_ams_expenditures);
	$f70 = array_sum($arr_admin_cost_donor_expenditures);

	if ($fa_start_month!=1)
	{
		$tetd_eu_admin_cost[] = getDebitCreditSumValue($admin_cost_sl_id,$fa_currency_code,$reference_date,$end_date,$eu_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_ams_admin_cost[] = getDebitCreditSumValue($admin_cost_sl_id,$fa_currency_code,$reference_date,$end_date,$ams_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);
		$tetd_donor_admin_cost[] = getDebitCreditSumValue($admin_cost_sl_id,$fa_currency_code,$reference_date,$end_date,$donor_budget_name,"",$component_id,"","","","","","","",$consolidate_reports,$use_obligated_values);	
		$admin_cost_tetd = array_sum($tetd_eu_admin_cost) + array_sum($tetd_ams_admin_cost) + array_sum($tetd_donor_admin_cost);
	}		
}
?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br />
					<?php echo upperCase(getCompanyName()); ?>
					<br />
					<?php echo upperCase(getCompanyAddress()); ?><br />
					For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?>
					<br>
					ACB-<?php echo $fa_currency_name; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
				  <td scope='col' valign=top>
				  <!-- Start of Inner Table -->
				  <table width="100%" border="1" cellspacing="0" cellpadding="2" bordercolor="#CCCCCC">
                    <thead>
                      <tr>
                        <th width="48%" scope="col">Budget Line </th>
                        <th width="5%" scope="col"><?php echo $budget_identification; ?> Budget</th>
                        <th width="8%" scope="col">Total EU Expenditures</th>
                        <th width="8%" scope="col">Total AMS Expenditures </th>
                        <th width="8%" scope="col">Total DONOR Expenditures </th>
                        <th width="8%" scope="col">Total Expenditures to date</th>
                        <th width="6%" scope="col">Fund Balance <?php echo $budget_identification; ?> Budget - <?php echo "Component ".$component_id; ?></th>
                        <th width="9%" scope="col">% of Utilization</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td scope="col"><strong>1 Human Resource </strong></td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">1.1 Salaries (gross amounts, local staff)</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.1 Technical</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.1.1 Director - ASEAN recruited staff (4AD)</td>
                        <td scope="col" align="right"><?php
								$c_11 = numberFormat($c11);
								echo $c_11;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_11 = numberFormat($d11);
								echo $d_11;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_11 = numberFormat($e11);
								echo $e_11;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_11 = numberFormat($f11);
								echo $f_11;
							?>                        </td>
                        <td scope="col" align="right">
							<?php
								if ($fa_start_month!=1) { $g11 = $dars_tetd; }
								else { $g11 = $d11+$e11+$f11; }		
								$g_11 = numberFormat($g11);
								echo $g_11;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h11 = $c11-$g11;
								$h_11 = numberFormat($h11);
								echo $h_11;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c11!=0)
								{
									$i11 = ($g11/$c11)*100;
								}
								else
								{
									$i11 = 0;
								}
								$i_11 = numberFormat($i11);
								echo $i_11;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.1.2 Seconded Staff</td>
                        <td scope="col" align="right"><?php
								$c_12 = numberFormat($c12);
								echo $c_12;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_12 = numberFormat($d12);
								echo $d_12;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_12 = numberFormat($e12);
								echo $e_12;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_12 = numberFormat($f12);
								echo $f_12;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g12 = $ss_tetd; }
								else { $g12 = $d12+$e12+$f12; }		
								$g_12 = numberFormat($g12);
								echo $g_12;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h12 = $c12-$g12;
								$h_12 = numberFormat($h12);
								echo $h_12;
							?></td>
                        <td scope="col" align="right"><?php
								if ($c12!=0)
								{
									$i12 = ($g12/$c12)*100;
								}
								else
								{
									$i12 = 0;
								}
								$i_12 = numberFormat($i12);
								echo $i_12;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.1.3 Specialist support staff (Technical)</td>
                        <td scope="col" align="right"><?php
								$c_13 = numberFormat($c13);
								echo $c_13;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_13 = numberFormat($d13);
								echo $d_13;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_13 = numberFormat($e13);
								echo $e_13;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_13 = numberFormat($f13);
								echo $f_13;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g13 = $sss_tetd; }
								else { $g13 = $d13+$e13+$f13; }		
								$g_13 = numberFormat($g13);
								echo $g_13;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h13 = $c13-$g13;
								$h_13 = numberFormat($h13);
								echo $h_13;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c13!=0)
								{
									$i13 = ($g13/$c13)*100;
								}
								else
								{
									$i13 = 0;
								}
								$i_13 = numberFormat($i13);
								echo $i_13;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.2 Administrative/ support staff</td>
                        <td scope="col" align="right"><?php
								$c_14 = numberFormat($c14);
								echo $c_14;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_14 = numberFormat($d14);
								echo $d_14;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_14 = numberFormat($e14);
								echo $e_14;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_14 = numberFormat($f14);
								echo $f_14;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g14 = $ed_fa_oed_sss_gss_tetd ; }
								else { $g14 = $d14+$e14+$f14; }							
								$g_14 = numberFormat($g14);
								echo $g_14;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h14 = $c14-$g14;
								$h_14 = numberFormat($h14);
								echo $h_14;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c14!=0)
								{
									$i14 = ($g14/$c14)*100;
								}
								else
								{
									$i14 = 0;
								}
								$i_14 = numberFormat($i14);
								echo $i_14;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.2.1 Executive Director - ASEAN recruited staff</td>
                        <td scope="col" align="right"><?php
								$c_15 = numberFormat($c15);
								echo $c_15;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_15 = numberFormat($d15);
								echo $d_15;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_15 = numberFormat($e15);
								echo $e_15;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_15 = numberFormat($f15);
								echo $f_15;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g15 = $ed_ars_tetd; }
								else { $g15 = $d15+$e15+$f15; }		
								$g_15 = numberFormat($g15);
								echo $g_15;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h15 = $c15-$g15;
								$h_15 = numberFormat($h15);
								echo $h_15;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c15!=0)
								{
									$i15 = ($g15/$c15)*100;
								}
								else
								{
									$i15 = 0;
								}
								$i_15 = numberFormat($i15);
								echo $i_15;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.2.2  Finance Administration - ASEAN recruited staff</td>
                        <td scope="col" align="right"><?php
								$c_16 = numberFormat($c16);
								echo $c_16;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_16 = numberFormat($d16);
								echo $d_16;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_16 = numberFormat($e16);
								echo $e_16;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_16 = numberFormat($f16);
								echo $f_16;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g16 = $fa_ars_tetd; }
								else { $g16 = $d16+$e16+$f16; }						
								$g_16 = numberFormat($g16);
								echo $g_16;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h16 = $c16-$g16;
								$h_16 = numberFormat($h16);
								echo $h_16;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c16!=0)
								{
									$i16 = ($g16/$c16)*100;
								}
								else
								{
									$i16 = 0;
								}
								$i_16 = numberFormat($i16);
								echo $i_16;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.2.5 Office of the Executive Director - Public Awareness</td>
                        <td scope="col" align="right"><?php
								$c_17 = numberFormat($c17);
								echo $c_17;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_17 = numberFormat($d17);
								echo $d_17;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_17 = numberFormat($e17);
								echo $e_17;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_17 = numberFormat($f17);
								echo $f_17;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g17 = $oed_pa_tetd; }
								else { $g17 = $d17+$e17+$f17; }								
								$g_17 = numberFormat($g17);
								echo $g_17;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h17 = $c17-$g17;
								$h_17 = numberFormat($h17);
								echo $h_17;
							?></td>
                        <td scope="col" align="right"><?php
								if ($c17!=0)
								{
									$i17 = ($g17/$c17)*100;
								}
								else
								{
									$i17 = 0;
								}
								$i_17 = numberFormat($i17);
								echo $i_17;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.2.3 Specialist support staff (Admin)</td>
                        <td scope="col" align="right"><?php
								$c_18 = numberFormat($c18);
								echo $c_18;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_18 = numberFormat($d18);
								echo $d_18;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_18 = numberFormat($e18);
								echo $e_18;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_18 = numberFormat($f18);
								echo $f_18;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g18 = $sss_admin_tetd; }
								else { $g18 = $d18+$e18+$f18; }								
								$g_18 = numberFormat($g18);
								echo $g_18;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h18 = $c18-$g18;
								$h_18 = numberFormat($h18);
								echo $h_18;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c18!=0)
								{
									$i18 = ($g18/$c18)*100;
								}
								else
								{
									$i18 = 0;
								}
								$i_18 = numberFormat($i18);
								echo $i_18;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.2.4 General support staff</td>
                        <td scope="col" align="right"><?php
								$c_19 = numberFormat($c19);
								echo $c_19;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_19 = numberFormat($d19);
								echo $d_19;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_19 = numberFormat($e19);
								echo $e_19;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_19 = numberFormat($f19);
								echo $f_19;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g19 = $gss_tetd; }
								else { $g19 = $d19+$e19+$f19; }	
								$g_19 = numberFormat($g19);
								echo $g_19;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h19 = $c19-$g19;
								$h_19 = numberFormat($h19);
								echo $h_19;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c19!=0)
								{
									$i19 = ($g19/$c19)*100;
								}
								else
								{
									$i19 = 0;
								}
								$i_19 = numberFormat($i19);
								echo $i_19;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1.3 Staff Allowances</td>
                        <td scope="col" align="right"><?php
								$c_20 = numberFormat($c20);
								echo $c_20;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_20 = numberFormat($d20);
								echo $d_20;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_20 = numberFormat($e20);
								echo $e_20;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_20 = numberFormat($f20);
								echo $f_20;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g20 = $sa_tetd; }
								else { $g20 = $d20+$e20+$f20; }							
								$g_20 = numberFormat($g20);
								echo $g_20;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h20 = $c20-$g20;
								$h_20 = numberFormat($h20);
								echo $h_20;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c20!=0)
								{
									$i20 = ($g20/$c20)*100;
								}
								else
								{
									$i20 = 0;
								}
								$i_20 = numberFormat($i20);
								echo $i_20;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">1.2 Salaries (gross amounts, expat/int. staff) ETAT</td>
                        <td scope="col" align="right"><?php
								$c_21 = numberFormat($c21);
								echo $c_21;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_21 = numberFormat($d21);
								echo $d_21;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_21 = numberFormat($e21);
								echo $e_21;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_21 = numberFormat($f21);
								echo $f_21;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g20 = $seis_tetd; }
								else { $g21 = $d21+$e21+$f21; }							
								$g_21 = numberFormat($g21);
								echo $g_21;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h21 = $c21-$g21;
								$h_21 = numberFormat($h21);
								echo $h_21;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c21!=0)
								{
									$i21 = ($g21/$c21)*100;
								}
								else
								{
									$i21 = 0;
								}
								$i_21 = numberFormat($i21);
								echo $i_21;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">1.3 Per diems for missions/travel</td>
                        <td scope="col" align="right"><?php
								$c_22 = numberFormat($c22);
								echo $c_22;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_22 = numberFormat($d22);
								echo $d_22;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_22 = numberFormat($e22);
								echo $e_22;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_22 = numberFormat($f22);
								echo $f_22;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$g22 = $d22+$e22+$f22;
								$g_22 = numberFormat($g22);
								echo $g_22;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h22 = $c22-$g22;
								$h_22 = numberFormat($h22);
								echo $h_22;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c22!=0)
								{
									$i22 = ($g22/$c22)*100;
								}
								else
								{
									$i22 = 0;
								}
								$i_22 = numberFormat($i22);
								echo $i_22;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.4.1 Abroad (project staff)</td>
                        <td scope="col" align="right"><?php
								$c_23 = numberFormat($c23);
								echo $c_23;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_23 = numberFormat($d23);
								echo $d_23;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_23 = numberFormat($e23);
								echo $e_23;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_23 = numberFormat($f23);
								echo $f_23;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g23 = $aps_tetd; }
								else { $g23 = $d23+$e23+$f23; }							
								$g_23 = numberFormat($g23);
								echo $g_23;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h23 = $c23-$g23;
								$h_23 = numberFormat($h23);
								echo $h_23;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c23!=0)
								{
									$i23 = ($g23/$c23)*100;
								}
								else
								{
									$i23 = 0;
								}
								$i_23 = numberFormat($i23);
								echo $i_23;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.4.2 Local ASEAN countries (project staff)</td>
                        <td scope="col" align="right"><?php
								$c_24 = numberFormat($c24);
								echo $c_24;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_24 = numberFormat($d24);
								echo $d_24;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_24 = numberFormat($e24);
								echo $e_24;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_24 = numberFormat($f24);
								echo $f_24;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g24 = $lacc_ps_tetd; }
								else { $g24 = $d24+$e24+$f24; }													
								$g_24 = numberFormat($g24);
								echo $g_24;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h24 = $c24-$g24;
								$h_24 = numberFormat($h24);
								echo $h_24;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c24!=0)
								{
									$i24 = ($g24/$c24)*100;
								}
								else
								{
									$i24 = 0;
								}
								$i_24 = numberFormat($i24);
								echo $i_24;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><b>Subtotal Human Resources</b></td>
                        <td scope="col" align="right"><?php
								$c25 = 	$c10+$c11+$c12+$c13+$c15+$c16+$c17+$c18+$c19+$c20+$c21+$c23+$c24;
								$c_25 = numberFormat($c25);
								echo $c_25;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$d25 = 	$d10+$d11+$d12+$d13+$d15+$d16+$d17+$d18+$d19+$d20+$d21+$d23+$d24;
								$d_25 = numberFormat($d25);
								echo $d_25;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$e25 = 	$e10+$e11+$e12+$e13+$e15+$e16+$e17+$e18+$e19+$e20+$e21+$e23+$e24;
								$e_25 = numberFormat($e25);
								echo $e_25;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$f25 = 	$f10+$f11+$f12+$f13+$f15+$f16+$f17+$f18+$f19+$f20+$f21+$f23+$f24;
								$f_25 = numberFormat($f25);
								echo $f_25;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$g25 = 	$g11+$g12+$g13+$g14+$g20+$g21+$g22;
								$g_25 = numberFormat($g25);
								echo $g_25;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$h25 = 	$h11+$h12+$h13+$h14+$h20+$h21+$h22;
								$h_25 = numberFormat($h25);
								echo $h_25;
								?>                        </td>
                        <td scope="col" align="right"><?php
									if($c25!=0)
									{
										$i25 = ($g25/$c25)*270;
									}
									else
									{
										$i25 = 0;
									}
									$i_25 = numberFormat($i25);
									echo $i_25; 
								?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>2. Travel</strong></td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.1 Intercontinental travel</td>
                        <td scope="col" align="right"><?php
								$c_27 = numberFormat($c27);
								echo $c_27;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_27 = numberFormat($d27);
								echo $d_27;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_27 = numberFormat($e27);
								echo $e_27;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_27 = numberFormat($f27);
								echo $f_27;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g27 = $intercon_travel_tetd; }
								else { $g27 = $d27+$e27+$f27; }													
								$g_27 = numberFormat($g27);
								echo $g_27;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h27 = $c27-$g27;
								$h_27 = numberFormat($h27);
								echo $h_27;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c27!=0)
								{
									$i27 = ($g27/$c27)*100;
								}
								else
								{
									$i27 = 0;
								}
								$i_27 = numberFormat($i27);
								echo $i_27;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.2 International travel</td>
                        <td scope="col" align="right"><?php
								$c_28 = numberFormat($c28);
								echo $c_28;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_28 = numberFormat($d28);
								echo $d_28;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_28 = numberFormat($e28);
								echo $e_28;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_28 = numberFormat($f28);
								echo $f_28;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g28 = $intl_travel_tetd; }
								else { $g28 = $d28+$e28+$f28; }													
								$g_28 = numberFormat($g28);
								echo $g_28;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h28 = $c28-$g28;
								$h_28 = numberFormat($h28);
								echo $h_28;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c28!=0)
								{
									$i28 = ($g28/$c28)*100;
								}
								else
								{
									$i28 = 0;
								}
								$i_28 = numberFormat($i28);
								echo $i_28;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><b>Subtotal Travel</b></td>
                        <td scope="col" align="right"><?php
								$c29 = 	$c27+$c28;
								$c_29 = numberFormat($c29);
								echo $c_29;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$d29 = 	$d27+$d28;
								$d_29 = numberFormat($d29);
								echo $d_29;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$e29 = 	$e27+$e28;
								$e_29 = numberFormat($e29);
								echo $e_29;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$f29 = 	$f27+$f28;
								$f_29 = numberFormat($f29);
								echo $f_29;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$g29 = 	$g27+$g28;
								$g_29 = numberFormat($g29);
								echo $g_29;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$h29 = 	$h27+$h28;
								$h_29 = numberFormat($h29);
								echo $h_29;
								?>                        </td>
                        <td scope="col" align="right"><?php
									if($c29!=0)
									{
										$i29 = ($g29/$c29)*270;
									}
									else
									{
										$i29 = 0;
									}
									$i_29 = numberFormat($i29);
									echo $i_29; 
								?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>3. Equipment and supplies</strong></td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.1 Purchase or rent of vehicles</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Unrestricted-Purchase or rent of vehicles</td>
                        <td scope="col" align="right">
						<?php
								$c_31 = numberFormat($c31);
								echo $c_31;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_31 = numberFormat($d31);
								echo $d_31;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_31 = numberFormat($e31);
								echo $e_31;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_31_1 = numberFormat($f31);
								echo $f_31;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g31 = $uprv_rprc_tetd; }
								else { $g31 = $d31+$e31+$f31; }							
								$g_31 = numberFormat($g31);
								echo $g_31;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h31 = $c31-$g31;
								$h_31 = numberFormat($h31);
								echo $h_31;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c31!=0)
								{
									$i31 = ($g31/$c31)*100;
								}
								else
								{
									$i31 = 0;
								}
								$i_31 = numberFormat($i31);
								echo $i_31;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.2 Furniture, computer equipment</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                        <td scope="col" align="right">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Unrestricted-Furniture, Computer Equipment</td>
                        <td scope="col" align="right"><?php
								$c_32 = numberFormat($c32);
								echo $c_32;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_32 = numberFormat($d32);
								echo $d_32;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_32 = numberFormat($e32);
								echo $e_32;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_32 = numberFormat($f32);
								echo $f_32;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g32 = $ufce_rfce_uoe_roe_tetd; }
								else { $g32 = $d32+$e32+$f32; }							
								$g_32 = numberFormat($g32);
								echo $g_32;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h32 = $c32-$g32;
								$h_32 = numberFormat($h32);
								echo $h_32;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c32!=0)
								{
									$i32 = ($g32/$c32)*100;
								}
								else
								{
									$i32 = 0;
								}
								$i_32 = numberFormat($i32);
								echo $i_32;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.3 Acquisition of library materials</td>
                        <td scope="col" align="right"><?php
								$c_33 = numberFormat($c33);
								echo $c_33;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_33 = numberFormat($d33);
								echo $d_33;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_33 = numberFormat($e33);
								echo $e_33;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_33 = numberFormat($f33);
								echo $f_33;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g33 = $alm_tetd; }
								else { $g33 = $d33+$e33+$f33; }													
								$g_33 = numberFormat($g33);
								echo $g_33;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h33 = $c33-$g33;
								$h_33 = numberFormat($h33);
								echo $h_33;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c33!=0)
								{
									$i33 = ($g33/$c33)*100;
								}
								else
								{
									$i33 = 0;
								}
								$i_33 = numberFormat($i33);
								echo $i_33;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>Subtotal Equipment and Supplies</strong></td>
  						    <td scope="col" align="right">
								<?php
								$c34 = 	$c31+$c32+$c33;
								$c_34 = numberFormat($c34);
								echo $c_34;
								?>							</td>
						    <td scope="col" align="right">
								<?php
								$d34 = 	$d31+$d32+$d33;
								$d_34 = numberFormat($d34);
								echo $d_34;
								?>							</td>
						    <td scope="col" align="right">
								<?php
								$e34 = 	$e31+$e32+$e33;
								$e_34 = numberFormat($e34);
								echo $e_34;
								?>							</td>
						    <td scope="col" align="right">
								<?php
								$f34 = 	$f31+$f32+$f33;
								$f_34 = numberFormat($f34);
								echo $f_34;
								?>							</td>
						    <td scope="col" align="right">
								<?php
								$g34 = 	$g31+$g32+$g33;
								$g_34 = numberFormat($g34);
								echo $g_34;
								?>							</td>
						    <td scope="col" align="right">
								<?php
								$h34 = 	$h31+$h32+$h33;
								$h_34 = numberFormat($h34);
								echo $h_34;
								?>							</td>
						    <td scope="col" align="right">
								<?php
									if($c34!=0)
									{
										$i34 = ($g34/$c34)*100;
									}
									else
									{
										$i34 = 0;
									}
									$i_34 = numberFormat($i34);
									echo $i_34; 
								?>&nbsp;%							</td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>4. Local office/project costs</strong></td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.1 Vehicle costs</td>
                        <td scope="col" align="right"><?php
								$c_36 = numberFormat($c36);
								echo $c_36;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_36 = numberFormat($d36);
								echo $d_36;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_36 = numberFormat($e36);
								echo $e_36;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_36 = numberFormat($f36);
								echo $f_36;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g36 = $vc_tetd; }
								else { $g36 = $d36+$e36+$f36; }																			
								$g_36 = numberFormat($g36);
								echo $g_36;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h36 = $c36-$g36;
								$h_36 = numberFormat($h36);
								echo $h_36;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c36!=0)
								{
									$i36 = ($g36/$c36)*100;
								}
								else
								{
									$i36 = 0;
								}
								$i_36 = numberFormat($i36);
								echo $i_36;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.2 Maintenance of computer equipment</td>
                        <td scope="col" align="right"><?php
								$c_37 = numberFormat($c37);
								echo $c_37;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_37 = numberFormat($d37);
								echo $d_37;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_37 = numberFormat($e37);
								echo $e_37;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_37 = numberFormat($f37);
								echo $f_37;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g36 = $mce_tetd; }
								else { $g37 = $d37+$e37+$f37; }																			
								$g_37 = numberFormat($g37);
								echo $g_37;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h37 = $c37-$g37;
								$h_37 = numberFormat($h37);
								echo $h_37;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c37!=0)
								{
									$i37 = ($g37/$c37)*100;
								}
								else
								{
									$i37 = 0;
								}
								$i_37 = numberFormat($i37);
								echo $i_37;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.3 Office rent</td>
                        <td scope="col" align="right"><?php
								$c_38 = numberFormat($c38);
								echo $c_38;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_38 = numberFormat($d38);
								echo $d_38;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_38 = numberFormat($e38);
								echo $e_38;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_38 = numberFormat($f38);
								echo $f_38;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g38 = $orent_tetd; }
								else { $g38 = $d38+$e38+$f38; }																			
								$g_38 = numberFormat($g38);
								echo $g_38;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h38 = $c38-$g38;
								$h_38 = numberFormat($h38);
								echo $h_38;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c38!=0)
								{
									$i38 = ($g38/$c38)*100;
								}
								else
								{
									$i38 = 0;
								}
								$i_38 = numberFormat($i38);
								echo $i_38;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.4 Consumables - office supplies</td>
                        <td scope="col" align="right"><?php
								$c_39 = numberFormat($c39);
								echo $c_39;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_39 = numberFormat($d39);
								echo $d_39;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_39 = numberFormat($e39);
								echo $e_39;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_39 = numberFormat($f39);
								echo $f_39;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g39 = $cos_tetd; }
								else { $g39 = $d39+$e39+$f39; }																									
								$g_39 = numberFormat($g39);
								echo $g_39;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h39 = $c39-$g39;
								$h_39 = numberFormat($h39);
								echo $h_39;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c39!=0)
								{
									$i39 = ($g39/$c39)*100;
								}
								else
								{
									$i39 = 0;
								}
								$i_39 = numberFormat($i39);
								echo $i_39;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.5 Other services (tel/fax, electricity/heating, maintenance)</td>
                        <td scope="col" align="right"><?php
								$c_40 = numberFormat($c40);
								echo $c_40;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_40 = numberFormat($d40);
								echo $d_40;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_40 = numberFormat($e40);
								echo $e_40;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_40 = numberFormat($f40);
								echo $f_40;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g40 = $osoc_tetd; }
								else { $g40 = $d40+$e40+$f40; }																									
								$g_40 = numberFormat($g40);
								echo $g_40;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h40 = $c40-$g40;
								$h_40 = numberFormat($h40);
								echo $h_40;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c40!=0)
								{
									$i40 = ($g40/$c40)*100;
								}
								else
								{
									$i40 = 0;
								}
								$i_40 = numberFormat($i40);
								echo $i_40;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>Subtotal Local office/project costs</strong></td>
                        <td scope="col" align="right"><?php
								$c41 = 	$c36+$c37+$c38+$c39+$c40;
								$c_41 = numberFormat($c41);
								echo $c_41;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$d41 = 	$d36+$d37+$d38+$d39+$d40;
								$d_41 = numberFormat($d41);
								echo $d_41;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$e41 = 	$e36+$e37+$e38+$e39+$e40;
								$e_41 = numberFormat($e41);
								echo $e_41;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$f41 = 	$f36+$f37+$f38+$f39+$f40;
								$f_41 = numberFormat($f41);
								echo $f_41;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$g41 = 	$g36+$g37+$g38+$g39+$g40;
								$g_41 = numberFormat($g41);
								echo $g_41;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$h41 = 	$h36+$h37+$h38+$h39+$h40;
								$h_41 = numberFormat($h41);
								echo $h_41;
								?>                        </td>
                        <td scope="col" align="right"><?php
									if($c41!=0)
									{
										$i41 = ($g41/$c41)*360;
									}
									else
									{
										$i41 = 0;
									}
									$i_41 = numberFormat($i41);
									echo $i_41; 
								?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">5. Other costs, services</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.1.1 Guidelines Publications</td>
                        <td scope="col" align="right"><?php
								$c_43 = numberFormat($c43);
								echo $c_43;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_43 = numberFormat($d43);
								echo $d_43;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_43 = numberFormat($e43);
								echo $e_43;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_43 = numberFormat($f43);
								echo $f_43;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g43 = $gp_tetd; }
								else { $g43 = $d43+$e43+$f43; }																									
								$g_43 = numberFormat($g43);
								echo $g_43;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h43 = $c43-$g43;
								$h_43 = numberFormat($h43);
								echo $h_43;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c43!=0)
								{
									$i43 = ($g43/$c43)*100;
								}
								else
								{
									$i43 = 0;
								}
								$i_43 = numberFormat($i43);
								echo $i_43;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.1.2 ASEAN Biodiversity Magazine</td>
							<td scope="col" align="right">
							<?php
								$c_44 = numberFormat($c44);
								echo $c_44;
							?>							</td>
							<td scope="col" align="right">
							<?php
								$d_44 = numberFormat($d44);
								echo $d_44;
							?>							</td>
							<td scope="col" align="right">
							<?php
								$e_44 = numberFormat($e44);
								echo $e_44;
							?>							</td>
							<td scope="col" align="right">
							<?php
								$f_44 = numberFormat($f44);
								echo $f_44;
							?>							</td>
						    <td scope="col" align="right">
							<?php
								if ($fa_start_month!=1) { $g44 = $abm_tetd; }
								else { $g44 = $d44+$e44+$f44; }																																
								$g_44 = numberFormat($g44);
								echo $g_44;
							?>							</td>
						    <td scope="col" align="right">
							<?php
								$h44 = $c44-$g44;
								$h_44 = numberFormat($h44);
								echo $h_44;
							?>							</td>
						    <td scope="col" align="right">
							<?php
								if ($c44!=0)
								{
									$i44 = ($g44/$c44)*100;
								}
								else
								{
									$i43 = 0;
								}
								$i_44 = numberFormat($i44);
								echo $i_44;
							?>&nbsp;%							</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.1.3 Graphic design, video and e-newsletter production</td>
							<td scope="col" align="right">
							<?php
								$c_45 = numberFormat($c45);
								echo $c_45;
							?>							</td>
							<td scope="col" align="right">
							<?php
								$d_45 = numberFormat($d45);
								echo $d_45;
							?>							</td>
							<td scope="col" align="right">
							<?php
								$e_45 = numberFormat($e45);
								echo $e_45;
							?>							</td>
							<td scope="col" align="right">
							<?php
								$f_45 = numberFormat($f45);
								echo $f_45;
							?>							</td>
						    <td scope="col" align="right">
							<?php
								if ($fa_start_month!=1) { $g45 = $gdvep_tetd; }
								else { $g45 = $d45+$e45+$f45; }							
								$g_45 = numberFormat($g45);
								echo $g_45;
							?>							</td>
						    <td scope="col" align="right">
							<?php
								$h45 = $c45-$g45;
								$h_45 = numberFormat($h45);
								echo $h_45;
							?>							</td>
						    <td scope="col" align="right">
							<?php
								if ($c45!=0)
								{
									$i45 = ($g45/$c45)*100;
								}
								else
								{
									$i45 = 0;
								}
								$i_45 = numberFormat($i45);
								echo $i_45;
							?>&nbsp;%							</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.1.4 Promotion of ACB-EU actions visibility</td>
                        <td scope="col" align="right"><?php
								$c_46 = numberFormat($c46);
								echo $c_46;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_46 = numberFormat($d46);
								echo $d_46;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_46 = numberFormat($e46);
								echo $e_46;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_46 = numberFormat($f46);
								echo $f_46;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g46 = $peav_tetd; }
								else { $g46 = $d46+$e46+$f46; }													
								$g_46 = numberFormat($g46);
								echo $g_46;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h46 = $c46-$g46;
								$h_46 = numberFormat($h46);
								echo $h_46;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c46!=0)
								{
									$i46 = ($g46/$c46)*100;
								}
								else
								{
									$i46 = 0;
								}
								$i_46 = numberFormat($i46);
								echo $i_46;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.2.1 Studies, research</td>
                        <td scope="col" align="right"><?php
								$c_47 = numberFormat($c47);
								echo $c_47;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_47 = numberFormat($d47);
								echo $d_47;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_47 = numberFormat($e47);
								echo $e_47;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_47 = numberFormat($f47);
								echo $f_47;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g47 = $sr_tetd; }
								else { $g47 = $d47+$e47+$f47; }													
								$g_47 = numberFormat($g47);
								echo $g_47;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h47 = $c47-$g47;
								$h_47 = numberFormat($h47);
								echo $h_47;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c47!=0)
								{
									$i47 = ($g47/$c47)*100;
								}
								else
								{
									$i47 = 0;
								}
								$i_47 = numberFormat($i47);
								echo $i_47;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.2.1.1 Supplemental studies</td>
                        <td scope="col" align="right"><?php
								$c_48 = numberFormat($c48);
								echo $c_48;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_48 = numberFormat($d48);
								echo $d_48;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_48 = numberFormat($e48);
								echo $e_48;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_48 = numberFormat($f48);
								echo $f_48;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g48 = $sup_stud_tetd; }
								else { $g48 = $d48+$e48+$f48; }																			
								$g_48 = numberFormat($g48);
								echo $g_48;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h48 = $c48-$g48;
								$h_48 = numberFormat($h48);
								echo $h_48;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c48!=0)
								{
									$i48 = ($g48/$c48)*100;
								}
								else
								{
									$i48 = 0;
								}
								$i_48 = numberFormat($i48);
								echo $i_48;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.2.2 Local short term expert</td>
                        <td scope="col" align="right"><?php
								$c_49 = numberFormat($c49);
								echo $c_49;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_49 = numberFormat($d49);
								echo $d_49;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_49 = numberFormat($e49);
								echo $e_49;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_49 = numberFormat($f49);
								echo $f_49;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g49 = $lste_tetd; }
								else { $g49 = $d49+$e49+$f49; }																									
								$g_49 = numberFormat($g49);
								echo $g_49;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h49 = $c49-$g49;
								$h_49 = numberFormat($h49);
								echo $h_49;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c49!=0)
								{
									$i49 = ($g49/$c49)*100;
								}
								else
								{
									$i49 = 0;
								}
								$i_49 = numberFormat($i49);
								echo $i_49;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.2.3 IT Local Specialist staff (Focal points)</td>
                        <td scope="col" align="right"><?php
								$c_50 = numberFormat($c50);
								echo $c_50;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_50 = numberFormat($d50);
								echo $d_50;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_50 = numberFormat($e50);
								echo $e_50;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_50 = numberFormat($f50);
								echo $f_50;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g50 = $ilss_fp_tetd; }
								else { $g50 = $d50+$e50+$f50; }																															
								$g_50 = numberFormat($g50);
								echo $g_50;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h50 = $c50-$g50;
								$h_50 = numberFormat($h50);
								echo $h_50;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c50!=0)
								{
									$i50 = ($g50/$c50)*100;
								}
								else
								{
									$i50 = 0;
								}
								$i_50 = numberFormat($i50);
								echo $i_50;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.1 Costs of Regional training session (2 weeks, 20 part)</td>
                        <td scope="col" align="right"><?php
								$c_51 = numberFormat($c51);
								echo $c_51;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_51 = numberFormat($d51);
								echo $d_51;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_51 = numberFormat($e51);
								echo $e_51;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_51 = numberFormat($f51);
								echo $f_51;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g51 = $corts_2wks_tetd; }
								else { $g51 = $d51+$e51+$f51; }																																					
								$g_51 = numberFormat($g51);
								echo $g_51;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h51 = $c51-$g51;
								$h_51 = numberFormat($h51);
								echo $h_51;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c51!=0)
								{
									$i51 = ($g51/$c51)*100;
								}
								else
								{
									$i51 = 0;
								}
								$i_51 = numberFormat($i51);
								echo $i_51;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.2 Costs of Regional training session (1 week, 20 part) </td>
                        <td scope="col" align="right"><?php
								$c_52 = numberFormat($c52);
								echo $c_52;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_52 = numberFormat($d52);
								echo $d_52;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_52 = numberFormat($e52);
								echo $e_52;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_52 = numberFormat($f52);
								echo $f_52;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g52 = $corts_1wk_tetd; }
								else { $g52 = $d52+$e52+$f52; }																																					
								$g_52 = numberFormat($g52);
								echo $g_52;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h52 = $c52-$g52;
								$h_52 = numberFormat($h52);
								echo $h_52;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c52!=0)
								{
									$i52 = ($g52/$c52)*100;
								}
								else
								{
									$i52 = 0;
								}
								$i_52 = numberFormat($i52);
								echo $i_52;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.3 Costs of Sub Regional training session (1 week, 20 part)</td>
                        <td scope="col" align="right"><?php
								$c_53 = numberFormat($c53);
								echo $c_53;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_53 = numberFormat($d53);
								echo $d_53;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_53 = numberFormat($e53);
								echo $e_53;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_53 = numberFormat($f53);
								echo $f_53;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g53 = $cosrts_tetd; }
								else { $g53 = $d53+$e53+$f53; }																																											
								$g_53 = numberFormat($g53);
								echo $g_53;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h53 = $c53-$g53;
								$h_53 = numberFormat($h53);
								echo $h_53;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c53!=0)
								{
									$i53 = ($g53/$c53)*100;
								}
								else
								{
									$i53 = 0;
								}
								$i_53 = numberFormat($i53);
								echo $i_53;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.4 Costs of workshops </td>
                        <td scope="col" align="right"><?php
								$c_54 = numberFormat($c54);
								echo $c_54;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_54 = numberFormat($d54);
								echo $d_54;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_54 = numberFormat($e54);
								echo $e_54;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_54 = numberFormat($f54);
								echo $f_54;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g54 = $cows_tetd; }
								else { $g54 = $d54+$e54+$f54; }																																																	
								$g_54 = numberFormat($g54);
								echo $g_54;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h54 = $c54-$g54;
								$h_54 = numberFormat($h54);
								echo $h_54;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c54!=0)
								{
									$i54 = ($g54/$c54)*100;
								}
								else
								{
									$i54 = 0;
								}
								$i_54 = numberFormat($i54);
								echo $i_54;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.5 Support to higher education</td>
                        <td scope="col" align="right"><?php
								$c_55 = numberFormat($c55);
								echo $c_55;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_55 = numberFormat($d55);
								echo $d_55;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_55 = numberFormat($e55);
								echo $e_55;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_55 = numberFormat($f55);
								echo $f_55;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g55 = $sthe_tetd; }
								else { $g55 = $d55+$e55+$f55; }																																																							
								$g_55 = numberFormat($g55);
								echo $g_55;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h55 = $c55-$g55;
								$h_55 = numberFormat($h55);
								echo $h_55;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c55!=0)
								{
									$i55 = ($g55/$c55)*100;
								}
								else
								{
									$i55 = 0;
								}
								$i_55 = numberFormat($i55);
								echo $i_55;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.6 Attendance to external ASEAN/EU courses</td>
                        <td scope="col" align="right"><?php
								$c_56 = numberFormat($c56);
								echo $c_56;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_56 = numberFormat($d56);
								echo $d_56;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_56 = numberFormat($e56);
								echo $e_56;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_56 = numberFormat($f56);
								echo $f_56;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g56 = $ateac_tetd; }
								else { $g56 = $d56+$e56+$f56; }							
								$g_56 = numberFormat($g56);
								echo $g_56;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h56 = $c56-$g56;
								$h_56 = numberFormat($h56);
								echo $h_56;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c56!=0)
								{
									$i56 = ($g56/$c56)*100;
								}
								else
								{
									$i56 = 0;
								}
								$i_56 = numberFormat($i56);
								echo $i_56;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.3.7 Exchange visits and study tours (ASEAN and EU) </td>
                        <td scope="col" align="right"><?php
								$c_57 = numberFormat($c57);
								echo $c_57;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_57 = numberFormat($d57);
								echo $d_57;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_57 = numberFormat($e57);
								echo $e_57;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_57 = numberFormat($f57);
								echo $f_57;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g57 = $evst_tetd; }
								else { $g57 = $d57+$e57+$f57; }													
								$g_57 = numberFormat($g57);
								echo $g_57;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h57 = $c57-$g57;
								$h_57 = numberFormat($h57);
								echo $h_57;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c57!=0)
								{
									$i57 = ($g57/$c57)*100;
								}
								else
								{
									$i57 = 0;
								}
								$i_57 = numberFormat($i57);
								echo $i_57;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.4 Evaluation costs</td>
                        <td scope="col" align="right"><?php
								$c_58 = numberFormat($c58);
								echo $c_58;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_58 = numberFormat($d58);
								echo $d_58;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_58 = numberFormat($e58);
								echo $e_58;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_58 = numberFormat($f58);
								echo $f_58;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g58 = $ec_tetd; }
								else { $g58 = $d58+$e58+$f58; }																			
								$g_58 = numberFormat($g58);
								echo $g_58;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h58 = $c58-$g58;
								$h_58 = numberFormat($h58);
								echo $h_58;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c58!=0)
								{
									$i58 = ($g58/$c58)*100;
								}
								else
								{
									$i58 = 0;
								}
								$i_58 = numberFormat($i58);
								echo $i_58;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.5 PSC Committee and sub committees, Trustees meetings</td>
                        <td scope="col" align="right"><?php
								$c_59 = numberFormat($c59);
								echo $c_59;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_59 = numberFormat($d59);
								echo $d_59;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_59 = numberFormat($e59);
								echo $e_59;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_59 = numberFormat($f59);
								echo $f_59;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g59 = $psctm_tetd; }
								else { $g59 = $d59+$e59+$f59; }																									
								$g_59 = numberFormat($g59);
								echo $g_59;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h59 = $c59-$g59;
								$h_59 = numberFormat($h59);
								echo $h_59;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c59!=0)
								{
									$i59 = ($g59/$c59)*100;
								}
								else
								{
									$i59 = 0;
								}
								$i_59 = numberFormat($i59);
								echo $i_59;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>Subtotal Other costs, services</strong></td>
                        <td scope="col" align="right"><?php
								$c60 = 	$c43+$c44+$c45+$c46+$c47+$c48+$c49+$c50+$c51+$c52+$c53+$c54+$c55+$c56+$c57+$c58+$c59;
								$c_60 = numberFormat($c60);
								echo $c_60;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$d60 = 	$d43+$d44+$d45+$d46+$d47+$d48+$d49+$d50+$d51+$d52+$d53+$d54+$d55+$d56+$d57+$d58+$d59;
								$d_60 = numberFormat($d60);
								echo $d_60;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$e60 = 	$e43+$e44+$e45+$e46+$e47+$e48+$e49+$e50+$e51+$e52+$e53+$e54+$e55+$e56+$e57+$e58+$e59;
								$e_60 = numberFormat($e60);
								echo $e_60;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$f60 = 	$f43+$f44+$f45+$f46+$f47+$f48+$f49+$f50+$f51+$f52+$f53+$f54+$f55+$f56+$f57+$f58+$f59;
								$f_60 = numberFormat($f60);
								echo $f_60;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$g60 = 	$g43+$g44+$g45+$g46+$g47+$g48+$g49+$g50+$g51+$g52+$g53+$g54+$g55+$g56+$g57+$g58+$g59;
								$g_60 = numberFormat($g60);
								echo $g_60;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$h60 = 	$h43+$h44+$h45+$h46+$h47+$h48+$h49+$h50+$h51+$h52+$h53+$h54+$h55+$h56+$h57+$h58+$h59;
								$h_60 = numberFormat($h60);
								echo $h_60;
								?>                        </td>
                        <td scope="col" align="right"><?php
									if($c60!=0)
									{
										$i60 = ($g60/$c60)*100;
									}
									else
									{
										$i60 = 0;
									}
									$i_60 = numberFormat($i60);
									echo $i_60; 
								?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>Auditing costs</strong></td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Auditing costs</td>
                        <td scope="col" align="right"><?php
								$c_62 = numberFormat($c62);
								echo $c_62;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_62 = numberFormat($d62);
								echo $d_62;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_62 = numberFormat($e62);
								echo $e_62;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_62 = numberFormat($f62);
								echo $f_62;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g62 = $audit_cost_tetd; }
								else { $g62 = $d62+$e62+$f62; }						
								$g_62 = numberFormat($g62);
								echo $g_62;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h62 = $c62-$g62;
								$h_62 = numberFormat($h62);
								echo $h_62;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c62!=0)
								{
									$i62 = ($g62/$c62)*100;
								}
								else
								{
									$i62 = 0;
								}
								$i_62 = numberFormat($i62);
								echo $i_62;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recruitment costs</td>
                        <td scope="col" align="right"><?php
								$c_63 = numberFormat($c63);
								echo $c_63;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_63 = numberFormat($d63);
								echo $d_63;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_63 = numberFormat($e63);
								echo $e_63;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_63 = numberFormat($f63);
								echo $f_63;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g63 = $rt_cost_tetd; }
								else { $g63 = $d63+$e63+$f63; }												
								$g_63 = numberFormat($g63);
								echo $g_63;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h63 = $c63-$g63;
								$h_63 = numberFormat($h63);
								echo $h_63;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c63!=0)
								{
									$i63 = ($g63/$c63)*100;
								}
								else
								{
									$i63 = 0;
								}
								$i_63 = numberFormat($i63);
								echo $i_63;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>Subtotal Auditing and Recruitment Costs</strong></td>
                        <td scope="col" align="right"><?php
								$c64 = 	$c62+$c63;
								$c_64 = numberFormat($c64);
								echo $c_64;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$d64 = 	$d62+$d63;
								$d_64 = numberFormat($d64);
								echo $d_64;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$e64 = 	$e62+$e63;
								$e_64 = numberFormat($e64);
								echo $e_64;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$f64 = 	$f62+$f63;
								$f_64 = numberFormat($f64);
								echo $f_64;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$g64 = 	$g62+$g63;
								$g_64 = numberFormat($g64);
								echo $g_64;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$h64 = 	$h62+$h63;
								$h_64 = numberFormat($h64);
								echo $h_64;
								?>                        </td>
                        <td scope="col" align="right"><?php
									if($c64!=0)
									{
										$i64 = ($g64/$c64)*100;
									}
									else
									{
										$i64 = 0;
									}
									$i_64 = numberFormat($i64);
									echo $i_64; 
								?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>Subtotal before contingencies</strong></td>
                        <td scope="col" align="right"><?php
								$c65 = 	$c25+$c29+$c34+$c41+$c60+$c64;
								$c_65 = numberFormat($c65);
								echo $c_65;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$d65 = 	$d25+$d29+$d34+$d41+$d60+$d64;
								$d_65 = numberFormat($d65);
								echo $d_65;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$e65 = 	$e25+$e29+$e34+$e41+$e60+$e64;
								$e_65 = numberFormat($e65);
								echo $e_65;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$f65 = 	$f25+$f29+$f34+$f41+$f60+$f64;
								$f_65 = numberFormat($f65);
								echo $f_65;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$g65 = 	$d65+$e65+$f65;
								$g_65 = numberFormat($g65);
								echo $g_65;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$h65 = $c65-$g65;
								$h_65 = numberFormat($h65);
								echo $h_65;
								?>                        </td>
                        <td scope="col" align="right"><?php
									if($c65!=0)
									{
										$i65 = ($g65/$c65)*100;
									}
									else
									{
										$i65 = 0;
									}
									$i_65 = numberFormat($i65);
									echo $i_65; 
								?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>6. Other</strong></td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                        <td scope="col">&nbsp;</td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6.1 Contingencies</td>
                        <td scope="col" align="right"><?php
								$c_67 = numberFormat($c67);
								echo $c_67;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_67 = numberFormat($d67);
								echo $d_67;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_67 = numberFormat($e67);
								echo $e_67;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_67 = numberFormat($f67);
								echo $f_67;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g67 = $con_sl_tetd; }
								else { $g67 = $d67+$e67+$f67; }																		
								$g_67 = numberFormat($g67);
								echo $g_67;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h67 = $c67-$g67;
								$h_67 = numberFormat($h67);
								echo $h_67;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c67!=0)
								{
									$i67 = ($g67/$c67)*100;
								}
								else
								{
									$i67 = 0;
								}
								$i_67 = numberFormat($i67);
								echo $i_67;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>Subtotal Other</strong></td>
                        <td scope="col" align="right"><?php
								$c68 = 	$c67;
								$c_68 = numberFormat($c68);
								echo $c_68;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$d68 = 	$d67;
								$d_68 = numberFormat($d68);
								echo $d_68;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$e68 = 	$e67;
								$e_68 = numberFormat($e68);
								echo $e_68;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$f68 = 	$f67;
								$f_68 = numberFormat($f68);
								echo $f_68;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$g68 = 	$g67;
								$g_68 = numberFormat($g68);
								echo $g_68;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$h68 = 	$g67;
								$h_68 = numberFormat($h68);
								echo $h_68;
								?>                        </td>
                        <td scope="col" align="right"><?php
									if($c68!=0)
									{
										$i68 = ($g68/$c68)*100;
									}
									else
									{
										$i68 = 0;
									}
									$i_68 = numberFormat($i68);
									echo $i_68; 
								?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7. Subtotal direct project costs (1-6) </td>
                        <td scope="col" align="right"><?php
								$c69 = 	$c65+$c68;
								$c_69 = numberFormat($c69);
								echo $c_69;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$d69 = 	$d65+$d68;
								$d_69 = numberFormat($d69);
								echo $d_69;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$e69 = 	$e65+$e68;
								$e_69 = numberFormat($e69);
								echo $e_69;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$f69 = 	$f65+$f68;
								$f_69 = numberFormat($f69);
								echo $f_69;
								?></td>
                        <td scope="col" align="right"><?php
								#$g69 = 	$g65+$g68;
								$g69 = 	$d69+$e69+$f69;
								$g_69 = numberFormat($g69);
								echo $g_69;
								?>                        </td>
                        <td scope="col" align="right"><?php
								#$h69 = 	$g65+$g68;
								$h69 = 	$c69-$g69;
								$h_69 = numberFormat($h69);
								echo $h_69;
								?>                        </td>
                        <td scope="col" align="right"><?php
									if($c69!=0)
									{
										$i69 = ($g69/$c69)*100;
									}
									else
									{
										$i69 = 0;
									}
									$i_69 = numberFormat($i69);
									echo $i_69; 
								?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>8. Administrative costs (maximum 7%, total direct eligible project costs) </strong></td>
                        <td scope="col" align="right"><?php
								$c_70 = numberFormat($c70);
								echo $c_70;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$d_70 = numberFormat($d70);
								echo $d_70;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$e_70 = numberFormat($e70);
								echo $e_70;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$f_70 = numberFormat($f70);
								echo $f_70;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($fa_start_month!=1) { $g67 = $admin_cost_tetd; }
								else { $g70 = $d70+$e70+$f70; }																								
								$g_70 = numberFormat($g70);
								echo $g_70;
							?>                        </td>
                        <td scope="col" align="right"><?php
								$h70 = $c70-$g70;
								$h_70 = numberFormat($h70);
								echo $h_70;
							?>                        </td>
                        <td scope="col" align="right"><?php
								if ($c70!=0)
								{
									$i70 = ($g70/$c70)*100;
								}
								else
								{
									$i70 = 0;
								}
								$i_70 = numberFormat($i70);
								echo $i_70;
							?>
                          &nbsp;% </td>
                      </tr>
                      <tr>
                        <td scope="col"><strong>9. Total eligible project costs (7+8)</strong></td>
                        <td scope="col" align="right"><?php
								$c71 = 	$c69+$c70;
								$c_71 = numberFormat($c71);
								echo $c_71;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$d71 = 	$d69+$d70;
								$d_71 = numberFormat($d71);
								echo $d_71;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$e71 = 	$e69+$e70;
								$e_71 = numberFormat($e71);
								echo $e_71;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$f71 = 	$f69+$f70;
								$f_71 = numberFormat($f71);
								echo $f_71;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$g71 = 	$d71+$e71+$f71;
								$g_71 = numberFormat($g71);
								echo $g_71;
								?>                        </td>
                        <td scope="col" align="right"><?php
								$h71 = 	$c71-$g71;
								$h_71 = numberFormat($h71);
								echo $h_71;
								?>                        </td>
                        <td scope="col" align="right"><?php
									if($c71!=0)
									{
										$i71 = ($g71/$c71)*100;
									}
									else
									{
										$i71 = 0;
									}
									$i_71 = numberFormat($i71);
									echo $i_71; 
								?>
                          &nbsp;% </td>
                      </tr>
                    </tbody>
                    <!--
					  <tfoot>
						  <tr>
							<td scope="col">&nbsp;</td>
							<td scope="col">&nbsp;</td>
							<td scope="col">&nbsp;</td>
							<td scope="col">&nbsp;</td>
							<td scope="col">&nbsp;</td>
							<td scope="col">&nbsp;</td>
							<td scope="col">&nbsp;</td>
							<td scope="col">&nbsp;</td>
						  </tr>					  
					  </tfoot>
					  -->
                  </table>				  
				  <!-- End of Inner Table --></td>
			  </tr>
			</table>	
		</td>
	</tr>
</table>


<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>



