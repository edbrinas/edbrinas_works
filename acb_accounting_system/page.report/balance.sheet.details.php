<?php 
$report_name = "BALANCE SHEET";
include("./../includes/header.report.php");
$report_code_srd = 55;
$report_code = 56;
$report_type_income = 54;
$report_type_expense = 53;

$bl_currency_code = $_POST['bl_currency_code'];
$bl_start_year = $_POST['bl_start_year'];
$bl_start_month = $_POST['bl_start_month'];
$bl_start_day = $_POST['bl_start_day'];
$bl_end_year = $_POST['bl_end_year'];
$bl_end_month = $_POST['bl_end_month'];
$bl_end_day = $_POST['bl_end_day'];
$bl_budget_id = $_POST['bl_budget_id'];
$bl_donor_id = $_POST['bl_donor_id'];
$consolidate_reports = $_POST['consolidate_reports'];
$use_obligated_value = $_POST['use_obligated_value'];

$system_start_date = date("Y-m-d", mktime(0,0,0,1,1,2008));
$selected_start_date_year = date("Y-m-d", mktime(0,0,0,1,1,$bl_start_year));
$start_date_minus_1 = date("Y-m-d", mktime(0,0,0,$bl_start_month,$bl_start_day-1, $bl_start_year));

$start_date = date("Y-m-d", mktime(0,0,0,$bl_start_month,$bl_start_day, $bl_start_year));
$end_date = date("Y-m-d", mktime(0,0,0,$bl_end_month,$bl_end_day,$bl_end_year));
$bl_currency_name = getConfigurationValueById($bl_currency_code);
?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br />
					<?php echo upperCase(getCompanyName()); ?>
					<br />
					<?php echo upperCase(getCompanyAddress()); ?><br />
					For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?><br>
					ACB-<?php echo $bl_currency_name; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
                <tr>
                	<td scope='col'>
<!--START OF INNER TABLE -->
					<table width='100%' border='0' cellspacing='2' cellpadding='2' class="report_printing">
                        <thead>
                            <tr>
                              <th width="10%" scope='col'>&nbsp;</th>
                              <th width="30%" scope='col'>&nbsp;</th>
                              <th width="15%" scope='col'><?php echo $start_date_minus_1; ?></th>
                              <th width="15%" scope='col'><?php echo $end_date; ?></th>
                              <th width="15%" scope='col'>Movement</th>
                              <th width="15%" scope='col'>Movement % </th>
                            </tr>
                        </thead>   
						<tbody>
                        <!-- START OF INCOME -->
                        <?php
						$sql = "SELECT 	report_id,
										report_name
								FROM 	tbl_report_category 
								WHERE 	report_field_id = 0
								AND		report_code = '".$report_code."'
								AND		report_type = '".$report_type_income."'";
						$rs = mysql_query($sql) or die($sql.mysql_error());	
						while($rows = mysql_fetch_array($rs))
						{
							unset($arr_ending_sub_total_income);
							unset($arr_starting_sub_total_income);
							unset($arr_movement_sub_total_income);
							unset($arr_movement_sub_total_income_pct);						
							?>
                            <tr>
                              <td colspan='5' scope='col'><b><?php echo $rows["report_name"]; ?></b></td>
                            </tr>   
                            <?php
							$sql2 = "SELECT report_id,
											report_sl_id,
											report_is_negative
									 FROM	tbl_report_category
									 WHERE	report_field_id = '".$rows["report_id"]."'";
							$rs2 = mysql_query($sql2) or die($sql2.mysql_error());	
							while($rows2 = mysql_fetch_array($rs2))
							{                            
							?>
                                <tr>
                                  <td scope='col'>&nbsp;</td>
                                  <td scope='col'><?php echo getSubsidiaryLedgerAccountTitleById($rows2['report_sl_id']); ?></td>
                                  <td scope='col' align='right'>
									<?php
										if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($rows2['report_sl_id'])==true)
										{
											$starting_income = 0;
										}
										else
										{
											$starting_income = getDebitCreditSumValue($rows2['report_sl_id'],$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
										}									
										if ($rows2["report_is_negative"]==1)
										{
											echo numberFormat(-abs($starting_income));
											$starting_income = -abs($starting_income);
										}
										else
										{
											/*
											echo numberFormat(abs($starting_income));
											$ending_income = abs($starting_income);
											*/
											echo numberFormat($starting_income);
										}
										$arr_starting_income[] = $starting_income;	
										$arr_starting_sub_total_income[] = $starting_income;									
                                    ?>                                  
                                  </td>
                                  <td scope='col' align='right'>
                                  	<?php
										if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($rows2['report_sl_id'])==true)
										{
											$ending_income = getDebitCreditSumValue($rows2['report_sl_id'],$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
										}
										else
										{
											$ending_income = getDebitCreditSumValue($rows2['report_sl_id'],$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
										}									
										if ($rows2["report_is_negative"]==1)
										{
											echo numberFormat(-abs($ending_income));
											$ending_income = -abs($ending_income);
										}
										else
										{
											/*
											echo numberFormat(abs($ending_income));
											$ending_income = abs($ending_income);
											*/
											echo numberFormat($ending_income);
										}
										$arr_ending_income[] = $ending_income;
										$arr_ending_sub_total_income[] = $ending_income;
									?>
                                  </td>
                                  <td scope='col' align='right'>
									<?php
                                    $movement_income = $ending_income-$starting_income;
                                    echo numberFormat($movement_income);
                                    $arr_movement_income[] = $movement_income;
									$arr_movement_sub_total_income[] = $movement_income;
                                    ?>
                                  </td>
                                  <td scope='col' align='right'>
									<?php
                                    if ($starting_income!=0)
                                    {
                                        $movement_income_pct = ($movement_income/$starting_income)*100;
                                    }
                                    else
                                    {
                                        $movement_income_pct = 0;
                                    }
                                    $arr_movement_income_pct[] = $movement_income_pct;
									$arr_movement_sub_total_income_pct[] = $movement_income_pct;
									echo numberFormat($movement_income_pct)." %";
									?>
                                  </td>
                                </tr>
                            <?php
							}
							?>
                            <tr>
                                <td scope='col' align='right'>&nbsp;</td>
                                <td scope='col' align='left'><b>Sub-Total <?php echo $rows['report_name']; ?></b></td>
                                <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_starting_sub_total_income)); ?></b></td>
                                <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_ending_sub_total_income)); ?></b></td>
                                <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_movement_sub_total_income)); ?></b></td>
                                <td scope='col' align='right'>
                                <b>
									<?php
                                    if (array_sum($arr_starting_sub_total_income)!=0)
                                    {
                                        $movement_movement_pct = (array_sum($arr_movement_sub_total_income)/array_sum($arr_starting_sub_total_income))*100;
                                    }
                                    else
                                    {
                                        $movement_movement_pct = 0;
                                    }
                                    echo numberFormat($movement_movement_pct)." %";
                                    ?>                            
                                </b>
                                </td>                                 

                            </tr>                             
                            <?php
						}
						?>   	            
                        <tr>
                          <td colspan='6' scope='col'><hr></td>
                        </tr>
                        <tr>
                          <td colspan='2' scope='col' align='left'><b>Total Assets</b></td>
                          <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_starting_income)); ?></b></td>
                          <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_ending_income)); ?></b></td>
                          <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_movement_income)); ?></b></td>
                          <td scope='col' align='right'>
                          <b>
							<?php
                            if (array_sum($arr_starting_income)!=0)
                            {
                                $movement_movement_pct = (array_sum($arr_movement_income)/array_sum($arr_starting_income))*100;
                            }
                            else
                            {
                                $movement_movement_pct = 0;
                            }
                            echo numberFormat($movement_movement_pct)." %";
                            ?>                            
                          </b>
                          </td>
                        </tr>                             
                        <tr>
                          <td colspan='6' scope='col'><hr></td>
                        </tr>
                        <!-- END OF INCOME -->                        
                        <!-- START OF EXPENSE -->
                        <?php
						$sql = "SELECT 	report_id,
										report_name
								FROM 	tbl_report_category 
								WHERE 	report_field_id = 0
								AND		report_code = '".$report_code."'
								AND		report_type = '".$report_type_expense."'";
						$rs = mysql_query($sql) or die($sql.mysql_error());	
						while($rows = mysql_fetch_array($rs))
						{
							unset($arr_ending_sub_total_expense);
							unset($arr_starting_sub_total_expense);
							unset($arr_movement_sub_total_expense);
							unset($arr_movement_sub_total_expense_pct);
							?>
                            <tr>
                              <td colspan='5' scope='col'><b><?php echo $rows["report_name"]; ?></b></td>
                            </tr>   
                            <?php
							$sql2 = "SELECT report_id,
											report_sl_id,
											report_is_negative
									 FROM	tbl_report_category
									 WHERE	report_field_id = '".$rows["report_id"]."'";
							$rs2 = mysql_query($sql2) or die($sql2.mysql_error());	
							while($rows2 = mysql_fetch_array($rs2))
							{                            

							?>
                                <tr>
                                  <td scope='col'>&nbsp;</td>
                                  <td scope='col'><?php echo getSubsidiaryLedgerAccountTitleById($rows2['report_sl_id']); ?></td>
                                  <td scope='col' align='right'>
									<?php
										if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($rows2['report_sl_id'])==true)
										{
											$starting_expense = 0;
										}
										else
										{
											$starting_expense = getDebitCreditSumValue($rows2['report_sl_id'],$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
										}                                   
										if ($rows2["report_is_negative"]==1)
										{
											echo numberFormat(-abs($starting_expense));
											$starting_expense = -abs($starting_expense);
										}
										else
										{
											/*
											echo numberFormat(abs($starting_expense));
											$starting_expense = abs($starting_expense);
											*/
											echo numberFormat($starting_expense);
										}
										$arr_starting_expense[] = $starting_expense;										
										$arr_starting_sub_total_expense[] = $starting_expense;										
                                    ?>                                  
                                  </td>
                                  <td scope='col' align='right'>
                                  	<?php
										if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($rows2['report_sl_id'])==true)
										{
											$ending_expense = getDebitCreditSumValue($rows2['report_sl_id'],$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
										}
										else
										{
											$ending_expense = getDebitCreditSumValue($rows2['report_sl_id'],$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
										}										
										if ($rows2["report_is_negative"]==1)
										{
											echo numberFormat(-abs($ending_expense));
											$ending_expense = -abs($ending_expense);
										}
										else
										{
											/*
											echo numberFormat(abs($ending_expense));
											$ending_expense = abs($ending_expense);
											*/
											echo numberFormat($ending_expense);
										}
										$arr_ending_expense[] = $ending_expense;
										$arr_ending_sub_total_expense[] = $ending_expense;	
									?>
                                  </td>
                                  <td scope='col' align='right'>
									<?php
                                    $movement_expense = $ending_expense-$starting_expense;
                                    echo numberFormat($movement_expense);
                                    $arr_movement_expense[] = $movement_expense;
									$arr_movement_sub_total_expense[] = $movement_expense;
                                    ?>
                                  </td>
                                  <td scope='col' align='right'>
									<?php
                                    if ($starting_expense!=0)
                                    {
                                        $movement_expense_pct = ($movement_expense/$starting_expense)*100;
                                    }
                                    else
                                    {
                                        $movement_expense_pct = 0;
                                    }
                                    $arr_movement_expense_pct[] = $movement_expense_pct;
									$arr_movement_sub_total_expense_pct[] = $movement_expense_pct;
									echo numberFormat($movement_expense_pct)." %";
									?>
                                  </td>
                                </tr>
                            <?php
							}
							?>
                            <tr>
                                <td scope='col' align='right'>&nbsp;</td>
                                <td scope='col' align='left'><b>Sub-Total <?php echo $rows['report_name']; ?></b></td>
                                <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_starting_sub_total_expense)); ?></b></td>
                                <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_ending_sub_total_expense)); ?></b></td>
                                <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_movement_sub_total_expense)); ?></b></td>
                                <td scope='col' align='right'>
                                <b>
									<?php
                                    if (array_sum($arr_starting_sub_total_expense)!=0)
                                    {
                                        $movement_movement_pct = (array_sum($arr_movement_sub_total_expense)/array_sum($arr_starting_sub_total_expense))*100;
                                    }
                                    else
                                    {
                                        $movement_movement_pct = 0;
                                    }
                                    echo numberFormat($movement_movement_pct)." %";
                                    ?>                            
                                </b>
                                </td>                                 
                            </tr>                              
                            <?php
						}
						?>
                       	<!-- START OF FUND EQUITY END -->
                        <?php
						#START OF INCOME
						$sql = "SELECT 	report_id,
										report_name
								FROM 	tbl_report_category 
								WHERE 	report_field_id = 0
								AND		report_code = '".$report_code_srd."'
								AND		report_type = '".$report_type_income."'";
						$rs = mysql_query($sql) or die($sql.mysql_error());	
						while($rows = mysql_fetch_array($rs))
						{
							$sql2 = "SELECT report_id,
											report_sl_id,
											report_is_negative
									 FROM	tbl_report_category
									 WHERE	report_field_id = '".$rows["report_id"]."'";
							$rs2 = mysql_query($sql2) or die($sql2.mysql_error());	
							while($rows2 = mysql_fetch_array($rs2))
							{     
								$starting_fee_income = getDebitCreditSumValue($rows2['report_sl_id'],$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
                                  
                                if ($rows2["report_is_negative"]==1)
                                {
                                    $starting_fee_income = -abs($starting_fee_income);
                                }
								/*
                                else
                                {
                                    $starting_fee_income = abs($starting_fee_income);
                                }
								*/
                                $arr_starting_fee_income[] = $starting_fee_income;										

								$ending_fee_income = getDebitCreditSumValue($rows2['report_sl_id'],$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);									
								
                                if ($rows2["report_is_negative"]==1)
                                {
                                    $ending_fee_income = -abs($ending_fee_income);
                                }
								/*
                                else
                                {
                                    $ending_fee_income = abs($ending_fee_income);
                                }
								*/
                                $arr_ending_fee_income[] = $ending_fee_income;
                                $arr_ending_sub_total_fee_income[] = $ending_fee_income;	

                                $movement_fee_income = $ending_fee_income-$starting_fee_income;
                                $arr_movement_fee_income[] = $movement_fee_income;

                                if ($starting_fee_income!=0)
                                {
                                    $movement_fee_income_pct = ($movement_fee_income/$starting_fee_income)*100;
                                }
                                else
                                {
                                    $movement_fee_income_pct = 0;
                                }
                                $arr_movement_fee_income_pct[] = $movement_fee_income_pct;
							}
						}						
						#END OF INCOME
						
						#START OF EXPENSE		
						$sql = "SELECT 	report_id,
										report_name
								FROM 	tbl_report_category 
								WHERE 	report_field_id = 0
								AND		report_code = '".$report_code_srd."'
								AND		report_type = '".$report_type_expense."'";
						$rs = mysql_query($sql) or die($sql.mysql_error());	
						while($rows = mysql_fetch_array($rs))
						{
							$sql2 = "SELECT report_id,
											report_sl_id,
											report_is_negative
									 FROM	tbl_report_category
									 WHERE	report_field_id = '".$rows["report_id"]."'";
							$rs2 = mysql_query($sql2) or die($sql2.mysql_error());	
							while($rows2 = mysql_fetch_array($rs2))
							{   
								$starting_fee_expense = getDebitCreditSumValue($rows2['report_sl_id'],$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);
																                              
                                if ($rows2["report_is_negative"]==1)
                                {
                                    $starting_fee_expense = -abs($starting_fee_expense);
                                }
								/*
                                else
                                {
                                    $starting_fee_expense = abs($starting_fee_expense);
                                }
								*/
                                $arr_starting_fee_expense[] = $starting_fee_expense;										

								$ending_fee_expense = getDebitCreditSumValue($rows2['report_sl_id'],$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);									
       
                                if ($rows2["report_is_negative"]==1)
                                {
                                    $ending_fee_expense = -abs($ending_fee_expense);
                                }
								/*
                                else
                                {
                                    $ending_fee_expense = abs($ending_fee_expense);
                                }
								*/
                                $arr_ending_fee_expense[] = $ending_fee_expense;
                                $arr_ending_sub_total_fee_expense[] = $ending_fee_expense;	

                                $movement_fee_expense = $ending_fee_expense-$starting_fee_expense;
                                $arr_movement_fee_expense[] = $movement_fee_expense;

                                if ($starting_fee_expense!=0)
                                {
                                    $movement_fee_expense_pct = ($movement_fee_expense/$starting_fee_expense)*100;
                                }
                                else
                                {
                                    $movement_fee_expense_pct = 0;
                                }
                                $arr_movement_fee_expense_pct[] = $movement_fee_expense_pct;
							}
						}
						#END OF EXPENSE
						$fee_erod_starting = array_sum($arr_starting_fee_expense)+array_sum($arr_starting_fee_income);
						$fee_erod_ending = array_sum($arr_ending_fee_expense)+array_sum($arr_ending_fee_income);
						#$sum_fee_erod_starting_ending = abs($fee_erod_starting) - abs($fee_erod_ending);
						$sum_fee_erod_starting_ending = $fee_erod_starting + $fee_erod_ending;
						$fee_erod_movement = $sum_fee_erod_starting_ending - $fee_erod_starting;
						if ($fee_erod_starting!=0)
						{
							$fee_erod_movement_pct = ($fee_erod_movement/$fee_erod_starting)*100;
						}
						else
						{
							$fee_erod_movement_pct = 0;
						}
						?> 
						<tr>
                          <td scope='col' align='left'><b>Fund Equity End</b></td>
                          <td scope='col' align='left'>&nbsp;</td>
                          <td scope='col' align='right'>
                          <b>
						  <?php echo numberFormat($fee_erod_starting); ?>
                          </b>
                          </td>
                          <td scope='col' align='right'>
                          <b>
						  <?php echo numberFormat($sum_fee_erod_starting_ending); ?>
                          </b>
                          </td>
                          <td scope='col' align='right'>
                          <b>
                          <?php echo numberFormat($fee_erod_ending); ?>
                          </b>
                          </td>
                          <td scope='col' align='right'>
                          <b>
						  <?php echo numberFormat($fee_erod_movement_pct); ?>&nbsp;%
                          </b>
                          </td>
                        </tr>  
                        <!-- END OF FUND EQUITY END -->                       
                        <tr>
                          <td colspan='6' scope='col'><hr></td>
                        </tr>
                        <tr>
                          <td colspan='2' scope='col' align='left'><b>Total Liabilities and Equity</b></td>
                          <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_starting_expense)+$fee_erod_starting); ?></b></td>
                          <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_ending_expense)+$sum_fee_erod_starting_ending); ?></b></td>
                          <td scope='col' align='right'><b><?php echo numberFormat(array_sum($arr_movement_expense)+$fee_erod_movement); ?></b></td>
                          <td scope='col' align='right'>
                          <b>
							<?php
                            if ((array_sum($arr_starting_expense)+$fee_erod_starting)!=0)
                            {
                                $movement_movement_pct = ((array_sum($arr_movement_expense)+$fee_erod_movement)/(array_sum($arr_starting_expense)+$fee_erod_starting))*100;
                            }
                            else
                            {
                                $movement_movement_pct = 0;
                            }
                            echo numberFormat($movement_movement_pct)." %";
                            ?>                            
                          </b>
                          </td>                          
                        </tr>   
                        <tr>
                          <td colspan='6' scope='col'><hr></td>
                        </tr>                                                  
                        <!-- END OF EXPENSE -->                         
                        </tbody>              
                    </table>                           
                    <!--END OF INNER TABLE -->                    
                    </td>
                </tr>
			</table>	
		</td>
	</tr>
</table>

<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>			                        