<?php 
$report_name = "GENERAL LEDGER";
include("./../includes/header.report.php");

$rep_currency_code = $_POST['gl_currency_code'];
$gl_start_year = $_POST['gl_start_year'];
$gl_start_month = $_POST['gl_start_month'];
$gl_start_day = $_POST['gl_start_day'];
$gl_end_year = $_POST['gl_end_year'];
$gl_end_month = $_POST['gl_end_month'];
$gl_end_day = $_POST['gl_end_day'];
$gl_donor_id = $_POST['gl_donor_id'];
$gl_budget_id = $_POST['gl_budget_id'];
$consolidate_reports = $_POST['consolidate_reports'];

$system_start_date = date("Y-m-d", mktime(0,0,0,1,1,2008));
$start_date_minus_1 = date("Y-m-d", mktime(0,0,0,$gl_start_month,$gl_start_day-1,$gl_start_year));

$start_date = date("Y-m-d", mktime(0,0,0,$gl_start_month,$gl_start_day,$gl_start_year));
$end_date = date("Y-m-d", mktime(0,0,0,$gl_end_month,$gl_end_day,$gl_end_year));

$rep_currency_name = getConfigurationValueById($rep_currency_code);
?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> <?php echo upperCase($report_name); ?><br />
					<?php echo upperCase(getCompanyName()); ?><br />
					<?php echo upperCase(getCompanyAddress()); ?><br />
					ACB-<?php echo $rep_currency_name; ?> </th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
					<td scope='col'>
					<table width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#CCCCCC">
						<thead>
						  <tr>
							<th width="9%" scope="col">Transaction Date </th>
							<th width="10%" scope="col">Document Type</th>
							<th width="10%" scope="col">Document Number</th>
							<th width="49%" scope="col" align="left">Particulars/Description</th>
							<th width="6%" scope="col">Debit</th>
							<th width="7%" scope="col">Credit</th>
							<th width="9%" scope="col">Balance</th>
						  </tr>
						</thead>
						<tbody>
						<?php
							$sql = "SELECT		SQL_BUFFER_RESULT
												SQL_CACHE 
												sl_id,
												sl_account_code,
												sl_account_title
									FROM		tbl_subsidiary_ledger
									ORDER BY	sl_account_code ASC";
							$rs = mysql_query(trim($sql)) or die("Error in sql : module : general.ledger.details.php ".$sql." ".mysql_error());
							$total_rows = mysql_num_rows($rs);	
							if($total_rows == 0)
							{
							?>
								<script language="javascript" type="text/javascript">
									alert("No records found!");
									window.close();
								</script>
							<?php
							}
							else
							{
								while($rows = mysql_fetch_array($rs))
								{
									?>
									<tr>
									  <td colspan="6" scope="col"><b>Account Code: <?php echo $rows['sl_account_code']." [".$rows['sl_account_title']."]"; ?></b></td>
									  <td scope="col" align="right">
                                      <b>
									  <?php 
										if ($gl_start_month==1 && $gl_start_day == 1 && checkIfNominalAccount($rows["sl_id"])==true)
										{
											$sl_details_starting_balance = 0;
										}
										else
										{
											$sl_details_starting_balance = getDebitCreditSumValue($rows["sl_id"],$rep_currency_code,$system_start_date,$start_date_minus_1,$budget_id="",$donor_id="",$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_values);	
										}									   
										echo numberFormat($sl_details_starting_balance); 
									  ?>
                                      </b>
                                      </td>
									</tr>
									<?php
									$sql2 = "SELECT SQL_BUFFER_RESULT
													SQL_CACHE 
													gl_id,
													gl_reference_number,
													gl_date,
													gl_report_type,
													gl_report_number,
													gl_description,
													gl_currency_code,
													gl_debit,
													gl_credit
											FROM	tbl_general_ledger
											WHERE	gl_account_code = '".$rows['sl_id']."'
											AND		gl_date
											BETWEEN	'".$start_date."'
											AND		'".$end_date."' ";
									if ($consolidate_reports!=1)
									{
										$sql2 .= "AND	gl_currency_code = '$rep_currency_code' ";
									}
									if ($gl_donor_id!="")
									{
										$sql2 .= "AND	gl_donor_id = '$gl_donor_id' ";
									} 
									if 	($gl_budget_id!="")
									{
										$sql2 .= "AND	gl_budget_id = '$gl_budget_id' ";
									} 			
									$sql2 .= "ORDER BY gl_date,gl_report_type,gl_report_number ASC";					
									$rs2 = mysql_query(trim($sql2)) or die("Error in sql : module : general.ledger.details.php ".$sql2." ".mysql_error());
									$total_rows_1 = mysql_num_rows($rs2);	
									if($total_rows_1 == 0)
									{
									?>
										<tr>
											<td scope="col" colspan="7">&nbsp;</td>
										</tr>
									<?php
									}
									else
									{
										$arr_gl_debit = "";
										$arr_gl_credit = "";										
										while($rows2 = mysql_fetch_array($rs2))
										{
											$gl_reference_number = $rows2['gl_reference_number'];
											$gl_date = $rows2['gl_date'];
											$gl_report_type = $rows2['gl_report_type'];
											$gl_report_number = $rows2['gl_report_number'];
											$gl_description = $rows2['gl_description'];
											$gl_currency_code = $rows2['gl_currency_code'];
											$gl_debit = $rows2['gl_debit'];
											$gl_credit = $rows2['gl_credit'];
											
											$gl_currency_name = getConfigurationValueById($gl_currency_code);	
											
											if ($rep_currency_code != $gl_currency_code && $consolidate_reports == 1)
											{
												$gl_debit = covertExchangeRate($gl_currency_code,$rep_currency_code,$gl_date,$gl_debit);
												$gl_credit = covertExchangeRate($gl_currency_code,$rep_currency_code,$gl_date,$gl_credit);
											}
			
											$arr_gl_debit[] = $gl_debit;
											$arr_gl_credit[] = $gl_credit;											
										?>
										<tr>
											<td scope="col" valign="top"><?php echo $gl_date; ?>&nbsp;</td>
											<td scope="col" valign="top"><?php echo $gl_report_type; ?>&nbsp;</td>
											<td scope="col" valign="top"><?php echo $gl_report_number; ?>&nbsp;</td>
											<td scope="col" valign="top"><?php echo $gl_description; ?>&nbsp;</td>
											<td scope="col" valign="top" align="right"><?php echo numberFormat($gl_debit); ?></td>
											<td scope="col" valign="top" align="right"><?php echo numberFormat($gl_credit); ?></td>
											<td scope="col" valign="top" align="right">
											<?php  
												if ($gl_debit==0 && $gl_credit!=0)
												{
													$balance = $sl_details_starting_balance-$gl_credit;
													$sl_details_starting_balance = $balance;
												}
												else if ($gl_debit!=0 && $gl_credit==0)
												{
													$balance = $sl_details_starting_balance+$gl_debit;
													$sl_details_starting_balance = $balance;
												}
												echo numberFormat($balance);
											?>											
                                            </td>
										</tr>
										<?php
										}
										if ($arr_gl_credit != 0 && $arr_gl_debit != 0)
										{
											$total_debit = array_sum($arr_gl_debit);
											$total_credit = array_sum($arr_gl_credit);
											$arr_grand_total_debit[] = $total_debit;
											$arr_grand_total_credit[] = $total_credit;
										}
										?>
										<tr>
										  <td scope="col" colspan="4" align="right">&nbsp;</td>
										  <td scope="col" align="right"><hr></td>
										  <td scope="col" align="right"><hr></td>
										  <td scope="col" align="right"><hr></td>
						  				</tr>
										<tr>
											<td scope="col" colspan="4" align="right"><b>Total:</b></td>
											<td scope="col" align="right"><b><?php echo numberFormat($total_debit); ?></b></td>
											<td scope="col" align="right"><b><?php echo numberFormat($total_credit); ?></b></td>
											<td scope="col" align="right"><b><?php echo numberFormat($balance); ?></b></td>
										</tr>
										<tr>
											<td scope="col" colspan="7">&nbsp;</td>
										</tr>
										<?php
									}
								}
							}
							?>
						</tbody>
						<tfoot>
							<tr>
					  			<td colspan="4" scope="col">&nbsp;</td>
							    <td scope="col"><hr></td>
							    <td scope="col"><hr></td>
							    <td scope="col">&nbsp;</td>
							</tr>						
							<?php
								if ($arr_gl_credit != 0 && $arr_gl_debit != 0)
								{
									$grand_total_debit = array_sum($arr_grand_total_debit);
									$grand_total_credit = array_sum($arr_grand_total_credit);
								}
							?>
							<tr>
					  			<td colspan="4" scope="col" align="right"><b>Report Totals:</b></td>
							    <td scope="col" align="right"><b><?php echo numberFormat($grand_total_debit); ?></b></td>
							    <td scope="col" align="right"><b><?php echo numberFormat($grand_total_credit); ?></b></td>
							    <td scope="col">&nbsp;</td>
							</tr>
						<tfoot>
                    </table>
				</td>
				</tr>
			</table>	
		</td>
	</tr>
</table>
<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>