<?php 
$report_name = 'Voucher List';
include('./../includes/header.report.php');

$vl_currency_code = $_POST['vl_currency_code'];
$vl_start_year = $_POST['vl_start_year'];
$vl_start_month = $_POST['vl_start_month'];
$vl_start_day = $_POST['vl_start_day'];
$vl_end_year = $_POST['vl_end_year'];
$vl_end_month = $_POST['vl_end_month'];
$vl_end_day = $_POST['vl_end_day'];
$vi_report_voucher = $_POST['vi_report_voucher'];
$use_obligated_value = $_POST['use_obligated_value'];

$start_date = dateFormat($vl_start_month,$vl_start_day,$vl_start_year);
$end_date = dateFormat($vl_end_month,$vl_end_day,$vl_end_year);

switch ($vi_report_voucher)
{
	case "dv":
		$table_main = "tbl_disbursement_voucher a";
		if ($use_obligated_value==1)
		{
			$table_details = "tbl_budget_controller b";		
			$use_sql = 2;	
		}
		else
		{
			$table_details = "tbl_disbursement_voucher_details b";
			$use_sql = 1;
		}
	break;
	
	case "jv":
		$table_main = "tbl_journal_voucher a";
		if ($use_obligated_value==1)
		{
			$table_details = "tbl_budget_controller b";
			$use_sql = 2;	
		}
		else
		{
			$table_details = "tbl_journal_voucher_details b";
			$use_sql = 1;
		}
	break;
	
	case "cv":
		$table_main = "tbl_cash_voucher a";
		if ($use_obligated_value==1)
		{
			$table_details = "tbl_budget_controller b";		
			$use_sql = 2;	
		}
		else
		{
			$table_details = "tbl_cash_voucher_details b";
			$use_sql = 1;
		}
	break;
}
switch($use_sql)
{
	case "1":	
		$sql = "SELECT	SQL_BUFFER_RESULT
						SQL_CACHE
						a.".$vi_report_voucher."_number AS voucher_number,
						a.".$vi_report_voucher."_currency_code AS currency_code,
						a.".$vi_report_voucher."_date AS date,
						a.".$vi_report_voucher."_payee AS payee,
						a.".$vi_report_voucher."_description AS description,
						b.".$vi_report_voucher."_details_account_code AS account_code,
						b.".$vi_report_voucher."_details_debit AS debit,
						b.".$vi_report_voucher."_details_credit AS credit,
						b.".$vi_report_voucher."_details_budget_id AS budget_id,
						b.".$vi_report_voucher."_details_donor_id AS donor_id,
						b.".$vi_report_voucher."_details_component_id AS component_id,
						b.".$vi_report_voucher."_details_activity_id As activity_id,
						b.".$vi_report_voucher."_details_other_cost_id AS cost_id,
						b.".$vi_report_voucher."_details_staff_id AS staff_id,
						b.".$vi_report_voucher."_details_benefits_id AS benefits_id,
						b.".$vi_report_voucher."_details_vehicle_id AS vehicle_id,
						b.".$vi_report_voucher."_details_equipment_id AS equipment_id,
						b.".$vi_report_voucher."_details_item_id AS item_id
				FROM	".$table_main.",
						".$table_details."
				WHERE	a.".$vi_report_voucher."_id = b.".$vi_report_voucher."_details_reference_number
				AND		a.".$vi_report_voucher."_date 
				BETWEEN '".$start_date."' AND '".$end_date."'
				AND		a.".$vi_report_voucher."_year	= '".$vl_start_year."'
				ORDER BY a.".$vi_report_voucher."_sequence_number ASC";
	break;
	
	case "2":
		$sql = "SELECT	SQL_BUFFER_RESULT
						SQL_CACHE
						a.".$vi_report_voucher."_number AS voucher_number,
						a.".$vi_report_voucher."_currency_code AS currency_code,
						a.".$vi_report_voucher."_date AS date,
						a.".$vi_report_voucher."_payee AS payee,
						a.".$vi_report_voucher."_description AS description,
						b.bc_account_code AS account_code,
						b.bc_debit AS debit,
						b.bc_credit AS credit,
						b.bc_budget_id AS budget_id,
						b.bc_donor_id AS donor_id,
						b.bc_component_id AS component_id,
						b.bc_activity_id As activity_id,
						b.bc_other_cost_id AS cost_id,
						b.bc_staff_id AS staff_id,
						b.bc_benefits_id AS benefits_id,
						b.bc_vehicle_id AS vehicle_id,
						b.bc_equipment_id AS equipment_id,
						b.bc_item_id AS item_id
				FROM	".$table_main.",
						".$table_details."
				WHERE	b.bc_voucher_type = '".strtoupper($vi_report_voucher)."'
				AND		a.".$vi_report_voucher."_id = b.bc_voucher_id
				AND		a.".$vi_report_voucher."_date BETWEEN '".$start_date."' AND '".$end_date."'
				AND		a.".$vi_report_voucher."_year = '".$vl_start_year."'
				ORDER BY a.".$vi_report_voucher."_sequence_number ASC";	
	break;
}
?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
                <tr>
                    <th scope='col'> 
                    <?php echo upperCase($report_name); ?><br>
                    <?php echo upperCase(getCompanyName()); ?><br>
                    <?php echo upperCase(getCompanyAddress()); ?><br>
                    For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?><br>
                    Voucher List [<?php echo strtoupper($vi_report_voucher); ?>]
                    </th>
                </tr>
                <tr>
                    <th scope='col'>&nbsp;</th>
                </tr>
                <tr>
                  <td scope='col'>
                  <!-- Start of Report Table -->
                  <table border="1">
					<thead>
                    	<tr>
                            <th>Voucher Number</th>
                            <th>Date</th>
                            <th width="30%">Description</th>
                            <th>Payee</th>
                            <th>Account Code</th>
                            <th>Currency</th>
                            <th>Debit</th>
                            <th>Credit</th>
                            <th>Budget ID </th>
                            <th>Donor ID </th>
                            <th>Component ID </th>
                            <th>Activity ID </th>
                            <th>Cost ID</th>
                            <th>Staff ID </th>
                            <th>Benefits ID </th>
                            <th>Vehicle ID </th>
                            <th>Equipment ID </th>
                            <th>Item Code </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
						$rs = mysql_query($sql) or die($sql."<br><br>".mysql_error());
						$total_rows = mysql_num_rows($rs);	
						if($total_rows == 0)
						{
						?>
							<script language="JavaScript" type="text/javascript">
								alert("No records found!");
								window.close();
							</script>
						<?php
						}
						else
						{	
							while($rows=mysql_fetch_array($rs))
							{
								?>
                                <tr>
                                    <td valign="top"><?php echo $rows["voucher_number"]; ?></td>
                                    <td valign="top"><?php echo $rows["date"]; ?></td>
                                    <td valign="top"><?php echo $rows["description"]; ?></td>
                                    <td valign="top"><?php echo getPayeeById($rows["payee"]); ?></td>
                                    <td valign="top"><?php echo getSubsidiaryLedgerAccountTitleById($rows["account_code"]); ?></td>
                                    <td valign="top"><?php echo getConfigurationValueById($rows["currency_code"]); ?></td>
                                    <td valign="top" align="right"><?php echo numberFormat($rows["debit"]); ?></td>
                                    <td valign="top" align="right"><?php echo numberFormat($rows["credit"]); ?></td>
                                    <td valign="top"><?php echo getBudgetName($rows["budget_id"]); ?></td>
                                    <td valign="top"><?php echo getDonorName($rows["donor_id"]); ?></td>
                                    <td valign="top"><?php echo getComponentName($rows["component_id"]); ?></td>
                                    <td valign="top"><?php echo getActivityName($rows["activity_id"]) ; ?></td>
                                    <td valign="top"><?php echo getOtherCostName($rows["cost_id"]); ?></td>
                                    <td valign="top"><?php echo getStaffName($rows["staff_id"]); ?></td>
                                    <td valign="top"><?php echo getBenefitsName($rows["benefits_id"]); ?></td>
                                    <td valign="top"><?php echo getVehicleName($rows["vehicle_id"]) ; ?></td>
                                    <td valign="top"><?php echo getEquipmentName($rows["equipment_id"]); ?></td> 
                                    <td valign="top"><?php echo getItemName($rows["item_id"]); ?></td>   
                                </tr>
								<?php
							}
						}
					?>
                    </tbody>
                    <tfoot>
                    </tfoot>                  	
                  </table>
                  <!-- End of Report Table -->				  
                  </td>
                </tr>
			</table>	
		</td>
	</tr>
</table>
<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to 'File' -> 'Page Setup...' then erase the text in the 'Headers and Footers' field.
	</em>
</div>
