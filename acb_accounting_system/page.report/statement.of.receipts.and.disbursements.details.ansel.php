<?php
$report_name = 'STATEMENT OF RECEIPTS AND DISBURSEMENTS';
if($_SESSION["logged"]==1)
{ 
	include("./../connections/cn.php");
}
include("./../includes/check.login.php");
include("./../includes/function.php");
include("./../includes/http.function.php"); 
include("./../includes/timer.includes.php"); 
require_once('ansel/fpdf.php');
$report_code = 55;
$report_type_income = 54;
$report_type_expense = 53;

$srd_currency_code = $_POST['srd_currency_code'];
$srd_start_year = $_POST['srd_start_year'];
$srd_start_month = $_POST['srd_start_month'];
$srd_start_day = $_POST['srd_start_day'];
$srd_end_year = $_POST['srd_end_year'];
$srd_end_month = $_POST['srd_end_month'];
$srd_end_day = $_POST['srd_end_day'];
$srd_budget_id = $_POST['srd_budget_id'];
$srd_donor_id = $_POST['srd_donor_id'];
$consolidate_reports = $_POST['consolidate_reports'];
$use_obligated_value = $_POST['use_obligated_value'];

$system_start_date = date("Y-m-d", mktime(0,0,0,1,1,$srd_start_year));
$start_date_minus_1 = date("Y-m-d", mktime(0,0,0,$srd_start_month,$srd_start_day-1,$srd_start_year));

$start_date = date("Y-m-d", mktime(0,0,0,$srd_start_month,$srd_start_day,$srd_start_year));
$end_date = date("Y-m-d", mktime(0,0,0,$srd_end_month,$srd_end_day,$srd_end_year));
$srd_currency_name = getConfigurationValueById($srd_currency_code);

$dump = "_,4,C,$start_date_minus_1,2,C,$end_date,2,C,Movement,2,C,Movement %,2,C,";

class PDF extends FPDF {
	public $defheight = 7; 
	public $arr; 
	public $cnt;
	
	function Header()
	{
		global $dump, $start_date, $end_date, $srd_currency_name;
		
		$this->SetFont('Arial','',5);
		$this->Cell(195,3,'Page '.$this->PageNo().' of {nb}',0,1,'R'); 
		$this->SetFont('Arial','',9);
		$this->Cell(195,5,"STATEMENT OF RECEIPTS AND DISBURSEMENTS",0,1,"C");
		$this->SetFont('Arial','B',9);
		$this->Cell(195,5,"ASEAN CENTRE FOR BIODIVERSITY ",0,1,"C");
		$this->Cell(195,5,"3F ERDB BUILDING, FORESTRY CAMPUS, COLLEGE, LAGUNA 4031 PHILIPPINES",0,1,"C");
		$this->SetFont('Arial','',8);
		$this->Cell(195,5,"For the period of $start_date to $end_date",0,1,"C");
		$this->Cell(195,5,"ACB - $srd_currency_name",0,1,"C");
		
		parent::arrayDump($dump,'P');
		$this->Ln(8);
	}
}

$pdf=new PDF('P');
$pdf->SetTitle("Master List");
$pdf->AliasNbPages();
$pdf->SetLeftMargin('8');
$pdf->AddPage();
$pdf->SetFont('Arial','',7);


						$sql = "SELECT 	report_id,
										report_name
								FROM 	tbl_report_category 
								WHERE 	report_field_id = 0
								AND		report_code = '".$report_code."'
								AND		report_type = '".$report_type_income."'";
						$rs = mysql_query($sql) or die($sql.mysql_error());	
						while($rows = mysql_fetch_array($rs))
						{
							unset($arr_ending_sub_total_income);
							unset($arr_starting_sub_total_income);
							unset($arr_movement_sub_total_income);
							unset($arr_movement_sub_total_income_pct);						
							
							$pdf->SetFont('Arial','B',8);
                            $pdf->Cell($pdf->arr[0][4],5,$rows["report_name"],0,1,"L");
                            $pdf->SetFont('Arial','',8);
							$sql2 = "SELECT report_id,
											report_sl_id,
											report_is_negative
									 FROM	tbl_report_category
									 WHERE	report_field_id = '".$rows["report_id"]."'";
							$rs2 = mysql_query($sql2) or die($sql2.mysql_error());	
							while($rows2 = mysql_fetch_array($rs2))
							{                            
								
                               $pdf->Cell($pdf->arr[0][4],5,"						".getSubsidiaryLedgerAccountTitleById($rows2['report_sl_id']),0,0,"L");
                                  
										if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($rows2['report_sl_id'])==true)
										{
											$starting_income = 0;
										}
										else
										{
											$starting_income = getDebitCreditSumValue($rows2['report_sl_id'],$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
										}                                    
										if ($rows2["report_is_negative"]==1)
										{
											$pdf->Cell($pdf->arr[1][4],5,numberFormat(-abs($starting_income)),0,0,"R");
											$starting_income = -abs($starting_income);
										}
										else
										{
											/*
											echo numberFormat(abs($starting_income));
											$starting_income = abs($starting_income);
											*/
											$pdf->Cell($pdf->arr[1][4],5,numberFormat($starting_income),0,0,"R");
										}
										$arr_starting_income[] = $starting_income;	
										$arr_starting_sub_total_income[] = $starting_income;									
                                 
										$ending_income = getDebitCreditSumValue($rows2['report_sl_id'],$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);                   
										if ($rows2["report_is_negative"]==1)
										{
											$pdf->Cell($pdf->arr[1][4],5,numberFormat(-abs($ending_income)),0,0,"R");
											$ending_income = -abs($ending_income);
										}
										else
										{
											/*
											echo numberFormat(abs($ending_income));
											$ending_income = abs($ending_income);
											*/
											$pdf->Cell($pdf->arr[1][4],5,numberFormat($ending_income),0,0,"R");
											
										}
										$arr_ending_income[] = $ending_income;
										$arr_ending_sub_total_income[] = $ending_income;
									
                                    $movement_income = $ending_income-$starting_income;
									$pdf->Cell($pdf->arr[1][4],5,numberFormat($movement_income),0,0,"R");
                                   
                                    $arr_movement_income[] = $movement_income;
									$arr_movement_sub_total_income[] = $movement_income;
                                    
                                    if ($starting_income!=0)
                                    {
                                        $movement_income_pct = ($movement_income/$starting_income)*100;
                                    }
                                    else
                                    {
                                        $movement_income_pct = 0;
                                    }
                                    $arr_movement_income_pct[] = $movement_income_pct;
									$arr_movement_sub_total_income_pct[] = $movement_income_pct;
									
									$pdf->Cell($pdf->arr[1][4],5,numberFormat($movement_income_pct)." %",0,1,"R");
																		
							}
								$pdf->SetFont('Arial','B',9);
								$pdf->Cell($pdf->arr[0][4],5,"					Sub-total".$rows['report_name'],0,0,"L");
								$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_starting_sub_total_income)),0,0,"R");
								$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_ending_sub_total_income)),0,0,"R");
								$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_movement_sub_total_income)),0,0,"R");
                          
                                    if (array_sum($arr_starting_sub_total_income)!=0)
                                    {
                                        $movement_movement_pct = (array_sum($arr_movement_sub_total_income)/array_sum($arr_starting_sub_total_income))*100;
                                    }
                                    else
                                    {
                                        $movement_movement_pct = 0;
                                    }
									$pdf->Cell($pdf->arr[1][4],5,numberFormat($movement_movement_pct)." %",0,1,"R");
									$pdf->Ln(5);
                                   
                            
						}
						
						$pdf->Cell($pdf->arr[0][4],5,'',0,0,"L");
						$pdf->Cell($pdf->arr[1][4],5,'','T',0,"R");
						$pdf->Cell($pdf->arr[1][4],5,'','T',0,"R");
						$pdf->Cell($pdf->arr[1][4],5,'','T',0,"R");
						$pdf->Cell($pdf->arr[1][4],5,'','T',1,"R");
                        
											
						$pdf->Cell($pdf->arr[0][4],5,'Total Receipts',0,0,"L");
						$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_starting_income)),0,0,"R");
						$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_ending_income)),0,0,"R");
						$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_movement_income)),0,0,"R");
						$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_movement_income_pct))." %",0,1,"R");
						
             			$pdf->Ln(5);
						$sql = "SELECT 	report_id,
										report_name
								FROM 	tbl_report_category 
								WHERE 	report_field_id = 0
								AND		report_code = '".$report_code."'
								AND		report_type = '".$report_type_expense."'";
						$rs = mysql_query($sql) or die($sql.mysql_error());	
						while($rows = mysql_fetch_array($rs))
						{
							unset($arr_ending_sub_total_expense);
							unset($arr_starting_sub_total_expense);
							unset($arr_movement_sub_total_expense);
							unset($arr_movement_sub_total_expense_pct);
							
                            $pdf->SetFont('Arial','B',9);
                            $pdf->Cell($pdf->arr[0][4],5,$rows["report_name"],0,1,"L");
                            $pdf->SetFont('Arial','',8);
                            
                            $sql2 = "SELECT report_id,
											report_sl_id,
											report_is_negative
									 FROM	tbl_report_category
									 WHERE	report_field_id = '".$rows["report_id"]."'";
							$rs2 = mysql_query($sql2) or die($sql2.mysql_error());	
							while($rows2 = mysql_fetch_array($rs2))
							{                            

							 $pdf->Cell($pdf->arr[0][4],5,"						".getSubsidiaryLedgerAccountTitleById($rows2['report_sl_id']),0,0,"L");
                                  
										if ($srd_start_month==1 && $srd_start_day == 1 && checkIfNominalAccount($rows2['report_sl_id'])==true)
										{
											$starting_expense = 0;
										}
										else
										{
											$starting_expense = getDebitCreditSumValue($rows2['report_sl_id'],$srd_currency_code,$system_start_date,$start_date_minus_1,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
										}                                    
										if ($rows2["report_is_negative"]==1)
										{
											$pdf->Cell($pdf->arr[1][4],5,numberFormat(-abs($starting_expense)),0,0,"R");
											
											$starting_expense = -abs($starting_expense);
										}
										else
										{
											/*
											echo numberFormat(abs($starting_expense));
											$starting_expense = abs($starting_expense);
											*/
											$pdf->Cell($pdf->arr[1][4],5,numberFormat($starting_expense),0,0,"R");
											
										}
										$arr_starting_expense[] = $starting_expense;										
										$arr_starting_sub_total_expense[] = $starting_expense;										
                                    
										$ending_expense = getDebitCreditSumValue($rows2['report_sl_id'],$srd_currency_code,$system_start_date,$end_date,$srd_budget_id,$srd_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);                   
										if ($rows2["report_is_negative"]==1)
										{
											$pdf->Cell($pdf->arr[1][4],5,numberFormat(-abs($ending_expense)),0,0,"R");
											
											$ending_expense = -abs($ending_expense);
										}
										else
										{
											/*
											echo numberFormat(abs($ending_expense));
											$ending_expense = abs($ending_expense);
											*/
											$pdf->Cell($pdf->arr[1][4],5,numberFormat($ending_expense),0,0,"R");
											
										}
										$arr_ending_expense[] = $ending_expense;
										$arr_ending_sub_total_expense[] = $ending_expense;	
									
                                    $movement_expense = $ending_expense-$starting_expense;
									$pdf->Cell($pdf->arr[1][4],5,numberFormat($movement_expense),0,0,"R");
                                   
                                    $arr_movement_expense[] = $movement_expense;
									$arr_movement_sub_total_expense[] = $movement_expense;
                                    
                                    if ($starting_expense!=0)
                                    {
                                        $movement_expense_pct = ($movement_expense/$starting_expense)*100;
                                    }
                                    else
                                    {
                                        $movement_expense_pct = 0;
                                    }
                                    $arr_movement_expense_pct[] = $movement_expense_pct;
									$arr_movement_sub_total_expense_pct[] = $movement_expense_pct;
									$pdf->Cell($pdf->arr[1][4],5,numberFormat($movement_expense_pct)." %",0,1,"R");
									
								
							}
							
								$pdf->SetFont('Arial','B',9);
								$pdf->Cell($pdf->arr[0][4],5,"					Sub-total".$rows['report_name'],0,0,"L");
								$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_starting_sub_total_expense)),0,0,"R");
								$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_ending_sub_total_expense)),0,0,"R");
								$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_movement_sub_total_expense)),0,0,"R");
								
                                    if (array_sum($arr_starting_sub_total_expense)!=0)
                                    {
                                        $movement_movement_pct = (array_sum($arr_movement_sub_total_expense)/array_sum($arr_starting_sub_total_expense))*100;
                                    }
                                    else
                                    {
                                        $movement_movement_pct = 0;
                                    }
									$pdf->Cell($pdf->arr[1][4],5,numberFormat($movement_movement_pct)." %",0,1,"R");
									$pdf->Ln(5);
                                    
						}
						
						$pdf->Cell($pdf->arr[0][4],5,'',0,0,"L");
						$pdf->Cell($pdf->arr[1][4],5,'','T',0,"R");
						$pdf->Cell($pdf->arr[1][4],5,'','T',0,"R");
						$pdf->Cell($pdf->arr[1][4],5,'','T',0,"R");
						$pdf->Cell($pdf->arr[1][4],5,'','T',1,"R");
                        
											
						$pdf->Cell($pdf->arr[0][4],5,'Total Disbursements',0,0,"L");
						$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_starting_expense)),0,0,"R");
						$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_ending_expense)),0,0,"R");
						$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_movement_expense)),0,0,"R");
						$pdf->Cell($pdf->arr[1][4],5,numberFormat(array_sum($arr_movement_expense_pct))." %",0,1,"R");
						
                        
                        $pdf->Cell($pdf->arr[0][4],5,'Excess of Receipts over Disbursements',0,0,"L");
                          
						  $erod_starting = array_sum($arr_starting_expense)+array_sum($arr_starting_income);
						  $pdf->Cell($pdf->arr[1][4],5,numberFormat($erod_starting),0,0,"R");
						  					  
						  $erod_ending = array_sum($arr_ending_expense)+array_sum($arr_ending_income);
						  $pdf->Cell($pdf->arr[1][4],5,numberFormat($erod_ending),0,0,"R");
						
						  $erod_movement = $erod_ending-$erod_starting;
						  $pdf->Cell($pdf->arr[1][4],5,numberFormat($erod_movement),0,0,"R");
						  
                            if ($erod_starting!=0)
                            {
                                $movement_erod_pct = ($erod_movement/$erod_starting)*100;
                            }
                            else
                            {
                                $movement_erod_pct = 0;
                            }
							$pdf->Cell($pdf->arr[1][4],5,numberFormat($movement_erod_pct)." %",0,1,"R");
                         
                        

$pdf->Output();
?>
