<?php 
session_start();
include("./../includes/header.main.php");

$user_id = $_SESSION["id"];

$months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$months_count = count($months);
$year = date("Y");
$month = date("m");
$day = date("d");
$startYear = getProgramStartYear();
$endYear = date("Y");
$date_time_inserted = date("Y-m-d H:i:s");

?>
<?php include("./../includes/menu.php"); ?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="main">
	<tr>
		<td>
		<form action="/workspaceGOP/page.report/account.inquiry.details.php" method="post" name="form1" target="formtarget" onSubmit="MM_openBrWindow('about:blank','formtarget','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1024,height=700')">
				<table id="rounded-add-entries" align="center">
					<thead>
						<tr>
							<th scope="col" class="rounded-header"><div class="main" align="left"><strong>Account Inquiry </strong></div></th>
							<th scope="col" class="rounded-q4"><div class="main" align="right"><strong>*Required Fields</strong></div></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td class="rounded-foot-left" align="left">&nbsp;</td>
							<td class="rounded-foot-right" align="right"><input type="submit" name="Submit" value="Generate Report" class="formbutton"></td>
						</tr>
					</tfoot>
					<tbody>
					<?php
					if ($display_msg==1)
					{
					?>
						<tr>
							 <td colspan="2"><div align="center" class="redlabel"><?php echo $msg; ?></div></td>
						</tr>
						<?php
					}
					?>
						<tr>
							<td width="20%"><b>Start Date* </b></td>
							<td width="80%">
							<select name='ai_start_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='ai_start_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='ai_start_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>							</td>
						</tr>
						<tr valign="top">
						  <td><b>End Date*</b></td>
						  <td>
							<select name='ai_end_year' class="formbutton">
								<?php
								for ($yr=$startYear; $yr<=$endYear; $yr++)
								{
									?>
									<option value='<?php echo $yr; ?>'
									<?php
									if ($yr == $year)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $yr; ?>
								<?php
								}
								?>
									</option>
							</select>
							<select name='ai_end_month' class="formbutton">
								<?php
								for ($mo=1; $mo<=count($months); $mo++)
								{
								?>
									<option value='<?php echo $mo; ?>'
									<?php
									if ($mo == $month)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $months[$mo-1]; ?>
									<?php
								}
								?>
									</option>
							</select>
							<select name='ai_end_day' class="formbutton">
								<?php
								for ($dy=1; $dy<=31; $dy++)
								{
									?>
									<option value='<?php echo $dy; ?>'
									<?php
									if ($dy == $day)
									{
									?>
										selected="selected"
									<?
									}
									?>
									><?php echo $dy; ?>
									<?php
								}
								?>
									</option>
							</select>							</td>
					  </tr>
						<tr valign="top">
						  <td><b>Account Code*</b></td>
						  <td>
								  <?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													sl_id,
													sl_account_code,
													sl_account_title
											FROM 	tbl_subsidiary_ledger
											ORDER BY sl_account_code ASC";
									$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
									?>
									<select name='ai_details_account_code' class='formbutton'>
									<?php
										while($data=mysql_fetch_array($rs))
										{
											$column_id = $data['sl_id'];
											$column_name = $data['sl_account_title'];
											?>
											<option value='<?php echo $column_id; ?>'
											<?php
											if ($column_id==$ai_details_account_code_select)
											{
											?>
												SELECTED
											<?php
											}
											?>
											><?php echo $column_name." [".$data['sl_account_code']."]"; ?></option>
                                            <?php 
										}
										?>
									</select>						  </td>
					  </tr>
						<tr valign="top">
						  <td><b>Report Currency*</b></td>
						  <td>
								<?php
									$sql = "SELECT	SQL_BUFFER_RESULT
													SQL_CACHE
													cfg_id,
													cfg_value
											FROM 	tbl_config
											WHERE 	cfg_name = 'currency_type'";
									$rs = mysql_query($sql) or die('Error ' . mysql_error()); 
									$str .= "<select name='ai_currency_code' class='formbutton'>";
									while($data=mysql_fetch_array($rs))
									{
										$column_id = $data['cfg_id'];
										$column_name = $data['cfg_value'];
										$str .= "<option value='$column_id'";
										if ($ai_currency_code == $column_id)
										{
											$str .= " SELECTED";
										}
										$str .= ">".$column_name."";
									}
									$str .= "</select>&nbsp;";
									echo $str;
								?>						</td>
					  </tr>
						<tr valign="top">
						  <td><b>Budget ID*</b></td>
						  <td>
								<?php
								$sql = "SELECT	SQL_BUFFER_RESULT
												SQL_CACHE
												budget_id,
												budget_name,
												budget_description
										FROM 	tbl_budget
										ORDER BY budget_name";
								$rs = mysql_query($sql) or die('Error ' . mysql_error());
								?>
								<select name='ai_budget_id' class='formbutton'>
								<option value='' SELECTED>[Please Select]</option>
								<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['budget_id'];
									$column_name = $data['budget_name'];
									$column_description = $data['budget_description'];
									?>
									<option value='<?php echo $column_id; ?>'
									<?php
									if ($cv_details_budget_id == $column_id)
									{
										?>
										SELECTED
									<?php
									}
									?>
									><?php echo $column_name;
								}
								?> </option>
								</select>							</td>
					  	</tr>
						<tr valign="top">
						  	<td><b>Donor ID*</b></td>
							<td>
								<?php
								$sql = "SELECT	SQL_BUFFER_RESULT
												SQL_CACHE
												donor_id,
												donor_name,
												donor_description
										FROM 	tbl_donor
										ORDER BY donor_name";
								$rs = mysql_query($sql) or die('Error ' . mysql_error());
								?>
								<select name='ai_donor_id' class='formbutton'>
								<option value='' SELECTED>[Please Select]</option>
								<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['donor_id'];
									$column_name = $data['donor_name'];
									$column_description = $data['donor_description'];
									?>
									<option value='<?php echo $column_id; ?>'
									<?php
									if ($cv_details_donor_id == $column_id)
									{
									?>
										SELECTED
									<?php
									}
									?>
									><?php echo $column_name;
								}
								?> </option>
								</select>							</td>
					  	</tr>	
						<tr valign="top">
						  <td><b>Staff ID *</b></td>
						  <td>
							<?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											staff_id,
											staff_name,
											staff_description
									FROM 	tbl_staff
									ORDER BY staff_description";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?>
							<select name='ai_staff_id' class='formbutton'>
							<option value='' SELECTED>[Please Select]</option>
							<?php
							while($data=mysql_fetch_array($rs))
							{
								$column_id = $data['staff_id'];
								$column_name = $data['staff_name'];
								$column_description = $data['staff_description'];
								?>
								<option value='<?php echo $column_id; ?>'
								<?php
								if ($ai_staff_id == $column_id)
								{
								?>
									SELECTED
								<?php
								}
								?>
								><?php echo $column_description;
							}
							?> </option>
							</select>							</td>
					   </tr>	
						<tr valign="top">
						  <td><b>Other Cost/Services ID *</b></td>
						  <td>
						  <?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											cs_id,
											cs_name,
											cs_description
									FROM 	tbl_other_cost_services
									ORDER BY cs_name";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?>
							<select name='ai_other_cost_id' class='formbutton'>
							<option value='' SELECTED>[Please Select]</option>
							<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['cs_id'];
									$column_name = $data['cs_name'];
									$column_description = $data['cs_description'];
									?>
									<option value='<?php echo $column_id; ?>'
									<?php
									if ($cv_details_cs_id == $column_id)
									{
									?>
										SELECTED
									<?php
									}
									?>
									><?php echo $column_description;
								}
								?> </option>
							</select>						  </td>
					  </tr>
						<tr valign="top">
						  <td><b>Benefits ID *</b></td>
						  <td>
						  <?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											benefits_id,
											benefits_name,
											benefits_description
									FROM 	tbl_benefits
									ORDER BY benefits_description";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?>
							<select name='ai_benefits_id' class='formbutton'>
							<option value='' SELECTED>[Please Select]</option>
							<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['benefits_id'];
									$column_name = $data['benefits_name'];
									$column_description = $data['benefits_description'];
									?>
									<option value='<?php echo $column_id; ?>'
									<?php
									if ($cv_details_benefits_id == $column_id)
									{
									?>
										SELECTED
									<?php
									}
									?>
									><?php echo $column_description;
								}
								?> </option>
							</select>						  </td>
					  </tr>
						<tr valign="top">
						  <td><b>Vehicle ID *</b></td>
						  <td>
						  <?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											vehicle_id,
											vehicle_name,
											vehicle_description
									FROM 	tbl_vehicle
									ORDER BY vehicle_description";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?>
							<select name='ai_vehicle_id' class='formbutton'>
							<option value='' SELECTED>[Please Select]</option>
							<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['vehicle_id'];
									$column_name = $data['vehicle_name'];
									$column_description = $data['vehicle_description'];
									?>
									<option value='<?php echo $column_id; ?>'
									<?php
									if ($cv_details_vehicle_id == $column_id)
									{
									?>
										SELECTED
									<?php
									}
									?>
									><?php echo $column_description;
								}
								?> </option>
							</select></td>
					  </tr>
						<tr valign="top">
						  <td><b>Equipment ID *</b></td>
						  <td>
						  <?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											equipment_id,
											equipment_name,
											equipment_description
									FROM 	tbl_equipment
									ORDER BY equipment_name";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?>
							<select name='ai_equipment_id' class='formbutton'>
							<option value='' SELECTED>[Please Select]</option>
							<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['equipment_id'];
									$column_name = $data['equipment_name'];
									$column_description = $data['equipment_description'];
									?>
									<option value='<?php echo $column_id; ?>'
									<?php
									if ($cv_details_equipment_id == $column_id)
									{
									?>
										SELECTED
									<?php
									}
									?>
									><?php echo $column_description;
								}
								?> </option>
							</select>
                            </td>
					  </tr>
						<tr valign="top">
						  <td><b>Item ID *</b></td>
						  <td>
						  <?php
							$sql = "SELECT	SQL_BUFFER_RESULT
											SQL_CACHE
											item_id,
											item_name,
											item_description
									FROM 	tbl_item_code
									ORDER BY item_name";
							$rs = mysql_query($sql) or die('Error ' . mysql_error());
							?>
							<select name='ai_item_id' class='formbutton'>
							<option value='' SELECTED>[Please Select]</option>
							<?php
								while($data=mysql_fetch_array($rs))
								{
									$column_id = $data['item_id'];
									$column_name = $data['item_name'];
									$column_description = $data['item_description'];
									?>
									<option value='<?php echo $column_id; ?>'
									<?php
									if ($cv_details_item_id == $column_id)
									{
									?>
										SELECTED
									<?php
									}
									?>
									><?php echo $column_description;
								}
								?> </option>
							</select>                          
                          </td>
					  </tr>
                    <tr valign="top">
                        <td><b>Consolidate Reports *</b></td>
                        <td><input name="consolidate_reports" type="checkbox" value="1"></td>
                    </tr>	
                    <tr valign="top">
                        <td><b>Use Obligated Value *</b></td>
                        <td><input name="use_obligated_value" type="checkbox" value="1"></td>
                    </tr>	                      										  																																
					</tbody>
				</table>
		  </form>
		</td>
	</tr>
</table>
<?php include("./../includes/footer.main.php"); ?>