<?php 
$report_name = "TELEGRAPHIC TRANSFER LOG";
include("./../includes/header.report.php");

$tt_start_year = $_POST['tt_start_year'];
$tt_start_month = $_POST['tt_start_month'];
$tt_start_day = $_POST['tt_start_day'];
$tt_end_year = $_POST['tt_end_year'];
$tt_end_month = $_POST['tt_end_month'];
$tt_end_day = $_POST['tt_end_day'];

$start_date = dateFormat($tt_start_month,$tt_start_day,$tt_start_year);
$end_date = dateFormat($tt_end_month,$tt_end_day,$tt_end_year);

?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br />
					<?php echo upperCase(getCompanyName()); ?>
					<br />
					<?php echo upperCase(getCompanyAddress()); ?><br />
					For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?><br>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
				  <td scope='col'>
				  <!--Start of inner table -->
					<table width="100%" border="1" cellpadding="2" cellspacing="0">
						<thead>
							<tr>
								<th width="6%">Date</th>
								<th width="11%">Telegraphic Transfer No.</th>
								<th width="6%">Country</th>
								<th width="9%">Payee</th>
								<th width="10%">Account No.</th>
								<th width="10%">DV No.</th>
								<th width="17%">Details/Purpose</th>
								<th width="10%">Amount Requested</th>
								<th width="6%">Currency Requested</th>
								<th width="10%">Amount Transferred (in euro)</th>
								<th width="5%">Bank Charges (in euro)</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$sql = "SELECT 	SQL_BUFFER_RESULT
										SQL_CACHE
										tt_number,
										tt_date,
										tt_country,
										tt_bank_from, 
										tt_bank_to,
										tt_bank_to_address,
										tt_bank_to_account_number,
										tt_currency,
										tt_amount,
										tt_swift_code,
										tt_iban,
										tt_beneficiary,
										tt_purpose,
										tt_dv_number,
										tt_amount_transferred,
										tt_bank_charges
								FROM	tbl_telegraphic_transfer
								WHERE	tt_date
								BETWEEN	'$start_date'
								AND		'$end_date'
								ORDER BY tt_date";
						$rs = mysql_query($sql) or die("Query Error" .mysql_error());
						$total_rows = mysql_num_rows($rs);	
						if($total_rows == 0)
						{
						?>
							<script language="JavaScript" type="text/javascript">
								alert("No records found!");
								window.close();
							</script>
						<?php
						}
						else
						{
							while($rows=mysql_fetch_array($rs))
							{
								$tt_number = $rows["tt_number"];
								$tt_date = $rows["tt_date"];
								$tt_country = $rows["tt_country"];
								$tt_bank_from  = $rows["tt_bank_from"];
								$tt_bank_to = $rows["tt_bank_to"];
								$tt_bank_to_address = $rows["tt_bank_to_address"];
								$tt_bank_to_account_number = $rows["tt_bank_to_account_number"];
								$tt_currency = $rows["tt_currency"];
								$tt_amount = $rows["tt_amount"];
								$tt_swift_code = $rows["tt_swift_code"];
								$tt_iban = $rows["tt_iban"];
								$tt_beneficiary = $rows["tt_beneficiary"];
								$tt_purpose = $rows["tt_purpose"];
								$tt_dv_number = $rows["tt_dv_number"];
								$tt_amount_transferred = $rows["tt_amount_transferred"];
								$tt_bank_charges = $rows["tt_bank_charges"];
								
								$tt_country = getConfigurationValueById($tt_country);
								$tt_bank_from = getAccountNumber($tt_bank_from);
								$tt_currency = getConfigurationValueById($tt_currency);
								$tt_amount = numberFormat($tt_amount);
								$tt_amount_transferred = numberFormat($tt_amount_transferred);
								$tt_bank_charges = numberFormat($tt_bank_charges);
								
								$tt_purpose = stripslashes($tt_purpose);							
							?>
								<tr>
									<td valign="top"><?php echo $tt_date; ?></td>
									<td valign="top"><?php echo $tt_number; ?></td>
									<td valign="top"><?php echo $tt_country; ?></td>
									<td valign="top"><?php echo $tt_beneficiary; ?></td>
									<td valign="top"><?php echo $tt_bank_to; ?></td>
									<td valign="top"><?php echo $tt_dv_number; ?></td>
									<td valign="top"><?php echo $tt_purpose; ?></td>
									<td valign="top" align="right"><?php echo $tt_amount; ?></td>
									<td valign="top"><?php echo $tt_currency; ?></td>
									<td valign="top" align="right"><?php echo $tt_amount_transferred; ?></td>
									<td valign="top" align="right"><?php echo $tt_bank_charges; ?></td>
								</tr>	
							<?php
							}
						}
						?>					
						</tbody>
					</table>

				  <!--End of inner table -->
				  </td>
				</tr>
			</table>	
		</td>
	</tr>
</table>


<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a></div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>



