<?php 
$report_name = "BALANCE SHEET";
include("./../includes/header.report.php");

$bl_currency_code = $_POST['bl_currency_code'];
$bl_start_year = $_POST['bl_start_year'];
$bl_start_month = $_POST['bl_start_month'];
$bl_start_day = $_POST['bl_start_day'];
$bl_end_year = $_POST['bl_end_year'];
$bl_end_month = $_POST['bl_end_month'];
$bl_end_day = $_POST['bl_end_day'];
$bl_budget_id = $_POST['bl_budget_id'];
$bl_donor_id = $_POST['bl_donor_id'];
$consolidate_reports = $_POST['consolidate_reports'];
$use_obligated_value = $_POST['use_obligated_value'];

$system_start_date = date("Y-m-d", mktime(0,0,0,1,1,2008));
$selected_start_date_year = date("Y-m-d", mktime(0,0,0,1,1,$bl_start_year));
$start_date_minus_1 = date("Y-m-d", mktime(0,0,0,$bl_start_month,$bl_start_day-1, $bl_start_year));

$start_date = date("Y-m-d", mktime(0,0,0,$bl_start_month,$bl_start_day, $bl_start_year));
$end_date = date("Y-m-d", mktime(0,0,0,$bl_end_month,$bl_end_day,$bl_end_year));
$bl_currency_name = getConfigurationValueById($bl_currency_code);

$pcf_a = getSlIdByAccountCode(10600);
$pcf_b = getSlIdByAccountCode(10700);
if ($pcf_a != "" && $pcf_b != "")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($pcf_a)==true)
	{
		$sum_pcf_s_a = 0;
	}
	else
	{
		$sum_pcf_s_a = getDebitCreditSumValue($pcf_a,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($pcf_b)==true)
	{
		$sum_pcf_s_b = 0;
	}
	else
	{
		$sum_pcf_s_b = getDebitCreditSumValue($pcf_b,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}	

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($pcf_a)==true)
	{
		$sum_pcf_e_a = getDebitCreditSumValue($pcf_a,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_pcf_e_a = getDebitCreditSumValue($pcf_a,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($pcf_b)==true)
	{
		$sum_pcf_e_b = getDebitCreditSumValue($pcf_b,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_pcf_e_b = getDebitCreditSumValue($pcf_b,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
			
	$arr_pcf_starting = $sum_pcf_s_a + $sum_pcf_s_b;	
	$arr_pcf_ending = $sum_pcf_e_a + $sum_pcf_e_b;
}

$cib_a = getSlIdByAccountCode(10100);
$cib_b = getSlIdByAccountCode(10200);
$cib_c = getSlIdByAccountCode(10300);
$cib_d = getSlIdByAccountCode(10400);
$cib_e = getSlIdByAccountCode(10500);
$cib_f = getSlIdByAccountCode(10800);
$cib_g = getSlIdByAccountCode(10900);
$cib_h = getSlIdByAccountCode(10920);
$cib_i = getSlIdByAccountCode(10910);
$cib_j = getSlIdByAccountCode(10930);
$cib_k = getSlIdByAccountCode(10940);
$cib_l = getSlIdByAccountCode(10950);
$cib_m = getSlIdByAccountCode(10960);

if ($cib_a!="" && $cib_b!="" && $cib_c!="" && $cib_d!="" && $cib_e!="" && $cib_f!="" && $cib_g!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_a)==true)
	{
		$sum_cib_s_a = 0;
	}
	else
	{
		$sum_cib_s_a = getDebitCreditSumValue($cib_a,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_b)==true)
	{
		$sum_cib_s_b = 0;
	}
	else
	{
		$sum_cib_s_b = getDebitCreditSumValue($cib_b,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_c)==true)
	{
		$sum_cib_s_c = 0;
	}
	else
	{
		$sum_cib_s_c = getDebitCreditSumValue($cib_c,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_d)==true)
	{
		$sum_cib_s_d = 0;
	}
	else
	{
		$sum_cib_s_d = getDebitCreditSumValue($cib_d,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_e)==true)
	{
		$sum_cib_s_e = 0;
	}
	else
	{
		$sum_cib_s_e = getDebitCreditSumValue($cib_e,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_f)==true)
	{
		$sum_cib_s_f = 0;
	}
	else
	{
		$sum_cib_s_f = getDebitCreditSumValue($cib_f,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_g)==true)
	{
		$sum_cib_s_g = 0;
	}
	else
	{
		$sum_cib_s_g = getDebitCreditSumValue($cib_g,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_h)==true)
	{
		$sum_cib_s_h = 0;
	}
	else
	{
		$sum_cib_s_h = getDebitCreditSumValue($cib_h,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_i)==true)
	{
		$sum_cib_s_i = 0;
	}
	else
	{
		$sum_cib_s_i = getDebitCreditSumValue($cib_i,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_j)==true)
	{
		$sum_cib_s_j = 0;
	}
	else
	{
		$sum_cib_s_j = getDebitCreditSumValue($cib_j,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_k)==true)
	{
		$sum_cib_s_k = 0;
	}
	else
	{
		$sum_cib_s_k = getDebitCreditSumValue($cib_k,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_l)==true)
	{
		$sum_cib_s_l = 0;
	}
	else
	{
		$sum_cib_s_l = getDebitCreditSumValue($cib_l,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($cib_m)==true)
	{
		$sum_cib_s_m = 0;
	}
	else
	{
		$sum_cib_s_m = getDebitCreditSumValue($cib_m,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}		
	
	$arr_cib_starting = $sum_cib_s_a + $sum_cib_s_b + $sum_cib_s_c + $sum_cib_s_d + $sum_cib_s_e + $sum_cib_s_f + $sum_cib_s_g + $sum_cib_s_h + $sum_cib_s_i + $sum_cib_s_j + $sum_cib_s_k + $sum_cib_s_l + $sum_cib_s_m;

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_a)==true)
	{
		$sum_cib_e_a = getDebitCreditSumValue($cib_a,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_a = getDebitCreditSumValue($cib_a,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
		
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_b)==true)
	{
		$sum_cib_e_b = getDebitCreditSumValue($cib_b,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_b = getDebitCreditSumValue($cib_b,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_c)==true)
	{
		$sum_cib_e_c = getDebitCreditSumValue($cib_c,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_c = getDebitCreditSumValue($cib_c,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_d)==true)
	{
		$sum_cib_e_d = getDebitCreditSumValue($cib_d,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_d = getDebitCreditSumValue($cib_d,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_e)==true)
	{
		$sum_cib_e_e = getDebitCreditSumValue($cib_e,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_e = getDebitCreditSumValue($cib_e,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}		
		
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_f)==true)
	{
		$sum_cib_e_f = getDebitCreditSumValue($cib_f,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_f = getDebitCreditSumValue($cib_f,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}		

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_g)==true)
	{
		$sum_cib_e_g = getDebitCreditSumValue($cib_g,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_g = getDebitCreditSumValue($cib_g,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_h)==true)
	{
		$sum_cib_e_h = getDebitCreditSumValue($cib_h,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_h = getDebitCreditSumValue($cib_h,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}			

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_i)==true)
	{
		$sum_cib_e_i = getDebitCreditSumValue($cib_i,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_i = getDebitCreditSumValue($cib_i,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}			

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_j)==true)
	{
		$sum_cib_e_j = getDebitCreditSumValue($cib_j,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_j = getDebitCreditSumValue($cib_j,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}			

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_k)==true)
	{
		$sum_cib_e_k = getDebitCreditSumValue($cib_k,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_k = getDebitCreditSumValue($cib_k,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}			

	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_l)==true)
	{
		$sum_cib_e_l = getDebitCreditSumValue($cib_l,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_l = getDebitCreditSumValue($cib_l,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}			
			
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($cib_m)==true)
	{
		$sum_cib_e_m = getDebitCreditSumValue($cib_m,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$sum_cib_e_m = getDebitCreditSumValue($cib_m,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}			
			
	$arr_cib_ending = $sum_cib_e_a+$sum_cib_e_b+$sum_cib_e_c+$sum_cib_e_d+$sum_cib_e_e+$sum_cib_e_f+$sum_cib_e_g+$sum_cib_e_h+$sum_cib_e_i+$sum_cib_e_j+$sum_cib_e_k+$sum_cib_e_l+$sum_cib_e_m;

}
$ams_sl_id = getSlIdByAccountCode(11300);
if ($ams_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($ams_sl_id)==true)
	{
		$arr_ams_starting = 0;
	}
	else
	{
		$arr_ams_starting = getDebitCreditSumValue($ams_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($ams_sl_id)==true)
	{
		$arr_ams_ending = getDebitCreditSumValue($ams_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_ams_ending = getDebitCreditSumValue($ams_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}		
}
$aoe_sl_id = getSlIdByAccountCode(11100);
if($aoe_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($aoe_sl_id)==true)
	{
		$arr_aoe_starting = 0;
	}
	else
	{
		$arr_aoe_starting = getDebitCreditSumValue($aoe_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($aoe_sl_id)==true)
	{
		$arr_aoe_ending = getDebitCreditSumValue($aoe_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_aoe_ending = getDebitCreditSumValue($aoe_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}		
}
$aas_sl_id = getSlIdByAccountCode(11200);
if ($aas_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($aas_sl_id)==true)
	{
		$arr_aas_starting = 0;
	}
	else
	{
		$arr_aas_starting = getDebitCreditSumValue($aas_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($aas_sl_id)==true)
	{
		$arr_aas_ending = getDebitCreditSumValue($aas_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_aas_ending = getDebitCreditSumValue($aas_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}	
}
$ar_sl_id = getSlIdByAccountCode(12100);
if ($ar_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($ar_sl_id)==true)
	{
		$arr_ar_starting = 0;
	}
	else
	{
		$arr_ar_starting = getDebitCreditSumValue($ar_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($ar_sl_id)==true)
	{
		$arr_ar_ending = getDebitCreditSumValue($ar_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_ar_ending = getDebitCreditSumValue($ar_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}		
	
}
$ar_gop_sl_id = getSlIdByAccountCode(12200);
if ($ar_gop_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($ar_gop_sl_id)==true)
	{
		$arr_ar_gop_starting = 0;
	}
	else
	{
		$arr_ar_gop_starting = getDebitCreditSumValue($ar_gop_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($ar_gop_sl_id)==true)
	{
		$arr_ar_gop_ending = getDebitCreditSumValue($ar_gop_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_ar_gop_ending = getDebitCreditSumValue($ar_gop_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}	
}
$ar_others_sl_id = getSlIdByAccountCode(12300);
if($ar_others_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($ar_others_sl_id)==true)
	{
		$arr_ar_others_starting = 0;
	}
	else
	{
		$arr_ar_others_starting = getDebitCreditSumValue($ar_others_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($ar_others_sl_id)==true)
	{
		$arr_ar_others_ending = getDebitCreditSumValue($ar_others_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_ar_others_ending = getDebitCreditSumValue($ar_others_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
}
$ap_sl_id = getSlIdByAccountCode(16100);
if($ap_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($ap_sl_id)==true)
	{
		$arr_ap_starting = 0;
	}
	else
	{
		$arr_ap_starting = getDebitCreditSumValue($ap_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($ap_sl_id)==true)
	{
		$arr_ap_ending = getDebitCreditSumValue($ap_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_ap_ending = getDebitCreditSumValue($ap_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
		
	$arr_ap_starting = abs($arr_ap_starting);
	$arr_ap_ending = abs($arr_ap_ending);	
}

$wtp_sl_id = getSlIdByAccountCode(16200);
if ($wtp_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($wtp_sl_id)==true)
	{
		$arr_wtp_starting = 0;
	}
	else
	{
		$arr_wtp_starting = getDebitCreditSumValue($wtp_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($wtp_sl_id)==true)
	{
		$arr_wtp_ending = getDebitCreditSumValue($wtp_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_wtp_ending = getDebitCreditSumValue($wtp_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	$arr_wtp_starting = abs($arr_wtp_starting);
	$arr_wtp_ending = abs($arr_wtp_ending);	
}

$op_sl_id = getSlIdByAccountCode(16300);
if ($op_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($op_sl_id)==true)
	{
		$arr_op_starting = 0;
	}
	else
	{
		$arr_op_starting = getDebitCreditSumValue($op_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($op_sl_id)==true)
	{
		$arr_op_ending = getDebitCreditSumValue($op_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_op_ending = getDebitCreditSumValue($op_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}	
	$arr_op_starting = abs($arr_op_starting);
	$arr_op_ending = abs($arr_op_ending);	
}

$feb_sl_id = getSlIdByAccountCode(17100);
if($feb_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($feb_sl_id)==true)
	{
		$arr_feb_starting = 0;
	}
	else
	{
		$arr_feb_starting = getDebitCreditSumValue($feb_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($feb_sl_id)==true)
	{
		$arr_feb_ending = getDebitCreditSumValue($feb_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_feb_ending = getDebitCreditSumValue($feb_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}			
	$arr_feb_starting = abs($arr_feb_starting);
	$arr_feb_ending = abs($arr_feb_ending);	
}

$ppa_sl_id = getSlIdByAccountCode(17300);
if ($ppa_sl_id!="")
{
	if ($bl_start_month==1 && $bl_start_day == 1 && checkIfNominalAccount($ppa_sl_id)==true)
	{
		$arr_ppa_starting = 0;
	}
	else
	{
		$arr_ppa_starting = getDebitCreditSumValue($ppa_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	
	if ((strtotime($start_date)>strtotime($selected_start_date_year)) && checkIfNominalAccount($ppa_sl_id)==true)
	{
		$arr_ppa_ending = getDebitCreditSumValue($ppa_sl_id,$bl_currency_code,$selected_start_date_year,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}
	else
	{
		$arr_ppa_ending = getDebitCreditSumValue($ppa_sl_id,$bl_currency_code,$system_start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	}			
	
	$arr_ppa_starting = abs($arr_ppa_starting);
	$arr_ppa_ending = abs($arr_ppa_ending);
}

/*
	START OF FUND EQUITY
*/
$ftfb_sl_id = getSlIdByAccountCode(20100);
if ($ftfb_sl_id!="")
{		
	$arr_sum_ftfb_starting = getDebitCreditSumValue($ftfb_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	$arr_sum_ftfb_ending = getDebitCreditSumValue($ftfb_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ftfe_sl_id = getSlIdByAccountCode(20200);
if ($ftfe_sl_id!="")
{								  
	$arr_sum_ftfe_starting = getDebitCreditSumValue($ftfe_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	$arr_sum_ftfe_ending = getDebitCreditSumValue($ftfe_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ftfu_sl_id = getSlIdByAccountCode(20300);
if ($ftfu_sl_id!="")
{		
	$arr_sum_ftfu_starting = getDebitCreditSumValue($ftfu_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	$arr_sum_ftfu_ending = getDebitCreditSumValue($ftfu_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ftfup_sl_id = getSlIdByAccountCode(20400);
if ($ftfup_sl_id!="")
{								  
	$arr_sum_ftfup_starting = getDebitCreditSumValue($ftfup_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	$arr_sum_ftfup_ending = getDebitCreditSumValue($ftfup_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$ftfap_sl_id = getSlIdByAccountCode(20500);
if ($ftfap_sl_id!="")
{		
	$arr_sum_ftfap_starting = getDebitCreditSumValue($ftfap_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
	$arr_sum_ftfap_ending = getDebitCreditSumValue($ftfap_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_obligated_value);	
}
$fgl_sl_id = getSlIdByAccountCode(21100);
if ($fgl_sl_id!="")
{								  
	$arr_sum_fgl_starting = getDebitCreditSumValue($fgl_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_fgl_ending = getDebitCreditSumValue($fgl_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$bi_sl_id = getSlIdByAccountCode(21200);
if ($bi_sl_id!="")
{								  
	$arr_sum_bi_starting = getDebitCreditSumValue($bi_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_bi_ending = getDebitCreditSumValue($bi_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$acb_sl_id = getSlIdByAccountCode(22110);
if ($acb_sl_id!="")
{		
	$arr_sum_acb_starting = getDebitCreditSumValue($acb_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_acb_ending = getDebitCreditSumValue($acb_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);
}
$acc_sl_id = getSlIdByAccountCode(22111);
if ($acc_sl_id!="")
{								  
	$arr_sum_acc_starting = getDebitCreditSumValue($acc_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_acc_ending = getDebitCreditSumValue($acc_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$aci_sl_id = getSlIdByAccountCode(22112);
if ($aci_sl_id!="")
{								  
	$arr_sum_aci_starting = getDebitCreditSumValue($aci_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_aci_ending = getDebitCreditSumValue($aci_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$aclp_sl_id = getSlIdByAccountCode(22113);
if ($aclp_sl_id!="")
{		
	$arr_sum_aclp_starting = getDebitCreditSumValue($aclp_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	

	$arr_sum_aclp_ending = getDebitCreditSumValue($aclp_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$acmal_sl_id = getSlIdByAccountCode(22114);
if ($acmal_sl_id!="")
{								  
	$arr_sum_acmal_starting = getDebitCreditSumValue($acmal_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_acmal_ending = getDebitCreditSumValue($acmal_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$acmy_sl_id = getSlIdByAccountCode(22115);
if ($acmy_sl_id!="")
{		
	$arr_sum_acmy_starting = getDebitCreditSumValue($acmy_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_acmy_ending = getDebitCreditSumValue($acmy_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$acp_sl_id = getSlIdByAccountCode(22116);
if ($acp_sl_id!="")
{								  
	$arr_sum_acp_starting = getDebitCreditSumValue($acp_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_acp_ending = getDebitCreditSumValue($acp_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$acs_sl_id = getSlIdByAccountCode(22117);
if ($acs_sl_id!="")
{		
	$arr_sum_acs_starting = getDebitCreditSumValue($acs_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_acs_ending = getDebitCreditSumValue($acs_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$act_sl_id = getSlIdByAccountCode(22118);
if ($act_sl_id!="")
{								  
	$arr_sum_act_starting = getDebitCreditSumValue($act_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_act_ending = getDebitCreditSumValue($act_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$acv_sl_id = getSlIdByAccountCode(22119);
if ($acv_sl_id!="")
{
	$arr_sum_acv_starting = getDebitCreditSumValue($acv_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_acv_ending = getDebitCreditSumValue($acv_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}

$df_sl_id_a = getSlIdByAccountCode(23100);
$df_sl_id_b = getSlIdByAccountCode(23200);
$df_sl_id_c = getSlIdByAccountCode(23300);
$df_sl_id_d = getSlIdByAccountCode(23400);
$df_sl_id_e = getSlIdByAccountCode(23500);
$df_sl_id_f = getSlIdByAccountCode(23600);
$df_sl_id_g = getSlIdByAccountCode(23700);
$df_sl_id_h = getSlIdByAccountCode(23800);
$df_sl_id_i = getSlIdByAccountCode(23900);
if ($df_sl_id_a!="" && $df_sl_id_b != "" && $df_sl_id_c != "" && $df_sl_id_d != "" && $df_sl_id_e != "" && $df_sl_id_f != "" && $df_sl_id_g != "" && $df_sl_id_h != "" && $df_sl_id_i != "")
{

	$sum_df_s_a = getDebitCreditSumValue($df_sl_id_a,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_s_b = getDebitCreditSumValue($df_sl_id_b,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_s_c = getDebitCreditSumValue($df_sl_id_c,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_s_d = getDebitCreditSumValue($df_sl_id_d,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_s_e = getDebitCreditSumValue($df_sl_id_e,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_s_f = getDebitCreditSumValue($df_sl_id_f,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_s_g = getDebitCreditSumValue($df_sl_id_g,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_s_h = getDebitCreditSumValue($df_sl_id_h,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_s_i = getDebitCreditSumValue($df_sl_id_i,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	

	$sum_df_e_a = getDebitCreditSumValue($df_sl_id_a,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_e_b = getDebitCreditSumValue($df_sl_id_b,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_e_c = getDebitCreditSumValue($df_sl_id_c,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_e_d = getDebitCreditSumValue($df_sl_id_d,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_e_e = getDebitCreditSumValue($df_sl_id_e,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_e_f = getDebitCreditSumValue($df_sl_id_f,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_e_g = getDebitCreditSumValue($df_sl_id_g,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_e_h = getDebitCreditSumValue($df_sl_id_h,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_df_e_i = getDebitCreditSumValue($df_sl_id_i,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
		
	$arr_sum_df_starting = $sum_df_s_a + $sum_df_s_b + $sum_df_s_c + $sum_df_s_d + $sum_df_s_e + $sum_df_s_f + $sum_df_s_g + $sum_df_s_h + $sum_df_s_i;
	$arr_sum_df_ending = $sum_df_e_a + $sum_df_e_b + $sum_df_e_c + $sum_df_e_d + $sum_df_e_e + $sum_df_e_f + $sum_df_e_g + $sum_df_e_h + $sum_df_e_i;	
}
 
$sls_sl_id_a = getSlIdByAccountCode(24100);
$sls_sl_id_b = getSlIdByAccountCode(24200);
$sls_sl_id_c = getSlIdByAccountCode(24300);
if ($sls_sl_id_a!="" && $sls_sl_id_b != "" && $sls_sl_id_c != "")
{		
	$sum_sls_s_a = getDebitCreditSumValue($sls_sl_id_a,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_sls_s_b = getDebitCreditSumValue($sls_sl_id_b,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_sls_s_c = getDebitCreditSumValue($sls_sl_id_c,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	

	$sum_sls_e_a = getDebitCreditSumValue($sls_sl_id_a,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_sls_e_b = getDebitCreditSumValue($sls_sl_id_b,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_sls_e_c = getDebitCreditSumValue($sls_sl_id_c,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	

	$arr_sum_sls_starting = $sum_sls_s_a + $sum_sls_s_b + $sum_sls_s_c;
	$arr_sum_sls_ending = $sum_sls_e_a + $sum_sls_e_b + $sum_sls_e_c;
}
$oi_sl_id_a = getSlIdByAccountCode(24400);
$oi_sl_id_b = getSlIdByAccountCode(24500);
if ($oi_sl_id_a!="" && $oi_sl_id_b != "")
{		
	$sum_oi_s_a = getDebitCreditSumValue($oi_sl_id_a,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_oi_s_b = getDebitCreditSumValue($oi_sl_id_b,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	

	$sum_oi_e_a = getDebitCreditSumValue($oi_sl_id_a,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$sum_oi_e_b = getDebitCreditSumValue($oi_sl_id_b,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	

	$arr_sum_oi_starting = $sum_oi_s_a + $sum_oi_s_b;
	$arr_sum_oi_ending = $sum_oi_e_a + $sum_oi_e_b;
}

$total_receipts_starting = 	abs($arr_sum_ftfb_starting) +
							abs($arr_sum_ftfe_starting) +
							abs($arr_sum_ftfu_starting) +
							abs($arr_sum_ftfup_starting) +
							abs($arr_sum_ftfap_starting) +
							abs($arr_sum_bi_starting) +
							abs($arr_sum_acb_starting) +
							abs($arr_sum_acc_starting) +
							abs($arr_sum_aci_starting) +
							abs($arr_sum_aclp_starting) +
							abs($arr_sum_acmal_starting) +
							abs($arr_sum_acmy_starting) +
							abs($arr_sum_acp_starting) +
							abs($arr_sum_acs_starting) +
							abs($arr_sum_act_starting) +
							abs($arr_sum_acv_starting) +
							abs($arr_sum_df_starting) +
							abs($arr_sum_sls_starting) +
							abs($arr_sum_oi_starting) + 
							(-abs($arr_sum_fgl_starting));
							
$total_receipts_ending = 	abs($arr_sum_ftfb_ending) +
							abs($arr_sum_ftfe_ending) +
							abs($arr_sum_ftfu_ending) +
							abs($arr_sum_ftfup_ending) +
							abs($arr_sum_ftfap_ending) +
							abs($arr_sum_bi_ending) +
							abs($arr_sum_acb_ending) +
							abs($arr_sum_acc_ending) +
							abs($arr_sum_aci_ending) +
							abs($arr_sum_aclp_ending) +
							abs($arr_sum_acmal_ending) +
							abs($arr_sum_acmy_ending) +
							abs($arr_sum_acp_ending) +
							abs($arr_sum_acs_ending) +
							abs($arr_sum_act_ending) +
							abs($arr_sum_acv_ending) +
							abs($arr_sum_df_ending) +
							abs($arr_sum_sls_ending) +
							abs($arr_sum_oi_ending) + 
							(-abs($arr_sum_fgl_ending));
													
$ed_sl_id = getSlIdByAccountCode(30010);
if ($ed_sl_id!="")
{								  
	$arr_sum_ed_starting = getDebitCreditSumValue($ed_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_ed_ending = getDebitCreditSumValue($ed_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$dars_sl_id = getSlIdByAccountCode(30020);
if ($dars_sl_id!="")
{		
	$arr_sum_dars_starting = getDebitCreditSumValue($dars_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_dars_ending = getDebitCreditSumValue($dars_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$faar_sl_id = getSlIdByAccountCode(30030);
if ($faar_sl_id!="")
{								  
	$arr_sum_faar_starting = getDebitCreditSumValue($faar_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_faar_ending = getDebitCreditSumValue($faar_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$oedpa_sl_id = getSlIdByAccountCode(30040);
if ($oedpa_sl_id!="")
{								  
	$arr_sum_oedpa_starting = getDebitCreditSumValue($oedpa_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_oedpa_ending = getDebitCreditSumValue($oedpa_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$ssst_sl_id = getSlIdByAccountCode(30050);
if ($ssst_sl_id!="")
{		
	$arr_sum_ssst_starting = getDebitCreditSumValue($ssst_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_ssst_ending = getDebitCreditSumValue($ssst_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$secstaff_sl_id = getSlIdByAccountCode(30060);
if ($secstaff_sl_id!="")
{								  
	$arr_sum_secstaff_starting = getDebitCreditSumValue($secstaff_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_secstaff_ending = getDebitCreditSumValue($secstaff_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$sssa_sl_id = getSlIdByAccountCode(30070);
if ($sssa_sl_id!="")
{	
	$arr_sum_sssa_starting = getDebitCreditSumValue($sssa_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_sssa_ending = getDebitCreditSumValue($sssa_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$gss_sl_id = getSlIdByAccountCode(30080);
if ($gss_sl_id!="")
{								  
	$arr_sum_gss_starting = getDebitCreditSumValue($gss_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_gss_ending = getDebitCreditSumValue($gss_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}

$arr_sum_sw_starting = $arr_sum_ed_starting + $arr_sum_dars_starting + $arr_sum_faar_starting + $arr_sum_oedpa_starting + $arr_sum_ssst_starting + $arr_sum_secstaff_starting + $arr_sum_sssa_starting + $arr_sum_gss_starting;

$arr_sum_sw_ending = $arr_sum_ed_ending + $arr_sum_dars_ending + $arr_sum_faar_ending + $arr_sum_oedpa_ending + $arr_sum_ssst_ending + $arr_sum_secstaff_ending + $arr_sum_sssa_ending + $arr_sum_gss_ending;


$staffallow_sl_id = getSlIdByAccountCode(30090);
if ($staffallow_sl_id!="")
{								  
	$arr_sum_staffallow_starting = getDebitCreditSumValue($staffallow_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_staffallow_ending = getDebitCreditSumValue($staffallow_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$sal_sl_id = getSlIdByAccountCode(30100);
if ($sal_sl_id!="")
{		
	$arr_sum_sal_starting = getDebitCreditSumValue($sal_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_sal_ending = getDebitCreditSumValue($sal_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$aps_sl_id = getSlIdByAccountCode(30200);
if ($aps_sl_id!="")
{								  
	$arr_sum_aps_starting = getDebitCreditSumValue($aps_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_aps_ending = getDebitCreditSumValue($aps_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$lacps_sl_id = getSlIdByAccountCode(30300);
if ($lacps_sl_id!="")
{			
	$arr_sum_lacps_starting = getDebitCreditSumValue($lacps_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_lacps_ending = getDebitCreditSumValue($lacps_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$arr_sum_pdmt_starting = $arr_sum_aps_starting+$arr_sum_lacps_starting;
$arr_sum_pdmt_ending = $arr_sum_aps_ending+$arr_sum_lacps_ending;

$arr_sum_hr_starting = $arr_sum_sw_starting+$arr_sum_staffallow_starting+$arr_sum_sal_starting+$arr_sum_pdmt_starting;
$arr_sum_hr_ending = $arr_sum_sw_ending+$arr_sum_staffallow_ending+$arr_sum_sal_ending+$arr_sum_pdmt_ending;

$intercon_sl_id = getSlIdByAccountCode(31100);
if ($intercon_sl_id!="")
{		
	$arr_sum_intercon_starting = getDebitCreditSumValue($intercon_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_intercon_ending = getDebitCreditSumValue($intercon_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$intl_sl_id = getSlIdByAccountCode(31200);
if ($intl_sl_id!="")
{								  
	$arr_sum_intl_starting = getDebitCreditSumValue($intl_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_intl_ending = getDebitCreditSumValue($intl_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}

$arr_sum_travel_starting = $arr_sum_intercon_starting+$arr_sum_intl_starting;
$arr_sum_travel_ending = $arr_sum_intercon_ending+$arr_sum_intl_ending;

$uprv_sl_id = getSlIdByAccountCode(32100);
if ($uprv_sl_id!="")
{								  
	$arr_sum_uprv_starting = getDebitCreditSumValue($uprv_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_uprv_ending = getDebitCreditSumValue($uprv_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$prv_sl_id = getSlIdByAccountCode(32110);
if ($prv_sl_id!="")
{								  
	$arr_sum_prv_starting = getDebitCreditSumValue($prv_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_prv_ending = getDebitCreditSumValue($prv_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$rfce_sl_id = getSlIdByAccountCode(32220);
if ($rfce_sl_id!="")
{		
	$arr_sum_rfce_starting = getDebitCreditSumValue($rfce_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_rfce_ending = getDebitCreditSumValue($rfce_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$ufce_sl_id = getSlIdByAccountCode(32200);
if ($ufce_sl_id!="")
{								  
	$arr_sum_ufce_starting = getDebitCreditSumValue($ufce_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_ufce_ending = getDebitCreditSumValue($ufce_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$roe_sl_id = getSlIdByAccountCode(32330);
if ($roe_sl_id!="")
{		
	$arr_sum_roe_starting = getDebitCreditSumValue($roe_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_roe_ending = getDebitCreditSumValue($roe_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$uoe_sl_id = getSlIdByAccountCode(32300);
if ($uoe_sl_id!="")
{					
	$arr_sum_uoe_starting = getDebitCreditSumValue($uoe_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_uoe_ending = getDebitCreditSumValue($uoe_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$lm_sl_id = getSlIdByAccountCode(32400);
if ($lm_sl_id!="")
{								  
	$arr_sum_lm_starting = getDebitCreditSumValue($lm_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_lm_ending = getDebitCreditSumValue($lm_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}

$arr_sum_ses_starting = $arr_sum_uprv_starting+
						$arr_sum_prv_starting+
						$arr_sum_rfce_starting+
						$arr_sum_ufce_starting+
						$arr_sum_roe_starting+
						$arr_sum_uoe_starting+
						$arr_sum_lm_starting;
						
$arr_sum_ses_ending = $arr_sum_uprv_ending+
						$arr_sum_prv_ending+
						$arr_sum_rfce_ending+
						$arr_sum_ufce_ending+
						$arr_sum_roe_ending+
						$arr_sum_uoe_ending+
						$arr_sum_lm_ending;

$vc_sl_id = getSlIdByAccountCode(33100);
if ($vc_sl_id!="")
{		
	$arr_sum_vc_starting = getDebitCreditSumValue($vc_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_vc_ending = getDebitCreditSumValue($vc_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$cos_sl_id = getSlIdByAccountCode(33200);
if ($cos_sl_id!="")
{		
	$arr_sum_cos_starting = getDebitCreditSumValue($cos_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_cos_ending = getDebitCreditSumValue($cos_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$moe_sl_id = getSlIdByAccountCode(33300);
if ($moe_sl_id!="")
{		
	$arr_sum_moe_starting = getDebitCreditSumValue($moe_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_moe_ending = getDebitCreditSumValue($moe_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$oserv_sl_id = getSlIdByAccountCode(33400);
if ($oserv_sl_id!="")
{		
	$arr_sum_oserv_starting = getDebitCreditSumValue($oserv_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_oserv_ending = getDebitCreditSumValue($oserv_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$orent_sl_id = getSlIdByAccountCode(33500);
if ($orent_sl_id!="")
{		
	$arr_sum_orent_starting = getDebitCreditSumValue($orent_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_orent_ending = getDebitCreditSumValue($orent_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$arr_sum_lopc_starting =	$arr_sum_vc_starting +
							$arr_sum_cos_starting +
							$arr_sum_moe_starting +
							$arr_sum_oserv_starting +
							$arr_sum_orent_starting;
							
$arr_sum_lopc_ending = 		$arr_sum_vc_ending +
							$arr_sum_cos_ending +
							$arr_sum_moe_ending +
							$arr_sum_oserv_ending +
							$arr_sum_orent_ending;

$guipub_sl_id = getSlIdByAccountCode(34101);
if ($guipub_sl_id!="")
{		
	$arr_sum_guipub_starting = getDebitCreditSumValue($guipub_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_guipub_ending = getDebitCreditSumValue($guipub_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$abmag_sl_id = getSlIdByAccountCode(34102);
if ($abmag_sl_id!="")
{								  
	$arr_sum_abmag_starting = getDebitCreditSumValue($abmag_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_abmag_ending = getDebitCreditSumValue($abmag_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$gdve_sl_id = getSlIdByAccountCode(34103);
if ($gdve_sl_id!="")
{		
	$arr_sum_gdve_starting = getDebitCreditSumValue($gdve_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_gdve_ending = getDebitCreditSumValue($gdve_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$peav_sl_id = getSlIdByAccountCode(34104);
if ($peav_sl_id!="")
{		
	$arr_sum_peav_starting = getDebitCreditSumValue($peav_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_peav_ending = getDebitCreditSumValue($peav_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$studre_sl_id = getSlIdByAccountCode(34105);
if ($studre_sl_id!="")
{		
	$arr_sum_studre_starting = getDebitCreditSumValue($studre_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_studre_ending = getDebitCreditSumValue($studre_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$supstud_sl_id = getSlIdByAccountCode(34106);
if ($supstud_sl_id!="")
{		
	$arr_sum_supstud_starting = getDebitCreditSumValue($supstud_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_supstud_ending = getDebitCreditSumValue($supstud_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$lste_sl_id = getSlIdByAccountCode(34107);
if ($lste_sl_id!="")
{		
	$arr_sum_lste_starting = getDebitCreditSumValue($lste_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_lste_ending = getDebitCreditSumValue($lste_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$ilssfp_sl_id = getSlIdByAccountCode(34108);
if ($ilssfp_sl_id!="")
{		
	$arr_sum_ilssfp_starting = getDebitCreditSumValue($ilssfp_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_ilssfp_ending = getDebitCreditSumValue($ilssfp_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$corts2wk_sl_id = getSlIdByAccountCode(34109);
if ($corts2wk_sl_id!="")
{		
	$arr_sum_corts2wk_starting = getDebitCreditSumValue($corts2wk_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_corts2wk_ending = getDebitCreditSumValue($corts2wk_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$corts1wk_sl_id = getSlIdByAccountCode(34110);
if ($corts1wk_sl_id!="")
{		
	$arr_sum_corts1wk_starting = getDebitCreditSumValue($corts1wk_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_corts1wk_ending = getDebitCreditSumValue($corts1wk_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}

$cosrts1w_sl_id = getSlIdByAccountCode(34111);
if ($cosrts1w_sl_id!="")
{								  
	$arr_sum_cosrts1w_starting = getDebitCreditSumValue($cosrts1w_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_cosrts1w_ending = getDebitCreditSumValue($cosrts1w_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$costwor_sl_id = getSlIdByAccountCode(34112);
if ($costwor_sl_id!="")
{		
	$arr_sum_costwor_starting = getDebitCreditSumValue($costwor_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_costwor_ending = getDebitCreditSumValue($costwor_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$suptohiged_sl_id = getSlIdByAccountCode(34113);
if ($suptohiged_sl_id!="")
{		
	$arr_sum_suptohiged_starting = getDebitCreditSumValue($suptohiged_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_suptohiged_ending = getDebitCreditSumValue($suptohiged_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$ateac_sl_id = getSlIdByAccountCode(34114);
if ($ateac_sl_id!="")
{								  
	$arr_sum_ateac_starting = getDebitCreditSumValue($ateac_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_ateac_ending = getDebitCreditSumValue($ateac_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$evast_sl_id = getSlIdByAccountCode(34115);
if ($evast_sl_id!="")
{								  
	$arr_sum_evast_starting = getDebitCreditSumValue($evast_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_evast_ending = getDebitCreditSumValue($evast_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$evacost_sl_id = getSlIdByAccountCode(34116);
if ($evast_sl_id!="")
{		
	$arr_sum_evacost_starting = getDebitCreditSumValue($evacost_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_evacost_ending = getDebitCreditSumValue($evacost_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$pcsct_sl_id = getSlIdByAccountCode(34117);
if ($pcsct_sl_id!="")
{		
	$arr_sum_pcsct_starting = getDebitCreditSumValue($pcsct_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_pcsct_ending = getDebitCreditSumValue($pcsct_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}

$arr_sum_ocostser_starting = 	$arr_sum_guipub_starting +
								$arr_sum_abmag_starting +
								$arr_sum_gdve_starting +
								$arr_sum_peav_starting +
								$arr_sum_studre_starting +
								$arr_sum_supstud_starting +
								$arr_sum_lste_starting +
								$arr_sum_ilssfp_starting +
								$arr_sum_corts2wk_starting +
								$arr_sum_corts1wk_starting +
								$arr_sum_cosrts1w_starting +
								$arr_sum_costwor_starting +
								$arr_sum_suptohiged_starting +
								$arr_sum_ateac_starting +
								$arr_sum_evast_starting +
								$arr_sum_evacost_starting +
								$arr_sum_pcsct_starting;

$arr_sum_ocostser_ending = 		$arr_sum_guipub_ending +
								$arr_sum_abmag_ending +
								$arr_sum_gdve_ending +
								$arr_sum_peav_ending +
								$arr_sum_studre_ending +
								$arr_sum_supstud_ending +
								$arr_sum_lste_ending +
								$arr_sum_ilssfp_ending +
								$arr_sum_corts2wk_ending +
								$arr_sum_corts1wk_ending +
								$arr_sum_cosrts1w_ending +
								$arr_sum_costwor_ending +
								$arr_sum_suptohiged_ending +
								$arr_sum_ateac_ending +
								$arr_sum_evast_ending +
								$arr_sum_evacost_ending +
								$arr_sum_pcsct_ending;
								
$auccost_sl_id = getSlIdByAccountCode(35100);
if ($auccost_sl_id!="")
{								  
	$arr_sum_auccost_starting = getDebitCreditSumValue($auccost_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_auccost_ending = getDebitCreditSumValue($auccost_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}
$reccost_sl_id = getSlIdByAccountCode(35200);
if ($reccost_sl_id!="")
{								  
	$arr_sum_reccost_starting = getDebitCreditSumValue($reccost_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_reccost_ending = getDebitCreditSumValue($reccost_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}					
$arr_sum_arcost_starting = $arr_sum_auccost_starting+$arr_sum_reccost_starting;
$arr_sum_arcost_ending = $arr_sum_auccost_ending+$arr_sum_reccost_ending;

$admincost_sl_id = getSlIdByAccountCode(36100);
if ($admincost_sl_id!="")
{								  
	$arr_sum_admincost_starting = getDebitCreditSumValue($admincost_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_admincost_ending = getDebitCreditSumValue($admincost_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}	
$cntngncy_sl_id = getSlIdByAccountCode(36200);
if ($cntngncy_sl_id!="")
{		
	$arr_sum_cntngncy_starting = getDebitCreditSumValue($cntngncy_sl_id,$bl_currency_code,$system_start_date,$start_date_minus_1,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
	$arr_sum_cntngncy_ending = getDebitCreditSumValue($cntngncy_sl_id,$bl_currency_code,$start_date,$end_date,$bl_budget_id,$bl_donor_id,$component_id="",$activity_id="",$other_cost_id="",$staff_id="",$benefits_id="",$vehicle_id="",$equipment_id="",$item_id="",$consolidate_reports,$use_osrdigated_value);	
}	

$arr_sum_totdis_starting =	$arr_sum_hr_starting +
							$arr_sum_travel_starting +
							$arr_sum_ses_starting +
							$arr_sum_lopc_starting +
							$arr_sum_ocostser_starting +
							$arr_sum_arcost_starting +
							$arr_sum_admincost_starting +
							$arr_sum_cntngncy_starting;
$arr_sum_totdis_ending =	$arr_sum_hr_ending +
							$arr_sum_travel_ending +
							$arr_sum_ses_ending +
							$arr_sum_lopc_ending +
							$arr_sum_ocostser_ending +
							$arr_sum_arcost_ending +
							$arr_sum_admincost_ending +
							$arr_sum_cntngncy_ending;							
/*
	END OF FUND EQUITY
*/											
$arr_fee_starting = (-abs($arr_sum_totdis_starting))+$total_receipts_starting;
$arr_fee_ending = (-abs($arr_sum_totdis_ending))+$total_receipts_ending;

?>
<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' style='table-layout:fixed'>
	<tr>
		<td>
			<table width='100%' border='0' cellspacing='2' cellpadding='2' class='report_printing' >
				<tr>
					<th scope='col'> 
					<?php echo upperCase($report_name); ?>
					<br />
					<?php echo upperCase(getCompanyName()); ?>
					<br />
					<?php echo upperCase(getCompanyAddress()); ?><br />
					For the period of <?php echo $start_date; ?> to <?php echo $end_date; ?><br>
					ACB-<?php echo $bl_currency_name; ?>
					</th>
				</tr>
				<tr>
					<th scope='col'>&nbsp;</th>
				</tr>
				<tr>
					<td scope='col'>
					<table width="100%" border="0" cellpadding="2" cellspacing="0" bordercolor="#CCCCCC">
						<thead>
						  <tr>
						    <th width="10%" scope="col">&nbsp;</th>
							<th width="12%" scope="col">&nbsp;</th>
							<th width="22%" scope="col">&nbsp;</th>
								<th width="15%" scope="col"><?php echo $start_date_minus_1; ?></th>
								<th width="15%" scope="col"><?php echo $end_date; ?></th>
								<th width="13%" scope="col">Movement</th>
								<th width="13%" scope="col">Movement % </th>
						  </tr>
						</thead>
						<tbody>
						<tr>
						  <td scope="col">&nbsp;</td>
						  <td scope="col">&nbsp;</td>
						  <td scope="col">&nbsp;</td>
						  <td scope="col"><hr></td>
						  <td scope="col"><hr></td>
						  <td scope="col"><hr></td>
						  <td scope="col"><hr></td>
						  </tr>
						<tr>
								  <td scope="col">Assets</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
						  </tr>
								<tr>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">Current Assets</td>
								  <td scope="col" align="right">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
						  </tr>
								<tr>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">Petty Cash Fund </td>
								  <td scope="col" align="right"><?php
									$pcf_starting = numberFormat($arr_pcf_starting);
									echo $pcf_starting;
								  ?></td>
								  <td scope="col" align="right">
									<?php
									$pcf_ending = numberFormat($arr_pcf_ending);  									
									echo $pcf_ending;
								  ?>								  </td>
								  <td scope="col" align="right">
								  <?php
								  	$total_pcf_movement = $arr_pcf_ending-$arr_pcf_starting;
									$pcf_movement = numberFormat(($total_pcf_movement)); 
									echo $pcf_movement;
								  ?>								  </td>
								  <td scope="col" align="right">
								  <?php
									if ($arr_pcf_starting!=0)
									{
										$total_pcf_pct_movement = ($total_pcf_movement/$arr_pcf_starting)*100;
									}
									else
									{
										$total_pcf_pct_movement = 0;
									}
									$pcf_pct_movement = numberFormat($total_pcf_pct_movement);
									echo $pcf_pct_movement;
								  ?>&nbsp;%								  </td>
						  </tr>
								<tr>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">Cash in Bank </td>
								  <td scope="col" align="right">
								  <?php
									$cib_starting = numberFormat($arr_cib_starting);  
									echo $cib_starting;
								  ?>								  </td>
								  <td scope="col" align="right">
									<?php
										$cib_ending = numberFormat($arr_cib_ending);  									
										echo $cib_ending;
								  	?>								  </td>
								  <td scope="col" align="right">
								  <?php
								  	$total_cib_movement = $arr_cib_ending-$arr_cib_starting;
									$cib_movement = numberFormat(($total_cib_movement));
									echo $cib_movement;
								  ?>								  </td>
								  <td scope="col" align="right">
								  <?php
									if ($arr_cib_starting!=0)
									{
										$total_cib_pct_movement = ($total_cib_movement/$arr_cib_starting)*100;
									}
									else
									{
										$total_cib_pct_movement = 0;
									}
									$cib_pct_movement = numberFormat($total_cib_pct_movement);
									echo $cib_pct_movement;
								  ?>&nbsp;%								  </td>
						  </tr>
								<tr>
								  <td colspan="7" scope="col"><hr></td>
						  </tr>
								<tr>
								  <td colspan="3" scope="col">Total Current Assets </td>
								  <td scope="col" align="right">
								  <?php
								  	$starting_current_assets = $arr_pcf_starting+$arr_cib_starting;
								  	$total_starting_current_assets = numberFormat($starting_current_assets);
									echo $total_starting_current_assets;
								  ?>								  </td>
								  <td scope="col" align="right">
								  <?php
								  	$ending_current_assets = $arr_pcf_ending+$arr_cib_ending;
								  	$total_ending_current_assets = numberFormat($ending_current_assets);
									echo $total_ending_current_assets;
								  ?>								  </td>
								  <td scope="col" align="right">
								  <?php
								  	$movement_current_assets = $total_pcf_movement+$total_cib_movement;
								  	$total_movement_current_assets = numberFormat(($movement_current_assets));
									echo $total_movement_current_assets;
								  ?>								  </td>
								  <td scope="col" align="right">
								  	<?php
										if ($starting_current_assets!=0)
										{
											$movement_pct_current_assets = ($movement_current_assets/$starting_current_assets)*100;
										}
										else
										{
											$movement_pct_current_assets = 0;
										}
										$total_movement_pct_current_assets = numberFormat($movement_pct_current_assets);
										echo $total_movement_pct_current_assets;
									?>&nbsp;%								  </td>
						  </tr>
								<tr>
								  <td colspan="7" scope="col"><hr></td>
						  </tr>
								<tr>
								  <td scope="col">Other Assets</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
						  </tr>
								<tr>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">Advances</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
						  </tr>
								<tr>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">Advances to AMS </td>
								  <td scope="col" align="right">
								  <?php
									$ams_starting = numberFormat($arr_ams_starting);  
									echo $ams_starting;
								  ?>								  </td>
								  <td scope="col" align="right">
									<?php
										$ams_ending = numberFormat($arr_ams_ending);  									
										echo $ams_ending;
								  	?>								  </td>
								  <td scope="col" align="right">
								  	<?php
								  	$total_ams_movement = $arr_ams_ending-$arr_ams_starting;
									$ams_movement = numberFormat($total_ams_movement);
									echo $ams_movement;									
									?>								  </td>
								  <td scope="col" align="right">
								  <?php
									if ($arr_ams_starting!=0)
									{
										$total_ams_pct_movement = ($total_ams_movement/$arr_ams_starting)*100;
									}
									else
									{
										$total_ams_pct_movement = 0;
									}
									$ams_pct_movement = numberFormat($total_ams_pct_movement);
									echo $ams_pct_movement;
								  ?>&nbsp;%								  </td>
						  </tr>
								<tr>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">Advances to Officers and Employees </td>
								  <td scope="col" align="right">
								  <?php
									$aoe_starting = numberFormat($arr_aoe_starting);  
									echo $aoe_starting;
								  ?>								  </td>
								  <td scope="col" align="right">
									<?php
										$aoe_ending = numberFormat($arr_aoe_ending);  									
										echo $aoe_ending;
								 	?>								  </td>
								  <td scope="col" align="right">
								  	<?php
								  	$total_aoe_movement = $arr_aoe_ending-$arr_aoe_starting;
									$aoe_movement = numberFormat(($total_aoe_movement));
									echo $aoe_movement;									
									?>								  </td>
								  <td scope="col" align="right">
								  <?php
									if ($arr_aoe_starting!=0)
									{
										$total_aoe_pct_movement = ($total_aoe_movement/$arr_aoe_starting)*100;
									}
									else
									{
										$total_aoe_pct_movement = 0;
									}
									$aoe_pct_movement = numberFormat($total_aoe_pct_movement);
									echo $aoe_pct_movement;
								  ?>&nbsp;%								  </td>
						  </tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">Advances to ASEAN Secretariat </td>
									<td scope="col" align="right">
									  <?php

										$aas_starting = numberFormat($arr_aas_starting);  
										echo $aas_starting;
									  ?>									</td>
									<td scope="col" align="right">
									<?php
										$aas_ending = numberFormat($arr_aas_ending);  									
										echo $aas_ending;
								 	?>									</td>
									<td scope="col" align="right">
									<?php
										$total_aas_movement = $arr_aas_ending-$arr_aas_starting;
										$aas_movement = numberFormat(($total_aas_movement));
										echo $aas_movement;									
									?>									</td>
									<td scope="col" align="right">
										<?php
										if ($arr_aas_starting!=0)
										{
											$total_aas_pct_movement = ($total_aas_movement/$arr_aas_starting)*100;
										}
										else
										{
											$total_aas_pct_movement = 0;
										}
										$aas_pct_movement = numberFormat($total_aas_pct_movement);
										echo $aas_pct_movement;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Total Advances </td>
									<td scope="col">&nbsp;</td>
									<td scope="col" align="right">
									<?php
										$starting_advances = $arr_ams_starting+$arr_aoe_starting+$arr_aas_starting;
										$total_starting_advaces = numberFormat($starting_advances);
										echo $total_starting_advaces;
									?>									</td>
									<td scope="col" align="right">
									<?php
										$ending_advances = $arr_ams_ending+$arr_aoe_ending+$arr_aas_ending;
										$total_ending_advaces = numberFormat($ending_advances);
										echo $total_ending_advaces;
									?>									</td>
									<td scope="col" align="right">
									<?php
										$movement_advances = $total_ams_movement+$total_aoe_movement+$total_aas_movement;
										$total_movement_advaces = numberFormat(($movement_advances));
										echo $total_movement_advaces;
									?>									</td>
									<td scope="col" align="right">
								  	<?php
										if ($starting_advances!=0)
										{
											$movement_pct_advances = ($movement_advances/$starting_advances)*100;
										}
										else
										{
											$movement_pct_advances = 0;
										}
										$total_movement_pct_advances = numberFormat($movement_pct_advances);
										echo $total_movement_pct_advances;
									?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Accounts Receivable </td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">Accounts Receivable </td>
									<td scope="col" align="right">
									  <?php
										$ar_starting = numberFormat($arr_ar_starting);  
										echo $ar_starting;
									  ?>									</td>
									<td scope="col" align="right">
										<?php
											$ar_ending = numberFormat($arr_ar_ending);  									
											echo $ar_ending
										?>									</td>
									<td scope="col" align="right">
									<?php
										$total_ar_movement = $arr_ar_ending-$arr_ar_starting;
										$ar_movement = numberFormat(($total_ar_movement));
										echo $ar_movement;									
									?>									</td>
									<td scope="col" align="right">
										<?php
										if ($arr_ar_starting!=0)
										{
											$total_ar_pct_movement = ($total_ar_movement/$arr_ar_starting)*100;
										}
										else
										{
											$total_ar_pct_movement = 0;
										}
										$ar_pct_movement = numberFormat($total_ar_pct_movement);
										echo $ar_pct_movement;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">Accounts Receivable - (GOP/VAT) </td>
									<td scope="col" align="right">
									<?php
										$ar_gop_starting = numberFormat($arr_ar_gop_starting);  
										echo $ar_gop_starting;
									  ?>									</td>
									<td scope="col" align="right">									
									<?php
										$ar_gop_ending = numberFormat($arr_ar_gop_ending);  	
										echo $ar_gop_ending;								
								 	?>									</td>
									<td scope="col" align="right">
									<?php
										$total_ar_gop_movement = $arr_ar_gop_ending-$arr_ar_gop_starting;
										$ar_gop_movement = numberFormat(($total_ar_gop_movement));
										echo $ar_gop_movement;									
									?>									</td>
									<td scope="col" align="right">
										<?php
										if ($arr_ar_gop_starting!=0)
										{
											$total_ar_gop_pct_movement = ($total_ar_gop_movement/$arr_ar_gop_starting)*100;
										}
										else
										{
											$total_ar_gop_pct_movement = 0;
										}
										$ar_gop_pct_movement = numberFormat($total_ar_gop_pct_movement);
										echo $ar_gop_pct_movement;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">Accounts Receivable - Others </td>
									<td scope="col" align="right">
									<?php
										$ar_others_starting = numberFormat($arr_ar_others_starting);  
										echo $ar_others_starting;
									  ?>									</td>
									<td scope="col" align="right">									
									<?php
										$ar_others_ending = numberFormat($arr_ar_others_ending);  									
										echo $ar_others_ending;
								 	?>									</td>
									<td scope="col" align="right">
									<?php
										$total_ar_others_movement = $arr_ar_others_ending-$arr_ar_others_starting;
										$ar_others_movement = numberFormat(($total_ar_others_movement));
										echo $ar_others_movement;									
									?>									</td>
									<td scope="col" align="right">
										<?php
										if ($arr_ar_others_starting!=0)
										{
											$total_ar_others_pct_movement = ($total_ar_others_movement/$arr_ar_others_starting)*100;
										}
										else
										{
											$total_ar_others_pct_movement = 0;
										}
										$ar_others_pct_movement = numberFormat($total_ar_others_pct_movement);
										echo $ar_others_pct_movement;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Total Receivables </td>
									<td scope="col">&nbsp;</td>
									<td scope="col" align="right">
									<?php
										$starting_ar = $arr_ar_starting+$arr_ar_gop_starting+$arr_ar_others_starting;
										$total_starting_ar = numberFormat($starting_ar);
										echo $total_starting_ar;
									?>									</td>
									<td scope="col" align="right">
									<?php
										$ending_ar = $arr_ar_ending+$arr_ar_gop_ending+$arr_ar_others_ending;
										$total_ending_ar = numberFormat($ending_ar);
										echo $total_ending_ar;
									?>									</td>
									<td scope="col" align="right">
									<?php
										$movement_ar = $total_ar_movement+$total_ar_gop_movement+$total_ar_others_movement;
										$total_movement_ar = numberFormat(($movement_ar));
										echo $total_movement_ar;
									?>									</td>
									<td scope="col" align="right">
									<?php
										if ($starting_ar!=0)
										{
											$movement_pct_ar = ($movement_ar/$starting_ar)*100;
										}
										else
										{
											$movement_pct_ar = 0;
										}
										$total_movement_pc_ar = numberFormat($movement_pct_ar);
										echo $total_movement_pc_ar;
									?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Total Other Assets</td>
									<td scope="col">&nbsp;</td>
									<td scope="col" align="right">
									<?php
										$toa_starting_ar = $starting_advances+$starting_ar;
										$toa_ar_starting = numberFormat($toa_starting_ar);
										echo $toa_ar_starting;
									?>									</td>
									<td scope="col" align="right">
									<?php
										$toa_ending_ar = $ending_advances+$ending_ar;
										$toa_ar_ending = numberFormat($toa_ending_ar);
										echo $toa_ar_ending;
									?>									</td>
									<td scope="col" align="right">
									<?php
										$toa_movement = $toa_ending_ar-$toa_starting_ar;
										$toa_total_movement = numberFormat(($toa_movement));
										echo $toa_total_movement;
									?>									</td>
									<td scope="col" align="right">
									<?php
										if ($toa_starting_ar!=0)
										{
											$toa_movement_pct = ($toa_movement/$toa_starting_ar)*100;
										}
										else
										{
											$toa_movement_pct  = 0;
										}
										$toa_total_movement_pct = numberFormat($toa_movement_pct);
										echo $toa_total_movement_pct;										
									?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td colspan="7" scope="col"><hr></td>
								</tr>
								<tr>
								  <td colspan="3" scope="col">Total Assets </td>
									<td scope="col" align="right">
									<?php
										$ta_starting = $starting_current_assets+$toa_starting_ar;
										$total_assets_starting = numberFormat($ta_starting);
										echo $total_assets_starting;
									?>									</td>
									<td scope="col" align="right">
									<?php
										$ta_ending = $ending_current_assets+$toa_ending_ar;
										$total_assets_ending = numberFormat($ta_ending);
										echo $total_assets_ending;
									?>									</td>
									<td scope="col" align="right">
									<?php
										$ta_movement = $movement_current_assets+$toa_movement;
										$total_assets_movement = numberFormat(($ta_movement));
										echo $total_assets_movement;
									?>									</td>
									<td scope="col"align="right">
									<?php
										if ($ta_starting!=0)
										{
											$ta_pct_movement = ($ta_movement/$ta_starting)*100;
										}
										else
										{
											$ta_pct_movement = 0;
										}
										$total_assets_movement_pct = numberFormat($ta_pct_movement);
										echo $total_assets_movement_pct;
									?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td colspan="7" scope="col"><hr></td>
								</tr>
								<tr>
								  <td scope="col">Liabilities and Equity </td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Liabilities</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Current Liabilities </td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">Accounts Payable </td>
									<td scope="col" align="right">
									<?php
										$ap_starting = numberFormat($arr_ap_starting);  
										echo $ap_starting;
									  ?>									</td>
									<td scope="col" align="right">
									<?php
										$ap_ending = numberFormat($arr_ap_ending);  									
										echo $ap_ending;
								 	?>									
									<td scope="col" align="right">
									<?php
										$total_ap_movement = $arr_ap_ending-$arr_ap_starting;
										$ap_movement = numberFormat(($total_ap_movement));
										echo $ap_movement;									
									?>									</td>
									<td scope="col" align="right">
										<?php
										if ($arr_ap_starting!=0)
										{
											$total_ap_pct_movement = ($total_ap_movement/$arr_ap_starting)*100;
										}
										else
										{
											$total_ap_pct_movement = 0;
										}
										$ap_pct_movement = numberFormat($total_ap_pct_movement);
										echo $ap_pct_movement;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">Withholding Tax Payable </td>
									<td scope="col" align="right">
									<?php
										$wtp_starting = numberFormat($arr_wtp_starting);  
										echo $wtp_starting;
									  ?>									</td>
									<td scope="col" align="right">
									<?php
										$wtp_ending = numberFormat($arr_wtp_ending);  									
										echo $wtp_ending;
								 	?>									</td>
									<td scope="col" align="right">
									<?php
										$total_wtp_movement = $arr_wtp_ending-$arr_wtp_starting;
										$wtp_movement = numberFormat(($total_wtp_movement));
										echo $wtp_movement;									
									?>									</td>
									<td scope="col" align="right">
										<?php
										if ($arr_wtp_starting!=0)
										{
											$total_wtp_pct_movement = ($total_wtp_movement/$arr_wtp_starting)*100;
										}
										else
										{
											$total_wtp_pct_movement = 0;
										}
										$wtp_pct_movement = numberFormat($total_wtp_pct_movement);
										echo $wtp_pct_movement;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">Other Payables </td>
									<td scope="col" align="right">
									<?php
										$op_starting = numberFormat($arr_op_starting);  
										echo $op_starting;
									  ?>									</td>
									<td scope="col" align="right">
									<?php
										$op_ending = numberFormat($arr_op_ending);  									
										echo $op_ending;
								 	?>									</td>
									<td scope="col" align="right">
									<?php
										$total_op_movement = $arr_op_ending-$arr_op_starting;
										$op_movement = numberFormat(($total_op_movement));
										echo $op_movement;									
									?>									</td>
									<td scope="col" align="right">
										<?php
										if ($arr_op_starting!=0)
										{
											$total_op_pct_movement = ($total_op_movement/$arr_op_starting)*100;
										}
										else
										{
											$total_op_pct_movement = 0;
										}
										$op_pct_movement = numberFormat($total_op_pct_movement);
										echo $op_pct_movement;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Total Current Liabilities </td>
									<td scope="col">&nbsp;</td>
									<td scope="col" align="right">
										<?php
										$tcl_starting = $arr_ap_starting+$arr_wtp_starting+$arr_op_starting;
										$tcl_total_starting = numberFormat($tcl_starting);
										echo $tcl_total_starting;
										?>									</td>
									<td scope="col" align="right">
										<?php
										$tcl_ending = $arr_ap_ending+$arr_wtp_ending+$arr_op_ending;
										$tcl_total_ending = numberFormat($tcl_ending);
										echo $tcl_total_ending;
										?>									</td>
									<td scope="col" align="right">
										<?php
										$tcl_movement = $total_ap_movement+$total_wtp_movement+$total_op_movement;
										$tcl_total_movement = numberFormat(($tcl_movement));
										echo $tcl_total_movement;
										?>									</td>
									<td scope="col" align="right">
									<?php
										if ($tcl_starting!=0)

										{
											$tcl_movement_pct = ($tcl_movement/$tcl_starting)*100;
										}
										else
										{
											$tcl_movement_pct = 0;
										}
										$tcl_total_movement_pct = numberFormat($tcl_movement_pct);
										echo $tcl_total_movement_pct;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Total Liabilities </td>
									<td scope="col">&nbsp;</td>
									<td scope="col" align="right">
									<?php
										$tcl_starting = $arr_ap_starting+$arr_wtp_starting+$arr_op_starting;
										$tcl_total_starting = numberFormat($tcl_starting);
										echo $tcl_total_starting;
										?>									</td>
									<td scope="col" align="right">
									<?php
										$tcl_ending = $arr_ap_ending+$arr_wtp_ending+$arr_op_ending;
										$tcl_total_ending = numberFormat($tcl_ending);
										echo $tcl_total_ending;
										?>									</td>
									<td scope="col" align="right">
									<?php
										$tcl_movement = $total_ap_movement+$total_wtp_movement+$total_op_movement;
										$tcl_total_movement = numberFormat(($tcl_movement));
										echo $tcl_total_movement;
										?>									</td>
									<td scope="col" align="right">
									<?php
										if ($tcl_starting!=0)
										{
											$tcl_movement_pct = ($tcl_movement/$tcl_starting)*100;
										}
										else
										{
											$tcl_movement_pct = 0;
										}
										$tcl_total_movement_pct = numberFormat($tcl_movement_pct);
										echo $tcl_total_movement_pct;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td colspan="7" scope="col"><hr></td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Equity</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Fund Equity </td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">Fund Equity, Beginning </td>
									<td scope="col" align="right">
									<?php
										echo numberFormat($arr_feb_starting);  
									  ?>									</td>
									<td scope="col" align="right">
									<?php
										echo numberFormat($arr_feb_ending);
									?>
                                    </td>
									<td scope="col" align="right">
									<?php
										$total_feb_movement = $arr_feb_ending-$arr_feb_starting;
										echo numberFormat($total_feb_movement);
									?>									</td>
									<td scope="col" align="right">
										<?php
										if ($arr_feb_starting!=0)
										{
											$total_feb_pct_movement = ($total_feb_movement/$arr_feb_starting)*100;
										}
										else
										{
											$total_feb_pct_movement = 0;
										}
										$feb_pct_movement = numberFormat($total_feb_pct_movement);
										echo $feb_pct_movement;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">&nbsp;</td>
									<td scope="col">Prior Period Adjustment </td>
									<td scope="col" align="right">
									<?php
										echo numberFormat($arr_ppa_starting);  
									  ?>									</td>
									<td scope="col" align="right">
									<?php
										echo numberFormat($arr_ppa_ending);  									
								 	?>									</td>
									<td scope="col" align="right">
									<?php

										$total_ppa_movement = $arr_ppa_ending-$arr_ppa_starting;
										echo numberFormat(($total_ppa_movement));
									?>									</td>
									<td scope="col" align="right">
										<?php
										if ($arr_ppa_starting!=0)
										{
											$total_ppa_pct_movement = ($total_ppa_movement/$arr_ppa_starting)*100;
										}
										else
										{
											$total_ppa_pct_movement = 0;
										}
										$ppa_pct_movement = numberFormat($total_ppa_pct_movement);
										echo $ppa_pct_movement;
										?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Total Fund Equity </td>
									<td scope="col">&nbsp;</td>
									<td scope="col" align="right">
									<?php
										$tfe_starting = $arr_feb_starting+$arr_ppa_starting;
										echo numberFormat($tfe_starting);
									?>									</td>
									<td scope="col" align="right">
									<?php
										$tfe_ending = $arr_feb_ending+$arr_ppa_ending;
										echo numberFormat($tfe_ending);
									?>									
                                    </td>
									<td scope="col" align="right">									
									<?php
										$tfe_movement = $total_feb_movement+$total_ppa_movement;
										$total_fund_equity_movement = numberFormat(($tfe_movement));
										echo $total_fund_equity_movement;
									?>									</td>
									<td scope="col" align="right">
									<?php
										if ($tfe_starting!=0)
										{
											$tfe_movement_pct = ($tfe_movement/$tfe_starting)*100;
										}
										else
										{
											$tfe_movement_pct = 0;
										}
										$total_fund_equity_movement_pct = numberFormat($tfe_movement_pct);
										echo $total_fund_equity_movement_pct;
									?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Fund Equity End </td>
									<td scope="col">&nbsp;</td>
									<td scope="col" align="right">
									<?php
										echo numberFormat($arr_fee_starting);  
									?>
									</td>
								  <td scope="col" align="right">
									<?php
										$total_fee_movement = (-abs($arr_fee_ending))+$arr_fee_starting;
										echo numberFormat($total_fee_movement);
									?>                            
                                    </td>
									<td scope="col" align="right">
									<?php
										echo numberFormat($arr_fee_ending);								
									?>                                    
									</td>
									<td scope="col" align="right">
									<?php
										if ($arr_fee_starting!=0)
										{
											$total_fee_pct_movement = ($total_fee_movement/$arr_fee_starting)*100;
										}
										else
										{
											$total_fee_pct_movement = 0;
										}
										echo numberFormat($total_fee_pct_movement);
									?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td colspan="7" scope="col"><hr></td>
								</tr>
								<tr>
								  <td scope="col">&nbsp;</td>
									<td scope="col">Total Equity </td>
									<td scope="col">&nbsp;</td>
									<td scope="col" align="right">
									<?php
										$te_starting = $arr_feb_starting+$arr_ppa_starting+$arr_fee_starting;
										echo numberFormat($te_starting);
									?>									</td>
									<td scope="col" align="right">
									<?php
										$te_ending = $arr_feb_ending+$arr_ppa_ending+$total_fee_movement;
										echo numberFormat($te_ending);
									?></td>
									<td scope="col" align="right">
									<?php
										$te_movement = $te_ending-$te_starting;
										$total_equity_movement = numberFormat(($te_movement));
										echo $total_equity_movement;
									?>									</td>
									<td scope="col" align="right">
									<?php
										if ($te_starting!=0)
										{
											$te_movement_pct = ($te_movement/$te_starting)*100;
										}
										else
										{
											$te_movement_pct = 0;
										}
										$total_equity_movement_pct = numberFormat($te_movement_pct);
										echo $total_equity_movement_pct;
									?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td colspan="7" scope="col"><hr></td>
								</tr>
								<tr>
								  <td colspan="3" scope="col">Total Liabilities and Equity </td>
									<td scope="col" align="right">
									<?php
										$tle_starting = $tcl_starting+$te_starting;
										echo numberFormat($tle_starting);
									?>									</td>
									<td scope="col" align="right">
									<?php
										$tle_ending = $tcl_ending+$te_ending;
										echo  numberFormat($tle_ending);
									?>									</td>
									<td scope="col" align="right">
									<?php
										$tle_movement = $tcl_movement+$te_movement;
										echo numberFormat(($tle_movement));
									?>									</td>
									<td scope="col" align="right">
									<?php
										if ($tle_starting!=0)
										{
											$tle_pct_movement = ($tle_movement/$tle_starting)*100;
										}
										else
										{
											$tle_pct_movement = 0;
										}
										$total_liabilities_and_equity_pct = numberFormat($tle_pct_movement);
										echo $total_liabilities_and_equity_pct;
									?>&nbsp;%									</td>
								</tr>
								<tr>
								  <td colspan="7" scope="col"><hr></td>
								</tr>
								<tr>
								  <td colspan="7" scope="col"><hr></td>
								</tr>								
						</tbody>
						<tfoot>
					  			<tr>
					  			  <td scope="col">&nbsp;</td>
					  			  <td scope="col">&nbsp;</td>
					  			  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
								  <td scope="col">&nbsp;</td>
						<tfoot>
                    </table>
				</td>
				</tr>
			</table>	
		</td>
	</tr>
</table>


<br>
<br>
<div align='center' class='hideonprint'>
	<a href='JavaScript:window.print();'><img src='/workspaceGOP/images/printer.png' border='0' title='Print this page'></a> &nbsp; 
</div>
<div align='center' class='hideonprint'>
	<em>
	To remove header and footer of page
	<br>
	Go to "File" -> "Page Setup..." then erase the text in the "Headers and Footers" field.
	</em>
</div>