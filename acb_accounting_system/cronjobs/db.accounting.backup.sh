temp="`date +"%Y%m%d"`"
wtmp='db_shift_monitoring_isit_'$temp'.tar'
dtmp='db_accounting_systemGOP'
dtmp1=$dtmp'_'$temp'.sql'

#if [ ! -e "/data/db_backup" ]; then
# /bin/mkdir -p /var/www/html/wimpsys/db_backup
#fi

/usr/bin/mysqldump --user=sysadmin --password=1q0o2w9i --databases db_accounting_systemGOP > /var/www/html/workspaceGOP/cronjobs/db.accounting.system.backup/$dtmp1 &
gzip /var/www/html/workspaceGOP/cronjobs/db.accounting.system.backup/$dtmp1 > $dtmp'.gz'