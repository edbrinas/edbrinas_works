<?php
include("./../connections/cn.php");
include("./../includes/function.php");
$date_time_inserted = date("Y-m-d H:i:s");
$show_msg = 0;
$err = 0;

if ($_POST['Submit'] == 'Upload File')
{
	$user_file = $_FILES['file_upload']['tmp_name'];
	if(isset($user_file))
	{
		$temp_file = $_FILES['file_upload']['tmp_name'];
		$file_name = $_FILES['file_upload']['name'];
		$handle = fopen($temp_file, "r");
		$filesize = filesize($temp_file);	
		$file_extention = substr($file_name, strrpos($file_name, '.'));

		if ($file_extention==".csv")
		{
			$x = 0;
			
			$sql = "DELETE FROM tbl_fund_analysis_by_component_by_budget
					WHERE	fa_budget_year = '".$_POST["budget_year"]."'";
			mysql_query($sql) or die($sql.mysql_error());	
						
			while($data = fgetcsv($handle, $filesize, ","))
			{

				$x = $x + 1;
				if ($x!=1 && $data[0]!="EOF")
				{
					$fa_account_code = trim($data[0]);
					$fa_component_id = trim($data[2]);
					$fa_budget = trim($data[3]);
		
					$fa_account_code = getSlIdByAccountCode($fa_account_code);
					
					if ($fa_account_code==0) { $err=1; $show_msg = 1; $msg .= "Error on line: ".$x.". Invalid account code: ".$data[0]."<br>"; }

					if ($err==0)
					{
						$sql = "INSERT INTO tbl_fund_analysis_by_component_by_budget 
											(
											fa_budget_year,
											fa_account_code,
											fa_component_id,
											fa_budget
											)
								VALUES (	'".$_POST["budget_year"]."',
											'".trim($fa_account_code)."',
											'".trim($fa_component_id)."',
											'".trim($fa_budget)."'
										)";
						mysql_query($sql) or die($sql.mysql_error());
					}					
				}
			}
			if ($err==0)
			{
				$show_msg = 1;
				$msg .= "File uploaded successfully!";
			}
		}
		else
		{
			$show_msg = 1;
			$msg = "Invalid file name.";
		}		
	}
}
?>
<form name="upload_bid" method="post" action="" enctype="multipart/form-data">
<table align="center" border="1">
    <thead>
        <tr>
            <th scope="col" colspan="2">Upload By Budget ID, By Component ID</th>
        </tr>
        <?php
		if ($show_msg==1)
		{
		?>
        <tr>
            <th scope="col" colspan="2"><?php echo $msg; ?></th>
        </tr>        
        <?php
        }
        ?>
    </thead>
    <tfoot>
        <tr>
            <td align="right">&nbsp;</td>
            <td align="left"><input type="submit" name="Submit" value="Upload File" class="formbutton"></td>
        </tr>
    </tfoot>
    <tbody>				
        <tr>
          <td align="right" valign="top">Component Year</td>
          <td align="left">
          <select name="budget_year">
          <?php
		  for ($x=date("Y")-1;$x<=date("Y")+2;$x++)
		  {
		  	?>
            <option value="<?php echo $x; ?>" <?php if ($x==date("Y")) { echo "SELECTED"; } ?>><?php echo $x; ?></option>
            <?php
		  }
		  ?>
          </select>
          
          </td>
        </tr>
        <tr>
            <td align="right" valign="top"><b>File:*</b></td>
            <td align="left"><input name='file_upload' type='file' class="formbutton"/></td>
        </tr>			
    </tbody>
</table>	  
</form>