<?php include("_includes/_header.php"); ?>

          <header class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand"><img src="<?php echo $b_url; ?>_assets/logo.png"></h3>
              <nav class="nav nav-masthead">
                <a class="nav-link" href="<?php echo $b_url; ?>contact.php">اتصل بنا</a>
                <a class="nav-link active" href="<?php echo $b_url; ?>about.php">خدمتنا</a>
                <a class="nav-link" href="<?php echo $b_url; ?>">من نحن</a>
              </nav>
            </div>
          </header>

          <main role="main" class="inner cover">
            <h1 class="display-3 text-warning">من نحن</h1>
            <p class="lead">
              <h1 class="h5 text-right text-warning"> رؤيتنا </h1>
              <p class="text-right"> شركة مواصلات وطنية ذات جودة ومعايير عالية </p>
              <h1 class="h5 text-right text-warning"> رسالتنا </h1>
              <p class="text-right"> شركة تقدم أرقى الخدمات في مجال النقل باستخدام أحدث التقنيات لخدمة قطاع الأعمال والأفراد والمجتمع </p>
              <h1 class="h5 text-right text-warning"> أهدافُنا </h1>
              <p class="text-right">
                   ن نصبح الشركة السعودية الأولى في مجال تقديم خدمات النقل في الشرق الأوسط -
                  <br> تقديم خدمات سريعة جديرة بالثقة -
                  <br> دعم الاقتصاد الوطني -
                  <br> دعم الشاب السعودي لتحقيق دخل إضافي -
              </p>
            </p>
          </main>

<?php include("_includes/_footer.php"); ?>
