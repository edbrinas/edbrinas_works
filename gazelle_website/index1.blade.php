<?php include("_includes/_header.php"); ?>

          <header class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand"><img src="<?php echo $b_url; ?>_assets/logo.png"></h3>
              <nav class="nav nav-masthead">
                <a class="nav-link" href="<?php echo $b_url; ?>contact.php">اتصل بنا</a>
                <a class="nav-link" href="<?php echo $b_url; ?>about.php">خدمتنا</a>
                <a class="nav-link active" href="<?php echo $b_url; ?>">من نحن</a>
              </nav>
            </div>
          </header>

          <main role="main" class="inner cover">
            <h1 class="display-3 text-warning">تطبيق غـــــزال</h1>
            <p class="lead">تطبيق توجيه مركبات سعودي يستخدم أفضل التقنيات في مجال النقل لخدمة المجتمع</p>
            <p class="lead">
              <a href="https://itunes.apple.com/ph/app/gazelle-%D8%BA%D8%B2%D8%A7%D9%84/id1223399855?mt=8"><img src="<?php echo $b_url; ?>_assets/3525-button-apple-store.png"></a>
              <a href="https://play.google.com/store/apps/details?id=com.it.gazelle"><img src="<?php echo $b_url; ?>_assets/3525-button-google-play.png"></a>
            </p>
          </main>

<?php include("_includes/_footer.php"); ?>
