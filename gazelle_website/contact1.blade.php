<?php include("_includes/_header.php"); ?>

          <header class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand"><img src="<?php echo $b_url; ?>_assets/logo.png"></h3>
              <nav class="nav nav-masthead">
                <a class="nav-link active" href="<?php echo $b_url; ?>contact.php">اتصل بنا</a>
                <a class="nav-link" href="<?php echo $b_url; ?>about.php">خدمتنا</a>
                <a class="nav-link" href="<?php echo $b_url; ?>">من نحن</a>
              </nav>
            </div>
          </header>

          <main role="main" class="inner cover">
            <h1 class="display-3 text-warning"> اتصل بنا </h1>
            <p class="lead">
              المملكة العربية السعودية - الرياض -
            </p>
            <p class="lead text-warning">
              920004690
            </p>
          </main>

<?php include("_includes/_footer.php"); ?>
