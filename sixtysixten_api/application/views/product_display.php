<html>
<head>
  <title>Test Website</title>

  <link rel="stylesheet" href="<?php echo base_url(); ?>_assets/main.css" />
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Titillium+Web:400,600,700" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <script>

  $(document).ready(function(){
       $("#product_variant_id").change(function(){
         $('#items_place_holder').empty();
         $.ajax({type: 'POST',
              url: "<?php echo base_url().'main/GetRecords'; ?>",
              data: $("#sys_frm").serialize(),
              success:function(response)
              {
                  $("#items_place_holder").append(response);
              },
              error:function (xhr, ajaxOptions, thrownError)
              {
                console.log(thrownError);
    						alert(thrownError);
              }
            });
       });
  });
  </script>

</head>

<body>
  <header>
      <div class="container">
        <div class="logo">Technical Test</div>
      </div>
  </header>

  <div class="container">
    <div class="content">
        <article>
          <form id="sys_frm" />

            <h1>Product Name: <?php echo $p_name; ?></h1>
            <h2>Product ID: <?php echo $p_id; ?></h2>

            <label>Variant Color:</label>

            <select name="product_variant_id" id="product_variant_id" >
              <?php echo $a_colors; ?>
            </select>
            <div id="items_place_holder"></div>
          </form>
        </article>
    </div>
  </div>

</body>

</html>
