<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_api extends CI_Model {

  function __construct()  {
      parent::__construct();
      $this->db = $this->load->database('main',TRUE);
  }

	/**
	 * Function to clean user input.
	 *
	 */
	private function _cleanRecord($options=array()) {
		foreach($options as $key=>$value) {
			$clean_array[$key] = addslashes(trim(strip_tags($value)));
		}
		return $clean_array;
	}

	/**
	 * Function Name
	 * ----------------
	 * AddRecord - Creates a record to database.
	 *
	 * Options: Array
	 * -----------------
	 * product_id @ Numeric
   * product_name @ String
   * product_variant_id @ Numeric
   * product_variant_position @ Numeric
   * product_variant_color @ String
	 *
	 * return numeric
	 *
	 */
	public function AddRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "INSERT INTO mtcproducts (product_id,product_name,product_variant_id,product_variant_position,product_variant_color)
				VALUES ({$this->db->escape($options['product_id'])},
						{$this->db->escape($options['product_name'])},
						{$this->db->escape($options['product_variant_id'])},
						{$this->db->escape($options['product_variant_position'])},
            {$this->db->escape($options['product_variant_color'])})";
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * UpdateRecord- Updates a record in the database.
	 *
	 * Options: Array
	 * -----------------
   * product_id @ Numeric
   * product_name @ String
   * product_variant_id @ Numeric
   * product_variant_position @ Numeric
   * product_variant_color @ String
	 *
	 * return numeric
	 *
	 */

	public function UpdateRecord($options=array())
	{
		$options = $this->_cleanRecord($options);

		$sql = "UPDATE mtcproducts SET ";
		$sql .= !empty($options['product_name']) ? " product_name = {$this->db->escape($options['product_name'])}, " : " ";
		$sql .= !empty($options['product_variant_id']) ? " product_variant_id = {$this->db->escape($options['product_variant_id'])}, " : " ";
		$sql .= !empty($options['product_variant_position']) ? " product_variant_position = {$this->db->escape($options['product_variant_position'])}, " : " ";
    $sql .= !empty($options['product_variant_color']) ? " product_variant_color = {$this->db->escape($options['product_variant_color'])}, " : " ";
    $sql = rtrim(trim($sql), ",");
		$sql .= "  WHERE product_id = {$options['product_id']}  ";
		$this->db->query(trim($sql));
		return $this->db->insert_id();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetAllRecords- Retrieves all records in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
   * product_id @ Numeric
   * product_name @ String
   * product_variant_id @ Numeric
   * product_variant_position @ Numeric
   * product_variant_color @ String
	 *
	 * return Array
	 *
	 */
	public function GetAllRecords($options=array()) {
		$sql = "SELECT * FROM mtcproducts ";
		$sql .= !empty($options['search_by']) ? strtoupper($options['search_by']) : " ";
		$sql .= !empty($options['order_by']) ? strtoupper($options['order_by'])." " : " ORDER BY product_id DESC ";
    $sql .= !empty($options['limit']) ? "LIMIT ".$options['limit'] : " ";
    $sql .= !empty($options['offset']) ? "OFFSET ".$options['offset'] : " ";
		$rs = $this->db->query(trim($sql));
		return $rs->result_array();
	}

	/**
	 * Function Name
	 * ----------------
	 * GetSingleRecord - Retrieve a single record in the database. Can be filtered using 'Where' statement.
	 *
	 * Options: Array
	 * -----------------
	 * Search By = String
	 *
	 * return Arrays
	 *
	 */
	public function GetSingleRecord($options=array()) {
		$sql = "SELECT * FROM mtcproducts ";
		$sql .= !empty($options['search_by']) ? strtoupper($options['search_by']) : " ";
		$sql .= "LIMIT 1 ";
		$rs = $this->db->query(trim($sql));
		return $rs->row();
	}

}

/* End of file Mdl_affiliateprogram.php */
/* Location: ./application/model/Mdl_affiliateprogram.php */
