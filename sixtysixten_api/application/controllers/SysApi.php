<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SysApi extends CI_Controller {

	public function __construct() {
		parent::__construct();

		// Load all libraries and models on page load
		$this->load->model('Mdl_api');
	}

	/**
	 * Function Name
	 * ----------------
	 * API- API for parsing and inserting data to database
	 *
	 * Options: None
	 * -----------------
	 *
	 * return Json Data
	 *
	 */
	public function api()
	{
    // Read JSON contents
    $json_data = file_get_contents('http://localhost/sixtysixten_api/_assets/data.json');

    // Convert to Array
    $data = json_decode($json_data, true);

    // Parse the data
    for ($i = 0; $i <= count($data['product']['variants'])-1; $i++) {
			// Insert to database
			$this->Mdl_api->AddRecord(array(
				'product_id' => $data['product']['id'],
	      'product_name' => $data['product']['title'],
	      'product_variant_id' => $data['product']['variants'][$i]['id'],
	      'product_variant_position' => $data['product']['variants'][$i]['position'],
	      'product_variant_color' => $data['product']['variants'][$i]['title']));
    }
		// Send response
		echo json_encode(array('success'=>1,'msg'=>'Records Added','response_code'=>200));
	}

	/**
	 * Function Name
	 * ----------------
	 * PostProduct - Post a product to shopify
	 *
	 * Options: None
	 * -----------------
	 *
	 * return Json Data
	 *
	 */
	public function PostProduct() {

		$u_store = "mtcDigital";
		$api_key = "407ee273b6de38e00280693534ffedb7";
		$api_pwd = "ca634ded78faf36d1279ab8d47b2437b";

		$a_p_data = array(
		    "product"=>array(
		        "title"=> "Edward Brinas Product",
		        "vendor"=> $u_store,
						"created_at"=>date('Y-m-d\TH:i:sO'),
		        "variants"=>array(
							array(
                "option1"=>"Small",
                "price"=>100,
								"position"=>1
							),
							array(
                "option1"=>"Medium",
                "price"=>100,
								"position"=>2
							),
							array(
                "option1"=>"Large",
                "price"=>100,
								"position"=>3
							)
		        )
		    )
		);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "https://{$api_key}:{$api_pwd}@hostname/admin/products.json");
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($a_p_data));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$response = curl_exec ($curl);
		curl_close ($curl);

		print_r($response);
	}

}
