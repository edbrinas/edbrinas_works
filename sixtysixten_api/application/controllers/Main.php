<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

  public function __construct() {
		parent::__construct();

		// Load all libraries and models on page load
		$this->load->model('Mdl_api');
	}

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
      $a_data = $this->Mdl_api->GetAllRecords(NULL);

      if ($a_data) {
				foreach ($a_data as $row) {
          $data['p_name'] = $row['product_name'];
          $data['p_id'] = $row['product_id'];
          $a_colors .= "<option value='".$row['product_variant_id']."'>".$row['product_variant_color']."</option>";
				}
        $data['a_colors'] = $a_colors;
			}
      $this->load->view('product_display',$data);
	}

  /**
	 * GetRecords - Get records based on user input.
	 *
   *
   * return string
	 */
  public function GetRecords()
	{
      // Get data based on user input
			$a_data = $this->Mdl_api->GetAllRecords(array('search_by'=>' WHERE product_variant_id = "'.$_POST['product_variant_id'].'"'));
			$data = "<p>";
      $data .= "Product Variant ID: ".$a_data[0]['product_variant_id'];
      $data .= "<br />";
      $data .= "Position: ".$a_data[0]['product_variant_position'];
			$data .= "</p>";
			echo $data;
	}

}
